//package org.sofiframework.infrasofi.serviceweb;
//
//import javax.jws.WebService;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//
//@WebService(endpointInterface = "org.sofiframework.infrasofi.serviceweb.AlloServiceWeb")
//public class AlloServiceWebImpl implements AlloServiceWeb {
//  
//  private Log log = LogFactory.getLog(this.getClass());
//  
//  @Override
//  public String dit(String texte) {
//    log.info("Allo " + texte);
//    return "Allo " + texte;
//  }
//}
