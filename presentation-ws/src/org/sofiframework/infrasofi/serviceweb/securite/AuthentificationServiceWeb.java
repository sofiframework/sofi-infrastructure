package org.sofiframework.infrasofi.serviceweb.securite;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
public interface AuthentificationServiceWeb {

  @WebResult(name = "codeUtilisateur")
  String getCodeUtilisateur(
      @WebParam(name="certificat") String certificat
  );

  void terminerCertificat(
      @WebParam(name="certificat") String certificat
  );

  @WebResult(name = "nouveauCertificat")
  String authentififier(
      @WebParam(name = "codeUtilisateur") String codeUtilisateur, 
      @WebParam(name = "motPasses") String motPasse
  );

  @WebResult(name = "utilisateur")
  String getUtilisateur(
      @WebParam(name = "codeApplication") String codeApplication, 
      @WebParam(name = "codeFacette") String codeFacette,
      @WebParam(name = "codeUtilisateur") String identifiant, 
      @WebParam(name = "codeClient") String codeClient
  );

}