//
//package org.sofiframework.infrasofi.serviceweb.securite;
//
//import javax.jws.WebService;
//
//import org.sofiframework.application.securite.objetstransfert.Utilisateur;
//import org.sofiframework.application.securite.service.ServiceAuthentification;
//import org.sofiframework.application.securite.service.ServiceSecurite;
//import org.sofiframework.application.securite.service.ServiceValidationAuthentification;
//
//@WebService(endpointInterface = "org.sofiframework.infrasofi.serviceweb.securite.AuthentificationServiceWeb")
//public class AuthentificationServiceWebImpl implements AuthentificationServiceWeb {
//
//  private ServiceAuthentification serviceAuthentification;
//
//  private ServiceSecurite serviceSecurite;
//
//  private ServiceValidationAuthentification serviceValidationAuthentification;
//
//  public void test() {
//  }
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see
//   * org.sofiframework.infrasofi.serviceweb.securite.AuthentificationServiceWeb
//   * #getCodeUtilisateur(java.lang.String)
//   */
//  @Override
//  public String getCodeUtilisateur(String certificat) {
//    String codeUtilisateur = this.serviceAuthentification.getCodeUtilisateur(certificat);
//    return codeUtilisateur;
//  }
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see
//   * org.sofiframework.infrasofi.serviceweb.securite.AuthentificationServiceWeb
//   * #terminerCertificat(java.lang.String)
//   */
//  @Override
//  public void terminerCertificat(String certificat) {
//    this.serviceAuthentification.terminerCertificat(certificat);
//  }
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see
//   * org.sofiframework.infrasofi.serviceweb.securite.AuthentificationServiceWeb
//   * #authentififier(java.lang.String, java.lang.String)
//   */
//  @Override
//  public String authentififier(String codeUtilisateur, String motPasse) {
//    Boolean valide = this.serviceValidationAuthentification.validerAuthentification(codeUtilisateur, motPasse);
//    String certificat = null;
//    if (valide) {
//      certificat = this.serviceAuthentification.genererCertificat(codeUtilisateur);
//    }
//    return certificat;
//  }
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see
//   * org.sofiframework.infrasofi.serviceweb.securite.AuthentificationServiceWeb
//   * #getUtilisateur(java.lang.String, java.lang.String, java.lang.String,
//   * java.lang.String)
//   */
//  @Override
//  public String getUtilisateur(String codeApplication, String codeFacette, String identifiant, String codeClient) {
//    Utilisateur utilisateur = serviceSecurite.getUtilisateurSecuriseSelonClient(codeApplication, codeFacette, identifiant, codeClient);
//    return utilisateur.toString();
//  }
//
//  public void setServiceAuthentification(ServiceAuthentification serviceAuthentification) {
//    this.serviceAuthentification = serviceAuthentification;
//  }
//
//  public ServiceAuthentification getServiceAuthentification() {
//    return serviceAuthentification;
//  }
//
//  public void setServiceSecurite(ServiceSecurite serviceSecurite) {
//    this.serviceSecurite = serviceSecurite;
//  }
//
//  public ServiceSecurite getServiceSecurite() {
//    return serviceSecurite;
//  }
//
//  public void setServiceValidationAuthentification(ServiceValidationAuthentification serviceValidationAuthentification) {
//    this.serviceValidationAuthentification = serviceValidationAuthentification;
//  }
//
//  public ServiceValidationAuthentification getServiceValidationAuthentification() {
//    return serviceValidationAuthentification;
//  }
//}