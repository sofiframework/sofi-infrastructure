package org.sofiframework.infrasofi.api;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.application.securite.objetstransfert.Role;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.application.securite.service.ServiceSecurite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/v1")
public class AuthentificationApi {

  @Autowired
  private ServiceAuthentification serviceAuthentification;

  @Autowired
  @Qualifier("serviceSecuriteCommun")
  private ServiceSecurite serviceSecurite;

  @RequestMapping("ping")
  @ResponseBody
  public String ping() {
    return "{ status : 'ok' }";
  }

  @RequestMapping("/application/{codeApplication}/utilisateur/{codeUtilisateur:.+}/certificat")
  @ResponseBody
  public String genererCertificat(@PathVariable String codeUtilisateur,
      HttpServletRequest request) {
    Integer nbJours = getIntParam("nbJourExpiration", 1, request);
    String ip = request.getParameter("ip");
    String userAgent = request.getParameter("userAgent");
    String urlReferer = request.getParameter("urlReferer");
    String certificat = this.serviceAuthentification.genererCertificat(
        codeUtilisateur, nbJours, ip, userAgent, urlReferer);
    return certificat;
  }

  @RequestMapping(value = "/certificat/{codeUtilisateur}", method = RequestMethod.DELETE)
  @ResponseBody
  public String terminerCertificat(@PathVariable String codeUtilisateur) {
    this.serviceAuthentification.terminerCertificatPourCode(codeUtilisateur);
    return "{ status : 'ok' }";
  }

  @RequestMapping("/application/{codeApplication}/utilisateur/{codeUtilisateur:.+}")
  @ResponseBody
  public Utilisateur getUtilisateur(@PathVariable String codeApplication,
      @PathVariable String codeUtilisateur) {
    Utilisateur utilisateur = this.serviceSecurite
        .getUtilisateurAvecObjetsSecurises(codeApplication, codeUtilisateur);
    for (Role r : utilisateur.getListeRoles()) {
      r.setRoleParent(null);
      r.setListeRoleLie(null);
      r.setListeRoles(null);
    }
    utilisateur.setListeNotification(null);
    utilisateur.setListeObjetSecurisables(null);
    utilisateur.setListePropriete(null);

    return utilisateur;
  }

  @RequestMapping("/certificat/{certificat:.+}")
  @ResponseBody
  public String getCodeUtilisateur(@PathVariable String certificat) {
    return this.serviceAuthentification.getCodeUtilisateur(certificat);
  }

  private Integer getIntParam(String nom, int defaut, HttpServletRequest request) {
    Integer nombre = defaut;
    String valeur = request.getParameter(nom);
    if (valeur != null) {
      try {
        nombre = Integer.parseInt(valeur);
      } catch (Exception e) {
        // on laisse la valeur par défaut
      }
    }
    return nombre;
  }
}