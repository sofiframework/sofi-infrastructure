/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.util;
import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

import org.apache.commons.beanutils.PropertyUtils;
import org.sofiframework.exception.SOFIException;

@SuppressWarnings("rawtypes")
public class ComparateurPropriete implements Comparator {
  private String nomPropriete;
  private Comparator comparateur;

  public ComparateurPropriete(String nomPropriete) {
    this(nomPropriete, null);
  }

  public ComparateurPropriete(String nomPropriete, Comparator comparateur) {
    this.nomPropriete = nomPropriete;
    this.comparateur = comparateur;
  }

  @SuppressWarnings("unchecked")
  public int compare(Object o1, Object o2) {
    if (o1 == null) {
      throw new NullPointerException("o1");
    } else if (o2 == null) {
      throw new NullPointerException("o2");
    }
    Object p1;
    Object p2;
    try {
      p1 = PropertyUtils.getProperty(o1, nomPropriete);
      p2 = PropertyUtils.getProperty(o2, nomPropriete);
    } catch (InvocationTargetException e) {
      Throwable ex = e.getTargetException();
      if (ex instanceof RuntimeException) {
        throw (RuntimeException) ex;
      } else if (ex instanceof Error) {
        throw (Error) ex;
      } else {
        throw new SOFIException(e);
      }
    } catch (NoSuchMethodException e) {
      throw new IllegalArgumentException(
          "La propriété " + nomPropriete + " est introuvable");
    } catch (IllegalAccessException e) {
      throw new IllegalArgumentException(
          "La propriété " + nomPropriete + " n'est pas publique");
    }
    if (p1 == null) {
      throw new NullPointerException("o1." + nomPropriete);
    } else if (p2 == null) {
      throw new NullPointerException("o2." + nomPropriete);
    } else if (comparateur != null) {
      return comparateur.compare(p1, p2);
    } else if (p1 instanceof Comparable && p2 instanceof Comparable) {
      return ((Comparable) p1).compareTo(p2);
    } else {
      throw new IllegalStateException(
          "Les propriétés comparés doivent implémenté Comparable si " +
          "aucun comparateur n'est fournis");
    }
  }
}