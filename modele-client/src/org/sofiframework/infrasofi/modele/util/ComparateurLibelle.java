/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.util;
import java.util.Comparator;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Libelle;

@SuppressWarnings("rawtypes")
public class ComparateurLibelle implements Comparator {
  private HttpServletRequest request;
  public ComparateurLibelle(HttpServletRequest request) {
    this.request = request;
    if (request == null) {
      throw new NullPointerException("request");
    }
  }

  public int compare(Object o1, Object o2) {
    Libelle libelle1 = UtilitaireLibelle.getLibelle(o1.toString(), request, null);
    Libelle libelle2 = UtilitaireLibelle.getLibelle(o2.toString(), request, null);
    return libelle1.getMessage().compareTo(libelle2.getMessage());
  }
}