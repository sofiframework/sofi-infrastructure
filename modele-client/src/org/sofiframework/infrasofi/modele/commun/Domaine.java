/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.commun;

/**
 * Classe qui regroupe les valeurs de domaine qui sont utilisées dans le code.
 * Contralise les domaines de valeurs au même endroit.
 * 
 * Exemples :
 * 
 * Pour obtenir la valeur : Domaine.role.accesPublic.getValeur() <br/>
 * Pour obtenir le nom du domaine (lorsque requis) :
 * Domaine.typeUtilisateur.getNom()
 * 
 * @author Jean-Maxime Pelletier
 */
public class Domaine {

  /**
   * Les enums qui sont des valeurs de domaines devrait implémenter cette
   * interface.
   */
  private interface ValeurDomaine {
    public String getValeur();

    public String toString();
  }

  /**
   * Ce n'est pas un domaine précis mais les Actifs inactifs on tous les mêmes
   * valeurs.
   */
  public static enum statut implements ValeurDomaine {

    actif("A"), inactif("I");

    private String valeur;

    statut(String valeur) {
      this.valeur = valeur;
    }

    public String getValeur() {
      return this.valeur;
    }

    @Override
    public String toString() {
      return this.valeur;
    }
  }

  /**
   * Valeurs possibles pour le domaine de valeurs LISTE_TYPE_UTILISATEUR
   */
  public static enum typeUtilisateur implements ValeurDomaine {

    interne("interne"), externe("externe");

    private String valeur;

    typeUtilisateur(String valeur) {
      this.valeur = valeur;
    }

    public static String getNom() {
      return "LISTE_TYPE_UTILISATEUR";
    }

    public String getValeur() {
      return this.valeur;
    }

    @Override
    public String toString() {
      return this.valeur;
    }
  }

  /*
   * Ce n'est pas une valeur de domaine mais je garde quand même la meme façon
   * de faire.
   */
  public static enum application {

    console("INFRA_SOFI");

    private String valeur;

    application(String valeur) {
      this.valeur = valeur;
    }

    public String getValeur() {
      return this.valeur;
    }

    @Override
    public String toString() {
      return this.valeur;
    }
  }

  /**
   * Liste des valeurs de role. Utiliser la nomenclature suivante :
   * acces[codeApp][codeRole]
   */
  public static enum role {

    accesPublic("PUBLIC"),

    accesDeveloppeur("DEVELOPPEUR");

    private String valeur;

    role(String valeur) {
      this.valeur = valeur;
    }

    public String getValeur() {
      return this.valeur;
    }

    @Override
    public String toString() {
      return this.valeur;
    }
  }

  public static enum statutOrdonnanceur implements ValeurDomaine {

    arrete("A"), demarre("D");

    private String valeur;

    private statutOrdonnanceur(String valeur) {
      this.valeur = valeur;
    }

    public String getValeur() {
      return this.valeur;
    }

    @Override
    public String toString() {
      return this.valeur;
    }
  }

  public static enum statutTentativeEnvoiCourriel implements ValeurDomaine {

    /**
     * Tentative qui est un succès. Le courriel
     * a été envoyé sans erreur.
     */
    succes("S"),

    /**
     * Tentative qui a généré une erreur.
     */
    echec("E");

    private String valeur;

    private statutTentativeEnvoiCourriel(String valeur) {
      this.valeur = valeur;
    }

    public String getValeur() {
      return this.valeur;
    }

    @Override
    public String toString() {
      return this.valeur;
    }
  }

  public static enum statutEnvoiCourriel implements ValeurDomaine {

    /**
     * Tentative d'envoi réussie
     */
    succes("S"),

    /**
     * Dernière tentative a résultée par
     * une erreur. On doit réessayer.
     */
    echec("E"),

    /**
     * Toutes les tentatives jusqu'au nombre
     * maximal permis on résultés en erreur.
     */
    abandon("A");

    private String valeur;

    private statutEnvoiCourriel(String valeur) {
      this.valeur = valeur;
    }

    public String getValeur() {
      return this.valeur;
    }

    @Override
    public String toString() {
      return this.valeur;
    }
  }

  public static enum typeContenuCourriel implements ValeurDomaine {

    html("html"),
    texte("texte");

    private String valeur;

    private typeContenuCourriel(String valeur) {
      this.valeur = valeur;
    }

    public String getValeur() {
      return this.valeur;
    }

    @Override
    public String toString() {
      return this.valeur;
    }
  }
}