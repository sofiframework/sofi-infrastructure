/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.commun;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.quartz.JobDataMap;
import org.sofiframework.modele.entite.EntiteTrace;
import org.sofiframework.modele.exception.ModeleException;

public abstract class QuartzEntite extends EntiteTrace {

  private static final long serialVersionUID = 5306319693631954089L;

  private Long id;

  public abstract Object getQuartzObject(Boolean useProperties);

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }

  protected Object getObjectFromBlob(byte[] blob) {
    Object result = null;
    if (blob != null) {
      ByteArrayInputStream byteArrayStream = new ByteArrayInputStream(blob);
      ObjectInputStream objectStream = null;
      try {
        objectStream = new ObjectInputStream(byteArrayStream);
        result = objectStream.readObject();
      } catch (Exception e) {
        throw new ModeleException(e);
      } finally {
        try {
          if (objectStream != null) {
            objectStream.close();
          }
        } catch (Exception e) {
        }
      }
    } else {
      result = new Object();
    }
    return result;
  }

  @SuppressWarnings("rawtypes")
  protected Map getMapObject(byte[] blob, boolean useProperties) {
    Map map = null;
    if (blob != null) {
      ByteArrayInputStream byteArrayStream = new ByteArrayInputStream(blob);
      ObjectInputStream objectStream = null;

      try {
        objectStream = new ObjectInputStream(byteArrayStream);
        if (useProperties) {
          Properties properties = new Properties();
          if (objectStream != null) {
            properties.load(objectStream);
          }
          map = new JobDataMap(properties);
        } else {
          map = (Map) objectStream.readObject();
        }
      } catch (Exception ex) {
        throw new ModeleException(ex);
      } finally {
        try {
          if (objectStream != null) {
            objectStream.close();
          }
        } catch (Exception e) {
        }
      }
    } else {
      map = new JobDataMap();
    }

    return map;
  }

  @SuppressWarnings("rawtypes")
  private Properties convertToProperty(Map data) {
    Properties properties = new Properties();

    for (Iterator entryIter = data.entrySet().iterator(); entryIter.hasNext();) {
      Map.Entry entry = (Map.Entry) entryIter.next();

      Object key = entry.getKey();
      Object val = entry.getValue() == null ? "" : entry.getValue();

      if (!(key instanceof String)) {
        key = "La cle doit etre une String";
      }
      if (!(val instanceof String)) {
        val = "La valeur doit etre une String";
      }

      properties.put(key, val);
    }
    return properties;
  }

  private ByteArrayOutputStream serializeProperties(JobDataMap data) {
    ByteArrayOutputStream result = new ByteArrayOutputStream();
    try {
      if (null != data) {
        Properties properties = convertToProperty(data.getWrappedMap());
        properties.store(result, "");
      }
    } catch (Exception e) {
      result = null;
    }

    return result;
  }

  protected ByteArrayOutputStream serializeObject(Object obj) {
    ByteArrayOutputStream result = new ByteArrayOutputStream();
    try {
      if (null != obj) {
        ObjectOutputStream out = new ObjectOutputStream(result);
        out.writeObject(obj);
        out.flush();
      }
    } catch (Exception e) {
      result = null;
    }
    return result;
  }

  protected ByteArrayOutputStream serializeJobData(JobDataMap data, Boolean useProperties) {

    ByteArrayOutputStream result = null;
    try {
      if (useProperties) {
        result = serializeProperties(data);
      } else {
        result = serializeObject(data);
      }
    } catch (Exception e) {
      result = null;
    }
    return result;
  }

  @SuppressWarnings("rawtypes")
  protected Object getKeyOfNonSerializableValue(Map data) {

    Object result = null;
    ByteArrayOutputStream baos = null;

    for (Iterator entryIter = data.entrySet().iterator(); entryIter.hasNext();) {
      Map.Entry entry = (Map.Entry) entryIter.next();

      baos = serializeObject(entry.getValue());
      if (baos == null) {
        result = entry.getKey();
        break;
      } else {
        try {
          baos.close();
        } catch (IOException ignore) {
        }
      }
    }
    return result;
  }
}
