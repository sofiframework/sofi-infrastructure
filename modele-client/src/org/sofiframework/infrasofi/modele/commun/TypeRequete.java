package org.sofiframework.infrasofi.modele.commun;

/**
 * Type de requête à appliquer sur un critère de recherche.
 * 
 * @author Jean-Maxime Pelletier
 */
public enum TypeRequete {
  greater,
  greaterOrEquals,
  lower,
  lowerOrEquals
}
