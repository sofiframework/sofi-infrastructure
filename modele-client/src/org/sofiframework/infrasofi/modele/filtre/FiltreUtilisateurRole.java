/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;
import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * Filtre de recherche permettant de rechercher une liste d'objets java du référentiel par rapport à une série de
 * critères.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 * @version 1.0
 */
public class FiltreUtilisateurRole extends ObjetFiltre  {

  private static final long serialVersionUID = 7969474957790476857L;

  /** L'identifiant unique de l'utilisateur **/
  private Long id;

  /** le nom de famille de l'utilisateur à rechercher */
  private String nom;

  /** le prénom de l'utilisateur à rechercher */
  private String prenom;

  /** l'adresse de courriel à rechercher */
  private String courriel;

  /** l'adresse de codeUtilisateur à rechercher */
  private String codeUtilisateur;

  /** l'identifiant unique de l'application pour lequel on fait une recherche */
  private String codeApplication;

  /** l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel */
  private String codeRole;

  /** le code du statut de l'utilisateur pour lequel on fait une recherche */
  private String codeStatut;

  /** Spécifie si on désire les objet qui n'inclus le role spécifié **/
  private Boolean codeRoleNonInclus;

  private String codeClient;

  /** Constructeur par défaut */
  public FiltreUtilisateurRole() {
    this.setRechercheAvantEtApresAttribut(true);
    ajouterExclureRechercheAvantApresValeur(new String[]{"codeUtilisateur", "codeRole", "codeAppication","codeStatut","codeClient"});
  }

  /**
   * Obtenir le nom de famille de l'utilisateur à rechercher.
   * <p>
   * @return le nom de famille de l'utilisateur à rechercher
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de famille de l'utilisateur à rechercher.
   * <p>
   * @param nom le nom de famille de l'utilisateur à rechercher
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le prénom de l'utilisateur à rechercher.
   * <p>
   * @return le prénom de l'utilisateur à rechercher
   */
  public String getPrenom() {
    return prenom;
  }

  /**
   * Fixer le prénom de l'utilisateur à rechercher.
   * <p>
   * @param prenom le prénom de l'utilisateur à rechercher
   */
  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  /**
   * Obtenir le courriel de l'utilisateur à rechercher.
   * <p>
   * @return le courriel de l'utilisateur à rechercher
   */
  public String getCourriel() {
    return courriel;
  }

  /**
   * Fixer le courriel de l'utilisateur à rechercher.
   * <p>
   * @param courriel le courriel de l'utilisateur à rechercher
   */
  public void setCourriel(String courriel) {
    this.courriel = courriel;
  }


  /**
   * Obtenir le codeStatut de l'utilisateur à rechercher.
   * <p>
   * @return le codeStatut de l'utilisateur à rechercher
   */
  public String getCodeStatut() {
    return codeStatut;
  }

  /**
   * Fixer le codeStatut de l'utilisateur à rechercher.
   * <p>
   * @param codeStatut le codeStatut de l'utilisateur à rechercher
   */
  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  /**
   * Obtenir l'identifiant unique de l'application pour lequel on fait une recherche.
   * <p>
   * @return l'identifiant unique de l'application pour lequel on fait une recherche
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique de l'application pour lequel on fait une recherche.
   * <p>
   * @param codeApplication l'identifiant unique de l'application pour lequel on fait une recherche
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir le codeUtilisateur de l'utilisateur à rechercher.
   * <p>
   * @return le codeUtilisateur de l'utilisateur à rechercher
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * Fixer le codeUtilisateur de l'utilisateur à rechercher.
   * <p>
   * @param codeUtilisateur le codeUtilisateur de l'utilisateur à rechercher
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * Obtenir l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel.
   * <p>
   * @return l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel
   */
  public String getCodeRole() {
    return codeRole;
  }

  /**
   * Fixer l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel.
   * <p>
   * @param codeRole l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel
   */
  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  /**
   * Obtenir spécifie si on désire les objet qui n'inclus le role spécifié.
   * <p>
   * @return codeRoleNonInclus spécifie si on désire les objet qui n'inclus le role spécifié
   */
  public void setCodeRoleNonInclus(Boolean codeRoleNonInclus) {
    this.codeRoleNonInclus = codeRoleNonInclus;
  }

  /**
   * Fixer spécifie si on désire les objet qui n'inclus le role spécifié.
   * <p>
   * @param spécifie si on désire les objet qui n'inclus le role spécifié
   */
  public Boolean isCodeRoleNonInclus() {
    return codeRoleNonInclus;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }
}