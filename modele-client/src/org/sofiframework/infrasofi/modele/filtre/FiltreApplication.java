/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;


/**
 * Filtre de recherche permettant de rechercher une liste d'applications par rapport à une série de
 * critères.
 * <p>
 * @author Maxime Ouellet, Nurun inc.
 * @author jfbrassard
 * @version 1.0
 * @since 3.0 Ajout des critères de recherches sur codeApplication et commun.
 */
public class FiltreApplication extends ObjetFiltre {

  private static final long serialVersionUID = -953406586182368864L;

  /** Le nom de l'application à rechercher */
  private String nom;

  /** Le statut de l'application à rechercher */
  private String statut;

  /** Le code d'application à rechercher */
  private String codeApplication;

  /** Faire une recheche sur les applications communes ou tous */
  private Boolean commun;

  /** Constructeur par défaut */
  public FiltreApplication() {
    this.setRechercheAvantEtApresAttribut(true);
  }

  /**
   * Obtenir le nom de l'application à rechercher.
   * <p>
   * @return le nom de l'application à rechercher
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de l'application à rechercher.
   * <p>
   * @param nom le nom de l'application à rechercher
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le statut de l'application à rechercher.
   * <p>
   * @return le statut de l'application à rechercher
   */
  public String getStatut() {
    return statut;
  }

  /**
   * Fixer le statut de l'application à rechercher.
   * <p>
   * @param le statut de l'application à rechercher
   */
  public void setStatut(String statut) {
    this.statut = statut;
  }

  /**
   * Obtenir le code d'application à rechercher.
   * @return le code d'application.
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer le code d'application à rechercher.
   * @param codeApplication le code d'application à rechercher.
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne true on désire extraire juste les applications communes.
   * @return true on désire extraire juste les applications communes.
   */
  public Boolean getCommun() {
    return commun;
  }

  /**
   * 
   * @param commun
   */
  public void setCommun(Boolean commun) {
    this.commun = commun;
  }
}
