/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;


/**
 * Objet de formulaire permettant la recherche d'utilisateurs en fonctions de leur rôles.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 * @version 1.0
 */
public class FiltreObjetReferentielRole extends ObjetFiltre {

  private static final long serialVersionUID = -5829946724415153889L;

  /** l'identifiant unique de l'application pour lequel on fait une recherche */
  private String codeApplication;

  /** l'identifiant unique de l'objet java à rechercher */
  private String nom;

  /**
   * Le statut d'activation, A (Actif) ou I (Inactif).
   */
  private String codeStatut;

  /** le type de composant du référentiel à rechercher */
  private String type;

  /** l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel */
  private String codeRole;

  /** Spécifie si on désire les objet qui n'inclus le role spécifié **/
  private Boolean codeRoleNonInclus;

  /** Constructeur par défaut */
  public FiltreObjetReferentielRole() {
    this.setRechercheAvantEtApresAttribut(true);
    this.ajouterAttributAExclure("codeRoleNonInclus");
  }

  /**
   * Obtenir l'identifiant unique de l'application pour lequel on fait une recherche.
   * <p>
   * @return l'identifiant unique de l'application pour lequel on fait une recherche
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique de l'application pour lequel on fait une recherche.
   * <p>
   * @param codeApplication l'identifiant unique de l'application pour lequel on fait une recherche
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir l'identifiant unique de l'objet java à rechercher.
   * <p>
   * @return l'identifiant unique de l'objet java à rechercher
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer l'identifiant unique de l'objet java à rechercher.
   * <p>
   * @param nom l'identifiant unique de l'objet java à rechercher
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le type de composant du référentiel à rechercher.
   * <p>
   * @return le type de composant du référentiel à rechercher
   */
  public String getType() {
    return type;
  }

  /**
   * Fixer le type de composant du référentiel à rechercher.
   * <p>
   * @param type le type de composant du référentiel à rechercher
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * Obtenir l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel.
   * <p>
   * @return l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel
   */
  public String getCodeRole() {
    return codeRole;
  }

  /**
   * Fixer l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel.
   * <p>
   * @param codeRole l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel
   */
  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  public void setCodeRoleNonInclus(Boolean codeRoleNonInclus) {
    this.codeRoleNonInclus = codeRoleNonInclus;
  }

  public Boolean getCodeRoleNonInclus() {
    return codeRoleNonInclus;
  }

  public String getCodeStatut() {
    return codeStatut;
  }

  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }
}
