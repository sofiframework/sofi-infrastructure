/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

public class FiltreUtilisateurApplication extends ObjetFiltre {

  private static final long serialVersionUID = -170610074188645752L;

  private String codeUtilisateur;
  private String codeApplication;

  public FiltreUtilisateurApplication(String codeUtilisateur) {
    this.codeUtilisateur=codeUtilisateur;
  }

  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }
  public String getCodeApplication() {
    return codeApplication;
  }

}
