/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;

import java.util.Date;

import org.sofiframework.composantweb.liste.ObjetFiltre;
import org.sofiframework.infrasofi.modele.commun.TypeRequete;

public class FiltreEnvoiCourriel extends ObjetFiltre {

  private static final long serialVersionUID = 3189345509409212672L;

  /**
   * Si le nombre de tentative est inférieur.
   */
  private Integer nombreTentative;

  /**
   * succes, echec, abandon. Les statuts seronts
   * appliqués comme un in.
   */
  private String[] statut;

  /**
   * Rechercher les envoi qui se sont
   * produits avant la date d'envoi.
   */
  private Date dateEnvoi;

  /**
   * Permet de spécifier le type de requête à effectuer 
   * pour le nombre de tentatives.
   */
  private TypeRequete typeRequeteNombreTentative = TypeRequete.greaterOrEquals;

  public FiltreEnvoiCourriel() {
    // Critère appliqué manuellement dans le dao.
    this.ajouterAttributAExclure("nombreTentative");
    this.ajouterAttributAExclure("typeRequeteNombreTentative");
    this.ajouterAttributAExclure("dateEnvoi");
  }

  public Integer getNombreTentative() {
    return nombreTentative;
  }

  public void setNombreTentative(Integer nombreTentative) {
    this.nombreTentative = nombreTentative;
  }

  public String[] getStatut() {
    return statut;
  }

  public void setStatut(String[] statut) {
    this.statut = statut;
  }

  public Date getDateEnvoi() {
    return dateEnvoi;
  }

  public void setDateEnvoi(Date dateEnvoi) {
    this.dateEnvoi = dateEnvoi;
  }

  public TypeRequete getTypeRequeteNombreTentative() {
    return typeRequeteNombreTentative;
  }

  public void setTypeRequeteNombreTentative(TypeRequete typeRequeteNombreTentative) {
    this.typeRequeteNombreTentative = typeRequeteNombreTentative;
  }
}
