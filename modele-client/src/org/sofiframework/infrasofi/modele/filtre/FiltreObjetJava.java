/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;


public class FiltreObjetJava extends ObjetFiltre {

  private static final long serialVersionUID = -7185704299838666127L;

  /**
   * La clé primaire généré par Oracle
   */
  private Long id;

  /**
   * Le code d'application
   */
  private String codeApplication;

  /**
   * L'identifiant
   */
  private String nom;

  /**
   * Le type d'objet référentiel
   * e.g. SE (Service), ON (Onglet), etc.
   */
  private String type;

  /**
   * Le statut d'activation, A (Actif) ou I (Inactif).
   */
  private String codeStatut;

  private Long idParent;

  /**
   * La clé de l'aide contextuelle.
   */
  private String cleAide;

  private String versionLivraison;

  /**
   * Le filtre de rechercher sélectionné.
   */
  private String filtreOperateur;

  public FiltreObjetJava() {
    this.setRechercheAvantEtApresAttribut(true);

    // Association du filtre avec la version de livraison.
    ajouterAssociation("filtreOperateur", "versionLivraison", "versionLivraison");

    this.ajouterExclureRechercheAvantApresValeur(new String[] { "codeApplication" });
  }

  /**
   * Fixe le code d'application
   * @param codeApplication le code d'application
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne le code d'application
   * @return le code d'application
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixe l'identifiant
   * @param nom l'identifiant
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Retourne l'identifiant
   * @return  l'identifiant
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixe le type d'objet référentiel
   * @param type le type d'objet référentiel
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * Retourne le type d'objet référentiel
   * @return  le type d'objet référentiel
   */
  public String getType() {
    return type;
  }

  public void setCleAide(String cleAide) {
    this.cleAide = cleAide;
  }

  public String getCleAide() {
    return cleAide;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodeStatut() {
    return codeStatut;
  }

  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  public Long getIdParent() {
    return idParent;
  }

  public void setIdParent(Long idParent) {
    this.idParent = idParent;
  }

  public void setVersionLivraison(String versionLivraison) {
    this.versionLivraison = versionLivraison;
  }

  public String getVersionLivraison() {
    return versionLivraison;
  }

  public void setFiltreOperateur(String filtreOperateur) {
    this.filtreOperateur = filtreOperateur;
  }

  public String getFiltreOperateur() {
    return filtreOperateur;
  }
}
