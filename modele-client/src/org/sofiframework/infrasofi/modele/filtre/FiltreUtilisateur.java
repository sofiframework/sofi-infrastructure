/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;
import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * Filtre de recherche permettant de rechercher une liste d'utilisateurs par rapport à une série de
 * critères.
 * <p>
 * @author Maxime Ouellet, Nurun inc.
 * @version 1.0
 */
public class FiltreUtilisateur extends ObjetFiltre  {

  private static final long serialVersionUID = -7192366528445206199L;
  /** L'identifiant unique de l'utilisateur **/
  private Long id;
  /**Le code d'authentifcation de l'utilisateur */
  private String codeUtilisateur;
  /** le nom de famille de l'utilisateur à rechercher */
  private String nom;
  /** le prénom de l'utilisateur à rechercher */
  private String prenom;
  /** le courriel de l'utilisateur */
  private String courriel;
  /** le code de statut de l'utilisateur dans l'application */
  private String codeStatut;


  /** Constructeur par défaut */
  public FiltreUtilisateur() {
    this.setRechercheAvantEtApresAttribut(true);
  }

  /**
   * Obtenir l'identifiant unique de l'utilisateur.
   * @return l'identifiant unique de l'utilisateur.
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * Fixer l'identifiant unique de l'utilisateur.
   * @param codeUtilisateur l'identifiant unique de l'utilisateur.
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * Obtenir le nom de famille de l'utilisateur à rechercher.
   * <p>
   * @return le nom de famille de l'utilisateur à rechercher
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de famille de l'utilisateur à rechercher.
   * <p>
   * @param nom le nom de famille de l'utilisateur à rechercher
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le prénom de l'utilisateur à rechercher.
   * <p>
   * @return le prénom de l'utilisateur à rechercher
   */
  public String getPrenom() {
    return prenom;
  }

  /**
   * Fixer le prénom de l'utilisateur à rechercher.
   * <p>
   * @param prenom le prénom de l'utilisateur à rechercher
   */
  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  /**
   * Obtenir le courriel de l'utilisateur.
   * @return le courriel de l'utilisateur.
   */
  public String getCourriel() {
    return courriel;
  }

  /**
   * Fixer le courriel de l'utilisateur.
   * @param courriel le courriel de l'utilisateur.
   */
  public void setCourriel(String courriel) {
    this.courriel = courriel;
  }

  /**
   * Obtenir le code de statut de l'utilisateur dans l'application.
   * @return le code de statut de l'utilisateur dans l'application.
   */
  public String getCodeStatut() {
    return codeStatut;
  }

  /**
   * Fixer le code de statut de l'utilisateur dans l'application.
   * @param codeStatut le code de statut de l'utilisateur dans l'application.
   */
  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }
}