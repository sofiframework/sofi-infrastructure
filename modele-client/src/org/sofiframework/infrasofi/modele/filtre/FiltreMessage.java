/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

public class FiltreMessage extends ObjetFiltre {

  private static final long serialVersionUID = -2964899899250626749L;

  /**
   * Le contenu du message
   */
  private String texteMessage;

  /**
   * Le code de langue
   */
  private String codeLangue;

  /**
   * La clé unique identifiant le message
   */
  private String cleMessage;

  /**
   * Le code d'application
   */
  private String codeApplication;

  /**
   * Le code de sévérité
   */
  private String codeSeverite;
  
  /**
   * No de référence
   */
  private String reference;

  /**
   * Le code client
   */
  private String codeClient;

  public FiltreMessage() {
    this.setRechercheAvantEtApresAttribut(true);
    this.ajouterExclureRechercheAvantApresValeur(new String[] { "codeApplication", "codeClient", "codeLangue"});
  }

  /**
   * Fixe le contenu du message
   * 
   * @param texte
   *          le contenu du message
   */
  public void setTexteMessage(String texteMessage) {
    this.texteMessage = texteMessage;
  }

  /**
   * Retourne le contenu du message
   * 
   * @return le contenu du message
   */
  public String getTexteMessage() {
    return texteMessage;
  }

  /**
   * Fixe le code de langue
   * 
   * @param codeLangue
   *          le code de langue
   */
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  /**
   * Retourne le code de langue
   * 
   * @return le code de langue
   */
  public String getCodeLangue() {
    return codeLangue;
  }

  /**
   * Fixe la clé du message
   * 
   * @param identifiant
   *          la clé du message
   */
  public void setCleMessage(String cleMessage) {
    this.cleMessage = cleMessage;
  }

  /**
   * Retourne la clé du message
   * 
   * @return la clé du message
   */
  public String getCleMessage() {
    return cleMessage;
  }

  /**
   * Fixe le code d'application
   * 
   * @param codeApplication
   *          le code d'application
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne le code d'application
   * 
   * @return le code d'application
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixe le code de sévérité
   * 
   * @param codeSeverite
   *          le code de sévérité
   */
  public void setCodeSeverite(String codeSeverite) {
    this.codeSeverite = codeSeverite;
  }

  /**
   * Retourne le code de sévérité
   * 
   * @return le code de sévérité
   */
  public String getCodeSeverite() {
    return codeSeverite;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

}