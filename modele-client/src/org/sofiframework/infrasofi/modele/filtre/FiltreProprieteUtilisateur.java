package org.sofiframework.infrasofi.modele.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

public class FiltreProprieteUtilisateur extends ObjetFiltre {

  private static final long serialVersionUID = 8998867847325231132L;

  private Long utilisateurId;

  public Long getUtilisateurId() {
    return utilisateurId;
  }

  public void setUtilisateurId(Long utilisateurId) {
    this.utilisateurId = utilisateurId;
  }



}
