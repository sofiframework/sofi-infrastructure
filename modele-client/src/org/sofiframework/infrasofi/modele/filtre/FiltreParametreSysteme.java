/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

public class FiltreParametreSysteme extends ObjetFiltre {

  private static final long serialVersionUID = -9216086683203889346L;

  private String codeApplication;

  private String codeParametre;

  private String codeClient;

  private java.util.Date dateDebutActivite;

  private java.util.Date dateFinActivite;

  private String codeType;

  private String valeur;

  private String description;

  private Boolean differentEnvironnement;

  private Boolean modificationADistance;

  public FiltreParametreSysteme() {
    this.setRechercheAvantEtApresAttribut(true);
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeParametre(String codeParametre) {
    this.codeParametre = codeParametre;
  }

  public String getCodeParametre() {
    return codeParametre;
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  public String getValeur() {
    return valeur;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  public void setDifferentEnvironnement(Boolean differentEnvironnement) {
    this.differentEnvironnement = differentEnvironnement;
  }

  public Boolean getDifferentEnvironnement() {
    return differentEnvironnement;
  }

  public void setModificationADistance(Boolean modificationADistance) {
    this.modificationADistance = modificationADistance;
  }

  public Boolean getModificationADistance() {
    return modificationADistance;
  }

  public void setCodeType(String codeType) {
    this.codeType = codeType;
  }

  public String getCodeType() {
    return codeType;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public java.util.Date getDateDebutActivite() {
    return dateDebutActivite;
  }

  public void setDateDebutActivite(java.util.Date dateDebutActivite) {
    this.dateDebutActivite = dateDebutActivite;
  }

  public java.util.Date getDateFinActivite() {
    return dateFinActivite;
  }

  public void setDateFinActivite(java.util.Date dateFinActivite) {
    this.dateFinActivite = dateFinActivite;
  }
}
