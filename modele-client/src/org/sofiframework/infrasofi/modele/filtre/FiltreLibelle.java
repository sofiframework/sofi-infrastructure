/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

public class FiltreLibelle extends ObjetFiltre {

  private static final long serialVersionUID = 6224815010217036979L;

  private String texteLibelle;
  private String codeLangue;
  private String cleLibelle;
  private String codeApplication;
  private String codeClient;
  private String aideContextuelle;
  private Boolean libelleHtml;
  private String reference;

  public FiltreLibelle() {
    this.ajouterExclureRechercheAvantApresValeur(new String[] {
        "codeApplication", "codeClient", "codeLangue" });
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCleLibelle(String cleLibelle) {
    this.cleLibelle = cleLibelle;
  }

  public String getCleLibelle() {
    return cleLibelle;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setTexteLibelle(String texteLibelle) {
    this.texteLibelle = texteLibelle;
  }

  public String getTexteLibelle() {
    return texteLibelle;
  }

  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  public String getAideContextuelle() {
    return aideContextuelle;
  }

  public void setLibelleHtml(Boolean libelleHtml) {
    this.libelleHtml = libelleHtml;
  }

  public Boolean getLibelleHtml() {
    return libelleHtml;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

}
