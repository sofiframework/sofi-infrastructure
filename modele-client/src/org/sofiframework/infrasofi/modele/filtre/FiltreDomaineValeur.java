/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;

import java.util.Date;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * Filtre de recherche permettant de rechercher une liste de domaines de valeur par rapport à une série de critères.
 * <p>
 * 
 * @author Maxime Ouellet, Nurun inc.
 * @version 1.0
 */
public class FiltreDomaineValeur extends ObjetFiltre {

  private static final long serialVersionUID = 3926560332609130141L;

  /** l'identifiant unique du code de l'application du domaine de valeur */
  private String codeApplication;

  /** le nom du domaine de valeur */
  private String nom;

  /** la valeur du domaine de valeur */
  private String valeur;

  private String codeLangue;

  private String nomParent;

  private String codeApplicationParent;

  private String valeurParent;

  private String codeLangueParent;

  private Date dateDebut;

  private Date dateFin;

  private Boolean actif;

  private String codeClient;

  /**
   * Constructeur par dï¿½faut
   */
  public FiltreDomaineValeur() {
    this.setRechercheAvantEtApresAttribut(true);

    this.ajouterExclureRechercheAvantApresValeur(new String[] { "codeApplication", "codeClient" });

  }

  /**
   * Obtenir l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @return l'identifiant unique du code de l'application du domaine de valeur.
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @param codeApplication
   *          l'identifiant unique du code de l'application du domaine de valeur.
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir le nom du domaine de valeur.
   * 
   * @return le nom du domaine de valeur.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du domaine de valeur.
   * 
   * @param nom
   *          le nom du domaine de valeur.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir la valeur du domaine de valeur.
   * 
   * @return la valeur du domaine de valeur.
   */
  public String getValeur() {
    return valeur;
  }

  /**
   * Fixer la valeur du domaine de valeur.
   * 
   * @param valeur
   *          la valeur du domaine de valeur.
   */
  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  public String getNomParent() {
    return nomParent;
  }

  public void setNomParent(String nomParent) {
    this.nomParent = nomParent;
  }

  public String getCodeApplicationParent() {
    return codeApplicationParent;
  }

  public void setCodeApplicationParent(String codeApplicationParent) {
    this.codeApplicationParent = codeApplicationParent;
  }

  public String getValeurParent() {
    return valeurParent;
  }

  public void setValeurParent(String valeurParent) {
    this.valeurParent = valeurParent;
  }

  public String getCodeLangueParent() {
    return codeLangueParent;
  }

  public void setCodeLangueParent(String codeLangueParent) {
    this.codeLangueParent = codeLangueParent;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public Date getDateDebut() {
    return dateDebut;
  }

  public void setDateDebut(Date dateDebut) {
    this.dateDebut = dateDebut;
  }

  public Date getDateFin() {
    return dateFin;
  }

  public void setDateFin(Date dateFin) {
    this.dateFin = dateFin;
  }

  public void setActif(Boolean actif) {
    this.actif = actif;
  }

  public Boolean getActif() {
    return actif;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }


}