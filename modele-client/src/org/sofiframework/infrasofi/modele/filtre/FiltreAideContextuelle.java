/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

public class FiltreAideContextuelle extends ObjetFiltre {

  private static final long serialVersionUID = 5989903258805355335L;

  private String cleAide;
  private String codeLangue;
  private String codeApplication;
  private String texteAide;
  private String titre;

  public FiltreAideContextuelle() {
    setRechercheAvantEtApresAttribut(true);
  }

  public void setCleAide(String cleAide) {
    this.cleAide = cleAide;
  }

  public String getCleAide() {
    return cleAide;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setTexteAide(String texteAide) {
    this.texteAide = texteAide;
  }

  public String getTexteAide() {
    return texteAide;
  }

  public String getTitre() {
    return titre;
  }

  public void setTitre(String titre) {
    this.titre = titre;
  }
}