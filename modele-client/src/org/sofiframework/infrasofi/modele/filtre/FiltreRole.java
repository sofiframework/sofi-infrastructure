/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;


/**
 * Objet d'affaire décrivant une filtre de recherche pour des rôles utilisateurs.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 * @version 1.0
 */
public class FiltreRole extends ObjetFiltre {

  private static final long serialVersionUID = 1551986050063973650L;

  private String codeRole;

  /** le nom du rôle à rechercher */
  private String nom;

  /** l'identifiant unique de l'application pour lequel on recherche les rôles */
  private String codeApplication;

  /** Constructeur par défaut */
  public FiltreRole() {
    this.setRechercheAvantEtApresAttribut(true);
  }

  /**
   * Obtenir le nom du rôle à rechercher.
   * <p>
   * @return le nom du rôle à rechercher
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du rôle à rechercher.
   * <p>
   * @param nom le nom du rôle à rechercher
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir l'identifiant unique de l'application pour lequel on recherche les rôles.
   * <p>
   * @return l'identifiant unique de l'application pour lequel on recherche les rôles
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique de l'application pour lequel on recherche les rôles.
   * <p>
   * @param codeApplication l'identifiant unique de l'application pour lequel on recherche les rôles
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeRole() {
    return codeRole;
  }

  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }
}
