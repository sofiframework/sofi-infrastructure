package org.sofiframework.infrasofi.modele.service;

import java.util.List;

import org.sofiframework.application.courriel.objetstransfert.Courriel;
import org.sofiframework.application.courriel.objetstransfert.EnvoiCourriel;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Smtp;
import org.sofiframework.infrasofi.modele.filtre.FiltreEnvoiCourriel;

public interface ServiceEnvoiCourriel {

  void envoyer(Courriel courriel, Smtp smtp);

  void effectuerTentatives();

  void nettoyerAnciensEnvoisCourriels();

  List<EnvoiCourriel> getListeEnvoiCourriel(FiltreEnvoiCourriel filtre);

  ListeNavigation getListeEnvoiCourriel(ListeNavigation ln);
}
