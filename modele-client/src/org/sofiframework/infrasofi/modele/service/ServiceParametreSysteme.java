/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.ParametreSysteme;
import org.sofiframework.modele.spring.service.DaoService;

/**
 * Service de gestion des paramètres système.
 * 
 * @auhor Jean-Maxime Pelletier
 */
public interface ServiceParametreSysteme extends DaoService {

  String getValeur(String codeApplication, String codeParametreSysteme);

  String getValeur(String codeApplication, String codeParametreSysteme, String codeClient);

  ParametreSysteme get(String coceApplication, String codeParametreSysteme);

  ParametreSysteme get(String coceApplication, String codeParametreSysteme, String codeClient);

  ParametreSysteme get(Long id);

  void supprimer(Long id);

  ListeNavigation getListeParametresPourTableauBord(ListeNavigation liste, Integer nombreJours, String codeApplication);

  ListeNavigation getListeParametrePilotage(ListeNavigation liste);

  void enregistrer(ParametreSysteme parametre, ParametreSysteme parametreOriginal);
}