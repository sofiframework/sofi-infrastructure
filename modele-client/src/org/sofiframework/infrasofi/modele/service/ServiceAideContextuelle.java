/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.AideContextuelle;
import org.sofiframework.infrasofi.modele.entite.AssociationAideContextuelle;
import org.sofiframework.modele.spring.service.DaoService;

public interface ServiceAideContextuelle extends DaoService {

  AideContextuelle getAide(String cleAide, String codeLangue);

  AideContextuelle getAide(Long seqObjetSecurisable, String codeLangue);

  void supprimerAide(String cleAide);

  ListeNavigation getListeAidePourTableauBord(ListeNavigation ln, Integer nombreJours, String codeApplication);

  ListeNavigation getListeAideContextuellePilotage(ListeNavigation liste);

  void enregistrer(AideContextuelle aide, String ancienneCle);

  List<AssociationAideContextuelle> getListeAssociation(String cleAide);

}