/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import java.io.Serializable;
import java.util.List;

import org.sofiframework.infrasofi.modele.entite.QuartzTrigger;
import org.sofiframework.modele.spring.service.DaoService;

public interface ServiceDeclencheur extends DaoService{

  Serializable ajouter(QuartzTrigger declencheur) ;

  void modifier(QuartzTrigger declencheur);

  QuartzTrigger getDeclencheur(Long declencheurId);

  void supprimerDeclencheur(QuartzTrigger declencheur);

  /**
   * Permet de récupérer la liste des déclencheurs d'un ordonnanceur identifié par les
   * valeurs passées en paramètre.
   * @param schedulerId
   * @return une liste d'objet de type {@link QuartzTrigger}
   */
  List<QuartzTrigger> getListeDeclencheur(Long schedulerId);

  /**
   * Permet de faire en sorte que le trigger soit schédulé par l'ordonnanceur
   * pour déclencher la job à laquelle il est associé
   * @param id l'identifiant du trigger à activer
   */
  void activerDeclencheur(Serializable id);

  /**
   * Permet de faire en sorte que le trigger ne soit plus schédulé par l'ordonnanceur
   * @param id l'identifiant du trigger à désactiver
   */
  void desactiverDeclencheur(Serializable id);
}
