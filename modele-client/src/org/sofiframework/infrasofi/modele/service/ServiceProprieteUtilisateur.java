package org.sofiframework.infrasofi.modele.service;

import java.util.List;
import java.util.Set;

import org.sofiframework.infrasofi.modele.entite.ProprieteUtilisateur;

public interface ServiceProprieteUtilisateur {

  List<ProprieteUtilisateur> getListeProprieteUtilisateur(Long utilisateurId);

  void sauvegarderProprieteUtilisateur(ProprieteUtilisateur proprieteUtilisateur);

  /**
   * Supprimer les propriétés qui ne sont pas incluses dans la liste spécifiée
   * (not in) pour un utilisateur.
   */
  void supprimerProprieteUtilisateur(Long utilisateurId, Set<Long> exclureIds);
}
