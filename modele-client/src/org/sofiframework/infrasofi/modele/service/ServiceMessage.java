/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Message;
import org.sofiframework.modele.spring.service.DaoService;

/**
 */
public interface ServiceMessage extends DaoService {

  /**
   * Retourne le message par clé primaire.
   */
  Message get(String identifiant, String codeLangue);

  /**
   * 
   * @param identifiant
   * @param codeLangue
   * @param codeClient
   * @return
   * @since 3.2
   */
  Message get(String identifiant, String codeLangue, String codeClient);

  /**
   * Supprimer le message qui correspond à la clé primaire.
   * 
   * @param identifiant
   * @since 3.2 : déclenche l'exception UnsupportedOperationException.
   * @deprecated depuis 3.2, la clé primaire d'un message a changé, il devient dangereux d'utiliser cette méthode.
   *             Utiliser {@link ServiceMessage#supprimerMessage(String, String)}.
   */
  @Deprecated
  void supprimerMessage(String identifiant);

  /**
   * Supprime les messages correspondant aux paramètres pour toutes les locales déclarées dans l'application.<br>
   * Le paramètre <code>codeClient</code> peut être null : dans ce cas, seul les messages ayant un codeClient à null
   * seront supprimés.
   * 
   * @param cleMessage
   * @param codeClient
   */
  void supprimerMessage(String cleMessage, String codeClient);

  /**
   * Obtenir une liste des nouveaux messages en fonction d'un nombre de jour pour une application.
   * 
   * @param liste
   *          Liste de navigation
   * @param nombreJours
   *          Nombre de jours
   * @param codeApplication
   *          Code de l'application
   * @return Liste de navigation
   */
  ListeNavigation getListeMessagesPourTableauBord(ListeNavigation liste, Integer nombreJours, String codeApplication);

  /**
   * Obtenir la liste des messages qui peuvent être pilotés par un pilote.
   * 
   * @param liste
   * @return
   */
  ListeNavigation getListeMessagePilotage(ListeNavigation liste);

  /**
   * 
   * @param message
   * @param ancienneCle
   */
  void enregistrer(Message message, String ancienneCle);
}