/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import org.sofiframework.application.securite.objetstransfert.ChangementMotPasse;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Utilisateur;
import org.sofiframework.infrasofi.modele.entite.UtilisateurApplication;

/**
 * Service de gestion des utilisateurs.
 * 
 * @author Jean-Maxime Pelletier
 * @author Jean-François Samson, Nurun inc.
 */
public interface ServiceUtilisateur {

  /**
   * Recherche une liste de navigation d'objets du référentiel à l'aide d'un
   * filtre de type <code>FiltreObjetReferentielRole</code>.
   * <p>
   * 
   * @param liste
   *          la liste de navigation possédant toutes les caractéristiques pour
   *          la recherche
   * @return la liste de navigation d'objets du référentiel qui répond à la
   *         recherche
   */
  ListeNavigation getListeUtilisateurPilotage(ListeNavigation liste);

  /**
   * Retourne un objet du référentiel spécifié l'aide d'un identifiant unique
   * (CODE_UTILISATEUR) de type <code>FiltreObjetReferentielRole</code>.
   * <p>
   * 
   * @param codeUtilisateur
   *          l<identifiant unique de l'utilisateur à obtenir
   * @return l'utilisateur correspondant à l<identifiant fourni
   */
  Utilisateur getUtilisateur(String codeUtilisateur);

  /**
   * Ajouter un objet de type <code>Utilisateur</code> dans la base de données.
   * <p>
   * 
   * @param utilisateur
   *          l'utilisateur à ajouter
   * @return l'utilisateur ajouté
   */
  void ajouterUtilisateur(Utilisateur utilisateur);

  /**
   * Sauvegarder un objet de type <code>Utilisateur</code> dans la base de
   * données.
   * <p>
   * 
   * @param utilisateur
   *          l'utilisateur à sauvegarder
   * @return l'utilisateur sauvegardé
   */
  void modifierUtilisateur(Utilisateur utilisateur);

  /**
   * Supprimer un objet de type <code>Utilisateur</code> dans la base de
   * données.
   * <p>
   * 
   * @param codeUtilisateur
   *          l'identifiant unique de l'utilisateur à supprimer
   * @throws org.sofiframework.sofi.modele.exception.ModeleException
   *           Erreur générique lors de l'appel du modèle
   */
  void supprimerUtilisateur(String codeUtilisateur);

  void supprimerUtilisateur(Long id);

  /**
   * Permet de modifier le mot de passe d'un utilisateur.
   * 
   * @param changementMotPasse
   *          information pour changer le mot de passe
   */
  void changerMotPasse(ChangementMotPasse changementMotPasse);

  /**
   * Obtenir un utilisateur actif
   * 
   * @param identifiant
   * @return
   */
  Utilisateur getUtilisateurActif(String identifiant);

  /**
   * Obtenir un utilisateur complété avec une entité utilisateur application. Si
   * l'entité utilisateur application n'existe pas
   * 
   * @param codeUtilisateur
   * @param codeApplication
   * @return
   */
  Utilisateur getUtilisateur(String codeUtilisateur, String codeApplication);

  /**
   * Permet de modifier le mot de passe expiré d'un utilisateur.
   * 
   * @param changementMotPasse
   *          information pour changer le mot de passe
   */
  void changerMotPasseExpirer(ChangementMotPasse changementMotPasse);

  /**
   * Force le changement du mot de passe de l'utilisateur lors de sa prochaine
   * authentification.
   * 
   * @param codeUtilisateur
   *          Code de l'utilisateur
   */
  void forcerChangementMotPasse(String codeUtilisateur);


  /**
   * Sauvegarder un utilisateur. Permet de sauvegarder l'entité
   * utilisateurApplication si il est présent.
   * 
   * @param utilisateur
   *          Utilisateur
   */
  void enregistrerUtilisateur(Utilisateur utilisateur);

  /**
   * Enregistrer une entité Utilisateur Application. Insert ou met à jour selon
   * que l'entité existe ou non.
   * 
   * @param utilisateurApplication
   */
  void enregistrerUtilisateurApplication(
      UtilisateurApplication utilisateurApplication);

  /**
   * Crée l'utilisateur. Donne un rôle de DEVELOPPEUR dans l'infrastructure.
   * Donne un rôle PUBLIC pour l'authentification. On doit donner le code
   * organisation dans l'entrée utilisateur application. Crée un lien avec code
   * organisation égal au code d'organisation donné lors de l'inscription.
   * 
   * @param utilisateur
   * @param codeOrganisation
   */
  void inscrire(Utilisateur utilisateur, String codeClient);

}