/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Role;

/**
 * Interface définissant les méthodes nécessaire au service de gestion des
 * rôles.
 * <p>
 * 
 * @author Pierre-Frédérick Duret, Nurun inc.
 * @version 1.0
 */
public interface ServiceRole {
  /**
   * Retourne une liste contenant les rôles disponible pour une application.
   * <p>
   * 
   * @param codeApplication
   *          l'identifiant unique de l'application pour lequel on recherche des
   *          rôles
   * @return la liste de tous les rôles disponible dans l'application
   * @throws org.sofiframework.modele.exception.ModeleException
   *           Erreur générique lors de l'appel du modèle
   */
  List<Role> getListeRole(String codeApplication);

  /**
   * Méthode qui sert a récupérer une liste de rôles selon un filtre de
   * recherche.
   * <p>
   * 
   * @param liste
   *          la liste de navigation à remplir avec les rôles
   * @param filtre
   *          le filtre permettant de cerner certains rôles
   * @return la liste de tous les objets <code>Role</code> qui répondent aux
   *         critères de recherche
   * @throws org.sofiframework.modele.exception.ModeleException
   *           Erreur générique lors de l'appel du modèle
   * @throws org.sofiframework.composantweb.liste.ListeNavigationException
   *           Erreur lors de l'assignation du filtre de recherche à la liste de
   *           navigation
   */
  ListeNavigation getListeRolePilotage(ListeNavigation liste);

  /**
   * Obtenir la liste des roles disponibles à un utilisateur pour une
   * application.
   * 
   * @param codeUtilisateur
   *          code de l'utilisateur
   * @param codeApplication
   *          code de l'application
   * @return liste
   */
  List<Role> getListeRoleDisponible(String codeUtilisateur, String codeApplication);

  /**
   * Obtenir la liste des rôles dont l'utilisateur à acces pour une application.
   * 
   * @param codeUtilisateur
   *          code utilisateur
   * @param codeApplication
   *          code d'application
   * @return Liste de roles
   */
  List<Role> getListeRolePilotage(String codeApplication, String codeStatut);

  /**
   * Méthode qui sert récupérer un rôle en fonction de son identifiant unique.
   * <p>
   * 
   * @param codeRole
   *          l'identifiant unique du rôle à récupérer
   * @param codeApplication
   *          l'identifiant unique de l'application auquel est associé le rôle
   * @return l'objets <code>Role</code> tel qu'il est dans la base de données
   * @throws org.sofiframework.modele.exception.ModeleException
   *           Erreur générique lors de l'appel du modèle
   */
  Role getRole(String codeRole, String codeApplication);

  /**
   * Méthode qui sert à ajouter ou modifier un rôle dans la base de données.
   * <p>
   * 
   * @param role
   *          l'objet <code>Role</code> à ajouter dans la base de données
   */
  void enregistrerRole(Role role);

  /**
   * Méthode qui sert à supprimer un rôle déjà existant de la base de données.
   * <p>
   * 
   * @param codeRole
   *          l'identifiant unique du rôle à supprimer
   * @param codeApplication
   *          l'identifiant unique de l'application auquel est associé le rôle à
   *          supprimer
   * @throws org.sofiframework.modele.exception.ModeleException
   *           Erreur générique lors de l'appel du modèle
   */
  void supprimerRole(String codeRole, String codeApplication);

}
