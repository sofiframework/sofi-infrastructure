/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import java.io.Serializable;
import java.util.List;

import org.sofiframework.infrasofi.modele.entite.QuartzJob;
import org.sofiframework.modele.spring.service.DaoService;

public interface ServiceJob extends DaoService{

  /**
   * 
   * @param nouvJob
   * @param ancJob
   * @return l'identifiant du job créé ou modifié
   */
  Long enregistrer(QuartzJob nouvJob,QuartzJob ancJob);

  /**
   * Permet de récupérer la liste des jobs de l'ordonnanceur correspondant à
   * l'ordonnanceur spécifié en paramètre
   * @param schedulerId un identifiant d'ordonnanceur
   * @return une liste d'objet de type {@link QuartzJob}
   */
  List<QuartzJob> getListeJob(Long schedulerId);

  /**
   * Permet d'activer tous les déclencheurs associés à la job.
   * Voir {@link ServiceDeclencheur#activerDeclencheur(Serializable)}.
   * @param id l'identifiant de la job
   */
  void activerJob(Serializable id);

  /**
   * Permet de désactiver tous les déclencheurs de la job.
   * Voir {@link ServiceDeclencheur#desactiverDeclencheur(Serializable)}.
   * @param id l'identifiant de la jobà désactiver
   */
  void desactiverJob(Serializable id);

}
