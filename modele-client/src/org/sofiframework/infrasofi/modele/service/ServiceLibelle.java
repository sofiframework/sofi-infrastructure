/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Libelle;
import org.sofiframework.modele.spring.service.DaoService;

/**
 * Service de gestion des libellés.
 */
public interface ServiceLibelle extends DaoService {

  Libelle get(String cleLibelle, String codeLangue);

  /**
   * 
   * @param cleLibelle
   * @param codeLangue
   * @param codeClient
   * @return
   * @since 3.2
   */
  Libelle get(String cleLibelle, String codeLangue, String codeClient);

  /**
   * 
   * @param cleLibelle
   * @since 3.2 : déclenche l'exception UnsupportedOperationException.
   * @deprecated depuis 3.2, la clé primaire d'un libellé a changé, il devient dangereux d'utiliser cette méthode.
   *             Utiliser {@link ServiceMessage#supprimerLibelle(String, String)}.
   */
  @Deprecated
  void supprimerLibelle(String cleLibelle);

  /**
   * Supprime les libellés correspondant aux paramètres pour toutes les locales déclarées dans l'application.<br>
   * Le paramètre <code>codeClient</code> peut être null : dans ce cas, seul les messages ayant un codeClient à null
   * seront supprimés.
   * 
   * @param cleLibelle
   * @param codeClient
   */
  void supprimerLibelle(String cleLibelle, String codeClient);

  ListeNavigation getListeLibellesPourTableauBord(ListeNavigation liste, Integer nombreJours, String codeApplication);

  ListeNavigation getListeLibellePilotage(ListeNavigation liste);

  void enregistrer(Libelle libelle, String ancienneCle);

  boolean existe(String cle, String codeApplication);

  void modifierCleLibelle(String cleOriginale, Libelle libelle);
}