/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.modele.spring.service.DaoService;

public interface ServiceApplication extends DaoService {

  /**
   * Retourne la liste des applications, excluant les enfants.
   */
  List<Application> getListeApplication();

  /**
   * Retourne une liste contenant les application qui peuvent être consultées par un utilisateur.
   * <p>
   * @param codeUtilisateur l'identifiant unique de l'utilisateur pour lequel on recherche les application
   * @return la liste de toutes les application que l'utilisateur peut consulter
   */
  List<Application> getListeApplicationAccessible();

  /**
   * Retourne une liste contenant les application qui peuvent être pilotées par un utilisateur.
   * <p>
   * @param codeUtilisateur l'identifiant unique de l'utilisateur pour lequel on recherche les application
   * @return la liste de toutes les application que l'utilisateur peut consulter
   */
  List<Application> getListeApplicationAccessiblePilotage();

  /**
   * Recherche une liste de navigation d'applications à l'aide d'un filtre
   * de type <code>FiltreApplication</code>.
   * <p>
   * @param liste la liste de navigation possédant toutes les caractéristiques pour la recherche
   * @return la liste de navigation d'applications qui répond à la recherche
   */
  ListeNavigation getListeApplicationAccessible(ListeNavigation liste);

  /**
   * Recherche une liste de navigation d'applications à l'aide d'un filtre
   * de type <code>FiltreApplication</code>.
   * <p>
   * @param liste la liste de navigation possédant toutes les caractéristiques pour la recherche
   * @return la liste de navigation d'applications qui répond à la recherche
   */
  ListeNavigation getListeApplicationAccessiblePilotage(ListeNavigation liste);


  /**
   * Retourne une application avec la liste des objets sécurisables
   * <p>
   * @param codeApplication le code application a extraire.
   * @return une application avec ces objets sécurisables
   */
  Application getApplication(String codeApplication);

  /**
   * Retourne une application avec la liste des objets sécurisables cloisonnné par facette si demandé.
   * @param codeApplication le code application a extraire.
   * @param codeFacette le code de facette spécifique.
   * @return une application avec ces objets sécurisables cloisonné par facette si désiré.
   */
  Application getApplication(String codeApplication, String codeFacette);

  /**
   * 
   * @param codeApplication
   */
  void supprimerApplication(String codeApplication);

  /**
   * Enregistre une application.
   * 
   * @param application Application a suavegarder
   * @param applicationOriginale Application dans son état original
   */
  void enregistrer(Application application, Application applicationOriginale);
}
