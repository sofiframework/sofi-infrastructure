/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.modele.spring.service.DaoService;

/**
 * Service de gestion de la journalisation
 * 
 * @author : Jean-Maxime Pelletier
 */
public interface ServiceJournalisation extends DaoService {

  /**
   * Liste des entrées de journaux les plus récentes.
   * 
   * @param liste
   * @param codeApplication
   * @return
   */
  ListeNavigation getListeJournalisationPourTableauBord(ListeNavigation liste,
      String codeApplication);

  /**
   * Liste des journaux auquel un utilisateur est authorisé en fonction de sa
   * sécurité. Il peut voir les journaux des objets java associé à ses rôles de
   * premier niveau.
   * 
   * @param liste
   * @return
   */
  ListeNavigation getListeJournalisationPilotage(ListeNavigation liste);
}