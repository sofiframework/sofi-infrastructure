/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import java.util.List;

import org.sofiframework.application.domainevaleur.filtre.FiltreDomaineValeur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.DefinitionDomaineValeur;
import org.sofiframework.infrasofi.modele.entite.DomaineValeur;

/**
 * @author : Maxime Ouellet, Nurun inc.
 */
public interface ServiceDomaineValeur extends org.sofiframework.modele.spring.service.DaoService {

  /**
   * Obtenir la liste paginé des domaine ou définition de domaines qui sont
   * permises à un utilisateur pour le pilotage.
   * 
   * @param liste
   * @param definition
   *          Si on désire obtenir des définitions de domaines ou des valeurs de
   *          domaines.
   * @return
   */
  ListeNavigation getListeDomaineValeurPilotage(ListeNavigation liste,
      Boolean definition);

  /**
   * Méthode qui permet de récupérer les domaines actifs d'un domaine de valeur.
   * <p>
   * Cette méthode ajoute une valeur sélectionnée dans les cas où il faudrait en
   * ajouter une à la liste de résultat (une valeur déjà sélectionnée).
   * 
   * @param liste
   *          la liste de navigation possédant tous les critères de recherche.
   * @param valeurSelectionnee
   *          le code identifiant la valeur sélectionnée. Le reste de la PK est
   *          spécifié par l'objet Filtre.
   * @return le résultat de la recherche.
   */
  ListeNavigation getListeActif(ListeNavigation liste, String valeurSelectionnee);

  /**
   * Recherche une liste de navigation d'objets du référentiel à l'aide d'un
   * filtre de type <code>FiltreDomaineValeur</code>.
   * <p>
   * 
   * @param liste
   *          la liste de navigation possédant toutes les caractéristiques pour
   *          la recherche
   * @return la liste de navigation de domaines de valeur qui répond à la
   *         recherche
   */
  public ListeNavigation getListeDomaineValeur(ListeNavigation liste);

  /**
   * Recherche une liste de navigation d'objets du référentiel à l'aide d'un
   * filtre de type <code>FiltreDomaineValeur</code>.
   * <p>
   * 
   * @param liste
   *          la liste de navigation possédant toutes les caractéristiques pour
   *          la recherche
   * @return la liste de navigation de définition domaines de valeur qui répond
   *         à la recherche
   */
  public ListeNavigation getListeDefinitionDomaineValeur(ListeNavigation liste);

  /**
   * Méthode qui sert récupérer un définition de domaine de valeur en fonction
   * de ses identifiants.
   * <p>
   * 
   * @param codeApplication
   *          l'identifiant de l'application à laquelle appartient la définition
   * @param definition
   *          le nom de la définition du domaine de valeur
   * @return l'objets <code>DefinitionDomaineValeur</code> tel qu'il est dans la
   *         base de données
   */
  public DefinitionDomaineValeur getDefinitionDomaineValeur(String codeApplication, String nom, String codeLangue);

  /**
   * Ajouter un objet de type <code>DefinitionDomaineValeur</code> dans la base
   * de données.
   * <p>
   * 
   * @param definition
   *          la définition du domaine de valeur à ajouter
   * @return la définition du domaine de valeur ajouté
   */
  public void ajouterDefinitionDomaineValeur(DefinitionDomaineValeur definition);

  /**
   * Sauvegarder un objet de type <code>DefinitionDomaineValeur</code> dans la
   * base de données.
   * <p>
   * 
   * @param definition
   *          la définition du domaine de valeur à sauvegarder
   * @return la définition du domaine de valeur sauvegardé
   */
  public void enregistrerDefinitionDomaineValeur(DefinitionDomaineValeur definition, DefinitionDomaineValeur definitionOriginale);

  public void enregistrerDefinitionDomaineValeur(DefinitionDomaineValeur definition);

  /**
   * Supprimer un objet de type <code>DefinitionDomaineValeur</code> dans la
   * base de données.
   * <p>
   * 
   * @param definition
   *          la definition de domaine de valeur a supprimer
   */
  public void supprimerDefinitionDomaineValeur(DefinitionDomaineValeur definition);

  /**
   * Méthode qui sert récupérer un domaine de valeur en fonction de ses
   * identifiants.
   * <p>
   * 
   * @param codeApplication
   *          l'identifiant de l'application à laquelle appartient le domaine
   * @param nom
   *          le nom du domaine de valeur
   * @param valeur
   *          la valeur du domaine de valeur
   * @return l'objet <code>DomaineValeur</code> tel qu'il est dans la base de
   *         données
   */
  public DomaineValeur getDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue);

  /**
   * Méthode qui sert à ajouter un nouveau domaine de valeur dans la base de
   * données.
   * <p>
   * 
   * @param definition
   *          l'objet <code>DomaineValeur</code> à ajouter dans la base de
   *          données
   * @return l'objet <code>DomaineValeur</code> tel qu'il est dans la base de
   *         données
   */
  public void ajouterDomaineValeur(DomaineValeur domaine);

  /**
   * Méthode qui sert à sauvegarder un domaine de valeur déjà existant dans la
   * base de données.
   * <p>
   */
  public void enregistrerDomaineValeur(DomaineValeur domaine, DomaineValeur domaineOriginal);

  /**
   * Sauvegarder un domaine de valeur.
   * <p>
   */
  public void enregistrerDomaineValeur(DomaineValeur domaineValeur);

  /**
   * Méthode qui sert à supprimer un domaine de valeur déjà existant de la base
   * de données.
   * <p>
   * 
   * @param domaine
   *          le domaine de valeur à supprimer
   */
  public void supprimerDomaineValeur(DomaineValeur domaine);

  /**
   * Obtenir la liste des domaines de valeur enfants d'un domaine parent déjà
   * existant dans la base de données.
   * <p>
   * 
   * @param codeApplicationParent
   *          l'identifiant de l'application du domaine de valeur parent
   * @param nomParent
   *          le nom du domaine de valeur parent
   * @param valeurParent
   *          la valeur du domaine de valeur parent
   * @return la liste des domaines de valeur enfants
   */

  public List<DomaineValeur> getListeDomaineValeur(String codeApplicationParent, String nomParent, String valeurParent, String codeLangueParent);

  /**
   * Obtenir la liste des domaines de valeur enfants d'un domaine parent déjà
   * existant dans la base de données.
   * <p>
   * 
   * @param codeApplicationParent
   *          l'identifiant de l'application du domaine de valeur parent
   * @param nomParent
   *          le nom du domaine de valeur parent
   * @param valeurParent
   *          la valeur du domaine de valeur parent
   * @param valeurInactive
   *          Permet de spécifier si l'on veut obtenir les valeurs actives et
   *          inactives.
   * @return la liste des domaines de valeur enfants
   */
  public List<DomaineValeur> getListeDomaineValeur(String codeApplicationParent, String nomParent, String valeurParent, String codeLangueParent, boolean valeurInactive);

  /**
   * Méthode qui sert à obtenir la liste des domaines de valeur enfants d'une
   * définition de domaine déjà existant dans la base de données.
   * <p>
   * 
   * @param codeApplicationParent
   *          l'identifiant de l'application du domaine de valeur parent
   * @param nomParent
   *          le nom du domaine de valeur parent
   * @param codeLangueParent
   *          Le code de langue parent.*
   * @return la liste des domaines de valeur enfants
   */
  public List<DomaineValeur> getListeDomaineValeurPourDefinition(String codeApplicationParent, String nomParent, String codeLangueParent, boolean valeurInactive);


  /**
   * Méthode qui sert à obtenir la liste des domaines de valeur enfants d'un
   * domaine parent déjà existant dans la base de données.
   * <p>
   * 
   * @param codeApplicationParent
   *          l'identifiant de l'application du domaine de valeur parent
   * @param nomParent
   *          le nom du domaine de valeur parent
   * @param valeurParent
   *          la valeur du domaine de valeur parent
   */
  public List<DomaineValeur> getListeDomaineValeur(String codeApplicationParent, String nomParent, String valeurParent);

  /**
   * Obtenir la liste des domaines de valeurs et des branches de domaines de
   * valeur enfant qui sont dans la liste de valeur chargement. Fait un
   * chargement partiel de l'arbre des valeurs de domaine.
   */
  public List<DomaineValeur> getListeDomaineValeur(String codeApplicationParent, String nomParent, String valeurParent, String listeValeurChargement, String codeLangueParent);

  /**
   * Activer une valeur de domaine.
   * 
   * @param codeApplication
   *          Code de l'application
   * @param nom
   *          Nom du domaine
   * @param valeur
   *          Valeur
   * @param codeLangue
   *          Code de langue de la valeur de domaine
   */
  public void activerDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue);

  /**
   * Désaciver une valeur de domaine.
   * 
   * @param codeApplication
   *          Code d'application
   * @param nom
   *          Nom du domaine
   * @param valeur
   *          Valeur
   * @param codeLangue
   *          Code de langue
   */
  public void desactiverDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue);

  /**
   * Obtenir la liste des définitions de domaine de valeur qui répondent aux
   * critères de recherche inclus dans le filtre.
   * 
   * @param filtre
   *          Filtre de critères de recherche
   * @return Liste de définitnion de domaine de valeur.
   */
  public List<DomaineValeur> getListeDefinitionDomaineValeur(FiltreDomaineValeur filtre);

  /**
   * Obtenir une liste de valeurs de domaine qui répondent aux critères de
   * recherche du filtre.
   * 
   * @param filtre
   *          Filtre de recherche
   * @return Listes de valeurs de domaine
   */
  public List<DomaineValeur> getListeDomaineValeur(FiltreDomaineValeur filtre);

  /**
   * Obtenir la liste des valeurs de domaine pour une liste de navigation.s
   * 
   * @param liste
   *          Liste de navigation
   * @return Liste de navigation
   */
  public ListeNavigation getListeDomaineValeurEnfant(ListeNavigation liste);
}
