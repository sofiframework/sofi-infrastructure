/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.service;

import java.io.Serializable;
import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.modele.entite.PageApplication;
import org.sofiframework.infrasofi.modele.entite.RoleObjetJava;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.exception.ObjetTransfertNotFoundException;

/**
 * Service de gestion du référentiel.
 * 
 * @author : Guillaume Poirier, Nurun inc.
 */
public interface ServiceReferentiel {

  ObjetJava getObjetJava(String nom);

  /**
   * Obtenir la liste paginée des objets java accessibles au pilotage pour un
   * utilisateur.
   * 
   * @param liste
   * @return
   */
  ListeNavigation getListeObjetJavaPilotage(ListeNavigation liste);

  /**
   * Enregistrer un objet java. Compare avec l'objet java original. Permet de
   * vérifier si l'objet java a changé de clé si c'est le cas on doit supprimer
   * l'ancien libellé en insérer un nouveau.
   * 
   * @param objet
   * @param objetOriginal
   * @param texteLibelle
   * @param isNouveauFormulaire
   */
  void enregistrerObjetJava(ObjetJava objet, ObjetJava objetOriginal,
      String texteLibelle);

  /**
   * Enregristre un objet Java.
   * 
   * @param objet
   */
  void enregistrerObjetJava(ObjetJava objet);

  /**
   * Retourne l'Objet du référentiel correspondant au ID spécifié, incluant le
   * parent directe, sans enfant.
   * 
   * @param id
   *          La séquence de l'objet référentiel
   * @throws ObjetTransfertNotFoundException
   *           Si l'objet référentiel n'existe pas
   * @throws ModeleException
   *           Si une erreur BD se produit lors de l'accès au données
   */
  ObjetJava getObjetJava(Long id);

  /**
   * Execute la recherche selon les paramètres de la liste de navigation, et
   * retourne les resultats dans la propriété "liste" de l'objet
   * <code>ListeNaviagation</code>
   * 
   * @param liste
   *          L'objet ListeNavigation contient les paramètres de la recherche
   *          ainsi qu'un champ liste dans lequel les results doivent être
   *          insérés.
   * @return Une référence sur la liste passé en paramètre, modifier avec la
   *         liste contenant les résultats de la recherche.
   */
  ListeNavigation getListeObjetJava(ListeNavigation liste);

  /**
   * 
   * @param idObjetJavaParent
   * @return
   */
  List<ObjetJava> getListeObjetJava(Long idObjetJavaParent);

  /**
   * Supprime de la BD l'objet référentiel correspondant à l'ID passé en
   * paramètre.
   * 
   * @param id
   *          La séquence de l'objet référentiel à supprimer.
   */
  void supprimerObjetJava(Serializable id);

  /**
   * Retourne la liste des objet de type service pour une application
   * 
   * @return la liste des objet de type service
   */
  List<ObjetJava> getListeServiceParApplication(String codeApplication);

  /**
   * Retourne la liste des objet de type service pour un texte d'aide
   * 
   * @return la liste des objet de type service
   */
  List<ObjetJava> getListeServiceParCleAide(String cleAide);

  /**
   * Retourne la liste des objet de type onglets pour un service
   * 
   * @return la liste des objet de type onglets pour un service
   */
  List<ObjetJava> getListeOnglet(Long idService);

  /**
   * Méthode qui sert à enregistrer une page d'application en particulier
   * <p>
   * Cette méthode sert autant à l'ajout de nouvelles pages qu'a la sauvegarde
   * des pages déjà existantes
   * <p>
   * Cette méthode ne se sert pas de la méthode
   * <code>sauvegarderListe(...)</code> car cette dernière nécéssite un
   * identifiant unique pour la table à laquelle on ajoute ou modifie des
   * données.
   * <p>
   * 
   * @param pageApplication
   *          la page à sauvegarder dans la base de données
   * @throws org.sofiframework.modele.exception.ModeleException
   *           Erreur générique lors de l'appel du modèle
   */
  void enregistrerPageApplication(PageApplication pageApplication);

  /**
   * Méthode qui sert à sauvegarder une liste de <code>RoleObjetJava</code> pour
   * tous les enfants d'un objet java en particulier.
   * <p>
   * Cette méthode se sert de la méthode
   * <code>sauvegarderListeRoleObjetJavaPourUnObjetJava(...)</code> afin de
   * sauvegarder seulement la différence de la liste.
   * <p>
   * Cette méthode est récursive et s'appelle jusqu'à ce que tous les enfants de
   * l'objet java passé en paramètres possèdent la même sécurité que la liste
   * passée en paramètre.
   * <p>
   * 
   * @param idObjetJavaSource
   *          l'identifiant unique de l'objet java pour qui on reporte la
   *          sécurité vers ses composants enfants
   * @param listeRoleObjetJava
   *          la liste qui possède tous les <code>RoleObjetJava</code> d'un
   *          objet du référentiel
   * @param codeApplication
   *          l'identifiant unique de l'application pour lequel on ajoute des
   *          rôles
   * @throws org.sofiframework.modele.exception.ModeleException
   *           Erreur générique lors de l'appel du modèle
   */
  void enregistrerListeRoleObjetJavaPourEnfants(Long idObjetJava,
      List<RoleObjetJava> listeRoleObjetJava, String codeApplication);

  /**
   * Retourne la liste des objets java pour la page de sécurité du référentiel.
   * 
   * @param filtre
   * @return
   */
  public ListeNavigation getListeObjetJavaPourSecurite(ListeNavigation liste);

  /**
   * 
   * @param idService
   * @param type
   * @return
   */
  List<ObjetJava> getListeObjetJavaParParent(Long idService, String type);

  /**
   * Permet d'enregistrer une clé d'aide pour un objet java
   * 
   * @param idObjetJava
   * @param cleAide
   */
  void enregistrerCleAide(Long idObjetJava, String cleAide);

  /**
   * Permet de supprimer une association avec une clé d'aide
   * 
   * @param idObjetJava
   */
  void supprimerAssociationAide(Long idObjetJava);

  /**
   * Permet d'enregistrer une clé d'aide pour un objet java, et de supprimer
   * l'association de cette clé avec un autre objet java
   * 
   * @param idObjetJava
   * @param cleAide
   */
  void enregistrerCleAide(Long idObjetJava, Long idAncienObjetJava,
      String cleAide);

  /**
   * Enregistre une liste de role objet java pour un objet java
   * 
   * @param listeRoleObjetJava
   * @param codeApplication
   */
  public void enregistrerListeRoleObjetJavaPourUnObjetJava(
      List<RoleObjetJava> listeRoleObjetJava, Long idObjetJava,
      String codeApplication);
}