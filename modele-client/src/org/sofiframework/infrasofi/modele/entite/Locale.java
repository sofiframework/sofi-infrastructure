package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.modele.entite.EntiteTrace;


public class Locale extends EntiteTrace {

  /**
   * 
   */
  private static final long serialVersionUID = 7136846221559853776L;

  private String codeLocale;

  private String codeLangue;

  private String codePays;

  private String cleLibelle;

  public Locale() {
    super();
    this.setCle(new String[] { "codeLocale"});
  }

  /**
   * @param codeLocale
   *          the codeLocale to set
   */
  public void setCodeLocale(String codeLocale) {
    this.codeLocale = codeLocale;
  }

  /**
   * @return the codeLocale
   */
  public String getCodeLocale() {
    return codeLocale;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodePays(String codePays) {
    this.codePays = codePays;
  }

  public String getCodePays() {
    return codePays;
  }

  public void setCleLibelle(String cleLibelle) {
    this.cleLibelle = cleLibelle;
  }

  public String getCleLibelle() {
    return cleLibelle;
  }



}
