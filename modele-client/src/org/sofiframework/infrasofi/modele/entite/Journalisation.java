/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.application.journalisation.objetstransfert.Journal;

/**
 * Représente une entrée de journalisation.
 * 
 * @author Jean-Maxime Pelletier
 */
public class Journalisation extends Journal {

  private static final long serialVersionUID = -6540063204834382266L;

  public Journalisation() {
    this.setCle(new String[]{"idJournalisation"});
  }

  public String getNom() {
    return getUtilisateur().getNom();
  }

  public String getPrenom() {
    return getUtilisateur().getPrenom();
  }

  public String getCourriel() {
    return getUtilisateur().getCourriel();
  }

  public String getCodeUtilisateurApplicatif() {
    return getUtilisateur().getCodeUtilisateurApplicatif();
  }

  public String getCodeStatutUtilisateur() {
    return getUtilisateur().getCodeStatut();
  }
}
