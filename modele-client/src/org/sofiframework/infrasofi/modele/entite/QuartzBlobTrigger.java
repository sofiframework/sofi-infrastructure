/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.quartz.Trigger;


public class QuartzBlobTrigger extends QuartzTypeTrigger {

  private static final long serialVersionUID = 7142141224366014251L;

  private byte[] triggerData;

  public QuartzBlobTrigger(Long schedulerId, Trigger trigger) {
    super(schedulerId);
    this.triggerData = serializeObject(trigger).toByteArray();
  }

  public byte[] getTriggerData() {
    return triggerData;
  }

  public void setTriggerData(byte[] triggerData) {
    this.triggerData = triggerData;
  }

  @Override
  public Object getQuartzObject(Boolean useProperties){
    return this.getObjectFromBlob(triggerData);
  }

}
