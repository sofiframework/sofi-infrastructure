/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.impl.jdbcjobstore.Constants;
import org.quartz.impl.jdbcjobstore.FiredTriggerRecord;
import org.quartz.utils.Key;
import org.sofiframework.application.tacheplanifiee.FiredTriggerRecordImpl;
import org.sofiframework.infrasofi.modele.commun.QuartzEntite;

public class QuartzFiredTrigger extends QuartzEntite{

  private static final long serialVersionUID = 1662713447097742780L;

  private String entreeId;
  private Long declencheurId;

  private Boolean triggerVolatile;

  private String nomScheduler;
  private Long debutTraitement,priorite;

  private String etat;

  private Boolean stateful;
  private Boolean relance;

  private QuartzTrigger trigger;

  public QuartzFiredTrigger(){
  }

  public QuartzFiredTrigger(Long declencheurId, String entryId){
    this.declencheurId = declencheurId;
    this.entreeId = entryId;
  }

  public QuartzFiredTrigger(Long declencheurId, String instanceId, Trigger trigger, String state, JobDetail job){
    this(declencheurId, trigger.getFireInstanceId());

    this.triggerVolatile = trigger.isVolatile();
    this.nomScheduler = instanceId;
    this.debutTraitement = trigger.getNextFireTime().getTime();
    this.priorite = new Long(trigger.getPriority());
    this.etat = state;

    if (job != null) {
      this.stateful = job.isStateful();
      this.relance = job.requestsRecovery();
    }else{
      this.stateful = null;
      this.relance = null;
    }
  }

  public String getEntreeId() {
    return entreeId;
  }
  public void setEntreeId(String entreeId) {
    this.entreeId = entreeId;
  }

  public Boolean getTriggerVolatile() {
    return triggerVolatile;
  }
  public void setTriggerVolatile(Boolean triggerVolatile) {
    this.triggerVolatile = triggerVolatile;
  }
  public String getNomScheduler() {
    return nomScheduler;
  }
  public void setNomScheduler(String nomScheduler) {
    this.nomScheduler = nomScheduler;
  }
  public Long getDebutTraitement() {
    return debutTraitement;
  }
  public void setDebutTraitement(Long debutTraitement) {
    this.debutTraitement = debutTraitement;
  }
  public Long getPriorite() {
    return priorite;
  }
  public void setPriorite(Long priorite) {
    this.priorite = priorite;
  }
  public String getEtat() {
    return etat;
  }
  public void setEtat(String etat) {
    this.etat = etat;
  }

  public Boolean getStateful() {
    return stateful;
  }
  public void setStateful(Boolean stateful) {
    this.stateful = stateful;
  }
  public Boolean getRelance() {
    return relance;
  }
  public void setRelance(Boolean relance) {
    this.relance = relance;
  }
  @Override
  public Object getQuartzObject(Boolean useProperties){

    FiredTriggerRecord firedTriggerRecord = new FiredTriggerRecordImpl();

    firedTriggerRecord.setFireInstanceId(this.entreeId);
    firedTriggerRecord.setFireInstanceState(this.etat);
    firedTriggerRecord.setFireTimestamp(this.debutTraitement);
    firedTriggerRecord.setPriority(this.priorite.intValue());
    firedTriggerRecord.setSchedulerInstanceId(this.nomScheduler);
    firedTriggerRecord.setTriggerIsVolatile(this.triggerVolatile);
    firedTriggerRecord.setTriggerKey(new Key(this.getTrigger().getNom(), this.getTrigger().getGroupe()));

    if (!firedTriggerRecord.getFireInstanceState().equals(Constants.STATE_ACQUIRED)) {
      firedTriggerRecord.setJobIsStateful(this.stateful);
      firedTriggerRecord.setJobRequestsRecovery(this.relance);
      firedTriggerRecord.setJobKey(new Key(this.getTrigger().getJob().getNom(), this.getTrigger().getJob().getGroupe()));
    }

    return firedTriggerRecord;
  }

  public Long getDeclencheurId() {
    return declencheurId;
  }

  public void setDeclencheurId(Long declencheurId) {
    this.declencheurId = declencheurId;
  }

  public QuartzTrigger getTrigger() {
    return trigger;
  }

  public void setTrigger(QuartzTrigger trigger) {
    this.trigger = trigger;
  }
}
