/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.util.List;

import org.sofiframework.modele.entite.EntiteTrace;



/**
 * Objet d'affaire décrivant un rôle utilisateur.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 */
public class Role extends EntiteTrace {

  private static final long serialVersionUID = 7519656395213678528L;

  /** l'identifiant unique du rôle */
  private String codeRole;

  /** l'identifiant unique de l'application auquel est associé le rôle */
  private String codeApplication;

  /** le nom du rôle */
  private String nom;

  /**
   * Description du rôle
   */
  private String description;

  /**
   * Code du statut du rôle (A = Actif, I = Inactif)
   */
  private String codeStatut;

  /**
   * Est-ce que le rôle est un rôle de
   * consultation (pas de mise à jour)
   */
  private Boolean consultation;

  /**
   * Permet d'offrir les rôle d'un seul
   */
  private String codeTypeUtilisateur;

  /**
   * Rôle parent du rôle (Le rôle enfant hérite des attributs du parent)
   */
  private String codeRoleParent;

  private String codeApplicationParent;

  private Role roleParent;

  private List<Role> listeRole;

  private List<Role> listeRoleLie;

  private Integer niveau;

  /** Constructeur par défaut */
  public Role() {
    this.setCle(new String[]{"codeRole", "codeApplication"});
  }

  public Role(String codeRole, String codeApplication) {
    super();
    this.codeRole = codeRole;
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir l'identifiant unique du rôle.
   * <p>
   * @return l'identifiant unique du rôle
   */
  public String getCodeRole() {
    return codeRole;
  }

  /**
   * Fixer l'identifiant unique du rôle.
   * <p>
   * @param codeRole l'identifiant unique du rôle
   */
  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  /**
   * Obtenir le nom du rôle.
   * <p>
   * @return le nom du rôle
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du rôle.
   * <p>
   * @param nom le nom du rôle
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir l'identifiant unique de l'application auquel est associé le rôle.
   * <p>
   * @return l'identifiant unique de l'application auquel est associé le rôle
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique de l'application auquel est associé le rôle.
   * <p>
   * @param codeApplication l'identifiant unique de l'application auquel est associé le rôle
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getCodeStatut() {
    return codeStatut;
  }

  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  public String getCodeRoleParent() {
    return codeRoleParent;
  }

  public void setCodeRoleParent(String codeRoleParent) {
    this.codeRoleParent = codeRoleParent;
  }

  public String getCodeApplicationParent() {
    return codeApplicationParent;
  }

  public void setCodeApplicationParent(String codeApplicationParent) {
    this.codeApplicationParent = codeApplicationParent;
  }

  public Integer getNiveau() {
    return niveau;
  }

  public void setNiveau(Integer niveau) {
    this.niveau = niveau;
  }

  public void fixerNiveau() {

    int niveau = 1;

    Role parent = this.getRoleParent();

    if (parent != null) {
      for (; parent != null; niveau++) {
        parent = parent.getRoleParent();
      }
    }

    this.niveau = new Integer(niveau);

  }

  public Role getRoleParent() {
    return roleParent;
  }

  public void setRoleParent(Role roleParent) {
    this.roleParent = roleParent;
  }

  public Boolean getConsultation() {
    return consultation;
  }

  public void setConsultation(Boolean consultation) {
    this.consultation = consultation;
  }

  public String getCodeTypeUtilisateur() {
    return codeTypeUtilisateur;
  }

  public void setCodeTypeUtilisateur(String codeTypeUtilisateur) {
    this.codeTypeUtilisateur = codeTypeUtilisateur;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + (codeRole == null ? 0 : codeRole.hashCode());
    result = prime * result + (codeApplication == null ? 0 : codeApplication.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!super.equals(obj)) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Role other = (Role) obj;
    if (codeRole == null) {
      if (other.codeRole != null) {
        return false;
      }
    } else if (!codeRole.equals(other.codeRole)) {
      return false;
    }
    if (codeApplication == null) {
      if (other.codeApplication != null) {
        return false;
      }
    } else if (!codeApplication.equals(other.codeApplication)) {
      return false;
    }
    return true;
  }

  public void setListeRole(List<Role> listeRole) {
    this.listeRole = listeRole;
  }

  public List<Role> getListeRole() {
    return listeRole;
  }

  public void setListeRoleLie(List<Role> listeRoleLie) {
    this.listeRoleLie = listeRoleLie;
  }

  public List<Role> getListeRoleLie() {
    return listeRoleLie;
  }

}
