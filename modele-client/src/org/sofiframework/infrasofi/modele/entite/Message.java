/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Représetation objet d'un message affiché dans les applications SOFI.
 * 
 * @author Jean-Maxime Pelletier
 */
public class Message extends EntiteTrace {

  private static final long serialVersionUID = 3009938456986435333L;

  /**
   * La clé qui identifie uniquement le message.
   */
  private String cleMessage;

  /**
   * Un code qui indique la langue dans laquelle le message est écrit.
   */
  private String codeLangue;

  /**
   * Le code de l'application à laquelle le message est associé.
   */
  private String codeApplication;

  private String codeClient;

  /**
   * Le contenu du message
   */
  private String texteMessage;

  /**
   * Un code qui indique le type de message. e.g. E (Erreur), I (Information), etc.
   */
  private String codeSeverite;

  /**
   * Le texte à afficher dans la bulle d'aide du message, si possible.
   */
  private String aideContextuelle;

  /**
   * Le texte Html à afficher dans la bulle d'aide du message, si possible.
   */
  private String aideContextuelleHtml;

  private String reference;

  public Message(String cleMessage, String codeLangue, String codeClient) {
    this();
    this.cleMessage = cleMessage;
    this.codeLangue = codeLangue;
    this.codeClient = codeClient;
  }

  public Message() {
    super();
    this.setCle(new String[] { "id" });
  }
  
  public Message (Message message){
   this.cleMessage = message.getCleMessage();
   this.codeLangue = message.getCodeLangue();
   this.codeApplication = message.getCodeApplication();
   this.texteMessage = message.getTexteMessage();
   this.codeSeverite = message.getCodeSeverite();
   this.aideContextuelle = message.getAideContextuelle();
   this.aideContextuelleHtml = message.getAideContextuelleHtml();
   this.reference = message.getReference();
  }

  /**
   * Fixe la clé unique du message
   * 
   * @param identifiant
   *          la clé unique du message
   */
  public void setCleMessage(String cleMessage) {
    this.cleMessage = cleMessage;
  }

  /**
   * Retourne la clé unique du message
   * 
   * @return la clé unique du message
   */
  public String getCleMessage() {
    return cleMessage;
  }

  /**
   * Fixe le code de langue du message
   * 
   * @param codeLangue
   *          le code de langue du message
   */
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  /**
   * Retourne le code de langue du message
   * 
   * @return le code de langue du message
   */
  public String getCodeLangue() {
    return codeLangue;
  }

  /**
   * Fixe le code d'application
   * 
   * @param codeApplication
   *          le code d'application
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne le code d'application
   * 
   * @return le code d'application
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixe le code de sévéritié
   * 
   * @param codeSeverite
   *          le code de sévéritié
   */
  public void setCodeSeverite(String codeSeverite) {
    this.codeSeverite = codeSeverite;
  }

  /**
   * Retourne le code de sévéritié
   * 
   * @return le code de sévéritié
   */
  public String getCodeSeverite() {
    return codeSeverite;
  }

  /**
   * Fixe l'aide contextuelle
   * 
   * @param aideContextuelle
   *          l'aide contextuelle
   */
  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  /**
   * Retourne l'aide contextuelle
   * 
   * @return l'aide contextuelle
   */
  public String getAideContextuelle() {
    return aideContextuelle;
  }

  /**
   * Fixe le contenu du message
   * 
   * @param texte
   *          le contenu du message
   */
  public void setTexteMessage(String texteMessage) {
    this.texteMessage = texteMessage;
  }

  /**
   * Retourne le contenu du message
   * 
   * @return le contenu du message
   */
  public String getTexteMessage() {
    return texteMessage;
  }

  public void setAideContextuelleHtml(String aideContextuelleHtml) {
    this.aideContextuelleHtml = aideContextuelleHtml;
  }

  public String getAideContextuelleHtml() {
    return aideContextuelleHtml;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public String getReference() {
    return reference;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

}
