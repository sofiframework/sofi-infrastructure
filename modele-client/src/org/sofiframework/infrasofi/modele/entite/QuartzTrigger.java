/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.util.Date;
import java.util.Map;

import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.jdbcjobstore.Constants;
import org.sofiframework.infrasofi.modele.commun.QuartzEntite;

public class QuartzTrigger extends QuartzEntite {

  private static final long serialVersionUID = 1159298568311938665L;

  private String nom, groupe, description, etat, type;
  private Boolean triggerVolatile;

  private Long prochaineExecution, derniereExecution, priorite, debut, fin, appeleManque;

  private Long jobId;
  private Long calendrierId;

  private byte[] jobData;

  private QuartzCalendrier calendrier;
  private QuartzJob job;

  // contient le trigger (cron,simple, etc ...) Non mappé dans hibernate
  private QuartzTypeTrigger enfant;

  /**
   * Constructeur par défaut pour Hibernate
   */
  public QuartzTrigger() {
    triggerVolatile = Boolean.FALSE;
  }

  public QuartzTrigger(Long jobId, String nom, String groupe) {
    this.jobId = jobId;
    this.nom = nom;
    this.groupe = groupe;
    this.triggerVolatile = Boolean.FALSE;
  }

  public QuartzTrigger(Long jobId, Trigger trigger) {
    this(jobId, trigger.getName(), trigger.getGroup());

    if (trigger != null) {
      this.initialiserProprietes(trigger);
    }
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getGroupe() {
    return groupe;
  }

  public void setGroupe(String groupe) {
    this.groupe = groupe;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getEtat() {
    return etat;
  }

  public void setEtat(String etat) {
    this.etat = etat;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Boolean getTriggerVolatile() {
    return triggerVolatile;
  }

  public void setTriggerVolatile(Boolean triggerVolatile) {
    this.triggerVolatile = triggerVolatile;
  }

  public Long getProchaineExecution() {
    return prochaineExecution;
  }

  public void setProchaineExecution(Long prochaineExecution) {
    this.prochaineExecution = prochaineExecution;
  }

  /**
   * Retourne la date de la prochaine exécution du trigger
   * 
   * @return un objet de type {@link Date} construit avec l'attribut
   *         {@link QuartzTrigger#prochaineExecution}
   */
  public Date getDateProchaineExecution() {
    if (getProchaineExecution() != null && getProchaineExecution() > 0) {
      return new Date(getProchaineExecution());
    }
    return null;
  }

  public Long getDerniereExecution() {
    return derniereExecution;
  }

  public void setDerniereExecution(Long derniereExecution) {
    this.derniereExecution = derniereExecution;
  }

  /**
   * Retourne la date de la dernière exécution du trigger
   * 
   * @return un objet de type {@link Date} construit avec l'attribut
   *         {@link QuartzTrigger#derniereExecution}
   */
  public Date getDateDerniereExecution() {
    if (getDerniereExecution() != null && getDerniereExecution() > 0) {
      return new Date(getDerniereExecution());
    }
    return null;
  }

  public Long getPriorite() {
    return priorite;
  }

  public void setPriorite(Long priorite) {
    this.priorite = priorite;
  }

  public Long getDebut() {
    return debut;
  }

  public void setDebut(Long debut) {
    this.debut = debut;
  }

  public Long getFin() {
    return fin;
  }

  public void setFin(Long fin) {
    this.fin = fin;
  }

  public Long getAppeleManque() {
    return appeleManque;
  }

  public void setAppeleManque(Long appeleManque) {
    this.appeleManque = appeleManque;
  }

  public byte[] getJobData() {
    return jobData;
  }

  public void setJobData(byte[] jobData) {
    this.jobData = jobData;
  }

  public QuartzCalendrier getCalendrier() {
    return calendrier;
  }

  public void setCalendrier(QuartzCalendrier calendrier) {
    this.calendrier = calendrier;
  }

  public QuartzJob getJob() {
    return job;
  }

  public void setJob(QuartzJob job) {
    this.job = job;
  }

  @SuppressWarnings("rawtypes")
  public Map getDataMap(Boolean useProperties) {
    Map result = null;
    try {
      result = this.getMapObject(this.jobData, useProperties);
    } catch (Exception e) {
    }
    return result;
  }

  @Override
  public Object getQuartzObject(Boolean useProperties) {
    Trigger trigger = null;

    Date nextFireTime = null;
    if (prochaineExecution != null && prochaineExecution > 0) {
      nextFireTime = new Date(prochaineExecution);
    }

    Date previousFireTime = null;
    if (derniereExecution != null && derniereExecution > 0) {
      previousFireTime = new Date(derniereExecution);
    }

    if (Constants.TTYPE_SIMPLE.equals(getType())) {
      SimpleTrigger simpleTrigger = new SimpleTrigger(nom, groupe);
      simpleTrigger.setNextFireTime(nextFireTime);
      simpleTrigger.setPreviousFireTime(previousFireTime);
      trigger = simpleTrigger;
    } else if (Constants.TTYPE_CRON.equals(getType())) {
      CronTrigger cronTrigger = new CronTrigger(nom, groupe);
      cronTrigger.setNextFireTime(nextFireTime);
      cronTrigger.setPreviousFireTime(previousFireTime);
      trigger = cronTrigger;
    }

    if (!Constants.TTYPE_BLOB.equals(getType())) {
      trigger.setJobName(this.getJob().getNom());
      trigger.setJobGroup(this.getJob().getGroupe());
      trigger.setStartTime(new Date(debut));
      if (fin != null && fin > 0) {
        trigger.setEndTime(new Date(fin));
      }
      trigger.setCalendarName(calendrier != null ? calendrier.getNom() : null);
      trigger.setMisfireInstruction(appeleManque.intValue());
      trigger.setVolatility(triggerVolatile);
      trigger.setDescription(description);
      trigger.setPriority(priorite.intValue());
      trigger.setJobDataMap(new JobDataMap(this.getMapObject(jobData, useProperties)));
    }

    return trigger;
  }

  public QuartzTypeTrigger getEnfant() {
    return enfant;
  }

  public void setEnfant(QuartzTypeTrigger enfant) {
    this.enfant = enfant;
  }

  public boolean isSimple() {
    return this.type != null && this.type.equals(Constants.TTYPE_SIMPLE);
  }

  public boolean isCron() {
    return this.type != null && this.type.equals(Constants.TTYPE_CRON);
  }

  public boolean isBlob() {
    return this.type != null && this.type.equals(Constants.TTYPE_BLOB);
  }

  public boolean isActif() {
    return !(Constants.STATE_PAUSED.equals(etat) || Constants.STATE_PAUSED_BLOCKED.equals(etat));
  }

  @SuppressWarnings({ "rawtypes" })
  public Map getJobDataInMap() {
    Map result = null;
    if (this.jobData != null) {
      result = this.getMapObject(this.jobData, false);
    }
    return result;
  }

  @SuppressWarnings("rawtypes")
  public void setJobDatabyMap(Map map) {
    if (map == null) {
      map = new JobDataMap();
    }

    this.jobData = this.serializeObject(map).toByteArray();
  }

  public Long getJobId() {
    return jobId;
  }

  public void setJobId(Long jobId) {
    this.jobId = jobId;
  }

  public Long getCalendrierId() {
    return calendrierId;
  }

  public void setCalendrierId(Long calendrierId) {
    this.calendrierId = calendrierId;
  }

  /**
   * Permet d'initialiser les propriétés d'une entité {@link QuartzTrigger} à
   * partir de l'objet Quartz {@link Trigger}
   * 
   * @param trigger
   *          un objet de type {@link Trigger}
   */
  public void initialiserProprietes(Trigger trigger) {
    this.triggerVolatile = trigger.isVolatile();
    this.description = trigger.getDescription();
    this.prochaineExecution = trigger.getNextFireTime() != null ? trigger.getNextFireTime().getTime() : null;
    this.derniereExecution = trigger.getPreviousFireTime() != null ? trigger.getPreviousFireTime().getTime() : -1;

    String type = null;

    if (trigger instanceof SimpleTrigger && ((SimpleTrigger) trigger).hasAdditionalProperties() == false) {
      type = Constants.TTYPE_SIMPLE;
    } else if (trigger instanceof CronTrigger && ((CronTrigger) trigger).hasAdditionalProperties() == false) {
      type = Constants.TTYPE_CRON;
    } else {
      type = Constants.TTYPE_BLOB;
    }

    this.type = type;
    this.debut = trigger.getStartTime().getTime();
    this.fin = trigger.getEndTime() != null ? trigger.getEndTime().getTime() : 0;

    this.appeleManque = new Long(trigger.getMisfireInstruction());
    this.priorite = new Long(trigger.getPriority());
    this.jobData = serializeJobData(trigger.getJobDataMap(), Boolean.FALSE).toByteArray();
  }
}
