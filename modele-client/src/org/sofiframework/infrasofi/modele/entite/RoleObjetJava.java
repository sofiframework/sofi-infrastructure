/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.objetstransfert.ObjetTransfert;


/**
 * Objet d'affaire décrivant une relation entre un objet java et un rôle de l'objet java.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 */
public class RoleObjetJava extends ObjetTransfert {

  private static final long serialVersionUID = 8700798571834858044L;

  /** l'identifiant unique de l'application auquel on ajoute un droit d'accès */
  private String codeApplication;

  /** l'identifiant unique de l'objet java auquel on donne des droits */
  private Long idObjetJava;

  /** la valeur qui indique si l'objet java est en consultation ou non */
  private Boolean indicateurConsultation;

  /** l'identifiant unique du rôle auquel on donne droit à la fonctionnalité */
  private String codeRole;

  /**
   * Constructeur par défaut.
   */
  public RoleObjetJava() {
    setCle(new String[]{"codeRole", "codeApplication", "idObjetJava"});
  }

  /**
   * Obtenir l'identifiant unique de l'application auquel on ajoute un droit d'accès.
   * <p>
   * @return l'identifiant unique de l'application auquel on ajoute un droit d'accès
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique de l'application auquel on ajoute un droit d'accès.
   * <p>
   * @param codeApplication l'identifiant unique de l'application auquel on ajoute un droit d'accès
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir l'identifiant unique de l'objet java auquel on donne des droits.
   * <p>
   * @return l'identifiant unique de l'objet java auquel on donne des droits
   */
  public Long getIdObjetJava() {
    return idObjetJava;
  }

  /**
   * Fixer l'identifiant unique de l'objet java auquel on donne des droits.
   * <p>
   * @param idObjetJava l'identifiant unique de l'objet java auquel on donne des droits
   */
  public void setIdObjetJava(Long idObjetJava) {
    this.idObjetJava = idObjetJava;
  }

  /**
   * Obtenir la valeur qui indique si l'objet java est en consultation ou non.
   * <p>
   * @return la valeur qui indique si l'objet java est en consultation ou non
   */
  public Boolean getIndicateurConsultation() {
    return indicateurConsultation;
  }

  /**
   * Fixer la valeur qui indique si l'objet java est en consultation ou non.
   * <p>
   * @param indicateurConsultation la valeur qui indique si l'objet java est en consultation ou non
   */
  public void setIndicateurConsultation(Boolean indicateurConsultation) {
    this.indicateurConsultation = indicateurConsultation;
  }

  /**
   * Obtenir l'identifiant unique du rôle auquel on donne droit à la fonctionnalité.
   * <p>
   * @return l'identifiant unique du rôle auquel on donne droit à la fonctionnalité
   */
  public String getCodeRole() {
    return codeRole;
  }

  /**
   * Fixer l'identifiant unique du rôle auquel on donne droit à la fonctionnalité.
   * <p>
   * @param codeRole l'identifiant unique du rôle auquel on donne droit à la fonctionnalité
   */
  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }
}
