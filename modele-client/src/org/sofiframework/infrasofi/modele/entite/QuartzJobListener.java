/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.infrasofi.modele.commun.QuartzEntite;

public class QuartzJobListener extends QuartzEntite {

  private static final long serialVersionUID = 7371108296590269039L;

  private String ecouteurJob;
  private Long jobId;
  private QuartzJob job;

  /**
   * Constructeur par défaut pour hibernate
   */
  public QuartzJobListener() {
  }

  public QuartzJobListener(Long jobId, String listener){
    this.jobId = jobId;
    this.ecouteurJob = listener;
  }

  public QuartzJobListener(QuartzJob job,String listener){
    this.job = job;
    this.ecouteurJob = listener;
  }

  public String getEcouteurJob() {
    return ecouteurJob;
  }

  public void setEcouteurJob(String ecouteurJob) {
    this.ecouteurJob = ecouteurJob;
  }

  public QuartzJob getJob() {
    return job;
  }

  public void setJob(QuartzJob job) {
    this.job = job;
  }

  @Override
  public Object getQuartzObject(Boolean useProperties){

    return null;
  }

  public Long getJobId() {
    return jobId;
  }

  public void setJobId(Long jobId) {
    this.jobId = jobId;
  }
}
