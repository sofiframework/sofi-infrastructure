/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.infrasofi.modele.commun.QuartzEntite;

public class QuartzTriggerListener extends QuartzEntite {

  private static final long serialVersionUID = 3001879495911065059L;

  private String ecouteurTrigger;
  private Long declencheurId;

  private QuartzTrigger trigger;

  public QuartzTriggerListener(Long declencheurId, String ecouteurTrigger) {
    this.declencheurId = declencheurId;
    this.ecouteurTrigger = ecouteurTrigger;
  }

  public String getEcouteurTrigger() {
    return ecouteurTrigger;
  }

  public void setEcouteurTrigger(String ecouteurTrigger) {
    this.ecouteurTrigger = ecouteurTrigger;
  }

  public QuartzTrigger getTrigger() {
    return trigger;
  }

  public void setTrigger(QuartzTrigger trigger) {
    this.trigger = trigger;
  }

  @Override
  public Object getQuartzObject(Boolean useProperties){

    return null;
  }

  public Long getDeclencheurId() {
    return declencheurId;
  }

  public void setDeclencheurId(Long declencheurId) {
    this.declencheurId = declencheurId;
  }

}
