/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.infrasofi.modele.commun.QuartzEntite;

/**
 * @author rmercier
 *
 */
public class QuartzVerrou extends QuartzEntite {

  /**
   * 
   */
  private static final long serialVersionUID = 2138125228734531181L;


  private Long schedulerId;
  private String nomVerrou;
  private QuartzScheduler scheduler;

  /**
   * Par défaut pour Hibernate
   */
  public QuartzVerrou() {

  }

  public QuartzVerrou(Long schedulerId, String nomVerrou) {
    this.schedulerId = schedulerId;
    this.nomVerrou = nomVerrou;
  }

  /* (non-Javadoc)
   * @see com.nurun.infrasofi.modele.entite.QuartzEntite#getQuartzObject(java.lang.Boolean)
   */
  @Override
  public Object getQuartzObject(Boolean useProperties) {
    return null;
  }

  public String getNomVerrou() {
    return nomVerrou;
  }

  public void setNomVerrou(String nomVerrou) {
    this.nomVerrou = nomVerrou;
  }

  public Long getSchedulerId() {
    return schedulerId;
  }

  public void setSchedulerId(Long schedulerId) {
    this.schedulerId = schedulerId;
  }

  public QuartzScheduler getScheduler() {
    return scheduler;
  }

  public void setScheduler(QuartzScheduler scheduler) {
    this.scheduler = scheduler;
  }

}
