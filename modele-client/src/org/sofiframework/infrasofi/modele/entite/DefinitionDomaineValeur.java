/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.util.Date;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Objet d'affaire représentant un domaine de valeur.
 * <p>
 * 
 * @author Maxime Ouellet, Nurun inc.
 */
public class DefinitionDomaineValeur extends EntiteTrace {

  private static final long serialVersionUID = -1285210205413596530L;

  /** la valeur du domaine qui spécifie qu'il est une définition de domaine. **/
  public static final String VALEUR_DEFINITION = "DEFINITION";

  /** l'identifiant unique du code de l'application du domaine de valeur */
  private String codeApplication;

  /** le nom du domaine de valeur */
  private String nom;

  private String valeur;

  /** la description du domaine de valeur */
  private String description;

  private String codeLangue;

  /** le code de type de tri des domaines de valeur */
  private String codeTypeTri;

  /**
   * Date de début d'activité du domaine.
   */
  private Date dateDebut;

  /**
   * Date de fin d'activité du domaine.
   */
  private Date dateFin;

  private String codeClient;

  private DefinitionDomaineValeur(String codeApplication, String nom, String codeLangue, String valeur) {
    super();
    this.codeApplication = codeApplication;
    this.nom = nom;
    this.codeLangue = codeLangue;
    this.valeur = valeur;
  }

  public DefinitionDomaineValeur(String codeApplication, String nom, String codeLangue) {
    this(codeApplication, nom, codeLangue, VALEUR_DEFINITION);
  }

  /**
   * Constructeur par dï¿½faut.
   */
  public DefinitionDomaineValeur() {
    this.setCle(new String[] { "codeApplication", "nom", "codeLangue" });
  }

  /**
   * Obtenir l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @return l'identifiant unique du code de l'application du domaine de valeur.
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @param codeApplication
   *          l'identifiant unique du code de l'application du domaine de
   *          valeur.
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir le nom du domaine de valeur.
   * 
   * @return le nom du domaine de valeur.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du domaine de valeur.
   * 
   * @param nom
   *          le nom du domaine de valeur.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir la description du domaine de valeur.
   * 
   * @return la description du domaine de valeur.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Fixer la description du domaine de valeur.
   * 
   * @param description
   *          la description du domaine de valeur.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Obtenir le code de type de tri des domaines de valeur.
   * 
   * @return le code de type de tri des domaines de valeur.
   */
  public String getCodeTypeTri() {
    return codeTypeTri;
  }

  /**
   * Fixer le code de type de tri des domaines de valeur.
   * 
   * @param codeTypeTri
   *          le code de type de tri des domaines de valeur.
   */
  public void setCodeTypeTri(String codeTypeTri) {
    this.codeTypeTri = codeTypeTri;
  }

  public String getValeur() {
    return valeur;
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  /**
   * Obtenir le code de langue.
   * 
   * @return Code de langue
   */
  public String getCodeLangue() {
    return codeLangue;
  }

  /**
   * Fixer le code de langue.
   * 
   * @param codeLangue
   *          Code de langue
   */
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  /**
   * Obtenir la date de début.
   * 
   * @return Date de début
   */
  public Date getDateDebut() {
    return dateDebut;
  }

  /**
   * Fixer la date de début.
   * 
   * @param dateDebut Date de début
   */
  public void setDateDebut(Date dateDebut) {
    this.dateDebut = dateDebut;
  }

  /**
   * Obtenir la date de fin
   * @return date de fin
   */
  public Date getDateFin() {
    return dateFin;
  }

  /**
   * Fixer la date de fin.
   * @param dateFin Date de fin
   */
  public void setDateFin(Date dateFin) {
    this.dateFin = dateFin;
  }

  public Boolean getActif() {
    Boolean actif = Boolean.FALSE;

    Date maintenant = new Date();
    Date dd = this.dateDebut == null ? maintenant : this.dateDebut;
    Date df = this.dateFin == null ? maintenant : this.dateFin;

    actif = (maintenant.equals(dd) || maintenant.after(dd)) && (maintenant.equals(df) || maintenant.before(df));

    return actif;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

}
