/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.text.ParseException;
import java.util.TimeZone;

import org.quartz.CronTrigger;
import org.sofiframework.modele.exception.ModeleException;


public class QuartzCronTrigger extends QuartzTypeTrigger {

  private static final long serialVersionUID = -3392605271509347150L;

  private String expression,timeZone;

  public QuartzCronTrigger() {
  }

  public QuartzCronTrigger(Long declencheurId, CronTrigger trigger) {
    super(declencheurId);
    this.expression = trigger.getCronExpression();
    this.timeZone = trigger.getTimeZone().getID();
  }

  public String getExpression() {
    return expression;
  }

  public void setExpression(String expression) {
    this.expression = expression;
  }

  public String getTimeZone() {
    return timeZone;
  }

  public void setTimeZone(String timeZone) {
    this.timeZone = timeZone;
  }

  /*
   * (non-Javadoc)
   * @see com.nurun.infrasofi.modele.entite.QuartzEntite#getQuartzObject(java.lang.Boolean)
   */
  @Override
  public Object getQuartzObject(Boolean useProperties) {
    CronTrigger cronTrigger = (CronTrigger) getTrigger().getQuartzObject(useProperties);
    try {
      cronTrigger.setCronExpression(expression);
    } catch (ParseException e) {
      throw new ModeleException("Expression cron malformée", e);
    }
    if(timeZone != null) {
      cronTrigger.setTimeZone(TimeZone.getTimeZone(timeZone));
    }
    return cronTrigger;
  }

}
