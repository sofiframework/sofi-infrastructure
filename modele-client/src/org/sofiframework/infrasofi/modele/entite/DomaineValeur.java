/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.util.Date;
import java.util.List;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Objet d'affaire représentant un domaine de valeur.
 * <p>
 * 
 * @author Maxime Ouellet, Nurun inc.
 */
public class DomaineValeur extends EntiteTrace {

  private static final long serialVersionUID = -7148281886118290696L;

  /** l'identifiant unique du code de l'application du domaine de valeur */
  private String codeApplication;

  /** le nom du domaine de valeur */
  private String nom;

  /** la valeur du domaine de valeur */
  private String valeur;

  private String codeLangue;

  /** la description du domaine de valeur */
  private String description;

  /** l'ordre d'affichage du domaine de valeur */
  private Integer ordreAffichage;

  /** le nom du domaine parent du domaine de valeur */
  private String nomParent;

  /** la valeur du domaine parent du domaine de valeur */
  private String valeurParent;

  /** l'identifiant unique du code d'application du domaine de valeur parent */
  private String codeApplicationParent;

  /**
   * Code de langue du parent.
   */
  private String codeLangueParent;

  /**
   * Liste des valeurs de domaine enfant
   */
  private List<DomaineValeur> listeDomaineValeurEnfants;

  private Boolean chargementEnfant = Boolean.FALSE;

  /** Variable identifiant la racine d'un domaine de valeur SOFI */
  public static final String NOM_RACINE_DOMAINE_VALEUR = "DEFINITION";

  /**
   * Date de début d'activité de la valeur.
   */
  private Date dateDebut;

  /**
   * Date de fin d'activité de la valeur.
   */
  private Date dateFin;

  /**
   * Le code du client.
   */
  private String codeClient;

  /**
   * Constructeur par défaut.
   */
  public DomaineValeur() {
    this.setCle(new String[] { "codeApplication", "nom", "valeur", "codeLangue" });
  }

  /**
   * Obtenir l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @return l'identifiant unique du code de l'application du domaine de valeur.
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @param codeApplication
   *          l'identifiant unique du code de l'application du domaine de valeur.
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir le nom du domaine de valeur.
   * 
   * @return le nom du domaine de valeur.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du domaine de valeur.
   * 
   * @param nom
   *          le nom du domaine de valeur.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir la valeur du domaine de valeur.
   * 
   * @return la valeur du domaine de valeur.
   */
  public String getValeur() {
    return valeur;
  }

  /**
   * Fixer la valeur du domaine de valeur.
   * 
   * @param valeur
   *          la valeur du domaine de valeur.
   */
  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  /**
   * Obtenir la description du domaine de valeur.
   * 
   * @return la description du domaine de valeur.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Fixer la description du domaine de valeur.
   * 
   * @param description
   *          la description du domaine de valeur.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Obtenir l'ordre d'affichage du domaine de valeur.
   * 
   * @return l'ordre d'affichage du domaine de valeur.
   */
  public Integer getOrdreAffichage() {
    return ordreAffichage;
  }

  /**
   * Fixer l'ordre d'affichage du domaine de valeur.
   * 
   * @param ordreAffichage
   *          l'ordre d'affichage du domaine de valeur.
   */
  public void setOrdreAffichage(Integer ordreAffichage) {
    this.ordreAffichage = ordreAffichage;
  }

  /**
   * Obtenir le nom du domaine parent du domaine de valeur.
   * 
   * @return le nom du domaine parent du domaine de valeur.
   */
  public String getNomParent() {
    return nomParent;
  }

  /**
   * Fixer le nom du domaine parent du domaine de valeur.
   * 
   * @param nomParent
   *          le nom du domaine parent du domaine de valeur.
   */
  public void setNomParent(String nomParent) {
    this.nomParent = nomParent;
  }

  /**
   * Obtenir la valeur du domaine parent du domaine de valeur.
   * 
   * @return la valeur du domaine parent du domaine de valeur.
   */
  public String getValeurParent() {
    return valeurParent;
  }

  /**
   * Fixer la valeur du domaine parent du domaine de valeur.
   * 
   * @param valeurParent
   *          la valeur du domaine parent du domaine de valeur.
   */
  public void setValeurParent(String valeurParent) {
    this.valeurParent = valeurParent;
  }

  /**
   * Obtenir l'identifiant unique du code d'application du domaine de valeur parent.
   * 
   * @return l'identifiant unique du code d'application du domaine de valeur parent.
   */
  public String getCodeApplicationParent() {
    return codeApplicationParent;
  }

  /**
   * Fixer l'identifiant unique du code d'application du domaine de valeur parent.
   * 
   * @param codeApplicationParent
   *          l'identifiant unique du code d'application du domaine de valeur parent.
   */
  public void setCodeApplicationParent(String codeApplicationParent) {
    this.codeApplicationParent = codeApplicationParent;
  }

  public List<DomaineValeur> getListeDomaineValeurEnfants() {
    return listeDomaineValeurEnfants;
  }

  public void setListeDomaineValeurEnfants(List<DomaineValeur> listeDomaineValeurEnfants) {
    this.listeDomaineValeurEnfants = listeDomaineValeurEnfants;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangueParent() {
    return codeLangueParent;
  }

  public void setCodeLangueParent(String codeLangueParent) {
    this.codeLangueParent = codeLangueParent;
  }

  public Boolean getChargementEnfant() {
    return chargementEnfant;
  }

  public void setChargementEnfant(Boolean chargementEnfant) {
    this.chargementEnfant = chargementEnfant;
  }

  public Date getDateDebut() {
    return dateDebut;
  }

  public void setDateDebut(Date dateDebut) {
    this.dateDebut = dateDebut;
  }

  public Date getDateFin() {
    return dateFin;
  }

  public void setDateFin(Date dateFin) {
    this.dateFin = dateFin;
  }

  public Boolean getActif() {
    Boolean actif = Boolean.FALSE;
    Date maintenant = new Date();
    actif = (this.dateDebut == null || maintenant.after(this.dateDebut))
        && (this.dateFin == null || maintenant.before(this.dateFin));
    return actif;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }


}
