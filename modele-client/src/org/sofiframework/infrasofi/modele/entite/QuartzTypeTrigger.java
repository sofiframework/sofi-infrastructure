/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.infrasofi.modele.commun.QuartzEntite;

/**
 * Classe abstraite permettant de regrouper les propriétés communes
 * aux trois type de entité trigger (simple, cron et blob)
 * @author rmercier
 *
 */
public abstract class QuartzTypeTrigger extends QuartzEntite {

  private static final long serialVersionUID = 720542280376211580L;

  private Long declencheurId;
  private QuartzTrigger trigger;

  public QuartzTypeTrigger() {

  }

  public QuartzTypeTrigger(Long declencheurId) {
    this.declencheurId = declencheurId;
  }
  public QuartzTrigger getTrigger() {
    return trigger;
  }
  public void setTrigger(QuartzTrigger trigger) {
    this.trigger = trigger;
  }

  public Long getDeclencheurId() {
    return declencheurId;
  }

  public void setDeclencheurId(Long declencheurId) {
    this.declencheurId = declencheurId;
  }


}
