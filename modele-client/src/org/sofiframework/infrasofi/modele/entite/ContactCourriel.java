package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.modele.entite.Entite;

/**
 * Information sur l'expéditeur ou le destinataire d'un courriel.
 * 
 * @author jean-maxime.pelletier
 */
public class ContactCourriel extends Entite {

  private static final long serialVersionUID = -861072728668623847L;

  /**
   * Nom de la personne.
   */
  private String nom;

  /**
   * Adresse courriel de la personne.
   */
  private String adresse;

  public ContactCourriel() {
    super();
    this.setCle(new String[] { "id" });
  }

  public ContactCourriel(String nom, String adresse) {
    this.adresse = adresse;
    this.nom = nom;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getAdresse() {
    return adresse;
  }

  public void setAdresse(String adresse) {
    this.adresse = adresse;
  }
}
