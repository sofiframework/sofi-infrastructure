/**
 * 
 */
/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.util.List;

import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * @author Jérôme Fiolleau, Nurun Inc.
 * @date 2009-12-16
 *
 */
public class AssociationAideContextuelle extends ObjetTransfert {

  private static final long serialVersionUID = 7895004333903339226L;

  private Long idOriginal;

  private Long idService;
  private Long idOnglet;
  private Long idComposant;

  private List<ObjetJava> listeOnglet;
  private List<ObjetJava> listeComposant;


  public Long getIdOriginal() {
    return idOriginal;
  }
  public void setIdOriginal(Long id) {
    this.idOriginal = id;
  }
  public Long getIdService() {
    return idService;
  }
  public void setIdService(Long idService) {
    this.idService = idService;
  }
  public Long getIdOnglet() {
    return idOnglet;
  }
  public void setIdOnglet(Long idOnglet) {
    this.idOnglet = idOnglet;
  }
  public Long getIdComposant() {
    return idComposant;
  }
  public void setIdComposant(Long idComposant) {
    this.idComposant = idComposant;
  }
  public List<ObjetJava> getListeOnglet() {
    return listeOnglet;
  }
  public void setListeOnglet(List<ObjetJava> listeOnglet) {
    this.listeOnglet = listeOnglet;
  }
  public List<ObjetJava> getListeComposant() {
    return listeComposant;
  }
  public void setListeComposant(List<ObjetJava> listeComposant) {
    this.listeComposant = listeComposant;
  }
}
