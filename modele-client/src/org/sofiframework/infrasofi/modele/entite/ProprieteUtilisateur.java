/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Valeur d'une propriété pour un utilisateur. Les caractéristiques de
 * l'utilisateur sont défini par la métadonnée propriété.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ProprieteUtilisateur extends EntiteTrace {

  private static final long serialVersionUID = -2112606246748304015L;

  private String valeur;

  private MetadonneePropriete metadonnee;

  private Long utilisateurId;

  public MetadonneePropriete getMetadonnee() {
    return metadonnee;
  }

  public void setMetadonnee(MetadonneePropriete metadonnee) {
    this.metadonnee = metadonnee;
  }

  public String getValeur() {
    return valeur;
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  public Long getUtilisateurId() {
    return utilisateurId;
  }

  public void setUtilisateurId(Long utilisateurId) {
    this.utilisateurId = utilisateurId;
  }

}
