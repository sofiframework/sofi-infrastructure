package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.modele.entite.EntiteTrace;

public class LocaleApplication extends EntiteTrace {

  /**
   * 
   */
  private static final long serialVersionUID = -3492739373415002354L;

  private String codeLocale;
  private String codeApplication;
  private String cleLibelle;
  private Integer ordreAffichage;
  private boolean defaut;

  public LocaleApplication() {
    super();
    this.setCle(new String[] { "codeLocale", "codeApplication" });
  }

  /**
   * @param codeLocale
   *          the codeLocale to set
   */
  public void setCodeLocale(String codeLocale) {
    this.codeLocale = codeLocale;
  }

  /**
   * @return the codeLocale
   */
  public String getCodeLocale() {
    return codeLocale;
  }

  /**
   * @param codeApplication
   *          the codeApplication to set
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * @return the codeApplication
   */
  public String getCodeApplication() {
    return codeApplication;
  }


  /**
   * @param ordreAffichage
   *          the ordreAffichage to set
   */
  public void setOrdreAffichage(Integer ordreAffichage) {
    this.ordreAffichage = ordreAffichage;
  }

  /**
   * @return the ordreAffichage
   */
  public Integer getOrdreAffichage() {
    return ordreAffichage;
  }

  /**
   * @param defaut
   *          the defaut to set
   */
  public void setDefaut(boolean defaut) {
    this.defaut = defaut;
  }

  /**
   * @return the defaut
   */
  public boolean isDefaut() {
    return defaut;
  }

  public void setCleLibelle(String cleLibelle) {
    this.cleLibelle = cleLibelle;
  }

  public String getCleLibelle() {
    return cleLibelle;
  }
}
