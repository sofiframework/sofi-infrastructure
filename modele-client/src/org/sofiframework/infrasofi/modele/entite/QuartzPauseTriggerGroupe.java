/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.infrasofi.modele.commun.QuartzEntite;

public class QuartzPauseTriggerGroupe extends QuartzEntite {

  private static final long serialVersionUID = 3083205918111699763L;

  private String groupe;
  private Long schedulerId;
  private QuartzScheduler scheduler;

  /**
   * Par défaut pour Hibernate
   */
  public QuartzPauseTriggerGroupe() {
    super();
  }

  public QuartzPauseTriggerGroupe(Long schedulerId, String groupe) {
    this.schedulerId = schedulerId;
    this.groupe = groupe;
  }

  public String getGroupe() {
    return groupe;
  }

  public void setGroupe(String groupe) {
    this.groupe = groupe;
  }

  @Override
  public Object getQuartzObject(Boolean useProperties) {
    return null;
  }

  public Long getSchedulerId() {
    return schedulerId;
  }

  public void setSchedulerId(Long schedulerId) {
    this.schedulerId = schedulerId;
  }

  public QuartzScheduler getScheduler() {
    return scheduler;
  }

  public void setScheduler(QuartzScheduler scheduler) {
    this.scheduler = scheduler;
  }

}
