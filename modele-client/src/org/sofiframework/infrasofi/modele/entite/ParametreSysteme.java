/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Paramètre applicatif qui sont chargés au démarage d'une application.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ParametreSysteme extends EntiteTrace {

  private static final long serialVersionUID = -8209237770951949241L;

  private String codeApplication;

  private String codeParametre;

  private String codeClient;

  private java.util.Date dateDebutActivite;

  private java.util.Date dateFinActivite;

  private String valeur;

  private String description;

  /**
   * Permet de catégoriser les paramètres. Exemple : Adresses autres systèmes,
   * sécurité, valeurs pilotages, etc.
   */
  private String codeType;

  /**
   * Permet de savoir si on peut modifier le paramètre à l'aide d'un appel au
   * service commun de paramètre.
   */
  private Boolean modificationADistance;

  /**
   * Permet de savoir si le paramètre varie d'un environnement à l'autre.
   * Certains paramèetres commes des adresses Web d'autres systèmes varient
   * entre les environnements. Cette propriété permet de facilement filtrer les
   * paramètres a graduer lors d'une migration.
   */
  private Boolean differentEnvironnement;

  public ParametreSysteme() {
    setCle(new String[] { "id"});
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeParametre(String codeParametre) {
    this.codeParametre = codeParametre;
  }

  public String getCodeParametre() {
    return codeParametre;
  }

  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  public String getValeur() {
    return valeur;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result
        + (codeApplication == null ? 0 : codeApplication.hashCode());
    result = prime * result
        + (codeParametre == null ? 0 : codeParametre.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!super.equals(obj)) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ParametreSysteme other = (ParametreSysteme) obj;
    if (codeApplication == null) {
      if (other.codeApplication != null) {
        return false;
      }
    } else if (!codeApplication.equals(other.codeApplication)) {
      return false;
    }
    if (codeParametre == null) {
      if (other.codeParametre != null) {
        return false;
      }
    } else if (!codeParametre.equals(other.codeParametre)) {
      return false;
    }
    return true;
  }

  public Boolean getDifferentEnvironnement() {
    return differentEnvironnement;
  }

  public void setDifferentEnvironnement(Boolean differentEnvironnement) {
    this.differentEnvironnement = differentEnvironnement;
  }

  public void setModificationADistance(Boolean modificationADistance) {
    this.modificationADistance = modificationADistance;
  }

  public Boolean getModificationADistance() {
    return modificationADistance;
  }

  public void setCodeType(String codeType) {
    this.codeType = codeType;
  }

  public String getCodeType() {
    return codeType;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public java.util.Date getDateDebutActivite() {
    return dateDebutActivite;
  }

  public void setDateDebutActivite(java.util.Date dateDebutActivite) {
    this.dateDebutActivite = dateDebutActivite;
  }

  public java.util.Date getDateFinActivite() {
    return dateFinActivite;
  }

  public void setDateFinActivite(java.util.Date dateFinActivite) {
    this.dateFinActivite = dateFinActivite;
  }
}
