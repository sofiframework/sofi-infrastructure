/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.util.Date;

import org.sofiframework.modele.entite.EntiteTrace;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Objet d'affaire faisant le lien entre un utilisateur ainsi que les rôles des
 * applications qui s'offrent à lui.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 */
public class UtilisateurRole extends EntiteTrace {

  private static final long serialVersionUID = -4813044058874673119L;

  private Long id;

  /** l'identifiant unique du rôle associé à l'utilisateur */
  private String codeRole;

  /** l'identifiant unique de l'application auquel est associé le rôle utilisateur */
  private String codeApplication;

  /** l'identifiant unique de l'utilisateur auquel est associé le rôle */
  private String codeUtilisateur;

  /** le code client permet de spécifier un rôle différent pour un client particulier
   *  dans une application SOFI. Il permet à un utilisateur de jouer plusieurs rôles
   *  différents en fonction du client pour lequel il est authentifié dans
   *  l'application. */
  private String codeClient;

  /**
   * Rôle associé à l'utilisateur;
   */
  private Role role;

  private Date dateDebutActivite;
  private Date dateFinActivite;
  private String descriptionPeriodeActivite;

  private String nomUtilisateur;
  private String prenomUtilisateur;

  /** Constructeur par défaut */
  public UtilisateurRole() {
    this.setCle(new String[]{"id"});
  }

  /**
   * Obtenir l'identifiant unique du rôle associé à l'utilisateur.
   * <p>
   * @return l'identifiant unique du rôle associé à l'utilisateur
   */
  public String getCodeRole() {
    return codeRole;
  }

  /**
   * Fixer l'identifiant unique du rôle associé à l'utilisateur.
   * <p>
   * @param codeRole l'identifiant unique du rôle associé à l'utilisateur
   */
  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  /**
   * Obtenir l'identifiant unique de l'application auquel est associé le rôle utilisateur.
   * <p>
   * @return l'identifiant unique de l'application auquel est associé le rôle utilisateur
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique de l'application auquel est associé le rôle utilisateur.
   * <p>
   * @param codeApplication l'identifiant unique de l'application auquel est associé le rôle utilisateur
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir l'identifiant unique de l'utilisateur auquel est associé le rôle.
   * <p>
   * @return l'identifiant unique de l'utilisateur auquel est associé le rôle
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * Fixer l'identifiant unique de l'utilisateur auquel est associé le rôle.
   * <p>
   * @param codeUtilisateur l'identifiant unique de l'utilisateur auquel est associé le rôle
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * Obtenir le rôle associé à l'utilisateur.
   * @return Rôle
   */
  public Role getRole() {
    return role;
  }

  protected void setRole(Role role) {
    this.role = role;
  }

  public Date getDateDebutActivite() {
    return dateDebutActivite;
  }

  public void setDateDebutActivite(Date dateDebutActivite) {
    this.dateDebutActivite = dateDebutActivite;
  }

  public Date getDateFinActivite() {
    return dateFinActivite;
  }

  public void setDateFinActivite(Date dateFinActivite) {
    this.dateFinActivite = dateFinActivite;
  }

  public String getDescriptionPeriodeActivite() {
    return descriptionPeriodeActivite;
  }

  public void setDescriptionPeriodeActivite(String descriptionPeriodeActivite) {
    this.descriptionPeriodeActivite = descriptionPeriodeActivite;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getNomCompletUtilisateur() {
    StringBuffer nomComplet = new StringBuffer();

    if (!UtilitaireString.isVide(this.getPrenomUtilisateur())) {
      nomComplet.append(this.getPrenomUtilisateur());
    }

    if (!UtilitaireString.isVide(this.getNomUtilisateur())) {
      if (nomComplet.length() > 0) {
        nomComplet.append(" ");
      }
      nomComplet.append(this.getNomUtilisateur());
    }

    return nomComplet.toString();
  }

  public void setNomUtilisateur(String nomUtilisateur) {
    this.nomUtilisateur = nomUtilisateur;
  }

  public String getNomUtilisateur() {
    return nomUtilisateur;
  }

  public void setPrenomUtilisateur(String prenomUtilisateur) {
    this.prenomUtilisateur = prenomUtilisateur;
  }

  public String getPrenomUtilisateur() {
    return prenomUtilisateur;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public Long getId() {
    return id;
  }
}
