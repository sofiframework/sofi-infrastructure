/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Représentation objet d'un libellé d'une application SOFI.
 * 
 * @author Jean-Maxime Pelletier
 */
public class Libelle extends EntiteTrace {

  private static final long serialVersionUID = -8206732761750079555L;

  private String cleLibelle;
  private String codeLangue;
  private String codeApplication;
  private String codeClient;
  private String texteLibelle;
  private String texteLibelle2;
  private String texteLibelle3;
  private String aideContextuelle;
  private String aideContextuelleHtml;
  private String aideContextuelleExterne;
  private String reference;
  private Boolean libelleHtml;
  private String placeholder;

  public Libelle() {
    super();
    this.setCle(new String[] { "id" });
  }

  public Libelle(String cleLibelle, String codeLangue, String codeClient) {
    this();
    this.cleLibelle = cleLibelle;
    this.codeLangue = codeLangue;
    this.codeClient = codeClient;
  }
  
  public Libelle (Libelle libelle){
    this.cleLibelle = libelle.getCleLibelle();
    this.codeLangue = libelle.getCodeLangue();
    this.codeApplication = libelle.getCodeApplication();
    this.texteLibelle = libelle.getTexteLibelle();
    this.texteLibelle2 = libelle.getTexteLibelle2();
    this.texteLibelle3 = libelle.getTexteLibelle3();
    this.aideContextuelle = libelle.getAideContextuelle();
    this.aideContextuelleHtml = libelle.getAideContextuelleHtml();
    this.aideContextuelleExterne = libelle.getAideContextuelleExterne();
    this.reference = libelle.getReference();
    this.libelleHtml = libelle.getLibelleHtml();
    this.placeholder = libelle.getPlaceholder();
  }

  public void setCleLibelle(String cleLibelle) {
    this.cleLibelle = cleLibelle;
  }

  public String getCleLibelle() {
    return cleLibelle;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  public String getAideContextuelle() {
    return aideContextuelle;
  }

  public void setTexteLibelle(String texteLibelle) {
    this.texteLibelle = texteLibelle;
  }

  public String getTexteLibelle() {
    return texteLibelle;
  }

  public String getIdentifiant() {
    return this.cleLibelle + "&codeLangue=" + this.getCodeLangue();
  }

  public void setAideContextuelleHtml(String aideContextuelleHtml) {
    this.aideContextuelleHtml = aideContextuelleHtml;
  }

  public String getAideContextuelleHtml() {
    return aideContextuelleHtml;
  }

  public void setAideContextuelleExterne(String aideContextuelleExterne) {
    this.aideContextuelleExterne = aideContextuelleExterne;
  }

  public String getAideContextuelleExterne() {
    return aideContextuelleExterne;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public String getReference() {
    return reference;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + (cleLibelle == null ? 0 : cleLibelle.hashCode());
    result = prime * result + (codeLangue == null ? 0 : codeLangue.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!super.equals(obj)) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Libelle other = (Libelle) obj;
    if (cleLibelle == null) {
      if (other.cleLibelle != null) {
        return false;
      }
    } else if (!cleLibelle.equals(other.cleLibelle)) {
      return false;
    }
    if (codeLangue == null) {
      if (other.codeLangue != null) {
        return false;
      }
    } else if (!codeLangue.equals(other.codeLangue)) {
      return false;
    }
    return true;
  }

  public void setLibelleHtml(Boolean libelleHtml) {
    this.libelleHtml = libelleHtml;
  }

  public Boolean getLibelleHtml() {
    return libelleHtml;
  }

  public void setPlaceholder(String placeholder) {
    this.placeholder = placeholder;
  }

  public String getPlaceholder() {
    return placeholder;
  }

  public String getTexteLibelle2() {
    return texteLibelle2;
  }

  public void setTexteLibelle2(String texteLibelle2) {
    this.texteLibelle2 = texteLibelle2;
  }

  public String getTexteLibelle3() {
    return texteLibelle3;
  }

  public void setTexteLibelle3(String texteLibelle3) {
    this.texteLibelle3 = texteLibelle3;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

}
