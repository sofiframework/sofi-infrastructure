/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Définition d'un type de donnée pouvant être associé
 * à un utilisateur.
 * 
 * @author Jean-Maxime Pelletier
 */
public class MetadonneePropriete extends EntiteTrace {

  private static final long serialVersionUID = -2431616833981868650L;

  private String nom;

  private String typeDonnee;

  private String codeClient;

  private Long ordrePresentation;
  
  private Long OrdrePresentationCategorie;
  
  private Boolean obligatoire;
  
  private String codeDomaine;
  
  private String referenceExterne;
  
  private Boolean prioritaire;
  
  private Boolean actif;
  

  public MetadonneePropriete() {
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getTypeDonnee() {
    return typeDonnee;
  }

  public void setTypeDonnee(String typeDonnee) {
    this.typeDonnee = typeDonnee;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public Long getOrdrePresentation() {
    return ordrePresentation;
  }

  public void setOrdrePresentation(Long ordrePresentation) {
    this.ordrePresentation = ordrePresentation;
  }

  public Boolean getObligatoire() {
    return obligatoire;
  }

  public void setObligatoire(Boolean obligatoire) {
    this.obligatoire = obligatoire;
  }

  public String getCodeDomaine() {
    return codeDomaine;
  }

  public void setCodeDomaine(String codeDomaine) {
    this.codeDomaine = codeDomaine;
  }

  public Long getOrdrePresentationCategorie() {
    return OrdrePresentationCategorie;
  }

  public void setOrdrePresentationCategorie(Long ordrePresentationCategorie) {
    OrdrePresentationCategorie = ordrePresentationCategorie;
  }

  public String getReferenceExterne() {
    return referenceExterne;
  }

  public void setReferenceExterne(String referenceExterne) {
    this.referenceExterne = referenceExterne;
  }

  public Boolean getPrioritaire() {
    return prioritaire;
  }

  public void setPrioritaire(Boolean prioritaire) {
    this.prioritaire = prioritaire;
  }

  public Boolean getActif() {
    return actif;
  }

  public void setActif(Boolean actif) {
    this.actif = actif;
  }
}
