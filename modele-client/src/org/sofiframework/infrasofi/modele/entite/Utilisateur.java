/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.util.Date;
import java.util.List;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Objet d'affaire représentant un utilisateur.
 * <p>
 */
public class Utilisateur extends EntiteTrace {

  private static final long serialVersionUID = -8945863325297377927L;

  // L'identifiant généré de l'utilisateur.
  private Long id;

  /** l'identifiant unique de l'utilisateur */
  private String codeUtilisateur;

  /** le mot de passe pour l'authentification de l'utilisateur */
  private String motPasse;

  /** le nom de l'utilisateur */
  private String nom;

  /** le prenom de l'utilisateur */
  private String prenom;

  /** le courriel de l'utilisateur */
  private String courriel;

  /** le courriel de l'utilisateur */
  private String codeUtilisateurApp;

  /** le code de statut de l'utilisateur dans l'application */
  private String codeStatut;

  /**
   * la liste de tous les objets UtilisateurRole associés à l'utilisateur en
   * cours
   */
  private List<UtilisateurRole> listeUtilisateurRole;

  /**
   * La langue de l'utilisateur
   */
  private String langue;

  /**
   * Fuseau horaire de l'utilisateur
   */
  private String fuseauHoraire;

  /**
   * Code de sexe de l'utilisateur (M/F)
   */
  private String sexe;

  /**
   * Date de naissance.
   */
  private Date dateNaissance;

  /**
   * Code de groupe d'age.
   */
  private String codeGroupeAge;

  /**
   * Date d'expiration du mot de passe
   */
  private Date dateMotPasse;

  /**
   * Le nombre de tentative d'accès
   */
  private Integer nbTentativeAcces;

  /**
   * Identifier qu'il s'agit d'un nouveau mot de passe saisi par l'utilisateur.
   */
  private boolean changementMotPasse;

  /**
   * Date du dernier accès.
   */
  private Date dateDernierAcces;

  private UtilisateurApplication utilisateurApplication;

  private String codeLocale;

  /**
   * Url de la photo de l'utilisateur.
   */
  private String urlPhoto;

  /**
   * Le code du fournisseur d'accès de l'authentification.
   */
  private String codeFournisseurAcces;

  /**
   * Description de l'utilisateur.
   */
  private String description;

  private Boolean isDeleted;


  /** Constructeur par défaut */
  public Utilisateur() {
    this.setCle(new String[] { "codeUtilisateur" });
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public Long getId() {
    return id;
  }

  /**
   * Obtenir l'identifiant unique de l'utilisateur auquel est associé le rôle.
   * <p>
   * 
   * @return l'identifiant unique de l'utilisateur auquel est associé le rôle
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * Fixer l'identifiant unique de l'utilisateur auquel est associé le rôle.
   * <p>
   * 
   * @param codeUtilisateur
   *          l'identifiant unique de l'utilisateur auquel est associé le rôle
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * Obtenir le mot de passe pour l'authentification de l'utilisateur.
   * 
   * @return le mot de passe pour l'authentification de l'utilisateur.
   */
  public String getMotPasse() {
    return motPasse;
  }

  /**
   * Fixer le mot de passe pour l'authentification de l'utilisateur.
   * 
   * @param modeDePasse
   *          le mot de passe pour l'authentification de l'utilisateur.
   */
  public void setMotPasse(String motPasse) {
    this.motPasse = motPasse;
  }

  /**
   * Obtenir le nom de l'utilisateur.
   * 
   * @return le nom de l'utilisateur.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de l'utilisateur.
   * 
   * @param nom
   *          le nom de l'utilisateur.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le prenom de l'utilisateur.
   * 
   * @return le prenom de l'utilisateur.
   */
  public String getPrenom() {
    return prenom;
  }

  /**
   * Fixer le prenom de l'utilisateur.
   * 
   * @param prenom
   *          le prenom de l'utilisateur.
   */
  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  /**
   * Obtenir le courriel de l'utilisateur.
   * 
   * @return le courriel de l'utilisateur.
   */
  public String getCourriel() {
    return courriel;
  }

  /**
   * Fixer le courriel de l'utilisateur.
   * 
   * @param courriel
   *          le courriel de l'utilisateur.
   */
  public void setCourriel(String courriel) {
    this.courriel = courriel;
  }

  /**
   * Obtenir le code de statut de l'utilisateur dans l'application.
   * 
   * @return le code de statut de l'utilisateur dans l'application.
   */
  public String getCodeStatut() {
    return codeStatut;
  }

  /**
   * Fixer le code de statut de l'utilisateur dans l'application.
   * 
   * @param codeStatut
   *          le code de statut de l'utilisateur dans l'application.
   */
  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  /**
   * Obtenir la liste de tous les objets UtilisateurRole associés à
   * l'utilisateur en cours.
   * <p>
   * 
   * @return la liste de tous les objets UtilisateurRole associés à
   *         l'utilisateur en cours
   */
  public List<UtilisateurRole> getListeUtilisateurRole() {
    return listeUtilisateurRole;
  }

  /**
   * Fixer la liste de tous les objets UtilisateurRole associés à l'utilisateur
   * en cours.
   * <p>
   * 
   * @param listeUtilisateurRole
   *          la liste de tous les objets UtilisateurRole associés à
   *          l'utilisateur en cours
   */
  public void setListeUtilisateurRole(List<UtilisateurRole> listeUtilisateurRole) {
    this.listeUtilisateurRole = listeUtilisateurRole;
  }

  public String getCodeUtilisateurApp() {
    return codeUtilisateurApp;
  }

  public void setCodeUtilisateurApp(String codeUtilisateurApp) {
    this.codeUtilisateurApp = codeUtilisateurApp;
  }

  public boolean isChangementMotPasse() {
    return changementMotPasse;
  }

  public void setChangementMotPasse(boolean changementMotPasse) {
    this.changementMotPasse = changementMotPasse;
  }

  public String getLangue() {
    return langue;
  }

  public void setLangue(String langue) {
    this.langue = langue;
  }

  public Date getDateMotPasse() {
    return dateMotPasse;
  }

  public void setDateMotPasse(Date dateMotPasse) {
    this.dateMotPasse = dateMotPasse;
  }

  public void setUtilisateurApplication(
      UtilisateurApplication utilisateurApplication) {
    this.utilisateurApplication = utilisateurApplication;
  }

  public UtilisateurApplication getUtilisateurApplication() {
    return utilisateurApplication;
  }

  public Date getDateDernierAcces() {
    return dateDernierAcces;
  }

  public void setDateDernierAcces(Date dateDernierAcces) {
    this.dateDernierAcces = dateDernierAcces;
  }

  public Integer getNbTentativeAcces() {
    return nbTentativeAcces;
  }

  public void setNbTentativeAcces(Integer nbTentativeAcces) {
    this.nbTentativeAcces = nbTentativeAcces;
  }

  public String getCodeLocale() {
    return codeLocale;
  }

  public void setCodeLocale(String codeLocale) {
    this.codeLocale = codeLocale;
  }

  /**
   * L'url de la photo qui personnalise l'utilisateur.
   * @return l'url de la photo qui personnalise l'utilisateur.
   * @since 3.0.2
   */
  public String getUrlPhoto() {
    return urlPhoto;
  }

  /**
   * Fixer l'url de la photo qui personnalise l'utilisateur.
   * @param urlPhoto l'url de la photo qui personnalise l'utilisateur.
   * @since 3.0.2
   */
  public void setUrlPhoto(String urlPhoto) {
    this.urlPhoto = urlPhoto;
  }

  /**
   * Le code de fournisseur d'accès de l'authentification de l'utilisateur.
   * @return le code de fournisseur d'accès de l'authentification de l'utilisateur.
   * @since 3.0.2
   */
  public String getCodeFournisseurAcces() {
    return codeFournisseurAcces;
  }

  /**
   * Fixer le code de fournisseur d'accès de l'authentification de l'utilisateur.
   * @param codeFournisseurAcces le code de fournisseur d'accès de l'authentification de l'utilisateur.
   * @since 3.0.2
   */
  public void setCodeFournisseurAcces(String codeFournisseurAcces) {
    this.codeFournisseurAcces = codeFournisseurAcces;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }


  public void setIsDeleted(Boolean isDeleted) {
    this.isDeleted = isDeleted;
  }


  public Boolean getIsDeleted() {
    return isDeleted;
  }


  public String getFuseauHoraire() {
    return fuseauHoraire;
  }


  public void setFuseauHoraire(String fuseauHoraire) {
    this.fuseauHoraire = fuseauHoraire;
  }


  public String getSexe() {
    return sexe;
  }


  public void setSexe(String sexe) {
    this.sexe = sexe;
  }


  public Date getDateNaissance() {
    return dateNaissance;
  }


  public void setDateNaissance(Date dateNaissance) {
    this.dateNaissance = dateNaissance;
  }


  public String getCodeGroupeAge() {
    return codeGroupeAge;
  }


  public void setCodeGroupeAge(String codeGroupeAge) {
    this.codeGroupeAge = codeGroupeAge;
  }
}
