/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.util.List;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Aide contextuelle aux objets java de l'application.
 * 
 * @author Jean-Maxime Pelletier
 */
public class AideContextuelle extends EntiteTrace {

  private static final long serialVersionUID = -4263783551959308222L;

  private String cleAide;
  private String codeLangue;
  private String codeApplication;
  private String texteAide;
  private String titre;
  private String aideExterne;
  private List<AssociationAideContextuelle> listeAssociationAideContextuelle;

  public AideContextuelle() {
    super();
    this.setCle(new String[]{"cleAide", "codeLangue"});
  }

  public AideContextuelle(String cleAide, String codeLangue) {
    super();
    this.setCle(new String[]{"cleAide", "codeLangue"});
    this.cleAide = cleAide;
    this.codeLangue = codeLangue;
  }

  public String getCleAide() {
    return cleAide;
  }

  public void setCleAide(String cleAide) {
    this.cleAide = cleAide;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getTexteAide() {
    return texteAide;
  }

  public void setTexteAide(String texteAide) {
    this.texteAide = texteAide;
  }

  public String getTitre() {
    return titre;
  }

  public void setTitre(String titre) {
    this.titre = titre;
  }


  @Override
  public Object clone() {
    AideContextuelle aideContextuelle = (AideContextuelle) super.clone();

    //    if (listeAssociationAideContextuelle != null) {
    //      List liste = new ArrayList(listeAssociationAideContextuelle.size());
    //
    //      for (Iterator it = listeAssociationAideContextuelle.iterator(); it.hasNext();) {
    //        ObjetJava objet = (ObjetJava) it.next();
    //        liste.add(objet.clone());
    //      }
    //
    //      aideContextuelle.setListeAssociationAideContextuelle(liste);
    //    }

    return aideContextuelle;
  }

  public void setAideExterne(String aideExterne) {
    this.aideExterne = aideExterne;
  }

  public String getAideExterne() {
    return aideExterne;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + (cleAide == null ? 0 : cleAide.hashCode());
    result = prime * result + (codeLangue == null ? 0 : codeLangue.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!super.equals(obj)) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final AideContextuelle other = (AideContextuelle) obj;
    if (cleAide == null) {
      if (other.cleAide != null) {
        return false;
      }
    } else if (!cleAide.equals(other.cleAide)) {
      return false;
    }
    if (codeLangue == null) {
      if (other.codeLangue != null) {
        return false;
      }
    } else if (!codeLangue.equals(other.codeLangue)) {
      return false;
    }
    return true;
  }

  public List<AssociationAideContextuelle> getListeAssociationAideContextuelle() {
    return listeAssociationAideContextuelle;
  }

  public void setListeAssociationAideContextuelle(
      List<AssociationAideContextuelle> listeAssociationAideContextuelle) {
    this.listeAssociationAideContextuelle = listeAssociationAideContextuelle;
  }
}
