/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.util.Date;
import java.util.List;

import org.sofiframework.modele.entite.EntiteTrace;


/**
 * Objet d'affaire représentant une application.
 * <p>
 * Cette classe étend de la classe <code>Application</code> de SOFI ce qui fait qu'on ne
 * retrouve pas automatiquement tous les champs.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 */
public class Application extends EntiteTrace {

  private static final long serialVersionUID = 8075088374791998912L;

  /**
   * Code de l'application.
   */
  private String codeApplication;

  /**
   * Nom de l'application.
   */
  private String nom;

  /**
   * Adresse du site de l'application.
   */
  private String adresseSite;

  /**
   * Description de l'application.
   */
  private String description;

  /**
   * Acronyme de l'application.
   */
  private String acronyme;

  /**
   * Aide contextuelle associée à l'application.
   */
  private String aideContextuelle;

  /**
   * Le chemin correspondant à l'icône de l'application.
   */
  private String icone;

  /**
   * Version de la livraison courante de l'application.
   */
  private String versionLivraison;

  /**
   * Liste des objets sécurisables de l'application.
   */
  private List<ObjetJava> listeObjetJava;

  /** la date à laquelle l'application commence à être active */
  private Date dateDebutActivite;

  /** la date à laquelle l'application cesse d'être active */
  private Date dateFinActivite;

  /** le statut qui indique si l'application est active ou pas */
  private String statut;

  /** le code utilisateur de l'utilisateur qui est administrateur de l'application */
  private String codeUtilisateurAdministrateur;

  /** le mot de passe de l'utilisateur qui est administrateur de l'application */
  private String motPasseAdministrateur;

  /** la liste des pages d'application qui représentent les pages d'erreur de l'application */
  private List<PageApplication> listePageErreur;

  /** la liste des pages d'application qui représentent les pages de maintenance de l'application */
  private List<PageApplication> listePageMaintenance;

  /** la liste des pages d'application qui représentent les pages de sécurité de l'application */
  private List<PageApplication> listePageSecurite;

  /** l'identifiant unique des pages d'erreurs de l'application */
  private String clePageErreur;

  /** l'identifiant unique des pages de maintenance de l'application */
  private String clePageMaintenance;

  /** l'identifiant unique des pages de sécurité de l'application */
  private String clePageSecurite;

  /**
   * Est-ce une application commune.
   */
  private Boolean commun;

  /**
   * Est-ce une application externe (grand public).
   */
  private Boolean externe;

  /**
   * Adresse des noeuds pour rafraichir les caches dans l'applciation.
   */
  private String adresseCache;

  /** Constructeur par défaut */
  public Application() {
    this.setCle(new String[]{"codeApplication"});
  }

  /**
   * Obtenir la date à laquelle l'application commence à être active.
   * <p>
   * @return la date à laquelle l'application commence à être active
   */
  public Date getDateDebutActivite() {
    return dateDebutActivite;
  }

  /**
   * Fixer la date à laquelle l'application commence à être active.
   * <p>
   * @param dateDebutActivite la date à laquelle l'application commence à être active
   */
  public void setDateDebutActivite(Date dateDebutActivite) {
    this.dateDebutActivite = dateDebutActivite;
  }

  /**
   * Obtenir la date à laquelle l'application cesse d'être active.
   * <p>
   * @return la date à laquelle l'application cesse d'être active
   */
  public Date getDateFinActivite() {
    return dateFinActivite;
  }

  /**
   * Fixer la date à laquelle l'application cesse d'être active.
   * <p>
   * @param dateFinActivite la date à laquelle l'application cesse d'être active
   */
  public void setDateFinActivite(Date dateFinActivite) {
    this.dateFinActivite = dateFinActivite;
  }

  /**
   * Obtenir le statut qui indique si l'application est active ou pas.
   * <p>
   * @return le statut qui indique si l'application est active ou pas
   */
  public String getStatut() {
    return statut;
  }

  /**
   * Fixer le statut qui indique si l'application est active ou pas.
   * <p>
   * @param statut le statut qui indique si l'application est active ou pas
   */
  public void setStatut(String statut) {
    this.statut = statut;
  }

  /**
   * Obtenir le code utilisateur de l'utilisateur qui est administrateur de l'application.
   * <p>
   * @return le code utilisateur de l'utilisateur qui est administrateur de l'application
   */
  public String getCodeUtilisateurAdministrateur() {
    return codeUtilisateurAdministrateur;
  }

  /**
   * Fixer le code utilisateur de l'utilisateur qui est administrateur de l'application.
   * <p>
   * @param codeUtilisateurAdministrateur le code utilisateur de l'utilisateur qui est administrateur de l'application
   */
  public void setCodeUtilisateurAdministrateur(String codeUtilisateurAdministrateur) {
    this.codeUtilisateurAdministrateur = codeUtilisateurAdministrateur;
  }

  /**
   * Obtenir le mot de passe de l'utilisateur qui est administrateur de l'application.
   * <p>
   * @return le mot de passe de l'utilisateur qui est administrateur de l'application
   */
  public String getMotPasseAdministrateur() {
    return motPasseAdministrateur;
  }

  /**
   * Fixer le mot de passe de l'utilisateur qui est administrateur de l'application.
   * <p>
   * @param motPasseAdministrateur le mot de passe de l'utilisateur qui est administrateur de l'application
   */
  public void setMotPasseAdministrateur(String motPasseAdministrateur) {
    this.motPasseAdministrateur = motPasseAdministrateur;
  }

  /**
   * Obtenir la liste des pages d'application qui représentent les pages d'erreur de l'application.
   * <p>
   * @return la liste des pages d'application qui représentent les pages d'erreur de l'application
   */
  public List<PageApplication> getListePageErreur() {
    return listePageErreur;
  }

  /**
   * Fixer la liste des pages d'application qui représentent les pages d'erreur de l'application.
   * <p>
   * @param listePageErreur la liste des pages d'application qui représentent les pages d'erreur de l'application
   */
  public void setListePageErreur(List<PageApplication> listePageErreur) {
    this.listePageErreur = listePageErreur;
  }

  /**
   * Obtenir la liste des pages d'application qui représentent les pages de maintenance de l'application.
   * <p>
   * @return la liste des pages d'application qui représentent les pages de maintenance de l'application
   */
  public List<PageApplication> getListePageMaintenance() {
    return listePageMaintenance;
  }

  /**
   * Fixer la liste des pages d'application qui représentent les pages de maintenance de l'application.
   * <p>
   * @param listePageMaintenance la liste des pages d'application qui représentent les pages de maintenance de l'application
   */
  public void setListePageMaintenance(List<PageApplication> listePageMaintenance) {
    this.listePageMaintenance = listePageMaintenance;
  }

  /**
   * Obtenir la liste des pages d'application qui représentent les pages de sécurité de l'application.
   * <p>
   * @return la liste des pages d'application qui représentent les pages de sécurité de l'application
   */
  public List<PageApplication> getListePageSecurite() {
    return listePageSecurite;
  }

  /**
   * Fixer la liste des pages d'application qui représentent les pages de sécurité de l'application.
   * <p>
   * @param listePageSecurite la liste des pages d'application qui représentent les pages de sécurité de l'application
   */
  public void setListePageSecurite(List<PageApplication> listePageSecurite) {
    this.listePageSecurite = listePageSecurite;
  }

  /**
   * Obtenir l'identifiant unique des pages d'erreurs de l'application.
   * <p>
   * @return l'identifiant unique des pages d'erreurs de l'application
   */
  public String getClePageErreur() {
    return clePageErreur;
  }

  /**
   * Fixer l'identifiant unique des pages d'erreurs de l'application.
   * <p>
   * @param clePageErreur l'identifiant unique des pages d'erreurs de l'application
   */
  public void setClePageErreur(String clePageErreur) {
    this.clePageErreur = clePageErreur;
  }

  /**
   * Obtenir l'identifiant unique des pages de maintenance de l'application.
   * <p>
   * @return l'identifiant unique des pages de maintenance de l'application
   */
  public String getClePageMaintenance() {
    return clePageMaintenance;
  }

  /**
   * Fixer l'identifiant unique des pages de maintenance de l'application.
   * <p>
   * @param clePageMaintenance l'identifiant unique des pages de maintenance de l'application
   */
  public void setClePageMaintenance(String clePageMaintenance) {
    this.clePageMaintenance = clePageMaintenance;
  }

  /**
   * Obtenir l'identifiant unique des pages de sécurité de l'application.
   * <p>
   * @return l'identifiant unique des pages de sécurité de l'application
   */
  public String getClePageSecurite() {
    return clePageSecurite;
  }

  /**
   * Fixer l'identifiant unique des pages de sécurité de l'application.
   * <p>
   * @param clePageSecurite l'identifiant unique des pages de sécurité de l'application
   */
  public void setClePageSecurite(String clePageSecurite) {
    this.clePageSecurite = clePageSecurite;
  }

  /**
   * Obtenir le code, l'identifiant de l'application
   * @return le code, l'identifiant de l'application
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer le code de l'application.
   * @param newCodeApplication
   */
  public void setCodeApplication(String newCodeApplication) {
    codeApplication = newCodeApplication;
  }

  /**
   * Retourne le nom de l'application.
   * @return le nom de l'application
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de l'application.
   * @param newNom le nouveau nom de l'application
   */
  public void setNom(String newNom) {
    nom = newNom;
  }

  /**
   * Obtenir l'adresse qui permet d'appeller l'application.
   * @return l'adresse qui permet d'appeller l'application
   */
  public String getAdresseSite() {
    return adresseSite;
  }

  /**
   * Fixer l'adresse qui permet d'appeller l'application.
   * @param newAdresseSite nouvelle adresse de l'application
   */
  public void setAdresseSite(String newAdresseSite) {
    adresseSite = newAdresseSite;
  }

  /**
   * Retourne la description de l'application.
   * @return la description de l'application
   */
  public String getDescription() {
    return description;
  }

  /**
   * Fixer la description de l'application.
   * @param newDescription la description de l'application
   */
  public void setDescription(String newDescription) {
    description = newDescription;
  }

  /**
   * Retourne l'aide contextuelle pour l'application.
   * @return l'aide contextuelle pour l'application
   */
  public String getAideContextuelle() {
    return aideContextuelle;
  }

  /**
   * Fixer l'aide contextuelle pour l'application.
   * @param aideContextuelle l'aide contextuelle pour l'application
   */
  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  /**
   * Retourne le chemin correspondant à l'icône de l'application.
   * @return le chemin correspondant à l'icône de l'application
   */
  public String getIcone() {
    return icone;
  }

  /**
   * Fixer le chemin correspondant à l'icône de l'application.
   * @param icone le chemin correspondant à l'icône de l'application.
   */
  public void setIcone(String icone) {
    this.icone = icone;
  }

  /**
   * Obtenir la liste des objets sécurisables pour l'application.
   * <p>
   * C'est objets sécurisables peut être des sections de menu, des services,
   * des liens hypertextes, des boutons ou encore des champs de saisie.
   * @return la liste des objets sécurisables pour l'application
   */
  public List<ObjetJava> getListeObjetJava() {
    return listeObjetJava;
  }

  /**
   * Fixer la liste des objets sécurisables pour l'application.
   * <p>
   * C'est objets sécurisables peut être des sections de menu, des services,
   * des liens hypertextes, des boutons ou encore des champs de saisie.
   * @param listeObjetSecurisables
   */
  public void setListeObjetJava(List<ObjetJava> listeObjetJava) {
    this.listeObjetJava = listeObjetJava;
  }

  /**
   * Obtenir le numéro de la version de la livraison.
   * @return version
   */
  public String getVersionLivraison() {
    return versionLivraison;
  }

  /**
   * Modifier le numéro de la version de la livraison.
   * @param versionLivraison nouvelle version
   */
  public void setVersionLivraison(String versionLivraison) {
    this.versionLivraison = versionLivraison;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + (codeApplication == null ? 0 : codeApplication.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Application other = (Application) obj;
    if (codeApplication == null) {
      if (other.codeApplication != null) {
        return false;
      }
    } else if (!codeApplication.equals(other.codeApplication)) {
      return false;
    }
    return true;
  }

  public String getAcronyme() {
    return acronyme;
  }

  public void setAcronyme(String acronyme) {
    this.acronyme = acronyme;
  }

  public Boolean getCommun() {
    return commun;
  }

  public void setCommun(Boolean commun) {
    this.commun = commun;
  }

  public void setExterne(Boolean externe) {
    this.externe = externe;
  }

  public Boolean getExterne() {
    return externe;
  }

  public String getAdresseCache() {
    return adresseCache;
  }

  public void setAdresseCache(String adresseCache) {
    this.adresseCache = adresseCache;
  }
}
