/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Information requise pour effectuer le changement du mot
 * de passe d'un utilisateur.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ChangementMotPasse extends ObjetTransfert {

  private static final long serialVersionUID = -3239279923073401377L;

  private String codeUtilisateur = null;

  private String nouveauMotPasse = null;

  private String confirmerMotPasse = null;

  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  public String getNouveauMotPasse() {
    return nouveauMotPasse;
  }

  public void setNouveauMotPasse(String nouveauMotPasse) {
    this.nouveauMotPasse = nouveauMotPasse;
  }

  public String getConfirmerMotPasse() {
    return confirmerMotPasse;
  }

  public void setConfirmerMotPasse(String confirmerMotPasse) {
    this.confirmerMotPasse = confirmerMotPasse;
  }
}
