/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.modele.entite.EntiteTrace;



/**
 * Objet d'affaire représentant une page d'erreur, de maintenance ou de sécurité d'une application.
 * <p>
 * @author Maxime Ouellet, Nurun inc.
 */
public class PageApplication extends EntiteTrace {

  private static final long serialVersionUID = 9152585432436977375L;

  /** l'identifiant de la page d'application */
  private String clePage;

  /** l'identifiant unique de la langue de la page d'application */
  private String codeLangue;

  /** le titre de la page d'application */
  private String titrePage;

  /** le texte représentant la page d'application */
  private String textePage;

  /** Constructeur par défaut */
  public PageApplication() {
    this.setCle(new String[] {"clePage", "codeLangue"});
  }

  /**
   * Obtenir l'identifiant de la page d'application.
   * <p>
   * @return l'identifiant de la page d'application
   */
  public String getClePage() {
    return clePage;
  }

  /**
   * Fixer l'identifiant de la page d'application.
   * <p>
   * @param clePage l'identifiant de la page d'application
   */
  public void setClePage(String clePage) {
    this.clePage = clePage;
  }

  /**
   * Obtenir l'identifiant unique de la langue de la page d'application.
   * <p>
   * @return l'identifiant unique de la langue de la page d'application
   */
  public String getCodeLangue() {
    return codeLangue;
  }

  /**
   * Fixer l'identifiant unique de la langue de la page d'application.
   * <p>
   * @param codeLangue l'identifiant unique de la langue de la page d'application
   */
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  /**
   * Obtenir le titre de la page d'application.
   * <p>
   * @return le titre de la page d'application
   */
  public String getTitrePage() {
    return titrePage;
  }

  /**
   * Fixer le titre de la page d'application.
   * <p>
   * @param titrePage le titre de la page d'application
   */
  public void setTitrePage(String titrePage) {
    this.titrePage = titrePage;
  }

  /**
   * Obtenir le texte représentant la page d'application.
   * <p>
   * @return le texte représentant la page d'application
   */
  public String getTextePage() {
    return textePage;
  }

  /**
   * Fixer le texte représentant la page d'application.
   * <p>
   * @param textePage le texte représentant la page d'application
   */
  public void setTextePage(String textePage) {
    this.textePage = textePage;
  }
}
