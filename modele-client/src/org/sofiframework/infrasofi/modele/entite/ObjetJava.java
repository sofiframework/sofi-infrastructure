/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Objet qui représente une composante de l'application. <p/> Par exemple : une
 * section, un service, un onglet, etc.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ObjetJava extends EntiteTrace {

  private static final long serialVersionUID = 591536262595769511L;

  private Long id;
  private String type;
  private String codeStatut;
  private String nom;
  private String description;
  private String nomAction;
  private String nomParametre;
  private String adresseWeb;
  private Date dateDebutActivite;
  private Date dateFinActivite;
  private Long ordreAffichage;
  private Long idParent;
  private String codeApplication;
  private String codeFacette;
  private Boolean securise;
  private String cleAide;
  private String typeModeAffichage;
  private String divAjax;
  private String styleCss;
  private Long niveauOnglet;
  private String versionLivraison;
  public String getCodeFacette() {
    return codeFacette;
  }

  public void setCodeFacette(String codeFacette) {
    this.codeFacette = codeFacette;
  }

  private List<RoleObjetJava> listeRoleObjetJavaAutorise;
  private List<ObjetJava> listeObjetJavaEnfant;
  private ObjetJava objetJavaParent;

  private List<RoleObjetJava> listeCodeRole = new ArrayList<RoleObjetJava>();

  private String creePar;
  private Date dateCreation;
  private String modifiePar;
  private Date dateModification;

  /**
   * Constructeur par défaut.
   */
  public ObjetJava() {
    setCle(new String[] { "id" });
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCodeStatut() {
    return codeStatut;
  }

  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getNomAction() {
    return nomAction;
  }

  public void setNomAction(String nomAction) {
    this.nomAction = nomAction;
  }

  public String getNomParametre() {
    return nomParametre;
  }

  public void setNomParametre(String nomParametre) {
    this.nomParametre = nomParametre;
  }

  public String getAdresseWeb() {
    return adresseWeb;
  }

  public void setAdresseWeb(String adresseWeb) {
    this.adresseWeb = adresseWeb;
  }

  public Date getDateDebutActivite() {
    return dateDebutActivite;
  }

  public void setDateDebutActivite(Date dateDebutActivite) {
    this.dateDebutActivite = dateDebutActivite;
  }

  public Date getDateFinActivite() {
    return dateFinActivite;
  }

  public void setDateFinActivite(Date dateFinActivite) {
    this.dateFinActivite = dateFinActivite;
  }

  public Long getOrdreAffichage() {
    return ordreAffichage;
  }

  public void setOrdreAffichage(Long ordreAffichage) {
    this.ordreAffichage = ordreAffichage;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public Boolean getSecurise() {
    return securise;
  }

  public void setSecurise(Boolean securise) {
    this.securise = securise;
  }

  public String getCleAide() {
    return cleAide;
  }

  public void setCleAide(String cleAide) {
    this.cleAide = cleAide;
  }

  public String getTypeModeAffichage() {
    return typeModeAffichage;
  }

  public void setTypeModeAffichage(String typeModeAffichage) {
    this.typeModeAffichage = typeModeAffichage;
  }

  public String getDivAjax() {
    return divAjax;
  }

  public void setDivAjax(String divAjax) {
    this.divAjax = divAjax;
  }

  public String getStyleCss() {
    return styleCss;
  }

  public void setStyleCss(String styleCss) {
    this.styleCss = styleCss;
  }

  public Long getNiveauOnglet() {
    return niveauOnglet;
  }

  public void setNiveauOnglet(Long niveauOnglet) {
    this.niveauOnglet = niveauOnglet;
  }

  /**
   * Obtenir la liste des roles autorisés pour un objet java.
   * 
   * @return la liste des roles autorisés
   */
  public List<RoleObjetJava> getListeRoleObjetJavaAutorise() {
    return listeRoleObjetJavaAutorise;
  }

  /**
   * Fixer la liste des roles autorisés pour un objet java.
   * 
   * @param listeObjetJavaEnfants
   *          la liste des roles autorisés pour un objet java.
   */
  public void setListeRoleObjetJavaAutorise(List<RoleObjetJava> listeRoleObjetJavaAutorise) {
    this.listeRoleObjetJavaAutorise = listeRoleObjetJavaAutorise;
  }

  public List<ObjetJava> getListeObjetJavaEnfant() {
    return listeObjetJavaEnfant;
  }

  public void setListeObjetJavaEnfant(List<ObjetJava> listeObjetJavaEnfant) {
    this.listeObjetJavaEnfant = listeObjetJavaEnfant;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public Long getId() {
    return id;
  }

  public void setIdParent(Long idParent) {
    this.idParent = idParent;
  }

  public Long getIdParent() {
    return idParent;
  }

  public void setObjetJavaParent(ObjetJava objetJavaParent) {
    this.objetJavaParent = objetJavaParent;
  }

  public ObjetJava getObjetJavaParent() {
    return objetJavaParent;
  }

  public String getVersionLivraison() {
    return versionLivraison;
  }

  public void setVersionLivraison(String versionLivraison) {
    this.versionLivraison = versionLivraison;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + (id == null ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!super.equals(obj)) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ObjetJava other = (ObjetJava) obj;
    if (id == null) {
      if (other.id != null) {
        return false;
      }
    } else if (!id.equals(other.id)) {
      return false;
    }
    return true;
  }

  @Override
  public String getCreePar() {
    return creePar;
  }

  @Override
  public void setCreePar(String creePar) {
    this.creePar = creePar;
  }

  @Override
  public Date getDateCreation() {
    return dateCreation;
  }

  @Override
  public void setDateCreation(Date dateCreation) {
    this.dateCreation = dateCreation;
  }

  @Override
  public String getModifiePar() {
    return modifiePar;
  }

  @Override
  public void setModifiePar(String modifiePar) {
    this.modifiePar = modifiePar;
  }

  @Override
  public Date getDateModification() {
    return dateModification;
  }

  @Override
  public void setDateModification(Date dateModification) {
    this.dateModification = dateModification;
  }

  public List<RoleObjetJava> getListeCodeRole() {
    return listeCodeRole;
  }

  public void setListeCodeRole(List<RoleObjetJava> listeCodeRole) {
    this.listeCodeRole = listeCodeRole;
  }
}