/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.quartz.Calendar;
import org.sofiframework.infrasofi.modele.commun.QuartzEntite;

public class QuartzCalendrier extends QuartzEntite {

  private static final long serialVersionUID = 3020944330870438183L;

  private Long schedulerId;
  private String nom;
  private byte[] calendrierData;

  private QuartzScheduler scheduler;

  /**
   * Par défaut pour Hibernate
   */
  public QuartzCalendrier() {
    super();
  }

  public QuartzCalendrier(Long schedulerId, String nom) {
    this.schedulerId = schedulerId;
    this.nom = nom;
  }

  public QuartzCalendrier(Long schedulerId, String nom, Calendar calendar) {
    this(schedulerId, nom);
    this.calendrierData = serializeObject(calendar).toByteArray();
  }

  public String getNom() {
    return nom;
  }
  public void setNom(String nom) {
    this.nom = nom;
  }
  public byte[] getCalendrierData() {
    return calendrierData;
  }
  public void setCalendrierData(byte[] calendrierData) {
    this.calendrierData = calendrierData;
  }

  @Override
  public Object getQuartzObject(Boolean useProperties){
    return getObjectFromBlob(calendrierData);
  }

  public Long getSchedulerId() {
    return schedulerId;
  }

  public void setSchedulerId(Long schedulerId) {
    this.schedulerId = schedulerId;
  }

  public QuartzScheduler getScheduler() {
    return scheduler;
  }

  public void setScheduler(QuartzScheduler scheduler) {
    this.scheduler = scheduler;
  }
}
