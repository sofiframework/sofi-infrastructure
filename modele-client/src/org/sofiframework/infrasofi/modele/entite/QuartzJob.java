/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.StatefulJob;
import org.sofiframework.application.tacheplanifiee.JobDetailImpl;
import org.sofiframework.application.tacheplanifiee.job.TacheStateful.ParametreTacheStateful;
import org.sofiframework.application.tacheplanifiee.utils.StatistiqueTache;
import org.sofiframework.infrasofi.modele.commun.QuartzEntite;

public class QuartzJob extends QuartzEntite {

  private static final long serialVersionUID = 1567734209021605003L;

  private static final Log log = LogFactory.getLog(QuartzJob.class);

  private Long schedulerId;
  private String nom, groupe, description, nomClasse;
  private Boolean jobDurable, jobVolatile, jobStateful, jobRequestRecovery;
  private byte[] jobData;
  private Long nbrTriggerActif;
  private QuartzScheduler scheduler;

  /**
   * Par défaut pour Hibernate
   */
  public QuartzJob() {
    super();
  }

  public QuartzJob(Long schedulerId, JobDetail job, Boolean useProperties) {
    this.schedulerId = schedulerId;

    if (job != null) {
      this.initialiserProrpietes(job, useProperties);
    }
  }

  public QuartzJob(Long schedulerId, String nom, String groupe) {
    this.schedulerId = schedulerId;
    this.nom = nom;
    this.groupe = groupe;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getGroupe() {
    return groupe;
  }

  public void setGroupe(String groupe) {
    this.groupe = groupe;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getNomClasse() {
    return nomClasse;
  }

  public void setNomClasse(String nomClasse) {
    this.nomClasse = nomClasse;
  }

  public Boolean getJobDurable() {
    return jobDurable;
  }

  public void setJobDurable(Boolean jobDurable) {
    this.jobDurable = jobDurable;
  }

  public Boolean getJobVolatile() {
    return jobVolatile;
  }

  public void setJobVolatile(Boolean jobVolatile) {
    this.jobVolatile = jobVolatile;
  }

  public Boolean getJobStateful() {
    return jobStateful;
  }

  public void setJobStateful(Boolean jobStateful) {
    this.jobStateful = jobStateful;
  }

  public Boolean getJobRequestRecovery() {
    return jobRequestRecovery;
  }

  public void setJobRequestRecovery(Boolean jobRequestRecovery) {
    this.jobRequestRecovery = jobRequestRecovery;
  }

  public byte[] getJobData() {
    return jobData;
  }

  public void setJobData(byte[] jobData) {
    this.jobData = jobData;
  }

  @Override
  @SuppressWarnings("rawtypes")
  public Object getQuartzObject(Boolean useProperties) {

    JobDetailImpl result = new JobDetailImpl();

    result.setDescription(this.description);
    result.setDurability(this.jobDurable);
    result.setGroup(this.groupe);

    try {
      /*
       * Correction bug (Mercier Rémi 20110720) On instancie pas la classe ici
       * car elle se trouve sur l'application cliente ; elle sera instanciée
       * dans la méthode DriverDelegateImpl.selectJobDetail
       */
      // ClassLoader classeLoader = getClass().getClassLoader();
      // result.setJobClass(classeLoader.loadClass(this.nomClasse));
      result.setNomClasse(this.nomClasse);
      /* Fin correction bug (Mercier Rémi 20110720) */

      result.setName(this.nom);
      result.setRequestsRecovery(this.jobRequestRecovery);
      result.setVolatility(this.jobVolatile);

      Map map = getMapObject(this.jobData, useProperties);

      if (map != null) {
        result.setJobDataMap(new JobDataMap(map));
      }
    } catch (Exception e) {
      result = new JobDetailImpl();
      e.printStackTrace();
    }
    return result;
  }

  @SuppressWarnings("rawtypes")
  public Map getJobDataInMap() {
    Map result = null;
    if (this.jobData != null) {
      result = this.getMapObject(this.jobData, false);
    } else {
      result = new JobDataMap();
    }
    return result;
  }

  @SuppressWarnings("rawtypes")
  public void setJobDatabyMap(Map map) {
    if (map == null) {
      map = new JobDataMap();
    }

    this.jobData = this.serializeObject(map).toByteArray();
  }

  public Boolean getActif() {
    return nbrTriggerActif != null && nbrTriggerActif > 0;
  }

  public Long getNbrTriggerActif() {
    return nbrTriggerActif;
  }

  public void setNbrTriggerActif(Long nbrTriggerActif) {
    this.nbrTriggerActif = nbrTriggerActif;
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  public Long getTempDerniereExecution() {
    Long result = 0L;
    Map map = getJobDataInMap();
    if (map.containsKey(ParametreTacheStateful.LISTE_DUREE_EXECUTION.getCle())) {
      List<StatistiqueTache> executions = (List<StatistiqueTache>) map
          .get(ParametreTacheStateful.LISTE_DUREE_EXECUTION.getCle());
      if (executions != null && executions.size() > 0) {
        result = executions.get(0).getTempsExecution();
      }
    }
    return result;
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  public Long getTempMoyenExecution() {
    Long result = 0L;
    Map map = getJobDataInMap();
    if (map.containsKey(ParametreTacheStateful.LISTE_DUREE_EXECUTION.getCle())) {
      List<StatistiqueTache> executions = (List<StatistiqueTache>) map
          .get(ParametreTacheStateful.LISTE_DUREE_EXECUTION.getCle());
      if (executions != null && executions.size() > 0) {
        for (StatistiqueTache temps : executions) {
          result += temps.getTempsExecution();
        }
        result = result / executions.size();
      }
    }
    return result;
  }

  @SuppressWarnings("rawtypes")
  public boolean isJobPermanente() {
    boolean result = false;
    try {
      if (nomClasse != null) {
        Class classe = Class.forName(nomClasse);
        if (StatefulJob.class.isAssignableFrom(classe)) {
          result = true;
        }
      }
    } catch (ClassNotFoundException e) {
      result = false;
    }

    return result;
  }

  public Long getSchedulerId() {
    return schedulerId;
  }

  public void setSchedulerId(Long schedulerId) {
    this.schedulerId = schedulerId;
  }

  public QuartzScheduler getScheduler() {
    return scheduler;
  }

  public void setScheduler(QuartzScheduler scheduler) {
    this.scheduler = scheduler;
  }

  /**
   * Permet d'initialiser les propriétés de l'entité avec l'objet Quartz
   * {@link JobDetail}
   * 
   * @param job
   *          un objet de type {@link JobDetail}
   * @param useProperties
   * @exception IllegalStateException
   *              si le nom de la classe associée au job ne peut pas être
   *              déterminé.
   */
  public void initialiserProrpietes(JobDetail job, Boolean useProperties) {
    this.nom = job.getName();
    this.groupe = job.getGroup();
    this.description = job.getDescription();
    /*
     * Correction bug (Mercier Rémi 20110720) Si l'instance est du type alors le
     * nom de la classe doit être récupéré depuis la propriété nomClasse. Il ne
     * peut pas l'être depuis jobClass car l'infra n'a pas connaissance de la
     * classe qui devra réaliser le traitement (sur l'application cliente).
     */
    String nomClasse = null;
    if (job instanceof JobDetailImpl) {
      nomClasse = ((JobDetailImpl) job).getNomClasse();
    } else {
      // On tente de récupérer le nom de la classe ; si c'est un job
      // standard (fourni par Quartz ou Sofi) l'infra en aura connaissance et ça
      // sera OK.
      // Sinon, on lève une exception.
      try {
        nomClasse = job.getJobClass().getName();
      } catch (Exception ex) {
        if (log.isWarnEnabled()) {
          log.warn(
              "Impossible de récupérer le nom de classe à partir de la propriété jobClass.",
              ex);
        }
      }
    }
    if (nomClasse == null) {
      throw new IllegalStateException(
          "Un objet QuartzJob ne peut pas être créé sans nom de classe ; "
              + "utiliser la classe JobDetailImpl (propriété nomClasse) plutôt que JobDetail pour définir "
              + "le nom de la classe rattachée au job.");
    } else {
      this.nomClasse = nomClasse;
    }
    /* Fin correction bug (Mercier Rémi 20110720) */

    this.jobDurable = job.isDurable();
    this.jobVolatile = job.isVolatile();
    this.jobStateful = job.isStateful();
    this.jobRequestRecovery = job.requestsRecovery();

    this.jobData = serializeJobData(job.getJobDataMap(), useProperties)
        .toByteArray();
  }

}
