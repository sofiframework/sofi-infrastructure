/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.quartz.SimpleTrigger;

public class QuartzSimpleTrigger extends QuartzTypeTrigger {

  private static final long serialVersionUID = -8026885738719572973L;

  private Long nombreExecution;
  private Long interval;
  private Long nombreFoisExecute = 0L;

  public QuartzSimpleTrigger() {
  }

  public QuartzSimpleTrigger(Long declencheurId, SimpleTrigger trigger) {
    super(declencheurId);
    this.nombreExecution = new Long(trigger.getRepeatCount());
    this.interval = trigger.getRepeatInterval();
    this.nombreFoisExecute = new Long(trigger.getTimesTriggered());
  }

  public Long getNombreExecution() {
    return nombreExecution;
  }

  public void setNombreExecution(Long nombreExecution) {
    this.nombreExecution = nombreExecution;
  }

  public Long getInterval() {
    return interval;
  }

  public void setInterval(Long interval) {
    this.interval = interval;
  }

  public Long getNombreFoisExecute() {
    return nombreFoisExecute;
  }

  public void setNombreFoisExecute(Long nombreFoisExecute) {
    this.nombreFoisExecute = nombreFoisExecute;
  }

  /*
   * (non-Javadoc)
   * @see com.nurun.infrasofi.modele.entite.QuartzEntite#getQuartzObject(java.lang.Boolean)
   */
  @Override
  public Object getQuartzObject(Boolean useProperties){
    SimpleTrigger simpleTrigger = (SimpleTrigger) getTrigger().getQuartzObject(useProperties);
    simpleTrigger.setRepeatCount(nombreExecution.intValue());
    simpleTrigger.setRepeatInterval(interval);
    return simpleTrigger;
  }

}
