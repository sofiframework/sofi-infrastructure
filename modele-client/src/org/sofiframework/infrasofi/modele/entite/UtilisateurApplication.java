/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.entite;

import org.sofiframework.modele.entite.EntiteTrace;

/**
 * Information relative au rôle administratif joué par l'utilisateur dans
 * l'application. On y associée aussi un code statut qui permet de désactiver
 * l'accès d'un utilisateur à une application.
 * 
 * Exemple : Pour l'application X, mr Y est consultant externe.
 * 
 * @author Jean-Maxime Pelletier
 */
public class UtilisateurApplication extends EntiteTrace {

  private static final long serialVersionUID = -5218816252499659414L;

  private String codeUtilisateur;

  private String codeApplication;

  private String codeTypeUtilisateur;

  private String codeStatut;

  private String codeTypeOrganisation;

  private String codeOrganisation;

  public UtilisateurApplication() {
    this.setCle(new String[] { "codeUtilisateur", "codeApplication" });
  }

  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeTypeUtilisateur(String codeTypeUtilisateur) {
    this.codeTypeUtilisateur = codeTypeUtilisateur;
  }

  public String getCodeTypeUtilisateur() {
    return codeTypeUtilisateur;
  }

  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  public String getCodeStatut() {
    return codeStatut;
  }

  public void setCodeTypeOrganisation(String codeTypeOrganisation) {
    this.codeTypeOrganisation = codeTypeOrganisation;
  }

  public String getCodeTypeOrganisation() {
    return codeTypeOrganisation;
  }

  public void setCodeOrganisation(String codeOrganisation) {
    this.codeOrganisation = codeOrganisation;
  }

  public String getCodeOrganisation() {
    return codeOrganisation;
  }
}
