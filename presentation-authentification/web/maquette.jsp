<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-tiles" prefix="sofi-tiles"%>

<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>

<html:html xhtml="true">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <tiles:importAttribute name="titreApplication"/>
    <tiles:importAttribute name="titre" ignore="true"/>
    <sofi:libelle var="titreComplet" identifiant="${titreApplication}"/>
    <c:if test="${titre != null}">
      <sofi:libelle var="libellePage" identifiant="${titre}"/>
      <c:set var="titreComplet" value="${titreComplet} | ${libellePage}"/>
    </c:if>
    <title><c:out value="${titreComplet}" /></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <sofi:css href="css/fonts/Sansation-fontfacekit/stylesheet.css"></sofi:css>
    <sofi:css href="wro/authentification.css"></sofi:css>
    
    <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 960px)" href="css/mobile.css" />
    <link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 480px)" href="css/mobile.css" />
    
    <!--[if IE]><sofi:css href="css/ie.css" /><![endif]-->
    <!--[if IE 7]><sofi:css href="css/ie7.css" /><![endif]-->
    <sofi:javascript src="wro/authentification.js"></sofi:javascript>
  </head>
  <body>
    <div id="enrobage">
	    <div id="frame">
	      <div id="bandeau">
	        <tiles:insert attribute="entete"/>
	        <app:listeLangueSupportee libelle="login.libelle.commun.liste_langue" styleClass="listeLangue" />
	      </div> 
        <h1><sofi:libelle identifiant="${titre}"/></h1>  
	      <div id="content">
	        <tiles:insert attribute="detail"/>
	      </div> 
	    </div>
    </div>
    <div id="footerBox">
      <tiles:insert attribute="bas_page"/>
    </div>
  </body>
  <sofi:javascript appliquerOnLoad="true" />
</html:html>