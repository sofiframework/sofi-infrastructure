
<%@tag import="org.sofiframework.application.cache.ObjetCache"%>
<%@tag import="org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur"%>
<%@tag import="org.sofiframework.application.cache.GestionCache"%>
<%@tag import="java.util.List"%>
<%@tag import="org.sofiframework.modele.spring.application.ConfigurationServices"%>
<%@tag import="org.springframework.context.ApplicationContext"%>
<%@tag import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@tag import="org.springframework.web.context.WebApplicationContext"%><%@ tag body-content="scriptless" %>
<%@tag import="org.sofiframework.presentation.struts.controleur.UtilitaireControleur"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles"  prefix="tiles"%>

<%@ attribute name="libelle" required="true" type="java.lang.String"%>
<%@ attribute name="styleClass" required="true" type="java.lang.String"%>

<c:set var="langueUtilisateur" 
  value="<%=UtilitaireControleur.getLanguePourUtilisateur(session)%>" />

<div class="${styleClass}">
<sofi-html:select libelle="${libelle}"
  cache="listeLocale" value="${langueUtilisateur}" 
  onchange="changerLocale($(this).val());" />
</div>