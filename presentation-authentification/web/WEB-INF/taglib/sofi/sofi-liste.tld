<?xml version="1.0" encoding="ISO-8859-1" ?>

<!DOCTYPE taglib  
  PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.2//EN"  
    "http://java.sun.com/dtd/web-jsptaglibrary_1_2.dtd">

<taglib>
  <!-- ============== Description de la librairie des balises JSP ============= -->
  <tlib-version>1.0</tlib-version>
  <jsp-version>1.2</jsp-version>
  <short-name>sofi-liste</short-name>
  <uri>http://www.sofiframework.org/taglib/sofi-liste</uri>
  <description>Librairie des balises permettant l'utilisation de la liste
de navigation de SOFI.</description>
  <tag>
    <name>listeNavigation</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.ListeNavigationTag</tag-class>
    <body-content>JSP</body-content>
    <display-name>listeNavigation</display-name>
    <description>
    Balise permettant de traiter un liste de navigation page par page
    avec tri automatis� et plusieurs fonctionnalit� ajax.
    </description>    
    <attribute>
      <name>nomListe</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Sp�cifier le nom de la liste navigation qui accessible dans une port�e
        request ou session. Utiliser les expressions r�guli�re par exemple:
        ${listeClients}
      </description>
    </attribute>
    <attribute>
      <name>divId</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Identifiant du DIV encapsulant la liste de navigation. On doit sp�cifier un nom lorsqu'on d�sire
        r�f�rencer un traitement vers la liste de navigation, par exemple lorsqu'on d�sire d�placer la page vers la navigation
        apr�s une soumission de formulaire ex : action="/rechercheUtilisateur.do?methode=rechercher#LISTE_RECHERCHE_UTILISATEUR".
        Lors de l'utilisation de JSP 1.x, le nom du div de la liste est automatique le nom de la liste de navigation fix� par la
        propri�t� nomListe.
      </description>
    </attribute>  
    <attribute>
      <name>colgroup</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de d�couper les colonnes de la liste par groupe.
        Par exemple: 20%,60%,20% en pourcentage, 150,300,150 en pixels,
        s'il y un groupe de tailles �gales cons�cutives vous pouvez
        utiliser: 3-20%, 40%, dans cet exemple il y a 3 colonnes de 20%
        et une de 40%
      </description>
    </attribute>   
    <attribute>
      <name>decorateur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet d'appliquer un d�corateur pour la liste navigation.
      </description>
    </attribute>
    <attribute>
      <name>action</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        URL � laquelle ce formulaire sera soumis. Permet �galement de choisir
        l'ActionMapping, qui permet d'obtenir le bean associ� au formulaire.
      </description>
    </attribute>
    <attribute>
      <name>class</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer une feuille de style.</description>
    </attribute>
    <attribute>
      <name>id</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Nom de l'instance de l'occurence de la liste en traitement.
      </description>
    </attribute>
    <attribute>
      <name>trierPageSeulement</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Tri les occurences de la page seulement. D�faut est false.
      </description>
    </attribute>
    <attribute>
      <name>triDefaut</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>int</type>
      <description>
        Le num�ro de colonne dont le tri par d�faut est fait.
        La base est 0, 1, 2,...
        IMPORTANT : Depuis SOFI 1.8.2, le tri par d�faut des composants ListeNavigation se
        fait via la m�thode ajouterTriInitial(nomAttributAtrier, ascendant);. Cette propri�t�
        demeure utilis� lors de l'utilisation de collection.
      </description>
    </attribute>
    <attribute>
      <name>triDescendantDefaut</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Appliquer un tri descendant par d�faut. Si non sp�cifier le tri
        sera ascendant par d�faut.
        IMPORTANT : Depuis SOFI 1.8.2, le tri par d�faut des composants ListeNavigation se
        fait via la m�thode ajouterTriInitial(nomAttributAtrier, ascendant);. Cette propri�t�
        demeure utilis� lors de l'utilisation de collection.
      </description>
    </attribute>
    <attribute>
      <name>selection</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Sp�cifier les attributs qui sp�cifie unicit� d'une s�lection dans la
        liste. Souvent on sp�cifie l'attribut qui sp�cifie la cl� d'un objet de transfert.
	Par exemple: codeUtilisateur. Si plusieurs veuillez les s�parer les attributs par une virgule.
      </description>
    </attribute>  
    <attribute>
      <name>colonneLienDefaut</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Sp�cifier le num�ro de colonne qui correspondera au lien par d�faut
        lors de la s�lection d'une ligne de la liste.
      </description>
    </attribute>  
    <attribute>
      <name>fonctionRetourValeur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Fixer � true si vous d�sirez la g�n�ration automatique du retour des
        valeurs � la page parent. (Utile seulement pour les listes de
        valeurs).
      </description>
    </attribute>
    <attribute>
      <name>fonctionRetourValeurAjax</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Fixer � true si vous d�sirez la liste de navigation doit retourner des valeurs 
        � une liste de valeur configur�e Ajax.
      </description>
    </attribute>    
    <attribute>
      <name>messageListeVide</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Fixer un message sp�cifique pour la liste lorsque
        le resultat est vide.
      </description>
    </attribute>  
    <attribute>
      <name>afficheEnteteSiListeVide</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>Fixer true si on veut l'entete et la naviguation m�me si la liste de navigation est vide.</description>
    </attribute>    
    <attribute>
      <name>ajax</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Fixer � true si vous d�sirez que la liste soit Ajax.
      </description>
    </attribute> 
    <attribute>
      <name>afficherNavigationBasListe</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
       <description>
        Fixer � true si vous d�sirez que la barre de navigation soit aussi
        afficher en bas de la liste.
      </description>
    </attribute> 
    <attribute>
      <name>iterateurMultiPage</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
       <description>
        Fixer true si vous d�sirez avoir un it�rateur multi page.
      </description>
    </attribute> 
    <attribute>
      <name>nombreMaximalMultiPage</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
       <description>
        Fixer le nombre maximal de page afficher dans l'it�rateur.
      </description>
    </attribute>
    <attribute>
      <name>gabaritBarreNavigation</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
       <description>
        Fixer la variable qui loge le gabarit de la barre de navigation.
      </description>
    </attribute> 
    <attribute>
      <name>gabaritBarreNavigationBasListe</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
       <description>
        Fixer la variable qui loge le gabarit de la barre de navigation
        en bas de la liste.
      </description>
    </attribute> 
    <attribute>
      <name>libelleResultat</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
       <description>
        Fixer le libell� du r�sultat de la liste de navigation.
      </description>
    </attribute> 
    <attribute>
      <name>varIndex</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Fixer le nom de variable qui va contenir l'index d'it�ration
        de la liste. L'index d�bute � 1.
      </description>
    </attribute>
    <example>../exemples/exemple_sofi-liste_listeNavigation.html</example>
  </tag>
  <tag>
    <name>colonne</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.ColonneTag</tag-class>
    <body-content>JSP</body-content>
    <display-name>colonne</display-name>
    <description>
    Balise permettant de traiter une colonne d'une liste de navigation page par page
    avec tri automatis�.
    </description>      
    <attribute>
      <name>property</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Nom d'attribut qui est contenu dans la colonne de la liste. Doit �tre
        accessible depuis une instance de la liste.
      </description>
    </attribute>
    <attribute>
      <name>paramId</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Les identifiants des param�tres de url appelant la liste de valeurs.
        Si plusieurs identifiants, s�parer les d'une virgule.
        
        Ex. noOrgns, noLot
      </description>
    </attribute>
    <attribute>
      <name>paramProperty</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
      	Propri�t� du bean.
        
        La suite de toutes les valeurs des propri�t�s � passer dans l'URL.
      
        Les propri�t�s des param�tres de url appelant la liste de valeurs.
        Si plusieurs propri�t�s, s�parer les d'une virgule.
        
        La propri�t� de l'objet de transfert qui est la valeur du param�tre.
        
        La propri�t� du bean qui contient la valeur du ou des param�tres.
        Ex. noOrganisme, noLot
        
	IMPORTANT: Il est pr�f�rable de concevoir un URL avec l'aide de JSTL
	avec les balises c:url et c:param., vous pouvez ensuite ref�rez l'url sp�cifier 
	dans un var et placer dans la propri�t� href de la balise. Ex. href="${listeXXX}".
      </description>
    </attribute>    
    <attribute>
      <name>paramName</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Correspond � la valeur de l'attribut s�lectionn� pour cr�er un
        lien hypertexte. Vous pouvez utiliser les expressions r�guli�re Ex.
        ${client.codeUtilisateur} accessible dans la requ�te ou la session.
        
	IMPORTANT: Il est pr�f�rable de concevoir un URL avec l'aide de JSTL
	avec les balises c:url et c:param., vous pouvez ensuite ref�rez l'url sp�cifier 
	dans un var et placer dans la propri�t� href de la balise. Ex. href="${listeXXX}".
      </description>
    </attribute>
    <attribute>
      <name>fonction</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Le nom de la fonction javascript qui doit �tre appel� lors de la
        s�lection d'un �l�ment de la liste. � utiliser seulement lors de
        l'utilisation de la liste dans une fen�tre flottante.
      </description>
    </attribute>
    <attribute>
      <name>parametresFonction</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Sp�cifier les attributs dont les valeurs doivent �tre envoy� �
        la fonction sp�cifier. Les attributs doivent �tre s�parer de virgule,
        EX: nom,prenom. � utiliser seulement dans une fen�tre flottante.
      </description>
    </attribute>
    <attribute>
      <name>libelle</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier le nom de la colonne, vous pouvez sp�cifier une cl�
      </description>
    </attribute>
    <attribute>
      <name>aideContextuelle</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier l'aide contextuelle de la colonne, vous pouvez
        sp�cifier une cl�
      </description>
    </attribute>
    <attribute>
      <name>aideContextuelleCellule</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier l'aide contextuelle pour une cellule sp�cifique.
      </description>
    </attribute>     
    <attribute>
      <name>trier</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Est-ce que vous d�sirez trier cette colonne. (true/false)
      </description>
    </attribute>
    <attribute>
      <name>lienHypertexteAutomatique</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Est-ce que vous d�sirez un lien hypertexte automatique. (true/false)
      </description>
    </attribute>
    <attribute>
      <name>href</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        L'URL de base pour la construction dynamique du lien hypertexte de la
        ressource � appeler lors du click sur le lien.
      </description>
    </attribute>
    <attribute>
      <name>class</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer une feuille de style.</description>
    </attribute>
    <attribute>
      <name>decorateur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer un d�corateur pour une colonne.</description>
    </attribute>
    <attribute>
      <name>fonctionRetourValeur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Fixer � true si vous d�sirez que cette colonne utilise la fonction JS
        automatique du retour des valeurs � la page parent. (Utile seulement
        pour les listes de valeurs).
      </description>
    </attribute> 
    <attribute>
      <name>fonctionRetourValeurAjax</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Fixer � true si vous d�sirez la liste de navigation doit retourner des valeurs 
        � une liste de valeur configur�e Ajax.
      </description>
    </attribute>        
    <attribute>
      <name>format</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier un format si la colonne est un nombre.
        Ex. ###.#0
      </description>
    </attribute>  
    <attribute>
      <name>cache</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet d'extraire la valeur associ� � une liste d'un objet cache.
      </description>
    </attribute>  
    <attribute>
      <name>traiterCorps</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet d'�valuer l'int�rieur de la balise avec du contenu
        personnalis�.
        
	D�faut : false
      </description>
    </attribute>
    <attribute>
      <name>classCellule</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier une classe CSS sp�cifique pour une cellule.
      </description>
    </attribute>  
    <attribute>
      <name>convertirEnHtml</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Sp�cifie si vous d�sirez la conversion en format HTML. D�faut : true
      </description>
    </attribute>    
    <attribute>
      <name>sautLigneNombreCararactereMaximal</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Sp�cifie le nombre de caract�re maximal avant de forcer un saut de ligne.
      </description>
    </attribute> 
    <attribute>
      <name>sautLigneCararactereSeparateur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Sp�cifie le caract�re qui est utilis� afin d'appliquer un saut de ligne.
      </description>
    </attribute>      
    <example>../exemples/exemple_sofi-liste_colonne.html</example>
  </tag>
  <tag>
    <name>colonneMultibox</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.ColonneMultiboxTag</tag-class>
    <body-content>JSP</body-content>
    <display-name>colonneMultibox</display-name>
    <description>
    Balise permettant de traiter une colonne d�corant une boite � cocher
    d'une liste de navigation page par page.
    </description>       
    <attribute>
      <name>nomAttributMultibox</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>Sp�cifier le nom de la boite � cocher multiple.</description>
    </attribute>
    <attribute>
      <name>nomValeurMultibox</name>
      <required>true</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Sp�cifier le nom d'attribut qui sera la valeur de la boite
        � cocher multiple.
      </description>
    </attribute> 
    <attribute>
      <name>nomListeSelectionnee</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Sp�cifier le nom de la liste contenant les �l�ments de la boite �
        cocher multiple qui sont s�lectionn�. Doit �tre un HashMap.
      </description>
    </attribute>
    <attribute>
      <name>proprieteActivation</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>java.lang.String</type>
      <description>
        Sp�cifier le nom de m�thode de l'objet de transfert qui permet de
        sp�cifier si la boite � cocher est actif ou pas.
      </description>
    </attribute> 
    <attribute>
      <name>href</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        L'URL de base pour la construction dynamique du lien hypertexte de la
        ressource � appeler lors du click sur une des cases � cocher.
      </description>
    </attribute>
    <attribute>
      <name>paramId</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Les identifiants des param�tres de l'url appelant la liste de valeurs.
        Si plusieurs identifiants, s�parer les d'une virgule.
        
        Ex. noOrgns, noLot
      </description>
    </attribute>
    <attribute>
      <name>paramProperty</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
      	Propri�t� du bean.
        
        La suite de toutes les valeurs des propri�t�s � passer dans l'URL.
      
        Les propri�t�s des param�tres de url appelant la liste de valeurs.
        Si plusieurs propri�t�s, s�parer les d'une virgule.
        
        La propri�t� de l'objet de transfert qui est la valeur du param�tre.
        
        La propri�t� du bean qui contient la valeur du ou des param�tres.
        Ex. noOrganisme, noLot
        
	IMPORTANT: Il est pr�f�rable de concevoir un URL avec l'aide de JSTL
	avec les balises c:url et c:param., vous pouvez ensuite ref�rez l'url sp�cifier 
	dans un var et placer dans la propri�t� href de la balise. Ex. href="${listeXXX}".
      </description>
    </attribute>    
    <attribute>
      <name>libelle</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier le nom de la colonne, vous pouvez sp�cifier une cl�
      </description>
    </attribute>
    <attribute>
      <name>aideContextuelle</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier l'aide contextuelle de la colonne, vous pouvez
        sp�cifier une cl�
      </description>
    </attribute>
    <attribute>
      <name>aideContextuelleCellule</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier l'aide contextuelle pour une cellule sp�cifique.
      </description>
    </attribute>     
    <attribute>
      <name>class</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer une feuille de style.</description>
    </attribute>
    <attribute>
      <name>decorateur</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer un d�corateur pour une colonne.</description>
    </attribute>
    <attribute>
      <name>onClick</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier du code JavaScript sur la modification de la boite
        � cocher.
      </description>
    </attribute>
    <example>../exemples/exemple_sofi-liste_colonneMultibox.html</example>
  </tag>
  <tag>
    <name>colonneLibre</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.ColonneTag</tag-class>
    <body-content>JSP</body-content>
    <display-name>colonneLibre</display-name>
    <description>
      Balise permettant de traiter une colonne avec une valeur arbitraire
      d�finit dans le body du tag.
    </description>
    <attribute>
      <name>libelle</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Permet de sp�cifier le nom de la colonne, vous pouvez sp�cifier une cl�
      </description>
    </attribute>    
    <attribute>
      <name>class</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>Permet d'appliquer une classe CSS pour l'entete de la colonne.</description>
    </attribute>
    <attribute>
      <name>trier</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Est-ce que vous d�sirez trier cette colonne. (true/false)
      </description>
    </attribute>
    <attribute>
      <name>classCellule</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier une classe CSS sp�cifique pour une cellule.
      </description>
    </attribute>  
    <attribute>
      <name>convertirEnHtml</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <type>boolean</type>
      <description>
        Sp�cifie si vous d�sirez la conversion en format HTML. D�faut : true
      </description>
    </attribute>    
  </tag>
  <tag>
    <name>entete</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.EnteteTag</tag-class>
    <body-content>JSP</body-content>
    <display-name>colonneLibre</display-name>
    <description>
      Balise permettant de cr�er une ent�te personnalis� � la liste de
      navigation.
    </description>
    <attribute>
      <name>var</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Permet de sp�cifier le nom de la variable qui loge l'entete, cette
        variable aura la port� session.
      </description>
    </attribute>   
    <attribute>
      <name>gabaritBasListe</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Permet de sp�cifier si le gabarit de l'entete est pour le bas de
        la liste.
	D�faut : true
      </description>
    </attribute>   
  </tag>
  <tag>
    <name>utilitaire</name>
    <tag-class>org.sofiframework.presentation.balisesjsp.listenavigation.UtilitaireListeTag</tag-class>
    <body-content>empty</body-content>
    <display-name>utilitaire</display-name>
    <description>
      Balise qui permet d'offrir des m�thodes utilitaire sur des listes de type
      List ou ListeNavigation.
    </description>
    <attribute>
      <name>var</name>
      <required>false</required>
      <rtexprvalue>false</rtexprvalue>
      <description>
        Permet de sp�cifier le nom de la variable qui loge le r�sultat de
        la balise, cette variable aura la port� request.
      </description>
    </attribute>   
    <attribute>
      <name>liste</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier la liste a traiter, vous pouvez sp�cifier un nom
        de liste de navigation ou de tout autre liste de type List.
      </description>
    </attribute> 
    <attribute>
      <name>position</name>
      <required>false</required>
      <rtexprvalue>true</rtexprvalue>
      <description>
        Permet de sp�cifier la position de l'instance dont on d�sire extraire
        et placer dans un var.
      </description>
    </attribute>       
  </tag>  
</taglib>
