<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<div class="box">
	 <p>
	  <img src="images/authentification/logo-sofi-infrastructure.png" /><br/><br/>
	  <sofi:libelle identifiant = "login.libelle.inscription.explication_client" parametre1="${inscriptionForm.attributs.prenom}" parametre2="${inscriptionForm.attributs.nom}" />
	 </p>
	 <ul class="listeFleches">
	   <li class="last">
       <sofi-html:link href="index.html" libelle="login.libelle.commun.retour_authentification" />
     </li>
    </ul>
</div>