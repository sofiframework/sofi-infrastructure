<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<div class="box">
	<p>
	 <img src="images/authentification/logo-sofi-infrastructure.png" /><br/><br/>
	</p>
  <sofi:barreStatut afficherSeulementSiMessage="true" />
	<sofi-html:form action="/inscriptionClient.do?methode=enregistrer" transactionnel="false" focusAutomatique="true" >
		<fieldset>
		  <h3><sofi:libelle identifiant = "login.libelle.inscription.titre.selection_compte" /></h3>
				<ul class="formList">
			    <li class="clearfix">
						<sofi:libelle identifiant="login.libelle.inscription.champ.code_utilisateur" nomAttribut="codeUtilisateur" obligatoire="true"  />
						<sofi-html:text property="codeUtilisateur" obligatoire="true" size = "30"/>
				  </li>
				</ul>
        <ul class="formList">
          <li class="clearfix">
          <sofi:libelle identifiant="login.libelle.inscription.champ.mot_passe" nomAttribut="motPasse" obligatoire="true"  />
          <sofi-html:password  property="motPasse" obligatoire="true" size = "30" />
          </li>
        </ul>
        <ul class="formList">
          <li class="clearfix">
          <sofi:libelle identifiant="login.libelle.inscription.champ.confirmer_mot_passe" nomAttribut="motPasseConfirmation" obligatoire="true"  />
          <sofi-html:password  property="motPasseConfirmation" obligatoire="true"  size = "30"/>
          </li>
        </ul>
     </fieldset>
     <fieldset>
        <h3><sofi:libelle identifiant = "login.libelle.inscription.titre.selection_client" /></h3>
        <p>
          <sofi:libelle identifiant = "login.libelle.inscription.explication_client" />
        </p>
        <ul class="formList">
          <li class="clearfix">
            <sofi:libelle identifiant="Type d'organisation" nomAttribut="codeTypeOrganisation" obligatoire="true"  />
            <sofi-html:select   property="codeTypeOrganisation" 
                                ligneVide="true"
                                cache="listeTypeOrganisation" />
          </li>
        </ul>
        <ul class="formList">
          <li class="clearfix">
						<sofi:libelle identifiant="login.libelle.inscription.champ.code_client" nomAttribut="codeClient" obligatoire="true"  />
						<sofi-html:text property="codeClient" obligatoire="true" size = "30" maxlength="30"/>
          </li>
        </ul>
        <ul class="formList">
          <li class="clearfix">
            <sofi:libelle identifiant="login.libelle.inscription.champ.nom_client" nomAttribut="nomClient" obligatoire="true"  />
            <sofi-html:text property="nomClient" obligatoire="true" size = "50"/>
          </li>
        </ul>
      </fieldset>        
      <sofi-html:submit libelle="login.libelle.inscription.action.soumettre" />
  </sofi-html:form>
	<ul class="listeFleches">
     <li>
         <sofi-html:link href="inscription.do?methode=afficher" libelle="Retour à l'étape 1" />
     </li>
	   <li class="last">
	     <sofi-html:link href="index.html" libelle="login.libelle.commun.retour_authentification" />
	  </li>
	</ul>

</div>