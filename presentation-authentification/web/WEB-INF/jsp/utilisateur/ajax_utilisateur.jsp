<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<style>
  .dhtmlgoodies_windowContent {
    background-color:white;
  }
</style>
<div>
  <fieldset>
    <legend><sofi:libelle identifiant="auth_sofi.libelle.utilisateur.profil_utilisateur" /></legend>
    <table class="tableformulaire" border="0" cellpadding="0" cellspacing="0">
    <tbody>   
    <tr>
      <th scope="row"><sofi:libelle identifiant="auth_sofi.libelle.utilisateur.code_utilisateur" /></th>
      <td>${utilisateurForm.attributs.codeUtilisateur}</td>
    </tr>
    <tr>
      <th scope="row"><sofi:libelle identifiant="auth_sofi.libelle.utilisateur.prenom" /></th>
      <td>${utilisateurForm.attributs.prenom}</td>
    </tr>
    <tr>
      <th scope="row"><sofi:libelle identifiant="auth_sofi.libelle.utilisateur.nom" /></th>
      <td>${utilisateurForm.attributs.nom}</td>
    </tr>
    <tr>
      <th scope="row"><sofi:libelle identifiant="auth_sofi.libelle.utilisateur.courriel" /></th>
      <td><a href="mailto:${utilisateurForm.attributs.courriel}">${utilisateurForm.attributs.courriel}</a></td>
    </tr>
    </tbody>
    </table>
  </fieldset>
</div>