<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ taglib prefix="app-securite" tagdir="/WEB-INF/tags/securite" %>

<div class="box">
    <sofi-html:form action="/oublierMotPasse.do?methode=envoyerDemande" 
                    transactionnel="false" focusAutomatique="true"
                    styleClass="clearfix">
      <sofi:barreStatut afficherSeulementSiMessage="true" exclureMessageErreurDefaut="true" />
        <h2>Mot de passe oublié&nbsp;?</h2>
        <p>
          <sofi:libelle identifiant="login.libelle.oublier_mot_passe.explication" />
        </p>
        <ul class="formList">
          <li class="clearfix">
	          <sofi:libelle 
	            identifiant="login.libelle.oublier_mot_passe.code_utilisateur" 
	            nomAttribut="codeUtilisateur" />
	          <sofi-html:text 
	            property="codeUtilisateur" 
	            obligatoire="true"
	            size="30"
	            />
          </li>                          
        </ul>
        <div id="captcha">
          <sofi:libelle 
            identifiant="login.libelle.oublier_mot_passe.codeCaptcha" 
            nomAttribut="recaptcha_response_field" />
          <app-securite:captcha clePublique="6LdXEr0SAAAAAFBPYIb8wCur7qnVQIarjzbKhAdU" />
        </div>

    
      <p class="zoneBouton">
        <sofi-html:submit libelle="login.libelle.oublier_mot_passe.action.envoye" />
      </p>
    </sofi-html:form>
    <ul class="listeFleches">
      <li class="last">
        <sofi-html:link href="index.html" libelle="login.libelle.commun.retour_authentification" />
      </li>
     </ul>

</div>