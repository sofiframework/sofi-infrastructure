<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<div class="box">
	<p>
	 <img src="images/authentification/logo-sofi-infrastructure.png" /><br/><br/>
   <sofi:barreStatut afficherSeulementSiMessage="true" />
	 <sofi:libelle identifiant = "login.libelle.inscription.explication" />
	</p>
	
	<sofi-html:form styleClass="clearfix" action="/inscription.do?methode=enregistrer" transactionnel="false" focusAutomatique="true">
		<ul class="formList">
			<li class="clearfix">
				<sofi:libelle identifiant="login.libelle.inscription.champ.nom" nomAttribut="nom" obligatoire="true"  />
				<sofi-html:text property="nom" obligatoire="true" size = "30"/>
			</li>
		</ul>
	  <ul class="formList">
	    <li class="clearfix">
				<sofi:libelle identifiant="login.libelle.inscription.champ.prenom" nomAttribut="prenom" obligatoire="true"  />
			  <sofi-html:text  property="prenom" obligatoire="true" size = "30"/>
		  </li>
		</ul>
		<ul class="formList">
			<li class="clearfix">
				<sofi:libelle identifiant="login.libelle.inscription.champ.courriel" nomAttribut="courriel" obligatoire="true"  />
				<sofi-html:text  property="courriel" obligatoire="true" size = "30"/>
			</li>
		</ul>
		<ul class="formList">
			<li class="clearfix">
	      <sofi:libelle identifiant="login.libelle.inscription.champ.code_langue" nomAttribut="codeLangue" obligatoire="true"  />
	      <sofi-html:select cache="listeLocale" 
	                         property="langue" 
	                         obligatoire="true" />
	     </li>
	  </ul>        
	  <sofi-html:submit libelle="login.libelle.inscription.action.etape_suivante" />
	</sofi-html:form>

	<ul class="listeFleches">
	   <li class="last">
	     <sofi-html:link href="index.html" libelle="login.libelle.commun.retour_authentification" />
	  </li>
	</ul>

</div>