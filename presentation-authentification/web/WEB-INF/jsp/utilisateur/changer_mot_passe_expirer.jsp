<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<div class="box">
  <sofi-html:form action="/changerMotPasseExpirer.do?methode=changerMotPasse" transactionnel="false" focusAutomatique="true">
  <sofi:barreStatut afficherSeulementSiMessage="true" />
	<p>  
	 <c:choose>
	  <c:when test="${pwd_expired}">
			<div id="message_erreur">
				<div id="messageFormulaire" class="zoneMessageFormulaireSimple">
				  <span class="message_tout_type message_icone_avertissement">
				   <sofi:message identifiant="login.avertissement.commun.mot_passe_expire" />
				  </span>
				</div>
			  <div class="clear"></div>
			</div>
	  </c:when>
	  <c:when test="${new_pwd}">
	    <div id="message_erreur">
           <div id="messageFormulaire" class="zoneMessageFormulaireSimple">
             <span class="message_tout_type message_icone_avertissement">
               <sofi:message identifiant="login.avertissement.commun.nouveau_mot_passe_administrateur" />
             </span>
           </div>
           <div class="clear"></div>
         </div>
	  </c:when>
		</c:choose>
	</p>
  <ul class="formList">
    <li class="clearfix">
      <sofi:libelle identifiant="login.libelle.oublier_mot_passe.code_utilisateur" 
                     nomAttribut="codeUtilisateur" />
      <sofi-html:text property="codeUtilisateur" obligatoire="true" size="30" />
    </li>
    <li class="clearfix">         
      <sofi:libelle identifiant="login.libelle.login.champ.nouveau_mot_passe" 
                     nomAttribut="nouveauMotPasse" 
                     obligatoire="true"/>
      <sofi-html:password property="nouveauMotPasse" obligatoire="true" size="30"/>
    </li>
    <li class="clearfix">       
      <sofi:libelle identifiant="login.libelle.login.champ.confirmation_mot_passe" 
                     nomAttribut="confirmerMotPasse" />
      <sofi-html:password property="confirmerMotPasse" obligatoire="true" />
    </li>   
    <div class="zoneBouton clearfix">
      <sofi-html:submit libelle="login.libelle.oublier_mot_passe.action.soumettre" />
    </div>
  </sofi-html:form>  
</div>