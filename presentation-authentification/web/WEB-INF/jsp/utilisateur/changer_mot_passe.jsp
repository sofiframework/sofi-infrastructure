<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<div class="box">
	<sofi-html:form action="/changerMotPasse.do?methode=changerMotPasse" 
	                focusAutomatique="true" transactionnel="false"
	                styleClass="clearfix">
	    <sofi:barreStatut afficherSeulementSiMessage="true" />	
	    <h2><sofi:libelle identifiant="login.libelle.oublie_mot_passe.titre.nouveau_mot_passe" /></h2>	    
	  <p>
	      <sofi:libelle identifiant="login.libelle.changement_mot_passe.explication" />
	    </p>
	   <ul class="formList">
	      <li class="clearfix">
	       <sofi:libelle identifiant="login.libelle.changer_mot_passe.champ.code_utilisateur" 
	                     nomAttribut="codeUtilisateur" />
	       <sofi-html:text 
	                     property="codeUtilisateur" 
	                     obligatoire="true"
	                     disabled="${certificat ne null}"/>
	   
	      </li> 
	      <li class="clearfix">    
	       <sofi:libelle identifiant="login.libelle.changer_mot_passe.champ.nouveau_mot_passe" 
	                      nomAttribut="nouveauMotPasse" 
	                      obligatoire="true" />
	       <sofi-html:password 
	            property="nouveauMotPasse" 
	            obligatoire="true"
	            size="30"
	            />
	     </li> 
	        <li class="clearfix">    
	        <sofi:libelle identifiant="login.libelle.changer_mot_passe.champ.confirmer_mot_passe" 
	                      obligatoire="true"
	                      nomAttribut="confirmerMotPasse" />
	        <sofi-html:password
	             property="confirmerMotPasse" 
	             obligatoire="true"
	             size="30"
	             />
	        </li> 
	   </ul>       
	
	<div class="zoneBouton clearfix">
	  <sofi-html:bouton libelle="login.libelle.oublier_mot_passe.action.annuler" href="changerMotPasse.do?methode=annuler" />
    <sofi-html:submit libelle="login.libelle.oublier_mot_passe.action.soumettre" />
	</div>
	   </sofi-html:form>   

</div>