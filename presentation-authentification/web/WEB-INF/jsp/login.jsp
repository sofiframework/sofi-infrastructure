<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sofi" uri="http://www.sofiframework.org/taglib/sofi"%>
<%@ taglib prefix="sofi-html" uri="http://www.sofiframework.org/taglib/sofi-html"%>

<div class="box">
  <form class="clearfix" action="j_spring_security_check" method="POST">
    <h2> <sofi:libelle identifiant="login.libelle.authentification.titre.connexion" /> </h2>
    <c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
        <div id="message_erreur">
            <div id="messageFormulaire" class="zoneMessageFormulaireSimple">
               <span class="message_tout_type message_icone_attention">
                <sofi:message identifiant="${SPRING_SECURITY_LAST_EXCEPTION.message}" />
              </span>
            </div>
         <div class="clear"></div>
       </div>
    </c:if> 
    <c:if test="${not empty SOFI_LOGOUT}">
        <div id="message_erreur">
            <div id="messageFormulaire" class="zoneMessageFormulaireSimple">
               <span class="message_tout_type message_icone_information">
                <sofi:message identifiant="login.information.authentification.fermeture_session" />
              </span>
            </div>
         <div class="clear"></div>
       </div>
    </c:if> 
    <div>

    <ul class="formList">
      <li class="clearfix">

         <sofi:libelle identifiant="login.libelle.authentification.champ.nom_utilisateur"  nomAttribut="j_username"/>

         <sofi-html:text  styleId="j_username"

                          property="j_username" 

                          size="30" 

                          autocomplete="false"

                          />

      </li>

      <li class="clearfix">

        <sofi:libelle identifiant="login.libelle.authentification.champ.mot_passe" nomAttribut="j_password" />

        <sofi-html:password  styleId="j_password"

                         property="j_password" 

                         size="30"

                         autocomplete="false" 

                         />

      </li>

      <li class="clearfix">

        <sofi:libelle identifiant="login.libelle.authentification.champ.rester_connecte" nomAttribut="_spring_security_remember_me" />   

        <sofi-html:checkbox property="_spring_security_remember_me" styleClass="radio" /> 

      </li>
      <sofi:libelle identifiant="login.libelle.authentification.action.ouvrir_session"  var="libelleSubmit"/>
      <button type="submit" value="${libelleSubmit}" class="bouton"><span>${libelleSubmit}</span></button>

    </div>
    <script type="text/javascript">
      document.getElementById("j_username").focus();
    </script>
  </form>
  <ul class="listeFleches">
   <sofi:parametreSysteme code="authentificationInscription" var="authentiticationInscription"/>
   <c:if test="${authentiticationInscription }" >
	   <li>
	     <sofi-html:link href="inscription.do?methode=acceder" libelle="login.libelle.authentification.action.inscription" />
	   </li>
	 </c:if>
   <li>
    <sofi-html:link href="oublierMotPasse.do?methode=acceder" libelle="login.libelle.authentification.action.oublie_mot_passe" />
    </li>
   </ul>
</div>
