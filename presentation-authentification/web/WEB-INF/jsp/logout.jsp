<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core'%>
<%@ taglib prefix='sofi' uri="http://www.sofiframework.org/taglib/sofi"%>
<%@ taglib prefix='sofi-html' uri="http://www.sofiframework.org/taglib/sofi-html"%>

<br />
<br />
<center>
  <sofi:libelle identifiant="login.libelle.logout.lien_login" />
  <a href="index.jsp"><sofi:libelle identifiant="login.libelle.logout.ici" /></a>
</center>
<br />
<br />
<br />
<br />