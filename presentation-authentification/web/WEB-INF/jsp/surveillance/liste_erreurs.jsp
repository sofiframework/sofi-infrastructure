<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<fieldset>
  <legend><sofi:libelle identifiant="infra_sofi.libelle.surveillance.onglet.liste_erreurs.titre" /></legend>
    <sofi-liste:listeNavigation    class="tableresultat"
                                   nomListe="${listeErreur}"
                                   action="surveillance.do?methode=afficherListeErreur" 
                                   colgroup="10%,10%,40%,20%,20%" 
                                   triDefaut="1"
                                   messageListeVide="infra_sofi.information.surveillance.onglet.liste_erreurs.aucune_erreur"
                                   id="erreur">
      <sofi-liste:colonne property="nomPageErreur" 
                          libelle="infra_sofi.libelle.surveillance.onglet.liste_erreurs.entete.action" 
                          trier="true" />
                          
      <sofi-liste:colonne property="methode" 
                          libelle="infra_sofi.libelle.surveillance.onglet.liste_erreurs.entete.methode" 
                          trier="true" />
                          
      <sofi-liste:colonne property="messageCourt" 
                          libelle="infra_sofi.libelle.surveillance.onglet.liste_erreurs.entete.message_court" 
                          trier="true" 
                          paramId="no" 
                          paramProperty="no" 
                          href="surveillance.do?methode=afficherErreur"/>
                          
      <sofi-liste:colonne property="utilisateur.nom" 
                          libelle="infra_sofi.libelle.surveillance.onglet.liste_erreurs.entete.utilisateur" 
                          trier="true" 
                          paramId="codeUtilisateur,codeErreur" 
                          paramProperty="utilisateur.cle,no" 
                          href="surveillance.do?methode=afficherUtilisateur"
                          traiterCorps="true">
        <c:out value="${erreur.utilisateur.utilisateur.nom}" />,
        <c:out value="${erreur.utilisateur.utilisateur.prenom}" />
       
      </sofi-liste:colonne>
                          
      <sofi-liste:colonne property="dateHeureString" 
                          libelle="infra_sofi.libelle.surveillance.onglet.liste_erreurs.entete.date_heure" 
                          trier="true" />
    </sofi-liste:listeNavigation>
</fieldset>