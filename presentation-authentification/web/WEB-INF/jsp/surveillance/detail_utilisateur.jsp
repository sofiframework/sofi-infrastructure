<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<fieldset>
  <legend><sofi:libelle identifiant="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.titre" /></legend>
  <table class="tableaffichage">
    <sofi:affichageEL valeur="${utilisateur.nom}" libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.nom"/>
    <sofi:affichageEL valeur="${utilisateur.prenom}" libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.prenom"/>
    <sofi:affichageEL valeur="${utilisateur.codeUtilisateur}" libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.codeUtilisateur"/>
    <sofi:affichageEL libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.role">
      <c:forEach items="${listeRoles}" var="role">
        <c:out value="${role.code}" />
        -
         <c:out value="${role.nom}" />
         <br/>
      </c:forEach>
    </sofi:affichageEL>
    <sofi:affichageEL valeur="${utilisateurSession.adresseIP}" libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.adresseIP"/>
    <sofi:affichageEL valeur="${utilisateurSession.navigateur}" libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.navigateur"/>
    <sofi:affichageEL valeur="${utilisateurSession.creationAcces}" libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.cree_acces"/>
    <sofi:affichageEL valeur="${utilisateurSession.dernierAcces}" libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.dernier_acces"/>
    <sofi:affichageEL valeur="${utilisateurSession.derniereAction}" libelle="infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.derniere_action"/>
  </table>
</fieldset>