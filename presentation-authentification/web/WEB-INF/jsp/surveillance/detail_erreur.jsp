<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<fieldset>
  <legend><sofi:libelle identifiant="infra_sofi.libelle.surveillance.onglet.detail_erreur.titre" /></legend>
  <table class="tableaffichage">
    <sofi:affichageEL valeur="${action}" libelle="infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.action"/>
    <sofi:affichageEL valeur="${methode}" libelle="infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.methode"/>
    <sofi:affichageEL valeur="${utilisateur.nom}" libelle="infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.utilisateur"/>
    <sofi:affichageEL valeur="${utilisateur.prenom}" libelle="infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.prenom_utilisateur"/>
    <sofi:affichageEL valeur="${dateHeure}" libelle="infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.date_heure"/>
    <sofi:affichageEL valeur="${message}" libelle="infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.message"/>
  </table>
</fieldset>