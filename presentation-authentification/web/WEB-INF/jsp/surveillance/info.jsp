<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<fieldset>
  <legend><sofi:libelle identifiant="infra_sofi.libelle.surveillance.onglet.info_general.titre" /></legend>
  <table class="tableaffichage">
    <sofi:affichageEL valeur="${departApplication}" libelle="infra_sofi.libelle.surveillance.onglet.info_general.champ.depart_application"/>
    <sofi:affichageEL valeur="${nbPageConsulte}" libelle="infra_sofi.libelle.surveillance.onglet.info_general.champ.nb_pages_consultees"/>
    <sofi:affichageEL valeur="${nbPageEchec}" libelle="infra_sofi.libelle.surveillance.onglet.info_general.champ.nb_pages_echec">
      &nbsp;&nbsp;<sofi-html:link href="surveillance.do?methode=initialiserPageErreur" libelle="infra_sofi.libelle.surveillance.onglet.info_general.action.initialiser_les_erreurs"/>
    </sofi:affichageEL>
    <sofi:affichageEL valeur="${nbUtilisateur}" libelle="infra_sofi.libelle.surveillance.onglet.info_general.champ.nb_utilisateurs"/>
    <sofi:affichageEL valeur="${nbUtilisateurTotal}" libelle="infra_sofi.libelle.surveillance.onglet.info_general.champ.nb_utilisateurs_cumulatif"/>
  </table>
</fieldset>