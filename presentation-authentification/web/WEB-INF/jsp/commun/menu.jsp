<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>

<div id="zonemenu">
  <ul id="nav">
    <sofi-menu:menu nom="Velocity" gabaritVelocity="/com/nurun/sofi/presentation/velocity/gabarit/menu_deroulant_css.vm">
      <c:forEach var="premierNiveau" items="${menuUtilisateur.menuPremierNiveau}">
        <sofi-menu:section identifiant="${premierNiveau.identifiant}"/>
      </c:forEach>
    </sofi-menu:menu>
    <li id="sectionfinmenu">&nbsp;</li>
  </ul>
</div>