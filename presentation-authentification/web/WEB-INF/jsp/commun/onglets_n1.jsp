<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>

<!-- Onglets principaux -->
<div id="zoneonglet">
  <div id="enteteonglet">
    <sofi-menu:menu nom="Velocity" gabaritVelocity="/com/nurun/sofi/presentation/velocity/gabarit/onglets2.vm">
      <ul>
        <c:forEach var="onglet" items="${listeOnglets}">
          <sofi-menu:section identifiant="${onglet.identifiant}"/>
        </c:forEach>
      </ul>
    </sofi-menu:menu>
  </div>
</div>