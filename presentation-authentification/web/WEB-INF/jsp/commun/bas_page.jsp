<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>


<div id="innerFooterBox" class="clearfix">
    <sofi-html:link href="http://www.sofiframework.org"
                  styleClass="logo"
                  aideContextuelle="Accès au site officiel de SOFI Framework"
                  libelle="Propulsé par Sofi"/>
    <p>&copy; 2011 Tout droits réservés sofiframework.org</p>
</div>
<!-- /#pied-->