<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<a href="index.html" id="logo">SOFI, communauté Java | Authentification</a>
<ul id="menuSecondaire" class="clearfix">
  <c:if test="${utilisateur != null }">
	  <li class="first">
	   <a href="index.html"> 
	     <sofi:libelle identifiant="login.libelle.commun.action.accueil" />
	   </a>
	  </li>
	  <li>
	  </li>
	  <li><sofi-html:link libelle="login.libelle.commun.action.quitter" 
	                       href="j_spring_security_logout" 
	                       messageConfirmation="infra_sofi.confirmation.commun.fermer_session" />
	   </li>
  </c:if>
</ul>