<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>

<!-- Les onglets de 2ieme niveau -->
<c:if test="${listeOnglets2 != null}" >
  <div id="sous_onglet">
    <sofi-menu:menu nom="Velocity" gabaritVelocity="/com/nurun/sofi/presentation/velocity/gabarit/sous_onglets.vm" >
      <ul>
        <c:forEach var="onglet2" items="${listeOnglets2}" varStatus="compteur">
          <sofi-menu:section identifiant="${onglet2.identifiant}" niveauOnglet="2" noMenu="${compteur.index}" />
        </c:forEach>
      </ul>
    </sofi-menu:menu>
    <div class="clear"></div>
  </div>
</c:if>