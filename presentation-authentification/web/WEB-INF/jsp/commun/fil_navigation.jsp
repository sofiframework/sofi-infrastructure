<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>

<div id="zonefilnavigation">
  <sofi-menu:menu nom="Velocity" gabaritVelocity="/org/sofiframework/presentation/velocity/gabarit/fil_navigation.vm">
    <c:forEach var="composantMenu" items="${utilisateur.filNavigation}">
      <sofi-menu:section identifiant="${composantMenu.identifiant}" />
    </c:forEach>
  </sofi-menu:menu>
  <div id="zone_retour_tache_precedente">
      <sofi-html:link styleClass="retour_tache_precedente"
                      appliquerAdresseRetour="true"
                      libelle="infra_sofi.libelle.commun.retour_tache_precedente"
                      activerConfirmationModificationFormulaire="true"
                      />
  </div>
</div>