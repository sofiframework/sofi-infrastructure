<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi-liste:listeNavigation action="rechercheApplication.do?methode=rechercher"
                      nomListe="${listeApplication}"
                      triDefaut="1"
                      triDescendantDefaut="false"
                      class="tableresultat"
                      id="application"
                      trierPageSeulement="false"
                      ajax="true"
                      libelleResultat="auth_sofi.libelle.liste_application.resultats"
                      iterateurMultiPage="true"
                      varIndex="idx"
                      colgroup="30%,70%">
  <sofi-liste:colonne property="nom" trier="true" 
                      libelle="auth_sofi.libelle.liste_application.colonne.nom"
                      href="${application.adresseSite}"/>
  <sofi-liste:colonne property="description" 
                      libelle="auth_sofi.libelle.liste_application.colonne.description"/>
</sofi-liste:listeNavigation>  