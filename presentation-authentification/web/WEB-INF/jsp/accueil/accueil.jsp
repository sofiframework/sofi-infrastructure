<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<div class="box">
  <p>
   <sofi:libelle identifiant = "login.libelle.accueil.bonjour" parametre1="${utilisateur.prenom}" parametre2="${utilisateur.nom}"/><br/><br/>
    </p>
    <p>
     <sofi:libelle identifiant = "login.libelle.accueil.explication" />
    </p>
	<ul class="listeFleches">
	  <sofi-liste:utilitaire var="nombreApplication" liste="${listeApplication}"/>
	  <c:forEach items="${listeApplication}" var="application" varStatus="compteurListe">
	    <c:if test="${compteurListe.index < (nombreApplication-1)}">
	       <li>
	    </c:if>
	    <c:if test="${compteurListe.index == (nombreApplication -1)}">
	     <li class="last">
	    </c:if>
	    <li>
	      <sofi-html:link href="${application.adresseSite}" libelle="${application.nom}" />
	    </li>
	  </c:forEach>
	 <li>
	  <sofi-html:link href="changerMotPasse.do?methode=acceder" 
	                 libelle="login.libelle.authentification.action.modification_mot_passe" 
	                 memoriserHrefActuel="true" />
	 </li>
	</ul>
</div>

