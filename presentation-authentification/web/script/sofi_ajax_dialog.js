
$.postHtml = function(url, data, callback) {
  $.post(url, data, callback, "html");
};

/*
 * Initialisation d'un dialogue ajax.
 */
function initDialog(divId, height, width, resizable, reloadOnClose) {
  $(function() {
    $('#' + divId).dialog({
      bgiframe: true,
      autoOpen: false,
      height: height,
      width: width,
      modal: true,
      resizable: resizable,
      close: function(event, ui) {
    	  if (reloadOnClose) {
    	    location.reload(true);
    	  }else {
            $(this).empty();
    	  }
      }
    });
  });
}

/*
 * Obtenir un dialog a partir de son element.
 */
function getDialog(element) {
  return $(element).parents('.dialog');
}

/*
 * Ouverture d'un dialog ajax.
 */

function openDialog(divId, height, width, resizable, url, callback) {
	openDialog(divId, height, width, resizable, url, callback, false);
}

function openDialog(divId, height, width, resizable, url, callback, reloadOnClose) {
  initDialog(divId, height, width, resizable, reloadOnClose);

  if (url.indexOf('&ajax=true') == -1) {
    url = url + '&ajax=true';
  }
  
  $('#' + divId).html('<img src=\"images/sofi/roller.gif\" />');

  ajaxUpdate(url, divId, function(){
    if (callback) {
      callback();
    }
    NiceTitles.autoCreation();
  });
  $('#' + divId).dialog('open');
}


/*
 * Fermeture d'un dialogue.
 */
function closeDialog(element) {
  getDialog(element).dialog('close');
  getDialog(element).empty();
}

/*
 * Appel post pour un formulaire dans un dialog ajax.
 */
function postDialog(element, fonctionOnComplete) {
  var formulaire = $(element).parents('form');
  var donnees = formulaire.serializeArray();
  var action = formulaire[0].action;

  if (action && action.indexOf('&ajax=true') == -1) {
    action += '&ajax=true;';
  }

  var dialog = getDialog(element);
 
  var retour = $.post(action, donnees, function(resultat) {
    dialog.html(resultat);
    formulaire = dialog.find('form');
    if (fonctionOnComplete != null) {
      fonctionOnComplete();
      NiceTitles.autoCreation();
      var onfocus = false;
      var listeInput = formulaire.find("input");
      formulaire.find("input, textarea, select, radio, checkbox").each(function () {
        if (!onfocus && $(this).attr('class').indexOf('saisieErreur') != -1) {
          $(this).focus();
          onfocus = true;
        }
      });
    }
  }, "html");
}

/*
 * Permet de mettre à jour un div avec le retour 
 * d'un appel ajax.
 */
function ajaxUpdate(url, divId, callback) {
  if (url.indexOf('&ajax=true') == -1) {
    url = url + '&ajax=true';
  }
  var div = $('#' + divId);
  div.load(url, callback);
}

function soumettreFormulaireAjax(urlSoumettre, element, fonctionSucces, activerConfirmationFormulaire, messageConfirmation,e) {
  setMessageModificationFormulaire(messageConfirmation);
  if(!activerConfirmationFormulaire || confirmerModificationFormulaire(e)) {
    var formulaire = $(element).parents('form');
    formulaire.attr('action', urlSoumettre);
    postDialog(element, fonctionSucces);
    inactiverBoutonsFormulaire(formulaire);
  }
}

function supprimerFormulaireAjax(urlSuppression, element, messageConfirmation, fonctionSucces) {
  if (confirm(messageConfirmation)) {
    var formulaire = $(this).parents('form');
    formulaire.attr('action', urlSuppression);
    postDialog(element, fonction);
    inactiverBoutonsFormulaire(formulaire);
    fermerDialog(this);
  }
}

function afficherFenetreAttente(divId) {
  if (!divId) {
    divId = "fenetreAttente";
  }

  $(function() {
    $("#" + divId).dialog({
      bgiframe: true,
      height: 140,
      modal: true,
      closeOnEscape:false,
      resizable:false
    });
  });
}

function fermerFenetreAttente(divId) {
  if (!divId) {
    divId = "fenetreAttente";
  }
  $('#' + divId).dialog('close');
}

function inactiverBoutonsFormulaire(formulaire) {
  formulaire.find('.bouton,.boutonover').removeClass('bouton').addClass('boutondisabled').attr('disabled', 'true');
}

function ajouterAttenteFormulaireAjax(formulaire) {
  $('#BarreStatutPopup').html(
      "<div id='messageFormulaireBarreStatutPopup' class='zoneMessageFormulaireSimple'><span class='message_tout_type message_icone_information'><img src='images/sofi/roller.gif' /></span></div><div class='clear'></div>"
  );
}

function desactiverSubmitSurEnter(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  
  if ((evt.keyCode == 13) && (node.type == "text")) {
    return false;
  }
}

/*
 * Permet d'aller porter une valeur ajax dans le formulaire.
 */
function modifierComposant(composant, callback) {
  var action = null;
  $(composant).parents('form').each(function () {action = $(this).attr('action');})
  action = action.substring(action.lastIndexOf('/') + 1, action.indexOf('?'));
  var url = action + "?methode=modifierAttributAjax&ajax=true&ignorerValidationObligatoire=true";
  $.post(url, $(composant).serialize(), callback, "html");
}

/*
 * Initialiser un div progres ajax.
 */
function initProgres(id) {
  $(document).ready(function(){
    hideProgress(id);
  });
}

/*
 * Afficher un div progres ajax.
 */
function showProgress(id) {
  id = id == null ? 'progres' : id;
  // Pour aider le style, on cache à l'aide de visibility
  $("#" + id).css("visibility", "visible");
}

/*
 * Cacher un div progres ajax.
 */
function hideProgress(id) {
  id = id == null ? 'progres' : id;
  // Pour aider le style, on cache à l'aide de visibility
  $("#" + id).css("visibility", "hidden");
}

var ajaxCount = 0;

$(document).ajaxStart(
    function() {
      ajaxCount++;
      setTimeout("afficherAttenteAjax()", 500);
    }
).ajaxStop(
    function() {
      ajaxCount--;
      if (ajaxCount <= 0) {
        fermerAttenteAjax();
      }
    }
);

function afficherAttenteAjax() {
  if (ajaxCount == 1) {
    afficherFenetreAttente("attenteAjax");
    $("#attenteAjax").dialog('open');
  }
}

function fermerAttenteAjax() {
  $("#attenteAjax").dialog('close');
}
