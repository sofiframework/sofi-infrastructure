/**
 * struts-layout core javascript
 *
 * All rights reserved.
 */
// type checking functions

function checkValue(field, property, type, required) {

	if (field.value!="") {
	
		document.images[property + "required"].src= imgsrc + "clearpixel.gif";
		if (type=="NUMBER" && !isNumber(field.value)) document.images[property + "required"].src= imgsrc + "ast.gif";
		if (type=="DATE" && !isDate(field.value)) document.images[property + "required"].src = imgsrc + "ast.gif";
		if (type=="EMAIL" && !isEmail(field.value)) document.images[property + "required"].src= imgsrc + "ast.gif";
	
	} else {	
		if (required) document.images[property + "required"].src= imgsrc + "ast.gif";
	}
}

// Return true if value is an e-mail address
function isEmail(value) {
	invalidChars = " /:,;";
	if (value=="") return false;
	
	for (i=0; i<invalidChars.length;i++) {
	   badChar = invalidChars.charAt(i);
	   if (value.indexOf(badChar,0) != -1) return false;
	}
	
	atPos = value.indexOf("@", 1);
	if (atPos == -1) return false;
	if (value.indexOf("@", atPos + 1) != -1) return false;
	
	periodPos = value.indexOf(".", atPos);
	if (periodPos == -1) return false;
	
	if (periodPos+3 > value.length) return false;

	return true;
}



// Return true if value is a number
function isNumber(value) {
	if (value=="") return false;

	var d = parseInt(value);
	if (!isNaN(d)) return true; else return false;		

}


// menu functions

function initMenu(menu) {
	if (getMenuCookie(menu)=="hide") {
		document.getElementById(menu).style.display="none";
	} else {
		document.getElementById(menu).style.display="";
	}
}

function changeMenu(menu) {
if (document.getElementById(menu).style.display=="none") {
	document.getElementById(menu).style.display="";
	element = document.getElementById(menu+"b");
	if (element != null) {
		document.getElementById(element).style.display="none";
	}
	setMenuCookie(menu,"show");
} else {
	document.getElementById(menu).style.display="none";
	element = document.getElementById(menu+"b");
	if (element != null) {	
		var width = document.getElementById(menu).offsetWidth;	
		if (navigator.vendor == ("Netscape6") || navigator.product == ("Gecko"))
			document.getElementById(menu+"b").style.width = width;	
		else 
			document.getElementById(menu+"b").width = width;
		document.getElementById(menu+"b").style.display="";
	}
	setMenuCookie(menu,"hide");
}
return false;
}

function setMenuCookie(name, state) {	
	if (name.indexOf("treeView")!=-1) {
		if (state=="show") {
			var cookie = getMenuCookie("treeView", "");
			if (cookie=="???") cookie = "_";
			cookie = cookie + name + "_";
			document.cookie = "treeView=" + escape(cookie);

		} else {
			var cookie = getMenuCookie("treeView", "");
			var begin = cookie.indexOf("_" + name + "_");
			if (cookie.length > begin + name.length + 2) {
				cookie = cookie.substring(0, begin+1) + cookie.substring(begin + 2 + name.length);
			} else {
				cookie = cookie.substring(0, begin+1);
			}		
			document.cookie = "treeView=" + escape(cookie);
		}
	} else {
		var cookie = name + "STRUTSMENU=" + escape(state);
		document.cookie = cookie;	
	}
}

function getMenuCookie(name, suffix) {
	if (suffix==null) {
		suffix = "STRUTSMENU";
	}
	var prefix = name + suffix + "=";
	var cookieStartIndex = document.cookie.indexOf(prefix);
	if (cookieStartIndex == -1) return "???";
	var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length);
	if (cookieEndIndex == -1) cookieEndIndex = document.cookie.length;
	return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex));
}

// sort functions
function arrayCompare(e1,e2) {
	return e1[0] < e2[0] ? -1 : (e1[0] == e2[0] ? 0 : 1);

}

var tables = new Array();
function arraySort(tableName, column, lineNumber, columnNumber) {
	var aTable = tables[tableName];
	var arrayToSort;
	var array;
	var reverse = 0;
	if (aTable) {
		array = aTable[0];
		arrayToSort = new Array(lineNumber);
		for (i=0;i<lineNumber;i++) {
			arrayToSort[i] = new Array(2);
			arrayToSort[i][0] = array[i][column];
			arrayToSort[i][1] = i;				
		}
		reverse = 1 - aTable[1];
		aTable[1] = reverse;
	} else {
		array = new Array(lineNumber);
		arrayToSort = new Array(lineNumber);
		for (i=0;i<lineNumber;i++) {	
			array[i] = new Array(columnNumber);
			for (j=0;j<columnNumber;j++) {
				obj = document.getElementById("t" + tableName + "l" + (i+1) +"c" + j);		
				array[i][j] = obj.innerHTML;
			}
			arrayToSort[i] = new Array(2);
			arrayToSort[i][0] = array[i][column];
			arrayToSort[i][1] = i;		
	
			aTable = new Array(2);
			aTable[0] = array;
			aTable[1] = 0;
			tables[tableName] = aTable;
		}
	}

	arrayToSort.sort(arrayCompare);
	if (reverse) {
		arrayToSort.reverse();
	}

	for (i=0;i<lineNumber;i++) {
		goodLine = arrayToSort[i][1];
		for (j=0;j<columnNumber;j++) {
			document.getElementById("t" + tableName + "l" + (i+1) +"c" + j).innerHTML = array[goodLine][j];
		}
	}
}

// calendar functions

var calformname;
var calformelement;
var dayMain=0;
var monthMain=0;
var yearMain=0;
var daySelect=0;
var monthSelect=0;
var yearSelect=0;


function printCalendar(day1, day2, day3, day4, day5, day6, day7, month1, month2, month3, month4, month5, month6, month7, month8, month9, month10, month11, month12, day, month, year) {
	
	document.write('<div id="caltitre" style="z-index:10;">');
	document.write('<table class="CALENDRIER" cellpadding="0" cellspacing="0" border="0" width="253">');
//	document.write('<form>');
	document.write('<tr><td colspan="15" bgColor="#000000"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td></tr>');
	document.write('<tr>');
	document.write('	<td bgColor="#000000" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=20></td>');
	document.write('	<td colspan="3" align="right"><img src="' + imgsrc + 'previous.gif" onclick="cal_before(' + day + ',' + month + ',' + year + ');">&nbsp;&nbsp;&nbsp;</td>');
	document.write('	<td colspan=7 align="center">');
	document.write('<select class="listeDeroulanteCal" id="calmois" name="calmois" onchange="cal_chg(' + day +',' + month + ',' + year + ', this.options[this.selectedIndex].value);"><option value=0>...</option>');
	
	var str='';
	for(i=1;i<37;i++) {
		var annee= year + Math.floor((i-1)/12);
		str+='<option value='+i+'>';
		monthIndex = (i-1)%12;
		switch (monthIndex) {
			case 0: str += month1; break;
			case 1: str += month2; break;
			case 2: str += month3; break;
			case 3: str += month4; break;
			case 4: str += month5; break;
			case 5: str += month6; break;
			case 6: str += month7; break;
			case 7: str += month8; break;
			case 8: str += month9; break;
			case 9: str += month10; break;
			case 10: str += month11; break;
			case 11: str += month12; break;
		}
		str+=' ' + (year + Math.floor((i-1)/12));

	}
	document.write(str);

	document.write('</select>');
	document.write('	</td>');
	document.write('	<td class="CAL_ENTENTE" align="left" colspan="3"><img src="' + imgsrc + 'next.gif" onclick="cal_after(' + day + ',' + month + ',' + year + ');">&nbsp;&nbsp;<img src="' + imgsrc + 'close.gif" onclick="hideCalendar()"></td>');
	document.write('	<td bgcolor="#000000" width=1><img src="' + imgsrc + 'shim.gif" width="1" height="1"></td>');
	document.write('</tr>');
	document.write('<tr><td colspan=15 bgcolor="#000000"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td></tr>');
	document.write('<tr>');
	document.write('	<td bgcolor="#000000" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>');
	document.write('	<td class="CAL_ENTENTE" width="35">' + day1 + '</td>');
	document.write('	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>');
	document.write('	<td class="CAL_ENTENTE" width="35">' + day2 + '</td>');
	document.write('	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>');
	document.write('	<td class="CAL_ENTENTE" width="35">' + day3 + '</td>');
	document.write('	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>');
	document.write('	<td class="CAL_ENTENTE" width="35">' + day4 + '</td>');
	document.write('	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>');
	document.write('	<td class="CAL_ENTENTE" width="35">' + day5 + '</td>');
	document.write('	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>');
	document.write('	<td class="CAL_ENTENTE" width="35">' + day6 + '</td>');
	document.write('	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>');
	document.write('	<td class="CAL_ENTENTE" width="35">' + day7 + '</td>');
	document.write('	<td bgcolor="#000000" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>');
	document.write('</tr>');
	document.write('<tr><td colspan=15 bgcolor="#000000"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td></tr>');
//	document.write('</form>');
	document.write('</table>');
	document.write('</div>');
	document.write('<div id="caljour" style="z-index:10;"></div>');
}

/**
 * Show the calendar
 */
function showCalendar(year, month, day, formName, formProperty, event, debut, fin) {
	var cle = '';
	cle += month;
	cle += year;
	var sel = document.getElementsByTagName("SELECT")
	if(debut == null) {debut = 0}
	if(fin == 0 || fin == null) {fin = sel.length}
	
	for(i=debut;i<=fin;i++){
    if ( typeof sel[i] == "object"){
  		sel[i].style.visibility = "hidden"
    }  
	}
	position = event.clientX;
	offset=position;
	if (position < 200) {
		offset= 210;	
	}
	if(document.all) {
		// IE.
		var ofy=document.body.scrollTop;
		var ofx=document.body.scrollLeft;
		
		document.all.slcalcod.style.left = offset+ofx-202;
		document.all.slcalcod.style.top = event.clientY+ofy+15;
		document.all.slcalcod.style.visibility="visible";			
		document.all.calmois.selectedIndex=12;
	} else if(document.layers) {
		// Netspace 4
		document.slcalcod.left = offset-200;
		document.slcalcod.top = e.pageY+15;
		document.slcalcod.visibility="visible";
		document.slcalcod.document.caltitre.document.forms[0].calmois.selectedIndex=month;
	} else {
		// Mozilla
		var calendrier = document.getElementById("slcalcod");
		var ofy=document.body.scrollTop;
		var ofx=document.body.scrollLeft;
		calendrier.style.left = offset+ofx-202;
		calendrier.style.top = event.clientY+ofy+15;
		calendrier.style.visibility="visible";
		document.getElementById("calmois").selectedIndex=month;
	}
	if (document.forms[formName].elements[formProperty].stlayout) {
		var lc_day = document.forms[formName].elements[formProperty].stlayout.day;
		var lc_month = document.forms[formName].elements[formProperty].stlayout.month;
		var lc_year = parseInt(document.forms[formName].elements[formProperty].stlayout.year);
		cal_chg(lc_day, lc_month, lc_year, lc_month, 'true');	
	} else {
		daySelect=day;
		monthSelect=month;
		yearSelect=year;
		cal_chg(day, month, year, month,'true');	
	}
	calformname = formName;
	calformelement = formProperty;
}

/**
 * Redraw the calendar for the current date and a selected month
 */
function cal_chg(day, month, year, newMonth, depart){
	
	var oldMonth = month;
	var newMonth;

	
	if (depart != 'true')
	{	
		oldMonth = monthMain;	
		var monthAdd = newMonth - 13;
		newMonth= parseInt(oldMonth)+ parseInt(monthAdd);

		if (newMonth > 12)
		{
			year++;
			newMonth= newMonth - 12;
		}
		if (newMonth > 12)
		{
			year++;
			newMonth= newMonth - 12;
		}
		if (newMonth < 1)
		{
			newMonth += 12;
			year--;
		}
	}
	var yearCompteur = year;	
	var strTitre='';
	
	var day1="dim.";
	var day2="lun.";
	var day3="mar.";
	var day4="mer.";
	var day5="jeu.";
	var day6="ven.";
	var day7="sam.";
	var month1="Janv.";
	var month2="F�vr.";
	var month3="Mars";
	var month4="Avr.";
	var month5="Mai";
	var month6="Juin";
	var month7="Juillet";
	var month8="Ao�t";
	var month9="Sept.";
	var month10="Oct.";
	var month11="Nov.";
	var month12="D�c.";
	
	strTitre+='<table class="CALENDRIER" cellpadding="0" cellspacing="0" border="0" width="253">';
	strTitre+='<tr><td colspan="15" bgColor="#000000"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td></tr>';
	strTitre+='<tr>';
	strTitre+='	<td bgColor="#000000" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=20></td>';
	strTitre+='	<td colspan="3" align="right"><img src="' + imgsrc + 'previous.gif" onclick="cal_before(' + day + ',' + month + ',' + year + ');">&nbsp;&nbsp;&nbsp;</td>';
	strTitre+='	<td colspan=7 align="center">';
	strTitre+='<select class="listeDeroulanteCal" id="calmois" name="calmois" onchange="cal_chg(' + day +',' + month + ',' + year + ', this.options[this.selectedIndex].value);">';
	
	
	var moisCompteur = newMonth-12;
	if (moisCompteur < 1)
	{
		moisCompteur +=12;
		yearCompteur--;
		
	}
	for(i=1;i<37;i++) {
		
		moisCompteurStr = moisCompteur
		if (moisCompteur < 10)
		{
			moisCompteurStr='0' + moisCompteur;
		}
		
		strTitre+='<option value='+ i+'>';

		switch (moisCompteur) {
			case 1: strTitre += month1; break;
			case 2: strTitre += month2; break;
			case 3: strTitre += month3; break;
			case 4: strTitre += month4; break;
			case 5: strTitre += month5; break;
			case 6: strTitre += month6; break;
			case 7: strTitre += month7; break;
			case 8: strTitre += month8; break;
			case 9: strTitre += month9; break;
			case 10: strTitre += month10; break;
			case 11: strTitre += month11; break;
			case 12: strTitre += month12; break;
		}
		strTitre+=' ' + yearCompteur;
		moisCompteur++;
		if (moisCompteur > 12)
		{
			moisCompteur=1;
			yearCompteur++;
		}
	}
	
	strTitre+='</select>';
	strTitre+='	</td>';
	strTitre+='	<td class="CAL_ENTENTE" align="left" colspan="3"><img src="' + imgsrc + 'next.gif" onclick="cal_after(' + day + ',' + month + ',' + year + ');">&nbsp;&nbsp;<img src="' + imgsrc + 'close.gif" onclick="hideCalendar()"></td>';
	strTitre+='	<td bgcolor="#000000" width=1><img src="' + imgsrc + 'shim.gif" width="1" height="1"></td>';
	strTitre+='</tr>';
	strTitre+='<tr><td colspan=15 bgcolor="#000000"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td></tr>';
	strTitre+='<tr>';
	strTitre+='	<td bgcolor="#000000" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>';
	strTitre+='	<td class="CAL_ENTENTE" width="35">' + day1 + '</td>';
	strTitre+='	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>';
	strTitre+='	<td class="CAL_ENTENTE" width="35">' + day2 + '</td>';
	strTitre+='	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>';
	strTitre+='	<td class="CAL_ENTENTE" width="35">' + day3 + '</td>';
	strTitre+='	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>';
	strTitre+='	<td class="CAL_ENTENTE" width="35">' + day4 + '</td>';
	strTitre+='	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>';
	strTitre+='	<td class="CAL_ENTENTE" width="35">' + day5 + '</td>';
	strTitre+='	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>';
	strTitre+='	<td class="CAL_ENTENTE" width="35">' + day6 + '</td>';
	strTitre+='	<td bgcolor="#C2C2C2" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>';
	strTitre+='	<td class="CAL_ENTENTE" width="35">' + day7 + '</td>';
	strTitre+='	<td bgcolor="#000000" width="1"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>';
	strTitre+='</tr>';
	strTitre+='<tr><td colspan=15 bgcolor="#000000"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td></tr>';
	strTitre+='</table>';	
	
	if(document.all) document.all.caltitre.innerHTML=strTitre;
	if(document.layers) {obj=document.calendrier.document.caltitre; obj.top=48; obj.document.write(strTitre); obj.document.close();}
	if (!document.all && document.getElementById) document.getElementById("caltitre").innerHTML = strTitre;		
	
	var str='',j;
	var lc_annee = year;
	
	if (monthSelect != 0)
	{
		oldMonth= monthSelect;
	}
	
	if(newMonth>0) {
	
		j=1;
		str+='<table cellpadding=0 cellspacing=0 border=0 width=253>\n';
		for(u=0;u<6;u++){
			str+='	<tr>\n';
			for(i=0;i<7;i++){
				ldt=new Date(lc_annee,newMonth-1,j);
				str+='		<td bgcolor="#000000" width=1><img src="' + imgsrc + 'shim.gif" width=1 height=20></td>\n';
				str+='		<td bgcolor="#'; 
				
				if(ldt.getDay()==i && ldt.getDate()==j && j==day && newMonth==oldMonth && lc_annee==year) str+='FF8F01'; else if(i==0 || i==6) str+='D1E2F3'; else str+='FFFFFF'; str+='" width="35" align="center">';
				if (ldt.getDay()==i && ldt.getDate()==j) {str+='<a href="javascript://" class="CAL_JOUR" onmousedown="dtemaj(\'' + j + '\',\'' + newMonth + '\',\'' + lc_annee +'\');">'+j+'</a>'; j++;} else str+='&nbsp;';
				str+='</td>\n';
			}
			str+='		<td bgcolor="#000000" width=1><img src="' + imgsrc + 'shim.gif" width=1 height=1></td>\n';
			str+='	</tr>\n';
			str+='	<tr><td colspan=15 bgcolor="#000000"><img src="' + imgsrc + 'shim.gif" width=1 height=1></td></tr>\n';
		}
		str+='</table>\n';
	
	monthMain=newMonth;
	yearMain=year;
	
	}
	
	document.all.calmois.selectedIndex=12;
	if(document.all) document.all.caljour.innerHTML=str;
	if(document.layers) {obj=document.calendrier.document.caljour; obj.top=48; obj.document.write(str); obj.document.close();}
	if (!document.all && document.getElementById) document.getElementById("caljour").innerHTML = str;
}

/**
 * Display the previous month
 */
function cal_before(day, month, year) {
	var champ;
	if (document.all) champ = document.all.calmois;//document.all.calendrier.document.caltitre.form.calmois;
	if (document.layers) champ = document.calendrier.document.caltitre.document.form.calmois;
	if (!document.all && document.getElementById) champ = document.getElementById("calmois");
	if (champ.selectedIndex>1) champ.selectedIndex--;
	cal_chg(day, month, year, champ.options[champ.selectedIndex].value);
}

/**
 * Display the next month
 */
function cal_after(day, month, year) {
	// r�cup�ration de l'objet
	var champ;
	if (document.all) champ = document.all.calmois;//document.all.calendrier.form.calmois;
	if (document.layers) champ = document.calendrier.document.form.calmois;
	if (!document.all && document.getElementById) champ = document.getElementById("calmois");
	if (champ.selectedIndex<champ.options.length) champ.selectedIndex++;
	cal_chg(day, month, year, champ.options[champ.selectedIndex].value);
}

/**
 * Update the date in the input field and hide the calendar.
 * PENDING: find a way to make the format customable.
 */
function dtemaj(jour, mois, annee){
	var moisString= mois;
	var jourString= jour;
	
	if (mois<10)
	{
		moisString = "0" + mois;
	}
	if (jour<10)
	{
		jourString = "0" + jour;
	}

	if (langue=='fr') {
	
		document.forms[calformname].elements[calformelement].value = annee + "-" + moisString + "-" + jourString;
	} else {
		document.forms[calformname].elements[calformelement].value = annee + "-" + moisString + "-" + jourString;
	}
	document.forms[calformname].elements[calformelement].stlayout = new Object();
	document.forms[calformname].elements[calformelement].stlayout.day = jour;
	document.forms[calformname].elements[calformelement].stlayout.month = mois;
	document.forms[calformname].elements[calformelement].stlayout.year = annee;
	daySelect = jour;
	monthSelect = mois;
	yearSelect=annee;
	hideCalendar();
}

function hideCalendar() {
	var sel = document.getElementsByTagName("SELECT")
	if(document.all) {
		// IE.
		for(i=0;i<sel.length;i++){
		sel[i].style.visibility = "visible"
		}
		document.all.slcalcod.style.visibility="hidden";
		document.all.calmois.style.visibility="hidden";
		//sel[5].style.visibility = "visible"
		//sel[6].style.visibility = "visible"
		//sel[7].style.visibility = "visible"
	} else if(document.layers) {
		// Netspace 4
		document.slcalcod.visibility="hidden";
	} else {
		// Mozilla
		var calendrier = document.getElementById("slcalcod");
		calendrier.style.visibility="hidden";
	}
}

/**
 * Tabs code.
 */
function selectTab(tabGroupId, tabGroupSize, selectedTabId, enabledStyle, disabledStyle, errorStyle) {
	// first unselect all tab in the tag groups.
	for (i=0;i<tabGroupSize;i++) {
		element = document.getElementById("tabs" + tabGroupId + "head" + i);
		if (element.classNameErrorStdLayout) {
			element.className = errorStyle;
			element.style.color = "";
			document.getElementById("tabs" + tabGroupId + "tab" + i).style.display = "none";		
		} else if (element.className == enabledStyle) {
			element.className = disabledStyle;
			element.style.color = "";
			document.getElementById("tabs" + tabGroupId + "tab" + i).style.display = "none";
		}
	}
	if (document.getElementById("tabs" + tabGroupId + "head" + selectedTabId).className==errorStyle) {
		document.getElementById("tabs" + tabGroupId + "head" + selectedTabId).classNameErrorStdLayout = new Object();
	}
	document.getElementById("tabs" + tabGroupId + "head" + selectedTabId).className = enabledStyle;
	document.getElementById("tabs" + tabGroupId + "head" + selectedTabId).style.cursor = "default";
	document.getElementById("tabs" + tabGroupId + "tab" + selectedTabId).style.display = "";
}
function onTabHeaderOver(tabGroupId, selectedTabId, enabledStyle) {
	element = document.getElementById("tabs" + tabGroupId + "head" + selectedTabId);
	if (element.className == enabledStyle) {
		element.style.cursor = "default";
	} else {
		element.style.cursor = "hand";
	}
}

/**
 * Treeview code
 */
function loadTree(url, tree) {
	element = document.getElementById("treeView" + url);
	element.innerHTML = tree;	
	element.style.display = "";
	element = document.getElementById("treeViewNode" + url);
	element.href = "javascript://";
	setMenuCookie("treeView" + url, "show")	
}

function changeTree(tree, image1, image2) {
	var image = document.getElementById("treeViewImage" + tree);
	if (image.src.indexOf(image1)!=-1) {
		image.src = image2;
	} else {
		image.src = image1;
	}

	if (document.getElementById("treeView" + tree).innerHTML == "") {
		return true;
	} else {
		changeMenu("treeView" + tree);
		return false;
	}
}

/**
 * Popup code
 */
function openpopup(form, popup, width, height, e) {
	var xx, yy;
	xx = e.screenX;
	yy = e.screenY;
	window.open('about:blank', 'popup', 'directories=0, location=0, menubar=0, status=0, toolbar=0, width=' + width + ', height=' + height + ', top=' + yy + ', left=' + xx); 	
	var action = form.action;
	var target = form.target;
	if (popup == null || popup == "") {
		popup = action;
	}
	form.target='popup';
	form.action = popup
	form.submit();
	form.target = target;
	form.action = action;
		
	return false;
}

function closepopup(form, openerField, popupField) {
	var inputField = form[popupField];
	var value;
	if (inputField.options) {
		value = inputField.options[form[popupField].selectedIndex].value;
	} else {
		for (i=0; i < form.elements.length; i++) {
			var element = form.elements[i];
			if (element.name == popupField && element.checked) {
				value = element.value;
				break;
			}
		}
	}
	window.opener.document.forms[0][openerField].value = value;
	window.close();
}

/**
 * form changes detect code
 */
function checkFormChange(link, text) {
  var ok = true;
  for (var form=0; form < document.forms.length; form++) {
    what = document.forms[form];
    for (var i=0, j=what.elements.length; i<j; i++) {

        if (what.elements[i].type == "checkbox" || what.elements[i].type == "radio") {
            if (what.elements[i].checked != what.elements[i].defaultChecked) {
		ok = false; break;
	    }
	} else if (what.elements[i].type == "text" || what.elements[i].type == "hidden" || what.elements[i].type == "password" || what.elements[i].type == "textarea") {
            if (what.elements[i].value != what.elements[i].defaultValue) {
		ok = false; break;
	    }
	} else if (what.elements[i].type == "select-one" || what.elements[i].type == "select-multiple") {
            for (var k=0, l=what.elements[i].options.length; k<l; k++) {
                if (what.elements[i].options[k].selected != what.elements[i].options[k].defaultSelected) {
		ok = false; break;
		}
	    }
	} else {
		alert(what.elements[i].type);
	}
    }
  }
    if (ok) {	
	window.location.href = link;
	return;
    }
    if (confirm(text == null ? "Data will be lost. Continue ?" : text)) {
	window.location.href = link;
	return;
    }
}

