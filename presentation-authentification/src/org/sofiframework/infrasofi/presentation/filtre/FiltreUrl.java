/**
 * 
 */
/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.filtre;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Filtre permettant de stocker dans la session l'url de retour afin de pouvoir accéder à l'application cible après être passé dans tous les filtres.<p>
 * Il est nécessaire de stocker cette valeur dans la session lorsque l'utilisateur doit modifier son mot de passe, car cette valeur n'est plus présente dans la session.<p>
 *  
 * @author Jérôme Fiolleau, Nurun inc.
 * @date 09-10-13
 *
 */
public class FiltreUrl implements Filter {

  private static final String URL_RETOUR = "url_retour";
  
  /* (non-Javadoc)
   * @see javax.servlet.Filter#destroy()
   */
  public void destroy() {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
   */
  public void doFilter(ServletRequest req, ServletResponse res,
      FilterChain chain) throws IOException, ServletException {
    
    if (req instanceof HttpServletRequest) {
      HttpServletRequest request = (HttpServletRequest) req;

      String url_retour = request.getParameter(URL_RETOUR);
      String inactif = request.getParameter("inactif");
      
      if (StringUtils.isNotBlank(url_retour))
        request.getSession().setAttribute(URL_RETOUR, url_retour);
      
      if (!UtilitaireString.isVide(inactif))
        request.getSession().setAttribute("inactif", inactif);
      
      chain.doFilter(req, res);
    }
  }

  /* (non-Javadoc)
   * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
   */
  public void init(FilterConfig arg0) throws ServletException {
    // TODO Auto-generated method stub

  }

}
