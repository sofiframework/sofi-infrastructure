/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.utilisateur;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.sofiframework.infrasofi.presentation.action.FormAction;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.controleur.BaseDispatchAction;

/**
 * Contrôle les actions effectuées dans la page 
 * de profil de l'utilisateur.
 * 
 * @author Jean-Maxime Pelletier
 */
public class UtilisateurAction extends FormAction {
  
  /**
   * Consulter le profil.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
      
    this.acceder(mapping, form, request, response);
    
    return mapping.findForward("ajax");
  }

  public ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request) {
    Utilisateur utilisateur = null;
    try {
      String certificat = request.getParameter("certificat");
      String codeUtilisateur = GestionSecurite.getInstance().getServiceAuthentification().getCodeUtilisateur(certificat);
      utilisateur = GestionSecurite.getInstance().getUtilisateur(codeUtilisateur);
    } catch (Exception e) {
      throw new SOFIException("Erreur pour obtenir l'utilisateur.", e);
    }
    return utilisateur;
  }
}
