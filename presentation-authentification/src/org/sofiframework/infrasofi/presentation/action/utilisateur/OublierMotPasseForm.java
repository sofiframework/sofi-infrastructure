/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.utilisateur;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * Le formulaire qui permet de faire une demande de 
 * changement de mot de passe.
 * 
 * @author Jean-Maxime Pelletier
 */
public class OublierMotPasseForm extends BaseForm {

  private static final long serialVersionUID = 50392991697752708L;
  
  private String codeUtilisateur = null;
  
  public void validerFormulaire(HttpServletRequest request) {
    // captcha
    String url = "http://www.google.com/recaptcha/api/verify";
    String clePrive = "6LdXEr0SAAAAAGL10-V7wq7BroYLHNMv-5etZvke";
    String ip = request.getRemoteHost();
    String challenge = request.getParameter("recaptcha_challenge_field");
    String response = request.getParameter("recaptcha_response_field");
    PostMethod post =  new PostMethod(url);
    
    post.addParameter(new NameValuePair("privatekey", clePrive));
    post.addParameter(new NameValuePair("remoteip", ip));
    post.addParameter(new NameValuePair("challenge", challenge));
    post.addParameter(new NameValuePair("response", response));
    
    HttpClient client = new HttpClient();
    try {
      client.executeMethod(post);
      String postResponse = post.getResponseBodyAsString();

      if (postResponse != null) {
        Boolean succes = postResponse.startsWith("true");
        if (!succes) {
          this.ajouterMessageErreur("login.erreur.commun.captcha_invalide", request);
        }
      }
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour obtenir la validation du captcha.");
      }
    }
  }

  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }
}
