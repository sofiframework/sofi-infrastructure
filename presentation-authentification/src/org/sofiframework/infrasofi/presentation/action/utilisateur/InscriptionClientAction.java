package org.sofiframework.infrasofi.presentation.action.utilisateur;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Utilisateur;
import org.sofiframework.infrasofi.presentation.action.AuthentificationAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;

public class InscriptionClientAction extends AuthentificationAction {

  // L'instance de journalisation pour cette classe.
  private static final Log log = LogFactory.getLog(InscriptionAction.class);

  @Override
  public ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request) {
    Utilisateur utilisateur = new Utilisateur();
    return utilisateur;
  }
  
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {

    InscriptionForm formulaire = (InscriptionForm) form;
    String codeClient = formulaire.getString("codeClient");
    Utilisateur utilisateur = (Utilisateur) formulaire.getObjetTransfert();
    Boolean enErreur = Boolean.FALSE;
    
    try {
      ModeleInfrastructure modele = this.getModeleInfrastructure(request);
      modele.getServiceUtilisateur().inscrire(utilisateur, codeClient);
    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur lors de l'inscription.", e);
      }
      formulaire.ajouterMessageErreur(e, request);
      enErreur = Boolean.TRUE;
    }

    return mapping.findForward(enErreur ? "afficher" : "confirmation");
  }
}
