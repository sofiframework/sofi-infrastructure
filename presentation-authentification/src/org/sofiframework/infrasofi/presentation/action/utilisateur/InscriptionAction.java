package org.sofiframework.infrasofi.presentation.action.utilisateur;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Utilisateur;
import org.sofiframework.infrasofi.presentation.action.AuthentificationAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;

public class InscriptionAction extends AuthentificationAction {

  // L'instance de journalisation pour cette classe.
  private static final Log log = LogFactory.getLog(InscriptionAction.class);

  @Override
  public ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request) {
    Utilisateur utilisateur = new Utilisateur();
    return utilisateur;
  }
  
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {

    InscriptionForm formulaire = (InscriptionForm) form;
    
    Utilisateur utilisateur = (Utilisateur) formulaire.getObjetTransfert();
    
    if (utilisateur == null) {
      return mapping.findForward("acceder");
    }
  

    return mapping.findForward("inscription_client");
  }
}
