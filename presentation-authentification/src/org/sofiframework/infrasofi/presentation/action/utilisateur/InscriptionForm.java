package org.sofiframework.infrasofi.presentation.action.utilisateur;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Utilisateur;
import org.sofiframework.infrasofi.modele.filtre.FiltreUtilisateurRole;
import org.sofiframework.presentation.struts.form.BaseDynaForm;
import org.sofiframework.presentation.struts.form.formatter.FormatterCourriel;
import org.sofiframework.utilitaire.UtilitaireString;

public class InscriptionForm extends BaseDynaForm {
  
  private static final long serialVersionUID = 6107378833795053611L;
  
  
  public InscriptionForm() {
    this.ajouterTypeFormatter("courriel", FormatterCourriel.class);
    this.ajouterAttributDynamiqueSupplementaire("motPasseConfirmation", String.class);
  }
  
  @Override
  public void validerFormulaire(HttpServletRequest request) {
    ModeleInfrastructure modele = (ModeleInfrastructure) this.getModele(request);
    Utilisateur utilisateur = (Utilisateur) this.getObjetTransfert();

    if (utilisateur != null) {
      String codeUtilisateur = utilisateur.getCodeUtilisateur();
      String motPasseConfirmation = getString("motPasseConfirmation");
      if (!UtilitaireString.isVide(codeUtilisateur)) {
        // Le code utilisateur n'est pas utilisé
        Utilisateur testCodeUtilise = modele.getServiceUtilisateur()
            .getUtilisateur(codeUtilisateur);
        if (testCodeUtilise != null) {
          this.ajouterMessageErreur(
              "infra_sofi.erreur.gestion_utilisateur.code.utilisateur",
              "codeUtilisateur", request);
        }

        if (!utilisateur.getMotPasse().equals(motPasseConfirmation)
            && !isFormulaireEnErreur()) {
          this.ajouterMessageErreur(
              "login.erreur.inscription.motPasse.non_identiques", request);
        }
      }
      String codeClient = this.getString("codeClient");
      if (!UtilitaireString.isVide(codeClient) && !isFormulaireEnErreur()) {
        // Le code de client n'est pas utilisé
        FiltreUtilisateurRole filtreUtilisateurRole = new FiltreUtilisateurRole();
        filtreUtilisateurRole.setCodeClient(codeClient);
        List listeUtilisateurRole = modele.getServiceUtilisateurRole()
            .getListe(filtreUtilisateurRole);
        // Si la liste n'est pas null alors on a deja un client du meme code.
        if (listeUtilisateurRole != null && listeUtilisateurRole.size() > 0) {
          this.ajouterMessageErreur(
              "infra_sofi.erreur.gestion_utilisateur.code_client_existe",
              "codeClient", request);
        }
      }
    }
  }
}
