/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.utilisateur;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.securite.objetstransfert.ChangementMotPasse;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.presentation.action.FormAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Contrôle les actions effectuées pour qu'un utilisateur 
 * modifie son mot de passe.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ChangerMotPasseExpirerAction extends FormAction {
  
  /**
   * Le code utilisateur doit être celui de l'utilisateur en cours.
   */
  public ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request) {
    ChangementMotPasse changement = new ChangementMotPasse();
    changement.setCodeUtilisateur(getCodeUtilisateurEffectif(request));
    return changement;
  }
  
  private String getCodeUtilisateurEffectif(HttpServletRequest request) {
    Utilisateur utilisateur = getUtilisateur(request);
    return (utilisateur.getCodeUtilisateurApplicatif() != null) ? 
        utilisateur.getCodeUtilisateurApplicatif() : 
          utilisateur.getCodeUtilisateur();
  }

  /**
   * Effectue le changement du mot de passe.
   */
  public ActionForward changerMotPasse(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {

    ChangementMotPasse changement = (ChangementMotPasse) getFormulaire(form).getObjetTransfert();
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    try {
      modele.getServiceUtilisateur().changerMotPasseExpirer(changement);
      getFormulaire(form).ajouterMessageInformatif("login.erreur.authentification.motPasse.modifie_avec_succes", request);
    } catch (ModeleException e) {
      getFormulaire(form).ajouterMessageErreur(e, request);
      return afficher(mapping);
    }
    return mapping.findForward("chargerApplication");
  }  
}
