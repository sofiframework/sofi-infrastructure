package org.sofiframework.infrasofi.presentation.action.utilisateur;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.message.UtilitaireLibelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.infrasofi.modele.entite.Utilisateur;
import org.sofiframework.infrasofi.presentation.action.AuthentificationAction;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.presentation.velocity.UtilitaireVelocity;
import org.sofiframework.utilitaire.courriel.CourrielHtml;

/**
 * Action qui envoi une un courriel à l'utilisateur qui a oublié son mot de
 * passe. Le courriel contient l'url vers la page de changement du mot de passe.
 * 
 * @author Jean-Maxime Pelletier
 */
public class OublierMotPasseAction extends AuthentificationAction {

  private static final Log log = LogFactory.getLog(OublierMotPasseAction.class);

  public ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request) {
    // Cette page n' pas d'objet de transfert
    return new org.sofiframework.objetstransfert.ObjetCleValeur();
  }

  /**
   * Envoi le courriel à l'utilisateur.
   */
  public ActionForward envoyerDemande(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
    OublierMotPasseForm formulaire = (OublierMotPasseForm) form;

    // Extraire la langue de l'utilisateur.
    String langue = UtilitaireControleur.getLanguePourUtilisateur(request
        .getSession());
    try {

      // Extraire le code utilisateur
      String codeUtilisateur = formulaire.getCodeUtilisateur();

      // Extaire l'utilisateur selon son code.
      Utilisateur utilisateur = this.getModeleInfrastructure(request)
          .getServiceUtilisateur().getUtilisateur(codeUtilisateur);

      if (utilisateur != null) {

        // Générer un certificat afin de permettre l'envoi d'un URL pour le
        // changement de mot de passe.
        String certificat = GestionSecurite.getInstance()
            .getServiceAuthentification().genererCertificat(
                utilisateur.getCodeUtilisateur());

        // Extraire la base de l'url.
        String urlEnCours = request.getRequestURL().toString();

        // Génération de l'url pour permettre le changement de mot de passe.
        String urlChangerMotPasse = urlEnCours.toString().substring(0,
            urlEnCours.lastIndexOf("/") + 1)
            + "changerMotPasse.do?methode=acceder&certificat=" + certificat;

        // Extraire le libellé du sujet du message.
        Message sujetMessage = UtilitaireLibelle
            .getLibelle("login.libelle.oublier_mot_passe.sujet_courriel",
                request, null);

        // Extraire le contenu du message.
        Message contenuMessage = UtilitaireLibelle.getLibelle(
            "login.libelle.oublier_mot_passe.contenu_courriel", request,
            new Object[] { urlChangerMotPasse });
        

        // Préparation du courriel.
        CourrielHtml courriel = this.initialiserCourriel();
        courriel.ajouterTo(utilisateur.getCourriel(), utilisateur.getNom()
            + ", " + utilisateur.getPrenom());

        courriel.setSujet(sujetMessage.getMessage());

        UtilitaireVelocity velocity = new UtilitaireVelocity();
        velocity.ajouterAuContexte("contenu", contenuMessage.getMessage());
        courriel.setMessage(velocity.genererHTML("org/sofiframework/infrasofi/presentation/gabarit/courriel.vm"));
        courriel.envoyer();

        
        formulaire.ajouterMessageInformatif(
            "login.information.oublier_mot_passe.confirmer_envoi_courriel",
            request);
      }else {
        formulaire.ajouterMessageErreur("login.erreur.oublier_mot_passe_codeUtilisateur.invalide", request);  
      }

      formulaire.ajouterMessageInformatif(
          "login.information.oublier_mot_passe.confirmer_envoi_courriel",
          request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          "login.information.oublier_mot_passe.echec_envoi_courriel",
          request);
      if (log.isErrorEnabled()) {
        log.error("Erreur lors de l'envoi de courriel.", e);
      }
    }

    return afficher(mapping);
  }
  
  private CourrielHtml initialiserCourriel() {
    // Extraction des paramètres systèmes.
    String smtpServeur = GestionParametreSysteme.getInstance().getString("smtpServeur");
    String smtpUsager = GestionParametreSysteme.getInstance().getString("smtpUsager");
    String smtpMotPasse = GestionParametreSysteme.getInstance().getString("smtpMotPasse");
    Boolean smtpSecure = GestionParametreSysteme.getInstance().getBoolean("smtpSecure");
    String courrielDestinataire = GestionParametreSysteme.getInstance().getString("courrielDestinataire");

    // Préparation du courriel.
    CourrielHtml courriel = new CourrielHtml(smtpServeur, smtpUsager,smtpMotPasse, smtpSecure.booleanValue(), "UTF-8");
    courriel.setFrom(courrielDestinataire, courrielDestinataire);
    return courriel;
    
  }
  
}