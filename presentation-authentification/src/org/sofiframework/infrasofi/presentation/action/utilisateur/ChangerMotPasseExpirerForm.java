/**
 * 
 */
/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.utilisateur;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseDynaForm;

/**
 * @author Jérôme Fiolleau, Nurun inc.
 * @date 09-10-09
 *
 */
public class ChangerMotPasseExpirerForm extends BaseDynaForm {

  private static final Integer TAILLE_MINIMUM_MOT_PASSE = 8;
  /**
   * 
   */
  private static final long serialVersionUID = -7534388841093818477L;

  @Override
  public void validerFormulaire(HttpServletRequest request) {
    super.validerFormulaire(request);
    /*
    String motPasse = (String) this.get("nouveauMotPasse");
    
    if (motPasse.length() < TAILLE_MINIMUM_MOT_PASSE) {
      this.ajouterMessageErreur("auth_sofi.erreur.commun.taille_mot_passe", "nouveauMotPasse", request);
    } else {
      char[] motPasseArray = motPasse.toCharArray();
      
      boolean uppercaseExist = false;
      boolean lowercaseExist = false;
      boolean digitExist = false;
      
      for (char c : motPasseArray) {
        if (Character.isUpperCase(c)) {
          uppercaseExist = true; 
        }
        if (Character.isLowerCase(c)) {
          lowercaseExist = true; 
        }
        if (Character.isDigit(c)) {
          digitExist = true; 
        }
      }
      
      if (!uppercaseExist
          || !lowercaseExist
          || !digitExist) {
        this.ajouterMessageErreur("auth_sofi.erreur.commun.contenu_mot_passe", "nouveauMotPasse", request);
      }
    }*/
  }
}
