/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.controleur.BaseDispatchAction;
import org.sofiframework.presentation.struts.form.BaseForm;

public abstract class FormAction extends BaseDispatchAction {

  public static final String AFFICHER = "afficher";
  public static final String PAGE = "page";
  public static final String AJAX = "ajax";
  
  /**
   * Méthode utilisée pour l'accès initial à la page.
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */    
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
    BaseForm formulaire = (BaseForm) form;    
    formulaire.initialiserFormulaire(request, mapping, true);    
    formulaire.populerFormulaire(this.getNouvelObjetTransfert(request), request, true);
    return afficher(mapping);
  }
  
  /**
   * Méthode utilisée pour réafficher la page.
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */  
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
    this.traitementAvantAfficher(mapping, this.getFormulaire(form), request, response);
    return page(mapping);
  }
  
  protected BaseForm getFormulaire(ActionForm form) {
    return (BaseForm) form;
  }
  
  protected ActionForward ajax(ActionMapping mapping, HttpServletRequest request) {
    return direction((this.isAjax(request) ? AJAX : AFFICHER), mapping);
  }
  
  protected ActionForward afficher(ActionMapping mapping) {
    return direction(AFFICHER, mapping);
  }  
  
  protected ActionForward page(ActionMapping mapping) {
    return direction(PAGE, mapping);
  }
  
  protected ActionForward direction(String direction, ActionMapping mapping) {
    return mapping.findForward(direction);
  }
  
  protected void traitementAvantAfficher(ActionMapping mapping, BaseForm formulaire, 
      HttpServletRequest request, HttpServletResponse response) throws ServletException {
  }
 
  public abstract ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request);
}
