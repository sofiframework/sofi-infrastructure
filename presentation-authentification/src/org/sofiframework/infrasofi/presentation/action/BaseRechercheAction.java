/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.ObjetFiltre;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;

public abstract class BaseRechercheAction extends AuthentificationAction {
  
  private static final Log log = LogFactory.getLog(BaseRechercheAction.class);

  /**
   * Méthode utilisée pour effectuer la recherche.
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward rechercher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
      // Récupérer le filtre de recherche et la liste de navigation
      BaseForm formulaire = (BaseForm) form;

      ObjetFiltre filtre = (ObjetFiltre) formulaire.getObjetTransfert();
      if (filtre == null) {
        this.acceder(mapping, form, request, response);
        filtre = (ObjetFiltre) formulaire.getObjetTransfert();
      }      
      
      ListeNavigation liste = this.appliquerListeNavigation(request);
      liste.setObjetFiltre(filtre);
      try {
        this.effectuerRecherche(liste, formulaire, getModeleInfrastructure(request), request);
        this.setAttributTemporaireListeNavigationPourAction(getNomListeDansSession(), liste, request);
      } catch (ModeleException e) {
        if (log.isErrorEnabled()) {
          log.error("Erreur lors de la recherche " + getNomListeDansSession(), e);
        }
        formulaire.ajouterMessageErreur(e, request);
      }
      return ajax(mapping, request);
  }  

  public ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request) {
    return this.construireNouveauFiltre(request);
  }

  protected void traitementAvantAfficher(ActionMapping mapping, BaseForm formulaire, 
    HttpServletRequest request, HttpServletResponse response) 
    throws ServletException {
    if (this.isTraiterResultatRecherche(getNomListeDansSession(), request)) {
      this.rechercher(mapping, formulaire, request, response);
    }
  }

  public abstract String getNomListeDansSession();
  
  public abstract ObjetTransfert construireNouveauFiltre(HttpServletRequest request);
  
  public abstract void effectuerRecherche(ListeNavigation liste, BaseForm formulaire, ModeleInfrastructure modele, 
      HttpServletRequest request) throws ModeleException;
}
