/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.accueil;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.presentation.action.AuthentificationAction;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.constante.ConstantesParametreSysteme;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.securite.spring.context.ContexteAuthentification;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Action d'accueil.
 */
public class AccueilAction extends AuthentificationAction {

  private static final String URL_RETOUR = "url_retour";
  
  /**
   * Porte d'entrer à l'unité de traitement.
   */
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws ServletException {
     
      ActionForward direction = null;
      String urlRetour = (String) request.getParameter(URL_RETOUR);
      
      if (StringUtils.isBlank(urlRetour)) {
        urlRetour = (String) request.getSession().getAttribute(URL_RETOUR);
      }

      String nomCertificat = GestionSecurite.getInstance().getNomCookieCertificat();
      String certificat = (String) request.getSession().getAttribute(nomCertificat);
      
      if (urlRetour != null) {
        
        // Supprimer cette variable de la session
        request.getSession().removeAttribute(URL_RETOUR);
        
        try {
          urlRetour = urlRetour + (urlRetour.indexOf("?") != -1 ? "&" : "?") + nomCertificat + "=" + certificat;
          response.sendRedirect(urlRetour);
        } catch (IOException e) {
          throw new ServletException("Erreur de redirection.", e);
        }
        direction = null;
      } else {
        AccueilForm formulaire = (AccueilForm) form;
        // Fixer l'application sélectionné et la conservé dans la session pour
        // sélection
        // par défaut des autres services.
        if (formulaire.getCodeApplication() != null) {
          request.getSession().setAttribute("systeme_selectionne", formulaire.getCodeApplication());
        }
        ModeleInfrastructure modele = (ModeleInfrastructure) this.getModele(request);
        ListeNavigation liste = this.appliquerListeNavigation(request);
        ListeNavigation listeApplication = modele.getServiceApplication().getListeApplicationAccessible(liste);
        
        if (liste.getListe() != null) {
          for (Iterator iterator = liste.getListe().iterator(); iterator.hasNext();) {
            Application application = (Application) iterator.next();
            String url = application.getAdresseSite() + 
                        (application.getAdresseSite().indexOf("?") != -1 ? "&" : "?") 
                        + GestionSecurite.getInstance().getNomCookieCertificat() + "=" + certificat;
            application.setAdresseSite(url);
          }
        }
        request.getSession().setAttribute("listeApplication", listeApplication.getListe());        
        
        direction = afficher(mapping);
      }
      return direction;
    }

    public ActionForward afficher(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws ServletException {
      AccueilForm formulaire = (AccueilForm) form;
      formulaire.setCodeApplication(getApplicationSelectionne(request));
      acceder(mapping, form, request, response);
      return page(mapping);
    }

    public ObjetTransfert getNouvelObjetTransfert(HttpServletRequest request) {
      return null;
    }
}
