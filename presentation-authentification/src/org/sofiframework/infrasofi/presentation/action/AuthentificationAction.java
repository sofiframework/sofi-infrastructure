/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.sofiframework.infrasofi.modele.ModeleInfrastructure;

/**
 * Action abstraite de l'application de gestion de l'infrastructure.
 * 
 * @author Jean-Maxime Pelletier
 */
public abstract class AuthentificationAction extends FormAction {
  
  private static final Log log = LogFactory.getLog(AuthentificationAction.class);
  
  /**
   * Obtenir le modèle.
   * @param request Requete HTTP
   * @return Modele
   */
  public ModeleInfrastructure getModeleInfrastructure(HttpServletRequest request) {
    return (ModeleInfrastructure) this.getModele(request);
  }

  /**
   * Écrire une entrée dans le journal.
   * 
   * @param request Requête HTTP
   * @param mode mode du Journal (type de log)
   * @param detail Détail de l'entrée
   * @param parametres Paramètre
   */
  public void ecrireJournal(HttpServletRequest request, String mode, String detail, String[] parametres) {
    try {
      super.ecrireJournal(request, mode, detail, parametres);
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour journaliser.", e);
      }
    }
  }

  /**
   * Obtenir le code d'application sélectionné par l'utilisateur.
   * @param request Requête HTTP
   * @return Code de l'application
   */
  public String getApplicationSelectionne(HttpServletRequest request) {
    return (String) request.getSession().getAttribute("systeme_selectionne");
  }
  

}
