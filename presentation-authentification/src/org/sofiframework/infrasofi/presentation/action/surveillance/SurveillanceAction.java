/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.surveillance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.surveillance.ErreurApplication;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.application.surveillance.UtilisateurSession;
import org.sofiframework.presentation.struts.controleur.BaseDispatchAction;


public class SurveillanceAction extends BaseDispatchAction {
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
    HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
    GestionSurveillance.getInstance().initialiserListeUtilisateur();

    return mapping.findForward("afficher");
  }

  public ActionForward afficher(ActionMapping mapping, ActionForm form,
    HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
    String departApplication = GestionSurveillance.getInstance()
                                                  .getDateDepartApplication();
    int nbPageConsulte = GestionSurveillance.getInstance().getNbPageConsulte();
    int nbPageEchec = GestionSurveillance.getInstance().getNbPageEnEchec();
    int nbUtilisateur = GestionSurveillance.getInstance()
                                           .getNbUtilisateurAuthentifierEnLigne();
    int nbUtilisateurTotal = GestionSurveillance.getInstance()
                                                .getNbTotalUtilisateurAuthentifie();

    request.setAttribute("departApplication", departApplication);
    request.setAttribute("nbPageConsulte", new Integer(nbPageConsulte));
    request.setAttribute("nbPageEchec", new Integer(nbPageEchec));
    request.setAttribute("nbUtilisateur", new Integer(nbUtilisateur));
    request.setAttribute("nbUtilisateurTotal", new Integer(nbUtilisateurTotal));

    return mapping.findForward("page");
  }

  public ActionForward afficherListeUtilisateur(ActionMapping mapping,
    ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
    List listeUS = GestionSurveillance.getInstance().getListeUtilisateur();
    
    List listeUtilisateur = null;
    
    if (listeUS != null && listeUS.size() > 0) {
      listeUtilisateur = new ArrayList();
      for (Iterator i = listeUS.iterator(); i.hasNext();) {
        UtilisateurSession us = (UtilisateurSession) i.next();
        UtilisateurSession temp = new UtilisateurSession();
        temp.setUtilisateur(us.getUtilisateur());
        temp.setAdresseIP(us.getAdresseIP());
        temp.setDateDerniereAcces(us.getDateDerniereAcces());
        temp.setDateCreationSession(us.getDateCreationSession());
        temp.setNavigateur(us.getNavigateur());
        listeUtilisateur.add(temp); 
      }
    }
    
    request.setAttribute("listeUtilisateur", listeUtilisateur);
    return mapping.findForward(isAjax(request) ? "ajax_liste_utilisateur" : "page_liste_utilisateur");
  }

  public ActionForward afficherListeErreur(ActionMapping mapping,
    ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
    ArrayList listeErreur = GestionSurveillance.getInstance()
                                               .getListePageEnEchec();

    request.setAttribute("listeErreur", listeErreur);

    return mapping.findForward("page_liste_erreur");
  }

  public ActionForward initialiserPageErreur(ActionMapping mapping,
    ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
    GestionSurveillance.getInstance().initialiserListePageEnEchec();

    return mapping.findForward("afficher");
  }

  public ActionForward afficherErreur(ActionMapping mapping, ActionForm form,
    HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
    String no = request.getParameter("no");

    ErreurApplication erreur = (ErreurApplication) GestionSurveillance.getInstance()
                                                                      .getListeHMPageEnEchec()
                                                                      .get(new Integer(
          no));
    request.setAttribute("utilisateurErreur", erreur.getUtilisateur());
    request.setAttribute("action", erreur.getNomPageErreur());
    request.setAttribute("methode", erreur.getMethode());
    request.setAttribute("message", erreur.getMessage());
    request.setAttribute("dateHeure", erreur.getDateHeureString());

    return mapping.findForward("page_detail_erreur");
  }

  public ActionForward afficherUtilisateur(ActionMapping mapping,
    ActionForm form, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
    String codeUtilisateur = request.getParameter("codeUtilisateur");

    if (codeUtilisateur != null) {
      setAttributTemporairePourAction("codeUtilisateurEnTraitement",
        codeUtilisateur, request);
    } else {
      codeUtilisateur = (String) request.getSession().getAttribute("codeUtilisateurEnTraitement");
    }

    String codeErreur = request.getParameter("codeErreur");
    Utilisateur utilisateur = null;
    UtilisateurSession utilisateurSession = null;

    if (codeErreur == null) {
      utilisateurSession = (UtilisateurSession) GestionSurveillance.getInstance()
                                                                   .getListeHMUtilisateur()
                                                                   .get(codeUtilisateur);
      utilisateur = (Utilisateur) utilisateurSession.getUtilisateur();
      request.setAttribute("utilisateurSession", utilisateurSession);
    } else {
      ErreurApplication erreur = (ErreurApplication) GestionSurveillance.getInstance()
                                                                        .getListeHMPageEnEchec()
                                                                        .get(new Integer(
            codeErreur));
      utilisateurSession = (UtilisateurSession) erreur.getUtilisateur();
      utilisateur = (Utilisateur) utilisateurSession.getUtilisateur();
    }

    try {
      request.setAttribute("utilisateurDetail", utilisateur);
      request.setAttribute("utilisateurSession", utilisateurSession);
      request.setAttribute("listeRoles",
        utilisateur.getListeRole(GestionSecurite.getInstance()
                                                .getCodeApplication()));
    } catch (Exception e) {
    }

    return mapping.findForward("page_detail_utilisateur");
  }

  public ActionForward afficherUtilisateurEntete(ActionMapping mapping,
    ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    request.getSession().setAttribute("listeUtilisateurAccueil",
      GestionSurveillance.getInstance().getListeUtilisateurEnLigneUnique());

    return mapping.findForward("liste_utilisateur_entete");
  }

  public ActionForward rafraichirUtilisateurEntete(ActionMapping mapping,
    ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    ajouterValeurDansReponse("idNbUtilisateurEnLigne",
      String.valueOf(GestionSurveillance.getInstance()
                                        .getListeUtilisateurEnLigneUnique()
                                        .size()), request, response);

    genererReponseXMLAvecValeurs(request, response);

    return null;
  }
}
