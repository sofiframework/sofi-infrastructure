package org.sofiframework.infrasofi.api.v1;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.annotate.JsonUnwrapped;
import org.codehaus.jackson.map.JsonDeserializer;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.application.securite.service.ServiceSecurite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

@Controller
@RequestMapping("/v1")
public class AuthentificationApi {

  @Autowired
  private ServiceAuthentification serviceAuthentification;

  @Autowired
  private ServiceSecurite serviceSecurite;

  @RequestMapping("ping")
  @ResponseBody
  public String ping() {
    return "ok";
  } 

  @RequestMapping("/certificat/{codeUtilisateur}")
  @ResponseBody
  public String genererCertificat(@PathVariable String codeUtilisateur,
      HttpServletRequest request) {
    Integer nbJours = getIntParam("nbJourExpiration", 1, request);
    String ip = request.getParameter("ip");
    String userAgent = request.getParameter("userAgent");
    String urlReferer = request.getParameter("urlReferer");

    String certificat = this.serviceAuthentification.genererCertificat(
        codeUtilisateur, nbJours, ip, userAgent, urlReferer);

    return certificat;
  }

  @RequestMapping(value = "/certificat/{codeUtilisateur}", method = RequestMethod.DELETE)
  public void terminerCertificat(@PathVariable String codeUtilisateur) {
    this.serviceAuthentification.terminerCertificatPourCode(codeUtilisateur);
  }

  @RequestMapping("/application/{codeApplication}/utilisateur/{codeUtilisateur:.+}")
  @ResponseBody
  public String getUtilisateur(
      @PathVariable String codeApplication,
      @PathVariable String codeUtilisateur) {
    Utilisateur utilisateur = this.serviceSecurite.getUtilisateurAvecObjetsSecurises(codeApplication, codeUtilisateur);
    return utilisateur.toString();
  }

  private Integer getIntParam(String nom, int defaut, HttpServletRequest request) {
    Integer nombre = defaut;
    String valeur = request.getParameter(nom);

    if (valeur != null) {
      try {
        nombre = Integer.parseInt(valeur);
      } catch (Exception e) {
        // on laisse la valeur par défaut
      }
    }

    return nombre;
  }
}