/*
 * Copyright 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.application.parametresysteme.service.test;

import org.sofiframework.application.parametresysteme.service.ServiceParametreSysteme;
import org.sofiframework.test.BaseTest;

/**
 * Les cas d'essais sur le service des utilisateurs de l'infrastructure SOFI.
 * <p>
 * 
 * @author Jean-Francois Brassard
 */
public class ServiceParametreSystemeTest extends BaseTest {

  /**
   * Essai d'extraction d'un parametre systeme.
   */
  public void testModifierParametre() {
    ServiceParametreSysteme serviceParametre = getServiceParametreSysteme();

    // Extraire une valeur de parametre.
    String valeur = serviceParametre.getValeur("INFRA_SOFI", "testModifiable");
    // On doit avoir le cas de test.
    assertNotNull(valeur);
    // La valeur initiale est 0.
    assertEquals("0", valeur);
    // Essayer de modifier ca
    serviceParametre.enregistrerValeur("INFRA_SOFI", "testModifiable", "1");
    String valeurModifiee = serviceParametre.getValeur("INFRA_SOFI", "testModifiable");
    assertNotNull(valeurModifiee);
    assertEquals("1", valeurModifiee);

    // Rétablir la valeur 0.
    serviceParametre.enregistrerValeur("INFRA_SOFI", "testModifiable", "0");
  }

  public void testModifierParametreNonModifiable() {
    ServiceParametreSysteme serviceParametre = getServiceParametreSysteme();

    // Extraire une valeur de parametre.
    String valeur = serviceParametre.getValeur("INFRA_SOFI", "testNonModifiable");
    // On doit avoir le cas de test.
    assertNotNull(valeur);
    // La valeur initiale est 0.
    assertEquals("0", valeur);

    // Essayer de modifier ca
    Boolean succes = Boolean.TRUE;
    try {
      serviceParametre.enregistrerValeur("INFRA_SOFI", "testNonModifiable", "1");
      // Extraire une valeur de parametre.
      String valeurModifiee = serviceParametre.getValeur("INFRA_SOFI", "testNonModifiable");
      // On doit avoir le cas de test.
      assertNotNull(valeurModifiee);
      // La valeur initiale est 0.
      assertEquals("0", valeurModifiee);
    } catch (Exception e) {
      succes = Boolean.FALSE;
    }

    assertFalse(succes);
  }

  public void testGetParametreAvecHeritage() {
    String codeApplication = "INFRA_SOFI";
    String codeParametre = "authentificationUrl";

    ServiceParametreSysteme service = this.getServiceParametreSysteme();
    String valeur = service.getValeur(codeApplication, codeParametre);

    assertNotNull(valeur);
  }

  /**
   * Essai de sauvegarde d'un parametre systeme.
   * 
   * @todo Implémentation non réalisé.
   */
  public void testEnregistrerParametre() {
    // Acces au service.
    ServiceParametreSysteme serviceParametre = getServiceParametreSysteme();

    // Extraire une valeur de parametre.
    // serviceParametre.enregistrerValeur(CODE_APPLICATION, CODE_PARAMETRE,
    // VALEUR)

    // Les cas d'essais.
    // assertNotNull("Essai infrastructure 'ServiceParametre' : Probleme d'extraction d'un parametre",
    // valeur);
  }

}
