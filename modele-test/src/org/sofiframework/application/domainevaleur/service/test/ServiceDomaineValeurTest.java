/*
 * Copyright 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.application.domainevaleur.service.test;

import java.util.List;

import org.sofiframework.application.domainevaleur.filtre.FiltreDomaineValeur;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.application.domainevaleur.service.ServiceDomaineValeur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.test.BaseTest;

/**
 * Les cas d'essais sur le service des domaines de valeurs de l'infrastructure
 * SOFI.
 * <p>
 * 
 * @author Jean-Francois Brassard
 */
public class ServiceDomaineValeurTest extends BaseTest {
  private static String CODE_APPLICATION = "INFRA_SOFI";

  /**
   * Essai d'extraction d'un domaine de valeur.
   */
  public void testGetDomaineValeur() {
    // Acces au service.
    ServiceDomaineValeur service = getServiceDomaineValeur();

    // Test d'extraction d'une definition d'un domaine.
    DomaineValeur domaineValeur = service.getDomaineValeur(CODE_APPLICATION, "LISTE_TYPE_MESSAGE", null, null);

    // Les cas d'essais
    assertNotNull("Essai infrastructure 'ServiceDomaineValeur' : Probleme d'extraction d'une definition de domaine de valeurs", domaineValeur);
  }

  /**
   * Essai d'extraction d'une liste de valeur specifique pour une langue.
   */
  public void testGetListeValeurParLangue() {
    // Acces au service.
    ServiceDomaineValeur service = getServiceDomaineValeur();

    // Test du service de la liste de valeur par langue.
    List listeValeurParLangue = service.getListeValeur(CODE_APPLICATION, "LISTE_TYPE_MESSAGE", null, "FR");

    // Les cas d'essais
    assertNotNull("Essai infrastructure 'ServiceDomaineValeur' : Probleme de liste de valeurs par langue", listeValeurParLangue);
  }

  /**
   * Essai d'extraction d'une liste de valeur depuis une liste de navigation et
   * un objet de filtrage.
   */
  public void testGetListeValeurAvecFiltre() {
    // Acces au service.
    ServiceDomaineValeur service = getServiceDomaineValeur();

    // Test du service avec liste de navigation.
    ListeNavigation listeNavigationValeur = new ListeNavigation(1, -1);

    // Creation de l'objet de filtrage
    FiltreDomaineValeur filtre = new FiltreDomaineValeur();
    filtre.setCodeApplication(CODE_APPLICATION);
    filtre.setCodeLangue("FR");
    filtre.setNom("LISTE_TYPE_MESSAGE");

    listeNavigationValeur.setObjetFiltre(filtre);
    listeNavigationValeur.ajouterTriInitial("description", true);

    listeNavigationValeur = service.getListeValeur(listeNavigationValeur);

    assertNotNull("Essai infrastructure 'ServiceDomaineValeur' : Probleme de liste de valeurs avec filtre", listeNavigationValeur);
  }
}
