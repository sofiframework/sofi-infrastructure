/*
 * Copyright 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.application.libelle.service.test;

import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.service.ServiceLibelle;
import org.sofiframework.test.BaseTest;

/**
 * Les cas d'essais sur le service des libelles de l'infrastructure SOFI.
 * <p>
 * 
 * @author Jean-Francois Brassard
 */
public class ServiceLibelleTest extends BaseTest {
  /**
   * Essai d'extraction d'un libelle.
   */
  public void testGetLibelle() {
    // Acces au service.
    ServiceLibelle serviceLibelle = getServiceLibelle();

    // Extraire une valeur de parametre.
    Libelle libelle = serviceLibelle.get("infra_sofi.libelle.gestion_libelle.onglet.recherche", "fr_CA", null);

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceLibelle' : Probleme d'extraction d'un libelle", libelle);
  }

  public void testGetLibelleClient() {
    // Acces au service.
    ServiceLibelle serviceLibelle = getServiceLibelle();
    Libelle libelle = null;
    String message = null;

    libelle = serviceLibelle.get("infra_sofi.libelle.surveillance.service", "fr_CA", null);
    assertNotNull(libelle);
    message = libelle.getMessage();
    assertFalse(message.startsWith("CLI"));

    libelle = serviceLibelle.get("infra_sofi.libelle.surveillance.service", "fr_CA", "CLI2");
    assertNotNull(libelle);
    message = libelle.getMessage();
    assertTrue(message.startsWith("CLI2 - "));

    libelle = serviceLibelle.get("infra_sofi.libelle.surveillance.service", "fr_CA", "CLI3");
    assertNotNull(libelle);
    message = libelle.getMessage();
    assertTrue(message.startsWith("CLI - "));
  }
}
