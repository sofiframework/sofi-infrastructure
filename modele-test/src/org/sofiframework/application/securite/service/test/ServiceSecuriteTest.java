/*
 * Copyright 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.application.securite.service.test;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.service.ServiceSecurite;
import org.sofiframework.test.BaseTest;

/**
 * Les cas d'essais sur le service de securite de l'infrastructure SOFI.
 * <p>
 * 
 * @author Jean-Francois Brassard
 * @version 3.0
 */
public class ServiceSecuriteTest extends BaseTest {
  private static String CODE_APPLICATION = "INFRA_SOFI";
  private static String CODE_UTILISATEUR = "sofi";
  
  //TODO REFAIRE CE SCÉNARIO
  private static String CODE_APPLICATION_FACETTE = "?";
  private static String CODE_APPLICATION_CLIENT = "?";
  private static String CODE_FACETTE = "ACCUEIL";
  private static String CODE_UTILISATEUR_FACETTE = "mr_bidon";
  private static String CODE_UTILISATEUR_CLIENT = "test";
  private static String CODE_CLIENT = "test";

  /**
   * Essai d'extraction d'un utilisateur avec sa securite applicative.
   */
  public void testGetUtilisateurAvecObjetSecurises() {
    // Acces au service.
    ServiceSecurite serviceSecurite = getServiceSecurite();

    // Extraire un utilisateur avec sa securite applicative.
    Utilisateur utilisateur = serviceSecurite.getUtilisateurAvecObjetsSecurises(CODE_APPLICATION, CODE_UTILISATEUR);

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceSecurite' : Probleme d'extraction d'un utilisateur", utilisateur);
    assertTrue("Essai infrastructure 'ServiceSecurite' : Probleme d'extraction de la liste des acces de l'utilisateur",
        (utilisateur.getListeObjetSecurisables() != null) && (utilisateur.getListeObjetSecurisables().size() > 0));
    assertTrue(utilisateur.getListeObjetSecurisables().size() > 0);
  }

  /**
   * Essai d'extraction d'un utilisateur avec sa securite applicative.
   */
  public void _testGetUtilisateurAvecObjetSecurisesAvecFacette() {
    // Acces au service.
    ServiceSecurite serviceSecurite = getServiceSecurite();

    // Extraire un utilisateur avec sa securite applicative.
    Utilisateur utilisateur = serviceSecurite.getUtilisateurAvecObjetsSecurises(CODE_APPLICATION_FACETTE, CODE_FACETTE, CODE_UTILISATEUR_FACETTE);

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceSecurite' : Probleme d'extraction d'un utilisateur", utilisateur);
    assertTrue("Essai infrastructure 'ServiceSecurite' : Probleme d'extraction de la liste des acces de l'utilisateur",
        (utilisateur.getListeObjetSecurisables() != null) && (utilisateur.getListeObjetSecurisables().size() > 0));
    assertTrue(utilisateur.getListeObjetSecurisables().size() > 0);
  }

  /**
   * Essai d'extraction d'un utilisateur avec sa securite applicative.
   */
  public void _testGetUtilisateurAvecObjetSecurisesSelonClient1() {
    // Acces au service.
    ServiceSecurite serviceSecurite = getServiceSecurite();

    // Extraire un utilisateur avec sa securite applicative.
    Utilisateur utilisateur = serviceSecurite.getUtilisateurSecuriseSelonClient(CODE_APPLICATION_CLIENT, CODE_UTILISATEUR_CLIENT, CODE_CLIENT);

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceSecurite' : Probleme d'extraction d'un utilisateur", utilisateur);

    assertTrue("Essai infrastructure 'ServiceSecurite' : Probleme d'extraction de la liste des acces de l'utilisateur",
        (utilisateur.getListeObjetSecurisables() != null) && (utilisateur.getListeObjetSecurisables().size() > 0));
    assertTrue(utilisateur.getListeObjetSecurisables().size() > 0);
  }
}
