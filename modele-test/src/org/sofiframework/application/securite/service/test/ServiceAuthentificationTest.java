/*
 * Copyright 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.application.securite.service.test;

import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.test.BaseTest;

/**
 * Les cas d'essais sur le service d'authentification de l'infrastructure SOFI.
 * <p>
 * 
 * @author Jean-Francois Brassard
 */
public class ServiceAuthentificationTest extends BaseTest {
  /**
   * Essai de la verification qu'un certificat a bien ete terminer.
   */
  private void testTerminerCertificat(String certificat) {
    // Acces au service.
    ServiceAuthentification service = getServiceAuthentification();
    
    service.terminerCertificat(certificat);

    String codeUtilisateur = service.getCodeUtilisateur(certificat);

    // Les cas d'essais.
    assertNull("Essai infrastructure 'ServiceAuthentification' : Probleme de terminaison d'un certificat", codeUtilisateur);
  }

  /**
   * Essai de l'extraction d'un code depuis un certficat valide.
   */
  private void testGetCodeUtilisateur(String certificat) {
    // Acces au service.
    ServiceAuthentification service = getServiceAuthentification();

    String codeUtilisateur = service.getCodeUtilisateur(certificat);

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceAuthentification' : Probleme de d'extraction d'un certificat", codeUtilisateur);
  }

  /**
   * Essai de la generation d'un certificat.
   */
  public void testGenererCerticat() {
    // Acces au service.
    ServiceAuthentification service = getServiceAuthentification();

    String certificat = service.genererCertificat("sofi",Integer.valueOf("30"), "201.102.102.012","TEST_AGENT");

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceAuthentification' : Probleme de generation de certificat", certificat);

    testGetCodeUtilisateur(certificat);
    testTerminerCertificat(certificat);
  }
}
