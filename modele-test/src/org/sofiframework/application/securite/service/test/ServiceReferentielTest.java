/*
 * Copyright 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.application.securite.service.test;

import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.service.ServiceObjetSecurisable;
import org.sofiframework.test.BaseTest;

/**
 * Les cas d'essais sur le service du referentiel de l'infrastructure SOFI.
 * <p>
 * 
 * @author Jean-Francois Brassard
 */
public class ServiceReferentielTest extends BaseTest {
  private static String CODE_APPLICATION = "INFRA_SOFI";

  /**
   * Essai d'extraction du referentiel d'une application.
   */
  /**
   * Test d'extraction d'une appplication avec son referentiel.
   */
  public void testGetApplicationAvecReferentiel() {
    // Acces au service.
    ServiceObjetSecurisable service = getServiceObjetSecurisable();

    // Extraire une application.
    Application application = service.getApplicationAvecObjetSecurisable(CODE_APPLICATION, false);

    // Les cas d'essais
    assertNotNull("Essai infrastructure 'ServiceReferentiel' : Probleme d'extraction d'une application avec son referentiel",
        application.getListeObjetSecurisables());
  }
}
