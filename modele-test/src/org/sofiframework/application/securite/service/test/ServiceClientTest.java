/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.securite.service.test;

import java.util.List;

import org.sofiframework.application.securite.objetstransfert.Client;
import org.sofiframework.test.BaseTest;
import org.sofiframework.utilitaire.UtilitaireListe;

/**
 * Classe de test du service commun des clients.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 * 
 */
public class ServiceClientTest extends BaseTest {

  @SuppressWarnings("unchecked")
  public void testGetListeClientDescendant() {
    List<Client> liste = getServiceClient().getListeClientDescendant(null);
    liste = UtilitaireListe.getListeParentEnfantsEn1Niveau(liste, "listeClientEnfant");
    assertEquals(5, liste.size());

    liste = getServiceClient().getListeClientDescendant("CLI");
    liste = UtilitaireListe.getListeParentEnfantsEn1Niveau(liste, "listeClientEnfant");
    assertEquals(4, liste.size());

    liste = getServiceClient().getListeClientDescendant("CLI3");
    liste = UtilitaireListe.getListeParentEnfantsEn1Niveau(liste, "listeClientEnfant");
    assertTrue(liste.isEmpty());

    liste = getServiceClient().getListeClientDescendant("CLI1");
    liste = UtilitaireListe.getListeParentEnfantsEn1Niveau(liste, "listeClientEnfant");
    assertEquals(1, liste.size());
  }

  public void testGetListeClientAscendant() {
    List<Client> liste = null;

    liste = getServiceClient().getListeClientAscendant(null);
    assertTrue(liste.isEmpty());

    liste = getServiceClient().getListeClientAscendant("CLI");
    assertTrue(liste.isEmpty());

    liste = getServiceClient().getListeClientAscendant("CLI11");
    assertFalse(liste.isEmpty());
    assertTrue(liste.size() == 2);
  }
}
