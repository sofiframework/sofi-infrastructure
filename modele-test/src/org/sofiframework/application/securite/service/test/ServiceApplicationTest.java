/*
 * Copyright 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.application.securite.service.test;


import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.service.ServiceApplication;
import org.sofiframework.test.BaseTest;

/**
 * Les cas d'essais sur le service des applications de l'infrastructure SOFI.
 * <p>
 * 
 * @author Jean-François Brassard
 */
public class ServiceApplicationTest extends BaseTest {
  private static String CODE_APPLICATION = "INFRA_SOFI";

  /**
   * Essai d'extraction du référentiel d'une application.
   */
  public void testGetApplication() {
    // Accès au service.
    ServiceApplication service = getServiceApplication();

    // Extraire l'application INFRA_SOFI.
    Application application = service.getApplication(CODE_APPLICATION);

    // Les cas d'essais
    assertNotNull("Essai infrastructure 'ServiceApplication' : Probleme d'extraction d'une application", application);
  }
}
