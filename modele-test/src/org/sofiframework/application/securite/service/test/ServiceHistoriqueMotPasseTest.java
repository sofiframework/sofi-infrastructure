package org.sofiframework.application.securite.service.test;

import java.util.List;

import org.sofiframework.application.securite.objetstransfert.ChangementMotPasse;
import org.sofiframework.application.securite.objetstransfert.HistoriqueMotPasse;
import org.sofiframework.application.securite.service.ServiceHistoriqueMotPasse;
import org.sofiframework.application.securite.service.ServiceUtilisateur;
import org.sofiframework.test.BaseTest;

public class ServiceHistoriqueMotPasseTest extends BaseTest {

  public void testgetListeHistoriqueMotPasse() {
    
    ServiceUtilisateur serviceUtilisateur = (ServiceUtilisateur) this.getBean("serviceUtilisateurCommun");

    ChangementMotPasse changement = new ChangementMotPasse();
    changement.setCodeUtilisateur("sofi");
    changement.setNouveauMotPasse("patate");
    changement.setConfirmerMotPasse("patate");
    serviceUtilisateur.changerMotPasseExpirer(changement);

    ServiceHistoriqueMotPasse serviceHistoriqueMotPasse = (ServiceHistoriqueMotPasse) this.getBean("serviceHistoriqueMotPasse");

    List<HistoriqueMotPasse> liste = serviceHistoriqueMotPasse.getListeHistoriqueMotPasse("sofi");

    assertNotNull(liste);
    assertTrue(liste.size() > 0);
  }
  
  
}
