/*
 * Copyright 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.application.securite.service.test;

import java.util.ArrayList;
import java.util.Locale;

import org.sofiframework.application.securite.filtre.FiltreUtilisateur;
import org.sofiframework.application.securite.objetstransfert.Propriete;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.objetstransfert.UtilisateurRole;
import org.sofiframework.application.securite.service.ServiceUtilisateur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.test.BaseTest;
import org.sofiframework.utilitaire.UtilitaireXml;

/**
 * Les cas d'essais sur le service des utilisateurs de l'infrastructure SOFI.
 * <p>
 * 
 * @author Jean-Francois Brassard
 */
public class ServiceUtilisateurTest extends BaseTest {
  private static String CODE_UTILISATEUR = "sofi";
  private static String CODE_APPLICATION = "INFRA_SOFI";
  private static String CODE_ROLE = "PILOTE";
  private static String CODE_ROLE_ASSOCIATION = "ANALYSTE";

  public void testgetUtilisateurAvecObjetSecurise() {
    Utilisateur utilisateur = this.getServiceSecurite().getUtilisateurAvecObjetsSecurises("INFRA_SOFI", "sofi");
    assertNotNull(utilisateur);
  }

  /**
   * Essai dans l'extration d'un liste d'utilisateur depuis un objet de
   * filtrage.
   */
  public void testGetListeUtilisateur() {
    // Acces au service.
    ServiceUtilisateur serviceUtilisateur = getServiceUtilisateur();

    // Declaration de l'objet de filtrage
    FiltreUtilisateur filtre = new FiltreUtilisateur();

    filtre.ajouterRechercheAvantApresValeur(new String[] { "nom", "prenom" });
    filtre.setRoleConsultationSeulement(null);
    filtre.setRoleActifSelonPeriode(Boolean.valueOf(true));
    filtre.setCodeRole(new String[] { CODE_ROLE, "DEVELOPPEUR" });
    filtre.setCodeApplication(new String[] { CODE_APPLICATION, "LOGIN" });
    filtre.setId(new Long[]{new Long(1)});
    
    System.out.println(UtilitaireXml.toXML(filtre));

    ListeNavigation liste = new ListeNavigation(1, -1);
    liste.setObjetFiltre(filtre);

    liste = serviceUtilisateur.getListeUtilisateur(liste);

    Utilisateur utilisateur = serviceUtilisateur.getUtilisateur(CODE_UTILISATEUR);

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceUtilisateur' : Probleme de recherche d'utilisateur", liste.getListe());

    assertNotNull("Essai infrastructure 'ServiceUtilisateur' : Utilisateur inexistant", utilisateur);
    assertNotNull("Essai infrastructure 'ServiceUtilisateur' : Aucun role pour l'utilisateur", utilisateur.getListeRoles());
  }

  /**
   * Essai qui permet de dissocier un role d'un utilisateur.
   */
  public void testDissocierRole() {
        
    // Acces au service.
    ServiceUtilisateur service = getServiceUtilisateur();

    UtilisateurRole utilisateurRole = new UtilisateurRole();
    utilisateurRole.setCodeApplication(CODE_APPLICATION);
    utilisateurRole.setCodeRole(CODE_ROLE_ASSOCIATION);
    utilisateurRole.setCodeUtilisateur(CODE_UTILISATEUR);
    
    service.associerRole(utilisateurRole);

    Utilisateur utilisateur = getServiceSecurite().getUtilisateurAvecObjetsSecurises(CODE_APPLICATION, CODE_UTILISATEUR);
    
    boolean utilisateurARole = utilisateur.isPossedeRole(CODE_APPLICATION, CODE_ROLE_ASSOCIATION);
    
    assertTrue(utilisateurARole);
    
    service.dissocierRole(utilisateurRole);

    // Les cas d'essais.
    utilisateur = getServiceUtilisateur().getUtilisateur(CODE_UTILISATEUR);
    assertNotNull(utilisateur);
    assertFalse(utilisateur.isPossedeRole(CODE_APPLICATION, CODE_ROLE_ASSOCIATION));
  }

  /**
   * Essai qui permet d'associer un role a un utilisateur.
   */
  public void testAssocierRole() {
    // Acces au service.
    ServiceUtilisateur service = getServiceUtilisateur();

    UtilisateurRole utilisateurRole = new UtilisateurRole();
    utilisateurRole.setCodeApplication(CODE_APPLICATION);
    utilisateurRole.setCodeRole(CODE_ROLE_ASSOCIATION);
    utilisateurRole.setCodeUtilisateur(CODE_UTILISATEUR);
    utilisateurRole.setDateDebutActivite(new java.util.Date());
    utilisateurRole.setDateFinActivite(null);
    utilisateurRole.setCreePar("system");
    utilisateurRole.setDateCreation(new java.util.Date());

    service.associerRole(utilisateurRole);

    Utilisateur utilisateur = getServiceUtilisateur().getUtilisateur(CODE_UTILISATEUR);

    // Les cas d'essais.
    assertNotNull(utilisateur);
  }

  public void testAjouterUtilisateur() {
    Utilisateur nouvelUtilisateur = new Utilisateur();
    nouvelUtilisateur.setCodeUtilisateur(null);
    nouvelUtilisateur.setPrenom("¨Prenom");
    nouvelUtilisateur.setNom("nom");
    nouvelUtilisateur.setCourriel("courriel");
    nouvelUtilisateur.setCodeFournisseurAcces("GOOGLE");
    nouvelUtilisateur.setLocale(Locale.CANADA_FRENCH);
    nouvelUtilisateur.setMotPasse("temp");

    Propriete p = new Propriete();
    p.setMetadonneeProprieteId(1L);
    p.setValeur("false");

    nouvelUtilisateur.setListePropriete(new ArrayList<Propriete>());
    nouvelUtilisateur.getListePropriete().add(p);

    nouvelUtilisateur = getServiceUtilisateur().ajouterUtilisateur(nouvelUtilisateur);

    assertNotNull(nouvelUtilisateur.getCodeUtilisateur());
    assertNotNull(nouvelUtilisateur.getId());
  }

  public void testGetUtilisateur() {
    Utilisateur utilisateur1 = getServiceUtilisateur().getUtilisateur("sofi");

    Utilisateur utilisateur2 = getServiceUtilisateur().getUtilisateurPourId(new Long(1));

    assertNotNull(utilisateur1.getCodeUtilisateur());
    assertNotNull(utilisateur1.getId());

    assertNotNull(utilisateur2.getCodeUtilisateur());
    assertNotNull(utilisateur2.getId());
  }
}
