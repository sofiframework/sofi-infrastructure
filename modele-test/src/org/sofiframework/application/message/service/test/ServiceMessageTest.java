/*
 * Copyright 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.application.message.service.test;

import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.message.service.ServiceMessage;
import org.sofiframework.test.BaseTest;

/**
 * Les cas d'essais sur le service des messages de l'infrastructure SOFI.
 * <p>
 * 
 * @author Jean-Francois Brassard
 */
public class ServiceMessageTest extends BaseTest {
  /**
   * Essai d'extraction d'un message.
   */
  public void testGetMessage() {
    // Acces au service.
    ServiceMessage serviceMessage = getServiceMessage();

    // Extraire une valeur du message.
    Message message = serviceMessage.get("infra_sofi.erreur.gestion_utilisateur.code.utilisateur", "fr_CA", null);

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceMessage' : Probleme d'extraction d'un message", message);
  }

  public void testGetMessageClient() {
    // Acces au service.
    ServiceMessage serviceMessage = getServiceMessage();
    Message objetMessage = null;
    String message = null;

    objetMessage = serviceMessage.get("infra_sofi.libelle.gestion.application.gestion.cache.avertissement", "fr_CA",
        null);
    assertNotNull(objetMessage);
    message = objetMessage.getMessage();
    assertFalse(message.startsWith("CLI"));

    objetMessage = serviceMessage.get("infra_sofi.libelle.gestion.application.gestion.cache.avertissement", "fr_CA",
        "CLI2");
    assertNotNull(objetMessage);
    message = objetMessage.getMessage();
    assertTrue(message.startsWith("CLI2 - "));

    objetMessage = serviceMessage.get("infra_sofi.libelle.gestion.application.gestion.cache.avertissement", "fr_CA",
        "CLI3");
    assertNotNull(objetMessage);
    message = objetMessage.getMessage();
    assertTrue(message.startsWith("CLI - "));

  }
}
