/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.service.test;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.sofiframework.application.rapport.objetstransfert.RapportResultatAgrege;
import org.sofiframework.test.BaseTest;
import org.sofiframework.utilitaire.Periode;

/**
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 * 
 */
public class ServiceRapportResultatTest extends BaseTest {

  public void testGetListeRapportResultatAgrege() throws ParseException {
    List<String> listeCodeClient = Arrays.asList("CLI");
    Date debut = DateUtils.parseDate("2013-02-09", new String[] { "yyyy-MM-dd" });
    Date fin = DateUtils.parseDate("2013-02-09", new String[] { "yyyy-MM-dd" });
    Periode periode = new Periode(debut, fin);
    Map<Long, List<RapportResultatAgrege>> liste = getServiceRapportResultat().getListeRapportResultatAgrege("BIDON",
        listeCodeClient, periode);

    assertNotNull(liste);
    assertEquals(3, liste.size());
  }
}
