/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.rapport.service.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.sofiframework.application.rapport.domaine.DomaineRapport.CodeEtatTraitementMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportGabarit;
import org.sofiframework.application.rapport.objetstransfert.RapportMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportTraitement;
import org.sofiframework.test.BaseTest;

/**
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 * 
 */
public class ServiceRapportTest extends BaseTest {

  public void testGetEntreeRapport() {
    RapportMetadonnee rapportEntree = getServiceRapport().getRapportMetadonnee(1L);
    assertNotNull(rapportEntree);
  }

  public void testGetListeRapportGabarit() {
    List<RapportGabarit> liste = getServiceRapport().getListeRapportGabaritActif("BIDON");
    assertEquals(2, liste.size());
    assertEquals("gabarit1", liste.get(0).getNom());
    assertEquals(8, liste.get(0).getListeRapportMetadonnee().size());
  }

  public void getListeRapportEntreeTraitement() {
    List<RapportTraitement> liste = getServiceRapport().getListeRapportTraitement("BIDON",
        CodeEtatTraitementMetadonnee.A_TRAITER);
    assertEquals(2, liste.size());
    liste = getServiceRapport().getListeRapportTraitement("BIDON", CodeEtatTraitementMetadonnee.TRAITEE);
    assertEquals(1, liste.size());
    liste = getServiceRapport()
        .getListeRapportTraitement("INFRA_SOFI", CodeEtatTraitementMetadonnee.A_TRAITER);
    assertEquals(1, liste.size());
  }

  public void testEnregistrerListeRapportEntreeTraitement() {
    runAs("moi");
    List<RapportTraitement> liste = new ArrayList<RapportTraitement>();
    liste.add(construireRapporTraitement(1L));
    liste.add(construireRapporTraitement(2L));
    liste.add(construireRapporTraitement(3L));
    getServiceRapport().enregistrerListeRapportTraitement(liste);

    liste = getServiceRapport().getListeRapportTraitement("INFRA_SOFI", CodeEtatTraitementMetadonnee.A_TRAITER);
    // 3 insérés + 1 dans 3.4.sql = 4
    assertEquals(4, liste.size());
  }

  private RapportTraitement construireRapporTraitement(Long rapportEntreeId) {
    RapportTraitement rapportTraitement = new RapportTraitement();
    rapportTraitement.setCodeApplication("INFRA_SOFI");
    rapportTraitement.setCodeClient("CLI");
    rapportTraitement.setCodeEtatTraitement(CodeEtatTraitementMetadonnee.A_TRAITER.getValeur());
    rapportTraitement.setDateTraitement(new Date());
    rapportTraitement.setNombrePeriode(1);
    rapportTraitement.setRapportMetadonneeId(rapportEntreeId);
    return rapportTraitement;
  }
}
