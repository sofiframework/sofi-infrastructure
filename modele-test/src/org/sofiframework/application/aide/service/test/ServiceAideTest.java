/*
 * Copyright 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.application.aide.service.test;

import org.sofiframework.application.aide.objetstransfert.Aide;
import org.sofiframework.application.aide.service.ServiceAide;
import org.sofiframework.test.BaseTest;

/**
 * Les cas d'essais sur le service d'aide en ligne de l'infrastructure SOFI.
 * <p>
 * 
 * @author Jean-Francois Brassard
 * @version 1.0
 */
public class ServiceAideTest extends BaseTest {
  private static Long ID_COMPOSANT_REFERENTIEL = new Long("37");

  private static String CLE_AIDE = "infra_sofi.aide.accueil.service";

  /**
   * Essai d'extraction d'un aide selon objet du referentiel
   */
  public void testGetAide() {
    // Acces au service.
    ServiceAide serviceAide = getServiceAide();

    // Extraire une aide
    Aide aide = serviceAide.getAide(ID_COMPOSANT_REFERENTIEL, "fr_CA");

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceAide' : Probleme d'extraction d'une aide associe au composant :" + ID_COMPOSANT_REFERENTIEL.toString(), aide);
  }

  /**
   * Essai d'extraction d'un aide selon sa cle.
   */
  public void testGetAideSelonCle() {
    // Acces au service.
    ServiceAide serviceAide = getServiceAide();

    // Extraire une aide
    Aide aide = serviceAide.getAideSelonCle(CLE_AIDE, "fr_CA");

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceAide' : Probleme d'extraction d'une aide associe a la cle :" + CLE_AIDE, aide);
  }
}
