/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.application.journalisation.service.test;

import java.util.Date;

import org.sofiframework.application.journalisation.objetstransfert.Journal;
import org.sofiframework.application.journalisation.service.ServiceJournalisation;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.filtre.FiltreJournalisation;
import org.sofiframework.test.BaseTest;

/**
 * Classe permettant de tester l'interface de SerciveJournalisation
 * 
 * @author Jean-Maxime Pelletier
 * @author Marie-Soleil L'Allier (Nurun inc.)
 */
public class ServiceJournalisationTest extends BaseTest {

  public ServiceJournalisationTest() {
  }

  public void testecrire() {
    ajouterJournal();
  }

  private void ajouterJournal() {
    ServiceJournalisation service = getServiceCommunJournalisation();
    Journal journal = new Journal();
    journal.setSeqObjetSecurisable(new Long("23"));
    journal.setCodeUtilisateur("sofi");
    journal.setDateHeure(new Date());
    journal.setDetail("TEST HIBERNATE");
    journal.setType("A");
    journal.setCodeApplication("INFRA_SOFI");
    service.ecrire(journal);
  }

  private ServiceJournalisation getServiceCommunJournalisation() {
    ServiceJournalisation service = (ServiceJournalisation) getContexte().getBean("serviceJournalisationCommun");
    return service;
  }

  public void testRecherche() {
    ListeNavigation liste = new ListeNavigation(1, 100);
    FiltreJournalisation filtre = new FiltreJournalisation();
    liste.setFiltre(filtre);

    filtre.setCodeApplication(new String[] { "INFRA_SOFI" });
    liste = this.getServiceCommunJournalisation().getListeJournalisation(liste);
    assertNotNull(liste);
    assertTrue(liste.getNbListe() > 1);
    assertEquals(Journal.class.getName(), liste.getListe().get(0).getClass().getName());
    assertTrue(liste.getNbListe() == 10);

    filtre.setCodeApplication(new String[] { "LOGIN" });
    liste = this.getServiceCommunJournalisation().getListeJournalisation(liste);
    assertNotNull(liste);
    assertTrue(liste.getNbListe() == 5);

    filtre.setCodeApplication(new String[] { "INFRA_SOFI", "LOGIN" });
    liste = this.getServiceCommunJournalisation().getListeJournalisation(liste);
    assertNotNull(liste);
    assertTrue(liste.getNbListe() == 15);
  }
}