/*
 * Copyright 2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sofiframework.test;

import org.sofiframework.application.aide.service.ServiceAide;
import org.sofiframework.application.domainevaleur.service.ServiceDomaineValeur;
import org.sofiframework.application.message.service.ServiceLibelle;
import org.sofiframework.application.message.service.ServiceMessage;
import org.sofiframework.application.parametresysteme.service.ServiceParametreSysteme;
import org.sofiframework.application.rapport.service.ServiceRapport;
import org.sofiframework.application.rapport.service.ServiceRapportResultat;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.service.ServiceApplication;
import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.application.securite.service.ServiceClient;
import org.sofiframework.application.securite.service.ServiceObjetSecurisable;
import org.sofiframework.application.securite.service.ServiceSecurite;
import org.sofiframework.application.securite.service.ServiceUtilisateur;
import org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle;
import org.sofiframework.infrasofi.modele.service.ServiceJournalisation;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.sofiframework.modele.spring.junit.AbstractSpringTest;

/**
 * Classe de base pour la configuration des tests Junit pour les services de l'infrastructure SOFI.
 * <p>
 * 
 * @author Jean-Francois Brassard
 */
public class BaseTest extends AbstractSpringTest {

  public BaseTest() {
    super("baseTest");
  }

  public void testContexte() {
    assertNotNull(this.getBean("modeleInfrastructure"));
    assertNotNull(this.getBean("configurationServices"));
  }

  protected void runAs(String codeUtilisateur) {
    GestionnaireUtilisateur gu = (GestionnaireUtilisateur) this.getBean("gestionnaireUtilisateur");
    Utilisateur u = new Utilisateur();
    u.setCodeUtilisateur(codeUtilisateur);
    gu.setUtilisateur(u);
  }

  /**
   * Fichier de configuration Spring pour la classe de tests.
   * 
   * @return les fichiers de configuration nécessaire.
   */
  protected String[] getConfigurationsSpring() {
    String typeTest = System.getProperty("typeTest");

    if (typeTest == null) {
      typeTest = "local";
    }

    if ("local".equals(typeTest)) {
      return new String[] {
          "/org/sofiframework/conf/contexte-junit.xml",
          "/org/sofiframework/infrasofi/modele/conf/bd/hsql.xml",
          // "/org/sofiframework/infrasofi/modele/conf/bd/oracle_junit.xml",
          "/org/sofiframework/infrasofi/modele/conf/contexte.xml",
          "/org/sofiframework/infrasofi/modele/conf/service.xml", "/org/sofiframework/infrasofi/modele/conf/dao.xml",
          "/org/sofiframework/infrasofi/modele/conf/entite.xml", };
    }

    if ("distant".equals(typeTest)) {
      return new String[] { "/org/sofiframework/conf/contexte-junit.xml",
          "/org/sofiframework/conf/service-distant-sofi-http.xml" };
    }

    return null;
  }

  /**
   * Acces au service des applications.
   * 
   * @return au service des applications.
   */
  protected ServiceApplication getServiceApplication() {
    return (ServiceApplication) this.getContexte().getBean("serviceApplicationCommun");
  }

  /**
   * Acces au service du referentiel d'une application.
   * 
   * @return au service du referentiel d'une application.
   */
  protected ServiceObjetSecurisable getServiceObjetSecurisable() {
    return (ServiceObjetSecurisable) this.getContexte().getBean("serviceObjetSecurisableCommun");
  }

  /**
   * Acces au service de securite.
   * 
   * @return au service de securite.
   */
  protected ServiceSecurite getServiceSecurite() {
    return (ServiceSecurite) this.getContexte().getBean("serviceSecuriteCommun");
  }

  /**
   * Acces au service d'authentification.
   * 
   * @return au service d'authentification.
   */
  protected ServiceAuthentification getServiceAuthentification() {
    return (ServiceAuthentification) this.getContexte().getBean("serviceAuthentificationCommun");
  }

  /**
   * Acces au service de gestion des utilisateurs.
   * 
   * @return au service de gestion des utilisateurs.
   */
  protected ServiceUtilisateur getServiceUtilisateur() {
    return (ServiceUtilisateur) this.getContexte().getBean("serviceUtilisateurCommun");
  }

  /**
   * Acces au service des domaines de valeurs.
   * 
   * @return au service des domaines de valeurs.
   */
  protected ServiceDomaineValeur getServiceDomaineValeur() {
    return (ServiceDomaineValeur) this.getContexte().getBean("serviceDomaineValeurCommun");
  }

  /**
   * Acces au service des parametres systemes.
   * 
   * @return au service des parametres systemes.
   */
  protected ServiceParametreSysteme getServiceParametreSysteme() {
    return (ServiceParametreSysteme) this.getContexte().getBean("serviceParametreSystemeCommun");
  }

  /**
   * Acces au service des libelles.
   * 
   * @return au service des libelles.
   */
  protected ServiceLibelle getServiceLibelle() {
    return (ServiceLibelle) this.getContexte().getBean("serviceLibelleCommun");
  }

  /**
   * Acces au service des messages.
   * 
   * @return au service des messages.
   */
  protected ServiceMessage getServiceMessage() {
    return (ServiceMessage) this.getContexte().getBean("serviceMessageCommun");
  }

  /**
   * Acces au service de l'aide en ligne.
   * 
   * @return au service de l'aide en ligne.
   */
  protected ServiceAide getServiceAide() {
    return (ServiceAide) this.getContexte().getBean("serviceAideCommun");
  }

  /**
   * Acces au service local d'aide contextuelle.
   * 
   * @return service aide contextuelle local
   */
  protected ServiceAideContextuelle getServiceAideContextuelle() {
    return (ServiceAideContextuelle) this.getContexte().getBean("serviceAideContextuelle");
  }

  /**
   * Acces au service de journalisation.
   * 
   * @return au service de journalisation.
   */
  protected ServiceJournalisation getServiceJournalisation() {
    return (ServiceJournalisation) this.getContexte().getBean("serviceJournalisation");
  }

  /**
   * Acces au service des libelles.
   * 
   * @return au service des libelles.
   */

  protected org.sofiframework.infrasofi.modele.service.ServiceLibelle getServiceLibelleLocal() {
    return (org.sofiframework.infrasofi.modele.service.ServiceLibelle) this.getContexte().getBean("serviceLibelle");
  }

  /**
   * Acces au service des messages.
   * 
   * @return au service des messages.
   */
  protected org.sofiframework.infrasofi.modele.service.ServiceMessage getServiceMessageLocal() {
    return (org.sofiframework.infrasofi.modele.service.ServiceMessage) this.getContexte().getBean("serviceMessage");
  }

  /**
   * Accès au service commun des rapports.
   * 
   * @return un objet de type {@link ServiceRapport}.
   */
  protected ServiceRapport getServiceRapport() {
    return (ServiceRapport) getContexte().getBean("serviceRapportCommun");
  }

  /**
   * Accès au service commun des rapports.
   * 
   * @return un objet de type {@link ServiceRapportResultat}.
   */
  protected ServiceRapportResultat getServiceRapportResultat() {
    return (ServiceRapportResultat) getContexte().getBean("serviceRapportResultatCommun");
  }

  /**
   * Accès au service commun des rapports.
   * 
   * @return un objet de type {@link ServiceRapport}.
   */
  protected ServiceClient getServiceClient() {
    return (ServiceClient) getContexte().getBean("serviceClientCommun");
  }

}
