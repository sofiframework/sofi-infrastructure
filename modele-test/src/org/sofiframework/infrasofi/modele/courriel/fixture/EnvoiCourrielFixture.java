package org.sofiframework.infrasofi.modele.courriel.fixture;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.sofiframework.infrasofi.modele.commun.Domaine;
import org.sofiframework.infrasofi.modele.entite.ContactCourriel;
import org.sofiframework.infrasofi.modele.entite.Courriel;
import org.sofiframework.infrasofi.modele.entite.EnvoiCourriel;
import org.sofiframework.infrasofi.modele.entite.TentativeEnvoiCourriel;

public class EnvoiCourrielFixture {

  public static EnvoiCourriel getEnvoiCourriel() {
    EnvoiCourriel envoi = new EnvoiCourriel();
    envoi.setCourriel(getCourriel());
    envoi.setListeTentativeEnvoiCourriel(getListeTentativeCourriel());
    envoi.setNombreTentative(1);
    envoi.setSmtpHost("my.smtp.com");
    envoi.setSmtpUsername("smtpUser");
    envoi.setSmtpPassword("123456");
    return envoi;
  }

  public static EnvoiCourriel getEnvoiCourrielDeuxMoisAbandon() {
    EnvoiCourriel envoi = getEnvoiCourriel();
    envoi.setStatut(Domaine.statutEnvoiCourriel.abandon.getValeur());
    envoi.setNombreTentative(4);
    Date maintenant = new Date();
    envoi.getListeTentativeEnvoiCourriel().get(0).setDateEnvoi(
        DateUtils.addMonths(maintenant, -2));

    return envoi;
  }
  
  private static List<TentativeEnvoiCourriel> getListeTentativeCourriel() {
    List<TentativeEnvoiCourriel> tentatives = new ArrayList<TentativeEnvoiCourriel>();
    tentatives.add(new TentativeEnvoiCourriel(new Date(), Domaine.statutTentativeEnvoiCourriel.echec.getValeur(), "java.lang.NullPointerException"));
    return tentatives;
  }

  public static Courriel getCourriel() {
    Courriel courriel = new Courriel();
    courriel.setCharset("UTF-8");
    courriel.setExpediteur(getExpediteur());
    courriel.setListeDestinataire(getListeDestinataire());
    courriel.setMessage("Corps du message");
    courriel.setSujet("Titre du message");
    courriel.setTypeContenu(Domaine.typeContenuCourriel.html.getValeur());
    return courriel;
  }

  private static List<ContactCourriel> getListeDestinataire() {
    List<ContactCourriel> destinataires = new ArrayList<ContactCourriel>(); 
    destinataires.add(new ContactCourriel("Nathalie Destinaire", "nathalie@destinataire.com"));
    destinataires.add(new ContactCourriel("Michel Destinaire", "michel@destinataire.com"));
    destinataires.add(new ContactCourriel("Denis Destinaire", "denis@destinataire.com"));
    return destinataires;
  }

  private static ContactCourriel getExpediteur() {
    ContactCourriel exp = new ContactCourriel();
    exp.setAdresse("expediteur@expediteur.com");
    exp.setNom("Gilles Expediteur");
    return exp;
  }
}
