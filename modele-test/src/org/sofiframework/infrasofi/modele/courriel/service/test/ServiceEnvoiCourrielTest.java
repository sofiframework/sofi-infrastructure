package org.sofiframework.infrasofi.modele.courriel.service.test;

import org.sofiframework.infrasofi.modele.courriel.fixture.EnvoiCourrielFixture;
import org.sofiframework.infrasofi.modele.entite.Courriel;
import org.sofiframework.infrasofi.modele.entite.Smtp;
import org.sofiframework.infrasofi.modele.service.ServiceEnvoiCourriel;
import org.sofiframework.test.BaseTest;

public class ServiceEnvoiCourrielTest extends BaseTest {

  public void testEnvoyer() {
    Courriel courriel = EnvoiCourrielFixture.getCourriel();
    this.getServiceEnvoiCourriel().envoyer(courriel, new Smtp("smtp.que.nurun.com", null, null, false));
  }

  private ServiceEnvoiCourriel getServiceEnvoiCourriel() {
    return (ServiceEnvoiCourriel) this.getBean("serviceEnvoiCourriel");
  }
}
