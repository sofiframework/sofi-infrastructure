package org.sofiframework.infrasofi.modele.courriel.dao.test;

import java.util.Date;
import java.util.List;

import org.sofiframework.infrasofi.modele.commun.Domaine;
import org.sofiframework.infrasofi.modele.courriel.dao.EnvoiCourrielDao;
import org.sofiframework.infrasofi.modele.courriel.fixture.EnvoiCourrielFixture;
import org.sofiframework.infrasofi.modele.entite.EnvoiCourriel;
import org.sofiframework.infrasofi.modele.filtre.FiltreEnvoiCourriel;
import org.sofiframework.test.BaseTest;

public class EnvoiCourrielDaoTest extends BaseTest {

  public void testAjouter() {
    EnvoiCourriel envoi = EnvoiCourrielFixture.getEnvoiCourriel();
    this.getDao().ajouter(envoi);
    envoi = this.getDao().getEnvoiCourriel(envoi.getId());
    assertNotNull(envoi);
    assertNotNull(envoi.getId());
  }

  public void testModifier() {
    EnvoiCourriel envoi = EnvoiCourrielFixture.getEnvoiCourriel();
    envoi.setSmtpHost("monHost");
    this.getDao().ajouter(envoi);

    Long id = envoi.getId();
    envoi.setSmtpHost("monHost2");
    this.getDao().modifier(envoi);

    envoi = this.getDao().getEnvoiCourriel(id);

    assertTrue(envoi.getSmtpHost().equals("monHost2"));
  }

  public void testGetListeEnvoiCourriel() {
    this.getDao().ajouter(EnvoiCourrielFixture.getEnvoiCourrielDeuxMoisAbandon());
    FiltreEnvoiCourriel filtre = new FiltreEnvoiCourriel();
    filtre.setDateEnvoi(new Date());
    filtre.setNombreTentative(3);
    filtre.setStatut(new String[]{Domaine.statutEnvoiCourriel.abandon.getValeur()});
    List<EnvoiCourriel> listeEnvoiCourriel = this.getDao().getListeEnvoiCourriel(filtre);
    assertNotNull(listeEnvoiCourriel);
    assertFalse(listeEnvoiCourriel.isEmpty());
  }

  private EnvoiCourrielDao getDao() {
    return (EnvoiCourrielDao) this.getBean("envoiCourrielDao");
  }
}
