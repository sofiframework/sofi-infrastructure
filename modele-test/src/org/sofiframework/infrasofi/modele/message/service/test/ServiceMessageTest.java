package org.sofiframework.infrasofi.modele.message.service.test;

import org.sofiframework.infrasofi.modele.service.ServiceMessage;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Message;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.sofiframework.test.BaseTest;

public class ServiceMessageTest extends BaseTest {

  private static String IDENTIFIANT = "infra_sofi.information.domaine_valeur.nouvelle_valeur";

  private static String ANCIENNE_CLE = "infra_sofi.information.domaine_valeur.creation_definition";

  private static String CLE = "infra_sofi.information.gestion_aide.ajout.nouvelle_association";

  private static String CODE_APPLICATION = "INFRA_SOFI";

  private static Integer NOMBRE_JOURS = 1;

  private static String TEXTE_MESSAGE = "Ceci est un message";

  public void testGetMessageLocal() {

    // Acces au service.
    ServiceMessage serviceMessages = this.getServiceMessageLocal();

    // Extraire une valeur du message.
    Message message = serviceMessages.get(IDENTIFIANT, "fr_CA");

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceMessages' : Probleme d'extraction d'un message", message);

  }

  public void testSupprimerMessage() {

    // Acces au service.
    ServiceMessage serviceMessages = this.getServiceMessageLocal();

    // Supprimer
    serviceMessages.supprimerMessage(IDENTIFIANT, null);

    // Extraire une valeur du message.
    Message message = serviceMessages.get(IDENTIFIANT, "fr_CA");

    // Les cas d'essais.
    assertNull(message);

  }

  public void testGetListeMessagesPourTableauBord() {

    // initialisation de la liste de navigation
    ListeNavigation listeNavigation = new ListeNavigation(0, 9000000);

    // Acces au service.
    ServiceMessage serviceMessages = this.getServiceMessageLocal();

    // Obtention de la liste d'aide tableau de bord
    ListeNavigation listeNavigationfinal = serviceMessages.getListeMessagesPourTableauBord(listeNavigation,
        NOMBRE_JOURS, CODE_APPLICATION);

    // Les cas d'essais.
    assertFalse(ListeNavigation.isVide(listeNavigationfinal));

  }

  public void testGetListeMessagePilotage() {

    // initialisation de la liste de navigation
    ListeNavigation listeNavigation = new ListeNavigation(0, 200);

    // Acces au service.
    ServiceMessage serviceMessages = this.getServiceMessageLocal();

    // Utilisateur courant
    GestionnaireUtilisateur gu = (GestionnaireUtilisateur) this.getBean("gestionnaireUtilisateur");
    Utilisateur utilisateur = new Utilisateur();
    utilisateur.setCodeUtilisateur("sofi");
    gu.setUtilisateur(utilisateur);

    // Obtention de la liste AideContextuellePilotage
    listeNavigation = serviceMessages.getListeMessagePilotage(listeNavigation);

    // Les cas d'essais.
    assertFalse(ListeNavigation.isVide(listeNavigation));

  }

  /**
   * Enregistrer un message (modification de la clé d'un message)
   */
  public void testEnregistrerModifierCle() {

    // Acces au service.
    ServiceMessage serviceMessages = this.getServiceMessageLocal();

    // Extraire une valeur du message.
    Message message = serviceMessages.get(ANCIENNE_CLE, "fr_CA");

    message.setCleMessage("infra_sofi.aide.gestion_libelle.onglet.detail.2");

    // modifier la clé message
    serviceMessages.enregistrer(message, ANCIENNE_CLE);

    // obtention de la clé message
    Message messageEnregistrer = serviceMessages.get(message.getCleMessage(), "fr_CA");

    // Les cas d'essais.
    assertNotNull(messageEnregistrer);
    assertSame(messageEnregistrer.getCleMessage(), message.getCleMessage());

  }

  /**
   * Enregistrer un message (modification d'un élément de message)
   */
  public void testEnregistrerModifier() {

    // Acces au service.
    ServiceMessage serviceMessages = this.getServiceMessageLocal();
    String ancienneCle = null;

    // obtention du message
    Message message = serviceMessages.get(CLE, "fr_CA");

    // modification du code de sévirité
    message.setCodeSeverite("E");

    // modifier la clé message
    serviceMessages.enregistrer(message, ancienneCle);

    // obtention du message
    Message messageEnregistrer = serviceMessages.get(CLE, "fr_CA");

    // Les cas d'essais.
    assertNotNull(messageEnregistrer);
    assertSame(message.getCodeSeverite(), messageEnregistrer.getCodeSeverite());

  }

  /**
   * Enregistrer un AideContextuelle (ajouter un nouvel élément message )
   */
  public void testEnregistrerAjouter() {

    // valeur pour la création d'un AideContextuelle
    String cleMessage = "infra_sofi.information.parametre.initialisation.parametre.3";
    String codeLangue = "fr_CA";
    int version = 1;
    String codeSeverite = "E";

    // Acces au service.
    ServiceMessage serviceMessages = this.getServiceMessageLocal();

    // création d'un nouvel aide contextuelle
    Message message = new Message();

    // modification des valeurs par défaut de AideContextuelle
    message.setCleMessage(cleMessage);
    message.setCodeLangue(codeLangue);
    message.setCodeApplication(CODE_APPLICATION);
    message.setCodeSeverite(codeSeverite);
    message.setTexteMessage(TEXTE_MESSAGE);

    message.setVersion(version);

    // ajouter un nouveau message
    serviceMessages.enregistrer(message, null);

    // obtention de la clé AideContextuelle
    Message messageEnregistrer = serviceMessages.get(cleMessage, codeLangue);

    // Les cas d'essais.
    assertNotNull(messageEnregistrer);
    assertSame(message.getCleMessage(), messageEnregistrer.getCleMessage());
  }
}
