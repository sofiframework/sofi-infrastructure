package org.sofiframework.infrasofi.modele.message.service.test;

import org.sofiframework.test.BaseTest;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Libelle;
import org.sofiframework.infrasofi.modele.service.ServiceLibelle;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;

public class ServiceLibelleTest extends BaseTest {

  private static String ANCIENNE_CLE = "infra_sofi.libelle.gestion_utilisateur.champ.statut";
  
  private static String CLE = "infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.nom";
  
  private static String IDENTIFIANT = "infra_sofi.libelle.gestion_utilisateur.champ.nom";
  
  private static String CODE_APPLICATION = "INFRA_SOFI";
  
  private static String TEXTE_LIBELLE = "Ceci est un libelle";
  
  private static Integer NOMBRE_JOURS = 1;

  
public void  testGetLibelle(){
    
    // Acces au service.
    ServiceLibelle serviceLibelle =  this.getServiceLibelleLocal();
    
    // Extraire une valeur du message.
    Libelle libelle = serviceLibelle.get(IDENTIFIANT, "fr_CA");

    // Les cas d'essais.
    assertNotNull("Essai infrastructure 'ServiceLibelle' : Probleme d'extraction d'un libelle", libelle);

  }
  
  public void  testSupprimerLibelle(){
      
    // Acces au service.
    ServiceLibelle serviceLibelle =  this.getServiceLibelleLocal();
    
    //Supprimer un libelle
    serviceLibelle.supprimerLibelle(IDENTIFIANT, null);
    
    // Extraire une valeur d'un libelle.
    Libelle libelle = serviceLibelle.get(IDENTIFIANT, "fr_CA");
    
    // Les cas d'essais.
    assertNull(libelle);

  }
  
  public void  testGetListeLibellesPourTableauBord(){
    
    // initialisation de la liste de navigation
    ListeNavigation listeNavigation = new ListeNavigation(0,200);

    // Acces au service.
    ServiceLibelle serviceLibelle =  this.getServiceLibelleLocal();
    
    
    //Obtention de la liste d'aide tableau de bord
    ListeNavigation listeNavigationLibelle = serviceLibelle.getListeLibellesPourTableauBord(listeNavigation, NOMBRE_JOURS,CODE_APPLICATION);
    
    // Les cas d'essais.
    assertNotNull(listeNavigationLibelle);
    
  }
  
  public void  testGetListeLibellePilotage() {
    
    // initialisation de la liste de navigation
    ListeNavigation listeNavigation = new ListeNavigation(0,200);
    
    // Acces au service.
    ServiceLibelle serviceLibelle =  this.getServiceLibelleLocal();
    
   // Utilisateur courant 
    GestionnaireUtilisateur gu = (GestionnaireUtilisateur) this.getBean("gestionnaireUtilisateur");
    Utilisateur utilisateur = new Utilisateur();
    utilisateur.setCodeUtilisateur("sofi");
    gu.setUtilisateur(utilisateur);
      
    //Obtention de la liste AideContextuellePilotage
    listeNavigation = serviceLibelle.getListeLibellePilotage(listeNavigation);
    
    // Les cas d'essais.
    assertFalse(ListeNavigation.isVide(listeNavigation));
    
  }
  /**
   * Enregistrer un message (modification de la clé d'un message) 
   */
  public void testEnregistrerModifierCle() {
      
    // Acces au service.
    ServiceLibelle serviceLibelle =  this.getServiceLibelleLocal();
  
    // Extraire une valeur du libellé.
    Libelle libelle = serviceLibelle.get(ANCIENNE_CLE, "fr_CA");

    libelle.setCleLibelle("infra_sofi.aide.gestion_libelle.onglet.detail.2");

    // modifier la clé du libellé
    serviceLibelle.enregistrer(libelle, ANCIENNE_CLE);
    
    // obtention du libellé
    Libelle libelleEnregistrer = serviceLibelle.get(libelle.getCleLibelle(),"fr_CA");
    
    // Les cas d'essais.
    assertNotNull(libelleEnregistrer);
    assertSame(libelleEnregistrer.getCleLibelle(), libelle.getCleLibelle());
    
    }
  
  /**
   * Enregistrer un message (modification d'un élément de message) 
   */
  public void testEnregistrerModifier() {
      
      // Acces au service.
      ServiceLibelle serviceLibelle =  this.getServiceLibelleLocal();
      String ancienneCle = null;
      
      // obtention du libellé
      Libelle libelle = serviceLibelle.get(CLE, "fr_CA");
      
      // modification du texte du libellé
      libelle.setTexteLibelle(TEXTE_LIBELLE);
      
      // modifier le message texte
      serviceLibelle.enregistrer(libelle, ancienneCle);
      
      // obtention du libellé
      Libelle libelleEnregistrer = serviceLibelle.get(CLE,"fr_CA");
      
      // Les cas d'essais.
      assertNotNull(libelleEnregistrer);
      
    }
  
  /**
   * Enregistrer un libellé (ajouter un nouvel élément libellé ) 
   */
  public void testEnregistrerAjouter() {
      
       // valeur pour la création d'un libellé
       String cleLibelle = "infra_sofi.information.parametre.initialisation.parametre.3";
       String codeLangue = "fr_CA";
       int version = 1;
       
       // Acces au service.
       ServiceLibelle serviceLibelle =  this.getServiceLibelleLocal();
       
      // création d'un nouveau libelle
      Libelle libelle = new Libelle();
      
      // modification des valeurs par défaut de libelle
      libelle.setCleLibelle(cleLibelle);
      libelle.setCodeLangue(codeLangue);
      libelle.setCodeApplication(CODE_APPLICATION);
      libelle.setTexteLibelle(TEXTE_LIBELLE);
      
      
      libelle.setVersion(version);
     
      // ajouter un nouveau libelle
      serviceLibelle.enregistrer(libelle, null);
      
      // obtention du libelle
      Libelle libelleEnregistrer = serviceLibelle.get(cleLibelle, codeLangue);
      
      // Les cas d'essais.
      assertNotNull(libelleEnregistrer);
      assertSame(libelle.getCleLibelle(), libelleEnregistrer.getCleLibelle());
    }
  
 public void testExiste(){
   
   boolean existe;
   
   // Acces au service.
   ServiceLibelle serviceLibelle =  this.getServiceLibelleLocal();
   
   existe = serviceLibelle.existe(CLE,CODE_APPLICATION);
   
   // Les cas d'essais.
   assertNotNull(existe);
   assertTrue(existe);
   
 }
 
 public void testModifierCleLibelle(){
   
// Acces au service.
   ServiceLibelle serviceLibelle =  this.getServiceLibelleLocal();
 
   // Extraire une valeur du libellé.
   Libelle libelle = serviceLibelle.get(ANCIENNE_CLE, "fr_CA");

   libelle.setCleLibelle("infra_sofi.aide.gestion_libelle.onglet.detail.2");

   // modifier la clé du libellé
   serviceLibelle.modifierCleLibelle(ANCIENNE_CLE,libelle);
   
   // obtention du libellé
   Libelle libelleEnregistrer = serviceLibelle.get(libelle.getCleLibelle(),"fr_CA");
   
   // Les cas d'essais.
   assertNotNull(libelleEnregistrer);
   assertSame(libelleEnregistrer.getCleLibelle(), libelle.getCleLibelle());
   
 }
  
}

