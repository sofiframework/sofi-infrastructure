package org.sofiframework.infrasofi.modele.journalisation.service.test;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.service.ServiceJournalisation;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.sofiframework.test.BaseTest;

public class ServiceJournalisationTest extends BaseTest {

  private static String CODE_APPLICATION = "INFRA_SOFI";

  public void testGetListeJournalisationPourTableauBord() {

    // initialisation de la liste de navigation
    ListeNavigation listeNavigation = new ListeNavigation(0, 200);

    // Acces au service.
    ServiceJournalisation serviceJournalisation = this.getServiceJournalisation();

    // Obtention de la liste de journalisation du tableau de bord
    ListeNavigation listeNavigationfinal = serviceJournalisation.getListeJournalisationPourTableauBord(listeNavigation, CODE_APPLICATION);

    // Les cas d'essais.
    assertNotNull(listeNavigationfinal);
    // assertFalse(ListeNavigation.isVide(listeNavigationfinal));

  }

  public void testGetListeJournalisationPilotage() {

    // initialisation de la liste de navigation
    ListeNavigation listeNavigation = new ListeNavigation(0, 200);

    // Acces au service.
    ServiceJournalisation serviceJournalisation = this.getServiceJournalisation();

    // Utilisateur courant
    GestionnaireUtilisateur gu = (GestionnaireUtilisateur) this.getBean("gestionnaireUtilisateur");
    Utilisateur utilisateur = new Utilisateur();
    utilisateur.setCodeUtilisateur("sofi");
    gu.setUtilisateur(utilisateur);

    // Obtention de la liste JournalisationPilotage
    ListeNavigation listeNavigationfinal = serviceJournalisation.getListeJournalisationPilotage(listeNavigation);

    // Les cas d'essais.
    assertNotNull(listeNavigationfinal);
    // assertFalse(ListeNavigation.isVide(listeNavigationfinal));

  }

}
