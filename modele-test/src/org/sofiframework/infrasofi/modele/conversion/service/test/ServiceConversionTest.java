/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.conversion.service.test;

import java.util.List;
import java.util.Map;

import org.sofiframework.application.conversion.objettransfert.Conversion;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.test.BaseTest;
import org.sofiframework.utilitaire.UtilitaireDate;

public class ServiceConversionTest extends BaseTest {

  public void testObjetConversion() {

    ModeleInfrastructure modele = (ModeleInfrastructure) this
        .getBean("modeleInfrastructure");
    List<Conversion> liste = modele.getServiceConversion().getListe();

    Conversion conversion = new Conversion();
    conversion.setNom("TEST");
    conversion.setCodeApplicationSource("APPSRC");
    conversion.setCodeApplicationDestination("APPDEST");
    conversion.setSourceId("1");
    conversion.setDestinationId("2");
    conversion.setCodeUtilisateurCreation("sofi");
    conversion.setDateCreation(UtilitaireDate.getDateJourSansHeure());
    
    Conversion conversion2 = new Conversion();
    conversion2.setNom("TEST");
    conversion2.setCodeApplicationSource("APPSRC");
    conversion2.setCodeApplicationDestination("APPDEST");
    conversion2.setSourceId("2");
    conversion2.setDestinationId("3");
    conversion2.setCodeUtilisateurCreation("sofi");
    conversion2.setDateCreation(UtilitaireDate.getDateJourSansHeure());

      modele.getServiceConversion().ajouter(conversion);
      modele.getServiceConversion().ajouter(conversion2);
      
      Conversion test = (Conversion) modele.getServiceConversion().get(
          conversion.getId());
      assertNotNull(test);

      org.sofiframework.application.conversion.service.ServiceConversion service = (org.sofiframework.application.conversion.service.ServiceConversion) this.getBean("serviceConversionCommun");
      Map<String, String> mapConversionTest =service
          .getListeConversion(conversion.getNom(),
              conversion.getCodeApplicationSource(),
              conversion.getCodeApplicationDestination());

      assertNotNull(mapConversionTest);
      assertTrue(mapConversionTest.size() == 2);
      assertNotNull(mapConversionTest.get(conversion.getSourceId()));
      assertNotNull(mapConversionTest.get(conversion2.getSourceId()));
  }
}
