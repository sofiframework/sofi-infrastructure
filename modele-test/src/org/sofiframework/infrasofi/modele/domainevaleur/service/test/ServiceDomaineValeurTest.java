package org.sofiframework.infrasofi.modele.domainevaleur.service.test;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.DefinitionDomaineValeur;
import org.sofiframework.infrasofi.modele.entite.DomaineValeur;
import org.sofiframework.infrasofi.modele.filtre.FiltreDomaineValeur;
import org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.sofiframework.test.BaseTest;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireObjet;

public class ServiceDomaineValeurTest extends BaseTest {
  
  private Log log = LogFactory.getLog(ServiceDomaineValeurTest.class);
  
  public void testgetServiceDomaineValeur() {
    assertNotNull(this.getServiceDomaineValeurLocal());
  }

  public void testgetDefinition() {
    DefinitionDomaineValeur d = this.getServiceDomaineValeurLocal().getDefinitionDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "fr_CA");
    assertNotNull(d);
    Boolean actif = d.getActif();
    assertTrue(actif);
  }
  
  public void testajouterDefinition() {
    DefinitionDomaineValeur d = new DefinitionDomaineValeur("SOFI_DEMO", "TEST_DEF", "fr_CA");
    d.setCodeTypeTri("A");
    d.setCreePar("sofi");
    d.setDateCreation(new Date());
    d.setDateDebut(new Date());
    d.setDateFin(UtilitaireDate.convertir_AAAA_MM_JJ_En_UtilDate("3000-01-01"));
    this.getServiceDomaineValeurLocal().ajouterDefinitionDomaineValeur(d);
    DefinitionDomaineValeur d2 = this.getServiceDomaineValeurLocal().getDefinitionDomaineValeur("SOFI_DEMO", "TEST_DEF", "fr_CA");
    assertNotNull(d2);
  }

  public void testgetDomaineValeur() {
    DomaineValeur dv = this.getServiceDomaineValeurLocal().getDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "I", "fr_CA");
    assertNotNull(dv);
    Boolean actif = dv.getActif();
    assertTrue(actif);
  }

  public void testactiverDomaineValeur() {
    this.getServiceDomaineValeurLocal().desactiverDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "I", "fr_CA");
    DomaineValeur dv = this.getServiceDomaineValeurLocal().getDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "I", "fr_CA");
    assertNotNull(dv);
    log.info("debut : " + dv.getDateDebut());
    log.info("fin : " + dv.getDateFin());
    log.info("maintenant : " + new Date());
    
    Boolean actif = dv.getActif();
    assertFalse(actif);

    this.getServiceDomaineValeurLocal().activerDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "I", "fr_CA");
    dv = this.getServiceDomaineValeurLocal().getDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "I", "fr_CA");
    assertNotNull(dv);
    actif = dv.getActif();
    assertTrue(actif);
  }

  public void testajouterDomaineValeur() {
    DomaineValeur domaine = new DomaineValeur();
    domaine.setCodeApplication("INFRA_SOFI");
    domaine.setNom("LISTE_ACTIVATION_UTILISATEUR");
    domaine.setValeur("Z");
    domaine.setCreePar("sofi");
    domaine.setCodeLangue("fr_CA");

    domaine.setDateDebut(new Date());
    domaine.setDateCreation(new Date());
    domaine.setDescription("Test JUnit");
    
    domaine.setOrdreAffichage(1);
    
    this.getServiceDomaineValeurLocal().ajouterDomaineValeur(domaine);
    
    DomaineValeur d2 = this.getServiceDomaineValeurLocal().getDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "Z", "fr_CA");
    assertNotNull(d2);
  }

  public void testenregistrerDefinitionDomaineValeur() {
    DefinitionDomaineValeur d = new DefinitionDomaineValeur("SOFI_DEMO", "TEST_DEF", "fr_CA");
    d.setCodeTypeTri("A");
    d.setCreePar("sofi");
    d.setDateCreation(new Date());
    d.setDateDebut(new Date());
    d.setDateFin(UtilitaireDate.convertir_AAAA_MM_JJ_En_UtilDate("3000-01-01"));
    this.getServiceDomaineValeurLocal().enregistrerDefinitionDomaineValeur(d);

    DefinitionDomaineValeur d2 = this.getServiceDomaineValeurLocal().getDefinitionDomaineValeur("SOFI_DEMO", "TEST_DEF", "fr_CA");
    assertNotNull(d2);
  }
  
  public void testenregistrerDomaineValeur() {
    DomaineValeur domaine = new DomaineValeur();
    domaine.setCodeApplication("INFRA_SOFI");
    domaine.setNom("LISTE_ACTIVATION_UTILISATEUR");
    domaine.setValeur("Z");
    domaine.setCreePar("sofi");
    domaine.setCodeLangue("fr_CA");

    domaine.setDateDebut(new Date());
    domaine.setDateCreation(new Date());
    domaine.setDescription("Test JUnit");
    
    domaine.setOrdreAffichage(1);
    
    this.getServiceDomaineValeurLocal().enregistrerDomaineValeur(domaine);
    
    DomaineValeur d2 = this.getServiceDomaineValeurLocal().getDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "Z", "fr_CA");
    assertNotNull(d2);
  }

  public void testenregistrerDomaineValeurDO() {
    DomaineValeur domaineOriginal = this.getServiceDomaineValeurLocal().getDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "I", "fr_CA");
    DomaineValeur domaine = new DomaineValeur();
    UtilitaireObjet.copierAttribut(domaine, domaineOriginal);
    domaine.setDescription("Patate");
    this.getServiceDomaineValeurLocal().enregistrerDomaineValeur(domaine, domaineOriginal);
    
    DomaineValeur domaineI = this.getServiceDomaineValeurLocal().getDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "I", "fr_CA");
    assertNotNull(domaineI);
  }

  public void testgetListe() {
    List liste = this.getServiceDomaineValeurLocal().getListe();
    assertNotNull(liste);
    assertTrue(liste.size() > 0);
  }

  public void testgetListeActif() {
    FiltreDomaineValeur f  = new FiltreDomaineValeur();
    f.setCodeApplication("INFRA_SOFI");
    f.setNom("LISTE_ACTIVATION_UTILISATEUR");
    f.setCodeLangue("fr_CA");

    ListeNavigation ln = new ListeNavigation(1, Integer.MAX_VALUE);
    ln.setFiltre(f);

    ln = this.getServiceDomaineValeurLocal().getListeActif(ln, null);
    
    assertNotNull(ln);
    assertFalse(ln.isVide());
  }

  public void testgetListeDefinitionDomaineValeurF() {
    org.sofiframework.application.domainevaleur.filtre.FiltreDomaineValeur f = new org.sofiframework.application.domainevaleur.filtre.FiltreDomaineValeur();
    f.setCodeApplication("INFRA_SOFI");
    List liste = this.getServiceDomaineValeurLocal().getListeDefinitionDomaineValeur(f);
    assertNotNull(liste);
  }

  public void testgetListeDefinitionDomaineValeurLn() {
    ListeNavigation liste = new ListeNavigation(1, 2);
    FiltreDomaineValeur f = new FiltreDomaineValeur();
    f.setNom("LISTE_ACTIVATION_UTILISATEUR");
    liste.setFiltre(f);
    liste = this.getServiceDomaineValeurLocal().getListeDefinitionDomaineValeur(liste);
    assertNotNull(liste);
    assertFalse(liste.isVide());
  }
  
  public void testgetListeDomaineValeurPilotage() {
    GestionnaireUtilisateur gu = (GestionnaireUtilisateur) this.getBean("gestionnaireUtilisateur");
    Utilisateur u = new Utilisateur();
    u.setCodeUtilisateur("sofi");
    gu.setUtilisateur(u);
    ListeNavigation liste = new ListeNavigation(1, 10);
    FiltreDomaineValeur f = new FiltreDomaineValeur();
    liste.setFiltre(f);

    liste = this.getServiceDomaineValeurLocal().getListeDomaineValeurPilotage(liste, true);
    assertNotNull(liste);
    assertFalse(liste.isVide());
    
    liste = this.getServiceDomaineValeurLocal().getListeDomaineValeurPilotage(liste, false);
    assertNotNull(liste);
    assertFalse(liste.isVide());
    
    gu.libererUtilisateur();
  }
  
  public void testsupprimerDefinitionDomaineValeur() {
    DefinitionDomaineValeur d = this.getServiceDomaineValeurLocal().getDefinitionDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "fr_CA");

    this.getServiceDomaineValeurLocal().supprimerDefinitionDomaineValeur(d);
    d = this.getServiceDomaineValeurLocal().getDefinitionDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "fr_CA");
    assertNull(d);
  }

  public void testsupprimerDomaineValeur() {
    DomaineValeur domaine = this.getServiceDomaineValeurLocal().getDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "I", "fr_CA");
    this.getServiceDomaineValeurLocal().supprimerDomaineValeur(domaine);
    domaine = this.getServiceDomaineValeurLocal().getDomaineValeur("INFRA_SOFI", "LISTE_ACTIVATION_UTILISATEUR", "I", "fr_CA");
    assertNull(domaine);
  }

  public ServiceDomaineValeur getServiceDomaineValeurLocal() {
    return (ServiceDomaineValeur) this.getBean("serviceDomaineValeur");
  }
}
