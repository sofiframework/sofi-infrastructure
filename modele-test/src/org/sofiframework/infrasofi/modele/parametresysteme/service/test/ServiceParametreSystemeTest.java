package org.sofiframework.infrasofi.modele.parametresysteme.service.test;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.ParametreSysteme;
import org.sofiframework.infrasofi.modele.filtre.FiltreParametreSysteme;
import org.sofiframework.infrasofi.modele.service.ServiceParametreSysteme;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.sofiframework.test.BaseTest;

public class ServiceParametreSystemeTest extends BaseTest {

  private Log log = LogFactory.getLog(ServiceParametreSystemeTest.class);

  public void testgetServiceParametreSysteme() {
    assertNotNull(this.getServiceParametreSysteme());
  }

  public void testgetValeur() {
    String codeApplication = "INFRA_SOFI";
    String codeParametreSysteme = "authentificationNomCookie";
    ParametreSysteme ps = this.getServiceParametreSystemeLocal().get(codeApplication, codeParametreSysteme);
    String valeur = ps.getValeur();
    assertNotNull(valeur);
    assertTrue(valeur.length() > 0);
  }

  public void testget() {
    String codeApplication = "INFRA_SOFI";
    String codeParametreSysteme = "authentificationNomCookie";
    boolean isParametreSysteme = false;
    assertNotNull(this.getServiceParametreSystemeLocal().get(codeApplication, codeParametreSysteme));
    if (this.getServiceParametreSystemeLocal().get(codeApplication, codeParametreSysteme) != null) {
      isParametreSysteme = this.getServiceParametreSystemeLocal().get(codeApplication, codeParametreSysteme) instanceof ParametreSysteme;
      assertTrue(isParametreSysteme);
    }
  }

  public void testenregistrer() {
    ParametreSysteme parametre = new ParametreSysteme();
    ParametreSysteme parametreOriginal = new ParametreSysteme();
    String valeur = "";
    String description = "";
    String codeApplication = "INFRA_SOFI";
    String codeParametreSysteme = "environnement";

    parametreOriginal.setCodeApplication(codeApplication);
    parametreOriginal.setCreePar("sofi");
    parametreOriginal.setDateCreation(new Date());
    parametreOriginal.setCodeParametre(codeParametreSysteme);
    parametreOriginal.setValeur("unitaire");

    parametre.setCodeApplication(codeApplication);
    parametre.setCreePar("sofi");
    parametre.setDateCreation(new Date());
    parametre.setCodeParametre(codeParametreSysteme);
    parametre.setValeur("unitaire");

    this.getServiceParametreSystemeLocal().ajouter(parametreOriginal);
    valeur = this.getServiceParametreSystemeLocal().getValeur(codeApplication, codeParametreSysteme);

    parametre.setDescription("Test JUnit");

    this.getServiceParametreSystemeLocal().enregistrer(parametre, parametreOriginal);

    ParametreSysteme ps = this.getServiceParametreSystemeLocal().get(codeApplication, codeParametreSysteme);
    description = ps.getDescription();

    assertNotNull(description);
    assertEquals(description, "Test JUnit", description);
  }

  public void testgetListeParametresPourTableauBord() {
    GestionnaireUtilisateur gu = (GestionnaireUtilisateur) this.getBean("gestionnaireUtilisateur");
    Utilisateur u = new Utilisateur();
    u.setCodeUtilisateur("sofi");
    gu.setUtilisateur(u);
    FiltreParametreSysteme f = new FiltreParametreSysteme();
    Integer nombreJours = (365 * 3); // Aller chercher l'historique sur 3 ans
    String codeApplication = "INFRA_SOFI";
    ListeNavigation ln = new ListeNavigation(1, Integer.MAX_VALUE);

    f.setCodeApplication(codeApplication);
    ln.setFiltre(f);

    ln = this.getServiceParametreSystemeLocal().getListeParametresPourTableauBord(ln, nombreJours, codeApplication);

    assertNotNull(ln);
    assertFalse(ln.isVide());
    gu.libererUtilisateur();
  }

  public void testgetListeParametrePilotage() {
    GestionnaireUtilisateur gu = (GestionnaireUtilisateur) this.getBean("gestionnaireUtilisateur");
    Utilisateur u = new Utilisateur();
    u.setCodeUtilisateur("sofi");
    gu.setUtilisateur(u);
    FiltreParametreSysteme f = new FiltreParametreSysteme();
    f.setCodeApplication("INFRA_SOFI");
    ListeNavigation ln = new ListeNavigation(1, Integer.MAX_VALUE);
    ln.setFiltre(f);

    ln = this.getServiceParametreSystemeLocal().getListeParametrePilotage(ln);

    assertNotNull(ln);
    assertFalse(ln.isVide());

    gu.libererUtilisateur();
  }
// todo compléter le test.
  public void testsupprimer() {
/*    ParametreSysteme parametre = new ParametreSysteme();
    String valeur = "";
    String codeApplication = "INFRA_SOFI";
    String codeParametreSysteme = "authentificationNomCookie";

    valeur = this.getServiceParametreSystemeLocal().getValeur(codeApplication, codeParametreSysteme);

    this.getServiceParametreSystemeLocal().supprimer(new Long(1));

    valeur = this.getServiceParametreSystemeLocal().getValeur(codeApplication, codeParametreSysteme);

    assertNull(valeur);
    */
  }

  public ServiceParametreSysteme getServiceParametreSystemeLocal() {
    return (ServiceParametreSysteme) this.getBean("serviceParametreSysteme");
  }
}
