package org.sofiframework.infrasofi.modele.aide.service.test;

import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.AideContextuelle;
import org.sofiframework.infrasofi.modele.entite.AssociationAideContextuelle;
import org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle;
import org.sofiframework.modele.securite.GestionnaireUtilisateur;
import org.sofiframework.test.BaseTest;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;


public class ServiceAideContextuelleTest extends BaseTest {
  
  private static Long ID_COMPOSANT_REFERENTIEL = new Long("37");

  private static String CLE_AIDE = "infra_sofi.aide.accueil.service";
  
  private static String CODE_APPLICATION = "INFRA_SOFI";
  
  private static Integer NOMBRE_JOURS = 1;
  
  
  /**
   * Essai d'extraction d'un aide selon objet du referentiel
   */
  public void testGetAideContextuelle() {
      
      // Acces au service. 
      ServiceAideContextuelle serviceAideContextuelle = getServiceAideContextuelle();
      
      // obtention de la clé AideContextuelle
      AideContextuelle aideContext = serviceAideContextuelle.getAide(ID_COMPOSANT_REFERENTIEL,"fr_CA");

   // Les cas d'essais.
      assertNotNull("Essai infrastructure 'ServiceAide' : Probleme d'extraction d'une aide associe au composant :" + ID_COMPOSANT_REFERENTIEL.toString(), aideContext);
      
  }
  
  /**
   * Essai d'extraction d'un aide selon objet du referentiel
   */
  public void testGetAideContextuelleSelonCle() {
      // Acces au service. 
      ServiceAideContextuelle serviceAideContextuelle = getServiceAideContextuelle();
  
      // Extraire une aide contextuelle
      AideContextuelle aideContext = serviceAideContextuelle.getAide(CLE_AIDE, "fr_CA");
  
      // Les cas d'essais.
      assertNotNull("Essai infrastructure 'ServiceAide' : Probleme d'extraction d'une aide associe a la cle :" + CLE_AIDE, aideContext);
    
  }
  
  /**
   * Essai de suppression d'un AideContextuelle
   */
  public void testSupprimerAide() {
      
      // Acces au service. 
      ServiceAideContextuelle serviceAideContextuelle = getServiceAideContextuelle();

      //Supprimer 
      serviceAideContextuelle.supprimerAide(CLE_AIDE);
      
      // obtention de l'aide contextuelle
      AideContextuelle aideContextuelle = serviceAideContextuelle.getAide(CLE_AIDE, "fr_CA");
      
      // Les cas d'essais.
      assertNull(aideContextuelle);
      
    }
  
  /**
   * Essai de l'obtention de la liste Aide Pour Tableau Bord
   */
  public void testGetListeAidePourTableauBord(){
    // initialisation de la liste de navigation
    ListeNavigation listeNavigation = new ListeNavigation(0,200);

    // Acces au service. 
    ServiceAideContextuelle serviceAideContextuelle = getServiceAideContextuelle();
    
    //Obtention de la liste d'aide tableau de bord
    listeNavigation = serviceAideContextuelle.getListeAidePourTableauBord(listeNavigation, NOMBRE_JOURS,CODE_APPLICATION);
    
    // Les cas d'essais.
    assertFalse(ListeNavigation.isVide(listeNavigation));
  }
  
  public void testGetListeAideContextuellePilotage(){
    
    // initialisation de la liste de navigation
    ListeNavigation listeNavigation = new ListeNavigation(0,200);
    
    // Acces au service. 
    ServiceAideContextuelle serviceAideContextuelle = getServiceAideContextuelle();
    
   // Utilisateur courant 
    GestionnaireUtilisateur gu = (GestionnaireUtilisateur) this.getBean("gestionnaireUtilisateur");
    Utilisateur utilisateur = new Utilisateur();
    utilisateur.setCodeUtilisateur("sofi");
    gu.setUtilisateur(utilisateur);
      
    //Obtention de la liste AideContextuellePilotage
    listeNavigation = serviceAideContextuelle.getListeAideContextuellePilotage(listeNavigation);
    
    // Les cas d'essais.
    assertFalse(ListeNavigation.isVide(listeNavigation));
  }
  
  /**
   * Enregistrer un AideContextuelle (modification de la clé) 
   */
  
  public void testEnregistrerModifierCle() {
      
      // Acces au service. 
      ServiceAideContextuelle serviceAideContextuelle = getServiceAideContextuelle();
      String ancienneCle = "infra_sofi.aide.gestion_libelle.onglet.detail";
      
      
      // obtention de la clé AideContextuelle
      AideContextuelle aideContext = serviceAideContextuelle.getAide(ancienneCle, "fr_CA");
      aideContext.setCleAide("infra_sofi.aide.gestion_libelle.onglet.detail.2");

      // modifier la clé contextuelle
      serviceAideContextuelle.enregistrer(aideContext, ancienneCle);
      
   // obtention de la clé AideContextuelle
      AideContextuelle aideContextEnregistrer = serviceAideContextuelle.getAide(aideContext.getCleAide(),"fr_CA");
      
      
      
   // Les cas d'essais.
      assertNotNull(aideContextEnregistrer);
      assertSame(aideContext.getCleAide(), aideContextEnregistrer.getCleAide());
      
    }
  /**
   * Enregistrer un AideContextuelle (modification d'un élément de AideContextuelle) 
   */
  public void testEnregistrerModifier() {
      
      // Acces au service. 
      ServiceAideContextuelle serviceAideContextuelle = getServiceAideContextuelle();
      String Cle = "infra_sofi.aide.gestion_libelle.onglet.detail";
      String ancienneCle = null;
      
      
      // obtention de la clé AideContextuelle
      AideContextuelle aideContext = serviceAideContextuelle.getAide(Cle, "fr_CA");
      
      // modification de l'aide externe
      aideContext.setAideExterne("INFRA_INFRA");
      

      // modifier la clé contextuelle
      serviceAideContextuelle.enregistrer(aideContext, ancienneCle);
      
   // obtention de la clé AideContextuelle
      AideContextuelle aideContextEnregistrer = serviceAideContextuelle.getAide(Cle,"fr_CA");
      
   // Les cas d'essais.
      assertNotNull(aideContextEnregistrer);
      assertSame(aideContext.getAideExterne(), aideContextEnregistrer.getAideExterne());
      
    }
  
  /**
   * Enregistrer un AideContextuelle (ajouter un nouvel élément AideContextuelle ) 
   */
 public void testEnregistrerAjouter() {
      
       // valeur pour la création d'un  AideContextuelle
       String cleAide = "ENTER_SOFI";
       String CodeLangue = "fr_CA";
       String codeApplication = "INFRA_SOFI";
       String texteAide = "ok doc";
       String titre = "Norme pour les tests";
       
      // Acces au service. 
      ServiceAideContextuelle serviceAideContextuelle = getServiceAideContextuelle();
      //String ancienneCle = "infra_sofi.aide.gestion_libelle.onglet.detail";
      
      // création d'un nouvel aide contextuelle
      AideContextuelle aideContext = new AideContextuelle();
      
      // modification des valeurs par défaut de AideContextuelle
      aideContext.setCleAide(cleAide);
      aideContext.setCodeLangue(CodeLangue);
      aideContext.setCodeApplication(codeApplication);
      aideContext.setTexteAide(texteAide);
      aideContext.setTitre(titre);
      
      // modifier la clé contextuelle
      serviceAideContextuelle.enregistrer(aideContext, null);
      
      // obtention de la clé AideContextuelle
      AideContextuelle aideContextEnregistrer = serviceAideContextuelle.getAide(cleAide,CodeLangue);
      
      // Les cas d'essais.
      assertNotNull(aideContextEnregistrer);
      assertSame(aideContext.getCleAide(), aideContextEnregistrer.getCleAide());
    }
  
 /**
  * Essaie obtention de la liste Association
  */
  public void testGetListeAssociation(){
    
    // Acces au service. 
    ServiceAideContextuelle serviceAideContextuelle = getServiceAideContextuelle();
    
    // obtention de la liste AssociationAideContextuelle
    List<AssociationAideContextuelle> lst =  serviceAideContextuelle.getListeAssociation(CLE_AIDE);
    
    // Les cas d'essais.
    assertNotNull(lst);
  }
  
}

