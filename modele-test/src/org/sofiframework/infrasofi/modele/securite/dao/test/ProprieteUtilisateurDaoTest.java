package org.sofiframework.infrasofi.modele.securite.dao.test;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.MetadonneePropriete;
import org.sofiframework.infrasofi.modele.entite.ProprieteUtilisateur;
import org.sofiframework.infrasofi.modele.filtre.FiltreProprieteUtilisateur;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.MetadonneeProprieteDao;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.ProprieteUtilisateurDao;
import org.sofiframework.infrasofi.modele.securite.fixture.ProprieteFixture;
import org.sofiframework.test.BaseTest;

public class ProprieteUtilisateurDaoTest extends BaseTest {

  public void testDao() {
    assertNotNull(this.getProprieteUtilisateurDao());
  }

  public void testAjouter() {
    ProprieteUtilisateur p = ajouter(1L);
    assertNotNull(p);
  }

  public void testModifier() {
    // Ajout
    ProprieteUtilisateur p = ajouter(1L);
    assertNotNull(p);

    p.setValeur("false");

    this.getProprieteUtilisateurDao().modifierProprieteUtilisateur(p);
    p = this.getProprieteUtilisateurDao().getProprieteUtilisateur(p.getId());

    assertNotNull(p);
  }

  private ProprieteUtilisateur ajouter(Long utilisateurId) {
    MetadonneePropriete m = ProprieteFixture.getNouvelleMetadonnePropriete();
    this.getMetadonneeProprieteDao().ajouterMetadonneePropriete(m);
    ProprieteUtilisateur p = ProprieteFixture.getNouvelleProprieteUtilisateur(m);
    p.setUtilisateurId(utilisateurId);
    this.getProprieteUtilisateurDao().ajouterProprieteUtilisateur(p);
    return p;
  }

  public void testgetListe() {
    // recherche pour un utilisateur
    ListeNavigation ln = new ListeNavigation(1, 20);
    FiltreProprieteUtilisateur filtre = new FiltreProprieteUtilisateur();
    filtre.setUtilisateurId(1L);
    ln.setFiltre(filtre);
    this.getProprieteUtilisateurDao().getListeProprieteUtilisateur(ln);
    Long count = ln.getNbListe();
    assertTrue(count.equals(2L));

    //recherche pour tous les utils.
    ln = new ListeNavigation(1, 20);
    filtre = new FiltreProprieteUtilisateur();
    ln.setFiltre(filtre);
    this.getProprieteUtilisateurDao().getListeProprieteUtilisateur(ln);
    count = ln.getNbListe();
    assertTrue(count.equals(3L));
  }
  
  private MetadonneeProprieteDao getMetadonneeProprieteDao() {
    return (MetadonneeProprieteDao) getBean("metadonneeProprieteDao");
  }

  private ProprieteUtilisateurDao getProprieteUtilisateurDao() {
    return (ProprieteUtilisateurDao) getBean("proprieteUtilisateurDao");
  }
}
