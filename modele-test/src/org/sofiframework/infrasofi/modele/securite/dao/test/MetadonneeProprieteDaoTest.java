package org.sofiframework.infrasofi.modele.securite.dao.test;

import java.util.List;

import org.sofiframework.infrasofi.modele.entite.MetadonneePropriete;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.MetadonneeProprieteDao;
import org.sofiframework.infrasofi.modele.securite.fixture.ProprieteFixture;
import org.sofiframework.test.BaseTest;

public class MetadonneeProprieteDaoTest extends BaseTest {

  
  public void testAjouter() {
    MetadonneePropriete m = ProprieteFixture.getNouvelleMetadonnePropriete();
    this.getDao().ajouterMetadonneePropriete(m);
    m = this.getDao().getMetadonneePropriete(m.getId());
    assertNotNull(m);
  }

  public void testDao() {
    assertNotNull(this.getDao());
  }

  public void testGetListe() {
    List<MetadonneePropriete> l = this.getDao().getListeMetadonneePropriete(null);
    assertNotNull(l);
  }

  private MetadonneeProprieteDao getDao() {
    return (MetadonneeProprieteDao) getBean("metadonneeProprieteDao");
  }
}
