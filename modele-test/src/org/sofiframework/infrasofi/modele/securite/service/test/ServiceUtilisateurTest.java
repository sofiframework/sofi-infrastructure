/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.test;

import java.util.ArrayList;
import java.util.List;

import org.sofiframework.application.securite.filtre.FiltrePropriete;
import org.sofiframework.application.securite.filtre.FiltreUtilisateur;
import org.sofiframework.application.securite.objetstransfert.ChangementMotPasse;
import org.sofiframework.application.securite.objetstransfert.Propriete;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.service.ServiceSecurite;
import org.sofiframework.application.securite.service.ServiceUtilisateur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.securite.encryption.UtilitaireEncryption;
import org.sofiframework.test.BaseTest;

public class ServiceUtilisateurTest extends BaseTest {

  private ServiceUtilisateur serviceUtilisateur = null;

  protected void configurer() throws Exception {
    super.configurer();
    this.serviceUtilisateur = (ServiceUtilisateur) this.getBean("serviceUtilisateurCommun");
  }

  protected void detruire() throws Exception {
    super.detruire();
    this.serviceUtilisateur = null;
  }

  public void testgetListeUtilisateurPourApplicationEtRole() {
    List liste = this.serviceUtilisateur.getListeUtilisateurPourApplicationEtRole("INFRA_SOFI", "PILOTE");
    assertTrue(liste != null && liste.size() > 0);
  }

  public void testgetUtilisateurParCode() {
    Utilisateur u = this.serviceUtilisateur.getUtilisateur("sofi");
    assertNotNull(u);
  }

  public void testgetUtilisateurParId() {
    Utilisateur u = this.serviceUtilisateur.getUtilisateurPourId(1L);
    assertNotNull(u);
  }

  public void testEncryptionMotPasse() {
    org.sofiframework.infrasofi.modele.service.ServiceUtilisateur service = (org.sofiframework.infrasofi.modele.service.ServiceUtilisateur) this.getContexte()
        .getBean("serviceUtilisateur");
    org.sofiframework.infrasofi.modele.entite.Utilisateur utilisateur = service.getUtilisateur("sofi");
    utilisateur.setMotPasse("sofi");

    String encryption = UtilitaireEncryption.digest(utilisateur.getMotPasse());
    utilisateur.setMotPasse(encryption);
    service.modifierUtilisateur(utilisateur);
    org.sofiframework.infrasofi.modele.entite.Utilisateur test = service.getUtilisateur("sofi");

    assertFalse(test.getMotPasse().trim().equalsIgnoreCase("sofi"));
  }

  public void testEnregistrerUtilisateur() {
    org.sofiframework.infrasofi.modele.service.ServiceUtilisateur service = (org.sofiframework.infrasofi.modele.service.ServiceUtilisateur) this.getContexte()
        .getBean("serviceUtilisateur");
    org.sofiframework.infrasofi.modele.entite.Utilisateur utilisateur = service.getUtilisateur("sofi");
    utilisateur.setCourriel("test11@gmail.co");
    service.modifierUtilisateur(utilisateur);
  }

  public void testChangementMotDePasse() {
    org.sofiframework.infrasofi.modele.service.ServiceUtilisateur service = (org.sofiframework.infrasofi.modele.service.ServiceUtilisateur) this.getContexte()
        .getBean("serviceUtilisateur");

    ChangementMotPasse changementMotPasse = new ChangementMotPasse();
    changementMotPasse.setCodeUtilisateur("sofi");
    changementMotPasse.setNouveauMotPasse("admin@test.com");
    changementMotPasse.setConfirmerMotPasse("admin@test.com");

    service.changerMotPasse(changementMotPasse);
  }

  public void testgetUtilisateurAvecObjetSecurises() {
    ServiceSecurite serviceSecurite = (ServiceSecurite) this.getBean("serviceSecuriteCommun");
    Utilisateur utilisateur1 = serviceSecurite.getUtilisateurSecuriseSelonClient("INFRA_SOFI", null, "sofi", "nurun");
    assertNotNull(utilisateur1);
    Utilisateur utilisateur2 = serviceSecurite.getUtilisateurSecuriseSelonClient("INFRA_SOFI", "JOURNALISATION", "sofi", null);
    assertNotNull(utilisateur2);
  }

  public void testInscription() {
    org.sofiframework.infrasofi.modele.entite.Utilisateur u = new org.sofiframework.infrasofi.modele.entite.Utilisateur();
    u.setCodeUtilisateur("joseph");
    u.setNom("artur");
    u.setPrenom("joseph");
    u.setMotPasse("sofi");
    u.setCodeStatut("A");
    u.setCourriel("jean-maxime.pelletier@sofiframework.org");
    String codeClient = "sofiframework.org";

    org.sofiframework.infrasofi.modele.service.ServiceUtilisateur serviceUtilisateurGestion = (org.sofiframework.infrasofi.modele.service.ServiceUtilisateur) this
        .getBean("serviceUtilisateur");

    serviceUtilisateurGestion.inscrire(u, codeClient);
  }

  public void testgetListe() {
    ListeNavigation ln = new ListeNavigation(1, 20);

    FiltreUtilisateur f = new FiltreUtilisateur();
    FiltrePropriete fp = new FiltrePropriete();
    fp.setCodeClient("nurun");
    FiltrePropriete fp2 = new FiltrePropriete();
    fp2.setCodeClient("sunmedia");
    List<FiltrePropriete> l = new ArrayList<FiltrePropriete>();
    l.add(fp);
    l.add(fp2);
    
    f.setListePropriete(l);
    ln.setFiltre(f);

    this.serviceUtilisateur.getListeUtilisateur(ln);

    assertFalse(ln.isVide());
  }
  
  public void testSauvegarde() {
    Utilisateur u = this.serviceUtilisateur.getUtilisateurPourId(1L);

    List<Propriete> props = u.getListePropriete();
    for (Propriete propriete : props) {
      propriete.setValeur("test");
    }

    this.serviceUtilisateur.enregistrerUtilisateur(u);
    u = this.serviceUtilisateur.getUtilisateur("sofi");
    props = u.getListePropriete();

    for (Propriete propriete : props) {
      assertTrue(propriete.getValeur().equals("test"));
    }
  }

  public void testgetListeUtilisateur() {
    List<FiltrePropriete> listeFiltrePropriete = this.serviceUtilisateur.getListeFiltrePropriete();
    assertNotNull(listeFiltrePropriete);
  }
}
