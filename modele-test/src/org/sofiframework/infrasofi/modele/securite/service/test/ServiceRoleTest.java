/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.test;

import java.util.List;

import org.sofiframework.infrasofi.modele.entite.Role;
import org.sofiframework.infrasofi.modele.service.ServiceRole;
import org.sofiframework.test.BaseTest;

public class ServiceRoleTest extends BaseTest {
  
//  public void testGetListeRoleDisponible() {
//    this.runAs("sofi");
//    ServiceRole service = (ServiceRole) getBean("serviceRole");
//    List liste = service.getListeRoleDisponible("sofi", "INFRA_SOFI");
//    assertNotNull(liste);
//    assertTrue(liste.size() > 0);
//  }

  public void testgetListeRole() {
    ServiceRole service = (ServiceRole) getBean("serviceRole");
    List<Role> liste = service.getListeRole("SOFI_DEMO");
    assertNotNull(liste);
    assertTrue(liste.size() > 0);
  }
  
  public void _testAjouterRole() {
    ServiceRole service = (ServiceRole) getBean("serviceRole");
    Role r1 = new Role();
    r1.setCodeRole("mon_role_n1");
    r1.setCodeApplication("SOFI_DEMO");
    r1.setCodeStatut("A");
    r1.setCodeTypeUtilisateur("temporaire");
    r1.setConsultation(Boolean.TRUE);
    r1.setCreePar("sofi");
    r1.setDescription("Role pour tester l'ajout");
    r1.setNom("Mon role N1");

    Role r2 = new Role();
    r2.setCodeRole("mon_role_n2");
    r2.setCodeApplication("SOFI_DEMO");
    r2.setCodeStatut("A");
    r2.setCodeTypeUtilisateur("temporaire");
    r2.setConsultation(Boolean.TRUE);
    r2.setCreePar("sofi");
    r2.setDescription("Role pour tester l'ajout");
    r2.setNom("Mon role N2");
    r2.setCodeRoleParent(r1.getCodeRole());
    r2.setCodeApplicationParent(r1.getCodeApplication());

    service.enregistrerRole(r1);
    service.enregistrerRole(r2);

    r1 = service.getRole("mon_role_n1", "SOFI_DEMO");
    r2 = service.getRole("mon_role_n2", "SOFI_DEMO");

    assertNotNull(r1);
    assertNotNull(r2);
    assertTrue(r1.getNiveau().equals(new Integer(1)));
    assertTrue(r2.getNiveau().equals(new Integer(2)));
  }
}
