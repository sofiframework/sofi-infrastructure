/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.test;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.modele.filtre.FiltreObjetJava;
import org.sofiframework.infrasofi.modele.service.ServiceReferentiel;
import org.sofiframework.test.BaseTest;

public class ServiceReferentielTest extends BaseTest {

  public void testGetListeOnglet() {
    FiltreObjetJava filtre = new FiltreObjetJava();
    filtre.setIdParent(59L);
    filtre.setType("ON");
    filtre.setCodeStatut("A");
    ListeNavigation liste = new ListeNavigation(1, 10);
    liste.setFiltre(filtre);
    ServiceReferentiel service = (ServiceReferentiel) getBean("serviceReferentiel");
    liste = service.getListeObjetJava(liste);
  }
  
  public void testGetListeServiceParApplication() {
    FiltreObjetJava filtre = new FiltreObjetJava();
    filtre.setCodeApplication("EREGROUPEMENT");
    filtre.setType("SE");
    filtre.setCodeStatut("A");
    ListeNavigation liste = new ListeNavigation(1, 10);
    liste.setFiltre(filtre);
    ServiceReferentiel service = (ServiceReferentiel) getBean("serviceReferentiel");
    liste = service.getListeObjetJava(liste);
  }

  public void testgetObjetJava() {
    ServiceReferentiel service = (ServiceReferentiel) getBean("serviceReferentiel");
    ObjetJava o = service.getObjetJava(new Long(34L));
    assertNotNull(o); 
  }
}
