/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.test;

import java.util.Date;
import java.util.List;

import org.sofiframework.application.securite.service.ServiceObjetSecurisable;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.modele.service.ServiceApplication;
import org.sofiframework.infrasofi.modele.service.ServiceReferentiel;
import org.sofiframework.test.BaseTest;

public class ServiceApplicationTest extends BaseTest {
  
  public void testgetListeApplication() {
    ServiceApplication service = (ServiceApplication) getBean("serviceApplication");
    List listeApplication = service.getListeApplication();
    assertNotNull(listeApplication);
    assertTrue(listeApplication != null && listeApplication.size() > 0);
  }
  
  public void testgetApplication() {
    ServiceReferentiel serviceReferentiel = (ServiceReferentiel) getBean("serviceReferentiel");
    ObjetJava o = serviceReferentiel.getObjetJava(new Long(23L));
    assertNotNull(o);

    ServiceApplication service = (ServiceApplication) getBean("serviceApplication");
    Application a = service.getApplication("INFRA_SOFI");
    assertNotNull(a);
  }

  public void testgetApplicationAvecFacette() {
    ServiceObjetSecurisable serviceObjetSecurisable = (ServiceObjetSecurisable) this.getBean("serviceObjetSecurisableCommun");

    org.sofiframework.application.securite.objetstransfert.Application a = 
      serviceObjetSecurisable.getApplicationAvecObjetSecurisable("INFRA_SOFI", "JOURNALISATION", true);

    assertNotNull(a);
  }

  public void testajouterApplication() {
    Application a = creerApplication(); 

    ServiceApplication service = (ServiceApplication) getBean("serviceApplication");
    service.enregistrer(a, null);

    Application a2 = service.getApplication("google");
    assertNotNull(a2);

    service.supprimerApplication(a2.getCodeApplication());

    Application a3 = service.getApplication("google");
    assertNull(a3);
  }
  
  public void testmodifierCleApplication() {
    Application a = creerApplication();
    ServiceApplication service = (ServiceApplication) getBean("serviceApplication");
    service.enregistrer(a, null);
    
    // On garde une version originale de l'application
    Application aOriginale = service.getApplication("google");
    
    // Modifier la clé
    a.setCodeApplication("MonGoogle");
    
    // On enregistre
    service.enregistrer(a, aOriginale);
    
    Application a2 = service.getApplication("MonGoogle");
    
    assertNotNull(a2);
  }
  
  private Application creerApplication() {
    Application a = new Application();
    a.setCodeApplication("google");
    a.setClePageErreur("a");
    a.setClePageMaintenance("b");
    a.setClePageSecurite("a");
    a.setAdresseSite("http://www.google.com");
    a.setAideContextuelle("google.aide.application");
    a.setCodeUtilisateurAdministrateur("admin");
    a.setCreePar("sofi");
    a.setDateDebutActivite(new Date());
    a.setDescription("Application de recherche Google");
    a.setNom("Recherche Google");
    a.setMotPasseAdministrateur("welcome");
    a.setStatut("A");
    a.setCommun(Boolean.FALSE);
    a.setExterne(Boolean.TRUE);
    a.setAcronyme("google");
    a.setDateDebutActivite(new Date());
    a.setDateFinActivite(new Date());
    a.setDescription("Site de recherche de google.");
    a.setIcone("images/monicone.jpg");
    a.setVersionLivraison("1.0");
    
    /*// sections et services
    ObjetJava section = new ObjetJava();
    section.setType("SC");
    section.setCodeApplication("google");
    section.setDateDebutActivite(new Date());
    section.setDescription("Section administration");
    section.setNom("google.libelle.section.administration");
    section.setSecurise(Boolean.TRUE);
    section.setOrdreAffichage(new Long(1L));
    
    List listeService = new ArrayList();
    ObjetJava service = new ObjetJava();
    service.setType("SE");
    service.setCodeApplication("google");
    service.setDateDebutActivite(new Date());
    service.setObjetJavaParent(section);
    service.setDescription("Gestion des utilisateurs");
    service.setNom("google.libelle.service.gestion_utilisateur");
    service.setSecurise(Boolean.TRUE);
    service.setOrdreAffichage(new Long(1L));
    listeService.add(service);
    
    section.setListeObjetJavaEnfant(listeService);
    
    List listeObjetJava = new ArrayList();
    listeObjetJava.add(section);
    a.setListeObjetJava(listeObjetJava);*/
    
    return a;
  }
}
