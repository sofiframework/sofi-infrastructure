package org.sofiframework.infrasofi.modele.securite.fixture;

import org.sofiframework.infrasofi.modele.entite.MetadonneePropriete;
import org.sofiframework.infrasofi.modele.entite.ProprieteUtilisateur;

public class ProprieteFixture {

  public static MetadonneePropriete getNouvelleMetadonnePropriete() {
    MetadonneePropriete m = new MetadonneePropriete();
    m.setNom("ma case a cocher X pour nurun");
    m.setTypeDonnee("checkbox");
    m.setCodeClient("nurun");
    return m;
  }
  
  public static ProprieteUtilisateur getNouvelleProprieteUtilisateur(MetadonneePropriete m) {
    ProprieteUtilisateur p = new ProprieteUtilisateur();
    p.setMetadonnee(m);
    p.setUtilisateurId(1L);
    p.setValeur("true");
    return p;
  }
}
