/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.dao.test;

import java.util.Date;

import org.sofiframework.application.rapport.domaine.DomaineRapport.CodeEtatTraitementMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportTraitement;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportTraitementDao;
import org.sofiframework.test.BaseTest;

/**
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class RapportTraitementDaoTest extends BaseTest {

  public void testAjouter() {
    RapportTraitement entree = new RapportTraitement();
    entree.setCodeApplication("INFRA_SOFI");
    entree.setCodeClient("CLI");
    entree.setRapportMetadonneeId(1L);
    entree.setCodeEtatTraitement("A_FAIRE");
    entree.setDateTraitement(new Date());
    entree.setNombrePeriode(1);
    entree.setDateCreation(new Date());
    entree.setCreePar("moi");

    Long id = (Long) getDao().ajouter(entree);
    RapportTraitement entreeAjoutee = (RapportTraitement) getDao().get(id);
    assertNotNull(entreeAjoutee);
    assertEquals(new Long(1L).longValue(), new Long(entreeAjoutee.getRapportMetadonneeId()).longValue());
  }

  public void testSupprimer() {
    RapportTraitement entree = new RapportTraitement();
    entree.setCodeApplication("INFRA_SOFI");
    entree.setCodeClient("CLI");
    entree.setRapportMetadonneeId(1L);
    entree.setCodeEtatTraitement("A_FAIRE");
    entree.setDateTraitement(new Date());
    entree.setNombrePeriode(1);
    entree.setDateCreation(new Date());
    entree.setCreePar("moi");
    getDao().ajouter(entree);

    entree = new RapportTraitement();
    entree.setCodeApplication("INFRA_SOFI");
    entree.setCodeClient("CLI");
    entree.setRapportMetadonneeId(2L);
    entree.setCodeEtatTraitement("A_FAIRE");
    entree.setDateTraitement(new Date());
    entree.setNombrePeriode(1);
    entree.setDateCreation(new Date());
    entree.setCreePar("moi");
    Long id2 = (Long) getDao().ajouter(entree);

    getDao().supprimer(id2);
    RapportTraitement entreeSupprimee = (RapportTraitement) getDao().get(id2);
    assertNull(entreeSupprimee);

  }

  public void testSupprimerRapportTraitement() {
    int nombre = getDao().supprimerRapportTraitement("BIDON", CodeEtatTraitementMetadonnee.A_TRAITER);
    assertEquals(2, nombre);
  }

  private RapportTraitementDao getDao() {
    return (RapportTraitementDao) getContexte().getBean("rapportTraitementDao");
  }
}
