/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.dao.test;

import java.util.Date;
import java.util.List;

import org.sofiframework.application.rapport.objetstransfert.RapportGabarit;
import org.sofiframework.application.rapport.objetstransfert.RapportMetadonnee;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportGabaritDao;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportMetadonneeDao;
import org.sofiframework.test.BaseTest;

/**
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class RapportGabaritDaoTest extends BaseTest {

  public void testConfiguration() {
    RapportGabaritDao dao = getDao();
    assertNotNull(dao);
  }

  public void testGet() {
    RapportGabarit rapportGabarit = (RapportGabarit) getDao().get(1L);
    assertNotNull(rapportGabarit);
    assertEquals(8, rapportGabarit.getListeRapportMetadonnee().size());
  }

  public void testAjouter() {
    RapportGabarit rapportGabarit = new RapportGabarit();
    rapportGabarit.setCodeApplication("INFRA_SOFI");
    rapportGabarit.setCodeClient("CLI");
    rapportGabarit.setCodeGabarit("GAB-1");
    rapportGabarit.setNom("gabarit3");
    rapportGabarit.setDateCreation(new Date());
    rapportGabarit.setCreePar("moi");
    rapportGabarit.getListeRapportMetadonnee().add((RapportMetadonnee) getRapportMetadonneeDao().get(1L));
    rapportGabarit.getListeRapportMetadonnee().add((RapportMetadonnee) getRapportMetadonneeDao().get(2L));

    Long rapportGabaritId = (Long) getDao().ajouter(rapportGabarit);
    RapportGabarit gabaritAjoute = (RapportGabarit) getDao().get(rapportGabaritId);
    assertNotNull(gabaritAjoute);
    assertEquals("gabarit3", gabaritAjoute.getNom());
    assertEquals(2, gabaritAjoute.getListeRapportMetadonnee().size());
    assertEquals("section1", gabaritAjoute.getListeRapportMetadonnee().get(1).getNom());
  }

  public void testGetListeRapportMetadonneeOrdonnee() {
    List<RapportMetadonnee> liste = getDao().getListeRapportMetadonneeOrdonnee("BIDON", "CLI", "GAB1");
    assertNotNull(liste);
    assertEquals(8, liste.size());
    assertEquals("section13", liste.get(4).getNom());
    assertEquals(new Integer(2), liste.get(4).getNiveau());
    assertEquals("section3", liste.get(7).getNom());
    assertEquals(new Integer(1), liste.get(7).getNiveau());
  }

  private RapportGabaritDao getDao() {
    return (RapportGabaritDao) getContexte().getBean("rapportGabaritDao");
  }

  private RapportMetadonneeDao getRapportMetadonneeDao() {
    return (RapportMetadonneeDao) getContexte().getBean("rapportMetadonneeDao");
  }
}
