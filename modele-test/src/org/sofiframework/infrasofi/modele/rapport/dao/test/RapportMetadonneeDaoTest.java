/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.dao.test;

import java.util.Date;

import org.sofiframework.application.rapport.objetstransfert.RapportMetadonnee;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportMetadonneeDao;
import org.sofiframework.test.BaseTest;

/**
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class RapportMetadonneeDaoTest extends BaseTest {

  public void testConfiguration() {
    RapportMetadonneeDao dao = getDao();
    assertNotNull(dao);
  }

  public void testGet() {
    RapportMetadonnee root = (RapportMetadonnee) getDao().get(1L);
    assertNotNull(root);
    assertNull(root.getRapportMetadonneeParent());

    RapportMetadonnee section2 = (RapportMetadonnee) getDao().get(3L);
    assertNotNull(section2);
    assertNotNull(section2.getRapportMetadonneeParent());
  }

  public void testAjouter() {
    RapportMetadonnee section4 = new RapportMetadonnee();
    section4.setCodeApplication("INFRA_SOFI");
    section4.setNom("section4");
    section4.setParentId(1L);
    section4.setCreePar("moi");
    section4.setDateCreation(new Date());
    section4.setCodeModeResultat("SIMPLE");
    Long rapportEntreeId = (Long) getDao().ajouter(section4);

    RapportMetadonnee sectionAjoutee = (RapportMetadonnee) getDao().get(rapportEntreeId);
    assertNotNull(sectionAjoutee);
    assertNotNull(sectionAjoutee.getRapportMetadonneeParent());
    assertEquals(new Long(1L).longValue(), new Long(sectionAjoutee.getRapportMetadonneeParent().getId()).longValue());

  }

  private RapportMetadonneeDao getDao() {
    return (RapportMetadonneeDao) getContexte().getBean("rapportMetadonneeDao");
  }
}
