/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.dao.test;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.sofiframework.application.rapport.objetstransfert.RapportResultat;
import org.sofiframework.application.rapport.objetstransfert.RapportResultatAgrege;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportResultatDao;
import org.sofiframework.test.BaseTest;
import org.sofiframework.utilitaire.Periode;

/**
 * @author Rémi Mercier (Nurun Inc.)
 * 
 */
public class RapportResultatDaoTest extends BaseTest {

  public void testGet() {
    RapportResultat resultat = (RapportResultat) getDao().get(1L);
    assertNotNull(resultat);
    assertEquals("10", resultat.getValeur());
  }

  public void testAjouter() {
    RapportResultat resultat = new RapportResultat();
    resultat.setCodeApplication("INFRA_SOFI");
    resultat.setCodeClient("CLI");
    resultat.setRapportMetadonneeId(1L);
    resultat.setValeur("20.00");
    resultat.setDateResultat(new Date());
    resultat.setNombrePeriode(1);
    resultat.setDateCreation(new Date());
    resultat.setCreePar("moi");
    Long id = (Long) getDao().ajouter(resultat);

    RapportResultat resultatAjoute = (RapportResultat) getDao().get(id);
    assertNotNull(resultatAjoute);
    assertEquals("20.00", resultatAjoute.getValeur());
  }

  public void testGetListeRapportResultatAgrege() throws ParseException {
    Periode periode = new Periode(DateUtils.parseDate("2013-02-09", new String[] { "yyyy-MM-dd" }),
        DateUtils.parseDate("2013-02-12 23:59:59", new String[] { "yyyy-MM-dd hh:mm:ss" }));
    Map<Long, List<RapportResultatAgrege>> map = getDao().getListeRapportResultatAgrege("BIDON",
        Arrays.asList(new String[] { "CLI", "CLI1" }), periode);
    assertNotNull(map);
    assertEquals(3, map.size());
  }

  private RapportResultatDao getDao() {
    return (RapportResultatDao) getContexte().getBean("rapportResultatDao");
  }
}
