/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.service.test;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SimpleTrigger;
import org.quartz.StatefulJob;
import org.quartz.Trigger;
import org.quartz.impl.jdbcjobstore.Constants;
import org.sofiframework.application.tacheplanifiee.GestionTachePlanifiee;
import org.sofiframework.application.tacheplanifiee.service.ServiceTachePlanifiee;
import org.sofiframework.application.tacheplanifiee.utils.CleSerialisable;
import org.sofiframework.infrasofi.modele.entite.QuartzFiredTrigger;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzFiredTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzJobListenerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzFiredTrigger;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzTrigger;
import org.sofiframework.infrasofi.modele.tacheplanifiee.service.test.JobDetailFactory.TypeJob;
import org.sofiframework.infrasofi.modele.tacheplanifiee.service.test.TriggerFactory.TypeTrigger;
import org.sofiframework.test.BaseTest;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class ServiceQuartzTest extends BaseTest {

  private ServiceTachePlanifiee service;
  
  @Override
  protected void configurer() throws Exception {
    service = GestionTachePlanifiee.getInstance().getService();
    super.configurer();
  }
  
  public void _testGetFiredTrigger() {
    QuartzFiredTrigger firedTrigger = new QuartzFiredTrigger(1L, "NON_CLUSTERED1295363877156");
    QuartzFiredTriggerDao dao = (QuartzFiredTriggerDao) this.getBean("firedTriggerDao");
    Object entite = dao.get(firedTrigger.getId());
    assertNotNull(entite);
  }
  
  public void testGetListeFiredTrigger() {
    FiltreQuartzFiredTrigger filtre = new FiltreQuartzFiredTrigger(1L);
    QuartzFiredTriggerDao dao = (QuartzFiredTriggerDao) this.getBean("firedTriggerDao");
    Object liste = dao.getListe(filtre);
    assertNotNull(liste);
  }
  
  public void testGetListeTriggerDao() {
    QuartzTriggerDao dao = (QuartzTriggerDao) this.getBean("triggerDao");
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(1L);
    List liste = dao.getListe(filtre);
    assertNotNull(liste);
  }
  
  public void testGetListeFiredTriggerDao() {
    QuartzFiredTriggerDao dao = (QuartzFiredTriggerDao) this.getBean("firedTriggerDao");
    FiltreQuartzFiredTrigger filtre = new FiltreQuartzFiredTrigger(1L);
    List liste = dao.getListe(filtre);
    assertNotNull(liste);
  }
  
  public void testDeleteFiredTriggerDao() {
    QuartzFiredTriggerDao dao = (QuartzFiredTriggerDao) this.getBean("firedTriggerDao");
    dao.deleteFiredTrigger(1L);
    assertNotNull(new Object());
  }
  
  public void testDeleteJobListenerDao() {
    QuartzJobListenerDao dao = (QuartzJobListenerDao) this.getBean("jobListenerDao");
    dao.supprimer(1L, "", "");
    assertNotNull(new Object());
  }
  
  //TODO a corriger
  public void _testPausedTriggerGroup() {
    service.deleteAllPausedTriggerGroups(1L);
    assertFalse(service.isTriggerGroupPaused(1L, "GROUP1"));
    
    service.insertPausedTriggerGroup(1L, "GROUP1");
    assertTrue(service.isTriggerGroupPaused(1L, "GROUP1"));
    service.insertPausedTriggerGroup(1L, "GROUP2");
    assertTrue(service.isTriggerGroupPaused(1L, "GROUP2"));
    service.insertPausedTriggerGroup(1L, "GROUP3");
    assertTrue(service.isTriggerGroupPaused(1L, "GROUP3"));
    service.insertPausedTriggerGroup(1L, "GROUP4");
    assertTrue(service.isTriggerGroupPaused(1L, "GROUP4"));
    
    service.deletePausedTriggerGroup(1L, "GROUP3");
    assertFalse(service.isTriggerGroupPaused(1L, "GROUP3"));
    
    service.deleteAllPausedTriggerGroups(1L);
    assertFalse(service.isTriggerGroupPaused(1L, "GROUP1"));
  }
  
  public void _testTrigger() throws ParseException {
    String nomGroup1 = "groupe1";
    JobDetail job1 = JobDetailFactory.getInstance().creerJob(TypeJob.JOB, nomGroup1);
    service.insertJobDetail(1L, job1, false);
    
    SimpleTrigger simpleTrigger1 = (SimpleTrigger) TriggerFactory.getInstance().creerTrigger(TypeTrigger.SIMPLE_TRIGGER, nomGroup1, job1.getName(), job1.getGroup() );
    SimpleTrigger simpleTrigger2 = (SimpleTrigger) TriggerFactory.getInstance().creerTrigger(TypeTrigger.SIMPLE_TRIGGER, nomGroup1, job1.getName(), job1.getGroup() );
    SimpleTrigger simpleTrigger3 = (SimpleTrigger) TriggerFactory.getInstance().creerTrigger(TypeTrigger.SIMPLE_TRIGGER, job1.getName(), job1.getGroup() );
    
    simpleTrigger1.setNextFireTime(new Date());
    simpleTrigger2.setNextFireTime(new Date());
    simpleTrigger3.setNextFireTime(new Date());
    
    service.insertTrigger(1L, simpleTrigger1, Constants.STATE_WAITING, job1);
    service.insertTrigger(1L, simpleTrigger2, Constants.STATE_COMPLETE, job1);
    service.insertTrigger(1L, simpleTrigger3, Constants.STATE_WAITING, job1);
    service.insertSimpleTrigger(1L, simpleTrigger1);
    service.insertSimpleTrigger(1L, simpleTrigger2);
    service.insertSimpleTrigger(1L, simpleTrigger3);
    
    JobDetail job2 = JobDetailFactory.getInstance().creerJob(TypeJob.STATEFUL_JOB, nomGroup1);
    service.insertJobDetail(1L, job2, false);
    CronTrigger cronTrigger1 =  (CronTrigger) TriggerFactory.getInstance().creerTrigger(TypeTrigger.CRON_TRIGGER, job2.getName(), job2.getGroup() );
    CronTrigger cronTrigger2 =  (CronTrigger) TriggerFactory.getInstance().creerTrigger(TypeTrigger.CRON_TRIGGER, nomGroup1, job2.getName(), job2.getGroup() );
    service.insertTrigger(1L, cronTrigger1, Constants.STATE_WAITING, job2);
    service.insertTrigger(1L, cronTrigger2, Constants.STATE_ACQUIRED, job2);
    service.insertCronTrigger(1L, cronTrigger1);
    service.insertCronTrigger(1L, cronTrigger2);
    
    // On vérifie que le job1 a 3 triggers 
    assertEquals(service.selectTriggerNamesForJob(1L, job1.getName(), job1.getGroup()).length, 3);
    // On vérifie que le job2 a 2 triggers 
    assertEquals(service.selectTriggerNamesForJob(1L, job2.getName(), job2.getGroup()).length, 2);
    
    // On vérifie les états des triggers
    assertEquals(service.selectTriggersInState(1L, Constants.STATE_WAITING).length, 3);
    assertEquals(service.selectTriggersInState(1L, Constants.STATE_COMPLETE).length, 1);
    assertEquals(service.selectTriggersInState(1L, Constants.STATE_ACQUIRED).length, 1);
    
    // on recupere les triggers qui sont dans un des etats STATE_WAITING
    CleSerialisable[] triggers = service.selectMisfiredTriggersInState(1L,Constants.STATE_WAITING,(new Date()).getTime());
    assertEquals(triggers.length,2);
    
    // On efface tout
    service.deleteCronTrigger(1L, cronTrigger1.getName(), cronTrigger1.getGroup());
    service.deleteCronTrigger(1L, cronTrigger2.getName(), cronTrigger2.getGroup());
    service.deleteSimpleTrigger(1L, simpleTrigger1.getName(), simpleTrigger1.getGroup());
    service.deleteSimpleTrigger(1L, simpleTrigger2.getName(), simpleTrigger2.getGroup());
    service.deleteSimpleTrigger(1L, simpleTrigger3.getName(), simpleTrigger3.getGroup());
    service.deleteTrigger(1L, cronTrigger1.getName(), cronTrigger1.getGroup());
    service.deleteTrigger(1L, cronTrigger2.getName(), cronTrigger2.getGroup());
    service.deleteTrigger(1L, simpleTrigger1.getName(), simpleTrigger1.getGroup());
    service.deleteTrigger(1L, simpleTrigger2.getName(), simpleTrigger2.getGroup());
    service.deleteTrigger(1L, simpleTrigger3.getName(), simpleTrigger3.getGroup());
    service.deleteJobDetail(1L, job1.getName(), job1.getGroup());
  }
 
  public void _testJobDetail(){
    JobDetail job = JobDetailFactory.getInstance().creerJob(TypeJob.STATEFUL_JOB);
    job.setName("JOB_TEST");
    job.setGroup("GROUPE_TEST");
    job.setDescription("Description de mon test");
    job.setJobDataMap(null);
    
    job.addJobListener("testListener1");
    job.addJobListener("testListener2");
    
  // tout d abord on enleve tout
    service.deleteJobListeners(1L, "JOB_TEST", "GROUPE_TEST");
    service.deleteJobDetail(1L, "JOB_TEST", "GROUPE_TEST");
 // on s assure que tout est enlever 
    assertFalse(service.jobExists(1L, "JOB_TEST", "GROUPE_TEST"));
   // on reinsere pour etre certains
    service.insertJobDetail(1L, job, false);
    assertTrue(service.jobExists(1L, "JOB_TEST", "GROUPE_TEST"));
    assertTrue(service.isJobStateful(1L, "JOB_TEST", "GROUPE_TEST"));

    job.setJobClass(JobExecution.class);
    service.updateJobData(1L, job, false);
    assertFalse(service.isJobStateful(1L, "JOB_TEST", "GROUPE_TEST"));

    service.insertJobListener(1L, job, "testListener3");
    String[] listenerNames = service.selectJobListeners(1L, "JOB_TEST", "GROUPE_TEST");
    assertEquals(listenerNames.length, 3);
    
  }
}

/**
 * Fabrique de trigger simple et cron
 * @author rmercier
 *
 */
class TriggerFactory {
  
  enum TypeTrigger {
    SIMPLE_TRIGGER, CRON_TRIGGER
  }
  
  private static final String[] cronExpressions = {
    "10 * * * * ?",
    "20 * * * * ?",
    "30 * * * * ?",
    "40 * * * * ?",
    "50 * * * * ?",
    "0 1 * * * ?",
    "0 2 * * * ?",
    "0 3 * * * ?",
    "0 4 * * * ?",
    "0 5 * * * ?"
  };
  
  private static TriggerFactory instance;
  private TriggerFactory() {
  }
  
  public static synchronized TriggerFactory getInstance() {
    if(instance == null) {
      instance = new TriggerFactory();
    }
    return instance;
  }
  
  public Trigger creerTrigger(TypeTrigger typeTrigger, String nomJob, String nomGroupe) throws ParseException {
    return creerTrigger1(typeTrigger, null, nomJob, nomGroupe);
  }
  
  public Trigger creerTrigger(TypeTrigger typeTrigger, String nomGroupeTrigger, String nomJob, String nomGroupe) throws ParseException {
    return creerTrigger1(typeTrigger, nomGroupeTrigger, nomJob, nomGroupe);
  }
  
  private Trigger creerTrigger1(TypeTrigger typeTrigger, String nomGroupeTrigger, String nomJob, String nomGroupe) throws ParseException {
    Trigger trigger = null;
    switch(typeTrigger) {
    case SIMPLE_TRIGGER :
      trigger = creerSimpleTrigger(nomJob, nomGroupe);
      break;
    case CRON_TRIGGER :
      trigger = creerCronTrigger(nomJob, nomGroupe);
      break;
    }
    
    if(nomGroupeTrigger != null) {
      trigger.setGroup(nomGroupeTrigger);
    }
    return trigger;
  }
  
  private Trigger creerSimpleTrigger(String nomJob, String nomGroupe) {
    String code = RandomStringUtils.randomAlphabetic(5);
    int nbMinute = RandomUtils.nextInt(60);
    int repeatCount = RandomUtils.nextInt(10);
    int repeatIntervale = RandomUtils.nextInt(10);
    
    Trigger trigger = new SimpleTrigger(
        "name_" + code,
        "group_" + code,
        nomJob,
        nomGroupe,
        new Date(Calendar.getInstance().getTime().getTime() - (1000 * 60 * nbMinute)),
        new Date(Calendar.getInstance().getTime().getTime() + (1000 * 60 * nbMinute)),
        repeatCount,
        repeatIntervale);
    
    return trigger;
  }
  
  private Trigger creerCronTrigger(String nomJob, String nomGroupe) throws ParseException {
    String code = RandomStringUtils.randomAlphabetic(5);
    int nbMinute = RandomUtils.nextInt(60);
    int indexCronExpression = RandomUtils.nextInt(9);
    Trigger trigger = new CronTrigger(
        "name_" + code,
        "group_" + code,
        nomJob,
        nomGroupe,
        new Date(Calendar.getInstance().getTime().getTime() - (1000 * 60 * nbMinute)),
        new Date(Calendar.getInstance().getTime().getTime() + (1000 * 60 * nbMinute)),
        cronExpressions[indexCronExpression]
    );
    return trigger;
  }
}

class JobDetailFactory {
  private static JobDetailFactory instance;
  
  enum TypeJob {
    STATEFUL_JOB, JOB;
  }
  
  private JobDetailFactory() {
  }
  
  public static synchronized JobDetailFactory getInstance() {
    if(instance == null) {
      instance = new JobDetailFactory();
    }
    return instance;
  }
  
  public JobDetail creerJob(TypeJob typeJob) {
    return creerJob1(typeJob, null);
  }
  
  public JobDetail creerJob(TypeJob typeJob, String nomGroupJob) {
    return creerJob1(typeJob, nomGroupJob);
  }
  
  @SuppressWarnings({ "rawtypes" })
  private JobDetail creerJob1(TypeJob typeJob, String nomGroupJob) {
    String code = RandomStringUtils.randomAlphabetic(5);
    Class clazz = null;
    if(TypeJob.STATEFUL_JOB.equals(typeJob)) {
      clazz = StatefulJobExecution.class;
    } else {
      clazz = JobExecution.class;
    }
    boolean isVolatile = RandomUtils.nextBoolean();
    boolean isDurable = RandomUtils.nextBoolean();
    boolean isRecover = RandomUtils.nextBoolean();
    
    JobDetail jobDetail = new JobDetail(
        "name_" + code,
        "group_" + code,
        clazz,
        isVolatile,
        isDurable,
        isRecover
    );
    if(nomGroupJob != null) {
      jobDetail.setGroup(nomGroupJob);
    }
    return jobDetail;
  }
}

class StatefulJobExecution implements StatefulJob {
  public void execute(JobExecutionContext context) throws JobExecutionException {
  }
}

class JobExecution implements Job {
  public void execute(JobExecutionContext context) throws JobExecutionException {
  }
}

