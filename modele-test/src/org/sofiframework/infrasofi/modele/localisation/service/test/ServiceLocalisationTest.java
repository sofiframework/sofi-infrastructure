/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.localisation.service.test;

import java.util.List;
import java.util.Locale;

import org.sofiframework.application.locale.objetstransfert.FuseauHoraire;
import org.sofiframework.application.locale.objetstransfert.Pays;
import org.sofiframework.application.locale.service.ServiceLocalisation;
import org.sofiframework.infrasofi.modele.localisation.service.local.ServiceLocalisationImpl;
import org.sofiframework.test.BaseTest;

public class ServiceLocalisationTest extends BaseTest {
  public void testgetListeFuseauHoraire() {
    ServiceLocalisationImpl service = new ServiceLocalisationImpl();
    List<FuseauHoraire> listeFuseau = service.getListeFuseauHoraire("Canada", Locale.ENGLISH);
    assertTrue(listeFuseau.size() > 0);
  }
  
  public void testMajPaysRegion() {
    ServiceLocalisation serviceLocalisation = (ServiceLocalisation) this.getBean("serviceLocalisationCommun");
    serviceLocalisation.chargerPaysEtRegions();
    
    List<Pays> pays = serviceLocalisation.getListePays();
    assertNotNull(pays);
  }
}
