SET DEFINE OFF:


INSERT INTO LOCALE VALUES ('fr_CA', 'infra_sofi.libelle.locale.francais.canada', 'fr', 'CA');
INSERT INTO LOCALE VALUES ('en_US', 'infra_sofi.libelle.locale.anglais.us', 'en', 'US');

INSERT INTO LOCALE_APPLICATION (code_locale, code_application, cle_libelle, ordre_affichage, defaut)
SELECT 
    'fr_CA', CODE_APPLICATION, 'infra_sofi.libelle.locale.francais.canada' , 1 , 'O'
FROM 
    APPLICATION 
;

INSERT INTO LOCALE_APPLICATION (code_locale, code_application, cle_libelle, ordre_affichage, defaut)
SELECT 
    'en_US', CODE_APPLICATION, 'infra_sofi.libelle.locale.anglais.us' , 2 , 'O'
FROM 
    APPLICATION 
;

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('infra_sofi.libelle.locale.francais.canada','fr_CA','INFRA_SOFI','Français',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('infra_sofi.libelle.locale.francais.canada','en_US','INFRA_SOFI','Français',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('infra_sofi.libelle.locale.anglais.us','fr_CA','INFRA_SOFI','English',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('infra_sofi.libelle.locale.anglais.us','en_US','INFRA_SOFI','English',null,null,null,null,1);



INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlListeNom', 'utilitaireSOFI.do?methode=obtenirListeCaches', 'Url pour obtenir la liste des caches.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlMaj', 'utilitaireSOFI.do?methode=viderCache&nomCache=', 'Url permettant de mettre à jour un nom de cache. Il faut spécifier le nom de la cache désiré en paramètre.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlMajLibelle', 'utilitaireSOFI.do?methode=mettreAJourLibelle&cleLibelle=', 'Url permettant de mettre à jour les libelles en cache.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlMajMessage', 'utilitaireSOFI.do?methode=mettreAJourMessage&cleMessage=', 'URL permettant la mise à jour des messages en cache.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlMajParametreSysteme', 'utilitaireSOFI.do?methode=mettreAJourParametreSysteme&codeParametre=', 'URL pemettant de mettre à jour les paramètres systèmes en cache.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlRecharger', 'utilitaireSOFI.do?methode=rechargerObjetCache&nomObjetCache=', 'Url pour recharger un élément de la cache.'); 


Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('infra_sofi.erreur.commun.securite_exception','fr_CA','INFRA_SOFI','EXCEPTION DE SÉCURITÉ: Accès interdit pour l''adresse suivante :','A',null,null,null,null,1);
Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('infra_sofi.erreur.commun.securite_exception','en_US','INFRA_SOFI','SECURITY EXCEPTION: No access for the following address:','A',null,null,null,null,1);