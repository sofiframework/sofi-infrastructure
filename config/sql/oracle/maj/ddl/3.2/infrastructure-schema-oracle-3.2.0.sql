-- Table générique pour support de client par système.

CREATE TABLE CLIENT    (
     CODE_CLIENT VARCHAR2 (50)  NOT NULL ,
     NOM VARCHAR2 (500) ,
     CODE_CLIENT_PARENT VARCHAR2 (50) ,
     CREE_PAR VARCHAR2 (200) ,
     DATE_CREATION DATE ,
     MODIFIE_PAR VARCHAR2 (200) ,
     DATE_MODIFICATION DATE ,
     VERSION NUMBER
    )
;


ALTER TABLE CLIENT
    ADD CONSTRAINT CLIENT_PK PRIMARY KEY ( CODE_CLIENT ) ;

ALTER TABLE CLIENT
    ADD CONSTRAINT CLIENT_CLIENT_FK FOREIGN KEY
    (
     CODE_CLIENT_PARENT
    )
    REFERENCES CLIENT
    (
     CODE_CLIENT
    )
;

-- Début migration des paramètres système pour support par client.
CREATE SEQUENCE PARAMETRE_SYSTEME_SEQ START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

ALTER TABLE PARAMETRE_SYSTEME     
	ADD ID_PARAMETRE_SYSTEME NUMBER(11);

ALTER TABLE PARAMETRE_SYSTEME     
	ADD DATE_DEBUT_ACTIVITE DATE;

ALTER TABLE PARAMETRE_SYSTEME     
	ADD DATE_FIN_ACTIVITE DATE;

ALTER TABLE PARAMETRE_SYSTEME     
	ADD CODE_CLIENT VARCHAR2(50);
  

CREATE TRIGGER T_PARAMETRE_SYSTEME_PK 
	BEFORE UPDATE ON PARAMETRE_SYSTEME FOR EACH ROW
BEGIN
   SELECT PARAMETRE_SYSTEME_SEQ.NEXTVAL INTO :NEW.ID_PARAMETRE_SYSTEME FROM DUAL;
END;
/

UPDATE PARAMETRE_SYSTEME SET VERSION =1  WHERE ID_PARAMETRE_SYSTEME is null;  

ALTER TABLE PARAMETRE_SYSTEME
disable CONSTRAINT PARAMETRE_SYSTEME_PK;

ALTER TABLE PARAMETRE_SYSTEME DROP CONSTRAINT PARAMETRE_SYSTEME_PK ;
DROP INDEX PARAMETRE_SYSTEME_PK;

ALTER TABLE PARAMETRE_SYSTEME ADD CONSTRAINT PARAMETRE_SYSTEME_PK PRIMARY KEY ( ID_PARAMETRE_SYSTEME  ) ;

DROP TRIGGER  T_PARAMETRE_SYSTEME_PK ; 

ALTER TABLE PARAMETRE_SYSTEME 
    ADD CONSTRAINT PARAMETRE_SYSTEME_CLIENT_FK FOREIGN KEY 
    ( 
     CODE_CLIENT
    ) 
    REFERENCES CLIENT 
    ( 
     CODE_CLIENT
    ) 
;


-- Fin migration des paramètres système pour support par client.

------------------
-- Facette Rapport
------------------

-- Suppression des tables et des séquences existantes
BEGIN
   EXECUTE IMMEDIATE 'drop table RAPPORT_METADONNEE cascade constraints purge';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
BEGIN
   EXECUTE IMMEDIATE 'drop table RAPPORT_RESULTAT cascade constraints purge';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
BEGIN
   EXECUTE IMMEDIATE 'drop table RAPPORT_TRAITEMENT cascade constraints purge';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
BEGIN
   EXECUTE IMMEDIATE 'drop table RAPPORT_GABARIT cascade constraints purge';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
BEGIN
   EXECUTE IMMEDIATE 'drop table RAPPORT_GABARIT_METADONNEE cascade constraints purge';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
BEGIN
   EXECUTE IMMEDIATE 'drop sequence RAPPORT_RESULTAT_SEQ';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -2289 THEN
         RAISE;
      END IF;
END;
/
BEGIN
   EXECUTE IMMEDIATE 'drop sequence RAPPORT_METADONNEE_SEQ';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -2289 THEN
         RAISE;
      END IF;
END;
/
BEGIN
   EXECUTE IMMEDIATE 'drop sequence RAPPORT_TRAITEMENT_SEQ';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -2289 THEN
         RAISE;
      END IF;
END;
/
BEGIN
   EXECUTE IMMEDIATE 'drop sequence RAPPORT_GABARIT_SEQ';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -2289 THEN
         RAISE;
      END IF;
END;
/

CREATE SEQUENCE RAPPORT_RESULTAT_SEQ 
    START WITH 1000 
    INCREMENT BY 1 
;

CREATE SEQUENCE RAPPORT_METADONNEE_SEQ 
    START WITH 1000 
    INCREMENT BY 1 
;

CREATE SEQUENCE RAPPORT_TRAITEMENT_SEQ 
    START WITH 1000 
    INCREMENT BY 1 
;

CREATE SEQUENCE RAPPORT_GABARIT_SEQ 
    START WITH 1000 
    INCREMENT BY 1 
;

-- Généré par Oracle SQL Developer Data Modeler 3.3.0.734
--   à :        2013-02-27 13:18:58 EST
--   site :      Oracle Database 11g
--   type :      Oracle Database 11g



CREATE TABLE RAPPORT_GABARIT 
    ( 
     ID INTEGER  NOT NULL , 
     CODE_APPLICATION VARCHAR2 (20 BYTE)  NOT NULL , 
     CODE_CLIENT VARCHAR2 (50)  NOT NULL , 
     CODE_GABARIT VARCHAR2 (50)  NOT NULL , 
     NOM VARCHAR2 (300 BYTE)  NOT NULL , 
     CODE_STATUT VARCHAR2 (1 BYTE) , 
     CREE_PAR VARCHAR2 (200 BYTE) DEFAULT 'system'  NOT NULL , 
     DATE_CREATION DATE DEFAULT SYSDATE  NOT NULL , 
     MODIFIE_PAR VARCHAR2 (200 BYTE) , 
     DATE_MODIFICATION DATE , 
     VERSION INTEGER DEFAULT 0  NOT NULL 
    ) 
;



COMMENT ON COLUMN RAPPORT_GABARIT.NOM IS 'Une clé de libellé ou le nom directement.' 
;

COMMENT ON COLUMN RAPPORT_GABARIT.CODE_STATUT IS 'Actif ou inactif.' 
;

ALTER TABLE RAPPORT_GABARIT 
    ADD CONSTRAINT RAPPORT_GABARIT_PK PRIMARY KEY ( ID ) ;

ALTER TABLE RAPPORT_GABARIT 
    ADD CONSTRAINT RAPPORT_GABARIT__UN UNIQUE ( CODE_APPLICATION , CODE_CLIENT , CODE_GABARIT ) ;


CREATE TABLE RAPPORT_GABARIT_METADONNEE 
    ( 
     RAPPORT_GABARIT_ID INTEGER  NOT NULL , 
     RAPPORT_METADONNEE_ID INTEGER  NOT NULL 
    ) 
;



ALTER TABLE RAPPORT_GABARIT_METADONNEE 
    ADD CONSTRAINT RAPPORT_GABARIT_METADONNEE_PK PRIMARY KEY ( RAPPORT_GABARIT_ID, RAPPORT_METADONNEE_ID ) ;



CREATE TABLE RAPPORT_METADONNEE 
    ( 
     ID INTEGER  NOT NULL , 
     CODE_APPLICATION VARCHAR2 (20 BYTE)  NOT NULL , 
     PARENT_ID INTEGER , 
     NOM VARCHAR2 (300 BYTE)  NOT NULL , 
     NOM_CLASSE_TRAITEMENT VARCHAR2 (250 BYTE) , 
     CODE_STATUT VARCHAR2 (1 BYTE) , 
     CODE_TYPE_METADONNEE VARCHAR2 (20 BYTE) , 
     ORDRE_AFFICHAGE INTEGER , 
     CODE_MODE_RESULTAT VARCHAR2 (20 BYTE)  NOT NULL , 
     IS_TOTALISABLE NUMBER (1) DEFAULT 0, 
     CREE_PAR VARCHAR2 (200 BYTE) DEFAULT 'system'  NOT NULL , 
     DATE_CREATION DATE DEFAULT SYSDATE  NOT NULL , 
     MODIFIE_PAR VARCHAR2 (200 BYTE) , 
     DATE_MODIFICATION DATE , 
     VERSION INTEGER DEFAULT 0  NOT NULL 
    ) 
;



COMMENT ON COLUMN RAPPORT_METADONNEE.NOM IS 'Une clé de libellé ou le nom directement.' 
;

COMMENT ON COLUMN RAPPORT_METADONNEE.NOM_CLASSE_TRAITEMENT IS 'Nom de la classe à instancier pour calculer cette entrée de rapport.' 
;

COMMENT ON COLUMN RAPPORT_METADONNEE.CODE_STATUT IS 'Actif ou inactif.' 
;

COMMENT ON COLUMN RAPPORT_METADONNEE.CODE_TYPE_METADONNEE IS 'Domaine de valeur. Spécifie le type de l''entrée de rapport (pourcentage, compte, montant, ...)' 
;

ALTER TABLE RAPPORT_METADONNEE 
    ADD CONSTRAINT RAPPORT_METADONNE_PK PRIMARY KEY ( ID ) ;



CREATE TABLE RAPPORT_RESULTAT 
    ( 
     ID INTEGER  NOT NULL , 
     CODE_APPLICATION VARCHAR2 (20 BYTE)  NOT NULL , 
     CODE_CLIENT VARCHAR2 (50)  NOT NULL , 
     RAPPORT_METADONNEE_ID INTEGER  NOT NULL , 
     VALEUR VARCHAR2 (20 BYTE)  NOT NULL , 
     DATE_RESULTAT DATE  NOT NULL , 
     NOMBRE_PERIODE INTEGER  NOT NULL , 
     CREE_PAR VARCHAR2 (200 BYTE) DEFAULT 'system'  NOT NULL , 
     DATE_CREATION DATE DEFAULT SYSDATE  NOT NULL , 
     MODIFIE_PAR VARCHAR2 (200 BYTE) , 
     DATE_MODIFICATION DATE , 
     VERSION INTEGER DEFAULT 0  NOT NULL 
    ) 
;



COMMENT ON COLUMN RAPPORT_RESULTAT.DATE_RESULTAT IS 'Journee pour laquelle compte le résultat.' 
;

COMMENT ON COLUMN RAPPORT_RESULTAT.NOMBRE_PERIODE IS 'Nombre de journees pour lesquelles vaut le resultat (une periode correspondant a un jour). ' 
;
CREATE UNIQUE INDEX RR_UNIQUE__IDX ON RAPPORT_RESULTAT 
    ( 
     CODE_APPLICATION, CODE_CLIENT, RAPPORT_METADONNEE_ID, TRUNC(DATE_RESULTAT), NOMBRE_PERIODE
    ) 
;

ALTER TABLE RAPPORT_RESULTAT 
    ADD CONSTRAINT RAPPORT_RESULTAT_PK PRIMARY KEY ( ID ) ;



CREATE TABLE RAPPORT_TRAITEMENT 
    ( 
     ID INTEGER  NOT NULL , 
     CODE_APPLICATION VARCHAR2 (20 BYTE)  NOT NULL , 
     CODE_CLIENT VARCHAR2 (50)  NOT NULL , 
     RAPPORT_METADONNEE_ID INTEGER  NOT NULL , 
     CODE_ETAT_TRAITEMENT VARCHAR2 (20 BYTE)  NOT NULL , 
     DATE_TRAITEMENT DATE  NOT NULL , 
     NOMBRE_PERIODE INTEGER  NOT NULL , 
     CREE_PAR VARCHAR2 (200 BYTE) DEFAULT 'system'  NOT NULL , 
     DATE_CREATION DATE DEFAULT SYSDATE  NOT NULL , 
     MODIFIE_PAR VARCHAR2 (200 BYTE) , 
     DATE_MODIFICATION DATE , 
     VERSION INTEGER DEFAULT 0  NOT NULL 
    ) 
;



COMMENT ON COLUMN RAPPORT_TRAITEMENT.CODE_ETAT_TRAITEMENT IS 'Domaine de valeur. A_TRAITER, EN_ERREUR,...' 
;

COMMENT ON COLUMN RAPPORT_TRAITEMENT.DATE_TRAITEMENT IS 'Journee pour laquelle lametadonnee de rapport doit etre traitee.' 
;
CREATE UNIQUE INDEX RT_UNIQUE__IDX ON RAPPORT_TRAITEMENT 
    ( 
     CODE_APPLICATION, CODE_CLIENT, RAPPORT_METADONNEE_ID, TRUNC(DATE_TRAITEMENT), NOMBRE_PERIODE
    ) 
;

ALTER TABLE RAPPORT_TRAITEMENT 
    ADD CONSTRAINT RAPPORT_TRAITEMENT_PK PRIMARY KEY ( ID ) ;




ALTER TABLE RAPPORT_GABARIT 
    ADD CONSTRAINT RAPPORT_GABARIT_CLIENT_FK FOREIGN KEY 
    ( 
     CODE_CLIENT
    ) 
    REFERENCES CLIENT 
    ( 
     CODE_CLIENT
    ) 
;


ALTER TABLE RAPPORT_GABARIT_METADONNEE 
    ADD CONSTRAINT RAPPORT_GABARIT_FK FOREIGN KEY 
    ( 
     RAPPORT_GABARIT_ID
    ) 
    REFERENCES RAPPORT_GABARIT 
    ( 
     ID
    ) 
;


ALTER TABLE RAPPORT_GABARIT_METADONNEE 
    ADD CONSTRAINT RAPPORT_METADONNE_FK FOREIGN KEY 
    ( 
     RAPPORT_METADONNEE_ID
    ) 
    REFERENCES RAPPORT_METADONNEE 
    ( 
     ID
    ) 
;


ALTER TABLE RAPPORT_GABARIT 
    ADD CONSTRAINT RG_APPLICATION_FK FOREIGN KEY 
    ( 
     CODE_APPLICATION
    ) 
    REFERENCES APPLICATION 
    ( 
     CODE_APPLICATION
    ) 
;


ALTER TABLE RAPPORT_METADONNEE 
    ADD CONSTRAINT RM_APPLICATION_FK FOREIGN KEY 
    ( 
     CODE_APPLICATION
    ) 
    REFERENCES APPLICATION 
    ( 
     CODE_APPLICATION
    ) 
;


ALTER TABLE RAPPORT_METADONNEE 
    ADD CONSTRAINT RM_RAPPORT_METADONNEE_FK FOREIGN KEY 
    ( 
     PARENT_ID
    ) 
    REFERENCES RAPPORT_METADONNEE 
    ( 
     ID
    ) 
;


ALTER TABLE RAPPORT_RESULTAT 
    ADD CONSTRAINT RR_APPLICATION_FK FOREIGN KEY 
    ( 
     CODE_APPLICATION
    ) 
    REFERENCES APPLICATION 
    ( 
     CODE_APPLICATION
    ) 
;


ALTER TABLE RAPPORT_RESULTAT 
    ADD CONSTRAINT RR_CLIENT_FK FOREIGN KEY 
    ( 
     CODE_CLIENT
    ) 
    REFERENCES CLIENT 
    ( 
     CODE_CLIENT
    ) 
;


ALTER TABLE RAPPORT_RESULTAT 
    ADD CONSTRAINT RR_RAPPORT_METADONNEE_FK FOREIGN KEY 
    ( 
     RAPPORT_METADONNEE_ID
    ) 
    REFERENCES RAPPORT_METADONNEE 
    ( 
     ID
    ) 
;


ALTER TABLE RAPPORT_TRAITEMENT 
    ADD CONSTRAINT RT_APPLICATION_FK FOREIGN KEY 
    ( 
     CODE_APPLICATION
    ) 
    REFERENCES APPLICATION 
    ( 
     CODE_APPLICATION
    ) 
;


ALTER TABLE RAPPORT_TRAITEMENT 
    ADD CONSTRAINT RT_CLIENT_FK FOREIGN KEY 
    ( 
     CODE_CLIENT
    ) 
    REFERENCES CLIENT 
    ( 
     CODE_CLIENT
    ) 
;


ALTER TABLE RAPPORT_TRAITEMENT 
    ADD CONSTRAINT RT_RAPPORT_METADONNEE_FK FOREIGN KEY 
    ( 
     RAPPORT_METADONNEE_ID
    ) 
    REFERENCES RAPPORT_METADONNEE 
    ( 
     ID
    ) 
;

// Ajustement dans CERTIFICAT_ACCES
ALTER TABLE CERTIFICAT_ACCES     
	ADD URL_REFERER VARCHAR2(2000)
;

