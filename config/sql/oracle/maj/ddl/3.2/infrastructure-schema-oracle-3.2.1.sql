--------------------------------------------
-- Début migration pour support par client.
--------------------------------------------


---------------------
-- Table LIBELLE_JAVA
---------------------
CREATE SEQUENCE LIBELLE_SEQ START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

ALTER TABLE LIBELLE_JAVA     
	ADD ID_LIBELLE NUMBER(11);

ALTER TABLE LIBELLE_JAVA     
	ADD CODE_CLIENT VARCHAR2(50);
  
-- On initialise la nouvelle clé primaire via un trigger
CREATE TRIGGER T_LIBELLE_JAVA_PK 
	BEFORE UPDATE ON LIBELLE_JAVA FOR EACH ROW
BEGIN
   SELECT LIBELLE_SEQ.NEXTVAL INTO :NEW.ID_LIBELLE FROM DUAL;
END;
/

UPDATE LIBELLE_JAVA SET VERSION = 1  WHERE ID_LIBELLE is null;  

ALTER TABLE LIBELLE_JAVA
disable CONSTRAINT LIBELLE_JAVA_PK;

ALTER TABLE LIBELLE_JAVA DROP CONSTRAINT LIBELLE_JAVA_PK ;
DROP INDEX LIBELLE_JAVA_PK;

ALTER TABLE LIBELLE_JAVA ADD CONSTRAINT LIBELLE_PK PRIMARY KEY ( ID_LIBELLE ) ;

DROP TRIGGER  T_LIBELLE_JAVA_PK ; 

ALTER TABLE LIBELLE_JAVA 
    ADD CONSTRAINT LIBELLE_CLIENT_FK FOREIGN KEY 
    ( 
     CODE_CLIENT
    ) 
    REFERENCES CLIENT 
    ( 
     CODE_CLIENT
    ) 
;

---------------------
-- Table MESSAGE_JAVA
---------------------
CREATE SEQUENCE MESSAGE_SEQ START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

ALTER TABLE MESSAGE_JAVA     
    ADD ID_MESSAGE NUMBER(11);

ALTER TABLE MESSAGE_JAVA     
    ADD CODE_CLIENT VARCHAR2(50);

-- On initialise la nouvelle clé primaire via un trigger  
CREATE TRIGGER T_MESSAGE_JAVA_PK 
    BEFORE UPDATE ON MESSAGE_JAVA FOR EACH ROW
BEGIN
   SELECT MESSAGE_SEQ.NEXTVAL INTO :NEW.ID_MESSAGE FROM DUAL;
END;
/

UPDATE MESSAGE_JAVA SET VERSION = 1  WHERE ID_MESSAGE is null;  

ALTER TABLE MESSAGE_JAVA
disable CONSTRAINT MESSAGE_JAVA_PK;

ALTER TABLE MESSAGE_JAVA DROP CONSTRAINT MESSAGE_JAVA_PK ;
DROP INDEX MESSAGE_JAVA_PK;

ALTER TABLE MESSAGE_JAVA ADD CONSTRAINT MESSAGE_PK PRIMARY KEY ( ID_MESSAGE ) ;

DROP TRIGGER  T_MESSAGE_JAVA_PK ; 

ALTER TABLE MESSAGE_JAVA 
    ADD CONSTRAINT MESSAGE_CLIENT_FK FOREIGN KEY 
    ( 
     CODE_CLIENT
    ) 
    REFERENCES CLIENT 
    ( 
     CODE_CLIENT
    ) 
;

-----------------------
-- Table DOMAINE_VALEUR
-----------------------
ALTER TABLE DOMAINE_VALEUR     
    ADD CODE_CLIENT VARCHAR2(50);

ALTER TABLE DOMAINE_VALEUR 
    ADD CONSTRAINT DOMAINE_VALEUR_CLIENT_FK FOREIGN KEY 
    ( 
     CODE_CLIENT
    ) 
    REFERENCES CLIENT 
    ( 
     CODE_CLIENT
    ) 
;

---------------------------------------------
-- Fin migration pour support par client.
---------------------------------------------

----------------------------------------
-- Migration de la journalisation
----------------------------------------

ALTER TABLE JOURNALISATION ADD (
    CODE_APPLICATION VARCHAR2(20),
    CODE_TYPE_UTILISATEUR VARCHAR(100),
    CODE_MODULE VARCHAR2(100),
    CODE_SYSTEME_EXTERNE VARCHAR(100),
    MESSAGE_ERREUR VARCHAR2(4000),
    CREE_PAR VARCHAR2(200)
);

UPDATE JOURNALISATION j SET j.CODE_APPLICATION = 
(
    SELECT oj.CODE_APPLICATION FROM OBJET_JAVA oj WHERE j.ID_OBJET_JAVA = oj.ID_OBJET_JAVA
) 
WHERE j.ID_OBJET_JAVA IS NOT NULL;

ALTER TABLE JOURNALISATION DROP CONSTRAINT JOURNALISATION_OBJET_JAVA_FK1;

ALTER TABLE JOURNALISATION MODIFY (
    ID_OBJET_JAVA NULL,
    DESC_ACTION VARCHAR2(4000)
);



----------------------------------------
-- Fin migraiton de la journalisation
----------------------------------------
