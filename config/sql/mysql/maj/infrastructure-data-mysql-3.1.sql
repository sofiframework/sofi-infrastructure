/* Les paramètres */

INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlListeNom', 'utilitaireSOFI.do?methode=obtenirListeCaches', 'Url pour obtenir la liste des caches.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlMaj', 'utilitaireSOFI.do?methode=viderCache&nomCache=', 'Url permettant de mettre à jour un nom de cache. Il faut spécifier le nom de la cache désiré en paramètre.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlMajLibelle', 'cacheUrlMajLibelle', 'Url permettant de mettre à jour les libelles en cache.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlMajMessage', 'utilitaireSOFI.do?methode=mettreAJourMessage&cleMessage=', 'URL permettant la mise à jour des messages en cache.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlMajParametreSysteme', 'utilitaireSOFI.do?methode=mettreAJourParametreSysteme&codeParametre=', 'URL pemettant de mettre à jour les paramètres systèmes en cache.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'cacheUrlRecharger', 'utilitaireSOFI.do?methode=rechargerObjetCache&nomObjetCache=', 'Url pour recharger un élément de la cache.');