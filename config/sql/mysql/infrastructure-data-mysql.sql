/* ------------------------------------------------ */
/* Code utilisateur et mot de passe (sofi/bonjour) */
/* ----------------------------------------------- */
INSERT INTO UTILISATEUR ( CODE_UTILISATEUR, MOT_PASSE, NOM, PRENOM, COURRIEL, CODE_STATUT, DATE_MOT_PASSE,
CODE_UTILISATEUR_APP ) VALUES ( 
'sofi', 'H3Hg9KybR82TvyaeQBerqrnTvWM=', 'SOFI', 'Pilote', 'info@sofiframework.org'
, 'A', now(), NULL); 


/* -------------------------------------------- */
/* Section donnée de l'infrastructure SOFI      */
/* -------------------------------------------- */

INSERT INTO APPLICATION ( CODE_APPLICATION, NOM, DESCRIPTION, DATE_DEBUT_ACTIVITE,
DATE_FIN_ACTIVITE, CODE_UTILISATEUR_ADMIN, MOT_PASSE_ADMIN, ADRESSE_SITE, CREE_PAR, CODE_STATUT,
VERSION_LIVRAISON ) VALUES ( 
'INFRA_SOFI', 'Infrastructure SOFI', 'La console de gestion de l''infrastructure SOFI'
, NULL, NULL, 'infra_sofi', 'infra_sofi', 'http://localhost:8080/console'
, 'sofi', 'A', '3.0'); 

INSERT INTO LOCALE VALUES ('fr_CA', 'infra_sofi.libelle.locale.francais.canada', 'fr', 'CA');
INSERT INTO LOCALE VALUES ('en_US', 'infra_sofi.libelle.locale.anglais.us', 'en', 'US');

INSERT INTO LOCALE_APPLICATION (code_locale, code_application, cle_libelle, ordre_affichage, defaut) values ('fr_CA', 'INFRA_SOFI', 'infra_sofi.libelle.locale.francais.canada' , 1 , 'O');
INSERT INTO LOCALE_APPLICATION (code_locale, code_application, cle_libelle, ordre_affichage, defaut) values ('en_US', 'INFRA_SOFI', 'infra_sofi.libelle.locale.anglais.us' , 2 , 'N');

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('infra_sofi.libelle.locale.francais.canada','fr_CA','INFRA_SOFI','Français',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('infra_sofi.libelle.locale.francais.canada','en_US','INFRA_SOFI','Français',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('infra_sofi.libelle.locale.anglais.us','fr_CA','INFRA_SOFI','English',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('infra_sofi.libelle.locale.anglais.us','en_US','INFRA_SOFI','English',null,null,null,null,1);

INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.aide.gestion_role.ajouter_role_enfant','fr_CA','INFRA_SOFI','Ajouter un rôle enfant au rôle suivant \'{0}\'.',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.aide.gestion_utilisateur.action.restriction_periode_activation','fr_CA','INFRA_SOFI','Il existe une restriction sur la période d''activation du profil.',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.aide.gestion_utilisateur.action.supprimer','fr_CA','INFRA_SOFI','Retirer l''autorisation au rôle de l''utilisateur.',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.commun.libelle.liste.page_x_de_y','fr_CA','INFRA_SOFI','page {0} de {1}',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.erreur.commun.nombre.erreur','en_US','INFRA_SOFI','Your form has {0} erreurs.',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.','fr_CA','INFRA_SOFI','Test',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.action.aide_ligne','fr_CA','INFRA_SOFI','Aide en ligne',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.action.journalisation','fr_CA','INFRA_SOFI','Journalisation',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.action.libelles','fr_CA','INFRA_SOFI','Libellés',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.action.messages','fr_CA','INFRA_SOFI','Messages',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.action.parametre_systemes','fr_CA','INFRA_SOFI','Paramètres systèmes',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.champ.codeApplication','fr_CA','INFRA_SOFI','Veuillez sélectionner un système ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.service','fr_CA','INFRA_SOFI','Accueil',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.titre.cliquez_consulter_aide_en_ligne','fr_CA','INFRA_SOFI','Cliquer sur le titre afin de consulter l''aide en ligne offert par le système sélectionné.',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.titre.cliquez_consulter_journalisation','fr_CA','INFRA_SOFI','Cliquer sur le titre afin de consulter la journalisation récente du système sélectionné.',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.titre.cliquez_consulter_libelle','fr_CA','INFRA_SOFI','Cliquer sur le titre afin de consulter les libellés récemment modifiés ou ajoutés pour le système sélectionné.',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.titre.cliquez_consulter_message','fr_CA','INFRA_SOFI','Cliquer sur le titre afin de consulter les messages récemment modifiés ou ajoutés pour le système sélectionné.',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.accueil.titre.cliquez_consulter_parametres','fr_CA','INFRA_SOFI','Cliquer sur le titre afin de consulter les paramètres systèmes récemment modifiés ou ajoutés pour le système sélectionné.',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.application.titre','en_US','INFRA_SOFI','SOFI Management console',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.application.titre','fr_CA','INFRA_SOFI','Console de gestion SOFI',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.baspage.copyright','en_US','INFRA_SOFI','SOFI Open source',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.baspage.copyright','fr_CA','INFRA_SOFI','SOFI Open source',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.action.exporter_pdf','fr_CA','INFRA_SOFI','Export PDF',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.action.rechercher','fr_CA','INFRA_SOFI','Lancer la recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.aide_en_ligne','fr_CA','INFRA_SOFI','Aide en ligne',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.aucun_application','fr_CA','INFRA_SOFI','Vous n''avez pas d''accès au pilotage d''applications. Créez une nouvelle application.',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.bienvenue','fr_CA','INFRA_SOFI','Bienvenue',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.bloc.criteres.recherche','fr_CA','INFRA_SOFI','Critères de recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.bloc.resultat_recherche','fr_CA','INFRA_SOFI','Résultats de recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.champ.a','fr_CA','INFRA_SOFI','à ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.champ.anglais','fr_CA','INFRA_SOFI','Anglais',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.champ.creePar','fr_CA','INFRA_SOFI','Créé par',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.champ.de','fr_CA','INFRA_SOFI','De ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.champ.francais','fr_CA','INFRA_SOFI','Français',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.champ.modifiePar','fr_CA','INFRA_SOFI','Modifé par ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.champ.obligatoire','fr_CA','INFRA_SOFI','Champs obligatoires',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.champ.statutActivation','fr_CA','INFRA_SOFI','Statut d''activation ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.element_inactif','fr_CA','INFRA_SOFI','(Inactif)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.entete.creation','fr_CA','INFRA_SOFI','Création',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.entete.maj','fr_CA','INFRA_SOFI','M.a.j',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.fermer_session','fr_CA','INFRA_SOFI','Fermer ma session',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.retour_tache_precedente','fr_CA','INFRA_SOFI','Retour à la tâche précédente',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.commun.supprimer','fr_CA','INFRA_SOFI','Supprimer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.contenu.section','fr_CA','INFRA_SOFI','Contenu',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.entete.accueil','en_US','INFRA_SOFI','Home',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.entete.accueil','fr_CA','INFRA_SOFI','Accueil',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.entete.english','en_US','INFRA_SOFI','English',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.entete.english','fr_CA','INFRA_SOFI','Anglais',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.entete.francais','en_US','INFRA_SOFI','French',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.entete.francais','fr_CA','INFRA_SOFI','Français',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.entete.quitter','en_US','INFRA_SOFI','Logout',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.entete.quitter','fr_CA','INFRA_SOFI','Quitter',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.application.champ.contenu.page','fr_CA','INFRA_SOFI','Contenu de la page ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.application.champ.identifiant.page','fr_CA','INFRA_SOFI','Identifiant ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.application.champ.titre.page','fr_CA','INFRA_SOFI','Titre ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.application.section.contenu.langue','fr_CA','INFRA_SOFI','Contenu par langue',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.application.section.detail.objet.java','fr_CA','INFRA_SOFI','Détail',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.role.bouton.creer','fr_CA','INFRA_SOFI','Créer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.role.bouton.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.role.bouton.rechercher','fr_CA','INFRA_SOFI','Rechercher',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.role.bouton.supprimer','fr_CA','INFRA_SOFI','Supprimer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.role.colonne.application','fr_CA','INFRA_SOFI','Application',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.role.colonne.nom','fr_CA','INFRA_SOFI','Nom',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.role.onglet.detail','fr_CA','INFRA_SOFI','Rôle',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.role.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.role.section.critere.recherche','fr_CA','INFRA_SOFI','Critères de recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.role.section.detail.role','fr_CA','INFRA_SOFI','Détail',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.role.section.resultat.element','fr_CA','INFRA_SOFI','rôle(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.bouton.ajouter','fr_CA','INFRA_SOFI','Ajouter',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.bouton.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.bouton.rechercher','fr_CA','INFRA_SOFI','Lancer la recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.bouton.reporter.securite','fr_CA','INFRA_SOFI','Reporter la sécurité',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.application','fr_CA','INFRA_SOFI','Système ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.composant.parent','fr_CA','INFRA_SOFI','Composant parent ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.description','fr_CA','INFRA_SOFI','Description ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.detail.identifiant','fr_CA','INFRA_SOFI','Identifiant ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.detail.systeme','fr_CA','INFRA_SOFI','Système ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.detail.type','fr_CA','INFRA_SOFI','Type ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.identifiant','fr_CA','INFRA_SOFI','Identifiant ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.nom','fr_CA','INFRA_SOFI','Nom ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.numero.composant','fr_CA','INFRA_SOFI','Numéro de composant ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.role','fr_CA','INFRA_SOFI','Rôle ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.section.composant.enfant','fr_CA','INFRA_SOFI','Composants enfants',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.statut','fr_CA','INFRA_SOFI','Statut d''activation ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.champ.type','fr_CA','INFRA_SOFI','Type ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.colonne.consultation.seulement','fr_CA','INFRA_SOFI','Consultation seulement',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.colonne.detail.nom','fr_CA','INFRA_SOFI','Nom',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.colonne.detail.type','fr_CA','INFRA_SOFI','Type',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.colonne.nom','fr_CA','INFRA_SOFI','Identifiant',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.colonne.role','fr_CA','INFRA_SOFI','Rôle',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.section.detail.objet.java','fr_CA','INFRA_SOFI','Détail de l''objet java',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.section.recherche','fr_CA','INFRA_SOFI','Critères de recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.section.resultat.element','fr_CA','INFRA_SOFI','objet(s) java',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.section.resultat.recherche','fr_CA','INFRA_SOFI','Résultat de la recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.section.role.autorise','fr_CA','INFRA_SOFI','Rôle(s) autorisé(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.securite.section.role.disponible','fr_CA','INFRA_SOFI','Rôle(s) disponible(s)',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.systeme.action.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.systeme.action.initialisation_aide_ligne','fr_CA','INFRA_SOFI','Initialisation de l''aide en ligne en cours...',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.systeme.action.initialisation_libelles','fr_CA','INFRA_SOFI','Initialisation des libellés en cours...',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.systeme.action.initialisation_messages','fr_CA','INFRA_SOFI','Initialisation des messages en cours...',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.systeme.action.libelle_aide_ligne','fr_CA','INFRA_SOFI','Initialiser l''aide en ligne',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.systeme.action.libelle_btn_libelles','fr_CA','INFRA_SOFI','Initialiser les libellés',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.systeme.action.libelle_btn_messages','fr_CA','INFRA_SOFI','Initialiser les messages',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.systeme.bloc.gestion_cache','fr_CA','INFRA_SOFI','Gestion de la cache',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.systeme.champ.selection_nom_objet','fr_CA','INFRA_SOFI','Sélectionner un nom de d''objet cache pour initialisation ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.utilisateur.action.liste.role','fr_CA','INFRA_SOFI','Rôle(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.utilisateur.champ.code_client','fr_CA','INFRA_SOFI','Client',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.utilisateur.champ.code_role','fr_CA','INFRA_SOFI','Code',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.utilisateur.champ.date_debut','fr_CA','INFRA_SOFI','Date début',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion.utilisateur.champ.date_fin','fr_CA','INFRA_SOFI','Date fin',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.action.ajouter','fr_CA','INFRA_SOFI','Ajouter',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.action.creer','fr_CA','INFRA_SOFI','Créer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.action.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.action.rechercher','fr_CA','INFRA_SOFI','Lancer la recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.action.supprimer','fr_CA','INFRA_SOFI','Supprimer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.bloc.detail','fr_CA','INFRA_SOFI','Aide contextuelle',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.bloc.services','fr_CA','INFRA_SOFI','Services et Onglets',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.champ.aideContextuelle','fr_CA','INFRA_SOFI','Aide contextuelle ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.champ.aideContextuelleExterne','fr_CA','INFRA_SOFI','Aide contextuelle externe ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.champ.cleAide','fr_CA','INFRA_SOFI','Identifiant','L''identifiant unique du texte d''aide',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.champ.codeApplication','fr_CA','INFRA_SOFI','Système','Le système dans lequel le texte d''aide est utilisé',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.champ.codeLangue','fr_CA','INFRA_SOFI','Langue','La langue du texte d''aide contextuelle',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.champ.texteAide','fr_CA','INFRA_SOFI','Texte aide','Le texte d''aide affiché',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.champ.titre','fr_CA','INFRA_SOFI','Titre ','Titre du texte d''aide',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.compteur.element.textes','fr_CA','INFRA_SOFI','texte(s) d''''aide','Nombre de textes d''aide',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.entete.action.dissocier','fr_CA','INFRA_SOFI','Dissocier',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.entete.cleAide','fr_CA','INFRA_SOFI','Identifiant','L''identifiant unique du texte d''aide',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.entete.codeApplication','fr_CA','INFRA_SOFI','Système','Le système dans lequel est utilisé le texte d''aide',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.entete.codeLangue','fr_CA','INFRA_SOFI','Langue','La langue du texte d''aide',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.entete.onglet','fr_CA','INFRA_SOFI','Onglet',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.entete.service','fr_CA','INFRA_SOFI','Service',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.entete.texteAide','fr_CA','INFRA_SOFI','Aide','Le texte d''aide affiché',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.entete.titre','fr_CA','INFRA_SOFI','Titre','Titre du texte d''aide',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.onglet.association','fr_CA','INFRA_SOFI','Association',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.onglet.detail','fr_CA','INFRA_SOFI','Aide en ligne',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.onglet.services','fr_CA','INFRA_SOFI','Services et Onglets',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_aide.service','fr_CA','INFRA_SOFI','Gérer l''aide en ligne',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_application.onglet.cache','fr_CA','INFRA_SOFI','Cache','','','','sofi','2010-08-10 13:22:38',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_application.onglet.detail','fr_CA','INFRA_SOFI','Système',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_application.onglet.page.erreur','en_US','INFRA_SOFI','Error page',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_application.onglet.page.erreur','fr_CA','INFRA_SOFI','Page d''erreur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_application.onglet.page.maintenance','fr_CA','INFRA_SOFI','Page de maintenance',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_application.onglet.page.securite','fr_CA','INFRA_SOFI','Page de sécurité',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_application.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.action.creer.sous.domaine','fr_CA','INFRA_SOFI','Créer un sous-domaine',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.action.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.champ.systeme','fr_CA','INFRA_SOFI','Système ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.compteur.element','fr_CA','INFRA_SOFI','domaine(s) de valeur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.definition.compteur.element','fr_CA','INFRA_SOFI','définition(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.detail','fr_CA','INFRA_SOFI','Détail du domaine','','','','sofi','2010-09-22 15:11:14',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.entete.systeme','fr_CA','INFRA_SOFI','Application',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.onglet.detailDomaine','fr_CA','INFRA_SOFI','Détail du domaine',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.service','fr_CA','INFRA_SOFI','Gérer les domaines de valeur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.action.rechercher','fr_CA','INFRA_SOFI','Rechercher',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.au','fr_CA','INFRA_SOFI',' au ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.bloc.action.journalisee','fr_CA','INFRA_SOFI','Action journalisée',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.bloc.liste.utilisateurs','fr_CA','INFRA_SOFI','Liste des utilisateurs',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.codeUtilisateur','fr_CA','INFRA_SOFI','Code utilisateur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.courriel','fr_CA','INFRA_SOFI','Courriel',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.date','fr_CA','INFRA_SOFI','Date / heure ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.date.intervalle','fr_CA','INFRA_SOFI','Date / heure DE ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.detail','fr_CA','INFRA_SOFI','Description de l''action ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.nom','fr_CA','INFRA_SOFI','Nom',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.systeme','fr_CA','INFRA_SOFI','Système ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.type','fr_CA','INFRA_SOFI','Type ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.type.valeur.ajout','fr_CA','INFRA_SOFI','Ajout',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.type.valeur.consultation','fr_CA','INFRA_SOFI','Consultation',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.type.valeur.modification','en_US','INFRA_SOFI','EnModification',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.type.valeur.modification','fr_CA','INFRA_SOFI','Modification',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.champ.type.valeur.suppression','fr_CA','INFRA_SOFI','Suppression',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.compteur.element.actions','fr_CA','INFRA_SOFI','action(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.compteur.element.utilisateurs','fr_CA','INFRA_SOFI','utilisateur(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.critere_recherche','fr_CA','INFRA_SOFI','Critères de recherche',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.entete.codeUtilisateur','fr_CA','INFRA_SOFI','Code utilisateur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.entete.date','fr_CA','INFRA_SOFI','Date',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.entete.description','fr_CA','INFRA_SOFI','Description',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.entete.systeme','fr_CA','INFRA_SOFI','Système',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.entete.type','fr_CA','INFRA_SOFI','Type',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.onglet.detail','fr_CA','INFRA_SOFI','Écriture au journal',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_journalisation.service','fr_CA','INFRA_SOFI','La journalisation des accès',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.action.','fr_CA','INFRA_SOFI','Lancer la recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.action.acceder_securite_composant','fr_CA','INFRA_SOFI','Accéder à la sécurité du composant',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.action.cache','fr_CA','INFRA_SOFI','Initialiser la cache',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.action.creer','fr_CA','INFRA_SOFI','Créer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.action.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.action.modifier','fr_CA','INFRA_SOFI','Modifier',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.action.rechercher','fr_CA','INFRA_SOFI','Lancer la recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.action.supprimer','fr_CA','INFRA_SOFI','Supprimer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.bloc.detail','fr_CA','INFRA_SOFI','Libellé',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.champ.aideContextuelle','fr_CA','INFRA_SOFI','Aide contextuelle<br/>en format texte ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.champ.aideContextuelleExterne','fr_CA','INFRA_SOFI','Aide contextuelle externe ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.champ.aideContextuelleHtml','fr_CA','INFRA_SOFI','Aide contextuelle<br/>en format HTML ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.champ.cleLibelle','fr_CA','INFRA_SOFI','Identifiant ','L''identifiant unique du libellé',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.champ.codeApplication','fr_CA','INFRA_SOFI','Système ','Le système dans lequel le libellé fait partie',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.champ.codeLangue','fr_CA','INFRA_SOFI','Langue ','La langue du libellé',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.champ.reference','fr_CA','INFRA_SOFI','Référence ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.champ.texteLibelle','fr_CA','INFRA_SOFI','Texte du libellé ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.champ.libelleHtml','fr_CA','INFRA_SOFI','Libellé en version HTML',NULL,NULL,NULL,NULL,'2012-02-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.champ.placeholder','fr_CA','INFRA_SOFI','Placeholder',NULL,NULL,NULL,NULL,'2012-02-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.compteur.element.libelles','fr_CA','INFRA_SOFI','libellé(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.entete.cleLibelle','fr_CA','INFRA_SOFI','Identifiant','L''identifiant du libellé',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.entete.codeApplication','fr_CA','INFRA_SOFI','Système','Le système dans lequel le libellé fait partie',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.entete.codeLangue','fr_CA','INFRA_SOFI','Langue','La langue du libellé',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.entete.texteLibelle','fr_CA','INFRA_SOFI','Libellé','Le texte du libellé',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.onglet.detail','en_US','INFRA_SOFI','Label detail 2',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.onglet.detail','fr_CA','INFRA_SOFI','Libellé',NULL,'Essai de test',NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,'Rechercher les libell&eacute;s',NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_libelle.service','fr_CA','INFRA_SOFI','Gérer les libellés',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.action.creer','fr_CA','INFRA_SOFI','Créer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.action.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.action.rechercher','fr_CA','INFRA_SOFI','Lancer la recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.action.supprimer','fr_CA','INFRA_SOFI','Supprimer','','','','sofi','2010-09-14 15:07:50',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.bloc.detail','fr_CA','INFRA_SOFI','Message',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.champ.codeApplication','fr_CA','INFRA_SOFI','Système',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.champ.codeLangue','fr_CA','INFRA_SOFI','Langue ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.champ.codeSeverite','fr_CA','INFRA_SOFI','Type de message ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.champ.codeSeverite.valeur.avertissement','fr_CA','INFRA_SOFI','Avertissement',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.champ.codeSeverite.valeur.confirmation','fr_CA','INFRA_SOFI','Confirmation',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.champ.codeSeverite.valeur.erreur','fr_CA','INFRA_SOFI','Erreur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.champ.codeSeverite.valeur.information','fr_CA','INFRA_SOFI','Information',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.champ.codeSeverite.valeur.journalisation','fr_CA','INFRA_SOFI','Journalisation',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.champ.identifiant','fr_CA','INFRA_SOFI','Identifiant',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.champ.reference','fr_CA','INFRA_SOFI','Référence ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.champ.texte','fr_CA','INFRA_SOFI','Texte du message ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.compteur.element.messages','fr_CA','INFRA_SOFI','message(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.entete.codeApplication','fr_CA','INFRA_SOFI','Système',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.entete.codeLangue','fr_CA','INFRA_SOFI','Langue',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.entete.identifiant','fr_CA','INFRA_SOFI','Identifiant',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.entete.texte','fr_CA','INFRA_SOFI','Message',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.onglet.detail','fr_CA','INFRA_SOFI','Message',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_message.service','fr_CA','INFRA_SOFI','Gérer les messages',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.bouton.creer','fr_CA','INFRA_SOFI','Créér',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.bouton.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.bouton.initialiser_cache','fr_CA','INFRA_SOFI','Initialiser la cache',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.bouton.rechercher','fr_CA','INFRA_SOFI','Rechercher',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.bouton.supprimer','fr_CA','INFRA_SOFI','Supprimer',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.champ.code','fr_CA','INFRA_SOFI','Code',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.champ.code_application','fr_CA','INFRA_SOFI','Application',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.champ.code_parametre','fr_CA','INFRA_SOFI','Code',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.champ.code_type','fr_CA','INFRA_SOFI','Type',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.champ.description','fr_CA','INFRA_SOFI','Description',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.champ.different_environnement','fr_CA','INFRA_SOFI','Différent par environnement',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.champ.modification_a_distance','fr_CA','INFRA_SOFI','Disponible pour modification à distance',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.champ.valeur','fr_CA','INFRA_SOFI','Valeur',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.code_resultat','fr_CA','INFRA_SOFI','Code Paramètre',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.colonne.different_environnement','fr_CA','INFRA_SOFI','Diff. env.',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.colonne.modification_a_distance','fr_CA','INFRA_SOFI','Mod. dist.',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.lien_editer','fr_CA','INFRA_SOFI','Éditer',NULL,NULL,NULL,'sofi','2010-09-30 04:00:00',NULL,NULL,NULL,0);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.resultat','fr_CA','INFRA_SOFI','Résultat(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre.titre','fr_CA','INFRA_SOFI','Détail paramètre système',NULL,NULL,NULL,'sofi','2010-09-30 04:00:00',NULL,NULL,NULL,0);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre_systeme.onglet.detail','fr_CA','INFRA_SOFI','Paramètre système',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre_systeme.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_parametre_systeme.service','fr_CA','INFRA_SOFI','Gérer les paramètres systèmes',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.action.consulter','fr_CA','INFRA_SOFI','Consulter',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.action.creer','fr_CA','INFRA_SOFI','Créer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.action.eclater','fr_CA','INFRA_SOFI','Tout éclater',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.action.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.action.fermer','fr_CA','INFRA_SOFI','Tout fermer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.action.livraison','fr_CA','INFRA_SOFI','Livraison ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.action.nouveau_composant','fr_CA','INFRA_SOFI','Cliquer ici pour créer une aide contextuelle au composant d''interface',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.action.rechercher','fr_CA','INFRA_SOFI','Lancer la recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.action.retour_referentiel','fr_CA','INFRA_SOFI','Retourner au référentiel',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.action.structure','fr_CA','INFRA_SOFI','Visualiser l''objet référentiel dans la structure.',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.action.supprimer','fr_CA','INFRA_SOFI','Supprimer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.bloc.detail','fr_CA','INFRA_SOFI','Composant applicatif',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.bloc.liste.objets','fr_CA','INFRA_SOFI','Liste des objets du référentiel',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.bloc.structure','fr_CA','INFRA_SOFI','Structure référentielle',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.adresseWeb','fr_CA','INFRA_SOFI','Adresse Web ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.cleAide','fr_CA','INFRA_SOFI','Aide contextuelle',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.codeApplication','fr_CA','INFRA_SOFI','Système ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.codeFacette','fr_CA','INFRA_SOFI','Code facette ','','','','sofi','2010-08-10 13:22:38',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.code_parametre','fr_CA','INFRA_SOFI','Code ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.code_type','fr_CA','INFRA_SOFI','Type',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.dateDebutActivite','fr_CA','INFRA_SOFI','Date de début d''activation ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.dateFinActivite','fr_CA','INFRA_SOFI','Date de fin d''activation ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.description','fr_CA','INFRA_SOFI','Description ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.description_para','fr_CA','INFRA_SOFI','Description ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.libelle','fr_CA','INFRA_SOFI','Libellé ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.mode_affichage','fr_CA','INFRA_SOFI','Mode d''affichage ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.niveau_onglet','fr_CA','INFRA_SOFI','Niveau onglet ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.nom','fr_CA','INFRA_SOFI','Identifiant ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.nomAction','fr_CA','INFRA_SOFI','Nom action ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.nomParametre','fr_CA','INFRA_SOFI','Nom paramètre ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.nomParent','fr_CA','INFRA_SOFI','Composant parent ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.ordrePresentation','fr_CA','INFRA_SOFI','Ordre d''affichage ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.securise','fr_CA','INFRA_SOFI','Composant sécurisé ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.seqObjetSecurisable','fr_CA','INFRA_SOFI','Numéro de composant ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.statutActivation','fr_CA','INFRA_SOFI','Statut d''activation ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.type','fr_CA','INFRA_SOFI','Type ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.champ.valeur','fr_CA','INFRA_SOFI','Valeur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.composant_exclus_affichage','fr_CA','INFRA_SOFI','(caché)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.compteur.element','fr_CA','INFRA_SOFI','occurrence(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.entete.codeApplication','fr_CA','INFRA_SOFI','Système',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.entete.nom','fr_CA','INFRA_SOFI','Identifiant',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.entete.seqObjetSecurisable','fr_CA','INFRA_SOFI','ID',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.entete.type','fr_CA','INFRA_SOFI','Type','','','','sofi','2010-10-01 02:42:44',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.information_complementaire','fr_CA','INFRA_SOFI','Information complémentaire','','','','sofi','2010-08-10 13:22:38',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.message.confirmation.supprimer','fr_CA','INFRA_SOFI','Êtes-vous certain de vouloir supprimer l''objet référentiel \'{0}\'?',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.onglet.detail','fr_CA','INFRA_SOFI','Composant applicatif',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.onglet.structure','fr_CA','INFRA_SOFI','Structure référentielle','Vue générale du référentiel',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.service','fr_CA','INFRA_SOFI','Gérer le référentiel SOFI',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.statut.activation.actif','fr_CA','INFRA_SOFI','Actif',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.statut.activation.inactif','fr_CA','INFRA_SOFI','Inactif',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.structure_referentiel_systeme','fr_CA','INFRA_SOFI','Système ','','','','sofi','2010-08-10 13:22:38','sofi','2010-07-07 18:38:15','',1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.type.valeur.action','fr_CA','INFRA_SOFI','Action',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.type.valeur.bloc','fr_CA','INFRA_SOFI','Bloc',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.type.valeur.item','fr_CA','INFRA_SOFI','Item',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.type.valeur.onglet','fr_CA','INFRA_SOFI','Onglet',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.type.valeur.section','fr_CA','INFRA_SOFI','Section',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_referentiel.type.valeur.service','fr_CA','INFRA_SOFI','Service',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role.action.acces_composant_referentiel','fr_CA','INFRA_SOFI','Composants du référentiel',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role.action.acces_utilisateurs','fr_CA','INFRA_SOFI','Utilisateurs',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role.champ.codeRole','fr_CA','INFRA_SOFI','Code de rôle ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role.champ.codeRoleParent','fr_CA','INFRA_SOFI','Code rôle parent ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role.champ.description','fr_CA','INFRA_SOFI','Description ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role.champ.nom','fr_CA','INFRA_SOFI','Nom ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role.champ.systeme','fr_CA','INFRA_SOFI','Application ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role.colonne.codeRole','fr_CA','INFRA_SOFI','Code rôle',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role.onglet.detail','fr_CA','INFRA_SOFI','Détail',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role.texte.acces','fr_CA','INFRA_SOFI','Accès ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role_referentiel.detail','fr_CA','INFRA_SOFI','Composant du référentiel',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role_referentiel.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_role_referentiel.service','fr_CA','INFRA_SOFI','Gérer la sécurité du référentiel',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_securite.action.consulter_detail_referentiel','fr_CA','INFRA_SOFI','Consulter le détail du composant.',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_securite_referentiel.champ.codeApplication','fr_CA','INFRA_SOFI','Système',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_securite_referentiel.champ.nom','fr_CA','INFRA_SOFI','Nom',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_securite_referentiel.champ.statutActivation','fr_CA','INFRA_SOFI','Statut',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_securite_referentiel.champ.type','fr_CA','INFRA_SOFI','Type',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_securite_referentiel.compteur.element','fr_CA','INFRA_SOFI','résultat(s)',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_securite_referentiel.entete.nom','fr_CA','INFRA_SOFI','Nom',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_securite_referentiel.entete.systeme','fr_CA','INFRA_SOFI','Système',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_securite_referentiel.entete.type','fr_CA','INFRA_SOFI','Type',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.action.creer','fr_CA','INFRA_SOFI','Créer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.action.supprimer','fr_CA','INFRA_SOFI','Supprimer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.acronyme','fr_CA','INFRA_SOFI','Acronyme ','','','','sofi','2010-08-10 13:22:38',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.adresseSite','fr_CA','INFRA_SOFI','Adresse du site ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.adresseCache','fr_CA','INFRA_SOFI','Adresse pour les caches','Spécifier le ou les adresses de base pour communiquer avec les noeuds pour permettre le rafraichissement des caches. Si plusieurs, veuillez séparer avec des virgules.',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.code','fr_CA','INFRA_SOFI','Code système ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.codeUtilisateurAdministrateur','fr_CA','INFRA_SOFI','Code d''utilisateur administrateur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.commun','fr_CA','INFRA_SOFI','Commun ','','','','sofi','2010-08-10 13:22:38',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.dateDebut','fr_CA','INFRA_SOFI','Date de début d''activation ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.dateFin','fr_CA','INFRA_SOFI','Date de fin d''activation ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.description','fr_CA','INFRA_SOFI','Description ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.motPasseAdministrateur','fr_CA','INFRA_SOFI','Mot de passe administrateur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.nom','fr_CA','INFRA_SOFI','Nom ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.statut','fr_CA','INFRA_SOFI','Statut d''activation ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.champ.versionLivraison','fr_CA','INFRA_SOFI','Version courante ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.colonne.code','fr_CA','INFRA_SOFI','Code','','','','sofi','2010-08-10 13:22:38',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.colonne.commun','fr_CA','INFRA_SOFI','Commun','','','','sofi','2010-08-10 13:22:38',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.colonne.nom','fr_CA','INFRA_SOFI','Nom',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.colonne.url','fr_CA','INFRA_SOFI','URL',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.liste_resultat_element','fr_CA','INFRA_SOFI','système(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme.section.recherche','fr_CA','INFRA_SOFI','Critères de recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_systeme_recherche.champ.commun','fr_CA','INFRA_SOFI','Système commun','','','','sofi','2010-08-10 13:22:38','sofi','2010-06-18 19:12:59','',1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.action.creer','fr_CA','INFRA_SOFI','Créer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.action.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.action.rechercher','fr_CA','INFRA_SOFI','Rechercher',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.action.supprimer','fr_CA','INFRA_SOFI','Supprimer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.bloc.role_utilisateur','fr_CA','INFRA_SOFI','Détail du rôle autorisé',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.bloc.utilisateur','fr_CA','INFRA_SOFI','Utilisateur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.bouton.sauvegarder_utilisateur_application','fr_CA','INFRA_SOFI','Sauvegarder les informations pour cette application',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.code.utilisateur','fr_CA','INFRA_SOFI','Code utilisateur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.codeStatut.valeur.actif','fr_CA','INFRA_SOFI','Actif',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.codeStatut.valeur.enVacance','fr_CA','INFRA_SOFI','En vacances',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.codeStatut.valeur.inactif','fr_CA','INFRA_SOFI','Inactif',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.codeUtilisateurApplicatif','fr_CA','INFRA_SOFI','Code utilisateur applicatif ','Correspond à l''identifiant que le client utilise pour s''authentifier aux systèmes.',NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.code_client','fr_CA','INFRA_SOFI','Client',NULL,NULL,NULL,'sofi','2010-09-21 14:47:19',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.code_role','fr_CA','INFRA_SOFI','Code du rôle ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.courriel','fr_CA','INFRA_SOFI','Courriel ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.date_debut','fr_CA','INFRA_SOFI','Date début ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.date_fin','fr_CA','INFRA_SOFI','Date fin ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.description_role','fr_CA','INFRA_SOFI','Description du rôle ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.langue','fr_CA','INFRA_SOFI','Langue',NULL,NULL,NULL,'sofi','2010-09-28 04:00:00',NULL,NULL,NULL,0);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.mot.de.passe','fr_CA','INFRA_SOFI','Mot de passe ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.id','fr_CA','INFRA_SOFI','ID',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.nom','fr_CA','INFRA_SOFI','Nom ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.nom_role','fr_CA','INFRA_SOFI','Nom du rôle ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.organisation','fr_CA','INFRA_SOFI','Organisation',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.prenom','fr_CA','INFRA_SOFI','Prénom ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.statut','fr_CA','INFRA_SOFI','Statut ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.type_organisation','fr_CA','INFRA_SOFI','Type de l''organisation',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.champ.type_utilisateur','fr_CA','INFRA_SOFI','Type de l''utilisateur',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.compteur.element','fr_CA','INFRA_SOFI','utilisateur(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.entete.codeUtilisateurApplicatif','fr_CA','INFRA_SOFI','Code utilisateur applicatif',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.entete.code_utilisateur','fr_CA','INFRA_SOFI','Code d''utilisateur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.entete.courriel','fr_CA','INFRA_SOFI','Courriel',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.entete.nom','fr_CA','INFRA_SOFI','Nom',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.entete.statut','fr_CA','INFRA_SOFI','Statut',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.onglet.detail','fr_CA','INFRA_SOFI','Utilisateur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.onglet.detail_role_autorise','fr_CA','INFRA_SOFI','Le détail du rôle autorisé',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.onglet.role','fr_CA','INFRA_SOFI','Profil par système',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.section.utilisateur_application','fr_CA','INFRA_SOFI','Information pour cette application',NULL,NULL,NULL,'sofi','2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur.service','fr_CA','INFRA_SOFI','Gérer les utilisateurs',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.bouton.rechercher','fr_CA','INFRA_SOFI','Rechercher',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.bouton_ajouter','fr_CA','INFRA_SOFI','Ajouter',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.bouton_enregistrer','fr_CA','INFRA_SOFI','Enregistrer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.champ.ne_contient_pas','fr_CA','INFRA_SOFI','Ne contient pas',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.champ.nom','fr_CA','INFRA_SOFI','Nom ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.champ.prenom','fr_CA','INFRA_SOFI','Prénom ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.champ.role','fr_CA','INFRA_SOFI','Rôle ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.champ.systeme','fr_CA','INFRA_SOFI','Application ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.colonne.application','fr_CA','INFRA_SOFI','Application',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.colonne.nom','fr_CA','INFRA_SOFI','Nom',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.colonne.prenom','fr_CA','INFRA_SOFI','Prénom',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.colonne.role','fr_CA','INFRA_SOFI','Rôle',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.element.utilisateur','fr_CA','INFRA_SOFI','utilisateur(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.section.detail_utilisateur','fr_CA','INFRA_SOFI','Profil par système',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.section.resultat_recherche','fr_CA','INFRA_SOFI','Résultat de la recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.section.systeme_disponible','fr_CA','INFRA_SOFI','Système(s) disponible(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_utilisateur_role.section_recherche','fr_CA','INFRA_SOFI','Critères de recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_valeur_domaine.onglet.detail','fr_CA','INFRA_SOFI','Définition du domaine',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_valeur_domaine.onglet.recherche','fr_CA','INFRA_SOFI','Recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.message_cache.service','fr_CA','INFRA_SOFI','La cache des messages',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.pilotage.service','fr_CA','INFRA_SOFI','Pilotage',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.referentiel.section','fr_CA','INFRA_SOFI','Référentiel',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.securite.section','fr_CA','INFRA_SOFI','Sécurité',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.service.gestion.role','fr_CA','INFRA_SOFI','Gérer les rôles',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.service.gestion_systeme','fr_CA','INFRA_SOFI','Gérer les systèmes',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.service.journalisation.champ.code.utilisateur','fr_CA','INFRA_SOFI','Code utilisateur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.service.journalisation.champ.courriel','fr_CA','INFRA_SOFI','Courriel ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.service.journalisation.champ.date','fr_CA','INFRA_SOFI','Date et heure ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.service.journalisation.champ.detail','fr_CA','INFRA_SOFI','Texte de la journalisation',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38','sofi','2010-10-12 21:07:22',NULL,7);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.service.journalisation.champ.nom','fr_CA','INFRA_SOFI','Nom ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.service.journalisation.champ.systeme','fr_CA','INFRA_SOFI','Système ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.service.journalisation.champ.type','fr_CA','INFRA_SOFI','Type ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.service.journalisation.section.detail','fr_CA','INFRA_SOFI','Détail de la journalisation',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_erreur','fr_CA','INFRA_SOFI','Détail de l''erreur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.action','fr_CA','INFRA_SOFI','Action ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.date_heure','fr_CA','INFRA_SOFI','Date heure de l''erreur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.message','fr_CA','INFRA_SOFI','Message de l''erreur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.methode','fr_CA','INFRA_SOFI','Méthode ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.prenom_utilisateur','fr_CA','INFRA_SOFI','Prénom utilisateur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_erreur.champ.utilisateur','fr_CA','INFRA_SOFI','Nom utilisateur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_erreur.titre','fr_CA','INFRA_SOFI','Détail complet de l''erreur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.adresseIP','fr_CA','INFRA_SOFI','Adresse IP ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.codeUtilisateur','fr_CA','INFRA_SOFI','Code utilisateur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.cree_acces','fr_CA','INFRA_SOFI','Création de l''accès ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.derniere_action','fr_CA','INFRA_SOFI','Dernière action ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.dernier_acces','fr_CA','INFRA_SOFI','Dernier accès ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.navigateur','fr_CA','INFRA_SOFI','Type de navigateur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.nom','fr_CA','INFRA_SOFI','Nom ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.prenom','fr_CA','INFRA_SOFI','Prénom ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_utilisateur.champ.role','fr_CA','INFRA_SOFI','Rôles ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.detail_utilisateur.titre','fr_CA','INFRA_SOFI','Détail de l''utilisateur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.erreurs_site','fr_CA','INFRA_SOFI','Liste des erreurs',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.info','fr_CA','INFRA_SOFI','Information générale',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.info_general.action.initialiser_les_erreurs','fr_CA','INFRA_SOFI','Initialiser toutes les erreurs',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.info_general.champ.depart_application','fr_CA','INFRA_SOFI','Lancement du site ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.info_general.champ.nb_pages_consultees','fr_CA','INFRA_SOFI','Nombre de pages consultées ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.info_general.champ.nb_pages_echec','fr_CA','INFRA_SOFI','Nombre de pages en échec ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.info_general.champ.nb_utilisateurs','fr_CA','INFRA_SOFI','Nombre d''utilisateurs en ligne ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.info_general.champ.nb_utilisateurs_cumulatif','fr_CA','INFRA_SOFI','Nombre total d''utilisateurs  ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.info_general.titre','fr_CA','INFRA_SOFI','Information générale sur l''activité du site',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_erreurs.entete.action','fr_CA','INFRA_SOFI','Action',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_erreurs.entete.date_heure','fr_CA','INFRA_SOFI','Date heure',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_erreurs.entete.message_court','fr_CA','INFRA_SOFI','Message court',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_erreurs.entete.methode','fr_CA','INFRA_SOFI','Méthode',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_erreurs.entete.utilisateur','fr_CA','INFRA_SOFI','Utilisateur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_erreurs.titre','fr_CA','INFRA_SOFI','Liste des erreurs présentes sur le site',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.aucun_utilisateur','fr_CA','INFRA_SOFI','Aucun utilisateur présentement connecté sur le site.',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.entete.code','fr_CA','INFRA_SOFI','Code',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.entete.cree_acces','fr_CA','INFRA_SOFI','Création accès',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.entete.derniere_action','fr_CA','INFRA_SOFI','Dernière action',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.entete.dernier_acces','fr_CA','INFRA_SOFI','Dernier accès',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.entete.nom','fr_CA','INFRA_SOFI','Nom',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.liste_utilisateurs.titre','fr_CA','INFRA_SOFI','Liste des utilisateurs présentement en ligne',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.utilisateur','fr_CA','INFRA_SOFI','Détail utilisateur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.onglet.utilisateurs_en_ligne','fr_CA','INFRA_SOFI','Utilisateurs en ligne',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.section','fr_CA','INFRA_SOFI','Surveillance',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.surveillance.service','fr_CA','INFRA_SOFI','La surveillance en direct',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);

INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.action.creer','fr_CA','INFRA_SOFI','Créer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.action.creer_valeur_domaine','fr_CA','INFRA_SOFI','Créer une nouvelle valeur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.action.rechercher','fr_CA','INFRA_SOFI','Lancer la recherche',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.action.supprimer','fr_CA','INFRA_SOFI','Supprimer',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.champ.description','fr_CA','INFRA_SOFI','Description ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.champ.nom','fr_CA','INFRA_SOFI','Nom ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.champ.nom.parent','fr_CA','INFRA_SOFI','Nom du domaine parent ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.champ.ordreAffichage','fr_CA','INFRA_SOFI','Ordre d''affichage ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.champ.tri','fr_CA','INFRA_SOFI','Tri ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.champ.type','fr_CA','INFRA_SOFI','Type ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.champ.valeur','fr_CA','INFRA_SOFI','Valeur ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.champ.valeur.parent','fr_CA','INFRA_SOFI','Valeur du domaine parent ',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.entete.application','fr_CA','INFRA_SOFI','Système',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.entete.definition_domaine','fr_CA','INFRA_SOFI','Définition du domaine',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.entete.description','fr_CA','INFRA_SOFI','Description',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.entete.liste.sous.domaine','fr_CA','INFRA_SOFI','Liste des sous-domaines',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.entete.liste_valeur_domaine','fr_CA','INFRA_SOFI','Liste des valeurs du domaine',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.entete.nom','fr_CA','INFRA_SOFI','Nom',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.entete.valeur','fr_CA','INFRA_SOFI','Valeur',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.champ.langue', 'fr_CA', 'INFRA_SOFI', 'Langue ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.resume_domaine.titre', 'fr_CA', 'INFRA_SOFI', 'Domaine',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.resume_domaine.champ.nom', 'fr_CA', 'INFRA_SOFI', 'Nom',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.resume_domaine.champ.description', 'fr_CA', 'INFRA_SOFI', 'Description',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.resume_domaine.champ.tri', 'fr_CA', 'INFRA_SOFI', 'Tri',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.bouton.creer', 'fr_CA', 'INFRA_SOFI', 'Créer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.bouton.ordonner', 'fr_CA', 'INFRA_SOFI', 'Ordonner',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.bouton.effacer_ordres', 'fr_CA', 'INFRA_SOFI', 'Effacer les ordres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.colonne.ordre', 'fr_CA', 'INFRA_SOFI', 'Ordre',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.colonne.description', 'fr_CA', 'INFRA_SOFI', 'Description',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.colonne.valeur', 'fr_CA', 'INFRA_SOFI', 'Valeur',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.titre', 'fr_CA', 'INFRA_SOFI', 'Liste des valeurs du domaine',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);

INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi_libelle.gestion_role.action.vue_detaillee','fr_CA','INFRA_SOFI','Vue détaillée',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi_libelle.gestion_role.action.vue_simplifiee','fr_CA','INFRA_SOFI','Vue simplifiée',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi_libelle.gestion_role.champ.aucune_description','fr_CA','INFRA_SOFI','Aucune description',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi_libelle.gestion_role.resultat_role','fr_CA','INFRA_SOFI','rôle(s)',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi_libelle.gestion_role.titre.vue_hierarchique_detaillee','fr_CA','INFRA_SOFI','Vue hiérarchique détaillée des rôles',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('infra_sofi_libelle.gestion_role.titre.vue_hierarchique_simplifiee','fr_CA','INFRA_SOFI','Vue hiérarchique simplifiée des rôles',NULL,NULL,NULL,NULL,'2010-08-10 13:22:38',NULL,NULL,NULL,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.ordonnanceur.section','fr_CA','INFRA_SOFI','Tâche planifiée',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.service','fr_CA','INFRA_SOFI','Gérer les ordonnanceurs',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.onglet.recherche','fr_CA','INFRA_SOFI','Rechercher',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.onglet.detail','fr_CA','INFRA_SOFI','Ordonnanceur',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.section.recherche','fr_CA','INFRA_SOFI','Critères de recherche',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.champ.codeApplication','fr_CA','INFRA_SOFI','Système ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.champ.nom','fr_CA','INFRA_SOFI','Nom de l''ordonnanceur ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.champ.statut','fr_CA','INFRA_SOFI','Statut d''activation ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.action.creer','fr_CA','INFRA_SOFI','Ajouter',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.liste_resultat_element','fr_CA','INFRA_SOFI','Ordonnanceur(s)',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.colonne.nom','fr_CA','INFRA_SOFI','Nom de l''ordonnanceur',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.colonne.description','fr_CA','INFRA_SOFI','Description',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.ordonnanceur.section.detail.objet.java','fr_CA','INFRA_SOFI','Ordonnanceur',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.champ.codeOrdonnanceur','fr_CA','INFRA_SOFI','Code de l''ordonnanceur ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.champ.description','fr_CA','INFRA_SOFI','Description ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.action.supprimer','fr_CA','INFRA_SOFI','Supprimer',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.ordonnanceur.action.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.declencheur.section.detail.objet.java','fr_CA','INFRA_SOFI','Déclencheur',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.type','fr_CA','INFRA_SOFI','Type de déclencheur ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.nom','fr_CA','INFRA_SOFI','Nom ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.groupe','fr_CA','INFRA_SOFI','Groupe ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.description','fr_CA','INFRA_SOFI','Description ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.volatile','fr_CA','INFRA_SOFI','Volatile',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.declencheur.section.declencheur.simple','fr_CA','INFRA_SOFI','Déclencheur simple',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.declencheur.section.declencheur.blob','fr_CA','INFRA_SOFI','Déclencheur de type blob',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.declencheur.section.declencheur.cron','fr_CA','INFRA_SOFI','Déclencheur de type expression CRON',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.hashmap','fr_CA','INFRA_SOFI','Propriétés dynamiques',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.nombre_execution','fr_CA','INFRA_SOFI','Nombre d''exécution ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.interval','fr_CA','INFRA_SOFI','Interval (ms) ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.expression_cron','fr_CA','INFRA_SOFI','Expression cron ',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.section.propriete_dynamique','fr_CA','INFRA_SOFI','Propriété(s) dynamique(s)',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.propriete_dynamique.key','fr_CA','INFRA_SOFI','Clé',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.propriete_dynamique.value','fr_CA','INFRA_SOFI','Valeur',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.propriete_dynamique.action.appliquer','fr_CA','INFRA_SOFI','Appliquer',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.declencheur.action.fermer','fr_CA','INFRA_SOFI','Fermer',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.action.supprimer','fr_CA','INFRA_SOFI','Supprimer',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.action.creer','fr_CA','INFRA_SOFI','Ajouter',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.declencheur.action.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.priorite','fr_CA','INFRA_SOFI','Priorité','Si 2 triggers doivent être déclenchés au même moment, le schéduler fera sont possible pour lancer en premier celui avec la priorité la plus haute.',null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.missfired','fr_CA','INFRA_SOFI','Action lors d''une reprise ','Action à effectuer lors du redémarrage du schéduler, si le déclencheur a manqué son exécution.',null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.simple_missfired.1','fr_CA','INFRA_SOFI',' ','Indique à l''ordonnanceur que si une situation de reprise se présente, le déclencheur sera lancé immédiatement par l''ordonnanceur.',null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.simple_missfired.2','fr_CA','INFRA_SOFI',' ','Indique à l''ordonnanceur que si une situation de reprise se présente, le déclencheur sera lancer immédiatement par l''ordonnanceur en laissant le compteur de répétition comme il est. Par contre, ceci obéit au temps de fin du déclencheur. Si maintenant est après ce temps de fin, le déclencheur ne sera plus déclenché. Le comptage du déclencheur est remis à zéros.',null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.simple_missfired.3','fr_CA','INFRA_SOFI',' ','Indique à l''ordonnanceur que si une situation de reprise se présente, le déclencheur sera lancer immédiatement par l''ordonnanceur avec un compteur initialisé à ce qu''il aurait été si la situation de reprise n''avait pas eu lieu. Par contre, ceci obéit au temps de fin du déclencheur. Si maintenant est après ce temps de fin, le déclencheur ne sera pas déclenché.',null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.simple_missfired.4','fr_CA','INFRA_SOFI',' ','Indique à l''ordonnanceur que si une situation de reprise se présente, le déclencheur sera déclenché au prochain moment prévu et le compteur de répitition sera à ce qu''il aurait été si la situation de reprise n''avait pas eu lieu.',null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.simple_missfired.5','fr_CA','INFRA_SOFI',' ','Indique à l''ordonnanceur que si une situation de reprise se présente, le déclencheur sera déclenché au prochain moment prévue et le compteur de répétition restera inchangé.',null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.cron_missfired.1','fr_CA','INFRA_SOFI',' ','Indique à l''ordonnanceur que si une situation de reprise se présente, le déclencheur sera lancé immédiatement par l''ordonnanceur.',null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_declencheur.champ.cron_missfired.2','fr_CA','INFRA_SOFI',' ','Indique à l''ordonnanceur que si une situation de reprise se présente, le déclencheur sera lancé par l''ordonanceur au prochain moment prévu.',null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.onglet.tableau_de_bord','fr_CA','INFRA_SOFI','Tableau de bord',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.action.tableau_de_bord','fr_CA','INFRA_SOFI','Tableau de bord',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.ordonnanceur.section.tableau_de_bord.liste_job','fr_CA','INFRA_SOFI','Liste des jobs',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.ordonnanceur.section.tableau_de_bord.liste_declencheur','fr_CA','INFRA_SOFI','Liste des triggers',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.section.tableau_de_bord.liste_declencheur.colonne.action','fr_CA','INFRA_SOFI','Action',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.tableau_de_bord.delai','fr_CA','INFRA_SOFI','Délai de rafraichissement (en secondes)',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.ordonnanceur.tableau_de_bord.action.appliquer','fr_CA','INFRA_SOFI','Appliquer',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.liste_declencheur.colonne.date_derniere_execution','fr_CA','INFRA_SOFI','Date de la dernière exécution',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.liste_declencheur.colonne.date_prochaine_execution','fr_CA','INFRA_SOFI','Date de la prochaine exécution',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.liste_job.colonne.action.activer','fr_CA','INFRA_SOFI','Activer tous les déclencheurs de la job',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.liste_job.colonne.action.desactiver','fr_CA','INFRA_SOFI','Désactiver tous les déclencheurs de la job',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_ordonnanceur.tableau_de_bord.page_rafraichie','fr_CA','INFRA_SOFI','Page rafraichie à {0}',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_ordonnanceur.champ.heberge_par','fr_CA','INFRA_SOFI','Hébergé par',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.section.temps_execution','fr_CA','INFRA_SOFI','Temps d''éxécution',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.temps_execution.derniereExecution','fr_CA','INFRA_SOFI','Dernière exécution',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion.temps_execution.temps','fr_CA','INFRA_SOFI','Temps requis (ms)',null,null,null,'sofi',null,null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.service','fr_CA','INFRA_SOFI','Gérer les tâches',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.onglet.recherche','fr_CA','INFRA_SOFI','Rechercher',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.section.recherche','fr_CA','INFRA_SOFI','Critères de recherche',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.champ.codeApplication','fr_CA','INFRA_SOFI','Système ',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.champ.codeScheduler','fr_CA','INFRA_SOFI','Nom de l''ordonnanceur ',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.champ.groupe','fr_CA','INFRA_SOFI','Groupe ',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.champ.nom','fr_CA','INFRA_SOFI','Nom de la tâche ',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.action.creer','fr_CA','INFRA_SOFI','Ajouter',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.liste_resultat_element','fr_CA','INFRA_SOFI','Tâche(s)',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.colonne.groupe','fr_CA','INFRA_SOFI','Groupe de la tâche',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.colonne.nom','fr_CA','INFRA_SOFI','Nom de la tâche',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.colonne.description','fr_CA','INFRA_SOFI','Description',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.champ.description','fr_CA','INFRA_SOFI','Description ',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.champ.nom_classe','fr_CA','INFRA_SOFI','Nom de la classe ',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.champ.durable','fr_CA','INFRA_SOFI','Durable ','Si une tâche n est pas durable, elle sera automatiquement détruite de l''ordonnanceur une fois qu''il ne restera plus de déclencheur associé.',null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.champ.volatile','fr_CA','INFRA_SOFI','Volatile ','Si une tâche est volatile, celle-ci sera supprimée entre les redémarrages de l''ordonnanceur',null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.champ.etat_persistent','fr_CA','INFRA_SOFI','État persistent ','Si une tâche n'' est pas persitente, toutes les modifications effectuées à la tâche pendant son éxécution, ne seront pas envoyé dans la base de donnée, ces modifications seront perdues.',null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.champ.relancer_interuption','fr_CA','INFRA_SOFI','Relancer après un interuption ','Suite à un arrêt de l''ordonnanceur ou un redémarrage du serveur, si la tâche est en exécution, elle sera relancée.',null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.action.supprimer','fr_CA','INFRA_SOFI','Supprimer',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion.tache.action.enregistrer','fr_CA','INFRA_SOFI','Enregistrer',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion.tache.section.detail.objet.java','fr_CA','INFRA_SOFI','Tâche',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.hashmap','fr_CA','INFRA_SOFI','Propriétés dynamiques',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.titre.fenetre.hashmap','fr_CA','INFRA_SOFI','Propriétés dynamiques',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.aide.statefuljob','fr_CA','INFRA_SOFI','(Attention votre class doit implémenter l''interface StatefulJob)',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion.tache.action.fermer','fr_CA','INFRA_SOFI','Fermer',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.onglet.declencheur','fr_CA','INFRA_SOFI','Déclencheur',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion.tache.section.declencheur.objet.java','fr_CA','INFRA_SOFI','Déclencheur de la tâche',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.liste_declencheur_element','fr_CA','INFRA_SOFI','Déclencheur(s)',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.type','fr_CA','INFRA_SOFI','Type de déclencheur',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.groupe','fr_CA','INFRA_SOFI','Groupe',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.nom','fr_CA','INFRA_SOFI','Nom',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.description','fr_CA','INFRA_SOFI','Description',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.actif','fr_CA','INFRA_SOFI','Actif (état Quartz)',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.action','fr_CA','INFRA_SOFI','Action',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_tache.colonne.temps_derniere_execution','fr_CA','INFRA_SOFI','Temps requis pour la dernière exécution',null,null,null,'sofi','2011-03-01 13:22:08',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_tache.colonne.temps_moyen_execution','fr_CA','INFRA_SOFI','Temps moyen requis pour exécuter la tâche',null,null,null,'sofi','2011-03-01 13:22:08',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.libelle.gestion_tache.colonne.tache_non_stateful','fr_CA','INFRA_SOFI','Non applicable, tâche non stateful',null,null,null,'sofi','2011-03-01 13:22:08',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.colonne.actif','fr_CA','INFRA_SOFI','Actif','Au moins un trigger associé tâche est actif',null,null,'sofi','2011-03-01 13:22:08',null,null);
       
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion.ordonnanceur.section.tableau_de_bord.liste_tache','fr_CA','INFRA_SOFI','Liste des tâches',null,null,null,'sofi','2011-03-01 13:22:08',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.onglet.detail','fr_CA','INFRA_SOFI','Tâche',null,null,null,'sofi','2011-03-01 13:22:08',null,null);

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.libelle.gestion_tache.action.ajouter.declencheur','fr_CA','INFRA_SOFI','Ajouter',null,null,null,'sofi','2011-03-01 13:22:08',null,null);



INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.avertissement.commun.modification.formulaire','fr_CA','INFRA_SOFI','Une modification a été appliquée au formulaire.','A',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.avertissement.gestion_application.gestion_cache_avertissement','fr_CA','INFRA_SOFI','La gestion de la cache n''est pas disponible pour l''adresse du site spécifiée.','A',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.avertissement.gestion_role.onglet.recherche.aucun_role_defini','fr_CA','INFRA_SOFI','Aucun rôle présentement défini pour les critères de recherche spécifiés.','A',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.commun.fermer_session','en_US','INFRA_SOFI','Are you sure you want to quit this page?','C',NULL,NULL,NULL,'sofi','2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.commun.fermer_session','fr_CA','INFRA_SOFI','Ëtes-vous sûr de vouloir fermer votre session?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.commun.formulaire_transactionel_modifie','fr_CA','INFRA_SOFI','Votre formulaire a été modifié. Désirez-vous quitter sans sauvegarder vos données ?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion.securite.reporter.securite','fr_CA','INFRA_SOFI','La sécurité du composant courant sera reportée vers tous les composants enfants. Êtes-vous sûr de vouloir procéder à cette modification?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_aide.association.suppression','fr_CA','INFRA_SOFI','Êtes-vous sûr de vouloir supprimer l''association service - aide contextuelle?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_aide.suppression','fr_CA','INFRA_SOFI','Êtes-vous sûr de vouloir supprimer cette aide contextuelle?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_domaine_valeur.suppression_domaine','fr_CA','INFRA_SOFI','Êtes-vous sûr de vouloir supprimer le domaine de valeur \'{0}\'?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.avertissement.gestion_domaine_valeur.liste.vide','fr_CA','INFRA_SOFI','Aucune valeur pour le domaine.','A',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_libelle.suppression','fr_CA','INFRA_SOFI','Êtes-vous sûr de vouloir supprimer ce libellé?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_message.suppression','fr_CA','INFRA_SOFI','Êtes-vous sûr de vouloir supprimer ce message?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_message.supression','fr_CA','INFRA_SOFI','Êtes-vous sûr de vouloir supprimer ce paramètre système?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_referentiel.suppression','fr_CA','INFRA_SOFI','Êtes-vous sûr de vouloir supprimer ce composant?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_role.suppression_role','fr_CA','INFRA_SOFI','Êtes-vous sûr de vouloir supprimer le rôle \'{0}\' ?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_securite.dissocier_role','fr_CA','INFRA_SOFI','Êtes-vous sûr de vouloir dissocier le rôle \'{0}\' du composant?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_utilisateur.retirer_role_utilisateur','fr_CA','INFRA_SOFI','Ëtes-vous sûr de vouloir retirer le rôle \'{0}\' pour {1} {2} ?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_utilisateur_role.suppression_utilisateur_role','fr_CA','INFRA_SOFI','Êtes-vous sûr de vouloir supprimer ce rôle?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.sofi.gestion_domaine_valeur.suppression_definition','fr_CA','INFRA_SOFI','Êtes-vous sûr de vouloir supprimer la définition de domaine de valeur {0}?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.confirmation.gestion_domaine_valeur.supression','fr_CA','INFRA_SOFI','Ëtes-vous sûr de vouloir supprimer la valeur \'{0}\' ?','C',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.champ.obligatoire','en_US','INFRA_SOFI','Required field','E',NULL,NULL,NULL,'sofi','2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.champ.obligatoire','fr_CA','INFRA_SOFI','Champ obligatoire','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.critere.recherche.vide','fr_CA','INFRA_SOFI','Au moins un critère de recherche doit être spécifié pour lancer une recherche.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.element.non.trouve','fr_CA','INFRA_SOFI','L''élément désiré a été supprimé, veuillez recommencer.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.format.courriel','fr_CA','INFRA_SOFI','Le format de l''adresse de courriel est invalide.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.format.date','fr_CA','INFRA_SOFI','Le format de la date est invalide.<br/>Entrez une date respectant le format''AAAA-MM-JJ''.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.format.dateheure','fr_CA','INFRA_SOFI','La date et l''heure doivent être de format AAAA-MM-JJ HH:MM.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.format.devise','fr_CA','INFRA_SOFI','Le format doit être de type monétaire. Ex. 9,99','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.format.numerique','fr_CA','INFRA_SOFI','Le format doit être numérique. Ex : 999.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.format.telephone','fr_CA','INFRA_SOFI','Le format de téléphone est invalide.<br/>Entrez un format parmis ceux-ci: ''(999)999-9999'', ''999-999-9999'' ou ''9999999999''.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.intervalle.dateheure','fr_CA','INFRA_SOFI','La date et l''heure de fin doivent être supérieures à la date et l''heure de début.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.nombre.erreur ','en_US','INFRA_SOFI','Your form has {0} erreurs.','E',NULL,NULL,NULL,'sofi','2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.nombre.erreur','fr_CA','INFRA_SOFI','Le formulaire contient {0} errreur(s).','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.commun.suppression.impossible','fr_CA','INFRA_SOFI','Suppression impossible.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion.application.application.deja.existante','fr_CA','INFRA_SOFI','Une application existe déjà pour ce code d''application veuillez changer de code d''application.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion.application.communication','fr_CA','INFRA_SOFI','Erreur de communication avec l''application ','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion.application.date.debut.obligatoire.si.date.fin','fr_CA','INFRA_SOFI','La date de début d''activation est obligatoire si la date de fin est renseignée.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion.application.date.debut.superieur.date.fin','fr_CA','INFRA_SOFI','La date de fin d''activation ne peut être avant la date de début d''activation.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion.application.date.fin.vs.date.debut','fr_CA','INFRA_SOFI','La date de fin d''activation ne peut être avant la date de début d''activation.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion.application.mot.passe.vs.confirmation.mot.passe','fr_CA','INFRA_SOFI','Le mot de passe de confirmation doit être identique au mot de passe de l''administrateur.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion.application.supprimer.dependances','fr_CA','INFRA_SOFI','Impossible de supprimer le système, car il existe plusieurs dépendances, veuillez plutôt inactiver le système.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion.role.role.deja.existante','fr_CA','INFRA_SOFI','Un rôle existe déjà pour ce code de rôle. Veuillez changer de code de rôle.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion.role.suppression.impossible','fr_CA','INFRA_SOFI','Il est impossible de supprimer ce rôle, car il est associé à au moins un utilisateur.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestionAide.association_services','fr_CA','INFRA_SOFI','Vous ne pouvez pas supprimer une aide qui est associé à un ou plusieurs services. Veuillez retirer les associations et ensuite supprimer l''aide.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_aide.cleAide.code_systeme_inexistant','fr_CA','INFRA_SOFI','Le code du système doit être inclus dans l''identifiant de l''aide.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_aide.cleAide.coherence.vs.codeApplication','fr_CA','INFRA_SOFI','Le nom de l''identifiant doit commencer par le nom du système.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_aide.cleAide.unique','fr_CA','INFRA_SOFI','Une aide contextuelle existe déjà avec cet identifiant.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_aide.listeServices.doublon','fr_CA','INFRA_SOFI','L''aide contextuelle ne peut être associé deux fois au même service ou onglet.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_libelle.cleLibelle.code_systeme_inexistant','fr_CA','INFRA_SOFI','Le code du système doit être inclus dans l''identifiant du libellé.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_libelle.cleLibelle.coherence.vs.codeApplication','fr_CA','INFRA_SOFI','Le nom de l''identifiant doit commencer par le nom du système.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_libelle.cleLibelle.unique','fr_CA','INFRA_SOFI','Un libellé existe déjà avec cet identifiant et l''application {0}.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_libelle.communication','fr_CA','INFRA_SOFI','Erreur de communication avec l''application ','E',NULL,NULL,NULL,NULL,'2010-09-21 13:38:08',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_libelle.identifiant_introuvable','fr_CA','INFRA_SOFI','L''identifiant du libellé n''existe pas.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_message.identifiant.code_systeme_inexistant','fr_CA','INFRA_SOFI','Le code du système doit être inclus dans l''identifiant du message.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_message.identifiant.coherence.vs.codeApplication','fr_CA','INFRA_SOFI','La deuxième partie de l''identifiant doit être le code du système.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_message.identifiant.coherence.vs.codeSeverite','fr_CA','INFRA_SOFI','Le nom de l''identifiant doit commencer par le type de message.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_message.identifiant.unique','fr_CA','INFRA_SOFI','Un message existe déjà avec cet identifiant et l''application {0}.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.codeApplication.different.parent','fr_CA','INFRA_SOFI','Le code d''application doit être le même que le parent.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.dateDebutActivite.chronologie.vs.dateFinActivite','fr_CA','INFRA_SOFI','La date de début d''activation doit être inférieure à la date de fin d''activation.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.nom.coherence.vs.codeApplication','fr_CA','INFRA_SOFI','Le nom de l''identifiant doit commencer par le nom du système.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.nom.unique','fr_CA','INFRA_SOFI','Un composant applicatif existe déjà avec cet identifiant.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.nomParent.inexistant','fr_CA','INFRA_SOFI','Le composant parent doit exister.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.nomParent.obligatoire','fr_CA','INFRA_SOFI','Le type du composant applicatif spécifié requiert un composant parent.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.nomParent.onglet.invalide','fr_CA','INFRA_SOFI','Le parent doit être un onglet ou un service.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.nomParent.recursif','fr_CA','INFRA_SOFI','L''association parent-enfant est récursive.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.nomParent.section.invalide','fr_CA','INFRA_SOFI','Le parent d''une section doit être nul ou une autre section.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.nomParent.service.invalide','fr_CA','INFRA_SOFI','Le parent d''un service doit être nul, une section ou un autre service.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.specifier.niveau_onglet','fr_CA','INFRA_SOFI','Vous devez spécifier le niveau d''onglet','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.suppression.echec','fr_CA','INFRA_SOFI','Impossible de supprimer l''objet java car certaines données importantes y sont rattachées. Vous pouvez par contre le rendre inactif.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_referentiel.supprimer.enfants.avant.parent','fr_CA','INFRA_SOFI','Vous devez d''abord supprimer les enfants de l''objet référentiel.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_role.codeRoleParent.identiqueCodeRole','fr_CA','INFRA_SOFI','Le code de rôle parent est identique au code de rôle.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_role.codeRoleParent.inexistant','fr_CA','INFRA_SOFI','Le code du rôle parent est inexistant.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_role.onglet.detail.code_role_non_valide','fr_CA','INFRA_SOFI','Le code de rôle n''est pas valide.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_utilisateur.code.utilisateur','fr_CA','INFRA_SOFI','Le code utilisateur est déjà utilisé.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_utilisateur.code_client_existe','fr_CA','INFRA_SOFI','Le code de client est déjà utilisé.','E',NULL,NULL,NULL,'sofi','2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.gestion_utilisateur.suppression.impossible','fr_CA','INFRA_SOFI','Il est impossible de supprimer cet utilisateur, car il est associé à au moins un rôle.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.message.communication','fr_CA','INFRA_SOFI','Erreur de communication avec l''application ','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.parametre.communication','fr_CA','INFRA_SOFI','Erreur de communication avec l''application ','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.parametre.parametre.existant','fr_CA','INFRA_SOFI','Le code de paramètre existe déjà pour ce système','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.erreur.referentiel.libelle.existant','fr_CA','INFRA_SOFI','Il existe déjà un libellé correspondant au nom du composant spécifié.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.informatif.gestion_role.onglet.detail.ajout_nouveau_role','fr_CA','INFRA_SOFI','Ajout d''un nouveau rôle.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.commun.operation.succes','fr_CA','INFRA_SOFI','Opération effectuée avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.commun.transaction.aucune.modification','fr_CA','INFRA_SOFI','Aucune modification a été appliquée au formulaire.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.commun.transaction.succes','fr_CA','INFRA_SOFI','Opération effectuée avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion.application.aide.ligne.succes','fr_CA','INFRA_SOFI','L''aide en ligne de l''application \'{0}\' a été initialisé avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion.application.cache.succes','fr_CA','INFRA_SOFI','La cache \'{0}\' de l''application \'{1}\' a été rechargée avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion.application.libelles.succes','fr_CA','INFRA_SOFI','Tous les libellés de l''application \'{0}\' ont été initialisés avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion.application.message.succes','fr_CA','INFRA_SOFI','Tous les messages de l''application \'{0}\' ont été initialisés avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_aide.ajout.nouvelle_association','fr_CA','INFRA_SOFI','Ajout d''une nouvelle association de l''aide en ligne.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_aide.creation','fr_CA','INFRA_SOFI','Création d''un nouveau texte d''aide contextuelle.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_aide.dissociation.succes','fr_CA','INFRA_SOFI','L''association avec le service a été supprimée avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_aide.suppression.succes','fr_CA','INFRA_SOFI','Le texte d''aide \'{0}\' a été supprimé avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_application.confirmation_ajout.systeme','fr_CA','INFRA_SOFI','Ajout d''un nouveau système.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_application.confirmation_suppression','fr_CA','INFRA_SOFI','Le système \'{0}\' a été supprimé avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_journalisation.compteur.element.utilisateurs','fr_CA','INFRA_SOFI','utilisateur(s)','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_libelle.creation','fr_CA','INFRA_SOFI','Création d''un nouveau libellé.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_libelle.libelle.succes','fr_CA','INFRA_SOFI','Le libellé \'{0}\' a été initialisé avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_libelle.suppression.succes','fr_CA','INFRA_SOFI','Le libellé \'{0}\' a été supprimé avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_message.creation','fr_CA','INFRA_SOFI','Création d''un nouveau message.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_message.suppression.succes','fr_CA','INFRA_SOFI','Le message \'{0}\' a été supprimé avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_referentiel.creation','fr_CA','INFRA_SOFI','Création d''un nouveau composant applicatif.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_referentiel.onglet.detail.ajout_nouvel_objet_java','fr_CA','INFRA_SOFI','Ajout d''un nouveau composant de référentiel.','I',NULL,NULL,NULL,'sofi','2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_referentiel.suppression.succes','fr_CA','INFRA_SOFI','Le composant applicatif \'{0}\' a été supprimé avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_role.suppression_role_succes','fr_CA','INFRA_SOFI','Le rôle \'{0}\' a été supprimé avec succès.','E',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_utilisateur.aucun_role_autorise','fr_CA','INFRA_SOFI','Aucun rôle autorisé pour ce système.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.gestion_utilisateur.aucun_role_supplementaire','fr_CA','INFRA_SOFI','Aucun rôle disponible supplémentaire à offrir pour ce système.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.message.message.succes','fr_CA','INFRA_SOFI','Le message \'{0}\' a été initialisé avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.parametre.initialisation.parametre','fr_CA','INFRA_SOFI','Le paramètre système \'{0}\' a été intialisé avec succès.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.parametre.nouveau.parametre_systeme','fr_CA','INFRA_SOFI','Création d''un nouveau paramètre système','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.securite.utilisateur.nouveau.mot_passe','fr_CA','INFRA_SOFI','Veuillez saisir un nouveau mot de passe.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.surveillance.onglet.liste_erreurs.aucune_erreur','fr_CA','INFRA_SOFI','Aucune erreur présentement sur le site.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.surveillance.onglet.liste_utilisateurs.aucun_utilisateur','fr_CA','INFRA_SOFI','Aucun utilisateur présentement en ligne.','I',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.libelle.gestion.application.gestion.cache.avertissement','fr_CA','INFRA_SOFI','La gestion de la cache n''est pas disponible pour l''adresse du site spécifiée.','A',NULL,NULL,NULL,NULL,'2010-08-10 13:22:52',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.accueil.liste_journalisation_vide','fr_CA','INFRA_SOFI','Aucune journalisation récente.','I',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.accueil.liste_message_vide','fr_CA','INFRA_SOFI','Aucun message récent.','I',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.accueil.liste_libelle_vide','fr_CA','INFRA_SOFI','Aucun libellé récent.','I',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.accueil.liste_aide_vide','fr_CA','INFRA_SOFI','Aucune aide récente.','I',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.accueil.liste_parametre_vide','fr_CA','INFRA_SOFI','Aucun paramètre récent.','I',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.information.gestion_ordonnanceur.confirmation_ajout.ordonnanceur','fr_CA','INFRA_SOFI','Ajout d''un nouveau ordonnanceur.'
        ,'I',null,null,null,'sofi',null,null,null);

Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.confirmation.gestion_ordonnanceur.suppression','fr_CA','INFRA_SOFI','Êtes-vous sur de vouloir supprimer cet ordonnanceur ?'
        ,'C',null,null,null,'sofi',null,null,null);

Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE
                          ,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.information.gestion_ordonnanceur.confirmation_suppression','fr_CA','INFRA_SOFI','L''ordonnanceur \'{0}\' a été supprimé avec succès.','I'
        ,null,null,null,'sofi',null,null,null);

Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE
                          ,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.erreur.gestion.ordonnanceur.supprimer.dependances','fr_CA','INFRA_SOFI'
        ,'Impossible de supprimer l''ordonnanceur, car il existe plusieurs dépendances, veuillez plutôt inactiver le système.','E',null,null,null,'sofi',null,null,null);

Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE
                          ,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.information.gestion_declencheur.confirmation_ajout.declencheur','fr_CA','INFRA_SOFI'
        ,'Ajout d''un déclencheur','I',null,null,null,'sofi',null,null,null);

Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.confirmation.gestion_declencheur.suppression','fr_CA','INFRA_SOFI','Êtes-vous sur de vouloir supprimer ce déclencheur ?'
        ,'C',null,null,null,'sofi',null,null,null);
Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE
                          ,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.information.gestion_declencheur.confirmation_suppression','fr_CA','INFRA_SOFI','Le déclencheur \'{0}\' a été supprimé avec succès.','I'
        ,null,null,null,'sofi',null,null,null);

Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE
                          ,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('infra_sofi.erreur.gestion.declencheur.supprimer.dependances','fr_CA','INFRA_SOFI'
        ,'Impossible de supprimer le déclencheur, car il existe plusieurs dépendances, veuillez détruire les dépendances.','E',null,null,null,'sofi',null,null,null);
Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.information.gestion_tache.confirmation_ajout.tache','fr_CA','INFRA_SOFI','Ajout d''une nouvelle tâche.'
        ,'I',null,null,null,'sofi','2011-03-01 13:22:38',null,null);
INSERT INTO UTILISATEUR ( CODE_UTILISATEUR, MOT_PASSE, NOM, PRENOM, COURRIEL, CODE_STATUT, DATE_MOT_PASSE,
CODE_UTILISATEUR_APP ) VALUES ( 
'gabarit', 'H3Hg9KybR82TvyaeQBerqrnTvWM=', 'SOFI', 'Gabarit', 'info@sofiframework.org'
, 'A', SYSDATE, NULL); 

INSERT INTO APPLICATION ( CODE_APPLICATION, NOM, DESCRIPTION, DATE_DEBUT_ACTIVITE,
DATE_FIN_ACTIVITE, CODE_UTILISATEUR_ADMIN, MOT_PASSE_ADMIN, ADRESSE_SITE, CREE_PAR, CODE_STATUT,
VERSION_LIVRAISON ) VALUES ( 
'GABARIT', 'Démonstration SOFI', 'Application exemple de SOFI'
, NULL, NULL, 'sofi', 'sofi', 'http://localhost:8080/gabarit/accueil.do?methode=acceder'
, 'gabarit', 'A', '1.0'); 


INSERT INTO ROLE ( CODE_ROLE, CODE_APPLICATION, NOM, DESCRIPTION, CODE_STATUT, CODE_ROLE_PARENT,
CODE_APPLICATION_PARENT) VALUES ( 
'PILOTE', 'GABARIT', 'Pilote', NULL, 'A', NULL, NULL); 
INSERT INTO ROLE ( CODE_ROLE, CODE_APPLICATION, NOM, DESCRIPTION, CODE_STATUT, CODE_ROLE_PARENT,
CODE_APPLICATION_PARENT) VALUES ( 
'PUBLIC', 'GABARIT', 'Public', NULL, 'A', 'PILOTE', 'GABARIT'); 

INSERT INTO UTILISATEUR_ROLE (ID_UTILISATEUR_ROLE, CODE_UTILISATEUR, CODE_ROLE, CODE_APPLICATION, DATE_DEBUT_ACTIVITE,
DATE_FIN_ACTIVITE, DESCRIPTION_PERIODE_ACTIVITE ) VALUES (4,
'sofi', 'PILOTE', 'GABARIT', NULL, NULL, NULL); 
INSERT INTO UTILISATEUR_ROLE (ID_UTILISATEUR_ROLE, CODE_UTILISATEUR, CODE_ROLE, CODE_APPLICATION, DATE_DEBUT_ACTIVITE,
DATE_FIN_ACTIVITE, DESCRIPTION_PERIODE_ACTIVITE ) VALUES (5,
'gabarit', 'PUBLIC', 'GABARIT', NULL, NULL, NULL); 

INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1000, 'SE', 'A', 'gabarit.libelle.service.accueil', 'Accueil'
, 'accueil', 'acceder', NULL, 1, NULL, 'GABARIT', 'O', NULL, 'M', NULL, NULL
, NULL, NULL); 


INSERT INTO LOCALE_APPLICATION (code_locale, code_application, cle_libelle, ordre_affichage, defaut) values ('fr_CA', 'GABARIT', 'infra_sofi.libelle.locale.francais.canada' , 1 , 'O');
INSERT INTO LOCALE_APPLICATION (code_locale, code_application, cle_libelle, ordre_affichage, defaut) values ('en_US', 'GABARIT', 'infra_sofi.libelle.locale.anglais.us' , 2 , 'N');

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.accueil.texte_accueil','fr_CA','GABARIT','Bienvenue dans le gabarit de développement SOFI, <br/><br/>Vous pouvez accéder au service de surveillance dans la section de menu Utilitaire.<br/><br>L''équipe SOFI.',null,null,null,null,1); 
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.service.accueil','fr_CA','GABARIT','Accueil',null,null,null,null,1); 
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.section','fr_CA','GABARIT','Surveillance',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.service','fr_CA','GABARIT','La surveillance en direct',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.onglet.info','fr_CA','GABARIT','Information générale',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.onglet.utilisateurs_en_ligne','fr_CA','GABARIT','Utilisateurs en ligne',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.onglet.utilisateur','fr_CA','GABARIT','Détail utilisateur',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.onglet.erreurs_site','fr_CA','GABARIT','Liste des erreurs',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.onglet.detail_erreur','fr_CA','GABARIT','Détail de l''erreur',null,null,null,null,1);

INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1001, 'SC', 'A', 'gabarit.libelle.surveillance.section', 'Section surveillance de site'
, NULL, NULL, NULL, 5, NULL, 'GABARIT', 'N', NULL, 'M', NULL, NULL, NULL, NULL); 
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1002, 'SE', 'A', 'gabarit.libelle.surveillance.service', 'Le service de surveillance'
, 'surveillance', 'acceder', NULL, 2, 1001, 'GABARIT', 'N', NULL, 'M', NULL, NULL
, NULL, NULL);
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1003, 'ON', 'A', 'gabarit.libelle.surveillance.onglet.info', 'Onglet de l''info général de la surveillance'
, 'surveillance', 'afficher', NULL, 1, 1002, 'GABARIT', 'N', NULL, 'M', NULL, NULL
, 1, NULL); 
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1004, 'ON', 'A', 'gabarit.libelle.surveillance.onglet.utilisateurs_en_ligne', 'Onglet de la liste des utilisateurs en ligne'
, 'surveillance', 'afficherListeUtilisateur', NULL, 2, 1002, 'GABARIT', 'N', NULL
, 'M', NULL, NULL, 1, NULL); 
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1005, 'ON', 'A', 'gabarit.libelle.surveillance.onglet.utilisateur', 'Onglet spécifiant le détail de l''utilisateur.'
, 'surveillance', 'afficherUtilisateur', NULL, 1, 1004, 'GABARIT', 'N', NULL, 'M'
, NULL, NULL, 1, NULL); 
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1006, 'ON', 'A', 'gabarit.libelle.surveillance.onglet.erreurs_site', 'Onglet des erreurs sur le site'
, 'surveillance', 'afficherListeErreur', NULL, 3, 1002, 'GABARIT', 'N', NULL, 'M'
, NULL, NULL, 1, NULL); 
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1007, 'ON', 'A', 'gabarit.libelle.surveillance.onglet.detail_erreur', 'Onglet du détail d''un erreur'
, 'surveillance', 'afficherErreur', NULL, 1, 1006, 'GABARIT', 'N', NULL, 'M', NULL
, NULL, 1, NULL); 

INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT',1000, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT',1001, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1002, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1003, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1004, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1005, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1006, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1007, 'N');
Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML
                          ,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.confirmation.gestion_tache.suppression','fr_CA','INFRA_SOFI','Êtes-vous sur de vouloir supprimer cette tâche ?'
        ,'C',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE
                          ,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.information.gestion_tache.confirmation_suppression','fr_CA','INFRA_SOFI','La tâche ''{0}'' a été supprimée avec succès.','I'
        ,null,null,null,'sofi','2011-03-01 13:22:38',null,null);

Insert into MESSAGE_JAVA (CLE_MESSAGE,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE
                          ,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('infra_sofi.erreur.gestion.tache.supprimer.dependances','fr_CA','INFRA_SOFI'
        ,'Impossible de supprimer la tâche, car il existe plusieurs dépendances, veuillez détruire les dépendances.','E',null,null,null,'sofi','2011-03-01 13:22:38',null,null);

INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.domaine_valeur.nouvelle_valeur','fr_CA','INFRA_SOFI','Création d''une nouvelle valeur','I',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('infra_sofi.information.domaine_valeur.creation_definition', 'fr_CA', 'INFRA_SOFI', 'Création d''un nouveau domaine de valeurs.', 'I',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);



INSERT INTO ROLE ( CODE_ROLE, CODE_APPLICATION, NOM, DESCRIPTION, CODE_STATUT, CODE_ROLE_PARENT,
CODE_APPLICATION_PARENT, CREE_PAR, DATE_CREATION) VALUES ( 
'PILOTE', 'INFRA_SOFI', 'Pilote', NULL, 'A', NULL, NULL, 'sofi', now()); 
INSERT INTO ROLE ( CODE_ROLE, CODE_APPLICATION, NOM, DESCRIPTION, CODE_STATUT, CODE_ROLE_PARENT,
CODE_APPLICATION_PARENT, CREE_PAR, DATE_CREATION) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 'Développeur', NULL, 'A', 'PILOTE', 'INFRA_SOFI', 'sofi', now()); 
INSERT INTO ROLE ( CODE_ROLE, CODE_APPLICATION, NOM, DESCRIPTION, CODE_STATUT, CODE_ROLE_PARENT,
CODE_APPLICATION_PARENT, CREE_PAR, DATE_CREATION) VALUES ( 
'ANALYSTE', 'INFRA_SOFI', 'Analyste', NULL, 'A', 'PILOTE', 'INFRA_SOFI', 'sofi', now()); 
INSERT INTO ROLE ( CODE_ROLE, CODE_APPLICATION, NOM, DESCRIPTION, CODE_STATUT, CODE_ROLE_PARENT,
CODE_APPLICATION_PARENT, CREE_PAR, DATE_CREATION) VALUES ( 
'PUBLIC', 'INFRA_SOFI', 'Public', NULL, 'A', 'ANALYSTE', 'INFRA_SOFI', 'sofi', now()); 

INSERT INTO UTILISATEUR_ROLE (ID_UTILISATEUR_ROLE, CODE_UTILISATEUR, CODE_ROLE, CODE_APPLICATION, DATE_DEBUT_ACTIVITE,
DATE_FIN_ACTIVITE, DESCRIPTION_PERIODE_ACTIVITE ) VALUES (1, 
'sofi', 'DEVELOPPEUR', 'INFRA_SOFI', NULL, NULL, NULL); 
INSERT INTO UTILISATEUR_ROLE (ID_UTILISATEUR_ROLE, CODE_UTILISATEUR, CODE_ROLE, CODE_APPLICATION, DATE_DEBUT_ACTIVITE,
DATE_FIN_ACTIVITE, DESCRIPTION_PERIODE_ACTIVITE ) VALUES (2,
'sofi', 'PILOTE', 'INFRA_SOFI', NULL, NULL, NULL); 

INSERT INTO OBJET_JAVA VALUES (14,'ON','A','infra_sofi.libelle.surveillance.onglet.utilisateur','Onglet spécifiant le détail de l''utilisateur.','surveillance','afficherUtilisateur',NULL,NULL,NULL,1,62,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (23,'SC','A','infra_sofi.libelle.securite.section','Section sécurité',NULL,NULL,NULL,NULL,NULL,2,NULL,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (24,'SC','A','infra_sofi.libelle.referentiel.section','Section référentiel',NULL,NULL,NULL,NULL,NULL,1,NULL,'INFRA_SOFI','O',NULL,'M',NULL,'btnProjets',NULL,NULL,'2010-10-01 03:04:14',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (25,'SC','A','infra_sofi.libelle.contenu.section','Section gestion de contenu',NULL,NULL,NULL,'2010-07-07',NULL,3,NULL,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01','sofi','2010-07-07 21:07:34','','',1);
INSERT INTO OBJET_JAVA VALUES (26,'SC','A','infra_sofi.libelle.surveillance.section','Section surveillance de site',NULL,NULL,NULL,NULL,NULL,5,NULL,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (27,'SE','A','infra_sofi.libelle.surveillance.service','Le service de surveillance','surveillance','acceder',NULL,NULL,NULL,2,26,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (28,'SE','A','infra_sofi.libelle.gestion_referentiel.service','Service du référentiel SOFI','rechercheReferentiel','afficher',NULL,NULL,NULL,2,24,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,'1',NULL,1);
INSERT INTO OBJET_JAVA VALUES (29,'ON','A','infra_sofi.libelle.gestion_referentiel.onglet.recherche','Onglet Recherche','rechercheReferentiel','rechercher',NULL,NULL,NULL,1,28,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (30,'ON','A','infra_sofi.libelle.gestion_referentiel.onglet.structure','Onglet Structure','rechercheReferentiel','structure',NULL,'2010-06-23',NULL,1,28,'INFRA_SOFI','O',NULL,'M',NULL,NULL,2,NULL,'2010-08-10 13:23:01','sofi','2010-06-23 19:39:14','',NULL,1);
INSERT INTO OBJET_JAVA VALUES (31,'ON','A','infra_sofi.libelle.gestion_referentiel.onglet.detail','Onglet Détail','referentiel','afficher',NULL,NULL,NULL,1,30,'INFRA_SOFI','O','infra_sofi.aide.gestion_referentiel.onglet.detail','M',NULL,NULL,1,NULL,'2010-08-10 13:23:01','sofi','2010-06-23 18:35:10','1',NULL,1);
INSERT INTO OBJET_JAVA VALUES (32,'SE','A','infra_sofi.libelle.gestion_libelle.service','Service de gestion des libellés','rechercheLibelle','acceder',NULL,'2010-07-07',NULL,1,25,'INFRA_SOFI','O',NULL,NULL,NULL,NULL,NULL,NULL,'2010-08-10 13:23:01','sofi','2010-07-07 21:06:29','','',1);
INSERT INTO OBJET_JAVA VALUES (33,'ON','A','infra_sofi.libelle.gestion_libelle.onglet.recherche','Onglet Recherche','rechercheLibelle','afficher',NULL,NULL,NULL,1,32,'INFRA_SOFI','O',NULL,NULL,NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (34,'ON','A','infra_sofi.libelle.gestion_libelle.onglet.detail','Onglet Detail','libelle','afficher',NULL,NULL,NULL,1,33,'INFRA_SOFI','O','infra_sofi.aide.gestion_libelle.onglet.detail','M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (35,'SE','A','infra_sofi.libelle.gestion_journalisation.service','Service de journalisation des accès','rechercheJournalisation','acceder',NULL,NULL,NULL,1,26,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (36,'ON','A','infra_sofi.libelle.surveillance.onglet.info','Onglet de l''info général de la surveillance','surveillance','afficher',NULL,NULL,NULL,1,27,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (37,'SE','A','infra_sofi.libelle.accueil.service','Service d''accueil des outils de gestion SOFI.','accueil','afficher',NULL,NULL,NULL,1,NULL,'INFRA_SOFI','O','infra_sofi.aide.accueil.service','M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01','jfbrassard@sofiframework.org','2010-06-04 15:12:25','1',NULL,1);
INSERT INTO OBJET_JAVA VALUES (38,'SE','A','infra_sofi.libelle.gestion_message.service',NULL,'rechercheMessage','acceder',NULL,NULL,NULL,1,25,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (39,'ON','A','infra_sofi.libelle.gestion_message.onglet.recherche','Onglet recherche','rechercheMessage','afficher',NULL,NULL,NULL,1,38,'INFRA_SOFI','N',NULL,NULL,NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (40,'ON','A','infra_sofi.libelle.gestion_message.onglet.detail','Message','message','afficher',NULL,NULL,NULL,1,39,'INFRA_SOFI','O','infra_sofi.aide.gestion_message.onglet.detail','M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (41,'SE','A','infra_sofi.libelle.gestion_role_referentiel.service',NULL,'rechercheSecuriteReferentiel','acceder',NULL,NULL,NULL,4,23,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (42,'SE','A','infra_sofi.libelle.gestion_aide.service',NULL,'rechercheAide','acceder',NULL,NULL,NULL,3,25,'INFRA_SOFI','N',NULL,NULL,NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (43,'ON','A','infra_sofi.libelle.gestion_aide.onglet.recherche',NULL,'rechercheAide','afficher',NULL,NULL,NULL,1,42,'INFRA_SOFI','O',NULL,NULL,NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (44,'ON','A','infra_sofi.libelle.gestion_aide.onglet.detail',NULL,'aide','afficher',NULL,NULL,NULL,1,43,'INFRA_SOFI','O','infra_sofi.aide.gestion_message.champ.aideContextuelleExterne','M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (45,'ON','A','infra_sofi.libelle.gestion_aide.onglet.association',NULL,'afficherAideService','afficher',NULL,NULL,NULL,2,43,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,'1',NULL,1);
INSERT INTO OBJET_JAVA VALUES (46,'SE','A','infra_sofi.libelle.gestion_parametre_systeme.service','Service de gestion des paramètres système','rechercheParametre','acceder',NULL,NULL,NULL,3,24,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (47,'ON','A','infra_sofi.libelle.gestion_parametre_systeme.onglet.recherche','Onglet recherche','rechercheParametre','afficher',NULL,NULL,NULL,1,46,'INFRA_SOFI','O',NULL,NULL,NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (48,'ON','A','infra_sofi.libelle.gestion_parametre_systeme.onglet.detail','Onglet détail','parametre','afficher',NULL,NULL,NULL,1,47,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (49,'ON','A','infra_sofi.libelle.gestion_journalisation.onglet.recherche','Onglet recherche','rechercheJournalisation','afficher',NULL,NULL,NULL,1,35,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (50,'ON','A','infra_sofi.libelle.gestion_journalisation.onglet.detail','Onglet détail','journalisation','afficher',NULL,NULL,NULL,1,49,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (51,'ON','A','infra_sofi.libelle.gestion_role_referentiel.onglet.recherche','Onglet de recherche de l''unité de traitement qui gère la sécurité appliquée aux objets du référentiel Java','rechercheSecuriteReferentiel','afficher',NULL,NULL,NULL,1,41,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (52,'ON','A','infra_sofi.libelle.gestion_role_referentiel.detail','Onglet de détail qui permet la gestion de la sécurité à appliquer à un composant du référentiel Java','securiteReferentiel','afficher',NULL,NULL,NULL,2,51,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (53,'SE','A','infra_sofi.libelle.service.gestion_systeme','Unité de traitement qui permet l''ajout et la modification des applications','rechercheApplication','acceder',NULL,NULL,NULL,1,24,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (54,'ON','A','infra_sofi.libelle.gestion_application.onglet.recherche','Onglet permettant d''utiliser l''engin de recherche','rechercheApplication','afficher',NULL,NULL,NULL,1,53,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (55,'ON','A','infra_sofi.libelle.gestion_application.onglet.detail','Onglet affichant le détail d''une application','application','afficher',NULL,NULL,NULL,2,54,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (56,'ON','A','infra_sofi.libelle.gestion_application.onglet.cache','Onglet permettant la gestion de la cache d''un système.','cache','afficher',NULL,NULL,NULL,3,54,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01','sofi','2010-06-23 18:14:41','3',NULL,1);
INSERT INTO OBJET_JAVA VALUES (59,'SE','A','infra_sofi.libelle.service.gestion.role','Unité de traitement de gestion des rôles','rechercheRole','acceder',NULL,NULL,NULL,2,23,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (60,'ON','A','infra_sofi.libelle.gestion.role.onglet.recherche','Onglet de l''engin de recherche des rôles','rechercheRole','afficher',NULL,NULL,NULL,1,59,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (61,'ON','A','infra_sofi.libelle.gestion.role.onglet.detail','Onglet permettant la gestion du détail des rôles','role','afficher',NULL,NULL,NULL,2,60,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (62,'ON','A','infra_sofi.libelle.surveillance.onglet.utilisateurs_en_ligne','Onglet de la liste des utilisateurs en ligne','surveillance','afficherListeUtilisateur',NULL,NULL,NULL,2,27,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (63,'ON','A','infra_sofi.libelle.surveillance.onglet.erreurs_site','Onglet des erreurs sur le site','surveillance','afficherListeErreur',NULL,NULL,NULL,3,27,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (64,'ON','A','infra_sofi.libelle.surveillance.onglet.detail_erreur','Onglet du détail d''un erreur','surveillance','afficherErreur',NULL,NULL,NULL,1,63,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (65,'SE','A','infra_sofi.libelle.gestion_utilisateur.service','Le service de gestion des utilisateurs','rechercheUtilisateur','acceder',NULL,NULL,NULL,1,23,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (66,'ON','A','infra_sofi.libelle.gestion_utilisateur.onglet.recherche','Onglet de recherche du service utilisateur.','rechercheUtilisateur','afficher',NULL,NULL,NULL,1,65,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (67,'ON','A','infra_sofi.libelle.gestion_utilisateur.onglet.detail','Onglet du détail d''un utilisateur du service gestion des utilisateurs.','utilisateur','afficher',NULL,NULL,NULL,1,66,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (68,'SE','A','infra_sofi.libelle.gestion_domaine_valeur.service','Gérer les domaines de valeur','rechercheDomaine','acceder',NULL,NULL,NULL,4,24,'INFRA_SOFI','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,'1.8',NULL,1);
INSERT INTO OBJET_JAVA VALUES (69,'ON','A','infra_sofi.libelle.gestion_valeur_domaine.onglet.recherche','Onglet recherche','rechercheDomaine','afficher',NULL,NULL,NULL,1,68,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,'1.8',NULL,1);
INSERT INTO OBJET_JAVA VALUES (70,'ON','A','infra_sofi.libelle.gestion_valeur_domaine.onglet.detail','Onglet détail','domaine','afficher',NULL,'2010-07-01',NULL,1,69,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01','sofi','2010-07-07 19:53:19','1.8',NULL,1);
INSERT INTO OBJET_JAVA VALUES (71,'ON','A','infra_sofi.libelle.gestion_utilisateur.onglet.role','Onglet sur la liste des rôles par application disponible pour l''utilisateur.','detailUtilisateurRole','afficher',NULL,NULL,NULL,2,66,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,'1',NULL,1);
INSERT INTO OBJET_JAVA VALUES (72,'ON','A','infra_sofi.libelle.gestion_domaine_valeur.onglet.detailDomaine','','valeur','afficherValeur',NULL,'2010-07-01',NULL,1,70,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01','sofi','2010-07-07 19:52:45','1.8',NULL,1);
INSERT INTO OBJET_JAVA VALUES (73,'IT','A','infra_sofi.libelle.gestion_referentiel.champ.ordrePresentation','Champ de saisie de l''ordre de présentation.',NULL,NULL,NULL,NULL,NULL,NULL,31,'INFRA_SOFI','N','infra_sofi.aide.gestion_referentiel.champ.ordreAffichage',NULL,NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,'1',NULL,1);
INSERT INTO OBJET_JAVA VALUES (74,'IT','A','infra_sofi.libelle.gestion_systeme.champ.versionLivraison','Spécifier la version courante dont le système est accessible.',NULL,NULL,NULL,NULL,NULL,NULL,55,'INFRA_SOFI','N','infra_sofi.aide.gestion_systeme.champ.versionLivraison',NULL,NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (75,'IT','A','infra_sofi.libelle.gestion_libelle.champ.aideContextuelleExterne',NULL,NULL,NULL,NULL,NULL,NULL,NULL,34,'INFRA_SOFI','N','infra_sofi.aide.commun.champ.aideContextuelleExterne',NULL,NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (76,'IT','A','infra_sofi.libelle.gestion_aide.champ.aideContextuelleExterne',NULL,NULL,NULL,NULL,NULL,NULL,NULL,44,'INFRA_SOFI','N','infra_sofi.aide.commun.champ.aideContextuelleExterne',NULL,NULL,NULL,NULL,NULL,'2010-08-10 13:23:01',NULL,NULL,'1',NULL,1);
INSERT INTO OBJET_JAVA VALUES (77,'ON','A','infra_sofi.libelle.gestion_utilisateur.onglet.detail_role_autorise','Le détail du rôle autorisé','detailRoleAutorise','afficherPeriodeActivation',NULL,NULL,NULL,3,71,'INFRA_SOFI','O',NULL,'M',NULL,NULL,1,NULL,'2010-08-10 13:23:01',NULL,NULL,'1',NULL,1);
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES (
78, 'ON', 'A', 'infra_sofi.libelle.gestion_domaine_valeur.onglet.liste_valeur'
, '', 'listeValeur'
, 'afficher', NULL
, 2, 69, 'INFRA_SOFI', 'O', NULL, 'M', NULL, NULL, 1, '1');


Insert into OBJET_JAVA (ID_OBJET_JAVA,TYPE,CODE_STATUT,NOM,DESCRIPTION,NOM_ACTION,NOM_PARAMETRE,ADRESSE_WEB
                        ,DATE_DEBUT_ACTIVITE,DATE_FIN_ACTIVITE,ORDRE_AFFICHAGE,ID_OBJET_JAVA_EST_ENFANT
                        ,IND_SECURITE,CLE_AIDE,TYPE_MODE_AFFICHAGE,DIV_AJAX,STYLE_CSS,NIVEAU_ONGLET,CREE_PAR,DATE_CREATION
                        ,MODIFIE_PAR,DATE_MODIFICATION,VERSION_LIVRAISON,CODE_APPLICATION) 
values (83,'SC','A','infra_sofi.libelle.ordonnanceur.section','Section ordonnanceur quartz'
      ,null,null,null,null,null,5,null,'O',null,'M',null,null,null,null,null,null,null,null,'INFRA_SOFI');
      
Insert into OBJET_JAVA (ID_OBJET_JAVA,TYPE,CODE_STATUT,NOM,DESCRIPTION,NOM_ACTION,NOM_PARAMETRE,ADRESSE_WEB
                        ,DATE_DEBUT_ACTIVITE,DATE_FIN_ACTIVITE,ORDRE_AFFICHAGE,ID_OBJET_JAVA_EST_ENFANT
                        ,IND_SECURITE,CLE_AIDE,TYPE_MODE_AFFICHAGE,DIV_AJAX,STYLE_CSS,NIVEAU_ONGLET,CREE_PAR,DATE_CREATION
                        ,MODIFIE_PAR,DATE_MODIFICATION,VERSION_LIVRAISON,CODE_APPLICATION) 
values (84,'SE','A','infra_sofi.libelle.gestion_ordonnanceur.service','Service de l''ordonnanceur SOFI','rechercheOrdonnanceur','acceder'
        ,null,null,null,1,83,'O',null,'M',null,null,null,null,null,null,null,'1','INFRA_SOFI');

Insert into OBJET_JAVA (ID_OBJET_JAVA,TYPE,CODE_STATUT,NOM,DESCRIPTION,NOM_ACTION,NOM_PARAMETRE,ADRESSE_WEB
                        ,DATE_DEBUT_ACTIVITE,DATE_FIN_ACTIVITE,ORDRE_AFFICHAGE,ID_OBJET_JAVA_EST_ENFANT
                        ,IND_SECURITE,CLE_AIDE,TYPE_MODE_AFFICHAGE,DIV_AJAX,STYLE_CSS,NIVEAU_ONGLET,CREE_PAR,DATE_CREATION
                        ,MODIFIE_PAR,DATE_MODIFICATION,VERSION_LIVRAISON,CODE_APPLICATION)
values (85,'SE','A','infra_sofi.libelle.gestion_tache.service','Service de gestion des tâches des ordonnanceurs','rechercheJob','acceder'
        ,null,null,null,2,83,'O',null,'M',null,null,null,null,null,null,null,'1','INFRA_SOFI');

Insert into OBJET_JAVA (ID_OBJET_JAVA,TYPE,CODE_STATUT,NOM,DESCRIPTION,NOM_ACTION,NOM_PARAMETRE,ADRESSE_WEB
                       ,DATE_DEBUT_ACTIVITE,DATE_FIN_ACTIVITE,ORDRE_AFFICHAGE,ID_OBJET_JAVA_EST_ENFANT
                       ,IND_SECURITE,CLE_AIDE,TYPE_MODE_AFFICHAGE,DIV_AJAX,STYLE_CSS,NIVEAU_ONGLET,CREE_PAR,DATE_CREATION
                       ,MODIFIE_PAR,DATE_MODIFICATION,VERSION_LIVRAISON,CODE_APPLICATION) 
values (86,'ON','A','infra_sofi.libelle.gestion_ordonnanceur.onglet.recherche','Onglet Recherche','rechercheOrdonnanceur','afficher'
      ,null,null,null,1,84,'O',null,'M',null,null,1,null,null,'sofi',null,null,'INFRA_SOFI');    

Insert into OBJET_JAVA (ID_OBJET_JAVA,TYPE,CODE_STATUT,NOM,DESCRIPTION,NOM_ACTION,NOM_PARAMETRE,ADRESSE_WEB
                       ,DATE_DEBUT_ACTIVITE,DATE_FIN_ACTIVITE,ORDRE_AFFICHAGE,ID_OBJET_JAVA_EST_ENFANT
                       ,IND_SECURITE,CLE_AIDE,TYPE_MODE_AFFICHAGE,DIV_AJAX,STYLE_CSS,NIVEAU_ONGLET,CREE_PAR,DATE_CREATION
                       ,MODIFIE_PAR,DATE_MODIFICATION,VERSION_LIVRAISON,CODE_APPLICATION)
values (87,'ON','A','infra_sofi.libelle.gestion_tache.onglet.recherche','Onglet Recherche','rechercheJob','afficher'
      ,null,null,null,1,85,'O',null,'M',null,null,1,null,null,'sofi','2011-03-01 13:22:38',null,'INFRA_SOFI');

Insert into OBJET_JAVA (ID_OBJET_JAVA,TYPE,CODE_STATUT,NOM,DESCRIPTION,NOM_ACTION,NOM_PARAMETRE,ADRESSE_WEB
                       ,DATE_DEBUT_ACTIVITE,DATE_FIN_ACTIVITE,ORDRE_AFFICHAGE,ID_OBJET_JAVA_EST_ENFANT
                       ,IND_SECURITE,CLE_AIDE,TYPE_MODE_AFFICHAGE,DIV_AJAX,STYLE_CSS,NIVEAU_ONGLET,CREE_PAR,DATE_CREATION
                       ,MODIFIE_PAR,DATE_MODIFICATION,VERSION_LIVRAISON,CODE_APPLICATION) 
values (88,'ON','A','infra_sofi.libelle.gestion_ordonnanceur.onglet.detail','Onglet Detail','ordonnanceur','afficher'
      ,null,null,null,1,86,'O',null,'M',null,null,1,null,null,'sofi',null,null,'INFRA_SOFI');    

Insert into OBJET_JAVA (ID_OBJET_JAVA,TYPE,CODE_STATUT,NOM,DESCRIPTION,NOM_ACTION,NOM_PARAMETRE,ADRESSE_WEB
                       ,DATE_DEBUT_ACTIVITE,DATE_FIN_ACTIVITE,ORDRE_AFFICHAGE,ID_OBJET_JAVA_EST_ENFANT
                       ,IND_SECURITE,CLE_AIDE,TYPE_MODE_AFFICHAGE,DIV_AJAX,STYLE_CSS,NIVEAU_ONGLET,CREE_PAR,DATE_CREATION
                       ,MODIFIE_PAR,DATE_MODIFICATION,VERSION_LIVRAISON,CODE_APPLICATION)
values (89,'ON','A','infra_sofi.libelle.gestion_tache.onglet.detail','Onglet Detail','job','afficher'
      ,null,null,null,1,87,'O',null,'M',null,null,1,null,null,'sofi','2011-03-01 13:22:38',null,'INFRA_SOFI');

Insert into OBJET_JAVA (ID_OBJET_JAVA,TYPE,CODE_STATUT,NOM,DESCRIPTION,NOM_ACTION,NOM_PARAMETRE,ADRESSE_WEB
                       ,DATE_DEBUT_ACTIVITE,DATE_FIN_ACTIVITE,ORDRE_AFFICHAGE,ID_OBJET_JAVA_EST_ENFANT
                       ,IND_SECURITE,CLE_AIDE,TYPE_MODE_AFFICHAGE,DIV_AJAX,STYLE_CSS,NIVEAU_ONGLET,CREE_PAR,DATE_CREATION
                       ,MODIFIE_PAR,DATE_MODIFICATION,VERSION_LIVRAISON,CODE_APPLICATION)
values (90,'ON','A','infra_sofi.libelle.gestion_tache.onglet.declencheur','Onglet Déclencheur','declencheur','afficher'
      ,null,null,null,2,89,'O',null,'M',null,null,1,null,null,'sofi','2011-03-01 13:22:38',null,'INFRA_SOFI');

Insert into OBJET_JAVA (ID_OBJET_JAVA,TYPE,CODE_STATUT,NOM,DESCRIPTION,NOM_ACTION,NOM_PARAMETRE,ADRESSE_WEB
                       ,DATE_DEBUT_ACTIVITE,DATE_FIN_ACTIVITE,ORDRE_AFFICHAGE,ID_OBJET_JAVA_EST_ENFANT
                       ,IND_SECURITE,CLE_AIDE,TYPE_MODE_AFFICHAGE,DIV_AJAX,STYLE_CSS,NIVEAU_ONGLET,CREE_PAR,DATE_CREATION
                       ,MODIFIE_PAR,DATE_MODIFICATION,VERSION_LIVRAISON,CODE_APPLICATION) 
values (91,'ON','A','infra_sofi.libelle.gestion_ordonnanceur.onglet.tableau_de_bord','Onglet tableau de bord','tableauDeBord','afficher'
      ,null,null,null,1,88,'O',null,'M',null,null,1,null,null,'sofi',null,null,'INFRA_SOFI');    


Insert into OBJET_JAVA (ID_OBJET_JAVA,TYPE,CODE_STATUT,NOM,DESCRIPTION,NOM_ACTION,NOM_PARAMETRE,ADRESSE_WEB
                       ,DATE_DEBUT_ACTIVITE,DATE_FIN_ACTIVITE,ORDRE_AFFICHAGE,ID_OBJET_JAVA_EST_ENFANT
                       ,IND_SECURITE,CLE_AIDE,TYPE_MODE_AFFICHAGE,DIV_AJAX,STYLE_CSS,NIVEAU_ONGLET,CREE_PAR,DATE_CREATION
                       ,MODIFIE_PAR,DATE_MODIFICATION,VERSION_LIVRAISON,CODE_APPLICATION) 
values (92,'ON','A','infra_sofi.libelle.gestion_domaine_valeur.onglet.liste_valeur','Liste des valeurs d''un domaine','listeValeur','afficher'
      ,null,null,null,2,69,'O',null,'M',null,null,1,null,null,'sofi',sysdate,null,'INFRA_SOFI');


INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 14, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 23, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 24, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 25, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 26, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 27, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 28, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 29, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 30, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 31, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 32, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 33, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 34, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 35, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 36, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 37, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 38, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 39, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 40, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 41, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 42, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 43, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 44, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 45, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 46, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 47, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 48, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 49, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 50, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 51, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 52, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 53, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 54, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 55, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 56, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 57, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 58, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 59, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 60, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 61, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 62, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 63, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 64, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 65, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 66, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 67, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 68, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 69, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 70, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 71, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 72, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 77, 'N');
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'DEVELOPPEUR', 'INFRA_SOFI', 78, 'N'); 
Insert into ROLE_OBJET_JAVA (CODE_ROLE,CODE_APPLICATION,ID_OBJET_JAVA,IND_CONSULTATION) values ('PILOTE','INFRA_SOFI',83,'N');
Insert into ROLE_OBJET_JAVA (CODE_ROLE,CODE_APPLICATION,ID_OBJET_JAVA,IND_CONSULTATION) values ('PILOTE','INFRA_SOFI',84,'N');
Insert into ROLE_OBJET_JAVA (CODE_ROLE,CODE_APPLICATION,ID_OBJET_JAVA,IND_CONSULTATION) values ('PILOTE','INFRA_SOFI',85,'N');   
Insert into ROLE_OBJET_JAVA (CODE_ROLE,CODE_APPLICATION,ID_OBJET_JAVA,IND_CONSULTATION) values ('PILOTE','INFRA_SOFI',86,'N'); 
Insert into ROLE_OBJET_JAVA (CODE_ROLE,CODE_APPLICATION,ID_OBJET_JAVA,IND_CONSULTATION) values ('PILOTE','INFRA_SOFI',87,'N');       
Insert into ROLE_OBJET_JAVA (CODE_ROLE,CODE_APPLICATION,ID_OBJET_JAVA,IND_CONSULTATION) values ('PILOTE','INFRA_SOFI',88,'N'); 
Insert into ROLE_OBJET_JAVA (CODE_ROLE,CODE_APPLICATION,ID_OBJET_JAVA,IND_CONSULTATION) values ('PILOTE','INFRA_SOFI',89,'N');       
Insert into ROLE_OBJET_JAVA (CODE_ROLE,CODE_APPLICATION,ID_OBJET_JAVA,IND_CONSULTATION) values ('PILOTE','INFRA_SOFI',90,'N');       
Insert into ROLE_OBJET_JAVA (CODE_ROLE,CODE_APPLICATION,ID_OBJET_JAVA,IND_CONSULTATION) values ('PILOTE','INFRA_SOFI',91,'N');   
Insert into ROLE_OBJET_JAVA (CODE_ROLE,CODE_APPLICATION,ID_OBJET_JAVA,IND_CONSULTATION) values ('PILOTE','INFRA_SOFI',92,'N');       


INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'authentificationDomaineCookie', '.sofiframework.org', 'Le domaine dont le cookie d''authentification est accessible'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'authentificationNomCookie', 'certificat', 'Le nom du cookie.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'authentificationServeur', 'IP:PORT', 'Le serveur d''authentification'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'authentificationAgeCookie', '-1', 'La durée de vie du cookie de l''authentification SOFI. Pour que le cookie ait la durée de vie de la session, utiliser la valeur -1.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'authentificationUrl', '/authentification/accueil.do?methode=acceder'
, 'Url du serveur d''authentification'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'authentificationQuitterUrl', 'IP:PORT/console/j_spring_security_logout'
, 'Url pour fermer la sesssion d''un utilisateur.');
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'authentificationNomParamUrlRetour', 'url_retour'
, 'Paramètre utilisé par les applications pour être rappelé par le login après une authentification réussie.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'urlProfilUtilisateur', 'http://IP:PORT/authentification/changementMotDePasse.do'
, 'L''url de changement de mot de passe.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'codeUtilisateurApplicatif', 'false', 'Utilisation d''un code utilisateur applicatif autre que celui qui est technique.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'INFRA_SOFI', 'authentificationSecure', 'false', 'Permet d''activer ou pas le mode HTTPS lors de l''authentification.'); 
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR, 
DESCRIPTION) VALUES (
'INFRA_SOFI', 'DUREE_MOT_PASSE', '90', 'Nombre de jours pendant lesquels un mot de passe usager est valide.');
INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR, 
DESCRIPTION) VALUES (
'INFRA_SOFI', 'NB_MOT_PASSE_HISTORIQUE', '5', 'Nombre de mot de passe sauvegardé pour l''historique.');

INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_ACTIVATION_UTILISATEUR', 'DEFINITION', 'fr_CA', 'Liste des statut d''activation d''un utilisateur.'
, NULL, 'A', NULL, NULL, NULL, 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'fr_CA', 'La liste des types de journalisation disponibles pour l''application.'
, NULL, 'A', NULL, NULL, NULL, 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA', 'La liste des types de messages disponibles'
, NULL, 'A', NULL, NULL, NULL, 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'en_US', 'La liste des types de journalisation disponibles pour l''application.'
, NULL, 'A', NULL, NULL, NULL, 'en_US'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'M', 'fr_CA', 'Modification', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'M', 'en_US', 'Update', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'en_US'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'A', 'fr_CA', 'Ajout', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_ACTIVATION_UTILISATEUR', 'I', 'fr_CA', 'Inactif', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_ACTIVATION_UTILISATEUR', 'DEFINITION', 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'C', 'fr_CA', 'Consultation', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'S', 'fr_CA', 'Suppression', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_ACTIVATION_UTILISATEUR', 'A', 'fr_CA', 'Actif', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_ACTIVATION_UTILISATEUR', 'DEFINITION', 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'I', 'fr_CA', 'Informatif', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'C', 'fr_CA', 'Confirmation', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'J', 'fr_CA', 'Journalisation', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'E', 'fr_CA', 'Erreur', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA'); 
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES ( 
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'A', 'fr_CA', 'Avertisement', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA'); 

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','TYPE_TRIGGER','DEFINITION','fr_CA','Définition',1,null,null,null,null,null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','TYPE_TRIGGER','SIMPLE','fr_CA','Simple',1,null,'INFRA_SOFI','TYPE_TRIGGER','DEFINITION',null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','TYPE_TRIGGER','CRON','fr_CA','Expression cron',2,null,'INFRA_SOFI','TYPE_TRIGGER','DEFINITION',null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','MISSFIRE_CRON_TRIGGER','DEFINITION','fr_CA','Définition',1,null,null,null,null,null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','MISSFIRE_CRON_TRIGGER','1','fr_CA','Lancer une fois dès la reprise',1,null,'INFRA_SOFI','MISSFIRE_CRON_TRIGGER','DEFINITION',null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','MISSFIRE_CRON_TRIGGER','2','fr_CA','Ne rien faire à la reprise',2,null,'INFRA_SOFI','MISSFIRE_CRON_TRIGGER','DEFINITION',null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION','fr_CA','Définition',1,null,null,null,null,null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','1','fr_CA','Lancer une fois dès la reprise',1,null,'INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION',null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','2','fr_CA','Exécution immédiate avec le compteur remis à zéros',2,null,'INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION',null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','3','fr_CA','Exécution immédiate avec le compteur sauvegardé',3,null,'INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION',null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','4','fr_CA','Prochaine exécution avec le compteur sauvegardé',4,null,'INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION',null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','5','fr_CA','Prochaine exécution avec le compteur remis à zéros',5,null,'INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION',null,'sofi',null,null,null);


Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','STATUT_ORDONNANCEUR','DEFINITION','fr_CA','Définition',1,null,null,null,null,null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','STATUT_ORDONNANCEUR','A','fr_CA','Arrêté',1,null,'INFRA_SOFI','STATUT_ORDONNANCEUR','DEFINITION',null,'sofi',null,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','STATUT_ORDONNANCEUR','D','fr_CA','Démarré',2,null,'INFRA_SOFI','STATUT_ORDONNANCEUR','DEFINITION',null,'sofi',null,null,null);


/* ------------------------------------------- */
/* Debut de la section pour l'authentification */
/* ------------------------------------------- */

/* l'application */
INSERT INTO APPLICATION ( CODE_APPLICATION, NOM, DESCRIPTION, DATE_DEBUT_ACTIVITE,
DATE_FIN_ACTIVITE, CODE_UTILISATEUR_ADMIN, MOT_PASSE_ADMIN, ADRESSE_SITE, CREE_PAR, CODE_STATUT,
VERSION_LIVRAISON ) VALUES ( 
'LOGIN', 'Authentification SOFI', 'L''application d''authentification de SOFI'
, NULL, NULL, 'infra_sofi', 'infra_sofi', 'http://localhost:8080/authentification'
, 'sofi', 'A', '3.0'); 

/* les rôles */
INSERT INTO ROLE ( CODE_ROLE, CODE_APPLICATION, NOM, DESCRIPTION, CODE_STATUT, CODE_ROLE_PARENT,
CODE_APPLICATION_PARENT ) VALUES ( 
'PILOTE', 'LOGIN', 'Pilote', NULL, NULL, NULL, NULL); 

/* Association role/utilisateur */
INSERT INTO UTILISATEUR_ROLE (ID_UTILISATEUR_ROLE, CODE_UTILISATEUR, CODE_ROLE, CODE_APPLICATION, DATE_DEBUT_ACTIVITE,
DATE_FIN_ACTIVITE, DESCRIPTION_PERIODE_ACTIVITE ) VALUES (3,
'sofi', 'PILOTE', 'LOGIN', NULL, NULL, NULL);

/* les libellés */
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.accueil.explication','en_US','LOGIN','Here is the list of services offered','','','','sofi','2010-09-22 02:01:36',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.accueil.explication','fr_CA','LOGIN','Voici la liste des services offerts ','','','','sofi','2010-09-22 01:59:33',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.accueil.service','fr_CA','LOGIN','Accueil',NULL,NULL,NULL,NULL,'2010-09-08 19:25:11',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.accueil.titre','en_US','LOGIN','Home','','','','sofi','2010-09-10 17:40:23',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.accueil.titre','fr_CA','LOGIN','Accueil',NULL,NULL,NULL,'sofi','2010-09-10 17:40:38','sofi','2010-09-10 17:40:40',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.application.titre','en_US','LOGIN','SOFI Community',NULL,NULL,NULL,'sofi','2010-09-10 17:16:00','sofi','2010-09-10 17:16:01',NULL,4);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.application.titre','fr_CA','LOGIN','Communauté SOFI',NULL,NULL,NULL,'sofi','2010-09-10 17:16:18','sofi','2010-09-10 17:16:19',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.action.connecter','en_US','LOGIN','Login',NULL,NULL,NULL,'sofi','2010-09-08 19:23:05','sofi','2010-09-08 18:14:36',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.action.connecter','fr_CA','LOGIN','Ouvrir une session','','','','sofi','2010-09-08 19:23:05',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.action.inscription','en_US','LOGIN','Register',NULL,NULL,NULL,'sofi','2010-09-21 04:00:00',NULL,NULL,NULL,0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.action.inscription','fr_CA','LOGIN','Je désire m''inscrire!','','','','sofi','2010-09-08 19:23:06',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.action.modification_mot_passe','en_US','LOGIN','Change password',NULL,NULL,NULL,'sofi','2010-09-08 19:23:05','sofi','2010-09-08 18:16:48',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.action.modification_mot_passe','fr_CA','LOGIN','Je désire modifier mon mot de passe.','','','','sofi','2010-09-08 19:23:05',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.action.oublie_mot_passe','en_US','LOGIN','Forgot password',NULL,NULL,NULL,'sofi','2010-09-21 04:00:00',NULL,NULL,NULL,NULL);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.action.oublie_mot_passe','fr_CA','LOGIN','J''ai oublié mon mot de passe','','','','sofi','2010-09-08 19:23:05',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.action.ouvrir_session','en_US','LOGIN','Sign in','','','','sofi','2010-09-22 14:30:14','sofi','2010-09-22 14:30:14','',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.action.ouvrir_session','fr_CA','LOGIN','Ouvrir une session','','','','sofi','2010-09-22 14:30:14',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.champ.mot_passe','en_US','LOGIN','Password',NULL,NULL,NULL,'sofi','2010-09-22 15:03:40','sofi','2010-09-22 15:03:40',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.champ.mot_passe','fr_CA','LOGIN','Mot de passe','','','','sofi','2010-09-22 15:03:40','sofi','2010-09-10 18:57:37','',2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.champ.nom_utilisateur','en_US','LOGIN','Account SOFI',NULL,NULL,NULL,'sofi','2010-09-22 17:40:39','sofi','2010-09-22 17:40:38',NULL,3);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.champ.nom_utilisateur','fr_CA','LOGIN','Compte SOFI','','','','sofi','2010-09-22 17:40:38','sofi','2010-09-10 18:57:17','',2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.champ.rester_connecte','en_US','LOGIN','Keep me signed in for 2 weeks',NULL,NULL,NULL,'sofi','2010-09-30 03:11:07','sofi','2010-09-30 03:11:07',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.champ.rester_connecte','fr_CA','LOGIN','Rester connecté pendant 14 jours',NULL,NULL,NULL,'sofi','2010-09-30 03:10:58','sofi','2010-09-30 03:10:58',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.titre','en_US','LOGIN','Login','','','','sofi','2010-09-10 18:25:25',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.titre','fr_CA','LOGIN','Authentification','','','','sofi','2010-09-10 17:17:31',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.titre.connexion','en_US','LOGIN','Sign in to SOFI',NULL,NULL,NULL,'sofi','2010-09-22 15:16:47','sofi','2010-09-23 00:39:57',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.authentification.titre.connexion','fr_CA','LOGIN','Connexion à SOFI',NULL,NULL,NULL,'sofi','2010-09-23 00:40:05','sofi','2010-09-23 00:40:05',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.changement_mot_passe.explication','en_US','LOGIN','For your <b>SOFI account</b>, please enter a new password and confirm it.',NULL,NULL,NULL,'sofi','2010-09-22 21:05:53','sofi','2010-09-22 21:05:55',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.changement_mot_passe.explication','fr_CA','LOGIN','Pour votre <b>Compte SOFI</b>, veuillez saisir un nouveau mot de passe et le confirmer une deuxième fois.','','','','sofi','2010-09-22 02:32:42',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.changer_mot_passe.bouton.changer_mot_passe','fr_CA','LOGIN','Enregistrer','','','','sofi','2010-09-08 19:23:49',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.changer_mot_passe.champ.code_utilisateur','en_US','LOGIN','Your SOFI account','','','','sofi','2010-09-28 00:34:28',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.changer_mot_passe.champ.code_utilisateur','fr_CA','LOGIN','Mon Compte SOFI','','','','sofi','2010-09-21 20:12:53',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.changer_mot_passe.champ.confirmer_mot_passe','en_US','LOGIN','Re-type Password','','','','sofi','2010-09-28 00:39:22',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.changer_mot_passe.champ.confirmer_mot_passe','fr_CA','LOGIN','Répétez le mot de passe',NULL,NULL,NULL,'sofi','2010-09-28 00:38:44','sofi','2010-09-28 00:38:44',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.changer_mot_passe.champ.nouveau_mot_passe','en_US','LOGIN','New password','','','','sofi','2010-09-28 00:35:17',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.changer_mot_passe.champ.nouveau_mot_passe','fr_CA','LOGIN','Nouveau mot de passe','','','','sofi','2010-09-21 20:12:53',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.commun.action.accueil','en_US','LOGIN','Home','','',NULL,'sofi','2010-09-28 01:05:48',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.commun.action.accueil','fr_CA','LOGIN','Accueil','','',NULL,'sofi','2010-09-28 01:05:41',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.commun.action.quitter','en_US','LOGIN','Sign Out','','',NULL,'sofi','2010-09-28 01:10:14',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.commun.action.quitter','fr_CA','LOGIN','Fermer ma session','','',NULL,'sofi','2010-09-28 01:09:45',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.commun.liste_langue','en_US','LOGIN','Language&nbsp;',NULL,NULL,NULL,'sofi','2010-09-21 04:00:00',NULL,NULL,NULL,NULL);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.commun.liste_langue','fr_CA','LOGIN','Langue&nbsp;',NULL,NULL,NULL,'sofi','2010-09-21 04:00:00',NULL,NULL,NULL,NULL);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.commun.retour_authentification','en_US','LOGIN','Back to login','','','','sofi','2010-09-21 14:48:57',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.commun.retour_authentification','fr_CA','LOGIN','Retour à l''authentification','','','','sofi','2010-09-14 17:38:38',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.expiration_mot_passe.titre','en_US','LOGIN','Expired password','','','','sofi','2010-09-10 17:36:05',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.expiration_mot_passe.titre','fr_CA','LOGIN','Mot de passe expiré','','','','sofi','2010-09-10 17:35:57',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.fermeture_session.titre','en_US','LOGIN','Logoff','','','','sofi','2010-09-10 17:34:07',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.fermeture_session.titre','fr_CA','LOGIN','Fermeture de session','','','','sofi','2010-09-10 17:33:12',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.action.etape_suivante','en_US','LOGIN','Next step','','','','sofi','2010-09-22 14:29:02',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.action.etape_suivante','fr_CA','LOGIN','Étape suivante','','','','sofi','2010-09-22 14:28:53',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.action.soumettre','en_US','LOGIN','Submit','','','','sofi','2010-09-24 15:52:09',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.action.soumettre','fr_CA','LOGIN','Soumettre','','','','sofi','2010-09-24 15:52:03',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.bouton.inscrire','en_US','LOGIN','Confirm registration',NULL,NULL,NULL,'sofi','2010-09-08 19:23:06',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.bouton.inscrire','fr_CA','LOGIN','Soumettre votre inscription',NULL,NULL,NULL,'sofi','2010-09-08 19:23:05',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.code_client','en_US','LOGIN','Unique Client ID',NULL,NULL,NULL,'sofi','2010-09-21 14:47:20',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.code_client','fr_CA','LOGIN','Entrer votre code unique de client',NULL,NULL,NULL,NULL,'2010-09-21 14:47:20',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.code_langue','en_US','LOGIN','Language Preference',NULL,NULL,NULL,'sofi','2010-09-22 19:44:44','sofi','2010-09-22 19:44:46',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.code_langue','fr_CA','LOGIN','Langue de préférence',NULL,NULL,NULL,'sofi','2010-09-22 19:43:54','sofi','2010-09-22 19:43:56',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.code_utilisateur','en_US','LOGIN','SOFI account (your username)',NULL,NULL,NULL,'sofi','2010-09-22 20:29:41','sofi','2010-09-22 20:29:43',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.code_utilisateur','fr_CA','LOGIN','Compte SOFI (votre code utilisateur)',NULL,NULL,NULL,'sofi','2010-09-22 20:29:22','sofi','2010-09-22 20:29:23',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.confirmer_mot_passe','en_US','LOGIN','Re-type password',NULL,NULL,NULL,'sofi','2010-09-22 19:41:05','sofi','2010-09-22 19:41:07',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.confirmer_mot_passe','fr_CA','LOGIN','Répétez le mot de passe',NULL,NULL,NULL,'sofi','2010-09-22 19:40:49','sofi','2010-09-22 19:40:50',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.courriel','en_US','LOGIN','Email adress',NULL,NULL,NULL,'sofi','2010-09-21 14:47:19',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.courriel','fr_CA','LOGIN','Courriel',NULL,NULL,NULL,'sofi','2010-09-21 14:47:19',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.mot_passe','en_US','LOGIN','Password',NULL,NULL,NULL,'sofi','2010-09-21 14:47:19',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.mot_passe','fr_CA','LOGIN','Votre mot de passe',NULL,NULL,NULL,'sofi','2010-09-21 14:47:20',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.nom','en_US','LOGIN','Last name',NULL,NULL,NULL,'sofi','2010-09-21 16:43:04',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.nom','fr_CA','LOGIN','Nom',NULL,NULL,NULL,'sofi','2010-09-21 14:47:20',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.nom_client','en_US','LOGIN','Your organization''s name (if personal, please specify your name)','','','','sofi','2010-09-24 16:44:18',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.nom_client','fr_CA','LOGIN','Nom de votre organisation (si personnel, veuillez spécifiez votre nom)',NULL,NULL,NULL,'sofi','2010-09-24 16:44:15','sofi','2010-09-24 16:44:15',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.prenom','en_US','LOGIN','First name',NULL,NULL,NULL,'sofi','2010-09-21 14:47:19',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.champ.prenom','fr_CA','LOGIN','Prénom',NULL,NULL,NULL,'sofi','2010-09-21 14:47:20',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.explication','en_US','LOGIN','With a <b>SOFI account</b>, you can develop an application based on a common infrastructure available free and readily available for you. You can find an application management console to manage the common services such as security, language, message, menu, setting, etc..',NULL,NULL,NULL,'sofi','2010-09-21 14:46:10','sofi','2010-09-22 17:57:24',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.explication','fr_CA','LOGIN','Avec un <b>compte SOFI</b>, vous pouvez développer une application basée sur une infrastructure commune offerte gratuitement et rapidement disponible pour vous. Vous aurez accès une application console d''administration afin de gérer les services communs tel que la sécurité, les libellés, les messages, les menus, les paramètres, etc.',NULL,NULL,NULL,'sofi','2010-09-14 17:51:41','sofi','2010-09-22 02:18:03',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.explication_client','en_US','LOGIN','Your client code can access a secure environment for you, all the elements of the SOFI repository will be available only for you and your users.',NULL,NULL,NULL,'sofi','2010-09-26 11:37:40','sofi','2010-09-26 11:37:41',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.explication_client','fr_CA','LOGIN','Votre code client permet d''avoir accès à un environnement sécurisé pour vous, tout les éléments du référentiel SOFI sera disponible seulement pour vous et vos utilisateurs.',NULL,NULL,NULL,'sofi','2010-09-26 11:36:55','sofi','2010-09-26 11:36:55',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.section.client','fr_CA','LOGIN','Client',NULL,NULL,NULL,'sofi','2010-09-08 19:23:05',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.section.utilisateur','fr_CA','LOGIN','Utilisateur',NULL,NULL,NULL,'sofi','2010-09-08 19:23:49',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.titre','en_US','LOGIN','Subscribe to SOFI Infrastructure (Step 1 of 2)',NULL,NULL,NULL,'sofi','2010-09-24 15:30:05','sofi','2010-09-24 15:30:05',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.titre','fr_CA','LOGIN','Inscription à l''infratructure SOFI (Étape 1 de 2)',NULL,NULL,NULL,'sofi','2010-09-24 15:17:56','sofi','2010-09-24 15:17:56',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.titre.selection_client','en_US','LOGIN','Specify your organization','','','','sofi','2010-09-24 16:37:58',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.titre.selection_client','fr_CA','LOGIN','Spécifier votre organisation','','','','sofi','2010-09-24 16:37:54',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.titre.selection_compte','en_US','LOGIN','Select an account and password','','','','sofi','2010-09-22 20:28:51',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription.titre.selection_compte','fr_CA','LOGIN','Sélectionnez un compte et un mot de passe','','','','sofi','2010-09-22 20:28:38',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription_client.titre','fr_CA','LOGIN','Inscription à l''infrastructure SOFI (Étape 2 de 2)','','','','sofi','2010-09-24 15:16:57',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription_confirmation.titre','en_US','LOGIN','Confirmation of registration to SOFI Infrastructure','','',NULL,'sofi','2010-09-24 18:32:49',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.inscription_confirmation.titre','fr_CA','LOGIN','Confirmation de l''inscription à l''infrastructure SOFI',NULL,NULL,NULL,'sofi','2010-09-24 18:47:35','sofi','2010-09-24 18:47:36',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.bouton.initialiser','en_US','LOGIN','Initialize','','','','sofi','2010-09-22 17:45:07',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.bouton.initialiser','fr_CA','LOGIN','Initialiser',NULL,NULL,NULL,NULL,'2010-09-08 19:25:10',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.champ.code_utilisateur','en_US','LOGIN','SOFI account',NULL,NULL,NULL,NULL,'2010-09-22 17:41:58','sofi','2010-09-22 17:42:00',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.champ.code_utilisateur','fr_CA','LOGIN','Compte SOFI',NULL,NULL,NULL,NULL,'2010-09-22 17:41:48','sofi','2010-09-22 17:41:49',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.champ.confirmation_mot_passe','en_US','LOGIN','Confirmer mot de passe',NULL,NULL,NULL,NULL,'2010-09-21 14:47:19',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.champ.confirmation_mot_passe','fr_CA','LOGIN','Confirmer mot de passe',NULL,NULL,NULL,NULL,'2010-09-22 17:41:08','sofi','2010-09-22 17:41:10',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.champ.mode_passe','en_US','LOGIN','Password','','','','sofi','2010-09-22 17:44:55',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.champ.mode_passe','fr_CA','LOGIN','Mode de passe',NULL,NULL,NULL,NULL,'2010-09-21 14:47:20',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.champ.nouveau_mot_passe','en_US','LOGIN','New password',NULL,NULL,NULL,NULL,'2010-09-22 17:41:28','sofi','2010-09-22 17:41:29',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.champ.nouveau_mot_passe','fr_CA','LOGIN','Nouveau mot de passe',NULL,NULL,NULL,NULL,'2010-09-22 17:41:33','sofi','2010-09-22 17:41:35',NULL,2);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.mot_passe.expire','en_US','LOGIN','Your password has expired, please set a new one.',NULL,NULL,NULL,'sofi','2010-09-22 17:44:39','sofi','2010-09-22 17:44:40',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.login.mot_passe.expire','fr_CA','LOGIN','Votre mot de passe est expiré, veuillez en définir un nouveau s.v.p.',NULL,NULL,NULL,NULL,'2010-09-22 17:44:14','sofi','2010-09-22 17:44:16',NULL,3);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.logout.ici','en_US','LOGIN','here',NULL,NULL,NULL,'sofi','2010-09-08 19:23:05',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.logout.ici','fr_CA','LOGIN','içi',NULL,NULL,NULL,'sofi','2010-09-08 19:23:05',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.logout.lien_login','en_US','LOGIN','To login please click',NULL,NULL,NULL,'sofi','2010-09-08 19:23:05',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.logout.lien_login','fr_CA','LOGIN','Pour vous authentifier à nouveau appuyer sur le',NULL,NULL,NULL,'sofi','2010-09-08 19:16:06',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.modification_mot_passe.titre','en_US','LOGIN','I want modify my password','','','','sofi','2010-09-10 17:22:38',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.modification_mot_passe.titre','fr_CA','LOGIN','Je désire modifier mon mot de passe','','','','sofi','2010-09-10 17:22:24',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.onglet.changer_mot_passe','fr_CA','LOGIN','Changement de mot de passe',NULL,NULL,NULL,NULL,'2010-09-08 19:25:08',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.action.annuler','en_US','LOGIN','Cancel','','','','sofi','2010-09-26 11:41:49',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.action.annuler','fr_CA','LOGIN','Annuler','','','','sofi','2010-09-26 11:41:44',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.action.envoye','en_US','LOGIN','Send request','','','','sofi','2010-09-22 21:00:37',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.action.envoye','fr_CA','LOGIN','Envoyer la demande',NULL,NULL,NULL,'sofi','2010-09-26 11:44:17','sofi','2010-09-26 11:44:17',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.action.soumettre','en_US','LOGIN','Submit','','','','sofi','2010-09-26 11:41:31',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.action.soumettre','fr_CA','LOGIN','Soumettre','','','','sofi','2010-09-26 11:41:25',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.bouton.envoyer','en_US','LOGIN','Send',NULL,NULL,NULL,'sofi','2010-09-08 19:23:49',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.bouton.envoyer','fr_CA','LOGIN','Envoyer','','','','sofi','2010-09-08 19:23:50','sofi','2010-06-23 02:54:26','',1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.codeCaptcha','en_US','LOGIN','Enter the code shown below','','','','sofi','2010-09-22 20:59:23',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.codeCaptcha','fr_CA','LOGIN','Tapez le code affiché ci-dessous',NULL,NULL,NULL,'sofi','2010-09-22 20:58:38','sofi','2010-09-22 20:58:46',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.code_utilisateur','en_US','LOGIN','My SOFI account',NULL,NULL,NULL,'sofi','2010-09-26 11:46:52','sofi','2010-09-26 11:46:53',NULL,3);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.code_utilisateur','fr_CA','LOGIN','Mon compte SOFI',NULL,NULL,NULL,'sofi','2010-09-26 11:46:46','sofi','2010-09-26 11:46:47',NULL,3);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.contenu_courriel','en_US','LOGIN','Hi,\n\nClick on this link to enter a new password : {0}',NULL,NULL,NULL,'sofi','2010-09-21 15:06:25',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.contenu_courriel','fr_CA','LOGIN','Bonjour,\r\n\r\nAfin de ré-initialiser votre demande, il suffit de cliquez sur le lien suivant : {0}','','','','sofi','2010-09-08 19:23:49','sofi','2010-06-23 03:09:10','',1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.explication','en_US','LOGIN','Enter your <b>Account SOFI</b> and an email will be sent with the procedure to recover your password.',NULL,NULL,NULL,'sofi','2010-09-22 21:06:15','sofi','2010-09-22 21:06:17',NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.explication','fr_CA','LOGIN','Saisir votre <b>Compte SOFI</b> et un courriel vous sera envoyé avec la procédure à suivre pour récupérer votre mot de passe.','','','','sofi','2010-09-14 17:23:35',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublier_mot_passe.sujet_courriel','fr_CA','LOGIN','SOFI : Demande de ré-initialisation de votre mot de passe','','','','sofi','2010-09-08 19:23:49',NULL,NULL,'',1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublie_mot_passe.titre','en_US','LOGIN','I forgot my password','','','','sofi','2010-09-10 17:21:43',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublie_mot_passe.titre','fr_CA','LOGIN','J''ai oublié mon mot de passe','','','','sofi','2010-09-10 17:21:36',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublie_mot_passe.titre.nouveau_mot_passe','en_US','LOGIN','New password','','','','sofi','2010-09-26 11:39:24',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.oublie_mot_passe.titre.nouveau_mot_passe','fr_CA','LOGIN','Nouveau mot de passe','','','','sofi','2010-09-26 11:39:17',NULL,NULL,'',0);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.section.utilisateur','fr_CA','LOGIN','Utilisateur',NULL,NULL,NULL,NULL,'2010-09-08 19:25:08',NULL,NULL,NULL,1);
INSERT INTO LIBELLE_JAVA VALUES ('login.libelle.service.changer_mot_passe','fr_CA','LOGIN','Changement de mot de passe',NULL,NULL,NULL,NULL,'2010-09-08 19:25:08',NULL,NULL,NULL,1);


/* Les messages */
INSERT INTO MESSAGE_JAVA VALUES ('login.avertissement.commun.mot_passe_expire','en_US','LOGIN','Your password has expired, please set a new one.','A',NULL,NULL,NULL,'sofi','2010-09-24 19:19:51',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.avertissement.commun.mot_passe_expire','fr_CA','LOGIN','Votre mot de passe est expiré, veuillez en définir un nouveau s.v.p.','A',NULL,NULL,NULL,'sofi','2010-09-24 19:18:49','sofi','2010-09-24 19:18:49',NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('login.avertissement.commun.nouveau_mot_passe_administrateur','en_US','LOGIN','Your password has been set by the administrator. Please set your own password.','A',NULL,NULL,NULL,'sofi','2010-09-24 19:09:45',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.avertissement.commun.nouveau_mot_passe_administrateur','fr_CA','LOGIN','Votre mot de passe a été défini par l''administrateur. Veuillez définir votre propre mot de passe.','A',NULL,NULL,NULL,'sofi','2010-09-24 19:09:20','sofi','2010-09-24 19:09:20',NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.authentification.codeUtilisateur.invalide','en_US','LOGIN','Account SOFI(User ID) and/or password are invalid.','E',NULL,NULL,NULL,'sofi','2010-09-23 01:24:36','sofi','2010-09-23 01:24:36',NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.authentification.codeUtilisateur.invalide','fr_CA','LOGIN','Le compte SOFI(code utilisateur) et/ou le mot de passe sont invalides.','E',NULL,NULL,NULL,'sofi','2010-09-14 15:12:38','sofi','2010-09-23 01:24:14',NULL,2);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.authentification.codeUtilisateur.obligatoire','en_US','LOGIN','Account SOFI(User ID) and the password are required.','E',NULL,NULL,NULL,'sofi','2010-09-23 01:23:46','sofi','2010-09-23 01:23:46',NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.authentification.codeUtilisateur.obligatoire','fr_CA','LOGIN','Le compte SOFI(code utilisateur) et le mot de passe sont obligatoires.','E',NULL,NULL,NULL,'sofi','2010-09-23 01:23:19','sofi','2010-09-23 01:23:19',NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.authentification.motPasse.modifie_avec_succes','en_US','LOGIN','Your password is successfully changed.','I',NULL,NULL,NULL,'sofi','2010-09-22 15:28:51',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.authentification.motPasse.modifie_avec_succes','fr_CA','LOGIN','Votre mot de passe est modifié avec succès.','I',NULL,NULL,NULL,'sofi','2010-09-22 15:28:39',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.authentification.motPasse.non_identiques','en_US','LOGIN','The new password and confirmation do not match.','E',NULL,NULL,NULL,'sofi','2010-09-22 15:25:48',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.authentification.motPasse.non_identiques','fr_CA','LOGIN','Le nouveau mot de passe et la confirmation ne sont pas identiques.','E',NULL,NULL,NULL,'sofi','2010-09-22 15:25:33',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.commun.captcha_invalide','en_US','LOGIN','Captcha answer is invalid.','E',NULL,NULL,NULL,'sofi','2010-09-21 04:00:00',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.commun.captcha_invalide','fr_CA','LOGIN','La réponse captcha est invalide.','E',NULL,NULL,NULL,'sofi','2010-09-21 04:00:00',NULL,NULL,NULL,NULL);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.commun.contenu_mot_passe','en_US','LOGIN','Your password need at least one letter and one number','E',NULL,NULL,NULL,NULL,'2010-09-14 18:59:41',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.commun.contenu_mot_passe','fr_CA','LOGIN','Le mot de passe doit contenir une lettre majuscule, un lettre minuscule et un chiffre au minimum','E',NULL,NULL,NULL,NULL,'2010-09-14 18:47:49',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.commun.taille_mot_passe','en_US','LOGIN','You need to enter a longer password','E',NULL,NULL,NULL,NULL,'2010-09-14 18:59:41',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.commun.taille_mot_passe','fr_CA','LOGIN','La taille du mot de passe n''est pas correcte.','E',NULL,NULL,NULL,NULL,'2010-09-14 18:47:49',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.commun.utilisateur.expire','en_US','LOGIN','User is expired','E',NULL,NULL,NULL,NULL,'2010-09-14 18:59:41',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.commun.utilisateur.expire','fr_CA','LOGIN','Le compte de l''utilisateur demandé semble être expiré.','E',NULL,NULL,NULL,NULL,'2010-09-14 18:47:49',NULL,NULL,NULL,1);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.oublier_mot_passe_codeUtilisateur.invalide','en_US','LOGIN','The SOFI account has entered is invalid, please try again!','E',NULL,NULL,NULL,'sofi','2010-09-14 19:29:17',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.erreur.oublier_mot_passe_codeUtilisateur.invalide','fr_CA','LOGIN','Le compte SOFI inscrit n''est pas valide, veuillez essayer à nouveau!','E',NULL,NULL,NULL,'sofi','2010-09-14 19:27:51',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.information.authentification.fermeture_session','en_US','LOGIN','Your session was successfully closed.','I',NULL,NULL,NULL,'sofi','2010-09-22 17:40:15',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.information.authentification.fermeture_session','fr_CA','LOGIN','Votre session a été fermé avec succès.','I',NULL,NULL,NULL,'sofi','2010-09-22 17:40:06',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.information.oublier_mot_passe.confirmer_envoi_courriel','en_US','LOGIN','An email has been sent with the procedure to follow.','I',NULL,NULL,NULL,'sofi','2010-09-22 21:05:17',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.information.oublier_mot_passe.confirmer_envoi_courriel','fr_CA','LOGIN','Un courriel a été envoyé avec la procédure a suivre.','I',NULL,NULL,NULL,'sofi','2010-09-22 21:04:27',NULL,NULL,'',0);
INSERT INTO MESSAGE_JAVA VALUES ('login.information.oublier_mot_passe.echec_envoi_courriel','fr_CA','LOGIN','Un problème est survenue lors de la tentative d''envoi de courriel, contactez l''administrateur du système.','E',NULL,NULL,NULL,'sofi','2010-09-14 18:59:41',NULL,NULL,NULL,1);


/* Les objets JAVA */

INSERT INTO OBJET_JAVA VALUES (500,'SE','A','login.libelle.accueil.service','Service d''accueil de l''authentification SOFI.','accueil','afficher',NULL,NULL,NULL,1,NULL,'LOGIN','O','LOGIN.aide.accueil.service','M',NULL,NULL,NULL,NULL,'2010-09-08 19:15:23',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (501,'SC','A','login.libelle.section.utilisateur','Section utilisateur de l''authentification',NULL,NULL,NULL,NULL,NULL,1,NULL,'LOGIN','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-09-08 19:15:23',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (502,'SE','A','login.libelle.service.changer_mot_passe','Service de changement du mot de passe.','changerMotPasse','acceder',NULL,NULL,NULL,1,501,'LOGIN','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-09-08 19:15:23',NULL,NULL,NULL,NULL,1);
INSERT INTO OBJET_JAVA VALUES (503,'ON','A','login.libelle.onglet.changer_mot_passe','Onglet de changement du mot de passe.','changerMotPasse','afficher',NULL,NULL,NULL,1,502,'LOGIN','O',NULL,'M',NULL,NULL,NULL,NULL,'2010-09-08 19:15:23',NULL,NULL,NULL,NULL,1);

/* Sécurité sur les objets */
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PILOTE', 'LOGIN', 500, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PILOTE', 'LOGIN', 501, 'N');
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PILOTE', 'LOGIN', 502, 'N');
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PILOTE', 'LOGIN', 503, 'N');
/* Fin de la section pour l'authentification*/

/* Les paramètres */

INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'LOGIN', 'codeApplicationCommun', 'INFRA_SOFI', 'Le nom du code d''application qui est utilisé pour les paramètres communs.');

INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'LOGIN', 'smtp', 'smtp.gmail.com', 'Le serveur smtp qui doit être utilisé pour l''envoi des courriels de changement de mot de passe.');

INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'LOGIN', 'courrielAdministrateur', 'info@sofiframework.org', 'Courriel qui est utilisé comme origine des envois de courriel effectués par l''application.');

INSERT INTO PARAMETRE_SYSTEME ( CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES ( 
'LOGIN', 'authentificationInscription', 'false', 'Indiquer si le mode d''auto inscription est disponible');

/* Les tâches planifiées */

INSERT INTO QUARTZ_SCHEDULER VALUES(1,
                'ORDONNANCEUR_SOFI',
                'LOGIN',
                'Ordonnanceur SOFI',
                'Ordonnanceur de SOFI qui permet de faire certaines tâches automatiques. Par exemple, le nettoyage dans les certificats',
                'A',null,null,null,null,null);

INSERT INTO QUARTZ_JOB_DETAILS VALUES(1,1,
                'NETTOYAGE_CERTIFICAT',
                'DEFAULT',
                'Tâche qui nettoie les certificats une fois par jour (1:00 am).',
                'org.sofiframework.infrasofi.modele.securite.tache.TacheNettoyageCertificat',
                '1','0','0','1',null,null,null,null,null);
INSERT INTO QUARTZ_TRIGGERS VALUES(1,1,NULL,
                'UNE_HEURE_AM',
                'DEFAULT',
                '0','Déclencheur une fois par jour a 1:00 am',
                -1,null,5,'WAITING','CRON',0,NULL,1,NULL,NULL,NULL,NULL,NULL);
INSERT INTO QUARTZ_CRON_TRIGGERS VALUES(1,1,'0 0 1 * * ?',NULL,NULL,NULL,NULL,NULL);

/* -------------------------------------------- */
/* Section Gabarit Eclipse                      */
/* -------------------------------------------- */

INSERT INTO UTILISATEUR ( CODE_UTILISATEUR, MOT_PASSE, NOM, PRENOM, COURRIEL, CODE_STATUT, DATE_MOT_PASSE,
CODE_UTILISATEUR_APP ) VALUES ( 
'gabarit', 'H3Hg9KybR82TvyaeQBerqrnTvWM=', 'SOFI', 'Gabarit', 'info@sofiframework.org'
, 'A', SYSDATE, NULL); 

INSERT INTO APPLICATION ( CODE_APPLICATION, NOM, DESCRIPTION, DATE_DEBUT_ACTIVITE,
DATE_FIN_ACTIVITE, CODE_UTILISATEUR_ADMIN, MOT_PASSE_ADMIN, ADRESSE_SITE, CREE_PAR, CODE_STATUT,
VERSION_LIVRAISON ) VALUES ( 
'GABARIT', 'Démonstration SOFI', 'Application exemple de SOFI'
, NULL, NULL, 'sofi', 'sofi', 'http://localhost:8080/gabarit/accueil.do?methode=acceder'
, 'gabarit', 'A', '1.0'); 


INSERT INTO ROLE ( CODE_ROLE, CODE_APPLICATION, NOM, DESCRIPTION, CODE_STATUT, CODE_ROLE_PARENT,
CODE_APPLICATION_PARENT) VALUES ( 
'PILOTE', 'GABARIT', 'Pilote', NULL, 'A', NULL, NULL); 
INSERT INTO ROLE ( CODE_ROLE, CODE_APPLICATION, NOM, DESCRIPTION, CODE_STATUT, CODE_ROLE_PARENT,
CODE_APPLICATION_PARENT) VALUES ( 
'PUBLIC', 'GABARIT', 'Public', NULL, 'A', 'PILOTE', 'GABARIT'); 

INSERT INTO UTILISATEUR_ROLE (ID_UTILISATEUR_ROLE, CODE_UTILISATEUR, CODE_ROLE, CODE_APPLICATION, DATE_DEBUT_ACTIVITE,
DATE_FIN_ACTIVITE, DESCRIPTION_PERIODE_ACTIVITE ) VALUES (4,
'sofi', 'PILOTE', 'GABARIT', NULL, NULL, NULL); 
INSERT INTO UTILISATEUR_ROLE (ID_UTILISATEUR_ROLE, CODE_UTILISATEUR, CODE_ROLE, CODE_APPLICATION, DATE_DEBUT_ACTIVITE,
DATE_FIN_ACTIVITE, DESCRIPTION_PERIODE_ACTIVITE ) VALUES (5,
'gabarit', 'PUBLIC', 'GABARIT', NULL, NULL, NULL); 

INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1000, 'SE', 'A', 'gabarit.libelle.service.accueil', 'Accueil'
, 'accueil', 'acceder', NULL, 1, NULL, 'GABARIT', 'O', NULL, 'M', NULL, NULL
, NULL, NULL); 


INSERT INTO LOCALE_APPLICATION (code_locale, code_application, cle_libelle, ordre_affichage, defaut) values ('fr_CA', 'GABARIT', 'infra_sofi.libelle.locale.francais.canada' , 1 , 'O');
INSERT INTO LOCALE_APPLICATION (code_locale, code_application, cle_libelle, ordre_affichage, defaut) values ('en_US', 'GABARIT', 'infra_sofi.libelle.locale.anglais.us' , 2 , 'N');

Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.accueil.texte_accueil','fr_CA','GABARIT','Bienvenue dans le gabarit de développement SOFI, <br/><br/>Vous pouvez accéder au service de surveillance dans la section de menu Utilitaire.<br/><br>L''équipe SOFI.',null,null,null,null,1); 
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.service.accueil','fr_CA','GABARIT','Accueil',null,null,null,null,1); 
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.section','fr_CA','GABARIT','Surveillance',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.service','fr_CA','GABARIT','La surveillance en direct',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.onglet.info','fr_CA','GABARIT','Information générale',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.onglet.utilisateurs_en_ligne','fr_CA','GABARIT','Utilisateurs en ligne',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.onglet.utilisateur','fr_CA','GABARIT','Détail utilisateur',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.onglet.erreurs_site','fr_CA','GABARIT','Liste des erreurs',null,null,null,null,1);
Insert into LIBELLE_JAVA (CLE_LIBELLE,CODE_LOCALE,CODE_APPLICATION,LIBELLE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) values ('gabarit.libelle.surveillance.onglet.detail_erreur','fr_CA','GABARIT','Détail de l''erreur',null,null,null,null,1);

INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1001, 'SC', 'A', 'gabarit.libelle.surveillance.section', 'Section surveillance de site'
, NULL, NULL, NULL, 5, NULL, 'GABARIT', 'N', NULL, 'M', NULL, NULL, NULL, NULL); 
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1002, 'SE', 'A', 'gabarit.libelle.surveillance.service', 'Le service de surveillance'
, 'surveillance', 'acceder', NULL, 2, 1001, 'GABARIT', 'N', NULL, 'M', NULL, NULL
, NULL, NULL);
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1003, 'ON', 'A', 'gabarit.libelle.surveillance.onglet.info', 'Onglet de l''info général de la surveillance'
, 'surveillance', 'afficher', NULL, 1, 1002, 'GABARIT', 'N', NULL, 'M', NULL, NULL
, 1, NULL); 
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1004, 'ON', 'A', 'gabarit.libelle.surveillance.onglet.utilisateurs_en_ligne', 'Onglet de la liste des utilisateurs en ligne'
, 'surveillance', 'afficherListeUtilisateur', NULL, 2, 1002, 'GABARIT', 'N', NULL
, 'M', NULL, NULL, 1, NULL); 
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1005, 'ON', 'A', 'gabarit.libelle.surveillance.onglet.utilisateur', 'Onglet spécifiant le détail de l''utilisateur.'
, 'surveillance', 'afficherUtilisateur', NULL, 1, 1004, 'GABARIT', 'N', NULL, 'M'
, NULL, NULL, 1, NULL); 
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1006, 'ON', 'A', 'gabarit.libelle.surveillance.onglet.erreurs_site', 'Onglet des erreurs sur le site'
, 'surveillance', 'afficherListeErreur', NULL, 3, 1002, 'GABARIT', 'N', NULL, 'M'
, NULL, NULL, 1, NULL); 
INSERT INTO OBJET_JAVA ( ID_OBJET_JAVA, TYPE, CODE_STATUT, NOM, DESCRIPTION, NOM_ACTION,
NOM_PARAMETRE, ADRESSE_WEB, ORDRE_AFFICHAGE, ID_OBJET_JAVA_EST_ENFANT, CODE_APPLICATION,
IND_SECURITE, CLE_AIDE, TYPE_MODE_AFFICHAGE, DIV_AJAX, STYLE_CSS, NIVEAU_ONGLET,
VERSION_LIVRAISON ) VALUES ( 
1007, 'ON', 'A', 'gabarit.libelle.surveillance.onglet.detail_erreur', 'Onglet du détail d''un erreur'
, 'surveillance', 'afficherErreur', NULL, 1, 1006, 'GABARIT', 'N', NULL, 'M', NULL
, NULL, 1, NULL); 

INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT',1000, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT',1001, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1002, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1003, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1004, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1005, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1006, 'N'); 
INSERT INTO ROLE_OBJET_JAVA ( CODE_ROLE, CODE_APPLICATION, ID_OBJET_JAVA,
IND_CONSULTATION ) VALUES ( 
'PUBLIC', 'GABARIT', 1007, 'N');

COMMIT;
