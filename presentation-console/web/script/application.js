//Mettre les functions du projet ici
  
  // domaines de valeur

  function consulterValeur(codeApplication,nom,valeur,codeLangue) {
    var q = '&codeApplication=' + codeApplication;
    q += '&nom=' + escape(nom);
    q += '&valeur=' + escape(valeur);
    q += '&codeLangue=' + escape(codeLangue);
    
    openDialog('fenetreValeur', 600, 800, false, 'valeur.do?methode=consulter' + q, function() {
      initValeurForm();
    });
  }
  
  function creerValeurNouvelleFenetre(codeApplication, nom, codeLangue) {
    var q = '&codeApplication=' + codeApplication;
    q += '&nom=' + nom + '&codeLangue=' + codeLangue;

    openDialog('fenetreValeur', 600, 800, false, 'valeur.do?methode=creer' + q, function() {
      initValeurForm();
    });
  }

  function changerLangue(codeApplication,nom,valeur,codeLangue) {
    var q = '&codeApplication=' + codeApplication;
    q += '&nom=' + nom;
    q += '&valeur=' + valeur;
    q += '&codeLangue=' + codeLangue;

    ajaxUpdate('valeur.do?methode=consulter' + q, 'fenetreValeur', function() {
      initValeurForm();
    });
  }
  
  function initValeurForm() {
    $("#valeurForm").submit(function(){
      var formulaire = $(this);
      var donnees = formulaire.serializeArray();
      var action = formulaire[0].action;

      if (action && action.indexOf('&ajax=true') == -1) {
        action += '&ajax=true;';
      }

      var retour = $.post(action, donnees, function(resultat) {
        $('#fenetreValeur').empty();
        $('#fenetreValeur').html(resultat);
        updateParent();
        initValeurForm();
      });

      return false;
    });
  }
  
  function supprimerValeur(url, element) {
    var messageConfirmation = $('#messageConfirmationSuppression').html();
    var formulaire = $('#valeurForm');
    if (confirm(messageConfirmation)) {
      $.get(url, function() {
        updateParent();
        $('#fenetreValeur').dialog('close');
      });
    }
  }

  function creerValeur(codeApplication, nom) {
    var url = 'valeur.do?methode=creer&codeApplication=' + codeApplication + '&nom=' + nom;
    ajaxUpdate(url, 'fenetreValeur', function() {
      initValeurForm();
    });
  }
  
  function consulterProprieteDynamique(proprietaire) {
	var q = '&proprietaire=' + proprietaire;
	openDialog('DIV_PROPRIETE_DYNAMIQUE', 400, 750, false, 'proprieteDynamique.do?methode=consulter' + q, function() {
	});
  }
  
  function consulterTempsExecutions(jobId){
	var q = '&jobId=' + jobId;
	// On encode car il peut y avoir des espaces dans le nom de la job
	q = encodeURI(q);
	openDialog('DIV_TEMPS_EXECUTION', 400, 600, false, 'tableauDeBord.do?methode=obtenirTempsExecution' + q, function() {
		});
	  }
  
  function initialiserLangue() {
	    $(document).ready(function(){
	    	   $(".langueSelectionne").switchClass("langueSelectionne","langue","fast");
	    });
	  }
  
  function showParametreEdition(href) {
	    openDialog('ParametreEditionWindow',550, 850, false, false, href, function() {
	    	initParametreEditionForm();
	    });
	}

	function initParametreEditionForm() {
		if ($('#ParametreEditionWindow').text().indexOf("ajax_redirect") != -1) {
			window.location = window.location;	
		}
		
		$("#ParametreEditionWindow").submit(function(){
		  $('#ParametreEditionWindow').empty();
		  $('#ParametreEditionWindow').html('<img src=\"images/sofi/roller.gif\" />');
			
		  var form = $(this);
	      var data = form.serializeArray();
	      var action = form[0].action;

	      if (action && action.indexOf('&ajax=true') == -1) {
	        action += '&ajax=true;';
	      }

	      $.post(action, data, function(result) {
	        if (result.indexOf("<html>") == -1) {
		        $('#ParametreEditionWindow').empty();
		        $('#ParametreEditionWindow').html(result);
		        NiceTitles.autoCreation();
		        focusPremierChampEnErreur();
		        initProductSelectionForm();
	        }else {
	        	window.location = window.location;
	        }
	      });

	      return false;
	    });
	}