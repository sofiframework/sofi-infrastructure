/*
 * Librairie JavaScript utiliser par le canevas SOFI
 * Version 2.0.2
 */
 
var NS6 = (document.getElementById && !document.all) ? 1 : 0;
var NS  = (document.layers) ? 1 : 0;
var IE  = getInternetExplorerVersion() >= 0;
var IE9 = getInternetExplorerVersion() >= 9.0;
var IE7 = getInternetExplorerVersion() >= 7.0 && !IE9;
var IE6 = IE && !IE7;



// If NS -- that is, !IE -- then set up for mouse capture
if (!IE) document.captureEvents(Event.MOUSEMOVE)


function getInternetExplorerVersion() {

  // Returns the version of Internet Explorer or a -1
  // (indicating the use of another browser).
  var rv = -1; // Return value assumes failure.

  if (navigator.appName == 'Microsoft Internet Explorer')  {

    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");

    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );

  }

  return rv;
}


// Configuration de l'evenement pour fixer la position x et y de la souris.
document.onmousemove = getMouseXY;

// Variable temporaire pour conserver la position x et x de la souris.
var tempX = 0
var tempY = 0

// Fonction qui permet d'extraire la position de la souris.
function getMouseXY(e) {
 try {
    if (IE) { // si IE
      tempX = event.clientX + document.body.scrollLeft
      tempY = event.clientY + document.body.scrollTop
    } else {  // si pas IE
      tempX = e.pageX
      tempY = e.pageY
    }  
    if (tempX < 0){tempX = 0}
    if (tempY < 0){tempY = 0}  
    return true
  }catch(E) {
  }
}

/** 
 * Fonctionnalite de debug Javascript.
 * Utilisez lorsque parametre developpeur debugAjax = true.
 */
var resultatDebug = "";

function ajouterLigneDebug(contenu) {
  resultatDebug = resultatDebug + getDateHeure() + ' ' + contenu + '<br/>'; 
}

function afficherFenetreDebug() {
 var popupDebug = window.open("","popupSOFIDebug"); 
 
 try {
   popupDebug.document.open(); 
   popupDebug.document.write('<html>');
   popupDebug.document.write('<head>'); 
   popupDebug.document.write('<title>Debuggeur JavaScript Ajax SOFI</title>');
   popupDebug.document.write('</head>');   
   popupDebug.document.write('<body>');
   popupDebug.document.write(resultatDebug);
   popupDebug.document.write('</body>');
   popupDebug.document.write('</html>'); 
   popupDebug.document.close(); 
   popupDebug.focus(); 
  } catch (E) { alert ('Fonctionnalite non disponible dans Firefox, utiliser Internet Explorer pour debugger le Ajax');}
}

/*
 * Retourne l'heure courante.
 */
function heureCourante() {
  var temps = (new Date()).getTime();
  return temps;
}

/* 
 * Permet de cacher ou faire apparaitre un objet. 
 */
// Main function to retrieve mouse x-y pos.s
function getMouseXY(e) {
 try {
    if (IE) { // grab the x-y pos.s if browser is IE
      tempX = event.clientX + document.body.scrollLeft
      tempY = event.clientY + document.body.scrollTop
    } else {  // grab the x-y pos.s if browser is NS
      tempX = e.pageX
      tempY = e.pageY
    }  
    // catch possible negative values in NS4
    if (tempX < 0){tempX = 0}
    if (tempY < 0){tempY = 0}  
    return true
  }catch(E) {
  }
}
    
function display(object, show) {
 var displayValue = show ? "" : "none";
  try {
    if (object) {
      document.getElementById(object).style.display = displayValue;
    }
  }catch(E) {
  }
}

function getStyleElement(identifiant) {
  if (NS) {
    return document.layers[identifiant];
  } else if (IE) {
    return document.all[identifiant].style;
  } else if (NS6) {  
    return document.getElementById(identifiant).style;
  }
}

function showDeplacer(object) {
  if (document.getElementById) {
    document.getElementById(object).style.display = '';
    document.getElementById(object).style.visibility = 'visible';
  }
  else if (document.layers && document.layers[object]) {
    document.layers[object].display = '';
    document.layers[object].visibility = 'visible';
  }
  else if (document.all) {
    document.all[object].style.display = '';
    document.all[object].style.visibility = 'visible';
  }
}

function hideDeplacer(object) {
  if (object != null) {
    if (document.getElementById) {
      document.getElementById(object).style.display = 'none';
      document.getElementById(object).style.visibility = 'hidden';
    }
    else if (document.layers && document.layers[object]) {
      document.layers[object].display = 'none';
      document.layers[object].visibility = 'hidden';
    }
    else if (document.all) {
      document.all[object].style.display = 'none';
      document.all[object].style.visibility = 'hidden';
    }
  }
}

function estVisible(identifiant) {
  return (getStyleElement(identifiant).display != 'none');
}

function inverserAffichage(identifiant) {
  if(estVisible(identifiant)) {
    hideDeplacer(identifiant);
  }
  else {
    showDeplacer(identifiant);
  }
}

function inverserAffichage(identifiant) {
    if(estVisible(identifiant)) {
      hideDeplacer(identifiant);
    }
    else {
      showDeplacer(identifiant);
    }
  }


function visible(object, show) {
 var visibleValue = show ? "" : "hidden";
 if (NS) {
   document.layers[object].visibility = visibleValue;
 } else if (IE) {
   document.all[object].style.visibility = visibleValue;
 } else if (NS6) {
   document.getElementById(object).style.visibility = visibleValue;
 }
}

function openWin(newURL, newName, newFeatures)
{
  var newWin = open(newURL, newName, newFeatures);
  if (newWin.opener == null)
    newWin.opener = window;
  return newWin;
}

function popup(aURL, newName, aWIDTH, aHEIGHT, aFeatures) {
  if (aHEIGHT == "*")
  {
    aHEIGHT = (screen.availHeight - 80)
  }
  if (aWIDTH == "*")
  {
    aWIDTH = (screen.availWidth - 30)
  }

  var newFeatures = "height=" + aHEIGHT + ",innerHeight=" + aHEIGHT;
  newFeatures += ",width=" + aWIDTH + ",innerWidth=" + aWIDTH;
  if (window.screen)
  {

    var ah = (screen.availHeight - 30);
    var aw = (screen.availWidth - 10);
    var xc = ((aw - aWIDTH ) / 2);
    var yc = ((ah - aHEIGHT) / 2);
    newFeatures += ",left=" + xc + ",screenX=" + xc;
    newFeatures += ",top=" + yc + ",screenY=" + yc;
    newFeatures += "," + aFeatures;
  }

  var newWin = openWin(aURL, newName, newFeatures);
  newWin.focus();
  return newWin;
}

function popupMaximise(aURL, newName, newFeatures) {
  newFeatures += ",height=" + (screen.availHeight - 190) + ",innerHeight=" + (screen.availHeight - 190);
  newFeatures += ",width=" + (screen.availWidth - 10) + ",innerWidth=" + (screen.availWidth - 10); 
  newFeatures += ",left=0,top=0";
  var newWin = open(aURL, newName, newFeatures);
  newWin.focus();
  return newWin; 
}

function popupPetitDroite(aURL, newName, aWIDTH, aHEIGHT, aFeatures)
{
  if (aHEIGHT == "*")
  {
    aHEIGHT = (screen.availHeight - 10)
  }
  if (aWIDTH == "*")
  {
    aWIDTH = (screen.availWidth -10)
  }

  var newFeatures = "height=" + aHEIGHT + ",innerHeight=" + aHEIGHT;
  newFeatures += ",width=" + aWIDTH + ",innerWidth=" + aWIDTH;
  if (window.screen)
  {

    var ah = (screen.availHeight - 10);
    var aw = (screen.availWidth -10);
    var xc = (aw);
    var yc = (ah);
    newFeatures += ",left=" + xc + ",screenX=" + xc;
    newFeatures += ",top=" + yc + ",screenY=" + yc;
    newFeatures += "," + aFeatures;
  }

  var newWin = openWin(aURL, newName, newFeatures);
  newWin.focus();
  return newWin;
}

function show(object) {
  if (document.getElementById) {
    document.getElementById(object).style.visibility = 'visible';
  }
  else if (document.layers && document.layers[object]) {
    document.layers[object].visibility = 'visible';
  }
  else if (document.all) {
    document.all[object].style.visibility = 'visible';
  }
}

function hide(object) {
  if (object != null) {
    if (document.getElementById) {
      document.getElementById(object).style.visibility = 'hidden';
    }
    else if (document.layers && document.layers[object]) {
      document.layers[object].visibility = 'hidden';
    }
    else if (document.all) {
      document.all[object].style.visibility = 'hidden';
    }
  }
}

function move(object, x, y) {
    if (document.getElementById) {
      document.getElementById(object).style.top = x;
      document.getElementById(object).style.right = y;
    } 
    else if (document.layers) {
      document.layers[object].left = x - window.screenX - (window.outerWidth - window.innerWidth);
      document.layers[object].top = y - window.screenY - (window.outerHeight - window.innerHeight) + 25;;
    }
    else if (document.all) {
      document.all(object).style.posLeft = x;
      document.all(object).style.posTop = y;
    }
}

function miseEnValeurListe(tableId, colonneLien) {

  var table = document.getElementById(tableId);

  if (table != null)  {
    var tbody = table.getElementsByTagName("tbody")[0];
    var rows = tbody.getElementsByTagName("tr");

    for (i = 0; rows != null && (i < rows.length); i++) {
      rows[i].onmouseover = function() { 
        if (colonneLien != 'null') {
          this.style.cursor="pointer";
          this.style.cursor="hand";
        }
        var styleClass = this.className;
        // Si le style ne termine pas par 'over', l'ajouter.
        if (styleClass.substr(styleClass.length-4,4)!='over') {
          this.className+='over';
        } 
      };

      rows[i].onmouseout = function() { 
        var styleClass = this.className;
        // Enlever le suffixe 'over' s'il est present.
        if (styleClass.substr(styleClass.length-4,4)=='over') {
          this.className = styleClass.substr(0,styleClass.length-4);
        } 
      };
      
      // Fonction onclick
      rows[i].onclick = function(e) {
        if (e){
          //Autre
          tagName = e.target.tagName;
        } else {
          //IE
          tagName = event.srcElement.tagName;
        }
        //On gere le clique sur un element TD seulement.
        if (colonneLien && tagName=='TD') {     
          var cell = this.getElementsByTagName("td")[--colonneLien];

          if (cell != null && cell.getElementsByTagName("a").length > 0) {
            var link = cell.getElementsByTagName("a")[0];

            if (link != null && link.onclick) {
                call = link.getAttribute("onclick");
                eval(call);
            } else {
              location.href = link.getAttribute("href");
              return false; // GBO: Corrige le probleme de double-requete
            }
            this.style.cursor="wait";
          }
        }
      };      
      // Fin fonction onclick
    }
  }
}

function submitenter(myfield,e) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;
    
    if (keycode == 13)
       {
       initialiserChampsPlaceholder(true);
       myfield.form.submit();
       return false;
       }
    else
       return true;
  }
  
function limiterChampTexte(champLimite, nombreLimite) {
  if (champLimite.value.length > nombreLimite) {
    champLimite.value = champLimite.value.substring(0, nombreLimite);
  }
}

function limiterChampTexteSansBlanc(champLimite, nombreLimite) {

  var texte = removeCharacters(champLimite.value, " ");
  var diff = champLimite.value.length - texte.length;

  if (texte.length > nombreLimite) {
    champLimite.value = champLimite.value.substring(0, nombreLimite+diff);
  } 
}

function removeCharacters( strValue, strMatchPattern ) {
  /************************************************
  DESCRIPTION: Removes characters from a source string
    based upon matches of the supplied pattern.
  
  PARAMETERS:
    strValue - source string containing number.
  
  RETURNS: String modified with characters
    matching search pattern removed
  
  USAGE:  strNoSpaces = removeCharacters( ' sfdf  dfd', '\s*')
  *************************************************/
 var objRegExp =  new RegExp( strMatchPattern, 'gi' );

  //replace passed pattern matches with blanks
  return strValue.replace(objRegExp,'');
}

function limiterChampTexteAvecCompteur(champLimite, compteurLimite, nombreLimite) {
  
  if (champLimite.value.length > nombreLimite) {
    champLimite.value = champLimite.value.substring(0, nombreLimite);
  } else {
    compteurLimite.value = nombreLimite - champLimite.value.length;
  }
}


function formatterNombre(obj, nbDecimal, defautZero, maxLongueur, negatifDisponible, langue) {

  if (!nbDecimal) {
    nbDecimal = 0;
  }
  
  var nombreAFormatte = obj.value.replace(',', '.');
  
  var nombreAFormatte = removeCharacters(nombreAFormatte, " ");
 
  if (nombreAFormatte.indexOf(".") != -1) {
    var nbDecimalSaisie = nombreAFormatte.substring(nombreAFormatte.indexOf(".")).length-1;
    if (nbDecimalSaisie < nbDecimal) {
      maxLongueur = maxLongueur + 1 + nbDecimalSaisie;
    }else {
      var decimalIndex = nombreAFormatte.indexOf(".");
      if(decimalIndex != -1){
        nombreAFormatte = nombreAFormatte.substring(0, decimalIndex+nbDecimal+1);
      }
      maxLongueur = maxLongueur + 1 + nbDecimal;
    }
  }

  if (nombreAFormatte.length > maxLongueur) {
    nombreAFormatte = nombreAFormatte.substring(nombreAFormatte.length-maxLongueur);
  } 

  if (negatifDisponible != true) {
    if (nombreAFormatte < 0) {
      nombreAFormatte = 0;
    }
  }

  nombreFormatte = new NumberFormat(nombreAFormatte);
  
  if (nbDecimal != 0) {
  
    if (langue == 'en') {
      nombreFormatte.setSeparators(true, ',', '.');
    }else {
      nombreFormatte.setSeparators(true, ' ', ',');
    }
    
  nombreFormatte.setPlaces(nbDecimal);
   
  }else {
    if (langue == 'en') {
      nombreFormatte.setSeparators(true, ',', ' ');
    }else {
      nombreFormatte.setSeparators(true, ' ', ' ');
    }
    nombreFormatte.setPlaces(0);
  }
   
  var resultat = nombreFormatte.toFormatted();

  if (!defautZero && nombreAFormatte == '') {
	  obj.value = '';
  } else {
	  obj.value = resultat;
  }
}

/*
 * Retourne une valeur Integer pour une valeur en String.
 */
function getInteger(valeur) {
  var valeurSansEspace = removeCharacters(valeur, " ");
  return parseInt(valeurSansEspace);
}

/*
 * NumberFormat -The constructor
 * num - The number to be formatted.
 *  Also refer to setNumber
 * inputDecimal - (Optional) The decimal character for the input
 *  Also refer to setInputDecimal
 */
function NumberFormat(num, inputDecimal)
{
  // constants
  this.COMMA = ',';
  this.PERIOD = '.';
  this.DASH = '-'; // v1.5.0 - new - used internally
  this.LEFT_PAREN = '('; // v1.5.0 - new - used internally
  this.RIGHT_PAREN = ')'; // v1.5.0 - new - used internally
  this.LEFT_OUTSIDE = 0; // v1.5.0 - new - currency
  this.LEFT_INSIDE = 1;  // v1.5.0 - new - currency
  this.RIGHT_INSIDE = 2;  // v1.5.0 - new - currency
  this.RIGHT_OUTSIDE = 3;  // v1.5.0 - new - currency
  this.LEFT_DASH = 0; // v1.5.0 - new - negative
  this.RIGHT_DASH = 1; // v1.5.0 - new - negative
  this.PARENTHESIS = 2; // v1.5.0 - new - negative
  this.NO_ROUNDING = -1 // v1.5.1 - new

  // member variables
  this.num;
  this.numOriginal;
  this.hasSeparators = false;  // v1.5.0 - new
  this.separatorValue;  // v1.5.0 - new
  this.inputDecimalValue; // v1.5.0 - new
  this.decimalValue;  // v1.5.0 - new
  this.negativeFormat; // v1.5.0 - new
  this.negativeRed; // v1.5.0 - new
  this.hasCurrency;  // v1.5.0 - modified
  this.currencyPosition;  // v1.5.0 - new
  this.currencyValue;  // v1.5.0 - modified
  this.places;
  this.roundToPlaces; // v1.5.1 - new

  // external methods
  this.setNumber = setNumberNF;
  this.toUnformatted = toUnformattedNF;
  this.setInputDecimal = setInputDecimalNF; // v1.5.0 - new
  this.setSeparators = setSeparatorsNF; // v1.5.0 - new - for separators and decimals
  this.setCommas = setCommasNF;
  this.setNegativeFormat = setNegativeFormatNF; // v1.5.0 - new
  this.setNegativeRed = setNegativeRedNF; // v1.5.0 - new
  this.setCurrency = setCurrencyNF;
  this.setCurrencyPrefix = setCurrencyPrefixNF;
  this.setCurrencyValue = setCurrencyValueNF; // v1.5.0 - new - setCurrencyPrefix uses this
  this.setCurrencyPosition = setCurrencyPositionNF; // v1.5.0 - new - setCurrencyPrefix uses this
  this.setPlaces = setPlacesNF;
  this.toFormatted = toFormattedNF;
  this.toPercentage = toPercentageNF;
  this.getOriginal = getOriginalNF;
  this.moveDecimalRight = moveDecimalRightNF;
  this.moveDecimalLeft = moveDecimalLeftNF;

  // internal methods
  this.getRounded = getRoundedNF;
  this.preserveZeros = preserveZerosNF;
  this.justNumber = justNumberNF;
  this.expandExponential = expandExponentialNF;
  this.getZeros = getZerosNF;
  this.moveDecimalAsString = moveDecimalAsStringNF;
  this.moveDecimal = moveDecimalNF;
  this.addSeparators = addSeparatorsNF;

  // setup defaults
  if (inputDecimal == null) {
    this.setNumber(num, this.PERIOD);
  } else {
    this.setNumber(num, inputDecimal); // v.1.5.1 - new
  }
  this.setCommas(true);
  this.setNegativeFormat(this.LEFT_DASH); // v1.5.0 - new
  this.setNegativeRed(false); // v1.5.0 - new
  this.setCurrency(false); // v1.5.1 - false by default
  this.setCurrencyPrefix('$');
  this.setPlaces(2);
}

/*
 * setInputDecimal
 * val - The decimal value for the input.
 *
 * v1.5.0 - new
 */
function setInputDecimalNF(val)
{
  this.inputDecimalValue = val;
}

/*
 * setNumber - Sets the number
 * num - The number to be formatted
 * inputDecimal - (Optional) The decimal character for the input
 *  Also refer to setInputDecimal
 * 
 * If there is a non-period decimal format for the input,
 * setInputDecimal should be called before calling setNumber.
 *
 * v1.5.0 - modified
 */
function setNumberNF(num, inputDecimal)
{
  if (inputDecimal != null) {
    this.setInputDecimal(inputDecimal); // v.1.5.1 - new
  }
  
  this.numOriginal = num;
  this.num = this.justNumber(num);
}

/*
 * toUnformatted - Returns the number as just a number.
 * If the original value was '100,000', then this method will return the number 100000
 * v1.0.2 - Modified comments, because this method no longer returns the original value.
 */
function toUnformattedNF()
{
  return (this.num);
}

/*
 * getOriginal - Returns the number as it was passed in, which may include non-number characters.
 * This function is new in v1.0.2
 */
function getOriginalNF()
{
  return (this.numOriginal);
}

/*
 * setNegativeFormat - How to format a negative number.
 * 
 * format - The format. Use one of the following constants.
 * LEFT_DASH   example: -1000
 * RIGHT_DASH  example: 1000-
 * PARENTHESIS example: (1000)
 *
 * v1.5.0 - new
 */
function setNegativeFormatNF(format)
{
  this.negativeFormat = format;
}

/*
 * setNegativeRed - Format the number red if it's negative.
 * 
 * isRed - true, to format the number red if negative, black if positive;
 *  false, for it to always be black font.
 *
 * v1.5.0 - new
 */
function setNegativeRedNF(isRed)
{
  this.negativeRed = isRed;
}

/*
 * setSeparators - One purpose of this method is to set a
 *  switch that indicates if there should be separators between groups of numbers.
 *  Also, can use it to set the values for the separator and decimal.
 *  For example, in the value 1,000.00
 *   The comma (,) is the separator and the period (.) is the decimal.
 *
 * Both separator or decimal are not required.
 * The separator and decimal cannot be the same value. If they are, decimal with be changed.
 * Can use the following constants (via the instantiated object) for separator or decimal:
 *  COMMA
 *  PERIOD
 * 
 * isC - true, if there should be separators; false, if there should be no separators
 * separator - the value of the separator.
 * decimal - the value of the decimal.
 *
 * v1.5.0 - new
 */
function setSeparatorsNF(isC, separator, decimal)
{
  this.hasSeparators = isC;
  
  // Make sure a separator was passed in
  if (separator == null) separator = this.COMMA;
  
  // Make sure a decimal was passed in
  if (decimal == null) decimal = this.PERIOD;
  
  // Additionally, make sure the values aren't the same.
  //  When the separator and decimal both are periods, make the decimal a comma.
  //  When the separator and decimal both are any other value, make the decimal a period.
  if (separator == decimal) {
    this.decimalValue = (decimal == this.PERIOD) ? this.COMMA : this.PERIOD;
  } else {
    this.decimalValue = decimal;
  }
  
  // Since the decimal value changes if decimal and separator are the same,
  // the separator value can keep its setting.
  this.separatorValue = separator;
}

/*
 * setCommas - Sets a switch that indicates if there should be commas.
 * The separator value is set to a comma and the decimal value is set to a period.
 * isC - true, if the number should be formatted with separators (commas); false, if no separators
 *
 * v1.5.0 - modified
 */
function setCommasNF(isC)
{
  this.setSeparators(isC, this.COMMA, this.PERIOD);
}

/*
 * setCurrency - Sets a switch that indicates if should be displayed as currency
 * isC - true, if should be currency; false, if not currency
 */
function setCurrencyNF(isC)
{
  this.hasCurrency = isC;
}

/*
 * setCurrencyPrefix - Sets the symbol for currency.
 * val - The symbol
 */
function setCurrencyValueNF(val)
{
  this.currencyValue = val;
}

/*
 * setCurrencyPrefix - Sets the symbol for currency.
 * The symbol will show up on the left of the numbers and outside a negative sign.
 * cp - The symbol
 *
 * v1.5.0 - modified - This now calls setCurrencyValue and setCurrencyPosition(this.LEFT_OUTSIDE)
 */
function setCurrencyPrefixNF(cp)
{
  this.setCurrencyValue(cp);
  this.setCurrencyPosition(this.LEFT_OUTSIDE);
}

/*
 * setCurrencyPosition - Sets the position for currency,
 *  which includes position relative to the numbers and negative sign.
 * cp - The position. Use one of the following constants.
 *  This method does not automatically put the negative sign at the left or right.
 *  They are left by default, and would need to be set right with setNegativeFormat.
 *  LEFT_OUTSIDE  example: $-1.00
 *  LEFT_INSIDE   example: -$1.00
 *  RIGHT_INSIDE  example: 1.00$-
 *  RIGHT_OUTSIDE example: 1.00-$
 *
 * v1.5.0 - new
 */
function setCurrencyPositionNF(cp)
{
  this.currencyPosition = cp
}

/*
 * setPlaces - Sets the precision of decimal places
 * p - The number of places.
 *  -1 or the constant NO_ROUNDING turns off rounding to a set number of places.
 *  Any other number of places less than or equal to zero is considered zero.
 *
 * v1.5.1 - modified
 */
function setPlacesNF(p)
{
  this.roundToPlaces = !(p == this.NO_ROUNDING); // v1.5.1
  this.places = (p < 0) ? 0 : p; // v1.5.1 - Don't leave negatives.
}

/*
 * v1.5.2 - new
 *
 * addSeparators
 * The value to be formatted shouldn't have any formatting already.
 *
 * nStr - A number or number as a string
 * inD - Input decimal (string value). Example: '.'
 * outD - Output decimal (string value). Example: '.'
 * sep - Output separator (string value). Example: ','
 */
function addSeparatorsNF(nStr, inD, outD, sep)
{
  nStr += '';
  var dpos = nStr.indexOf(inD);
  var nStrEnd = '';
  if (dpos != -1) {
    nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
    nStr = nStr.substring(0, dpos);
  }
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(nStr)) {
    nStr = nStr.replace(rgx, '$1' + sep + '$2');
  }
  return nStr + nStrEnd;
}

/*
 * toFormatted - Returns the number formatted according to the settings (a string)
 *
 * v1.5.0 - modified
 * v1.5.1 - modified
 */
function toFormattedNF()
{ 
  var pos;
  var nNum = this.num; // v1.0.1 - number as a number
  var nStr;            // v1.0.1 - number as a string
  var splitString = new Array(2);   // v1.5.0
  
  // round decimal places - modified v1.5.1
  // Note: Take away negative temporarily with Math.abs
  if (this.roundToPlaces) {
    nNum = this.getRounded(nNum);
    nStr = this.preserveZeros(Math.abs(nNum)); // this step makes nNum into a string. v1.0.1 Math.abs
  } else {
    nStr = this.expandExponential(Math.abs(nNum)); // expandExponential is called in preserveZeros, so call it here too
  }
  
  // v1.5.3 - lost the if in 1.5.2, so putting it back
  if (this.hasSeparators) {
    // v1.5.2
    // Note that the argument being passed in for inD is this.PERIOD
    //  That's because the toFormatted method is working with an unformatted number
    nStr = this.addSeparators(nStr, this.PERIOD, this.decimalValue, this.separatorValue);
  }
  
  // negative and currency
  // $[c0] -[n0] $[c1] -[n1] #.#[nStr] -[n2] $[c2] -[n3] $[c3]
  var c0 = '';
  var n0 = '';
  var c1 = '';
  var n1 = '';
  var n2 = '';
  var c2 = '';
  var n3 = '';
  var c3 = '';
  var negSignL = (this.negativeFormat == this.PARENTHESIS) ? this.LEFT_PAREN : this.DASH;
  var negSignR = (this.negativeFormat == this.PARENTHESIS) ? this.RIGHT_PAREN : this.DASH;
    
  if (this.currencyPosition == this.LEFT_OUTSIDE) {
    // add currency sign in front, outside of any negative. example: $-1.00 
    if (nNum < 0) {
      if (this.negativeFormat == this.LEFT_DASH || this.negativeFormat == this.PARENTHESIS) n1 = negSignL;
      if (this.negativeFormat == this.RIGHT_DASH || this.negativeFormat == this.PARENTHESIS) n2 = negSignR;
    }
    if (this.hasCurrency) c0 = this.currencyValue;
  } else if (this.currencyPosition == this.LEFT_INSIDE) {
    // add currency sign in front, inside of any negative. example: -$1.00
    if (nNum < 0) {
      if (this.negativeFormat == this.LEFT_DASH || this.negativeFormat == this.PARENTHESIS) n0 = negSignL;
      if (this.negativeFormat == this.RIGHT_DASH || this.negativeFormat == this.PARENTHESIS) n3 = negSignR;
    }
    if (this.hasCurrency) c1 = this.currencyValue;
  }
  else if (this.currencyPosition == this.RIGHT_INSIDE) {
    // add currency sign at the end, inside of any negative. example: 1.00$-
    if (nNum < 0) {
      if (this.negativeFormat == this.LEFT_DASH || this.negativeFormat == this.PARENTHESIS) n0 = negSignL;
      if (this.negativeFormat == this.RIGHT_DASH || this.negativeFormat == this.PARENTHESIS) n3 = negSignR;
    }
    if (this.hasCurrency) c2 = this.currencyValue;
  }
  else if (this.currencyPosition == this.RIGHT_OUTSIDE) {
    // add currency sign at the end, outside of any negative. example: 1.00-$
    if (nNum < 0) {
      if (this.negativeFormat == this.LEFT_DASH || this.negativeFormat == this.PARENTHESIS) n1 = negSignL;
      if (this.negativeFormat == this.RIGHT_DASH || this.negativeFormat == this.PARENTHESIS) n2 = negSignR;
    }
    if (this.hasCurrency) c3 = this.currencyValue;
  }

  nStr = c0 + n0 + c1 + n1 + nStr + n2 + c2 + n3 + c3;
  
  // negative red
  if (this.negativeRed && nNum < 0) {
    nStr = '<font color="red">' + nStr + '</font>';
  }

  return (nStr);
}

/*
 * toPercentage - Format the current number as a percentage.
 * This is separate from most of the regular formatting settings.
 * The exception is the number of decimal places.
 * If a number is 0.123 it will be formatted as 12.3%
 *
 * !! This is an initial version, so it doesn't use many settings.
 * !! should use some of the formatting settings that toFormatted uses.
 * !! probably won't want to use settings like currency.
 *
 * v1.5.0 - new
 */
function toPercentageNF()
{
  nNum = this.num * 100;
  
  // round decimal places
  nNum = this.getRounded(nNum);
  
  return nNum + '%';
}

/*
 * Return concatenated zeros as a string. Used to pad a number.
 * It might be extra if already have many decimal places
 * but is needed if the number doesn't have enough decimals. 
 */
function getZerosNF(places)
{
    var extraZ = '';
    var i;
    for (i=0; i<places; i++) {
      extraZ += '0';
    }
    return extraZ;
}

/*
 * Takes a number that JavaScript expresses in notational format
 * and makes it the full number (as a string).
 * e.g. Makes -1e-21 into -0.000000000000000000001
 *
 * If the value passed in is not a number (as determined by isNaN),
 * this function just returns the original value.
 *
 * Exponential number formats can include 1e21 1e+21 1e-21
 *  where 1e21 and 1e+21 are the same thing.
 *
 * If an exponential number is evaluated by JavaScript,
 * it will change 12.34e-9 to 1.234e-8,
 * which is a benefit to this method, because
 * it prevents extra zeros that occur for certain numbers
 * when using moveDecimalAsString
 *
 * Returns a string.
 *
 * v1.5.1 - new
 */
function expandExponentialNF(origVal)
{
  if (isNaN(origVal)) return origVal;

  var newVal = parseFloat(origVal) + ''; // parseFloat to let JavaScript evaluate number
  var eLoc = newVal.toLowerCase().indexOf('e');

  if (eLoc != -1) {
    var plusLoc = newVal.toLowerCase().indexOf('+');
    var negLoc = newVal.toLowerCase().indexOf('-', eLoc); // search for - after the e
    var justNumber = newVal.substring(0, eLoc);
    
    if (negLoc != -1) {
      // shift decimal to the left
      var places = newVal.substring(negLoc + 1, newVal.length);
      justNumber = this.moveDecimalAsString(justNumber, true, parseInt(places));
    } else {
      // shift decimal to the right
      // Check if there's a plus sign, and if not refer to where the e is.
      // This is to account for either formatting 1e21 or 1e+21
      if (plusLoc == -1) plusLoc = eLoc;
      var places = newVal.substring(plusLoc + 1, newVal.length);
      justNumber = this.moveDecimalAsString(justNumber, false, parseInt(places));
    }
    
    newVal = justNumber;
  }

  return newVal;
} 

/*
 * Move decimal right.
 * Returns a number.
 *
 * v1.5.1 - new
 */
function moveDecimalRightNF(val, places)
{
  var newVal = '';
  
  if (places == null) {
    newVal = this.moveDecimal(val, false);
  } else {
    newVal = this.moveDecimal(val, false, places);
  }
  
  return newVal;
}

/*
 * Move decimal left.
 * Returns a number.
 *
 * v1.5.1 - new
 */
function moveDecimalLeftNF(val, places)
{
  var newVal = '';
  
  if (places == null) {
    newVal = this.moveDecimal(val, true);
  } else {
    newVal = this.moveDecimal(val, true, places);
  }
  
  return newVal;
}

/*
 * moveDecimalAsString
 * This is used by moveDecimal, and does not run parseFloat on the return value.
 * 
 * Normally a decimal place is moved by multiplying by powers of 10
 * Multiplication and division in JavaScript can result in floating point limitations.
 * So use this method to move a decimal place left or right.
 *
 * Parameters:
 * val - The value to be shifted. Can be a number or a string,
 *  but don't include special characters. It should evaluate to a number.
 * left - If true, then move decimal left. If false, move right.
 * places - (optional) If not included, then use the objects this.places
 *  The purpose is so this method can be used independent of the state of the object.
 *
 * The regular expressions:
 * re1
 * Pad with zeros in case there aren't enough numbers to cover the spaces shift.
 * A left shift pads to the left, and a right shift pads to the right.
 * Can't just concatenate. There might be a negative sign or the value could be an exponential.
 *
 * re2
 * Switch the decimal.
 * Need the first [0-9]+ to force the search to start rightmost.
 * The \.? and [0-9]{} criteria are the pieces that will be switched
 *
 * Other notes:
 * This method works on exponential numbers, e.g. 1.7e-12
 * because the regular expressions only modify the number and decimal parts.
 *
 * Mozilla can't handle [0-9]{0} in the regular expression.
 *  Fix: Since nothing changes when the decimal is shifted zero places, return the original value.
 *
 * IE is incorrect if exponential ends in .
 *  e.g. -8500000000000000000000. should be -8.5e+21
 *  IE counts it as -8.5e+22
 *  Fix: Replace trailing period, if there is one, using replace(/\.$/, '').
 *
 * Netscape 4.74 cannot handle a leading - in the string being searched for the re2 expressions.
 *  e.g. /([0-9]*)(\.?)([0-9]{2})/ should match everything in -100.00 except the -
 *  but it matches nothing using Netscape 4.74.
 *  It might be a combination of the * ? special characters.
 *  Fix: (-?) was added to each of the re2 expressions to look for - one or zero times.
 *
 * Returns a string.
 *
 * v1.5.1 - new
 * v1.5.2 - modified
 */
function moveDecimalAsStringNF(val, left, places)
{
  var spaces = (arguments.length < 3) ? this.places : places;
  if (spaces <= 0) return val; // to avoid Mozilla limitation
      
  var newVal = val + '';
  var extraZ = this.getZeros(spaces);
  var re1 = new RegExp('([0-9.]+)');
  if (left) {
    newVal = newVal.replace(re1, extraZ + '$1');
    var re2 = new RegExp('(-?)([0-9]*)([0-9]{' + spaces + '})(\\.?)');    
    newVal = newVal.replace(re2, '$1$2.$3');
  } else {
    var reArray = re1.exec(newVal); // v1.5.2
    if (reArray != null) {
      newVal = newVal.substring(0,reArray.index) + reArray[1] + extraZ + newVal.substring(reArray.index + reArray[0].length); // v1.5.2
    }
    var re2 = new RegExp('(-?)([0-9]*)(\\.?)([0-9]{' + spaces + '})');
    newVal = newVal.replace(re2, '$1$2$4.');
  }
  newVal = newVal.replace(/\.$/, ''); // to avoid IE flaw
  
  return newVal;
}

/*
 * moveDecimal
 * Refer to notes in moveDecimalAsString
 * parseFloat is called here to clear away the padded zeros.
 *
 * Returns a number.
 *
 * v1.5.1 - new
 */
function moveDecimalNF(val, left, places)
{
  var newVal = '';
  
  if (places == null) {
    newVal = this.moveDecimalAsString(val, left);
  } else {
    newVal = this.moveDecimalAsString(val, left, places);
  }
  
  return parseFloat(newVal);
}

/*
 * getRounded - Used internally to round a value
 * val - The number to be rounded
 * 
 *  To round to a certain decimal precision,
 *  all that should need to be done is
 *  multiply by a power of 10, round, then divide by the same power of 10.
 *  However, occasional numbers don't get exact results in most browsers.
 *  e.g. 0.295 multiplied by 10 yields 2.9499999999999997 instead of 2.95
 *  Instead of adjusting the incorrect multiplication,
 *  this function uses string manipulation to shift the decimal.
 *
 * Returns a number.
 *
 * v1.5.1 - modified
 */
function getRoundedNF(val)
{
  val = this.moveDecimalRight(val);
  val = Math.round(val);
  val = this.moveDecimalLeft(val);
  
  return val;
}

/*
 * preserveZeros - Used internally to make the number a string
 *  that preserves zeros at the end of the number
 * val - The number
 */
function preserveZerosNF(val)
{
  var i;

  // make a string - to preserve the zeros at the end
  val = this.expandExponential(val);
  
  if (this.places <= 0) return val; // leave now. no zeros are necessary - v1.0.1 less than or equal
  
  var decimalPos = val.indexOf('.');
  if (decimalPos == -1) {
    val += '.';
    for (i=0; i<this.places; i++) {
      val += '0';
    }
  } else {
    var actualDecimals = (val.length - 1) - decimalPos;
    var difference = this.places - actualDecimals;
    for (i=0; i<difference; i++) {
      val += '0';
    }
  }
  
  return val;
}

/*
 * justNumber - Used internally to parse the value into a floating point number.
 * Replace all characters that are not 0-9, a decimal point, or a negative sign.
 *
 *  A number can be entered using special notation.
 *  For example, the following is a valid number: 0.0314E+2
 *
 * v1.0.2 - new
 * v1.5.0 - modified
 * v1.5.1 - modified
 * v1.5.2 - modified
 */
function justNumberNF(val)
{
  newVal = val + '';
  
  var isPercentage = false;
  
  // check for percentage
  // v1.5.0
  if (newVal.indexOf('%') != -1) {
    newVal = newVal.replace(/\%/g, '');
    isPercentage = true; // mark a flag
  }
    
  // Replace everything but digits - + ( ) e E
  var re = new RegExp('[^\\' + this.inputDecimalValue + '\\d\\-\\+\\(\\)eE]', 'g'); // v1.5.2 
  newVal = newVal.replace(re, '');
  // Replace the first decimal with a period and the rest with blank
  // The regular expression will only break if a special character
  //  is used as the inputDecimalValue
  //  e.g. \ but not .
  var tempRe = new RegExp('[' + this.inputDecimalValue + ']', 'g');
  var treArray = tempRe.exec(newVal); // v1.5.2
  if (treArray != null) {
    var tempRight = newVal.substring(treArray.index + treArray[0].length); // v1.5.2
    newVal = newVal.substring(0,treArray.index) + this.PERIOD + tempRight.replace(tempRe, ''); // v1.5.2
  }
  
  // If negative, get it in -n format
  if (newVal.charAt(newVal.length - 1) == this.DASH ) {
    newVal = newVal.substring(0, newVal.length - 1);
    newVal = '-' + newVal;
  }
  else if (newVal.charAt(0) == this.LEFT_PAREN
   && newVal.charAt(newVal.length - 1) == this.RIGHT_PAREN) {
    newVal = newVal.substring(1, newVal.length - 1);
    newVal = '-' + newVal;
  }
  
  newVal = parseFloat(newVal);
  
  if (!isFinite(newVal)) {
    newVal = 0;
  }
  
  // now that it's a number, adjust for percentage, if applicable.
  // example. if the number was formatted 24%, then move decimal left to get 0.24
  // v1.5.0 - updated v1.5.1
  if (isPercentage) {
    newVal = this.moveDecimalLeft(newVal, 2);
  }
    
  return newVal;
}

function setBoutonInactif(inactif, id, styleClassActif, styleClassInactif) {

  if (styleClassActif == null) {
  
    styleClassActif = "bouton";
  }
  
  if (styleClassInactif == null) {
    styleClassInactif = "boutondisabled";
  }
  if (document.getElementById(id)) {
  
    if (inactif && inactif == true) {
      document.getElementById(id).className = styleClassInactif;
    }else {
      document.getElementById(id).className = styleClassActif;
    }
    document.getElementById(id).disabled = inactif;
  }
}

function inactiverBouton(theform, avecSubmit) {

  initialiserChampsPlaceholder(true);
  
  if (theform && (document.all || document.getElementById)) {
    for (i = 0; i < theform.length; i++) {
      var tempobj = theform.elements[i];
      if (tempobj.type) {
        if (tempobj.type.toLowerCase() == "button" || tempobj.type.toLowerCase() == "reset" || tempobj.type.toLowerCase() == "submit") {
          tempobj.disabled = true;
          try {
            var classCourante = tempobj.className;
            if (classCourante.indexOf("disabled") == -1) {
              if (classCourante.indexOf("over") != -1 ) {
                var indexOver = classCourante.indexOf("over");
                classCourante = classCourante.substr(0,indexOver);
              }
              var classModifie = classCourante+='disabled';
              tempobj.className=classModifie;
            }
          }catch(E) {
            tempobj.className='boutondisabled';
          }
        }
      }
    }
  }
  
  if (avecSubmit && theform && !IE9) {
	  theform.submit();
  }
}

function ecrireFuseauHoraire() {

  // On obtient la date du jour
  var maintenant = new Date();

  /* On obtient le premier janvier. On assume que le timezon client 
   * n'est pas en heure d'ete au premier janvier.
   */
  var premierJanvier = new Date(maintenant.getFullYear(), 0, 1, 0, 0, 0, 0);

  var offset = maintenant.getTimezoneOffset()*(-1)/60;
  var offSetPremierJanvier = premierJanvier.getTimezoneOffset()*(-1)/60;
  
  /* On corrige le decalage pour obtenir le timezone ae l'heure normale
   */
  if (offset != offSetPremierJanvier) {
    offset = offset - 1;
  }
  
  // On affiche en titre du champ le timezone du navigateur
  document.writeln('GMT' + offset + ':00');
}

function focus(id) {
  try {
    document.getElementById(id).focus();
    setTimeout(document.getElementById(id).focus(),100);
  }catch(E) {
  }  
}

function select(id) {
  try {
    document.getElementById(id).select();
    setTimeout(document.getElementById(id).select(),100);
  }catch(E) {
  }  
}

replacements = {
171 : 34, // Â® to "
187 : 34, // Â¯ to "
8217 : 39, // ` to '
8216 : 39, // ` to '
96 : 39, // ` to '
8221 : 34 // " to "
};

function traiterWord(id) {  
 try {
  var chars = document.getElementById(id).value.split('');
  for (var i = 0, n = chars.length; i < n; i++) {
    var code = replacements[chars[i].charCodeAt(0)];
    if (code) {
      chars[i] = String.fromCharCode(code);
    }
  }
  document.getElementById(id).value = chars.join('');
  }catch(E) { 
  }
}

function afficherMessageAttente(messageAttente, idBouton, paramPositionY, paramPositionX) {

  var body=document.getElementsByTagName('body')[0];
  var div=document.createElement('div');
  
  var bouton= document.getElementById(idBouton);
  positionY = findPosY(bouton);
  positionX = findPosX(bouton);

  if (document.body.clientHeight - positionY < 100) {
    positionY = positionY - paramPositionY;
  }else {
    positionY = positionY + paramPositionY;
  }
  positionX = positionX + paramPositionX;

  div.style.position = "absolute";

  div.style.top = positionY + "px"; //Defaut "-200px";
  div.style.left = positionX + "px";  //Defaut "-600px";
  div.className = 'tableMessageAttente';
  div.innerHTML = '<table class="texteMessageAttente">' + '<tr><td align="center">' + messageAttente + '</td></tr></table>';
  body.appendChild(div);
    
}

function findPosX(obj) {
  var curleft = 0;
  if (obj.offsetParent) {
    while (obj.offsetParent) {
      curleft += obj.offsetLeft
      obj = obj.offsetParent;
    }
  }
  else if (obj.x) {
    curleft += obj.x;
  }
  return curleft;
}

function findPosY(obj) {
  var curtop = 0;
  if (obj.offsetParent) {
    while (obj.offsetParent) {
      curtop += obj.offsetTop
      obj = obj.offsetParent;
    }
  }
  else if (obj.y) {
    curtop += obj.y;
  }
  return curtop;
}

var messageModificationFormulaire;

function setMessageModificationFormulaire(message) {
  messageModificationFormulaire = message;
}

function go(url) {
  try {
    if (messageModificationFormulaire) {
      if (document.forms[0].indModification.value == '1') {
        if (confirm(messageModificationFormulaire)) {
            document.forms[0].indModification.value = '0';
            var urlComplet = url + '&indModification=0';
            window.location = urlComplet;
        }
      }else {
        window.location = url;
      }
    }else {
      window.location = url;
    }
  }catch(E) {
    window.location = url;
  }
}

function confirmerModificationFormulaire(e) {
  try {
    if (messageModificationFormulaire) {
      var node = getElementEvenement(e);
      var modifie = $(node).parents('form').children("input[name='indModification']").attr('value');
      if (modifie == '1') {
        if (confirm(messageModificationFormulaire)) {
            return true;
        }
        return false;
      }else {
        return true;
      }
    }else {
      return true;
    }
  }catch(E) {
    return false;
  }
}

function getElementEvenement(e) {
  var evenement = (window.event) ? window.event : e;
  var node = (evenement.target) ? evenement.target : ((evenement.srcElement) ? evenement.srcElement : null);
  return node;
}

function focusPremierChampFormulaire() {

  var trouve = false;

  if (window.document.location.toString().indexOf("#") == -1) { 
    // Pour toute les formulaires
    for (f=0; f < document.forms.length; f++) {
     
      // Pour tous les elements dans le formulaire
      for(i=0; i < document.forms[f].length; i++)  {
        // Si l'element n'est pas un champ hiddent
        if (document.forms[f][i].type && document.forms[f][i].type != "hidden" 
          && document.forms[f][i].type != "button" && document.forms[f][i].type != "fieldset" && document.forms[f][i].type != "submit") {
    
          // Si l'element n'est pas disabled
          if (document.forms[f][i].disabled != true && document.forms[f][i].className.indexOf('lectureSeulement') == -1) {
        	  var iePlaceholder = $(document.forms[f][i]).attr('placeholder') && IE;

        	  // Fixer le focus, si pas de placeholder avec IE, car non supporté.
        	  if (!iePlaceholder) {
        		  document.forms[f][i].focus();
        		  
                  if (document.forms[f][i].className.indexOf('saisieFocus') == -1) {
                	  document.forms[f][i].className += 'saisieFocus';
                  }
        	  }
        	  
              trouve = true;
          }
        }
        // Si l'element a ete trouve, arretez de chercher
        if (trouve == true)
          break;
      }
      // Si trouve dans le formulaire, arretez de chercher
      if (trouve == true)
        break;
    }
  }
}

function fireOnClickEvent(id) {
  var fireOnThis = document.getElementById(id);
  try {
    fireOnThis.fireEvent('onclick');
  }catch(E) {
    try {
      fireOnThis.onclick();
    }catch(E) {
      // Si pas de id.
    }
  }
}

/*
 * Permet de forcer le lancement d'un evenement onchange sur un id.
 * @since SOFI 2.0.2
 */
function fireOnChangeEvent(id) {
  var fireOnThis = document.getElementById(id);
  try {
    fireOnThis.fireEvent('onchange');
  }catch(E) {
    try {
      fireOnThis.onchange();
    }catch(E) {
      // Si pas de id.
    }
  }
}

function remplacerDivHtml(idSource, idDestination) {
  try {
    document.getElementById(idDestination).innerHTML = document.getElementById(idSource).innerHTML;
  }catch(E) {
  }  
}

function setStyleClass(id, styleClass) {
  if (document.getElementById(id)) {
    document.getElementById(id).className = styleClass;
  }
}

function getDateHeure() {
  var dateJour= new Date()
  var annee =dateJour.getFullYear();
  var mois = dateJour.getMonth()+1;
  var jour = dateJour.getDate();
  var heure = dateJour.getHours();
  var minute = dateJour.getMinutes();
  var seconde = dateJour.getSeconds();

  if (parseInt(mois) < 10) {
    mois = '0' + mois;
  }
  
  if (parseInt(jour) < 10) {
    jour = '0' + jour;
  }
  
  if (parseInt(heure) < 10) {
    heure = '0' + heure;
  }  
  
  if (parseInt(minute) < 10) {
    minute = '0' + minute;
  }    

  if (parseInt(seconde) < 10) {
    seconde = '0' + seconde;
  }    
  
  return annee + '-' + mois + '-' + jour + ' ' + heure + ':' + minute + ':' + seconde;
}


/*
 * Utilisez par la balise <sofi-html:date /> ou <sofi-nested:date /> pour afficher
 * le calendrier de selection de date.
 */
function showCalendar(property, id, format, heure) {
  Calendar.setup({
    singleClick : true,
    inputField : property, // id of the input field
    ifFormat : format, // format of the input field
    showsTime : heure,
    button : id
  });
  fireOnClickEvent(id);
}

/*
 * Obtenir une concatenation des positions des scrolls en X et Y.
  * Format : 'x,y'
 */
function getScrolling() {
    return getScrollingX() + "," + getScrollingY();
}

/*
 * Obtenir la position du scroll en X.
 */
function getScrollingX() {
  var x = 0;
  if (self.pageXOffset) {
    x = self.pageXOffset;
  } else if (document.documentElement && document.documentElement.scrollLeft) {
    x = document.documentElement.scrollLeft;
  } else if (document.body) {
    x = document.body.scrollLeft;
  }
  return x;
}

/*
 * Obtenir la position du scroll en Y.
 */
function getScrollingY() {
  var y = 0;
  if (self.pageYOffset) {
    y = self.pageYOffset;
  } else if (document.documentElement && document.documentElement.scrollTop) {
        y = document.documentElement.scrollTop;
    } else if (document.body) {
        y = document.body.scrollTop;
    }
    return y;
}

/*
 * Obtenir le calcul d'une position en X centree en fonction de la largeur 
 * de l'element a afficher
 */
function getPositionX(largeur) {
  var position;
  
    if (IE) {
    position = (document.documentElement.clientWidth - largeur) / 2;
    } else {
      position = (window.innerWidth - largeur)/2;
    }
    
    position += getScrollingX();
    position = (position < 1) ? 30 : position;
    
  return position;
}

/*
 * Obtenir le calcul d'une position en Y centree en fonction de la hauteur 
 * de l'element a afficher
 */
function getPositionY(hauteur) {
  var position;
  
  if (IE) {
    position = (document.documentElement.clientHeight - hauteur) / 2;    
  } else {
    position = (window.innerHeight - hauteur)/2;
  }

  position += getScrollingY();
  position = (position < 1) ? 30 : position;
  
  return position;
}

/*
 * Obtenir l'element dans la page par son identifiant.
 */
function getId(id) {
  return document.getElementById(id);
}

/*
 * Indique si la liste deroulante est vide.
 */
function isListeDeroulanteVide(id) {
  return getId(id).options.length == 0;
}

/*
 * Appliquer la fonction si Enter seulement
 */
function isEnter() {
  var keycode;
  if (window.event) keycode = window.event.keyCode;
  if (keycode && keycode == 13) {
    return true;
  }else {
    return false;
  }
}

/*
 * Obtenir le contenu javascript d'une fonction.
 */
function getContenuFonction(fonction) {
  var contenu = fonction + '';
  var contenu = contenu.substring(contenu.indexOf('{') + 1, contenu.lastIndexOf('}'));
  return contenu;
}

/*
 * Afficher un popup d'alerte si la valeur d'un champ depasse 
 * une certaine longueur.
 */
function alerteSiTexteTropLong(champTexte, message, longueurMaximale) {
  if (longueurMaximale != null && champTexte.value.length > longueurMaximale) {
    alert(message);
    return true;
  }
  return false;
}

function focusLienListeNavigation() {
  setTimeout("focusPremierLien()", 400);
}

function focusPremierLien() {
  var premierLien = document.getElementById('lien_liste_00');
  if (premierLien != null) {
    premierLien.focus();
  } else {
    setTimeout("focusPremierLien()", 100);
  }
}

/**
 * Focus du premier champ de liste de valeur.
 */
function focusPremierChampListeValeur(listeChamp) {
  var tableau = listeChamp.split(',');
  var done = false;

  for (var i = 0; i < tableau.length && !done; i++) {
    var nomChamp = tableau[i];
    var champ = document.getElementById(nomChamp);

    if (champ != null && champ.type != 'hidden') {
      champ.focus();
      done = true;
    }
  }
}

/**
 * Ajoute les tirets pour une date de format annee-mois-jour 
 * dans le cas ou la date est saisie sans les tirets.
*/
function formatterDate(champDate) {
  var dateAFormatter = champDate.value;
  var dateFormattee = '';
  if (dateAFormatter != '' 
   && dateAFormatter.length == 8 
   && dateAFormatter.indexOf('-') == -1) {
    dateFormattee = 
        dateAFormatter.substring(0, 4) + '-' 
      + dateAFormatter.substring(4, 6) + '-' 
      + dateAFormatter.substring(6, 8);
    champDate.value = dateFormattee;
  }
}

/**
 * Formatter un champ en numero de telephone si celui 
 * ci est saisi seulement en nombre.
 */
 function formatterNumeroTelephone(champTel) {
  var noAFormatter = champTel.value;
  var noFormatte = '';
  if (noAFormatter != '' 
   && noAFormatter.length == 10 
   && noAFormatter.indexOf(' ') == -1 
   && noAFormatter.indexOf('-') == -1) {
    noFormatte = noAFormatter.substring(0, 3);
    noFormatte += ' ';
    noFormatte += noAFormatter.substring(3, 6);
    noFormatte += '-';
    noFormatte += noAFormatter.substring(6, 10);
    champTel.value = noFormatte;
  }
}

/**
 * Popule une valeur d'un champ d'un formulaire.
 */
function setChampFormAjax(nomForm, nomChamp, valeur) {
  var champ = window.document.forms[nomForm].elements[nomChamp];
  var ancienneValeur = champ.value;
  champ.value = (valeur && valeur != null) ? valeur : ''; 
  appliquerStyleChampSaisie(champ);
  return (ancienneValeur != valeur);
}

/**
 * Popule les valeurs d'un formulaire en fonction d'une liste de noms et de valeurs.
 * execute la notification passee sous la forme d'une chaine de javascript.
 */
function setValeursLovAjax(nomForm, listeNomChamp, listeValeur, jsNotifier) {
  var modifier = false;
  for (var i = 0; i < listeNomChamp.length; i++) {
    modifier = setChampFormAjax(nomForm, listeNomChamp[i], listeValeur[i]) || modifier;
  }
  
  if (modifier) {
    try {
      eval(jsNotifier);
    } catch (ex) {
    }
  }
  
  initialiserChampsPlaceholder(false);
  
  return modifier;
}

function notifierModificationFormulaire(nomFormulaire) {
  // Il est necessaire de tester l'existence de l'objet formulaire sans quoi une erreur javascript est declenchee
  // et les traitements qui suivent dans l'evenement onchange ne sont pas executes.
  var formulaire = document.forms[nomFormulaire];
  var nomChampIndModification = new String("indModification");

  if (nomFormulaire.indexOf("_") > -1){
    var elem = nomFormulaire.split("_",2);
    formulaire = document.forms[elem[0]];
    nomChampIndModification = new String(elem[1] + "." + nomChampIndModification);
  }

  if(formulaire != null) {
    formulaire.indModification.value = '1';
    var nouveau = formulaire.indNouveau.value == '1';
    var enErreur = formulaire.indErreur.value == '1';

    if (!nouveau && !enErreur) {
      $('.zoneMessageFormulaireSimple').filter(
          function (index) {
            return $(this).attr("id").indexOf("modificationFormulaire") == 0;
          }
      ).css('display', '');

      $('.zoneMessageFormulaireSimple').filter(
          function (index) {
            return $(this).attr("id").indexOf("messageFormulaire") == 0;
          }
      ).css('display', 'none');

      // Dans le cas oÃ¹ plusieurs message d'information avait ete affiche, cacher cette zone aussi
      $('.zoneMessageFormulaireMultiple').filter(
          function (index) {
            return $(this).attr("id").indexOf("messageFormulaire") == 0;
          }
      ).css('display', 'none');
      
      $('.zoneMessageFormulaireVide').css('display', 'none');
    }
  }
  
  activerLesBoutons();
  inactiverLesBoutons();
}

function focusErreur(idChamp) {
  idChamp = idChamp.split('[').join('\\[');
  idChamp = idChamp.split(']').join('\\]');
  idChamp = idChamp.split('.').join('\\.');
  $('#' + idChamp).focus();
}

//Modifier la langue en cours
function changerLocale(langue) {
  var urlActuel = window.location.href;

  if (urlActuel.indexOf('#') != -1) {
    urlActuel = urlActuel.substring(0, urlActuel.indexOf('#'));
  }

  urlActuel = urlActuel + (urlActuel.indexOf('?') == -1 ? '?' : '&');
  if (urlActuel.indexOf('locale=') == -1) {
	  urlActuel =  urlActuel + 'locale=' + langue;
  }
  else {
	  urlActuel =  urlActuel.substring(0, urlActuel.indexOf('locale=')) + 'locale=' + langue;
  }
  go(urlActuel);
}

function initialiserEditLocale() {
    $(document).ready(function(){
    	   $(".langueSelectionne").switchClass("langueSelectionne","langue","fast");
    });
  }

function traitementApresInitListeValeur(executer) {
	if (executer) {
		focusLienListeNavigation();
		initialiserPlaceHolder();
		initialiserChampsPlaceholder(false);
	}else {
		setTimeout("traitementApresInitListeValeur(true);", 200);
	}
}

function initialiserPlaceHolder() {
	 $(function() {
		  // Plugin placeholder without support HTML 5
		  $('input, textarea').placeholder();
		 });

}

function initialiserChampsPlaceholder() {
	initialiserChampsPlaceholder(false); 
}

function initialiserChampsPlaceholder(avecSubmit) {
	try {
		$('input').each(function(index, value){

			if (IE) {
			    var placeholder = $(this).attr('placeholder');
			    var currentId = $(this).attr('id');
			    
			    var val;
			    if (currentId) {
			    	val = getId(currentId).value;
			    }

			    var conditionPlaceholderNonEgaleVal =  (placeholder != null && (val != null && val != '') && val != placeholder);
			    var conditionPlaceHolderVarNull = (val == null && placeholder == null);
			    
			    var saisieNormal = false;

			    if (conditionPlaceholderNonEgaleVal || conditionPlaceHolderVarNull ) {
				 	setStyleClass(currentId,'saisieNormal'); 
				 	saisieNormal = true;
		        } 
			    
			    if (placeholder != null && (placeholder == val || val == '') && !saisieNormal) {
				     setStyleClass(currentId,'placeholder');
				     getId(currentId).value = placeholder;
				     if (avecSubmit) {
				    	 getId(currentId).value = '';
				     }
			    }
			}
		});
		

	 }catch(E) {
	 }
}

function initialiserChampSaisie() {

	$(document).ready(function(){
		  $("input").focus(function() {
		    $(this).addClass("saisieFocus");
		  });
		  $("input").blur(function() {
		    $(this).removeClass("saisieFocus");
		  });
		  $("textarea").focus(function() {
		    $(this).addClass("saisieFocus");
		  });
		  $("textarea").blur(function() {
		    $(this).removeClass("saisieFocus");
		  });
		  $("select").focus(function() {
		    $(this).addClass("saisieFocus");
		  });
		  $("select").blur(function() {
		    $(this).removeClass("saisieFocus");
		  });
		  
	});
}

function focusPremierChampEnErreur() {
	$('input').each(function(index, value){

	    var currentId = $(this).attr('id');
	    
	    if ($(this).hasClass("saisieErreur")) {
	    	getId(currentId).focus();
	    	return false;
	    }

	});
	
}

function modifierContenuCKEditor(id) {  
  // Fixer la valeur dans l'editeur HTML.
  CKEDITOR.instances[id].setData(getId(id).value);
}

