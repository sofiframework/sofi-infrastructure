<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-tiles" prefix="sofi-tiles"%>

<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="app-dialog" tagdir="/WEB-INF/tags/dialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true">
<head>
  <tiles:importAttribute name="titreApplication" />
  <tiles:importAttribute name="titre" ignore="true" />
  <sofi:libelle var="titrePage" identifiant="${titreApplication}" />
  <c:if test="${titre != null}">
    <sofi:libelle var="titre" identifiant="${titre}" />
    <c:set var="titrePage" value="${titrePage} -> ${titre}" />
  </c:if>
  <title><c:out value="${titrePage}"/></title> 
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <sofi:css href="wro/console.css"></sofi:css>
  <!--[if IE 7]>
    <sofi:css href="css/console_ie7.css"></sofi:css>
  <![endif]-->
  
  <sofi:javascript src="wro/console.js"></sofi:javascript>
  <sofi:javascript src="script/ckeditor/ckeditor.js"></sofi:javascript>
  <sofi:javascript src="script/ckeditor/adapters/jquery.js"></sofi:javascript>
  
  <!-- afficher le calendrier dans la langue de l'utilisateur -->
  <c:if test="${(utilisateur.langue != null) && (utilisateur.langue == 'en')}">
    <sofi:javascript src="script/calendrier/lang/calendar-en.js"></sofi:javascript>
  </c:if>
  <c:if test="${(utilisateur.langue == null) || (utilisateur.langue == 'fr')}">
    <sofi:javascript src="script/calendrier/lang/calendar-ca-fr.js"></sofi:javascript>
  </c:if>
  <!-- afficher les messages de select2 dans la langue de l'utilisateur (par défaut en anglais) -->
  <c:if
    test="${(codeLangueEnCours == null) || (codeLangueEnCours == 'fr_CA')}">
    <sofi:javascript src="script/select2/lang/select2_locale_fr.js"></sofi:javascript>
  </c:if>
 
  
  <link rel="prefetch" href="images/sofi/bouton_noir_droit_disabled.gif">
  <link rel="prefetch" href="images/sofi/bouton_noir_gauche_disabled.gif">  
</head>
<body>
  <div id="background">
   <div id="contenant">
      <tiles:insert attribute="entete"></tiles:insert>
      <app:listeLangueSupportee styleClass="listeLangue"/>
      <tiles:useAttribute name="isOnglet" ignore="true"></tiles:useAttribute>
      <c:if test="${isOnglet != null}">
        <div id="zone_fond_navigation">
          <tiles:insert attribute="filnavigation"></tiles:insert>
          <tiles:insert attribute="onglet"></tiles:insert>
      </c:if>
      <div id="corps">
        <tiles:insert attribute="detail"></tiles:insert>
      </div>

      <div class="clear"></div>

      <div id="bas">
        <tiles:insert attribute="bas_page"/>
      </div>
    </div>
  </div>

  <app:fenetreAttente styleId="attenteAjax" libelle="En traitement, veuillez attentre un moment svp"></app:fenetreAttente>

</body>
<sofi:javascript appliquerOnLoad="true" />
</html:html>