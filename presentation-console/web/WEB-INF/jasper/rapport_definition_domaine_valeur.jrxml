<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="classic_landscape"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="535"
		 columnSpacing="0"
		 leftMargin="25"
		 rightMargin="25"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="WHERE_CLAUSE" isForPrompting="false" class="java.lang.String"/>
	<parameter name="ORDER_BY" isForPrompting="false" class="java.lang.String"/>
	<parameter name="LOGO_SOFI" isForPrompting="false" class="java.lang.String"/>
	<queryString><![CDATA[SELECT domaine.CODE_APPLICATION,
       application.NOM AS NOM_APPLICATION,
       domaine.NOM,
       (
	SELECT DISTINCT DESCRIPTION 
        FROM DOMAINE_VALEUR
        WHERE CODE_APPLICATION = domaine.CODE_APPLICATION
        AND CODE_LANGUE = domaine.CODE_LANGUE
        AND NOM = domaine.NOM
        AND VALEUR = 'DEFINITION'
       ) AS DEFINITION_NOM_DOMAINE,
       domaine.VALEUR,
       domaine.CODE_LANGUE,
       domaine.DESCRIPTION,
       domaine.ORDRE_AFFICHAGE,
       domaine.CODE_TYPE_TRI,
       domaine.CODE_APPLICATION_PARENT,
       domaine.NOM_PARENT,
       domaine.VALEUR_PARENT,
       domaine.CODE_LANGUE_PARENT
FROM DOMAINE_VALEUR domaine, APPLICATION application
WHERE domaine.CODE_APPLICATION = application.CODE_APPLICATION
AND domaine.valeur = 'DEFINITION'
$P!{WHERE_CLAUSE}
]]></queryString>

	<field name="CODE_APPLICATION" class="java.lang.String"/>
	<field name="NOM_APPLICATION" class="java.lang.String"/>
	<field name="NOM" class="java.lang.String"/>
	<field name="DEFINITION_NOM_DOMAINE" class="java.lang.String"/>
	<field name="VALEUR" class="java.lang.String"/>
	<field name="CODE_LANGUE" class="java.lang.String"/>
	<field name="DESCRIPTION" class="java.lang.String"/>
	<field name="ORDRE_AFFICHAGE" class="java.math.BigDecimal"/>
	<field name="CODE_TYPE_TRI" class="java.lang.String"/>
	<field name="CODE_APPLICATION_PARENT" class="java.lang.String"/>
	<field name="NOM_PARENT" class="java.lang.String"/>
	<field name="VALEUR_PARENT" class="java.lang.String"/>
	<field name="CODE_LANGUE_PARENT" class="java.lang.String"/>


		<group  name="CODE_APPLICATION" >
			<groupExpression><![CDATA[$F{CODE_APPLICATION}]]></groupExpression>
			<groupHeader>
			<band height="1"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="10"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="NOM" >
			<groupExpression><![CDATA[$F{NOM}]]></groupExpression>
			<groupHeader>
			<band height="17"  isSplitAllowed="true" >
				<rectangle>
					<reportElement
						x="-2"
						y="2"
						width="544"
						height="15"
						forecolor="#FFFFFF"
						backcolor="#D1DEE7"
						key="rectangle"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="1"
						width="542"
						height="15"
						forecolor="#000000"
						backcolor="#D1DEE7"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true" isItalic="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{NOM}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="4"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="2"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="2"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="73"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="38"
						width="541"
						height="16"
						forecolor="#0C2E6D"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Liste des définitions de domaine pour " + $F{NOM_APPLICATION} + "(" + $F{CODE_APPLICATION} + ")"]]></textFieldExpression>
				</textField>
				<image  scaleImage="RetainShape" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="3"
						width="541"
						height="33"
						key="image-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA[$P{LOGO_SOFI}]]></imageExpression>
				</image>
				<line direction="TopDown">
					<reportElement
						x="1"
						y="56"
						width="541"
						height="0"
						forecolor="#0C2E6D"
						backcolor="#0C2E6D"
						key="line-1"/>
					<graphicElement stretchType="NoStretch" pen="2Point"/>
				</line>
			</band>
		</columnHeader>
		<detail>
			<band height="15"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="25"
						height="15"
						forecolor="#000000"
						key="textField"
						stretchType="RelativeToTallestObject"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Top">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{CODE_LANGUE}+ " - "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="25"
						y="0"
						width="517"
						height="15"
						forecolor="#000000"
						key="textField"
						stretchType="RelativeToBandHeight"
						isPrintInFirstWholeBand="true"
						isPrintWhenDetailOverflows="true"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Top">
						<font size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{DEFINITION_NOM_DOMAINE}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="27"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="333"
						y="7"
						width="170"
						height="19"
						forecolor="#000000"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " de "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="506"
						y="7"
						width="36"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="3"
						width="542"
						height="0"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="line"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="6"
						width="209"
						height="19"
						forecolor="#000000"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
