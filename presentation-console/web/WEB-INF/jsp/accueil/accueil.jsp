<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi" %>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

 <style>
  table.tableformulaire th {
    width:45%;
  }
 </style>
 
<sofi-html:form action="/accueil.do?methode=acceder" transactionnel="false" focus="codeApplication">

    <div id="anti-corps">
      <div id="zone_fond_navigation">
        <div id="zone_selection_application">
          <sofi:libelle styleClass="libelle_selection_application" identifiant="infra_sofi.libelle.accueil.champ.codeApplication" nomAttribut="codeApplication" />
          <sofi-html:select   
            property="codeApplication" 
            collection="${LISTE_APPLICATIONS_UTILISATEUR}"
            ligneVide="true"
            proprieteEtiquette="[nom] ([codeApplication])"
            proprieteValeur="codeApplication"
            href="accueil.do?methode=acceder" />
        </div>
      </div>
    </div> 
    
    <sofi:blocSecurite identifiant="infra_sofi.libelle.gestion_journalisation.service">
      <fieldset>
        <legend>
          <sofi-html:link libelle="infra_sofi.libelle.accueil.action.journalisation"
                  href="accueil.do?methode=afficherJournalisation"
                  divRetourAjax="sectionTableauDeBordJournalisation"
                  styleId="lienSectionJournalisation"
                  ajax="true"
                  ajaxEclatablePersistant="true"
                  ajaxEclatableFermetureNonAutomatique="true"
                  ajaxStyleClassSectionFermee="sectionEclatableFermee"
                  ajaxStyleClassSectionOuverte="sectionEclatableOuverte"
                  >             
          </sofi-html:link>
        </legend>
        <sofi:div id="sectionTableauDeBordJournalisation"
                href="accueil.do?methode=afficherJournalisation" 
                ouvrirFermerDiv="true"
                styleClassIdLienSectionFermee="sectionEclatableFermee"
                styleClassIdLienSectionOuverte="sectionEclatableOuverte"
                styleIdLienSection="lienSectionJournalisation"
                tempsRafraichissement="30"                
                traiterCorps="true">                
             <jsp:include page="ajax_liste_journalisation.jsp" /> 
          </sofi:div>
      </fieldset>    
    </sofi:blocSecurite>
    
    <sofi:blocSecurite identifiant="infra_sofi.libelle.gestion_message.service"> 
      <fieldset>
        <legend>
          <sofi-html:link libelle="infra_sofi.libelle.accueil.action.messages"
                  href="accueil.do?methode=afficherMessage"
                  divRetourAjax="sectionTableauDeBordMessage"
                  styleId="lienSectionMessage"
                  ajax="true"
                  ajaxEclatablePersistant="true"
                  ajaxEclatableFermetureNonAutomatique="true"
                  ajaxStyleClassSectionFermee="sectionEclatableFermee"
                  ajaxStyleClassSectionOuverte="sectionEclatableOuverte">
          </sofi-html:link>
        </legend>
        <sofi:div id="sectionTableauDeBordMessage"
                href="accueil.do?methode=afficherMessage" 
                ouvrirFermerDiv="true"
                styleClassIdLienSectionFermee="sectionEclatableFermee"
                styleClassIdLienSectionOuverte="sectionEclatableOuverte"
                styleIdLienSection="lienSectionMessage"
                tempsRafraichissement="30"
                traiterCorps="true"
                fermerParDefaut="true">                
            <jsp:include page="ajax_liste_message.jsp" />  
        </sofi:div>
      </fieldset>
    </sofi:blocSecurite>
    <sofi:blocSecurite identifiant="infra_sofi.libelle.gestion_libelle.service"> 
      <fieldset>
        <legend>
          <sofi-html:link libelle="infra_sofi.libelle.accueil.action.libelles"
                  href="accueil.do?methode=afficherLibelle"
                  divRetourAjax="sectionTableauDeBordLibelle"
                  styleId="lienSectionLibelle"
                  ajax="true"
                  ajaxEclatablePersistant="true"
                  ajaxEclatableFermetureNonAutomatique="true"                  
                  ajaxStyleClassSectionFermee="sectionEclatableFermee"
                  ajaxStyleClassSectionOuverte="sectionEclatableOuverte">             
          </sofi-html:link>
        </legend>
        <sofi:div id="sectionTableauDeBordLibelle"
                href="accueil.do?methode=afficherLibelle" 
                ouvrirFermerDiv="true"
                styleClassIdLienSectionFermee="sectionEclatableFermee"
                styleClassIdLienSectionOuverte="sectionEclatableOuverte"
                styleIdLienSection="lienSectionLibelle"
                tempsRafraichissement="60"
                traiterCorps="true"
                fermerParDefaut="true">    
              <jsp:include page="ajax_liste_libelle.jsp" />    
        </sofi:div>
      </fieldset>
    </sofi:blocSecurite>
    <sofi:blocSecurite identifiant="infra_sofi.libelle.gestion_parametre_systeme.service"> 
     <fieldset>
      <legend>
        <sofi-html:link libelle="infra_sofi.libelle.accueil.action.parametre_systemes"
                href="accueil.do?methode=afficherParametre"
                divRetourAjax="sectionTableauDeBordParametre"
                styleId="lienSectionParametre"
                ajax="true"
                ajaxEclatablePersistant="true"
                ajaxEclatableFermetureNonAutomatique="true"
                ajaxStyleClassSectionFermee="sectionEclatableFermee"
                ajaxStyleClassSectionOuverte="sectionEclatableOuverte">             
        </sofi-html:link>
      </legend>
        <sofi:div id="sectionTableauDeBordParametre"
              href="accueil.do?methode=afficherParametre" 
              ouvrirFermerDiv="true"
              styleClassIdLienSectionFermee="sectionEclatableFermee"
              styleClassIdLienSectionOuverte="sectionEclatableOuverte"
              styleIdLienSection="lienSectionParametre"
              tempsRafraichissement="90"
              traiterCorps="true"
              fermerParDefaut="true">                
          <jsp:include page="ajax_liste_parametre.jsp" />  
        </sofi:div>
      </fieldset>
    </sofi:blocSecurite>
    <sofi:blocSecurite identifiant="infra_sofi.libelle.gestion_aide.service"> 
      <fieldset>
        <legend>
          <sofi-html:link libelle="infra_sofi.libelle.accueil.action.aide_ligne"
                    href="accueil.do?methode=afficherAide"
                    divRetourAjax="sectionTableauDeBordAide"
                    styleId="lienSectionAide"
                    ajax="true"
                    ajaxEclatablePersistant="true"
                    ajaxEclatableFermetureNonAutomatique="true"                    
                    ajaxStyleClassSectionFermee="sectionEclatableFermee"
                    ajaxStyleClassSectionOuverte="sectionEclatableOuverte"
                    >             
          </sofi-html:link>
        </legend>
        <sofi:div id="sectionTableauDeBordAide"
              href="accueil.do?methode=afficherAide" 
              ouvrirFermerDiv="true"
              styleClassIdLienSectionFermee="sectionEclatableFermee"
              styleClassIdLienSectionOuverte="sectionEclatableOuverte"
              styleIdLienSection="lienSectionAide"
              tempsRafraichissement="60"
              traiterCorps="true"
              fermerParDefaut="true">                
          <jsp:include page="ajax_liste_aide.jsp" />  
        </sofi:div>
      </fieldset>  
    </sofi:blocSecurite>    
</sofi-html:form>