<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%> 
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>

<sofi:div id="sectionTableauDeBordParametre"
           varOuverture="sectionParametreOuvert"/>   

<c:if test="${sectionParametreOuvert}" >   
  <sofi-liste:listeNavigation action="accueil.do?methode=afficherParametre"
                              id = "parametre"
                              nomListe="${listeParametreTableauBord}"
                              divId="listeParametreTableauBord"
                              colgroup="30%,50%,10%,10%"
                              triDefaut="4"
                              triDescendantDefaut="true"
                              class="tableresultat"
                              ajax="true"
                              iterateurMultiPage="true"
                              libelleResultat="paramètre(s)"
                              messageListeVide="infra_sofi.information.accueil.liste_parametre_vide"
                              colonneLienDefaut="1"
                              >
    <sofi-liste:colonne property="codeParametre" href="parametre.do?methode=consulter" 
                        paramId="id" paramProperty="id" trier="true" 
                        libelle="Code Paramètre" />
    <sofi-liste:colonne property="valeur" trier="true" libelle="Valeur" />
    <sofi-liste:colonne property="dateCreation"
                        libelle="infra_sofi.libelle.commun.entete.creation" 
                        trier="true"
                        traiterCorps="true">  
      <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${parametre.dateCreation}"/>
    </sofi-liste:colonne> 
    <sofi-liste:colonne property="dateModification"
                        libelle="infra_sofi.libelle.commun.entete.maj" 
                        trier="true"
                        traiterCorps="true">  
      <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${parametre.dateModification}"/>
    </sofi-liste:colonne>   
  </sofi-liste:listeNavigation>  
</c:if>
<c:if test="${!sectionParametreOuvert}" >
  <sofi:libelle identifiant="infra_sofi.libelle.accueil.titre.cliquez_consulter_parametres" />
</c:if>