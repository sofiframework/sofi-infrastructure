<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%> 
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
 
<sofi:div id="sectionTableauDeBordMessage"
           varOuverture="sectionMessageOuvert"/>   
<c:if test="${sectionMessageOuvert}" >           
  <sofi-liste:listeNavigation action="accueil.do?methode=afficherMessage"
                              nomListe="${listeMessageTableauBord}"
                              divId="listeMessageTableauBord"
                              colgroup="80%,10%,10%"
                              triDefaut="3"
                              triDescendantDefaut="true"
                              class="tableresultat"
                              ajax="true"
                              iterateurMultiPage="true"
                              libelleResultat="message(s)"
                              id="message"
                              messageListeVide="infra_sofi.information.accueil.liste_message_vide"
                              colonneLienDefaut="1"
                              >
    <sofi-liste:colonne property="texteMessage" 
                        libelle="infra_sofi.libelle.gestion_message.entete.texte"   
                        href="message.do?methode=consulter" 
                        paramId="id" paramProperty="id"   
                        trier="true" 
                        traiterCorps="true">
      <c:out value="${message.codeLangue}"></c:out>-
      <c:out value="${message.texteMessage}"></c:out>
    </sofi-liste:colonne>
    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${message.dateCreation}" var="dateCreation" />
    <sofi-liste:colonne property="dateCreation"
                        libelle="infra_sofi.libelle.commun.entete.creation" 
                        trier="true"
                        traiterCorps="true">  
      <c:out value="${dateCreation}"/>
    </sofi-liste:colonne> 
    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${message.dateModification}" var="dateModification" />
    <sofi-liste:colonne property="dateModification"
                        libelle="infra_sofi.libelle.commun.entete.maj" 
                        trier="true"
                        traiterCorps="true">  
      <c:out value="${dateModification}"/>
    </sofi-liste:colonne>   
  </sofi-liste:listeNavigation>  
</c:if>
<c:if test="${!sectionMessageOuvert}" >
  <sofi:libelle identifiant="infra_sofi.libelle.accueil.titre.cliquez_consulter_message" />
</c:if>