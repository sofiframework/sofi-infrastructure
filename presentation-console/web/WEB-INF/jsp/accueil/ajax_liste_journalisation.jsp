<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%> 
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
 
<sofi:div id="sectionTableauDeBordJournalisation"
           varOuverture="sectionJournalisationOuvert"/>                 
 
<c:if test="${sectionJournalisationOuvert}" >
  <sofi-liste:listeNavigation action="accueil.do?methode=afficherJournalisation"
                              nomListe="${listeJournalisationTableauBord}"
                              divId="listeJournalisationTableauBord"
                              colgroup="55%,20%,15%,10%"
                              class="tableresultat"
                              ajax="true"
                              libelleResultat="infra_sofi.libelle.gestion_journalisation.compteur.element.actions"
                              iterateurMultiPage="true"
                              id="journal"
                              messageListeVide="infra_sofi.information.accueil.liste_journalisation_vide"
                              colonneLienDefaut="1"
                              triDescendantDefaut="true"
                              >   
                              
    <sofi-liste:colonne property="detail" 
                        href="journalisation.do?methode=consulter" 
                        paramId="idJournalisation" paramProperty="idJournalisation"
                        trier="true"
                        libelle="infra_sofi.libelle.gestion_journalisation.entete.description"
                        sautLigneCararactereSeparateur=""
                        sautLigneNombreCararactereMaximal="60" />
    <sofi-liste:colonne property="type" trier="true"
	                      libelle="infra_sofi.libelle.gestion_journalisation.entete.type" 
	                      traiterCorps="true">
      <sofi:cache cache="LISTE_TYPE_JOURNALISATION_${systeme_selectionne}" valeur="${journal.type}" />
    </sofi-liste:colonne>
    
    
    <sofi-liste:colonne property="codeUtilisateur" trier="true"
                        libelle="infra_sofi.libelle.gestion_journalisation.entete.codeUtilisateur" />           
                        
    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${journal.dateHeure}" var="dateJournal" />
    <sofi-liste:colonne property="dateHeure"
                        libelle="infra_sofi.libelle.gestion_journalisation.entete.date" 
                        trier="true"
                        traiterCorps="true">  
      <c:out value="${dateJournal}"/>
    </sofi-liste:colonne>                     
  </sofi-liste:listeNavigation> 
</c:if>

<c:if test="${!sectionJournalisationOuvert}" >
  <sofi:libelle identifiant="infra_sofi.libelle.accueil.titre.cliquez_consulter_journalisation" />
</c:if>