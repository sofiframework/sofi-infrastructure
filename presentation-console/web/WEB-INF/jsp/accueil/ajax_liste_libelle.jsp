<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

 
<sofi:div id="sectionTableauDeBordLibelle"
           varOuverture="sectionLibelleOuvert"/>
 
<c:if test="${sectionLibelleOuvert}" >
  <sofi-liste:listeNavigation action="accueil.do?methode=afficherLibelle"
                              nomListe="${listeLibelleTableauBord}"
                              divId="listeLibelleTableauBord"
                              colgroup="80%,10%,10%"
                              triDefaut="3" ajax="true"
                              triDescendantDefaut="true"
                              class="tableresultat" 
                              id="libelle"
                              libelleResultat="libellé(s)"
                              iterateurMultiPage="true"
                              messageListeVide="infra_sofi.information.accueil.liste_libelle_vide"
                              colonneLienDefaut="1"
                              >
    <sofi-liste:colonne property="texteLibelle" trier="true" 
                        libelle="infra_sofi.libelle.gestion_libelle.entete.texteLibelle"
                        href="libelle.do?methode=consulter" 
                        paramId="id" paramProperty="id"
                        traiterCorps="true">
      <c:out value="${libelle.codeLangue}"></c:out>-
      <c:out value="${libelle.texteLibelle}"></c:out>
    </sofi-liste:colonne>
    
    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${libelle.dateCreation}" var="dateCreation" />
    <sofi-liste:colonne property="dateCreation"
                        libelle="infra_sofi.libelle.commun.entete.creation" 
                        trier="true"
                        traiterCorps="true">  
      <c:out value="${dateCreation}"/>
    </sofi-liste:colonne> 
    
    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${libelle.dateModification}" var="dateModification" />
    <sofi-liste:colonne property="dateModification"
                        libelle="infra_sofi.libelle.commun.entete.maj" 
                        trier="true"
                        traiterCorps="true">  
      <c:out value="${dateModification}"/>
    </sofi-liste:colonne>     
    
  </sofi-liste:listeNavigation>  
</c:if>

<c:if test="${!sectionLibelleOuvert}" >
  <sofi:libelle identifiant="infra_sofi.libelle.accueil.titre.cliquez_consulter_libelle" />
</c:if>