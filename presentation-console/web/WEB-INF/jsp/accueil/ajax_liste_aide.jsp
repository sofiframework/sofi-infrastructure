<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi:div id="sectionTableauDeBordAide"
           varOuverture="sectionAideOuvert"/>       

<c:if test="${sectionAideOuvert}" >     
  <sofi-liste:listeNavigation action="accueil.do?methode=afficherAide"
                              nomListe="${listeAideTableauBord}"
                              divId="listeAideTableauBord"
                              colgroup="80%,10%,10%"
                              triDefaut="3" ajax="true"
                              triDescendantDefaut="true"
                              class="tableresultat" 
                              iterateurMultiPage="true"
                              libelleResultat="aide(s)"
                              id="aide"
                              messageListeVide="infra_sofi.information.accueil.liste_aide_vide"
                              colonneLienDefaut="1">
    <sofi-liste:colonne property="titre" 
                        libelle="infra_sofi.libelle.gestion_aide.entete.titre"   
                        href="aide.do?methode=consulter" 
                        paramId="cleAide, codeLangue" paramProperty="cleAide, codeLangue"
                        trier="true" 
                        traiterCorps="true">
        <c:out value="${aide.codeLangue}"></c:out>-
        <c:out value="${aide.titre}"></c:out>
    </sofi-liste:colonne>
    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${aide.dateCreation}" var="dateCreation" />
    <sofi-liste:colonne property="dateCreation"
                        libelle="infra_sofi.libelle.commun.entete.creation" 
                        trier="true"
                        traiterCorps="true">  
      <c:out value="${dateCreation}"/>
    </sofi-liste:colonne> 
    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${aide.dateModification}" var="dateModification" />
    <sofi-liste:colonne property="dateModification"
                        libelle="infra_sofi.libelle.commun.entete.maj" 
                        trier="true"
                        traiterCorps="true">  
      <c:out value="${dateModification}"/>
    </sofi-liste:colonne>   
  </sofi-liste:listeNavigation>  
</c:if>
<c:if test="${!sectionAideOuvert}" >
  <sofi:libelle identifiant="infra_sofi.libelle.accueil.titre.cliquez_consulter_aide_en_ligne" />
</c:if>