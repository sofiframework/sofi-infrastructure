<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi-liste:listeNavigation action="rechercheLibelle.do?methode=rechercher"
                            id="libelle"
                            nomListe="${listeLibelle}"
                            colgroup="35%,35%,10%,10%,10%"
                            triDefaut="4" ajax="true"
                            triDescendantDefaut="true"
                            class="tableresultat" 
                            iterateurMultiPage="true"
                            libelleResultat="infra_sofi.libelle.gestion_libelle.compteur.element.libelles"
                            colonneLienDefaut="1"
                            >
  <sofi-liste:colonne property="cleLibelle" href="libelle.do?methode=consulter" 
                      paramId="id" paramProperty="id" trier="true" 
                      libelle="infra_sofi.libelle.gestion_libelle.entete.cleLibelle"
                      sautLigneNombreCararactereMaximal="80"
                      sautLigneCararactereSeparateur="." />
  <sofi-liste:colonne property="texteLibelle" trier="true" 
                      libelle="infra_sofi.libelle.gestion_libelle.entete.texteLibelle" />
  <sofi-liste:colonne property="codeClient" libelle="infra_sofi.libelle.gestion_libelle.entete.client" trier="true" />                      
  <sofi-liste:colonne property="dateCreation" trier="true" traiterCorps="true"
                      libelle="infra_sofi.libelle.commun.entete.creation"> 
    <fmt:formatDate value="${libelle.dateCreation}" pattern="yyyy-MM-dd HH:mm:ss" />
  </sofi-liste:colonne> 
  <sofi-liste:colonne property="dateModification" trier="true" traiterCorps="true"
                      libelle="infra_sofi.libelle.commun.entete.maj">  
    <fmt:formatDate value="${libelle.dateModification}" pattern="yyyy-MM-dd HH:mm:ss" />
  </sofi-liste:colonne>     
</sofi-liste:listeNavigation>