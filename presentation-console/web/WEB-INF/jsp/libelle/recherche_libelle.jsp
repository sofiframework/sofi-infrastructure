<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste"
  prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html"
  prefix="sofi-html"%>

<!-- Section filtre formulaire -->
<sofi-html:form
  action="rechercheLibelle.do?methode=rechercher#listeLibelle"
  transactionnel="false" focus="texteLibelle">
  <fieldset class="formulaire">
    <legend>
      <sofi:libelle
        identifiant="infra_sofi.libelle.commun.bloc.criteres.recherche" />
    </legend>
    <table cellpadding="0" cellspacing="0" class="tableformulaire">
      <sofi-html:text property="texteLibelle"
        libelle="infra_sofi.libelle.gestion_libelle.champ.texteLibelle"
        size="70">
        <sofi-html:select property="codeLangue" cache="listeLocale" />
      </sofi-html:text>
      <sofi-html:text property="cleLibelle"
        libelle="infra_sofi.libelle.gestion_libelle.champ.cleLibelle"
        size="70" />
      <sofi-html:checkbox property="libelleHtml"
        libelle="infra_sofi.libelle.gestion_libelle.champ.libelleHtml" />
      <sofi-html:text property="aideContextuelle"
        libelle="infra_sofi.libelle.gestion_aide.champ.aideContextuelle"
        size="70" />
      <sofi-html:select property="codeApplication" ligneVide="true"
        libelle="infra_sofi.libelle.gestion_libelle.champ.codeApplication"
        collection="${LISTE_APPLICATIONS_UTILISATEUR}"
        proprieteEtiquette="[nom] ([codeApplication])"
        proprieteValeur="codeApplication"
        href="rechercheLibelle.do?methode=fixerApplication" ajax="true"
        nomParametreValeur="codeApplication"
         />

      <sofi-html:select property="codeClient"
        libelle="infra_sofi.libelle.gestion_parametre.codeClient"
        collection="${LISTE_CLIENT}" ligneVide="true"
        classeDecorator="org.sofiframework.infrasofi.presentation.decorator.ClientSelectDecorator" />
        
      <sofi-html:text property="reference" size="30" maxlength="100"
                      libelle="infra_sofi.libelle.gestion_libelle.champ.reference" />

    </table>
    <div class="barreBouton">
      <sofi-html:bouton
        libelle="infra_sofi.libelle.gestion_libelle.action.creer"
        href="libelle.do?methode=ajouter" />
      <sofi-html:submit
        libelle="infra_sofi.libelle.gestion_libelle.action.rechercher" />
    </div>
  </fieldset>
</sofi-html:form>

<!-- Section liste de navigation -->
<c:if test="${listeLibelle != null}">
  <fieldset>
    <legend>
      <sofi:libelle
        identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche" />
    </legend>
    <jsp:include page="ajax_liste_libelle.jsp" />
  </fieldset>
</c:if>

<script>
  $("#codeClient").select2({containerCssClass: "select_grand"});
</script>