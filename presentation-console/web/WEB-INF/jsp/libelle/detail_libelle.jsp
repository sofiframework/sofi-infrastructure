<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<p align="right" class="champsobligatoire">
  <span class="ind_obligatoire">*</span> 
  <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire"/>
</p>

<!-- Section formulaire -->
<sofi-html:form action="libelle.do?methode=enregistrer" transactionnel="true">
  <fieldset class="formulaire">
    <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion_libelle.bloc.detail"/></legend>
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <sofi-html:text property="cleLibelle" 
                      libelle="infra_sofi.libelle.gestion_libelle.champ.cleLibelle" 
                      obligatoire="true" 
                      size="80" 
                      maxlength="300"/>
      <sofi-html:select property="codeApplication" 
                        ligneVide="true"
                        libelle="infra_sofi.libelle.gestion_libelle.champ.codeApplication" 
                        obligatoire="true"
                        collection="${LISTE_APPLICATIONS_UTILISATEUR}"
                        proprieteEtiquette="[nom] ([codeApplication])"
                        proprieteValeur="codeApplication" />
      <sofi-html:select property="codeClient" ligneVide="true"
                      libelle="infra_sofi.libelle.gestion_parametre.codeClient"
                      collection="${LISTE_CLIENT}"
                      classeDecorator="org.sofiframework.infrasofi.presentation.decorator.ClientSelectDecorator" />    
      <sofi-html:text property="aideContextuelleExterne" 
                      libelle="infra_sofi.libelle.gestion_libelle.champ.aideContextuelleExterne" 
                      size="50" 
                      maxlength="300">
      </sofi-html:text>
      <sofi-html:checkbox property="libelleHtml"
                          libelle="infra_sofi.libelle.gestion_libelle.champ.libelleHtml" />
      <sofi-html:text property="reference" size="30" maxlength="100"
                      libelle="infra_sofi.libelle.gestion_libelle.champ.reference" />

    </table>

    <br/>
   
  
    <fieldset class="cadreNavigationSousSection">
      <legend>Contenu par langue</legend>  
        <div class="zoneNavigationSousSection">
          <ul>
            <sofi:cache cache="listeLocale" var="listeLocaleApplication" />
            <c:forEach items="${listeLocaleApplication }" var="localeApplication">
              <li>
	              <c:set var="stylelangue" value="langue" />
	              <c:if test="${localeApplication.key == libelleForm.codeLangue}">
	                <c:set var="stylelangue" value="langueSelectionne" />
	              </c:if>
	              
	              <c:if test="${libelleForm.libelleHtml}" >
	                <sofi-html:link styleClass="${stylelangue}"
				                          styleId="langue_${localeApplication.key}"
				                          libelle="${localeApplication.value}"
				                          href="libelle.do?methode=changerLangue&codeLangue=${localeApplication.key}"
				                          ajax="true"
				                          idRetourMultipleAjax="true"
				                          activerConfirmationModificationFormulaire="true"
				                          ajaxJSApresChargement="initialiserLangue();setStyleClass('langue_${localeApplication.key}','langueSelectionne');modifierContenuCKEditor('texteLibelle');" />
				         </c:if>  
                 <c:if test="${!libelleForm.libelleHtml}" >
                  <sofi-html:link styleClass="${stylelangue}"
                                  styleId="langue_${localeApplication.key}"
                                  libelle="${localeApplication.value}"
                                  href="libelle.do?methode=changerLangue&codeLangue=${localeApplication.key}"
                                  ajax="true"
                                  idRetourMultipleAjax="true"
                                  activerConfirmationModificationFormulaire="true"
                                  ajaxJSApresChargement="initialiserLangue();setStyleClass('langue_${localeApplication.key}','langueSelectionne');modifierContenuCKEditor('aideContextuelleHtml');" />
                 </c:if>  
              </li>
            </c:forEach>
          </ul>
        </div>
      </fieldset>  
      <div class="fond_sous_section" >
      <style>
      table.tableformulaireinterne th {
        width: 20%;
        }
      </style>
        <table border="0" cellspacing="0" cellpadding="0" class="tableformulaireinterne">
          <c:if test="${!libelleForm.libelleHtml}" >
        
            <sofi-html:textarea property="texteLibelle" 
                              libelle="infra_sofi.libelle.gestion_libelle.champ.texteLibelle" 
                              obligatoire="true" 
                              rows="5" 
                              cols="90" 
                              maxlength="12000"
                              /> 
 
           <sofi-html:text    property="placeholder" 
                              libelle="infra_sofi.libelle.gestion_libelle.champ.placeholder" 
                              size="85"
                              maxlength="100"/> 
                              
            <sofi-html:textarea property="aideContextuelle" 
                              libelle="infra_sofi.libelle.gestion_libelle.champ.aideContextuelle" 
                              obligatoire="false" 
                              rows="5" 
                              cols="90"/>  
                              
            <sofi-html:ckeditor  libelle="infra_sofi.libelle.gestion_libelle.champ.aideContextuelleHtml" 
                                 property="aideContextuelleHtml"
                                 rows="15" cols="90"/>
          </c:if>   
          <c:if test="${libelleForm.libelleHtml}" >   
            <sofi-html:ckeditor property="texteLibelle" 
                              libelle="infra_sofi.libelle.gestion_libelle.champ.texteLibelle" 
                              obligatoire="true" 
                              rows="40" 
                              cols="120" 
                              /> 
          
          </c:if>            
                      
          <jsp:include page="../trace.jsp" />    
        </table> 
		  </div>
		  <div class="barreBouton">
		      <c:if test="${!libelleForm.nouveauFormulaire}">

		      
		        <!-- Configurer un bouton qui permet de vider la cache du libelle -->
		        <sofi-html:bouton  href="libelle.do?methode=initialiserLibelle" 
		            libelle="infra_sofi.libelle.gestion_libelle.action.cache" />    
		            
            <sofi-html:bouton 
                libelle="infra_sofi.libelle.gestion_libelle.action.copier" 
                href="libelle.do?methode=copier" 
                styleClass="bouton" />
                		              
		        <sofi-html:submit libelle="infra_sofi.libelle.gestion_libelle.action.creer" 
		                          href="libelle.do?methode=ajouter"
		                          ignorerValidation="true" />
		        <!-- Configurer l'URL permettant de supprimer -->
		        <c:set var="original" value="${libelleForm.objetTransfertOriginal}"/>
		        <c:url value="libelle.do?methode=supprimer" var="urlSupprimerLibelle" />
		        <!-- Configurer un message de confirmation avec variable dynamique -->
		        <c:if test="${!libelleForm.nouveauFormulaire}">
			        <sofi-html:bouton  href="${urlSupprimerLibelle}" 
			            libelle="infra_sofi.libelle.gestion_libelle.action.supprimer" 
			            messageConfirmation="infra_sofi.confirmation.gestion_libelle.suppression" />
		        </c:if>
		      </c:if>
		      <sofi-html:submit 
		      		libelle="infra_sofi.libelle.gestion_libelle.action.enregistrer" 
		      		property="enregistrer" 
		      		styleClass="bouton" />

		    </div>
		</fieldset>
</sofi-html:form>

<script>
  $("#codeClient").select2({containerCssClass: "select_grand"});
</script>