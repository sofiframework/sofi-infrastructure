<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<!-- Section filtre formulaire -->
<sofi-html:form action="/rechercheJournalisation.do?methode=rechercher#listeJournalisation" transactionnel="false" focus="detail">
  <fieldset class="formulaire">
  <legend><sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.criteres.recherche"/></legend>
    <table cellpadding="0" cellspacing="0" class="tableformulaire">
      <sofi-html:select 
            property="codeApplication" 
            libelle="infra_sofi.libelle.gestion_journalisation.champ.systeme" 
            collection="${LISTE_APPLICATIONS_UTILISATEUR}"
            proprieteEtiquette="[nom] ([codeApplication])"
            proprieteValeur="codeApplication"
            ajax="true"
            ligneVide="true"
            href="rechercheJournalisation.do?methode=chargerListeTypeJournalisation"
            nomParametreValeur="codeApplication"
            nomAttributDestinationAjax="type" />
      <sofi-html:select property="type" collection="${LISTE_TYPE_JOURNALISATION}" 
                        proprieteEtiquette="description"
                        proprieteValeur="valeur"
                        ligneVide="true" libelle="infra_sofi.libelle.gestion_journalisation.champ.type" /> 
      <sofi-html:text property="detail" size="70" maxlength="300"
                      libelle="infra_sofi.libelle.gestion_journalisation.champ.detail" />
      <sofi-html:date property="dateDebut" 
                      libelle="infra_sofi.libelle.gestion_journalisation.champ.date"
                      heure="true">
          &nbsp;<sofi:libelle identifiant="infra_sofi.libelle.gestion_journalisation.au" nomAttribut="dateFin" />&nbsp;
          <sofi-html:date property="dateFin" heure="true"/>
      </sofi-html:date>
      <sofi-html:text property="nom" size="40" maxlength="100"
                      libelle="infra_sofi.libelle.gestion_journalisation.champ.nom" />
      <sofi-html:text property="courriel" size="60" maxlength="300"
                      libelle="infra_sofi.libelle.gestion_journalisation.champ.courriel" />
      <sofi-html:text property="codeUtilisateur" size="30" maxlength="300"
                      libelle="infra_sofi.libelle.gestion_journalisation.champ.codeUtilisateur" />
    </table>
    <div class="barreBouton">
      <sofi-html:submit libelle="infra_sofi.libelle.gestion_journalisation.action.rechercher"/>  
    </div>
  </fieldset>
</sofi-html:form>
<!-- Section liste de navigation -->
<c:if test="${listeJournalisation != null}">
  <fieldset>
    <legend><sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche"/></legend>
    <jsp:include page="ajax_liste_journalisation.jsp"/>
  </fieldset>
</c:if>