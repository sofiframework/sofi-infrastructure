<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%> 
<sofi-liste:listeNavigation action="rechercheJournalisation.do?methode=rechercher"
                            nomListe="${listeJournalisation}"
                            colgroup="50%,10%,10%,10%,10%"
                            triDefaut="3"
                            triDescendantDefaut="true"
                            class="tableresultat"
                            ajax="true"
                            libelleResultat="infra_sofi.libelle.gestion_journalisation.compteur.element.actions"
                            iterateurMultiPage="true"
                            id="journal">            
  <sofi-liste:colonne property="detail" 
                      href="journalisation.do?methode=consulter" 
                      paramId="idJournalisation" paramProperty="idJournalisation"
                      trier="true"
                      libelle="infra_sofi.libelle.gestion_journalisation.entete.description"
                      sautLigneNombreCararactereMaximal="80" />
  <sofi-liste:colonne property="type" trier="true"
                      libelle="infra_sofi.libelle.gestion_journalisation.entete.type" 
                      traiterCorps="true">
    <sofi:cache cache="LISTE_TYPE_JOURNALISATION_${systeme_selectionne}" valeur="${journal.type}" />
  </sofi-liste:colonne>
  <sofi-liste:colonne property="dateHeure" trier="true" traiterCorps="true"
                      libelle="infra_sofi.libelle.gestion_journalisation.entete.date">  
    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${journal.dateHeure}"/>
  </sofi-liste:colonne>        
  <sofi-liste:colonne property="codeApplication" trier="true" libelle="infra_sofi.libelle.gestion_journalisation.entete.systeme" />
  <sofi-liste:colonne property="codeUtilisateur" trier="true"
                      libelle="infra_sofi.libelle.gestion_journalisation.entete.codeUtilisateur" />
</sofi-liste:listeNavigation>