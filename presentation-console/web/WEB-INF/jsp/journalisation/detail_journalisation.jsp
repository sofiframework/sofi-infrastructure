<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<!-- Section formulaire -->
<sofi-html:form action="/journalisation.do?methode=enregistrer" 
                transactionnel="true"
                focus="identifiant">
                
  <fieldset class="formulaire"> 
    <legend><sofi:libelle identifiant="infra_sofi.libelle.service.journalisation.champ.detail"/></legend>  
    <c:out value="${journalisationForm.detail}" />
  </fieldset>
  <fieldset class="formulaire">
    <legend><sofi:libelle identifiant="infra_sofi.libelle.service.journalisation.section.detail"/></legend>
                         
    <table border="0" cellspacing="0" cellpadding="0" class="tableaffichage"> 
      <sofi:cache cache="listeApplication" 
                  valeur="${journalisationForm.codeApplication}" 
                  proprieteAffichage="nom" 
                  var="nomApplication" />
      <sofi-html:text value="${nomApplication}"
                      libelle="infra_sofi.libelle.service.journalisation.champ.systeme" 
                      affichageSeulement="true" />
   </table>                      
   <table border="0" cellspacing="0" cellpadding="0" class="tableaffichage">                      
      <th>
        <sofi:libelle identifiant="infra_sofi.libelle.service.journalisation.champ.type" 
                      styleClass="libelle_formulaire"/>
      </th>
      <td>
        <sofi:cache cache="LISTE_TYPE_JOURNALISATION_${journalisationForm.codeApplication}" valeur="${journalisationForm.type}" />
      </td>
    </table>
    <table border="0" cellspacing="0" cellpadding="0" class="tableaffichage">      
      <sofi-html:text property="nomObjet" 
                      libelle="Emplacement :"
                      affichageSeulement="true" />
      <th></th>
      <td>
        <c:if test="${journalisationForm.typeObjet != null}">
	        <sofi:cache cache="listeTypeObjetSecurisable" valeur="${journalisationForm.typeObjet}" var="typeObjet" />
	        <sofi:libelle identifiant="${typeObjet}" />
	        &nbsp;-&nbsp;
	        <sofi:libelle identifiant="${journalisationForm.nomObjet}" />
        </c:if>
      </td>
      </table>
      <table border="0" cellspacing="0" cellpadding="0" class="tableaffichage">
        <sofi-html:date property="dateHeure" 
                        libelle="infra_sofi.libelle.service.journalisation.champ.date" 
                        affichageSeulement="true"/>
        <sofi-html:text property="nomComplet" 
                        size="70" maxlength="300"
                        libelle="infra_sofi.libelle.service.journalisation.champ.nom"
                        affichageSeulement="true"/>
        <sofi-html:text property="courriel" size="70" maxlength="300"
                        libelle="infra_sofi.libelle.service.journalisation.champ.courriel"
                        affichageSeulement="true"/>
        <sofi-html:text property="codeUtilisateur" size="70" maxlength="300"
                        libelle="infra_sofi.libelle.service.journalisation.champ.code.utilisateur"
                        affichageSeulement="true"/> 
      </table>
    </table>
  </fieldset>
</sofi-html:form>