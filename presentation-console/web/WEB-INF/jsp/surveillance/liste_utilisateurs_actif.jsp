<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<sofi:div id = "sectionListeUtilisateur"
          href="surveillance.do?methode=afficherListeUtilisateur"
          tempsRafraichissement="10"
          traiterCorps="true">
    <jsp:include page="ajax_liste_utilisateurs_actif.jsp" />
</sofi:div>
