<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<div class="fond_sous_section" >
  <fieldset class="sous_section">
  <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion_referentiel.information_complementaire"/></legend>
 <table border="0" cellspacing="0" cellpadding="0" class="tableformulaireinterne">
    <sofi-html:text property="ordreAffichage" size="2" maxlength="2"
                libelle="infra_sofi.libelle.gestion_referentiel.champ.ordrePresentation" obligatoire="true" />
    <c:if test="${objetJavaForm.attributs.type == 'ON'}">
      <sofi-html:text property="niveauOnglet" libelle="infra_sofi.libelle.gestion_referentiel.champ.niveau_onglet"  size="1" affichageIndObligatoire="true"></sofi-html:text>
    </c:if>
    <sofi-html:link href="referentiel.do?methode=traiterDetailPresentation" 
                    ajax="true"
                    nomParametreValeur="modeAffichage"
                    idProprieteValeur="modeAffichage"
                    divRetourAjax="detailPresentation"
                    var="urlTypeAffichage"
                    ajaxJSApresChargement="focus('styleCSS');" />
    <sofi-html:select 
        property="modeAffichage"
        libelle="infra_sofi.libelle.gestion_referentiel.champ.mode_affichage" size="1" 
        cache="listeTypeModeAffichage"
        onchange="${urlTypeAffichage}" />
    <sofi-html:text property="styleCss" libelle="Style CSS :"  size="30"></sofi-html:text>
    <c:if test="${objetJavaForm.attributs.modeAffichage == 'A'}">
      <sofi-html:text property="divAjax" libelle="Div ID Ajax :"  size="30"  />
    </c:if>
  </table>
</fieldset>
</div>