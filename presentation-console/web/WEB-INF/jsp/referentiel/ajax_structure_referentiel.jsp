<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@page import="org.sofiframework.infrasofi.modele.entite.*"%>

<c:url var="urlDetail" value="/referentiel.do"/>

<c:if test="${structureReferentiel != null}">

<fieldset class="sansbordure">
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion_referentiel.bloc.structure"/>
  </legend>
  <div id="arbre" align="left">
  <sofi:arbre id="tree" 
              var="noeud" 
              liste="${structureReferentiel}" 
              classeDecorator="org.sofiframework.infrasofi.presentation.decorator.ObjetJavaNoeudDecorator">
	 <% 
	   Object noeud = pageContext.findAttribute("noeud");
	   boolean isApplication = (noeud instanceof Application);
	   pageContext.setAttribute("isApplication", Boolean.valueOf(isApplication));
	 %>
	 <c:if test="${isApplication}">
	   <sofi:cache var="nomApplication" cache="listeApplication" valeur="${noeud.codeApplication}" proprieteAffichage="nom" />
	   <sofi:libelle identifiant="${nomApplication}"/>
	 </c:if>
	 <c:if test="${!isApplication}">
	 <c:set var="styleClass" value="noeudactif"/>
	 <c:set var="title" value=""/>
	 <% pageContext.setAttribute("maintenant", new java.util.Date()); %>
	 <c:set var="inactif" value="${noeud.dateDebutActivite > maintenant || (noeud.dateFinActivite != null && noeud.dateFinActivite < maintenant)}"/>
	 <c:if test="${inactif}">
	  <c:set var="styleClass" value="noeudinactif"/>
	 </c:if>
	 <c:if test="${!inactif}">
	  <c:set var="styleClass" value="noeudactif"/>
	 </c:if>
	 <span class="<c:out value="${styleClass}"/>">
	  <sofi:cache cache="listeTypeObjetSecurisable" valeur="${noeud.type}"/> -
	  <sofi:libelle identifiant="${noeud.nom}" var="nomComposant" iconeAide="false"/>
	  <c:out value="${nomComposant}" escapeXml="true" />
	  <sofi:aide nomComposantReferentiel="${noeud.nom}" var="aide"/>
	  <c:if test="${aide != null}">
	   <sofi:libelle identifiant="${noeud.nom}" aideContextuelleSeulement="true" iconeAide="true"/>
	  </c:if>
	  <c:if test="${inactif}">
	    (Inactif)
	  </c:if>
	 </span>
	 </c:if>
	</sofi:arbre>
	</div>
	
	<!-- JavaScript pour initialiser l'arbre. -->
	<script type="text/javascript">
	   var item_selection;

	   var tree = new Zapatec.Tree("tree", {
	       initLevel : 0,
	       defaultIcons : "customIcon"
	   });

	   tree.onItemSelect = function() {
	     setStatutBoutons();
	   }
	   
	   if (${selectionStructure != null}) {
	     item_selection = 'tree_<c:out value="${selectionStructure}"/>';
	     Zapatec.Tree.all['tree'].sync(item_selection);
	     setStatutBoutons();
	   }
	   
	   // La fonction getId est remplacer par getIdSelectionner car elle est utiliser par un autre fichier Js
	   function getIdSelectionner() {
	     return item_selection.substring("tree_".length);
	   }
	   
	   function setStatutBoutons() {
	    
		 var id = getIdSelectionner();
	     var app = !parseInt(id);
	
	     setBoutonInactif(app, 'consulter');
	     setBoutonInactif(app, 'supprimer');
	   }
	   
	   function consulter() {
	     window.location = '<c:out value="${urlDetail}"/>?methode=consulter&seqObjetSecurisable=' + getIdSelectionner();
	   }
	   
	   function creer() {
	     var url = '<c:out value="${urlDetail}"/>?methode=creer';
	
	     if (item_selection != null) {
	       var id = getIdSelectionner();
	       if (parseInt(id)) {
	         url += '&idObjetJavaParent=' + id;
	       } else {
	         url += '&codeApplication=' + id;
	       }
	     }
	     window.location = url;
	   }
	   
	   function supprimer() {
	     var id = getIdSelectionner();
	     var msg = "<sofi:message identifiant="infra_sofi.confirmation.gestion_referentiel.suppression"/>";
	     if (confirm(msg)) {
	       window.location = '<c:out value="${urlDetail}"/>?indModification=1&methode=supprimer&seqObjetSecurisable=' + id;
	     }
	   }
   </script>
</fieldset>
</c:if>