<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<c:set var="nouveauFormulaire" value="${objetJavaForm.attributs.id eq null}"/>

<p align="right" class="champsobligatoire">
  <span class="ind_obligatoire">*</span>
  <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire"/>
</p>

<sofi-html:form action="/referentiel.do?methode=enregistrer" transactionnel="true" focus="nom">
<fieldset>
  <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion_referentiel.bloc.detail"/></legend>
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <sofi-html:text property="id" 
                      libelle="infra_sofi.libelle.gestion_referentiel.champ.seqObjetSecurisable" 
                      obligatoire="false" size="5"
                      affichageSeulement="true"/>
      <sofi-html:checkbox libelle="infra_sofi.libelle.gestion_referentiel.champ.securise" 
                          property="securise" 
                          styleClass="checkbox">
        <c:if test="${!nouveauFormulaire && objetJavaForm.attributs.securise}">  
          <c:url value="securiteReferentiel.do" var="lienSecurite">
            <c:param name="methode" value="consulter"></c:param>
            <c:param name="seqObjetSecurisable" value="${objetJavaForm.attributs.id}"></c:param>
          </c:url>
          <sofi-html:link href="${lienSecurite}" 
                          libelle="infra_sofi.libelle.gestion_libelle.action.acceder_securite_composant" 
                          activerConfirmationModificationFormulaire="true"
                          memoriserHrefActuel="true" />
        </c:if>
      </sofi-html:checkbox>
      
      <sofi-html:text property="versionLivraison" 
                      libelle="infra_sofi.libelle.gestion_referentiel.action.livraison" 
                      size="5"
                      maxlength="5"/> 
      <sofi-html:text libelle="infra_sofi.libelle.gestion_referentiel.champ.nom" 
                      property="nom" 
                      obligatoire="true"
                      size="90"
                      maxlength="300"
                      href="referentiel.do?methode=fixerLibelle"
                      idRetourAjax="libelle"/>
      <c:if test="${!nouveauFormulaire}">
        <sofi:libelle var="libelle" identifiant="${objetJavaForm.attributs.nom}" iconeAide="false"/>
      </c:if>
      <c:if test="${nouveauFormulaire}">
        <sofi:libelle var="libelle" identifiant="${objetJavaForm.attributs.libelle}"/>
      </c:if>
      <sofi-html:text libelle="infra_sofi.libelle.gestion_referentiel.champ.libelle" 
                      property="libelle"
                      value="${libelle}"
                      affichageSeulement="${!nouveauFormulaire}"
                      size="90"
                      obligatoire="${nouveauFormulaire}" >
                     
        <c:if test="${!nouveauFormulaire && !objetJavaForm.formulaireEnErreur}">
          <c:url value="libelle.do" var="lienLibelle">
            <c:param name="methode" value="consulter"></c:param>
            <c:param name="cleLibelle" value="${objetJavaForm.attributs.nom}"></c:param>
            <c:param name="codeLangue" value="${codeLangueEnCours}"></c:param>
          </c:url>
          &nbsp;
          <sofi-html:link href="${lienLibelle}" 
                          libelle="infra_sofi.libelle.gestion_libelle.action.modifier" 
                          activerConfirmationModificationFormulaire="true"
                          memoriserHrefActuel="true" />
        </c:if>
      </sofi-html:text>
      <sofi-html:link var="ajaxChangerApplication" ajax="true" 
        href="referentiel.do?methode=changerApplication" 
        nomParametreValeur="codeApplication"
        idProprieteValeur="codeApplication" />
      <sofi-html:select 
          property="codeApplication" 
          ligneVide="true"
          libelle="infra_sofi.libelle.gestion_referentiel.champ.codeApplication" 
          obligatoire="true"
          collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          proprieteEtiquette="nom"
          proprieteValeur="codeApplication" 
          onchange="${ajaxChangerApplication}" />
 
        <sofi:taille valeur="${LISTE_FACETTE_APPLICATION}" var="nbListeFacetteApplication"/>
 
        <c:if test="${nbListeFacetteApplication > 0}" >   
         <sofi-html:select
            property="codeFacette"
            libelle="infra_sofi.libelle.gestion_referentiel.champ.codeFacette"
            collection="${LISTE_FACETTE_APPLICATION}"
            proprieteEtiquette="description"
            proprieteValeur="valeur"
            ligneVide="true"
          />
        </c:if>         
          
        <sofi-html:link 
          href="referentiel.do?methode=traiterDetailPresentation" 
          ajax="true"
          nomParametreValeur="type"
          idProprieteValeur="type"
          divRetourAjax="detailPresentation"
          var="urlTypeObjet"
          ajaxJSApresChargement="focus('description');" />
        <sofi-html:select 
          property="type" ligneVide="true"
          libelle="infra_sofi.libelle.gestion_referentiel.champ.type" 
          obligatoire="true" size="1" 
          cache="listeTypeObjetSecurisable" 
          onchange="${urlTypeObjet}" />
        <sofi-html:textarea 
          property="description" rows="4" cols="90" maxlength="500"
          libelle="infra_sofi.libelle.gestion_referentiel.champ.description"
          styleClass="test" />
        <sofi-html:text 
          property="nomAction" size="50" maxlength="120"
          libelle="infra_sofi.libelle.gestion_referentiel.champ.nomAction" />
        <sofi-html:text 
          property="nomParametre" size="100" maxlength="300"
          libelle="infra_sofi.libelle.gestion_referentiel.champ.nomParametre" />
        <sofi-html:text 
          property="adresseWeb" size="100" maxlength="500"
          libelle="infra_sofi.libelle.gestion_referentiel.champ.adresseWeb" />
        <sofi-html:date 
          property="dateDebutActivite" obligatoire="true"
          libelle="infra_sofi.libelle.gestion_referentiel.champ.dateDebutActivite" />
        <sofi-html:date 
          property="dateFinActivite" 
          libelle="infra_sofi.libelle.gestion_referentiel.champ.dateFinActivite"/>
        <sofi-html:text 
          libelle="infra_sofi.libelle.gestion_referentiel.champ.statutActivation" 
          value="${statut}"
          affichageSeulement="true" />
          
        <sofi-html:listeValeurs href="rechercheReferentiel.do?methode=rechercherParent" 
                                property="idParent" 
                                libelle="infra_sofi.libelle.gestion_referentiel.champ.nomParent"
                                paramId="paramSeqObjetSecurisable, paramNom, paramTypeObjetParent" 
                                paramProperty="idParent, nomParent, typeObjetParent"
                                largeurfenetre="550" hauteurfenetre="280"
                                size="6" maxlength="12" 
                                attributRetour="idParent,nomParent"
                                ajax="true"
                                activerFiltreSurEvenementOnchange="true"
                                identifiant="idParent">
            <sofi-html:text property="nomParent" size="70" maxlength="300" />
            <sofi-html:select property="typeObjetParent" 
                              ligneVide="true"
                              size="1" 
                              cache="listeTypeObjetSecurisable" />
        </sofi-html:listeValeurs>
        <c:if test="${objetJavaForm.attributs.type != 'SC'}">
          <th class="libelle_formulaire">
            <sofi:libelle identifiant="infra_sofi.libelle.gestion_referentiel.champ.cleAide" />
          </th>
          <td>
            <c:if test="${objetJavaForm.attributs.cleAide != null}">
              <c:url var="lienAide" value="aide.do">
                <c:param name="codeLangue" value="FR"/>
                <c:param name="methode" value="consulter"/>
                <c:param name="cleAide" value="${objetJavaForm.attributs.cleAide}"/>
              </c:url>
              <sofi-html:link href="${lienAide}" 
                              libelle="${objetJavaForm.attributs.cleAide}" 
                              activerConfirmationModificationFormulaire="true"
                              memoriserHrefActuel="true">
              </sofi-html:link>
            </c:if>
            <c:if test="${objetJavaForm.attributs.cleAide == null}">
              <c:url var="lienAide" value="aide.do">
                <c:param name="codeLangue" value="FR"/>
                <c:param name="methode" value="ajouter"/>
                <c:param name="codeApplication" value="${objetJavaForm.attributs.codeApplication}"/>
              </c:url>
              <sofi-html:link href="${lienAide}" 
                              libelle="infra_sofi.libelle.gestion_referentiel.action.nouveau_composant" 
                              activerConfirmationModificationFormulaire="true"
                              memoriserHrefActuel="true"></sofi-html:link>
            </c:if>      
          </td>   
      </c:if>
    </table>
    <br/>
    <div id="detailPresentation">
      <c:if test="${objetJavaForm.attributs.type == 'ON' || objetJavaForm.attributs.type == 'SE' || objetJavaForm.attributs.type == 'SC'}">    
        <jsp:include page="detail_presentation.jsp" />
      </c:if>
    </div>   
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
        <sofi-html:text libelle="infra_sofi.libelle.commun.champ.creePar"
                        property="creePar"
                        disabled="true"
                        size="25">
                        &nbsp;<sofi-html:text property="dateCreation" disabled="true" />
        </sofi-html:text>
        <sofi-html:text libelle="infra_sofi.libelle.commun.champ.modifiePar"
                        property="modifiePar"
                        disabled="true"
                        size="25">
                        &nbsp;<sofi-html:text property="dateModification" disabled="true" />
        </sofi-html:text>  
    </table>
    <div class="barreBouton">
      <c:set var="nouveau" value="${nouveauFormulaire}"/>
      <c:url var="urlSupprimer" value="/referentiel.do">
        <c:param name="methode" value="supprimer"/>
        <c:param name="seqObjetSecurisable" 
                 value="${objetJavaForm.attributs.id}"/>
        <c:param name="ref" value="detail"/>
      </c:url>
     
		<c:if test="${nouveau}">
		
			 <sofi-html:bouton 
		          href="rechercheReferentiel.do?methode=structure&id=${objetJavaForm.attributs.idParent}" 
		          libelle="infra_sofi.libelle.gestion_referentiel.action.retour_referentiel"
		       />
		</c:if>
      <c:if test="${!nouveau}">
        <sofi:libelle 
          var="confirmation" parametre1="${objetJavaForm.attributs.nom}"
          identifiant="infra_sofi.confirmation.gestion_referentiel.suppression"/>
        <sofi-html:submit 
          href="referentiel.do?methode=creer&idObjetJavaParent=${objetJavaForm.attributs.id}" 
          libelle="infra_sofi.libelle.gestion_referentiel.action.creer"
          ignorerValidation="true" />
        <sofi-html:bouton 
          href="${urlSupprimer}" notifierModificationFormulaire="true"
          libelle="infra_sofi.libelle.gestion_referentiel.action.supprimer"
          messageConfirmation="${confirmation}" />
      </c:if>
      <sofi-html:submit 
        libelle="infra_sofi.libelle.gestion_referentiel.action.enregistrer" 
        href="referentiel.do?methode=enregistrer" />
    </div>
  </fieldset>
</sofi-html:form>
<br/>

