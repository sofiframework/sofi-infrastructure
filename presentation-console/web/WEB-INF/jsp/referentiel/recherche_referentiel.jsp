<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<fieldset>
	<legend>
	  <sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.criteres.recherche" />
	</legend>
	<sofi-html:form action="/rechercheReferentiel.do?methode=rechercher#listeReferentiel"
	                transactionnel="false" focus="nom" method="POST">
	  <input type="hidden" name="methode" value="rechercher" />
	  <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
	    <sofi-html:text 
	        libelle="infra_sofi.libelle.gestion_referentiel.champ.nom" 
	        property="nom" size="80" />
	    <sofi-html:select 
	        property="type" ligneVide="true"
	        libelle="infra_sofi.libelle.gestion_referentiel.champ.type"
	        cache="listeTypeObjetSecurisable" />
	    <sofi-html:select 
	        property="statutActivation" ligneVide="true"
	        libelle="infra_sofi.libelle.gestion_referentiel.champ.statutActivation"
	        cache="listeStatutActivation" />
	    <sofi-html:select 
	        property="codeApplication" 
	        libelle="infra_sofi.libelle.gestion_referentiel.champ.codeApplication"
	        collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          proprieteEtiquette="[nom] ([codeApplication])"
	        proprieteValeur="codeApplication"
	        href="rechercheReferentiel.do?methode=fixerApplication"
	        ajax="true"
	        nomParametreValeur="codeApplication" />
	    <sofi-html:select 
	        libelle="infra_sofi.libelle.gestion_referentiel.action.livraison"
	        property="filtreOperateur"
	        cache="listeOperateurFiltreRecherche"
	        value=""> 
	      <sofi-html:text property="versionLivraison" size="5" maxlength="5" />    
	    </sofi-html:select>
	    <sofi:taille valeur="${LISTE_FACETTE_APPLICATION}" var="nbListeFacette"/>         
      <c:if test="${nbListeFacette > 0}" >   
       <sofi-html:select
          property="codeFacette"
          libelle="infra_sofi.libelle.gestion_referentiel.champ.codeFacette"
        />
      </c:if>  
	    <sofi-html:text 
	        libelle="infra_sofi.libelle.gestion_referentiel.champ.cleAide" 
	        property="cleAide" size="80" />              
	  </table>
		<div class="barreBouton">
	   <sofi-html:bouton 
	      libelle="infra_sofi.libelle.gestion_referentiel.action.creer" 
	      href="referentiel.do?methode=creer" />
	   <sofi-html:submit 
	      libelle="infra_sofi.libelle.commun.action.rechercher" />
	  </div>
	  </sofi-html:form>
</fieldset>
<c:if test="${listeReferentiel != null}">
  <fieldset>
    <legend>
      <sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche" />
     </legend>
    <jsp:include page="ajax_liste_referentiel.jsp" />
  </fieldset>
</c:if>