<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi-liste:listeNavigation action="rechercheReferentiel.do?methode=rechercher"
                          nomListe="${listeReferentiel}"
                          triDefaut="1"
                          triDescendantDefaut="false"
                          class="tableresultat"
                          id="objetReferentiel"
                          trierPageSeulement="false"
                          ajax="true"
                          libelleResultat="infra_sofi.libelle.gestion_referentiel.compteur.element"
                          iterateurMultiPage="true"
                          colonneLienDefaut="1"
                          >
                          
  <sofi-liste:colonne property="nom" trier="true" 
                      libelle="infra_sofi.libelle.gestion_referentiel.entete.nom"
                      href="referentiel.do?methode=consulter"
                      paramId="seqObjetSecurisable"
                      paramProperty="id"
                      
                      />
                      
  <sofi-liste:colonne property="type" trier="true" libelle="infra_sofi.libelle.gestion_referentiel.entete.type" traiterCorps="true">
    <sofi:cache cache="listeTypeObjetSecurisable" valeur="${objetReferentiel.type}"/>
  </sofi-liste:colonne>
  
  <sofi-liste:colonne property="versionLivraison" trier="true" libelle="Livraison">
  </sofi-liste:colonne>
  
  <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${objetReferentiel.dateCreation}" var="dateCreation" />
  <sofi-liste:colonne property="dateCreation"
                      libelle="infra_sofi.libelle.commun.entete.creation" 
                      trier="true"
                      traiterCorps="true">  
    <c:out value="${dateCreation}"/>
  </sofi-liste:colonne> 
  
  <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${objetReferentiel.dateModification}" var="dateModification" />
  <sofi-liste:colonne property="dateModification"
                      libelle="infra_sofi.libelle.commun.entete.maj" 
                      trier="true"
                      traiterCorps="true">  
    <c:out value="${dateModification}"/>
  </sofi-liste:colonne>
  <sofi-liste:colonneLibre>
    <c:url var="url" value="rechercheReferentiel.do">
      <c:param name="methode" value="structure"/>
      <c:param name="id" 
               value="${objetReferentiel.id}"/>
    </c:url>
    <a href="<c:out value="${url}"/>" 
       title="<sofi:libelle identifiant="infra_sofi.libelle.gestion_referentiel.action.structure"/>">
      <img border="0" src="images/structure.gif" width="12" height="12"/>
    </a> 
  </sofi-liste:colonneLibre>

</sofi-liste:listeNavigation>  
