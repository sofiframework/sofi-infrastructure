<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
 
<sofi-liste:listeNavigation action="detailUtilisateurRole.do?methode=afficherRoleAutorises"
                          nomListe="${GESTION_UTILISATEUR_LISTE_ROLE_AUTORISE}"
                          class="tableresultat"
                          id="utilisateurRole"
                          trierPageSeulement="true"
                          libelleResultat="infra_sofi.libelle.gestion.utilisateur.action.liste.role"
                          colgroup="10%,20%,38%,10%,10%,6%,6%"
                          messageListeVide="infra_sofi.information.gestion_utilisateur.aucun_role_autorise"
                          ajax="true">
  <sofi-liste:colonne property="codeRole" 
                      libelle="infra_sofi.libelle.gestion.utilisateur.champ.code_role"/>
  <sofi-liste:colonneLibre libelle="infra_sofi.libelle.gestion_domaine_valeur.entete.nom">
    ${utilisateurRole.role.nom}
  </sofi-liste:colonneLibre>
  <sofi-liste:colonneLibre libelle="infra_sofi.libelle.gestion_domaine_valeur.entete.description">
    ${utilisateurRole.role.description}
  </sofi-liste:colonneLibre>
  <sofi-liste:colonne property="dateDebutActivite" 
                      libelle="infra_sofi.libelle.gestion.utilisateur.champ.date_debut"
                      trier="true" />
  <sofi-liste:colonne property="dateFinActivite" 
                      libelle="infra_sofi.libelle.gestion.utilisateur.champ.date_fin"
                      trier="true" />
  <sofi-liste:colonne property="codeClient" 
                      libelle="infra_sofi.libelle.gestion.utilisateur.champ.code_client"
                      trier="true" />
  <sofi-liste:colonneLibre libelle="">
    <c:url var="urlSupressionProfil" value="detailUtilisateurRole.do" >
      <c:param name="methode" value="supprimer"/>
      <c:param name="id" value="${utilisateurRole.id}" />
      <c:param name="codeRole" value="${utilisateurRole.codeRole}" />
    </c:url>
    <sofi:message identifiant="infra_sofi.confirmation.gestion_utilisateur.retirer_role_utilisateur"
                  parametre1="${utilisateurRole.role.nom}"
                  parametre2="${utilisateurForm.prenom}"
                  parametre3="${utilisateurForm.nom}"
                  var="messsageConfirmationDissociation" />
    <sofi-html:bouton libelle=""
                      styleClass="boutonSuppression"  
                      href="${urlSupressionProfil}" 
                      messageConfirmation="${messsageConfirmationDissociation}" 
                      aideContextuelle="infra_sofi.aide.gestion_utilisateur.action.supprimer" 
                      notifierModificationFormulaire="true"/>  
    <c:url var="urlModificationProfil" value="detailRoleAutorise.do" >
      <c:param name="methode" value="afficherPeriodeActivation"/>
      <c:param name="id" value="${utilisateurRole.id}" />
    </c:url>
    <sofi-html:bouton libelle="" 
                      styleClass="boutonEdition" 
                      href="${urlModificationProfil}" 
                      aideContextuelle="Consulter le détail du rôle autorisé." />
     <jsp:useBean id="dateCourante" class="java.util.Date" />
  </sofi-liste:colonneLibre>
</sofi-liste:listeNavigation>