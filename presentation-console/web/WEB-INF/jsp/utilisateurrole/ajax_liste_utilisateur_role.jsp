<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi-liste:listeNavigation action="rechercheUtilisateurRole.do?methode=rechercher"
                          nomListe="${LISTE_RECHERCHE_UTILISATEUR_ROLE}"
                          triDefaut="1"
                          triDescendantDefaut="false"
                          class="tableresultat"
                          id="objetJava"
                          trierPageSeulement="false"
                          ajax="true"
                          libelleResultat="infra_sofi.libelle.gestion_utilisateur_role.element.utilisateur"
                          iterateurMultiPage="true"
                          colgroup="50%,50%"
                          colonneLienDefaut="1"
                          >
                          
  <sofi-liste:colonne property="nom" trier="true" 
                      libelle="infra_sofi.libelle.gestion_utilisateur_role.colonne.nom"
                      href="detailUtilisateurRole.do?methode=consulter"
                      paramId="codeUtilisateur"
                      paramProperty="codeUtilisateur"/>
                      
  <sofi-liste:colonne property="prenom" 
                      trier="true" 
                      libelle="infra_sofi.libelle.gestion_utilisateur_role.colonne.prenom"/>
</sofi-liste:listeNavigation>