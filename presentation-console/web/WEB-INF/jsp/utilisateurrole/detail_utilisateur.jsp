<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>
<script>
  function changementApplication() {
    var url = 'detailUtilisateurRole.do?methode=modifierApplication&codeApplication=' 
        + $('#codeApplication').val();
    go(url);//Permet de faire le confirm si le form est modifié
  }
</script>
<sofi:taille var="taille" valeur="${LISTE_APPLICATIONS_UTILISATEUR}" />
<c:choose>
<c:when test="${taille == 0}">
  <sofi:libelle identifiant="infra_sofi.libelle.commun.aucun_application" />
</c:when>
<c:otherwise>
<sofi-html:form action="/detailUtilisateurRole.do?methode=enregistrer" transactionnel="true" focusAutomatique="false">
  <fieldset>
    <app:legende libelle="infra_sofi.libelle.gestion_utilisateur_role.section.detail_utilisateur" />
    <c:url var="lienModificationApplication" value="/detailUtilisateurRole.do">
      <c:param name="methode" value="modifierApplication"/>
    </c:url>
    <app:tableFormulaire>
      <sofi-html:select property="codeApplication" 
                        libelle="infra_sofi.libelle.gestion_utilisateur_role.champ.systeme"
                        collection="${LISTE_APPLICATIONS_UTILISATEUR}"
                        classeDecorator="org.sofiframework.infrasofi.presentation.decorator.ApplicationSelectDecorator"
                        transactionnel="false" onchange="changementApplication();" />
      <sofi:affichageEL libelle="infra_sofi.libelle.gestion_utilisateur_role.champ.nom" 
        valeur="${detailUtilisateurForm.objetTransfert.prenom} ${detailUtilisateurForm.objetTransfert.nom}" />
      </app:tableFormulaire>
      <sofi-nested:nest property="utilisateurApplication">
        <app:tableFormulaire>
           <sofi-nested:select 
            libelle="infra_sofi.libelle.gestion_utilisateur.champ.statut"
            property="codeStatut" 
            cache="listeStatutActivation" />
           
          <sofi:cache cache="listeTypeUtilisateur" var="listeTypeUtilisateur" />
          
          <c:if test="${listeTypeUtilisateur != null}" >
	          <sofi-nested:select 
	            libelle="infra_sofi.libelle.gestion_utilisateur.champ.type_utilisateur"
	            property="codeTypeUtilisateur" 
	            cache="listeTypeUtilisateur" ligneVide="true" />
          </c:if>
          
          <sofi:cache cache="listeTypeOrganisation" var="listeTypeOrganisation" />
          <c:if test="${listeTypeOrganisation != null}" >
	          <sofi-nested:select libelle="infra_sofi.libelle.gestion_utilisateur.champ.type_organisation"
	                              property="codeTypeOrganisation" 
	                              ligneVide="true"
	                              cache="listeTypeOrganisation" />
	          <sofi-nested:text libelle="infra_sofi.libelle.gestion_utilisateur.champ.organisation"
                              property="codeOrganisation" />
	        </c:if>
    
        </app:tableFormulaire>
        <app:barreBouton>
          <sofi-html:submit libelle="infra_sofi.libelle.gestion_utilisateur.bouton.sauvegarder_utilisateur_application" />
        </app:barreBouton>
      </sofi-nested:nest>
      <br/>
      <div id="roleParApplication">
        <jsp:include page="ajax_liste_application_role.jsp" />
      </div>
  </fieldset>
</sofi-html:form>
</c:otherwise>
</c:choose>