<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<fieldset>
  <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion_utilisateur_role.section_recherche"/></legend>
  <sofi-html:form action="/rechercheUtilisateurRole.do?methode=rechercher" transactionnel="false" >
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <sofi-html:text libelle="infra_sofi.libelle.gestion_utilisateur_role.champ.nom" 
                      property="nom" size="40"/>
      <sofi-html:text libelle="infra_sofi.libelle.gestion_utilisateur_role.champ.prenom" 
                      property="prenom" size="40"/>
      <sofi-html:select 
          property="codeApplication" 
          libelle="infra_sofi.libelle.gestion_utilisateur_role.champ.application"
          collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          classeDecorator="org.sofiframework.infrasofi.presentation.decorator.ApplicationSelectDecorator"
          ligneVide="true"
          href="rechercheRoleReferentiel.do?methode=chargerListeRole"
          nomParametreValeur="codeApplication"
          nomAttributDestinationAjax="codeRole" />
      <sofi-html:select 
          property="codeRole" 
          libelle="infra_sofi.libelle.gestion_utilisateur_role.champ.role"
          collection="${LISTE_ROLE}"
          proprieteEtiquette="nom"
          proprieteValeur="code"
          ligneVide="true"
          style="width:250px;">
          <sofi-html:checkbox libelle="infra_sofi.libelle.gestion_utilisateur_role.champ.ne_contient_pas" 
                              property="codeRoleNonInclus" 
                              libelleAssocie="true"
                              genererLigne="false"/>
      </sofi-html:select>
    </table>
    <div class="barreBouton">
      <sofi-html:submit libelle="infra_sofi.libelle.commun.action.rechercher"/>
    </div>
  </sofi-html:form>
</fieldset>

<c:if test="${LISTE_RECHERCHE_UTILISATEUR_ROLE != null}">
  <fieldset>
    <legend><sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche"/></legend>
      <jsp:include page="ajax_liste_utilisateur_role.jsp" />
  </fieldset>
</c:if>