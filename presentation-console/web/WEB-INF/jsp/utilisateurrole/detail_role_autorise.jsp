<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<sofi-html:form action="/detailRoleAutorise.do?methode=enregistrerPeriodeActivation" transactionnel="true">
  <fieldset>
    <legend>
      <sofi:libelle identifiant="infra_sofi.libelle.gestion_utilisateur.bloc.role_utilisateur" />
    </legend>
    <table border="0" cellspacing="0" cellpadding="0" class="tableaffichage">
      <tr>
        <th>
        <sofi:libelle identifiant="infra_sofi.libelle.gestion_utilisateur_role.champ.nom" />
        </th>
        <td>
        ${utilisateurRoleForm.objetTransfert.nomCompletUtilisateur}
        </td>
      </tr>
    </table>
    <br/>
    <table border="0" cellspacing="0" cellpadding="0" class="tableaffichage">
      <sofi-html:text property="codeRole" libelle="infra_sofi.libelle.gestion_utilisateur.champ.code_role"  
                      affichageSeulement="true"/>
      <tr>
        <th>
        <sofi:libelle identifiant="infra_sofi.libelle.gestion_utilisateur.champ.nom_role" />
        </th>
        <td>
        ${utilisateurRoleForm.objetTransfert.role.nom}
        </td>
      </tr>
      <tr>
        <th>
        <sofi:libelle identifiant="infra_sofi.libelle.gestion_utilisateur.champ.description_role" />
        </th>
        <td>
        ${utilisateurRoleForm.objetTransfert.role.description}
        </td>
      </tr>
      <tr>
        <th>
        <sofi:libelle identifiant="infra_sofi.libelle.gestion_utilisateur.champ.code_client" />
        </th>
        <td>
          <sofi-html:text property="codeClient" size="20" maxlength="50" 
            affichageSeulement="${codeClientPilote != null}"/>
        </td>
      </tr>
    </table>
    <br/>
    <div>
      <fieldset class="sous_section"><legend><sofi:libelle identifiant="Détail de la période d'activation"/></legend>
        <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
          <sofi-html:date property="dateDebutActivite" libelle="infra_sofi.libelle.gestion_utilisateur.champ.date_debut" />
          <sofi-html:date property="dateFinActivite" libelle="infra_sofi.libelle.gestion_utilisateur.champ.date_fin" />
          <sofi-html:textarea property="descriptionPeriodeActivite" 
                              libelle="infra_sofi.libelle.gestion.securite.champ.description"
                              rows="5" cols="90" />
        </table>
      </fieldset>
    </div>
    <div class="barreBouton">
      <sofi-html:submit libelle="infra_sofi.libelle.gestion_utilisateur.action.enregistrer"/>
    </div>
  </fieldset>
</sofi-html:form>