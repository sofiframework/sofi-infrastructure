<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi-liste:listeNavigation action="detailUtilisateurRole.do?methode=afficherRoleAutorises"
                          nomListe="${GESTION_UTILISATEUR_LISTE_ROLE_DISPONIBLE}"
                          class="tableresultat"
                          id="role"
                          trierPageSeulement="true"
                          libelleResultat="infra_sofi.libelle.gestion.utilisateur.action.liste.role"
                          colgroup="10%,20%,64%,6%"
                          messageListeVide="infra_sofi.information.gestion_utilisateur.aucun_role_supplementaire">
  <sofi-liste:colonne property="codeRole" 
                      libelle="infra_sofi.libelle.gestion.utilisateur.champ.code_role" />
  <sofi-liste:colonne property="nom" 
                      libelle="infra_sofi.libelle.gestion_domaine_valeur.entete.nom" />
  <sofi-liste:colonne property="description" 
                      libelle="infra_sofi.libelle.gestion_domaine_valeur.entete.description"
                      traiterCorps="true">
       <sofi:texte valeur="${role.description}" convertirRetourChariotEnSautLigneHTML="true" />
  </sofi-liste:colonne>
  <sofi-liste:colonneLibre libelle="">
    <c:url var="lienAjouterRole" value="detailUtilisateurRole.do?methode=ajouter">
      <c:param name="codeRole" value="${role.codeRole}" />
      <c:param name="codeUtilisateur" value="${utilisateurForm.codeUtilisateur}" />
      <c:param name="codeApplication" value="${systeme_selectionne}" />
    </c:url>
    <sofi-html:bouton libelle="" 
                      styleClass="boutonAjouter"
                      href="${lienAjouterRole}"
                      notifierModificationFormulaire="true"
                      aideContextuelle="Ajouter ce rôle dans la liste des rôles autorisés."/>
  </sofi-liste:colonneLibre>
</sofi-liste:listeNavigation>