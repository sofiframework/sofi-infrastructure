<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<fieldset class="sous_section">
  <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion.securite.section.role.autorise"/> </legend>
  <jsp:include page="liste_role_autorise.jsp"/>
</fieldset>
<fieldset class="sous_section">
  <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion.securite.section.role.disponible"/></legend>
  <jsp:include page="liste_role_disponible.jsp"/>
</fieldset>