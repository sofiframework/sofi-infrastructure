<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>

<!-- Onglets principaux -->
  <div id="zoneonglet">
    <div id="enteteonglet">
      <sofi-menu:menu nom="Velocity" gabaritVelocity="/org/sofiframework/presentation/velocity/gabarit/onglets2.vm">
        <ul>
          <c:forEach var="onglet" items="${listeOnglets}">
            <sofi-menu:section identifiant="${onglet.identifiant}"/>
          </c:forEach>
        </ul>
      </sofi-menu:menu>
    </div>
  </div>
</div> 

<!-- Les onglets de 2ieme niveau -->
<c:if test="${listeOnglets2 != null}" >
  <div id="sous_onglet">
    <sofi-menu:menu nom="Velocity" gabaritVelocity="/org/sofiframework/presentation/velocity/gabarit/sous_onglets.vm" >
      <ul>
        <c:forEach var="onglet2" items="${listeOnglets2}" varStatus="compteur">
          <sofi-menu:section identifiant="${onglet2.identifiant}" niveauOnglet="2" noMenu="${compteur.index}" />
        </c:forEach>
      </ul>
    </sofi-menu:menu>
    <div class="clear"></div>
  </div>
</c:if>

<sofi:barreStatut />

<!-- Fin onglets -->