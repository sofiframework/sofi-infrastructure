<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>

<div id="logo_general">
  <a href="index.html" id="logo">Sofi - </a>
	<sofi-html:link styleId="fermerSession" styleClass="blanc"
		href="authentification.do?methode=quitter"
		libelle="infra_sofi.libelle.commun.fermer_session"
		messageConfirmation="infra_sofi.confirmation.commun.fermer_session" />
	<div id="zone_utilisateur">
		<p class="utilisateur"><sofi:libelle identifiant="infra_sofi.libelle.commun.bienvenue" /> <span> <c:out
			value="${utilisateur.prenom}" />&nbsp;<c:out value="${utilisateur.nom}" />
		</span></p>
		<ul>
			<li class="btn_aide"><sofi:aide styleId="aideGeneral" ajax="true"
				styleClass="aideEnLigne" largeurfenetre="655" hauteurfenetre="400">
				<sofi:libelle identifiant="infra_sofi.libelle.commun.aide_en_ligne" />
			</sofi:aide></li>
			
      <li class="btn_nbUtilisateur"><sofi:popup
        href="surveillance.do?methode=afficherUtilisateurEntete" ajax="true"
        largeurfenetre="250" hauteurfenetre="150" ajustementPositionX="-220"
        ajustementPositionY="30" styleId="fenetreNbUtilisateur">
        <span id="idNbUtilisateurEnLigne"> <c:out
          value="${nbUtilisateurEnLigne}" /> </span>
      </sofi:popup></li>			
		</ul>
	</div>
</div>

<%-- 
<sofi:div id="zoneRafrachissementUtilisateurEntete"
	href="surveillance.do?methode=rafraichirUtilisateurEntete"
	tempsRafraichissement="5" />
--%>

<div class="clear"></div>

<!-- Menu -->
<div id="zonemenu">
	<ul id="nav">
		<sofi-menu:menu nom="Velocity"
			gabaritVelocity="/org/sofiframework/presentation/velocity/gabarit/menu_deroulant_css.vm">
			<c:forEach var="premierNiveau"
				items="${menuUtilisateur.menuPremierNiveau}">
				<sofi-menu:section identifiant="${premierNiveau.identifiant}" />
			</c:forEach>
		</sofi-menu:menu>
		<li id="sectionfinmenu">&nbsp;</li>
	</ul>
	<div class="clear"></div>
</div>
<!-- Fin menu -->
