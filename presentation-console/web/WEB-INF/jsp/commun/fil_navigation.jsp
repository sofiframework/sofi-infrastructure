<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-tiles" prefix="sofi-tiles"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-menu" prefix="sofi-menu"%>

<!-- fil_navigation -->
<p id="localisation">
   &nbsp;&nbsp;&nbsp;
  <sofi-menu:menu nom="Velocity" gabaritVelocity="/org/sofiframework/presentation/velocity/gabarit/fil_navigation.vm">
    <c:forEach var="composantMenu" items="${utilisateur.filNavigation}">
      <sofi-menu:section identifiant="${composantMenu.identifiant}" />
    </c:forEach>
  </sofi-menu:menu>
  <sofi-html:link
    styleClass="retour_tache_precedente"
    accesskey="${accessKey['tacheprecedente']}"
    appliquerAdresseRetour="true" 
    libelle="payment.label.common.back_to_previous_page" 
    styleId="zone_retour_tache_precedente"
    activerConfirmationModificationFormulaire="true" />
</p>
