<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%> 

<sofi-liste:listeNavigation action="rechercheMessage.do?methode=rechercher"
                            id="message"
                            nomListe="${listeMessage}"
                            colgroup="35%,35%,10%,10%,10%"
                            triDefaut="4"
                            triDescendantDefaut="true"
                            class="tableresultat"
                            ajax="true"
                            iterateurMultiPage="true"
                            libelleResultat="infra_sofi.libelle.gestion_message.compteur.element.messages"
                            colonneLienDefaut="1"
                            >
  <sofi-liste:colonne property="cleMessage" href="message.do?methode=consulter" 
                      paramId="id" paramProperty="id" trier="true" 
                      libelle="infra_sofi.libelle.gestion_message.entete.identifiant" 
                      sautLigneNombreCararactereMaximal="60"
                      sautLigneCararactereSeparateur="." />                      
  <sofi-liste:colonne property="texteMessage" trier="true" 
                      libelle="infra_sofi.libelle.gestion_message.entete.texte" />  
  <sofi-liste:colonne property="codeClient" libelle="infra_sofi.libelle.gestion_message.entete.client" trier="true" />                                                                
  <sofi-liste:colonne property="dateCreation" trier="true" traiterCorps="true"
                      libelle="infra_sofi.libelle.commun.entete.creation">  
    <fmt:formatDate value="${message.dateCreation}" pattern="yyyy-MM-dd HH:mm:ss" />
  </sofi-liste:colonne> 
  <sofi-liste:colonne property="dateModification" trier="true" traiterCorps="true"
                      libelle="infra_sofi.libelle.commun.entete.maj">  
    <fmt:formatDate value="${message.dateModification}" pattern="yyyy-MM-dd HH:mm:ss" />
  </sofi-liste:colonne>   
</sofi-liste:listeNavigation>  
