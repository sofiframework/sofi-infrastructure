<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<c:url var="urlDetail" value="/message.do"/>

<!-- Section filtre formulaire -->
<sofi-html:form action="/rechercheMessage.do?methode=rechercher#listeMessage" 
                transactionnel="false" focus="texte">
  <fieldset class="formulaire">
  <legend><sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.criteres.recherche"/></legend>
    <table cellpadding="0" cellspacing="0" class="tableformulaire">
      <sofi-html:text property="texteMessage" size="70" maxlength="2000"
        libelle="infra_sofi.libelle.gestion_message.champ.texte">
        <sofi-html:select property="codeLangue" cache="listeLocale" />
      </sofi-html:text>
      
      <sofi-html:text property="cleMessage" size="70" maxlength="300"
        libelle="infra_sofi.libelle.gestion_message.champ.identifiant" /> 
        
      <sofi-html:select property="codeSeverite" cache="listeTypeMessage"
        ligneVide="true" obligatoire="false"
        libelle="infra_sofi.libelle.gestion_message.champ.codeSeverite" />
        
      <sofi-html:select property="codeApplication" ligneVide="true"
        libelle="infra_sofi.libelle.gestion_message.champ.codeApplication"
        collection="${LISTE_APPLICATIONS_UTILISATEUR}"
        proprieteEtiquette="[nom] ([codeApplication])"
        proprieteValeur="codeApplication"
        href="rechercheMessage.do?methode=fixerApplication" ajax="true"
        nomParametreValeur="codeApplication" />

      <sofi-html:select property="codeClient" ligneVide="true"
        libelle="infra_sofi.libelle.gestion_parametre.codeClient"
        collection="${LISTE_CLIENT}"
        classeDecorator="org.sofiframework.infrasofi.presentation.decorator.ClientSelectDecorator" />
        
       <sofi-html:text property="reference" size="30" maxlength="100"
                      libelle="infra_sofi.libelle.gestion_message.champ.reference" />
    </table>
    
    <div class="barreBouton">
      <sofi-html:bouton libelle="infra_sofi.libelle.gestion_message.action.creer" 
                        href="${urlDetail}?methode=ajouter"/>
      <sofi-html:submit libelle="infra_sofi.libelle.commun.action.rechercher"/>  
    </div>
  </fieldset>
</sofi-html:form>

<!-- Section liste de navigation -->
<c:if test="${listeMessage != null}">
  <fieldset>
    <legend><sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche"/></legend>
    <jsp:include page="ajax_liste_message.jsp" />    
  </fieldset>
</c:if>

<script>
  $("#codeClient").select2({containerCssClass: "select_grand"});
</script>