<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<c:url var="urlDetail" value="/message.do"/>
<c:url var="urlRecherche" value="/rechercheMessage.do"/>

<p align="right" class="champsobligatoire">
  <span class="ind_obligatoire">*</span> 
  <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire"/>
</p>

<!-- Section formulaire -->
<sofi-html:form action="message.do?methode=enregistrer" transactionnel="true">
  <!-- Configurer l'URL de confirmation d'enregistrement d'un produit -->
  <c:url value="message.do?methode=enregistrer" var="urlConfirmationEnregistrement">
    <c:param name="confirmation" value="true"/>
  </c:url>
  <!-- Specifier un message de confirmation associer a  un href -->
  <sofi:message identifiant="confirmation.test" hrefConfirmation="${urlConfirmationEnregistrement}" />
  <fieldset class="formulaire">
    <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion_message.bloc.detail"/></legend>
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <sofi-html:text property="cleMessage" obligatoire="true" size="100" maxlength="300"
                      libelle="infra_sofi.libelle.gestion_message.champ.identifiant" />
      <sofi-html:select property="codeSeverite" cache="listeTypeMessage" 
                        ligneVide="true" obligatoire="true"
                        libelle="infra_sofi.libelle.gestion_message.champ.codeSeverite"/>
      <sofi-html:select 
        property="codeApplication" 
        ligneVide="false"
        libelle="infra_sofi.libelle.gestion_referentiel.champ.codeApplication" 
        obligatoire="true"
        collection="${LISTE_APPLICATIONS_UTILISATEUR}"
        proprieteEtiquette="[nom] ([codeApplication])"
        proprieteValeur="codeApplication" />
      <sofi-html:select property="codeClient" ligneVide="true"
                      libelle="infra_sofi.libelle.gestion_parametre.codeClient"
                      collection="${LISTE_CLIENT}"
                      classeDecorator="org.sofiframework.infrasofi.presentation.decorator.ClientSelectDecorator" />            
      <sofi-html:text property="reference" size="30" maxlength="100"
                      libelle="infra_sofi.libelle.gestion_message.champ.reference" />
    </table>
    
    <fieldset class="cadreNavigationSousSection">
      <legend>Contenu par langue</legend>  
        <div class="zoneNavigationSousSection">
          <ul>
            <sofi:cache cache="listeLocale" var="listeLocaleApplication" />
            <c:forEach items="${listeLocaleApplication }" var="localeApplication">
              <c:set var="stylelangue" value="langue" />
              <c:if test="${localeApplication.key == messageForm.codeLangue}">
                <c:set var="stylelangue" value="langueSelectionne" />
              </c:if>
              <li>
                <sofi-html:link styleClass="${stylelangue}"
			                          styleId="langue_${localeApplication.key}"
			                          libelle="${localeApplication.value}"
			                          href="message.do?methode=changerLangue&codeLangue=${localeApplication.key}"
			                          ajax="true"
			                          idRetourMultipleAjax="true"
			                          activerConfirmationModificationFormulaire="true"
			                          ajaxJSApresChargement="initialiserLangue();setStyleClass('langue_${localeApplication.key}','langueSelectionne');modifierContenuCKEditor('texteMessage');" />  
              </li>
            </c:forEach>
        </ul>
      </div>
      </fieldset>
      <div class="fond_sous_section" >
		      <table border="0" cellspacing="0" cellpadding="0" class="tableformulaireinterne">
		        <sofi-html:textarea property="texteMessage" 
		                            libelle="infra_sofi.libelle.gestion_message.champ.texte" 
		                            obligatoire="true" 
		                            rows="5" 
		                            cols="90" 
		                            maxlength="2000"/>
		        <jsp:include page="../trace.jsp" />
		       </table>
		    </center>
		  </div>
		  <div class="barreBouton">
		    <c:if test="${!formulaireEnCours.nouveauFormulaire}">
		    
		      <input type="hidden" name="ancienneCleMessage" 
		             value="<c:out value="${messageForm.ancienneCleMessage}"/>"/>
		      <input type="hidden" name="ancienCodeLangue" 
		             value="<c:out value="${messageForm.ancienCodeLangue}"/>"/>
		      <!-- Configurer un bouton qui permet de vider la cache du message -->
		      <sofi-html:bouton  href="message.do?methode=initialiserMessage" 
		          libelle="Initialiser la cache" />  
		          
          <sofi-html:bouton 
                            libelle="infra_sofi.message.gestion_message.action.copier" 
                            href="${urlDetail}?methode=copier" 
                            styleClass="bouton" />
                            		                  
		      <sofi-html:submit libelle="infra_sofi.libelle.gestion_message.action.creer" 
		                        href="${urlDetail}?methode=ajouter"
		                        ignorerValidation="true"/>
		                        		                        
		      <!-- Configurer l'URL permettant de supprimer un produit -->
		      <c:url var="urlSupprimer" value="/message.do?methode=supprimer"/>
		      <!-- Configurer un message de confirmation avec variable dynamique -->
		      <c:if test="${!messageForm.nouveauFormulaire}">
			      <sofi-html:bouton  href="${urlSupprimer}" 
			          libelle="infra_sofi.libelle.gestion_message.action.supprimer" 
			          messageConfirmation="infra_sofi.confirmation.gestion_message.suppression" />
		      </c:if>
		    </c:if>
		    <sofi-html:submit libelle="infra_sofi.libelle.gestion_message.action.enregistrer" 
		                      property="enregistrer" 
		                      styleClass="bouton"/>
		  </div>  
   </fieldset> 
</sofi-html:form>

<script>
  $("#codeClient").select2({containerCssClass: "select_grand"});
</script>