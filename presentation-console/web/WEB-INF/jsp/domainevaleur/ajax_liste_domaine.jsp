<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<sofi-liste:listeNavigation action="rechercheDomaine.do?methode=rechercher"
                          nomListe="${LISTE_RECHERCHE_DOMAINE_VALEUR}"
                          triDefaut="1"
                          triDescendantDefaut="false"
                          class="tableresultat"
                          id="domaine"
                          trierPageSeulement="false"
                          ajax="true"
                          libelleResultat="infra_sofi.libelle.gestion_domaine_valeur.definition.compteur.element"
                          iterateurMultiPage="true"
                          colgroup="40%,40%,20%"
                          colonneLienDefaut="1">
  <sofi-liste:colonne property="nom" trier="true" 
                      libelle="infra_sofi.libelle.gestion_domaine_valeur.entete.nom"
                        href="domaine.do?methode=consulter"
                        paramId="codeApplication,nom,valeur,codeLangue"
                        paramProperty="codeApplication,nom,valeur,codeLangue"/>
                                      
  <sofi-liste:colonne property="description" trier="true" 
                      libelle="infra_sofi.libelle.gestion_domaine_valeur.entete.description" />   
  <sofi-liste:colonne libelle="Tri" 
                      property="codeTypeTri" cache="listeTypeTri" trier="true" />
</sofi-liste:listeNavigation>  