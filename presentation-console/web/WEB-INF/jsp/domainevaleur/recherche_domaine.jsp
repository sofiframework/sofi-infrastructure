<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<script language="javascript">
  function traiterAffichageDivAjaxAsynchrone(lienAjax, idChamp, autre, aEvaluer) {
    idChamp = idChamp.split('[').join('\\[');
    idChamp = idChamp.split(']').join('\\]');
    idChamp = idChamp.split('.').join('\\.');
    idChamp = idChamp.split('?').join('\\?');
    idChamp = idChamp.split('&').join('\\&');
    idChamp = idChamp.split('=').join('\\=');
    ajaxUpdate(lienAjax, idChamp, function(){
      if (aEvaluer) {
        eval(aEvaluer);
      }
    });
  }

  function telechargement(){
    document.getElementById('div_document_pdf').innerHTML = '';
    window.location.href = 'rechercheDomaine.do?methode=ecrireReponsePdf';
  }

  function updateParent() {
    soumettre($('#rechercher'));
  }

  function soumettre(element) {
    $('#attenteRecherche').html("<img src='images/roller.gif'/>");

    var formulaire = $(element).parents('form');
    var donnees = formulaire.serializeArray();
    var action = formulaire[0].action;

    if (action && action.indexOf('&ajax=true') == -1) {
      action += '&ajax=true;';
    }

    var retour = $.post(action, donnees, function(resultat) {
      $('#formulaire').html(resultat);
      ajaxUpdate('rechercheDomaine.do?methode=afficherResultat&ajax_rafraichir=true', 'resultat');
    });
  }
</script>
<div id="formulaire">
  <jsp:include page="ajax_formulaire_recherche.jsp" />
</div>
<div id="resultat">
  <jsp:include page="ajax_resultat.jsp" />
</div>
<div id="fenetreValeur"></div>