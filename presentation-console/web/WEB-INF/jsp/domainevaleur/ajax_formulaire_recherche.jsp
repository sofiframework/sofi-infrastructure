<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
  <sofi-html:form action="/rechercheDomaine.do?methode=rechercher" transactionnel="false" >
    <fieldset>
      <legend><sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.criteres.recherche"/></legend>
        <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">     
          <sofi-html:select property="codeApplication" ligneVide="false" 
                            libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.systeme"
                            collection="${LISTE_APPLICATIONS_UTILISATEUR}" ajax="true" 
                            proprieteEtiquette="[nom] ([codeApplication])"
                            proprieteValeur="codeApplication"
                            href="rechercheDomaine.do?methode=fixerApplication" nomParametreValeur="codeApplication" />
          <sofi-html:select property="codeClient" ligneVide="true"
                          libelle="infra_sofi.libelle.gestion_parametre.codeClient"
                          collection="${LISTE_CLIENT}"
                          classeDecorator="org.sofiframework.infrasofi.presentation.decorator.ClientSelectDecorator" />                              
          <sofi-html:select ligneVide="false" property="typeDomaine" cache="listeTypeDomaine" 
                            libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.type" />
          <sofi-html:text libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.nom" 
                          property="nom" size="80" maxlength="100"/>
          <sofi-html:text libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.valeur" 
                          property="valeur" size="80" maxlength="100" />
          <sofi-html:select property="codeLangue"   cache="listeLocale" 
                            libelle="infra_sofi.libelle.gestion_message.champ.codeLangue" />

          <sofi-html:select property="actif" cache="listeActiviteDomaineValeur" ligneVide="true" 
                            libelle="Actif : " />
        </table>
        <div class="barreBouton">
          <span id="attenteRecherche"></span>
          <c:url var="lienCreer" value="/domaine.do">
            <c:param name="methode" value="ajouter"/>
            <c:param name="codeApplication" value="${domaineForm.codeApplication}"/>
          </c:url>
          <sofi-html:bouton libelle="infra_sofi.libelle.gestion_domaine_valeur.action.creer" href="${lienCreer}"/>
          <sofi-html:bouton libelle="infra_sofi.libelle.commun.action.rechercher" onclick="soumettre(this)" styleId="rechercher" />
        </div>
    </fieldset>
  </sofi-html:form>
  
  <script>
    $("#codeClient").select2({containerCssClass: "select_grand"});
  </script>