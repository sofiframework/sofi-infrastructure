<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<c:if test="${LISTE_RECHERCHE_DOMAINE_VALEUR != null}">
  <fieldset>
    <legend><sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche"/></legend>
      <jsp:include page="ajax_liste.jsp" />
      <div class="barreBouton">
        <sofi-html:submit libelle="infra_sofi.libelle.commun.action.exporter_pdf" 
                          href="rechercheDomaine.do?methode=exporterEnPdf"
                          libelleMessageAttente="infra_sofi.libelle.commun.generation_pds_en_cours"
                          positionXMessageAttente="-450"
                          positionYMessageAttente="-200" />
      </div>
  </fieldset>
</c:if>