<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<sofi:barreStatut />

<p align="right" class="champsobligatoire">
  <span class="ind_obligatoire">*</span> 
  <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire"/>
</p>
<fieldset>
  <legend>
    <sofi:libelle identifiant="Valeur du domaine"/>
  </legend>
  <sofi-html:form action="valeur.do?methode=enregistrer" transactionnel="true" focus="valeur">
    <sofi:parametreSysteme code="cleMessageAvertisementFormulaireModifie" 
                           var="messageConfirmation" />
    <sofi:message var="messageConfirmation" identifiant="${messageConfirmation}" />
    <sofi:message identifiant="infra_sofi.confirmation.gestion_domaine_valeur.suppression_domaine" 
		              parametre1="${valeurForm.nom}" 
		              var="messageConfirmationSuppression" />

    <div style="display:none;" id="messageConfirmationFormulaire">${messageConfirmation}</div>
    <div style="display:none;" id="messageConfirmationSuppression">${messageConfirmationSuppression}</div>
    
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <c:if test="${valeurForm.valeurParent != 'DEFINITION'}">
        <sofi-html:text libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.nom.parent" 
                        property="nomParent" 
                        size="100"
                        disabled="true">
          <c:url var="lienDetailDomaineParent" value="/valeur.do">
            <c:param name="methode" value="afficherDetail"/>
            <c:param name="codeApplication" value="${valeurForm.codeApplicationParent}"/>
            <c:param name="nom" value="${valeurForm.nomParent}"/>
            <c:param name="valeur" value="${valeurForm.valeurParent}"/>
          </c:url>
          <sofi-html:link href="${lienDetailDomaineParent}" libelle="Consulter le domaine parent"/>
        </sofi-html:text>
        <sofi-html:text libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.valeur.parent" 
                        property="valeurParent" 
                        disabled="true"/>
      </c:if>
      <sofi-html:text libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.systeme" 
                      property="codeApplication" 
                      affichageSeulement="true"/>
      <sofi-html:select property="codeClient" ligneVide="true"
                      libelle="infra_sofi.libelle.gestion_parametre.codeClient"
                      collection="${LISTE_CLIENT}"
                      classeDecorator="org.sofiframework.infrasofi.presentation.decorator.ClientSelectDecorator" />          
      <sofi-html:text libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.nom" 
                    property="nom" 
                    obligatoire="true" 
                    size="40"
                    maxlength="100" />
      <c:if test="${domaineForm.codeTypeTri == 'O'}">
        <sofi-html:text libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.ordreAffichage" 
                        property="ordreAffichage" 
                        size="4" />
      </c:if>
      <sofi-html:text libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.valeur" 
                      property="valeur" 
                      obligatoire="true" 
                      size="40"
                      maxlength="100" />
    </table>
    <p>
    <center>
		<sofi:cache cache="listeLocale" var="listeLocaleApplication" />
		<c:forEach items="${listeLocaleApplication }" var="localeApplication">
			<c:set var="stylelangue" value="langue" />
			<c:if test="${localeApplication.key == valeurForm.codeLangue}">
			 <c:set var="stylelangue" value="langueSelectionne" />
			</c:if>
			<sofi-html:link styleClass="${stylelangue}"
			               styleId="langue_${localeApplication.key}"
			               libelle="${localeApplication.value}"
			               href="javascript:changerLangue('${valeurForm.codeApplication}','${valeurForm.nom}','${valeurForm.valeur}','${localeApplication.key}');"
			              
			               />  
		</c:forEach>   
    </center><br/>
    </p>
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaireinterne"> 
      <sofi-html:textarea libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.description" 
                        property="description"
                        rows="4" 
                        cols="80" 
                        maxlength="300"/>
        <tr>
          <th>
            <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.creePar" />
          </th>
          <td>
            ${valeurForm.creePar} ${valeurForm.dateCreation}
          </td>
        </tr>
        <tr>
          <th>
            <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.modifiePar" />
          </th>
          <td>
            ${valeurForm.modifiePar} ${valeurForm.dateModification}
          </td>
        </tr>
        </table>
      
      <div class="barreBouton">
      <c:if test="${!formulaireEnCours.nouveauFormulaire}"> 
        <sofi-html:bouton 
          onclick="supprimerValeur('valeur.do?methode=supprimer', this);" 
          libelle="infra_sofi.libelle.gestion_domaine_valeur.action.supprimer" 
        />
        <sofi-html:bouton 
          libelle="infra_sofi.libelle.gestion_domaine_valeur.action.creer_valeur_domaine" 
          onclick="creerValeur('${valeurForm.codeApplication}','${valeurForm.nom}')"
        />
      </c:if>
      <button type="submit" class="bouton">
        <span><sofi:libelle identifiant="infra_sofi.libelle.gestion_domaine_valeur.action.enregistrer"/></span>
      </button>
      
	    <sofi-html:bouton libelle="infra_sofi.libelle.commun.fermer"
	                         onclick="$('#fenetreValeur').dialog('close');" />
    </div>
  </sofi-html:form>
</fieldset>

<script>
  $("#codeClient").select2({containerCssClass: "select_grand"});
</script>