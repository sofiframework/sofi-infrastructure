<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>
  <script>
  function updateParent() {
    // mettre a jour la liste des valeurs
    ajaxUpdate('listeValeur.do?methode=rafraichir&ajax_rafraichir=true', 'listeValeurDomaine');
  }

  function monterValeur(index) {
    ajaxUpdate('listeValeur.do?methode=deplacer&ajax_rafraichir=true&type=monter&i=' + index, 'listeValeurDomaine');
  }

  function descendreValeur(index) {
    ajaxUpdate('listeValeur.do?methode=deplacer&ajax_rafraichir=true&type=descendre&i=' + index, 'listeValeurDomaine');
  }
  
  function selectionnerValeur(index) {
    ajaxUpdate('listeValeur.do?methode=selectionner&ajax_rafraichir=true&i=' + index, 'listeValeurDomaine');
  }
  
  function changementLangue(codeLangue) {
    ajaxUpdate('listeValeur.do?methode=changerLangue&ajax_rafraichir=true&codeLangue=' + codeLangue, 'listeValeurDomaine');
  }
  
  function supprimerValeur(index) {
    if (confirm('Supprimer?')) {
      ajaxUpdate('listeValeur.do?methode=supprimer&ajax_rafraichir=true&i=' + index, 'listeValeurDomaine');
    }
  }
  
  function effacerOrdres() {
    ajaxUpdate('listeValeur.do?methode=effacerOrdres&ajax_rafraichir=true', 'listeValeurDomaine');
  }
  
  function ordonner() {
    ajaxUpdate('listeValeur.do?methode=ordonner&ajax_rafraichir=true', 'listeValeurDomaine');
  }
  </script>
  <fieldset>
    <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion_domaine_valeur.resume_domaine.titre" /></legend>
    <table>
    <tr>
      <th>
      <label><sofi:libelle identifiant="infra_sofi.libelle.gestion_domaine_valeur.resume_domaine.champ.nom" /></label>
      </th>
      <td>
        <span>${domaineForm.objetTransfert.nom}</span>
      </td>
    </tr>
    <tr>
      <th>
      <label><sofi:libelle identifiant="infra_sofi.libelle.gestion_domaine_valeur.resume_domaine.champ.description" /></label>
      </th>
      <td>
        <span>${domaineForm.objetTransfert.description}</span>
      </td>
    </tr>
    <tr>
      <th><label><sofi:libelle identifiant="infra_sofi.libelle.gestion_domaine_valeur.resume_domaine.champ.tri" /></label></th>
      <td>
      <sofi:cache cache="listeTypeTri" valeur="${domaineForm.codeTypeTri}"/>
      </td>
    </tr>
    </table>
  </fieldset>
  <fieldset>
    <legend>
      <sofi:libelle identifiant="infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.titre" iconeAideAGauche="true" /> 
      <sofi-html:select styleId="codeLangue" 
                        cache="listeLocale" value="${domaineForm.codeLangue}" 
                        onchange="changementLangue(this.value)" />
    </legend>
    <div class="barreBouton">
      <sofi-html:bouton libelle="infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.bouton.creer" onclick="creerValeurNouvelleFenetre('${domaineForm.codeApplication}', '${domaineForm.nom}', $('#codeLangue').val());" />
    </div>
    <jsp:include page="ajax_liste_valeur_domaine.jsp" />
    <div class="barreBouton">
      <sofi-html:bouton libelle="infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.bouton.effacer_ordres" onclick="effacerOrdres();" />
      <c:if test="${domaineForm.codeTypeTri == 'O'}">
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.bouton.ordonner" onclick="ordonner();" />
      </c:if>
    </div>
    <div id="fenetreValeur"></div>
  </fieldset>