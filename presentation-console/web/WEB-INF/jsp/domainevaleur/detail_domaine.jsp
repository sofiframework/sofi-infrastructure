<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<p align="right" class="champsobligatoire">
  <span class="ind_obligatoire">*</span>
  <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire"/>
</p>
<sofi-html:form action="/domaine.do?methode=enregistrer" transactionnel="true">
  <fieldset>
    <legend>
      <sofi:libelle identifiant="infra_sofi.libelle.gestion_domaine_valeur.entete.definition_domaine" iconeAideAGauche="true" />
    </legend>
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <sofi-html:text libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.nom" 
                      property="nom" 
                      obligatoire="true" 
                      size="80"
                      maxlength="100" />
      <sofi-html:select 
          property="codeApplication" 
          ligneVide="true"
          libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.systeme"
          collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          proprieteEtiquette="[nom] ([codeApplication])"
          proprieteValeur="codeApplication" 
          obligatoire="true"/>
	    <tr>
	      <th>
	        <sofi:libelle identifiant="infra_sofi.libelle.gestion_domaine_valeur.champ.langue" />
	      </th>
	    </tr>
      <sofi-html:textarea libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.description" 
                          property="description"
                          rows="4" 
                          cols="80" 
                          maxlength="300"/>
      <sofi-html:select property="codeTypeTri" 
                        libelle="infra_sofi.libelle.gestion_domaine_valeur.champ.tri"
                        cache="listeTypeTri"
                        ligneVide="true"/>
    <tr>
      <th>
        <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.creePar" />
      </th>
      <td>
        ${domaineForm.creePar} ${domaineForm.dateCreation}
      </td>
    </tr>
    <tr>
      <th>
        <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.modifiePar" />
      </th>
      <td>
        ${domaineForm.modifiePar} ${domaineForm.dateModification}
      </td>
    </tr>
    </table>
    
    <div class="barreBouton">
      <c:if test="${!formulaireEnCours.nouveauFormulaire}">
        <c:url var="lienSupprimer" value="/domaine.do">
          <c:param name="methode" value="supprimer"/>
        </c:url>
        <sofi:message identifiant="infra_sofi.confirmation.sofi.gestion_domaine_valeur.suppression_definition" 
                      parametre1="${domaineForm.nom}" 
                      var="messageConfirmation"/>
        <sofi-html:bouton 
          libelle="infra_sofi.libelle.gestion_domaine_valeur.action.supprimer" 
          href="${lienSupprimer}"
          messageConfirmation="${messageConfirmation}" />
      </c:if>
      <sofi-html:submit libelle="infra_sofi.libelle.gestion_domaine_valeur.action.enregistrer" />
    </div>
    </fieldset>
</sofi-html:form>