<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<sofi-liste:listeNavigation action="rechercheDomaine.do?methode=rechercher"
                          nomListe="${LISTE_RECHERCHE_DOMAINE_VALEUR}"
                          triDefaut="1"
                          triDescendantDefaut="false"
                          class="tableresultat"
                          id="domaine"
                          trierPageSeulement="false"
                          ajax="true"
                          libelleResultat="infra_sofi.libelle.gestion_domaine_valeur.compteur.element"
                          iterateurMultiPage="true"
                          colgroup="55%,35%,10%"
                          colonneLienDefaut="1">
  <sofi-liste:colonne property="nom" trier="true"
                      libelle="infra_sofi.libelle.gestion_domaine_valeur.entete.nom"
                      traiterCorps="true">
    <a href="javascript:consulterValeur('${domaine.codeApplication}','${domaine.nom}','${domaine.valeur}','${domaine.codeLangue}');">
    <sofi:libelle identifiant="${domaine.nom}" />
    </a>
  </sofi-liste:colonne>
  <sofi-liste:colonne property="valeur" trier="true" 
                      libelle="infra_sofi.libelle.gestion_domaine_valeur.entete.valeur" />
  <sofi-liste:colonne property="codeClient" trier="true" 
                      libelle="Client" />
</sofi-liste:listeNavigation>