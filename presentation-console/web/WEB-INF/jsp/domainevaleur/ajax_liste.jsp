<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<c:if test="${rechercheDomaineForm.typeDomaine == 'DOMAINE' }">
  <jsp:include page="ajax_liste_valeur.jsp"/>
</c:if>
<c:if test="${rechercheDomaineForm.typeDomaine == 'DEFINITION' }">
  <jsp:include page="ajax_liste_domaine.jsp"/>
</c:if>