<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
  <div id="listeValeurDomaine">
    <sofi-liste:listeNavigation 
      id="valeur" 
      colgroup="20%, 35%, 10%, 10%, 5%, 5%, 5%, 5%"
      nomListe="${listeValeurDomaine}" divId="listeValeurDomaine"
      action="listeValeur" selection="codeApplication,nom,valeur,codeLangue"
      class="tableresultat" varIndex="i">
      <sofi-liste:colonneLibre libelle="infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.colonne.valeur">
        <a href="javascript:selectionnerValeur('${i}');">${valeur.valeur}</a>
      </sofi-liste:colonneLibre>
      <sofi-liste:colonne libelle="infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.colonne.description" property="description" />
      <sofi-liste:colonne libelle="Client" property="codeClient" />
      <sofi-liste:colonne libelle="infra_sofi.libelle.gestion_domaine_valeur.liste_valeur.colonne.ordre" property="ordreAffichage" />
      <sofi-liste:colonneLibre>
        <a href="javascript:consulterValeur('${valeur.codeApplication}','${valeur.nom}','${valeur.valeur}','${valeur.codeLangue}');"><img src="images/edit.png"/></a>
      </sofi-liste:colonneLibre>
      <sofi-liste:colonneLibre>
        <a href="javascript:monterValeur('${i}');"><img src="images/go-up.png"/></a>
      </sofi-liste:colonneLibre>
      <sofi-liste:colonneLibre>
        <a href="javascript:descendreValeur('${i}');"><img src="images/go-down.png"/></a>
      </sofi-liste:colonneLibre>
      <sofi-liste:colonneLibre>
        <sofi-html:link href="javascript:supprimerValeur('${i}');"><img src="images/supprimer.png"/></sofi-html:link>
      </sofi-liste:colonneLibre>
    </sofi-liste:listeNavigation>
  </div>