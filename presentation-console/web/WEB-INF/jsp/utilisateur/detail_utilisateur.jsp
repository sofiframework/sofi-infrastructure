<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<p align="right" class="champsobligatoire">
  <span class="ind_obligatoire">*</span> 
  <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire"/>
</p>
<fieldset>
  <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion_utilisateur.bloc.utilisateur"/></legend>
  <sofi-html:form action="/utilisateur.do?methode=enregistrer" transactionnel="true">
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <c:if test="${utilisateurForm.nouveauFormulaire}">
        <sofi-html:text property="nouveauCodeUtilisateur" 
                        libelle="infra_sofi.libelle.gestion_utilisateur.champ.code.utilisateur"  
                        obligatoire="true" size="30" maxlength="200"/>
      </c:if>
      <c:if test="${!utilisateurForm.nouveauFormulaire}">
        <sofi-html:text property="id" 
                        libelle="infra_sofi.libelle.gestion_utilisateur.champ.id"
                        affichageSeulement="true" />
        <sofi-html:text property="codeUtilisateur" 
                        libelle="infra_sofi.libelle.gestion_utilisateur.champ.code.utilisateur"  
                        obligatoire="true" size="20" maxlength="200" affichageSeulement="true">
          <c:url value="detailUtilisateurRole.do" var="lienSecurite">
            <c:param name="methode" value="consulter"></c:param>
            <c:param name="codeUtilisateur" value="${utilisateurForm.codeUtilisateur}"></c:param>
          </c:url>
          &nbsp;
        </sofi-html:text>
      </c:if>
      <c:if test="${utilisateurForm.nouveauFormulaire || utilisateurForm.changementMotPasse}">
        <sofi-html:password property="nouveauMotPasse" 
                        libelle="infra_sofi.libelle.gestion_utilisateur.champ.mot.de.passe" 
                        size="30" maxlength="200" obligatoire="true"/>
      </c:if>
      <sofi-html:text property="nom" libelle="infra_sofi.libelle.gestion_utilisateur.champ.nom"  
                      obligatoire="true" size="40" maxlength="100"/>
      <sofi-html:text property="prenom" libelle="infra_sofi.libelle.gestion_utilisateur.champ.prenom"  
                      obligatoire="true" size="40" maxlength="100"/>
      <sofi-html:text property="courriel" libelle="infra_sofi.libelle.gestion_utilisateur.champ.courriel"  
                      size="80" maxlength="200"/>

      <sofi-html:select property="codeStatut" 
                        libelle="infra_sofi.libelle.gestion_utilisateur.champ.statut"
                        cache="listeStatutActivation"
                        obligatoire="true"/>

      <sofi-html:select cache="listeLocale" 
                        libelle="infra_sofi.libelle.gestion_utilisateur.champ.langue"
                        property="langue" 
                        obligatoire="true" />

    <sofi-html:text 
      libelle="infra_sofi.libelle.gestion_utilisateur.champ.fuseau_horaire" 
      property="fuseauHoraire" 
      affichageSeulement="true" />

    <sofi-html:select cache="listeSexe" 
                      ligneVide="true"
                      libelle="infra_sofi.libelle.gestion_utilisateur.champ.sexe" 
                      property="sexe" />

    <tr>
      <th><sofi:libelle  identifiant="infra_sofi.libelle.gestion_utilisateur.champ.photo" /></th>
      <td>
        <img src="${utilisateurForm.urlPhoto}" />
      </td>
    </tr>

    <sofi-html:text property="codeFournisseurAcces" 
                    libelle="infra_sofi.libelle.gestion_utilisateur.champ.fournisseur_acces" 
                    affichageSeulement="true" />

    <sofi-html:date property="dateNaissance" 
                    libelle="infra_sofi.libelle.gestion_utilisateur.champ.date_naissance" />

    <sofi-html:select cache="listeGroupeAge" 
                      ligneVide="true"
                      libelle="infra_sofi.libelle.gestion_utilisateur.champ.groupe_age" 
                      property="codeGroupeAge" />

    <sofi-html:textarea property="description" rows="5" maxlength="1000" cols="75"
                    libelle="infra_sofi.libelle.gestion_utilisateur.champ.description" /> 

      <jsp:include page="../trace.jsp" />

    </table>
    <div class="barreBouton">
      <c:if test="${!formulaireEnCours.nouveauFormulaire && !utilisateurForm.changementMotPasse}">
         <sofi-html:bouton libelle="Modifier le mot de passe" 
                           href="utilisateur.do?methode=changerMotPasse" 
                           activerConfirmationModificationFormulaire="true"/>
        <sofi:message identifiant="confirmation.infra_sofi.gestion_utilisateur.supprimer_utilisateur" 
                      parametre1="${utilisateurForm.prenom} ${utilisateurForm.nom}" 
                      var="messageConfirmation"/>
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion_utilisateur.action.supprimer" 
                          href="utilisateur.do?methode=supprimer" 
                          messageConfirmation="${messageConfirmation}" 
                          notifierModificationFormulaire="true" />
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion_utilisateur.action.creer" 
                          href="utilisateur.do?methode=ajouter"/>
      </c:if>
      <c:if test="${utilisateurForm.changementMotPasse}">
        <sofi-html:bouton libelle="Annuler" 
                          href="utilisateur.do?methode=annulerChangerMotPasse" />
      </c:if>
      <sofi-html:submit libelle="infra_sofi.libelle.gestion_utilisateur.action.enregistrer" />
    </div>    
  </sofi-html:form>
</fieldset>