<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.criteres.recherche"/>
  </legend>
  <sofi-html:form action="rechercheUtilisateur.do?methode=rechercher" transactionnel="false" >
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
    
      <sofi-html:text libelle="infra_sofi.libelle.gestion_utilisateur.champ.nom" 
                      property="nom" size="40" maxlength="100">
          <sofi-html:text placeholder="infra_sofi.libelle.gestion_utilisateur.champ.prenom" 
                         property="prenom" size="40" maxlength="100"/>
                      
      </sofi-html:text>
   
      <sofi-html:text libelle="infra_sofi.libelle.gestion_utilisateur.champ.code.utilisateur" 
                      property="codeUtilisateur" size="40" maxlength="200">
           <sofi-html:text placeholder="infra_sofi.libelle.gestion_utilisateur.champ.id" 
                      property="id" size="5" maxlength="10"/>   
      </sofi-html:text>   
      <sofi:parametreSysteme code="codeUtilisateurApplicatif" 
                              var="codeUtilisateurApplicatifActif" 
                              scope="request" />
      <c:if test="${codeUtilisateurApplicatifActif}">
        <sofi-html:text libelle="infra_sofi.libelle.gestion_utilisateur.champ.codeUtilisateurApplicatif" 
                        property="codeUtilisateurApplicatif" size="60" maxlength="200" iconeAide="true"/>    
      </c:if>
   
                      
      <sofi-html:text libelle="infra_sofi.libelle.gestion_utilisateur.champ.courriel" 
                      property="courriel" size="60" maxlength="200"/>        
      <sofi-html:select property="codeStatut" 
                        libelle="infra_sofi.libelle.gestion_utilisateur.champ.statut"
                        cache="listeStatutActivation"
                        ligneVide="true"/>
      <sofi-html:select property="codeApplication" 
                        libelle="infra_sofi.libelle.gestion_utilisateur_role.champ.systeme"
                        collection="${LISTE_APPLICATIONS_UTILISATEUR}"
                        proprieteEtiquette="nom"
                        proprieteValeur="codeApplication"
                        ligneVide="true"
                        href="rechercheUtilisateur.do?methode=chargerListeRole"
                        nomParametreValeur="codeApplication"
                        nomAttributDestinationAjax="codeRole"          
                        />
      <sofi-html:select property="codeRole" 
                        libelle="infra_sofi.libelle.gestion_utilisateur_role.champ.role"
                        collection="${LISTE_ROLE}"
                        proprieteEtiquette="nom"
                        proprieteValeur="codeRole"
                        ligneVide="true"
                        style="width:250px;">
          <sofi-html:checkbox libelle="infra_sofi.libelle.gestion_utilisateur_role.champ.ne_contient_pas" 
                              property="codeRoleNonInclus" 
                              libelleAssocie="true"
                              genererLigne="false"/>
      </sofi-html:select>          
    </table>
    <div class="barreBouton">   
      <sofi-html:bouton libelle="infra_sofi.libelle.gestion_utilisateur.action.creer" 
                        href="utilisateur.do?methode=ajouter"/>
      <sofi-html:submit libelle="infra_sofi.libelle.commun.action.rechercher" />
    </div>
  </sofi-html:form>
</fieldset>
<c:if test="${listeRechercheUtilisateur != null}">
  <fieldset>
    <legend>
      <sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche"/>
    </legend>
    <jsp:include page="ajax_liste_utilisateur.jsp"/>
  </fieldset>
</c:if>