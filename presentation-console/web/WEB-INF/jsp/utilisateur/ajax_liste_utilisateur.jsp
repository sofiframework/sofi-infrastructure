<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi:parametreSysteme code="codeUtilisateurApplicatif" var="codeUtilisateurApplicatifActif" scope="request" />
<c:if test="${codeUtilisateurApplicatifActif}">
  <c:set value="5%,20%,20%,30%,5%,10%,10%" var="colgroup"/>
</c:if>
<c:if test="${!codeUtilisateurApplicatifActif}">
  <c:set value="5%,20%,20%,30%,10%,10%" var="colgroup"/>
</c:if>
<sofi-liste:listeNavigation action="rechercheUtilisateur.do?methode=rechercher"
                          nomListe="${listeRechercheUtilisateur}"
                          triDefaut="1"
                          triDescendantDefaut="false"
                          class="tableresultat"
                          id="utilisateur"
                          trierPageSeulement="false"
                          ajax="true"
                          libelleResultat="infra_sofi.libelle.gestion_utilisateur.compteur.element"
                          iterateurMultiPage="true"
                          colgroup="${colgroup}"
                          colonneLienDefaut="2"
                          >
                          
  <sofi-liste:colonne property="id" 
                      trier="true" 
                      libelle="infra_sofi.libelle.gestion_utilisateur.champ.id"/>
  <sofi-liste:colonne property="nom" trier="true" 
                      libelle="infra_sofi.libelle.gestion_utilisateur.entete.nom"
                      href="utilisateur.do?methode=consulter"
                      paramId="codeUtilisateur"
                      paramProperty="codeUtilisateur"
                      traiterCorps="true">
    <c:out value="${utilisateur.nom}"/>, <c:out value="${utilisateur.prenom}"/>
  </sofi-liste:colonne>
  <sofi-liste:colonne property="codeUtilisateur" trier="true" 
                      libelle="infra_sofi.libelle.gestion_utilisateur.entete.code_utilisateur" />  
  <sofi-liste:colonne property="courriel" 
                      trier="true" 
                      libelle="infra_sofi.libelle.gestion_utilisateur.entete.courriel"/>
  <c:if test="${codeUtilisateurApplicatifActif}">  
    <sofi-liste:colonne property="codeUtilisateurApplicatif" trier="true" 
                        libelle="infra_sofi.libelle.gestion_utilisateur.entete.codeUtilisateurApplicatif" /> 
  </c:if>
  <sofi-liste:colonne property="dateCreation" trier="true" traiterCorps="true"
                      libelle="infra_sofi.libelle.commun.entete.creation">  
    <fmt:formatDate value="${utilisateur.dateCreation}" pattern="yyyy-MM-dd HH:mm:ss" />
  </sofi-liste:colonne>
  <sofi-liste:colonne property="dateModification" trier="true" traiterCorps="true"
                      libelle="infra_sofi.libelle.commun.entete.maj">  
    <fmt:formatDate value="${utilisateur.dateModification}" pattern="yyyy-MM-dd HH:mm:ss" />
  </sofi-liste:colonne>    
</sofi-liste:listeNavigation>  