<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%><%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<sofi-html:text libelle="infra_sofi.libelle.commun.champ.creePar"
  property="creePar" disabled="true" size="25">
        &nbsp;<sofi-html:text property="dateCreation" disabled="true" />
</sofi-html:text>
<sofi-html:text libelle="infra_sofi.libelle.commun.champ.modifiePar"
  property="modifiePar" disabled="true" size="25">
                      &nbsp;<sofi-html:text
    property="dateModification" disabled="true" />
</sofi-html:text>