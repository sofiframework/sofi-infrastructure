<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<sofi-liste:listeNavigation action="rechercheReferentiel.do?methode=rechercherParent"
                            class="tableresultat" 
                            nomListe="${listeReferentiel}"
                            trierPageSeulement="false" 
                            triDefaut="1" 
                            ajax="true"
                            fonctionRetourValeurAjax="true" 
                            iterateurMultiPage="true"
                            libelleResultat="infra_sofi.libelle.gestion_referentiel.compteur.element"
                            colonneLienDefaut="2"
                            >
    <sofi-liste:colonne property="seqObjetSecurisable" trier="true" 
      fonctionRetourValeurAjax="true" parametresFonction="seqObjetSecurisable, nom" 
      
      libelle="infra_sofi.libelle.gestion_referentiel.entete.seqObjetSecurisable" />
    <sofi-liste:colonne parametresFonction="seqObjetSecurisable, nom" 
      fonctionRetourValeurAjax="true" property="nom" trier="true" 
      libelle="infra_sofi.libelle.gestion_referentiel.entete.nom" />
</sofi-liste:listeNavigation>