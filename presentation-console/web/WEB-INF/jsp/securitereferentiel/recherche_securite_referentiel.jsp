<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>

<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.criteres.recherche" />
  </legend>
  <sofi-html:form action="/rechercheSecuriteReferentiel.do?methode=rechercher"
                  transactionnel="false" focus="nom" method="POST">
    <input type="hidden" name="methode" value="rechercher" />
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <sofi-html:text 
          libelle="infra_sofi.libelle.gestion_securite_referentiel.champ.nom" 
          property="nom" size="80" />
      <sofi-html:select 
          property="type" ligneVide="true"
          libelle="infra_sofi.libelle.gestion_securite_referentiel.champ.type"
          cache="listeTypeObjetSecurisable" />
      <sofi-html:select 
          property="codeStatut" ligneVide="true"
          libelle="infra_sofi.libelle.gestion_securite_referentiel.champ.statutActivation"
          cache="listeStatutActivation" />

      <sofi-html:select 
          property="codeApplication" 
          libelle="infra_sofi.libelle.gestion_securite_referentiel.champ.codeApplication"
          collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          proprieteEtiquette="[nom] ([codeApplication])"
          proprieteValeur="codeApplication"
          ajax="true"/>
      </table>
      <div class="barreBouton">
       <sofi-html:submit libelle="infra_sofi.libelle.commun.action.rechercher" />
      </div>
    </sofi-html:form>
</fieldset>
<c:if test="${listeSecuriteReferentiel != null}">
  <fieldset>
    <legend>
      <sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche" />
     </legend>
    <jsp:include page="ajax_liste_securite_referentiel.jsp" />
  </fieldset>
</c:if>