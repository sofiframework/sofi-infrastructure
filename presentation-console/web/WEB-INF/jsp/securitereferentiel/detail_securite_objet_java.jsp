<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<sofi-html:form action="/securiteReferentiel.do?methode=enregistrer#zoneRoleAutorise" transactionnel="true">
<fieldset>
  <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion.securite.section.detail.objet.java"/></legend>
  <table border="0" cellspacing="0" cellpadding="0" class="tableaffichage">
   <sofi-html:select 
          property="codeApplication" 
          libelle="infra_sofi.libelle.gestion.securite.champ.detail.systeme"
          collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          proprieteEtiquette="nom"
          proprieteValeur="codeApplication"
          affichageSeulement="true"/>  
   <sofi-html:text libelle="infra_sofi.libelle.gestion.securite.champ.detail.identifiant" property="nom" affichageSeulement="true" />
 
   <c:url value="referentiel.do" var="lienDetailReferentiel">
     <c:param name="methode" value="consulter"></c:param>
     <c:param name="seqObjetSecurisable" value="${securiteObjetJavaForm.objetTransfert.id}" />
   </c:url>
 
    <tr>
      <th><sofi:libelle identifiant="infra_sofi.libelle.gestion.securite.champ.nom" /></th>
      <td>
        <b>
          <sofi:libelle identifiant="${securiteObjetJavaForm.objetTransfert.nom}" />
          (<sofi:cache cache="listeTypeObjetSecurisable" valeur="${securiteObjetJavaForm.objetTransfert.type}"/>)
        </b>
	      &nbsp;
	      <sofi-html:link 
	        href="${lienDetailReferentiel}" 
	        libelle="infra_sofi.libelle.gestion_securite.action.consulter_detail_referentiel" 
	        activerConfirmationModificationFormulaire="true"
	        memoriserHrefActuel="true">
	      </sofi-html:link>
      </td>
    </tr>
    <tr>
      <th>
        <sofi:libelle identifiant="infra_sofi.libelle.gestion.securite.champ.section.composant.parent"/>
      </th>
      <td>
        <c:if test="${securiteObjetJavaForm.objetTransfert.objetJavaParent != null }">
          <c:set var="parent" value="${securiteObjetJavaForm.objetTransfert.objetJavaParent}"  />
          <c:url var="lienDetailParent" value="/securiteReferentiel.do">
            <c:param name="methode" value="consulter"/>
            <c:param name="seqObjetSecurisable" value="${parent.id}"/>
          </c:url>
          <a href="${lienDetailParent}"><sofi:libelle identifiant="${parent.nom}" /></a>
          (<sofi:cache cache="listeTypeObjetSecurisable" valeur="${parent.type}"/>)
        </c:if> 
        <c:if test="${securiteObjetJavaForm.objetTransfert.objetJavaParent == null }">
          Aucun
        </c:if> 
      </td>
    </tr> 
    <tr>
      <th>
        <sofi:libelle identifiant="infra_sofi.libelle.gestion.securite.champ.section.composant.enfant"/>
      </th>
      <td>
        <sofi:taille valeur="${securiteObjetJavaForm.objetTransfert.listeObjetJavaEnfant}" var="nbEnfant"/>
        <c:if test="${nbEnfant > 0 }" var="enfantPresent">
         <c:forEach items="${securiteObjetJavaForm.objetTransfert.listeObjetJavaEnfant}" var="enfant">
            <c:url var="lienDetailEnfant" value="/securiteReferentiel.do">
              <c:param name="methode" value="consulter"/>
              <c:param name="seqObjetSecurisable" value="${enfant.id}"/>
            </c:url>
            <a href="${lienDetailEnfant}"><sofi:libelle identifiant="${enfant.nom}" /></a>
            (<sofi:cache cache="listeTypeObjetSecurisable" valeur="${enfant.type}"/>)
            <br/>     
         </c:forEach>
        </c:if>
         <c:if test="${nbEnfant == 0 }">
          Aucun
        </c:if>
      </td>
    </tr> 
    <sofi-html:text libelle="infra_sofi.libelle.gestion.securite.champ.description" property="description" affichageSeulement="true" />
  </table>
  <br/>
  <div id="zoneRoleAutorise" >
    <fieldset class="sous_section"> 
      <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion.securite.section.role.autorise"/></legend>
      <table border="0" cellspacing="0" cellpadding="0" class="tableresultat">
        <colgroup>
          <col style="width:60%;" />
          <col style="width:20%;" />
          <col style="width:20%;" />
        </colgroup>
        <thead>
        <tr><th><sofi:libelle identifiant="infra_sofi.libelle.gestion.securite.colonne.role" /></th>
        <th><sofi:libelle identifiant="infra_sofi.libelle.gestion.securite.colonne.consultation.seulement" /></th>
        <th></th></tr>
        </thead>
        <tbody>
          <c:if test="${securiteObjetJavaForm.attributs.listeRoleObjetJavaAutorise ne null}">
            <sofi-nested:iterate collection="${securiteObjetJavaForm.attributs.listeRoleObjetJavaAutorise}"  property="listeRoleObjetJavaAutorise" id="roleAutorise" indexId="iteration">
              <tr>
              <td>
               <sofi-nested:select property="codeRole" 
                                   fermerLigne="false" 
                                   collection="${LISTE_ROLE_POUR_APPLICATION}" 
                                   proprieteEtiquette="nom" 
                                   proprieteValeur="codeRole" 
                                   ligneVide="true" />
              </td>
              <td align="center"><sofi-nested:checkbox property="indicateurConsultation" ouvrirLigne="false" fermerLigne="false" /></td>
              
              <c:url var="supprimerAssociation" value="/securiteReferentiel.do?methode=supprimerRoleObjetJava">
                <c:param name="index" value="${iteration}" />
              </c:url>
              <td align="center">
                <c:if test="${roleAutorise != null && roleAutorise.codeRole != null}">
                  <sofi:message var="confirmationDissocierRole"
                                identifiant="infra_sofi.confirmation.gestion_securite.dissocier_role"
                                parametre1="${roleAutorise.codeRole}"
                                />
                  <sofi-html:bouton libelle="" 
                                    href="${supprimerAssociation}" 
                                    styleClass="boutonSuppression" 
                                    messageConfirmation="${confirmationDissocierRole}" 
                                    notifierModificationFormulaire="true"/>
                </c:if>
              </td>
              </tr>
            </sofi-nested:iterate>
          </c:if>
        </tbody>
      </table>
      <div align="right">
        <sofi-html:submit libelle="Ajouter" styleId="ajouter"
                          href="securiteReferentiel.do?methode=ajouterRoleObjetJava#ajouter" 
                          ignorerValidation="true" />
      </div>
    </fieldset>
  </div>

  <div class="barreBouton">
    <c:url var="lienReporter" value="/securiteReferentiel.do?methode=reporterSecuriteVersEnfants"/>
    <sofi-html:bouton libelle="infra_sofi.libelle.gestion.securite.bouton.reporter.securite" 
                      href="${lienReporter}" 
                      messageConfirmation="infra_sofi.confirmation.gestion.securite.reporter.securite"
                      activerConfirmationModificationFormulaire="true"
                      notifierModificationFormulaire="true"/>
    <sofi-html:submit libelle="infra_sofi.libelle.gestion.securite.bouton.enregistrer"/>
  </div>
</fieldset>
  </sofi-html:form>
