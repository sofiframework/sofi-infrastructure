<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<link rel="stylesheet" href="script/zptree/themes/tree.css" >

<!-- Theme stylesheets -->
<link rel="stylesheet" type="text/css"  href="script/zptree/themes/windows.css" id='theme-windows' >
<link rel="alternate stylesheet" type="text/css" media="all" href="script/zptree/themes/kde.css" title="kde" id='theme-kde'>
<link rel="alternate stylesheet" type="text/css" media="all" href="script/zptree/themes/help.css" title="help" id='theme-help'>
<link rel="alternate stylesheet" type="text/css" media="all" href="script/zptree/themes/gnome.css" title="gnome" id='theme-gnome'>

<link id="css-tree-lines" rel="stylesheet" href="script/zptree/themes/tree-lines.css" >
<link id="css-tree-default-icons" class="not-in-output" rel="stylesheet" href="script/zptree/themes/tree-default-icons.css" >

<script type="text/javascript" src="script/zptree/utils/utils.js"></script>
<script type="text/javascript" src="script/zptree/src/tree.js"></script>
<script type="text/javascript" src="script/zptree/utils/tooltips.js"></script>

<fieldset style="border:0px;">
  <div id="zone_selection_application">
      <sofi:libelle styleClass="libelle_selection_application" identifiant="infra_sofi.libelle.gestion_referentiel.structure_referentiel_systeme" nomAttribut="codeApplication" />
      <sofi-html:select property="codeApplication" 
                        collection="${LISTE_APPLICATIONS_UTILISATEUR}"
                        proprieteEtiquette="[nom] ([codeApplication])"
                        proprieteValeur="codeApplication"
                        href="rechercheSecuriteReferentiel.do?methode=structure"
                        nomParametreValeur="codeApplication" />
  </div>
</fieldset>  
<table border="0" cellspacing="0" cellpadding="0" class="fondBarreBouton">
  <tr>
    <td style="text-align: left">
      &nbsp;&nbsp;
      <sofi-html:bouton onclick="javascript:Zapatec.Tree.all['tree'].expandAll()" 
                        libelle="infra_sofi.libelle.gestion_referentiel.action.eclater"/>
      <sofi-html:bouton onclick="javascript:Zapatec.Tree.all['tree'].collapseAll()"
                        libelle="infra_sofi.libelle.gestion_referentiel.action.fermer"/>
    </td>
    <td align="right">
      <sofi-html:bouton onclick="javascript:consulter()"
                        libelle="infra_sofi.libelle.gestion_referentiel.action.consulter"
                        styleId="consulter"
                        disabled="true"/>
      <sofi-html:bouton href="javascript:creer()"
                        libelle="infra_sofi.libelle.gestion_referentiel.action.creer"
                        styleId="creer"/>
      <sofi-html:bouton onclick="javascript:supprimer()" 
                        libelle="infra_sofi.libelle.gestion_referentiel.action.supprimer"
                        styleId="supprimer"
                        disabled="true"/>
    </td>
  </tr>
</table>
<%--jsp:include page="ajax_structure_securite_referentiel.jsp" /--%>