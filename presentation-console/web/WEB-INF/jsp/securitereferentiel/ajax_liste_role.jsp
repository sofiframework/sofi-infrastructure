 <%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib prefix="fqr" tagdir="/WEB-INF/tags/base" %>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>

<table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
  <sofi-nested:nest property="listeCodeRole">
    <sofi-nested:select 	
        property="codeRole" 
        ligneVide="true"
        libelle="infra_sofi.libelle.gestion_securite_referentiel.champ.codeRole"
        collection="${LISTE_ROLE}"
        proprieteEtiquette="nom"
        proprieteValeur="codeRole" >
      <sofi-nested:checkbox 
        libelle="infra_sofi.libelle.gestion_utilisateur_role.champ.ne_contient_pas" 
        property="codeRoleNonInclus" 
        libelleAssocie="true"
        genererLigne="false"/>
      </sofi-nested:select>
  </sofi-nested:nest>
</table>