<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi-liste:listeNavigation action="rechercheSecuriteReferentiel.do?methode=rechercher"
                          nomListe="${listeSecuriteReferentiel}"
                          triDefaut="1"
                          triDescendantDefaut="false"
                          class="tableresultat"
                          id="objetReferentiel"
                          trierPageSeulement="false"
                          ajax="true"
                          libelleResultat="infra_sofi.libelle.gestion_securite_referentiel.compteur.element"
                          iterateurMultiPage="true"
                          colonneLienDefaut="1">
  <sofi-liste:colonne property="nom" trier="true" 
                      libelle="infra_sofi.libelle.gestion_securite_referentiel.entete.nom"
                      href="securiteReferentiel.do?methode=consulter"
                      paramId="seqObjetSecurisable"
                      paramProperty="id" />
  <sofi-liste:colonne property="type" trier="true" traiterCorps="true"
    libelle="infra_sofi.libelle.gestion_securite_referentiel.entete.type">
    <sofi:cache cache="listeTypeObjetSecurisable" valeur="${objetReferentiel.type}"/>
  </sofi-liste:colonne>
 <sofi-liste:colonne property="codeApplication" trier="true" traiterCorps="true"
    libelle="infra_sofi.libelle.gestion_securite_referentiel.entete.systeme">
    <sofi:cache valeur="${objetReferentiel.codeApplication}" cache="listeApplication" proprieteAffichage="nom"/>
  </sofi-liste:colonne>
</sofi-liste:listeNavigation>