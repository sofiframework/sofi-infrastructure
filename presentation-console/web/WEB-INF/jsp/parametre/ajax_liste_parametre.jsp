<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%> 

<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="app-dialog" tagdir="/WEB-INF/tags/dialog" %>

<sofi:libelle identifiant="infra_sofi.libelle.gestion_parametre.titre" var="titreDialog" />
<div id="ParametreEditionWindow" title="${titreDialog}"></div>

<sofi-liste:listeNavigation action="rechercheParametre.do?methode=rechercher"
                            id="parametre"
                            nomListe="${listeParametreSysteme}"
                            colgroup="25%,50%,10%,12%,12% "
                            triDefaut="1"
                            triDescendantDefaut="false"
                            class="tableresultat"
                            ajax="true"
                            iterateurMultiPage="true"
                            libelleResultat="infra_sofi.libelle.gestion_parametre.resultat"
                            colonneLienDefaut="1"
                            >
  <sofi-liste:colonne property="codeParametre" href="parametre.do?methode=consulter" 
                      paramId="id" paramProperty="id" trier="true" 
                      libelle="infra_sofi.libelle.gestion_parametre.code_resultat"
                      sautLigneNombreCararactereMaximal="60"
                      sautLigneCararactereSeparateur="_"
                      >
  </sofi-liste:colonne>
                      
  <sofi-liste:colonne property="valeur" trier="true" 
                      libelle="infra_sofi.libelle.gestion_domaine_valeur.entete.valeur" 
                      sautLigneNombreCararactereMaximal="80"
                      sautLigneCararactereSeparateur="/"/>
                      
  <sofi-liste:colonne property="codeClient"
  libelle="Client"
  trier="true"
  />

  <sofi-liste:colonne property="dateCreation" trier="true" traiterCorps="true"
                      libelle="infra_sofi.libelle.commun.entete.creation">
    <fmt:formatDate value="${parametre.dateCreation}" pattern="yyyy-MM-dd HH:mm:ss" />
  </sofi-liste:colonne> 
  <sofi-liste:colonne property="dateModification" trier="true" traiterCorps="true"
                      libelle="infra_sofi.libelle.commun.entete.maj">  
    <fmt:formatDate value="${parametre.dateModification}" pattern="yyyy-MM-dd HH:mm:ss" />
  </sofi-liste:colonne>
</sofi-liste:listeNavigation>  