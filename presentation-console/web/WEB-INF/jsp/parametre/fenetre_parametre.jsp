<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>
<%@ taglib prefix="app-dialog" tagdir="/WEB-INF/tags/dialog" %>

<sofi:barreStatut 
  styleId="BarreStatutPopup" 
  exclureMessageErreurDefaut="false" afficherSeulementSiMessage="false" />

<app:ajaxListeDependante var="ajaxListeTypeParametre" 
  href="rechercheParametre.do?methode=chargerListeType" 
  idListeDeroulanteEnfant="codeType" 
  idListeDeroulanteParent="codeApplication" />

<!-- Section formulaire -->
<sofi-html:form action="/fenetreParametre.do?methode=enregistrer" transactionnel="true" focusAutomatique="true">

    <app:tableFormulaire>
      <sofi-html:hidden property="id"  />
      <sofi-html:text property="codeParametre" obligatoire="true" size="70" maxlength="100"
        libelle="Code" />
  
      <sofi-html:select property="codeApplication" ligneVide="false"
        obligatoire="true" collection="${LISTE_APPLICATIONS_UTILISATEUR}"
        proprieteEtiquette="nom" proprieteValeur="codeApplication" 
        ajaxJSApresChargement="${ajaxListeTypeParametre}"
        libelle="Application" />

      <sofi-html:select property="codeType" ligneVide="true"
                      libelle="Type"
                      collection="${LISTE_TYPE_PARAMETRE}"
                      proprieteEtiquette="description" 
                      proprieteValeur="valeur" />
                      
      <sofi-html:text property="codeClient" size="30" maxlength="50"
        libelle="infra_sofi.libelle.gestion_parametre.codeClient" />
  
      <sofi-html:text property="valeur" obligatoire="true" size="70"
        maxlength="500"
        libelle="Valeur" />
  
      <sofi-html:textarea property="description" obligatoire="true"
        cols="90" rows="3" maxlength="500"
        libelle="Description" />
  
      <sofi-html:checkbox property="differentEnvironnement"
        libelle="Diff. Env." />
  
      <sofi-html:checkbox property="modificationADistance"
        libelle="Mod. à dist." />
  
      <jsp:include page="../trace.jsp" />
    </app:tableFormulaire>
  
    <div class="barreBouton">
    &nbsp;&nbsp;&nbsp;
    <c:if test="${!parametreForm.nouveauFormulaire}">
			  
			<sofi-html:submit libelle="Créer"
			    href="fenetreParametre.do?methode=ajouter&ignorerValidation=true"
			    activerConfirmationModificationFormulaire="true"
			    inactiverBoutons="false" />
			 
			<sofi-html:bouton libelle="Supprimer"
			      inactiverBoutons="false"
			      href="parametre.do?methode=supprimer&codeParametre=${parametre.id}"
			      />
    </c:if>
    <sofi-html:submit libelle="Enregistrer"
               inactiverBoutons="false" />
    </div>
</sofi-html:form>