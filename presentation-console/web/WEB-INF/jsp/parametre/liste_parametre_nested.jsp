<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%> 

<c:if test="${rechercheParametreForm.listeParametre != null}">
  <table class="tableresultat">
    <colgroup align="left" width="30%" />
    <colgroup align="left" width="50%" />
    <colgroup align="left" width="20%" />
    <thead>
      <tr>
        <th>
          <sofi:libelle identifiant="infra_sofi.libelle.gestion_parametre.code_resultat" />
        </th>
        <th>
          <sofi:libelle identifiant="infra_sofi.libelle.gestion_domaine_valeur.entete.valeur" />
        </th>
        <th></th>
      </tr>
    </thead>
    <tbody>  
      <sofi-nested:iterate name="rechercheParametreForm" property="listeParametre" id="parametre" indexId="index">
      <tr>
        <td>
          <sofi-nested:text property="codeParametre" size="40" maxlength="100" />
        </td>
        <td>
          <sofi-nested:text property="valeur" size="90" maxlength="500" />
        </td>
        <td>
          <sofi-html:bouton href="rechercheParametre.do?methode=insererLigne&index=${index}" libelle="+" />
          <sofi-html:bouton href="rechercheParametre.do?methode=supprimerLigne&index=${index}" libelle="-" />
          <sofi-html:bouton href="rechercheParametre.do?methode=toggleLigne&index=${index}" libelle="&gt;" />
        </td>
      </tr>
      </sofi-nested:iterate>
    </tbody>
    <tfoot>
    </tfoot>
  </table>
</c:if>