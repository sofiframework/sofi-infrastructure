<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>

<app:ajaxListeDependante var="ajaxListeTypeParametre" 
  href="rechercheParametre.do?methode=chargerListeType" 
  idListeDeroulanteEnfant="codeType" 
  idListeDeroulanteParent="codeApplication" />

<!-- Section filtre formulaire -->
<sofi-html:form action="/rechercheParametre.do?methode=rechercher#listeParametreSysteme" 
              transactionnel="false" focus="codeParametre">
  <fieldset class="formulaire">
  <legend><sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.criteres.recherche"/></legend>
    <table cellpadding="0" cellspacing="0" class="tableformulaire">

      <sofi-html:text property="codeParametre" size="70" maxlength="2000"
                      libelle="infra_sofi.libelle.gestion_parametre.champ.code" />
                      
      <sofi-html:select property="codeApplication"
                      libelle="infra_sofi.libelle.gestion_parametre.champ.code_application"
                      collection="${LISTE_APPLICATIONS_UTILISATEUR}"
                      proprieteEtiquette="[nom] ([codeApplication])"
                      proprieteValeur="codeApplication"
                      href="rechercheReferentiel.do?methode=fixerApplication"
                      ajaxJSApresChargement="${ajaxListeTypeParametre}"
                      ajax="true"
                      nomParametreValeur="codeApplication" />     
                      
      <sofi-html:select property="codeClient" ligneVide="true"
                      libelle="infra_sofi.libelle.gestion_parametre.codeClient"
                      collection="${LISTE_CLIENT}"
                      classeDecorator="org.sofiframework.infrasofi.presentation.decorator.ClientSelectDecorator" />                                  

      <sofi-html:text property="description" size="70" maxlength="300"
                      libelle="infra_sofi.libelle.gestion_parametre.champ.description" />

                      
      <sofi:taille valeur="${LISTE_TYPE_PARAMETRE}" var="nbListeParametre"/>
 
      <c:if test="${nbListeParametre > 0}" >   

	      <sofi-html:select property="codeType" ligneVide="true"
	                      libelle="infra_sofi.libelle.gestion_parametre.champ.code_type"
	                      collection="${LISTE_TYPE_PARAMETRE}"
	                      proprieteEtiquette="description" 
	                      proprieteValeur="valeur" />
	    </c:if>

      <sofi-html:checkbox property="differentEnvironnement" 
                        libelle="infra_sofi.libelle.gestion_parametre.champ.different_environnement" />

      <sofi-html:checkbox property="modificationADistance" 
                        libelle="infra_sofi.libelle.gestion_parametre.champ.modification_a_distance" />
    </table>
    <div class="barreBouton">
      <sofi-html:bouton libelle="infra_sofi.libelle.gestion_parametre.bouton.creer" 
                        href="parametre.do?methode=ajouter" />
      <sofi-html:submit libelle="infra_sofi.libelle.gestion_parametre.bouton.rechercher" />
    </div>
  </fieldset>
</sofi-html:form>

<!-- Section liste de navigation -->
<c:if test="${listeParametreSysteme != null}">
  <fieldset>
    <legend>
      <sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche"/>
    </legend>
    <c:import url="WEB-INF/jsp/parametre/ajax_liste_parametre.jsp" />
  </fieldset>
</c:if>

<script>
  $("#codeClient").select2({containerCssClass: "select_grand"});
</script>