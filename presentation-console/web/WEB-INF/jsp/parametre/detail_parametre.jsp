<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<%@ taglib prefix="app" tagdir="/WEB-INF/tags/base" %>

<app:legendeObligatoire />

<app:ajaxListeDependante var="ajaxListeTypeParametre" 
  href="rechercheParametre.do?methode=chargerListeType" 
  idListeDeroulanteEnfant="codeType" 
  idListeDeroulanteParent="codeApplication" />

<!-- Section formulaire -->
<sofi-html:form action="/parametre.do?methode=enregistrer" transactionnel="true" focusAutomatique="true">
  <fieldset class="formulaire">
    <app:legende libelle="infra_sofi.libelle.gestion_parametre_systeme.onglet.detail" />
    <app:tableFormulaire>
      <sofi-html:hidden property="id"  />
      <sofi-html:text property="codeParametre" obligatoire="true" size="70" maxlength="100"
							        libelle="infra_sofi.libelle.gestion_parametre.champ.code_parametre" />
  
      <sofi-html:select property="codeApplication" ligneVide="false"
								        obligatoire="true" collection="${LISTE_APPLICATIONS_UTILISATEUR}"
								        proprieteEtiquette="[nom] ([codeApplication])" proprieteValeur="codeApplication" 
								        ajaxJSApresChargement="${ajaxListeTypeParametre}"
								        libelle="infra_sofi.libelle.gestion_parametre.champ.code_application" />
        
      <sofi-html:select property="codeClient" ligneVide="true"
                      libelle="infra_sofi.libelle.gestion_parametre.codeClient"
                      collection="${LISTE_CLIENT}"
                      classeDecorator="org.sofiframework.infrasofi.presentation.decorator.ClientSelectDecorator" />    
        
      <c:if test="${LISTE_TYPE_PARAMETRE != null}" >

	      <sofi-html:select property="codeType" ligneVide="true"
	                      libelle="infra_sofi.libelle.gestion_parametre.champ.code_type"
	                      collection="${LISTE_TYPE_PARAMETRE}"
	                      proprieteEtiquette="description" 
	                      proprieteValeur="valeur" />
	    </c:if>

  
      <sofi-html:text property="valeur" obligatoire="true" size="70"
        maxlength="500"
        libelle="infra_sofi.libelle.gestion_parametre.champ.valeur" />
  
      <sofi-html:textarea property="description" obligatoire="true"
        cols="90" rows="3" maxlength="500"
        libelle="infra_sofi.libelle.gestion_parametre.champ.description" />
  
      <sofi-html:checkbox property="differentEnvironnement"
        libelle="infra_sofi.libelle.gestion_parametre.champ.different_environnement" />
  
      <sofi-html:checkbox property="modificationADistance"
        libelle="infra_sofi.libelle.gestion_parametre.champ.modification_a_distance" />
  
      <jsp:include page="../trace.jsp" />
    </app:tableFormulaire>
  
    <div class="barreBouton">
    <c:if test="${!parametreForm.nouveauFormulaire}">
  
      <!-- Configurer un bouton qui permet de vider la cache du parametre systeme -->
      <sofi-html:bouton
        href="parametre.do?methode=initialiserParametreSysteme"
        libelle="infra_sofi.libelle.gestion_parametre.bouton.initialiser_cache" />
  
      <sofi-html:submit libelle="infra_sofi.libelle.gestion_parametre.bouton.creer"
        href="parametre.do?methode=ajouter" ignorerValidation="true" />
  
      <!-- Configurer un message de confirmation avec variable dynamique -->
      <sofi-html:bouton href="parametre.do?methode=supprimer"
        libelle="infra_sofi.libelle.gestion_parametre.bouton.supprimer"
        messageConfirmation="infra_sofi.confirmation.gestion_message.suppression" />
    </c:if>

    <sofi-html:submit libelle="infra_sofi.libelle.gestion_parametre.bouton.enregistrer" 
      property="enregistrer" styleClass="bouton" />
    </div>
  </fieldset>
</sofi-html:form>

<script>
  $("#codeClient").select2({containerCssClass: "select_grand"});
</script>