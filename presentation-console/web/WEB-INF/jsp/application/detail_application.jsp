<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<p align="right" class="champsobligatoire">
  <span class="ind_obligatoire">*</span> 
  <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire"/>
</p>
<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion.application.section.detail.objet.java"/>
  </legend>
  <sofi-html:form action="/application.do?methode=enregistrer" transactionnel="true">
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <sofi-html:text libelle="infra_sofi.libelle.gestion_systeme.champ.code" 
                      property="codeApplication" obligatoire="true" size="25" maxlength="20" 
                      affichageSeulement="${applicationForm.nouveauFormulaire != 'true'}" />
      <sofi-html:text libelle="infra_sofi.libelle.gestion_systeme.champ.nom" 
                      property="nom" obligatoire="true" size="70" maxlength="200" />
      <sofi-html:text libelle="infra_sofi.libelle.gestion_systeme.champ.acronyme"
                      property="acronyme" size="30" maxlength="30" />     
      <sofi-html:textarea libelle="infra_sofi.libelle.gestion_systeme.champ.description" 
                          property="description" rows="4" cols="70"/>
      <sofi-html:checkbox property="commun" libelle="infra_sofi.libelle.gestion_systeme.champ.commun" value="true" />    
      <sofi-html:text libelle="infra_sofi.libelle.gestion_systeme.champ.versionLivraison" 
                      property="versionLivraison" size="5" maxlength="5" />
      <sofi-html:date libelle="infra_sofi.libelle.gestion_systeme.champ.dateDebut" 
                      property="dateDebutActivite"/>
      <sofi-html:date libelle="infra_sofi.libelle.gestion_systeme.champ.dateFin" 
                      property="dateFinActivite"/>
      <sofi-html:select libelle="infra_sofi.libelle.gestion_systeme.champ.statut" 
                                        property="statut"
                                        cache="listeStatutActivation"
                                        affichageSeulement="true" />
      <sofi-html:text libelle="infra_sofi.libelle.gestion_systeme.champ.adresseSite" 
                      property="adresseSite" 
                      size="70"
                      maxlength="500" />
      <sofi-html:textarea libelle="infra_sofi.libelle.gestion_systeme.champ.adresseCache" 
                      property="adresseCache" 
                      rows="4"
                      cols="70"
                      maxlength="2000"
                      iconeAide="true" />
      <sofi-html:text libelle="infra_sofi.libelle.gestion_systeme.champ.codeUtilisateurAdministrateur" 
                      property="codeUtilisateurAdministrateur" 
                      size="40" 
                      maxlength="200" />
      <sofi-html:password libelle="infra_sofi.libelle.gestion_systeme.champ.motPasseAdministrateur" 
                          property="motPasseAdministrateur" 
                          size="40" 
                          maxlength="30" />
      <jsp:include page="../trace.jsp" />
    </table>
    <div class="barreBouton">
      <c:if test="${!formulaireEnCours.nouveauFormulaire}">
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion_systeme.action.supprimer" href="application.do?methode=supprimer" messageConfirmation="infra_sofi.libelle.gestion_libelle.confirmation_suppression.application" />  
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion_systeme.action.creer" href="application.do?methode=creer"/>
      </c:if>
      <sofi-html:submit libelle="infra_sofi.libelle.gestion.systeme.action.enregistrer" href="application.do?methode=enregistrer" />  
    </div>    
  </sofi-html:form>
</fieldset>