<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<fieldset>
  <legend>
  	<sofi:libelle identifiant="infra_sofi.libelle.gestion_systeme.section.recherche"/>
 	</legend>
  <sofi-html:form action="/rechercheApplication.do?methode=rechercher" transactionnel="false">
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <sofi-html:text libelle="infra_sofi.libelle.gestion_systeme.champ.code" 
                  property="codeApplication" size="20"/>
      <sofi-html:text libelle="infra_sofi.libelle.gestion_systeme.champ.nom" 
                      property="nom" size="80"/>
      <sofi-html:select property="statut" ligneVide="true" libelle="infra_sofi.libelle.gestion_systeme.champ.statut" cache="listeStatutActivation" />
      <sofi-html:checkbox property="commun" libelle="infra_sofi.libelle.gestion_systeme_recherche.champ.commun" 
                          libelleAssocie="true" value="true" />
    </table>
    <div class="barreBouton">
      <sofi-html:bouton libelle="infra_sofi.libelle.gestion_systeme.action.creer" href="application.do?methode=creer"/>
      <sofi-html:submit libelle="infra_sofi.libelle.commun.action.rechercher" />
    </div>
  </sofi-html:form>
</fieldset>

<c:if test="${listeApplication != null}">
  <fieldset>
    <legend>
    	<sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche"/>
		</legend>
    <jsp:include page="ajax_liste_application.jsp" />
  </fieldset>
</c:if>
