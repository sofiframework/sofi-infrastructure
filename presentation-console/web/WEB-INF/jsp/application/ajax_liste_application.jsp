<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi-liste:listeNavigation action="rechercheApplication.do?methode=rechercher"
                          nomListe="${listeApplication}"
                          triDefaut="2"
                          triDescendantDefaut="false"
                          class="tableresultat"
                          id="application"
                          trierPageSeulement="false"
                          ajax="true"
                          libelleResultat="infra_sofi.libelle.gestion_systeme.liste_resultat_element"
                          iterateurMultiPage="true"
                          colgroup="10%,30%,48%,7%,5%"
                          varIndex="compteur"
                          colonneLienDefaut="2">
       
  <sofi-liste:colonne property="codeApplication" trier="true" 
                      libelle="infra_sofi.libelle.gestion_systeme.colonne.code"
                      />
                          
  <sofi-liste:colonne property="nom" trier="true" 
                      libelle="infra_sofi.libelle.gestion_systeme.colonne.nom"
                      href="application.do?methode=consulter"
                      paramId="codeApplication"
                      paramProperty="codeApplication"
                      />
                      
  <sofi-liste:colonne property="adresseSite" 
                      libelle="infra_sofi.libelle.gestion_systeme.colonne.url" 
                      />
                      
  <sofi-liste:colonne property="versionLivraison" 
                      libelle="Version" 
                      />                      
                      
  <sofi-liste:colonne property="commun" 
                      libelle="infra_sofi.libelle.gestion_systeme.colonne.commun" 
                      trier="true"
                      traiterCorps="true"
                      >
    <c:if test="${application.commun}" >
      <img src="images/sofi/checkbox.gif" border="0" />
    </c:if>
  </sofi-liste:colonne>
</sofi-liste:listeNavigation>  