<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<sofi:taille var="nbListeCache" valeur="${listeCachesApplication}" />
<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion.systeme.bloc.gestion_cache"/>
  </legend>
  <c:if test="${LISTE_FACETTE_APPLICATION != null}" >   
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">     
       <sofi-html:select
          property="codeFacette"
          libelle="infra_sofi.libelle.gestion_referentiel.champ.codeFacette"
          collection="${LISTE_FACETTE_APPLICATION}"
          proprieteEtiquette="description"
          proprieteValeur="valeur"
          ligneVide="false"
          href="cache.do?methode=afficher"
          nomParametreValeur="codeFacette" 
          transactionnel="false" 
        />
    </table>
  </c:if>
  <c:if test="${nbListeCache > 0}">
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
  
        <!-- Définition du lien qui permet de traiter Ajax -->
        <c:url var="url" value="/cache.do">
          <c:param name="methode" value="rechargerCache"/>
        </c:url> 
        
        
          
        <!-- Définition de la commande JavaScript qui va lancer le traitement Ajax dans la liste deroulante nomCacheReinitialisee -->
        <sofi-html:link var="urlAjaxInitialisationCache"
                        href="${url}" 
                        ajax="true"
                        barreStatutAjax="true"
                        divBarreStatutAjax="message_erreur"
                        nomParametreValeur="nomCache"
                        idProprieteValeur="nomCacheReinitialisee"
                        />
  
        <sofi-html:select libelle="infra_sofi.libelle.gestion.systeme.champ.selection_nom_objet" 
                          styleId="nomCacheReinitialisee"
                          obligatoire="false" 
                          ligneVide="false" 
                          collection="${listeCachesApplication}" 
                          proprieteEtiquette="objetValeur" 
                          proprieteValeur="objetCle"
                          size="10"
                          onchange="${urlAjaxInitialisationCache}"
                          >
        </sofi-html:select>
    </table>

   <div class="barreBouton">  
      <!-- Configurer un bouton qui permet d'initialiser tous les messages -->
      <sofi-html:bouton href="cache.do?methode=initialiserTousMessages" 
                        libelleMessageAttente="infra_sofi.libelle.gestion.systeme.action.initialisation_messages"
                        libelle="infra_sofi.libelle.gestion.systeme.action.libelle_btn_messages" 
       />
      <!-- Configurer un bouton qui permet d'initialiser tous les libellés -->
      <sofi-html:bouton   href="cache.do?methode=initialiserTousLibelles" 
                          libelleMessageAttente="infra_sofi.libelle.gestion.systeme.action.initialisation_libelles"
                          libelle="infra_sofi.libelle.gestion.systeme.action.libelle_btn_libelles" 
       />
      <!-- Configurer un bouton qui permet d'initialiser tous les aides -->
      <sofi-html:bouton  href="cache.do?methode=initialiserTousAides" 
								          libelle="infra_sofi.libelle.gestion.systeme.action.libelle_aide_ligne" 
								          libelleMessageAttente="infra_sofi.libelle.gestion.systeme.action.initialisation_aide_ligne"
       /> 
    </div>
   </c:if>
  </fieldset>
