<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<sofi-html:form action="/proprieteDynamique.do?methode=enregistrerProprieteDynamique" transactionnel="false">
    
    <fieldset>
      <legend>
        <sofi:libelle identifiant="infra_sofi.libelle.gestion.section.propriete_dynamique"/>
      </legend>
      <table id="tableProprieteDynamique">
        <tr>
          <th><sofi:libelle identifiant="infra_sofi.libelle.gestion.propriete_dynamique.key"/></th>
          <th><sofi:libelle identifiant="infra_sofi.libelle.gestion.propriete_dynamique.value"/></th>
          <th></th>
        </tr>
            
        <sofi-nested:iterate property="proprieteDynamiqueForm" indexId="index">
          <tr>
            <sofi-html:link var="ajaxModifierKey" href="proprieteDynamique.do?methode=modifierKey&index=${index}" ajax="true" 
                 idProprieteValeur="key" nomParametreValeur="key" />
            <sofi-html:link var="ajaxModifierValue" href="proprieteDynamique.do?methode=modifierValue&index=${index}" ajax="true" 
                 idProprieteValeur="value" nomParametreValeur="value" />
          
            <th><sofi-nested:text property="key" onchange="${ajaxModifierKey}" obligatoire="true" maxlength="100" size="25" fermerLigne="false" ouvrirLigne="false"/></th>
            <th><sofi-nested:text property="value" onchange="${ajaxModifierValue}" obligatoire="true" maxlength="200" size="75" ouvrirLigne="false" fermerLigne="false"/></th>
            <th>     
              <sofi-html:link var="lienSupprimerProprieteDynamique"
                      ajax="true" 
                      href="proprieteDynamique.do?methode=supprimerProprieteDynamique&index=${index}"
                      divRetourAjax="DIV_PROPRIETE_DYNAMIQUE"/>
              <sofi-html:bouton libelle="" 
                              styleClass="boutonSuppression"
                              messageConfirmation="infra_sofi.confirmation.gestion.supprimer.propriete_dynamique"  
                              onclick="${lienSupprimerProprieteDynamique}"/>  
            </th>
          </tr>
        </sofi-nested:iterate>
      </table>
      <sofi-html:link var="lienAjoutProprieteDynamique"
                      ajax="true" 
                      href="proprieteDynamique.do?methode=ajouterProprieteDynamique"
                      divRetourAjax="DIV_PROPRIETE_DYNAMIQUE"/>
        <sofi-html:bouton styleClass="boutonAjouter" libelle="" onclick="${lienAjoutProprieteDynamique}" />
    </fieldset>
    
    <sofi-html:link var="lienEnregistrer"
                      ajax="true" 
                      href="proprieteDynamique.do?methode=enregistrerProprieteDynamique"/>
    
    <div class="barreBouton">
      <sofi-html:bouton 
        onclick="${lienEnregistrer};notifierFormulaireReference();$('#DIV_PROPRIETE_DYNAMIQUE').dialog('close');"
        libelle="infra_sofi.libelle.gestion.propriete_dynamique.action.appliquer" 
        disabled="${listeProprieteDynamiqueForm.formulaireDeReference.modeLectureSeulement}"/>
      <sofi-html:bouton onclick="$('#DIV_PROPRIETE_DYNAMIQUE').dialog('close');" libelle="infra_sofi.libelle.gestion.tache.action.fermer"/>
    </div>
        
 
    
  </sofi-html:form>  