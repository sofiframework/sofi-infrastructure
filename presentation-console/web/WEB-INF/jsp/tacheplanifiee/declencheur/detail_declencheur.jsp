<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<script>
	function notifierFormulaireReference() {
		notifierModificationFormulaire('declencheurForm');
	}
</script>

<p align="right" class="champsobligatoire">
  <span class="ind_obligatoire">*</span>
  <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire" />
</p>
<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion.declencheur.section.detail.objet.java" />
  </legend>
  <sofi-html:form action="/declencheur.do?methode=enregistrer" transactionnel="true">
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">

      <sofi-html:select property="type" ligneVide="false"
        libelle="infra_sofi.libelle.gestion_declencheur.champ.type" cache="listeTypeTrigger"
        proprieteValeur="valeur" href="declencheur.do?methode=fixerTypeDeclencheur" ajax="true"
        nomParametreValeur="typeDeclencheur" divRetourAjax="ajax_type_declencheur"
        affichageSeulement="${!formulaireEnCours.nouveauFormulaire}" />

      <sofi-html:text libelle="infra_sofi.libelle.gestion_declencheur.champ.nom" property="nom"
        obligatoire="true" size="80" maxlength="200"
        affichageSeulement="${!formulaireEnCours.nouveauFormulaire}" />

      <sofi-html:text libelle="infra_sofi.libelle.gestion_declencheur.champ.groupe"
        property="groupe" obligatoire="true" size="80" maxlength="200" />

      <sofi-html:textarea libelle="infra_sofi.libelle.gestion_declencheur.champ.description"
        property="description" rows="4" cols="100" maxlength="250" />

      <sofi-html:checkbox property="triggerVolatile"
        libelle="infra_sofi.libelle.gestion_declencheur.champ.volatile" />
      <tr>
        <th></th>
        <td>
          <a href="javascript:consulterProprieteDynamique('2');"> <sofi:libelle
              identifiant="infra_sofi.libelle.gestion_declencheur.hashmap" />
          </a>
        </td>
      </tr>

      <sofi-html:text libelle="infra_sofi.libelle.gestion_declencheur.champ.priorite"
        property="priorite" obligatoire="false" size="5" maxlength="13" />

    </table>

    <div id="ajax_type_declencheur">
      <c:choose>
        <c:when test="${formulaireEnCours.type eq 'SIMPLE'}">
          <%-- Bug bizarre : 
               Lors du 1er accès à un ObjetCache de type ListeDomaineValeur, la collection de valeur n'est pas chargée mais lors des appels suivants tout est correct. 
               On fait donc un 1er appel ici ; ainsi le vrai appel dans ajax_declencheur_simple.jsp fonctionnement correctement. 
          --%>
          <sofi:cache cache="listeMissFireSimpleTrigger" var="cacheMissFireTrigger" />
          <jsp:include page="ajax_declencheur_simple.jsp" />
        </c:when>
        <c:when test="${formulaireEnCours.type eq 'CRON'}">
          <%-- Bug bizarre : 
               Lors du 1er accès à un ObjetCache de type ListeDomaineValeur, la collection de valeur de valeur n'est pas chargée mais lors des appels suivants tout est correct. 
               On fait donc un 1er appel ici ; ainsi le vrai appel dans ajax_declencheur_simple.jsp fonctionnement correctement. 
          --%>
          <sofi:cache cache="listeMissFireCronTrigger" var="cacheMissFireTrigger" />
          <jsp:include page="ajax_declencheur_cron.jsp" />
        </c:when>
      </c:choose>
    </div>

    <div class="barreBouton">
      <c:if test="${!formulaireEnCours.nouveauFormulaire}">
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion_declencheur.action.supprimer"
          href="declencheur.do?methode=supprimer"
          messageConfirmation="infra_sofi.confirmation.gestion_declencheur.suppression" />
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion_declencheur.action.creer"
          href="declencheur.do?methode=creer" />
      </c:if>
      <sofi-html:submit libelle="infra_sofi.libelle.gestion.declencheur.action.enregistrer"
        href="declencheur.do?methode=enregistrer" />
    </div>
  </sofi-html:form>
</fieldset>

<div id="DIV_PROPRIETE_DYNAMIQUE"></div>
