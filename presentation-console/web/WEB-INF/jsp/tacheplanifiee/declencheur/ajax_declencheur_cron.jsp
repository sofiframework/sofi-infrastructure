<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<sofi:cache cache="listeMissFireCronTrigger" var="cacheMissFireTrigger"/>

<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion.declencheur.section.declencheur.cron" />
  </legend>
  <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
    <sofi-nested:nest property="enfant">
      <sofi-nested:text libelle="infra_sofi.libelle.gestion_declencheur.champ.expression_cron"
        property="expression" obligatoire="true" size="50" maxlength="120" />
    </sofi-nested:nest>
    <tr>
      <th>
        <sofi:libelle identifiant="infra_sofi.libelle.gestion_declencheur.champ.missfired"
          obligatoire="true" nomAttribut="appeleManque" />
      </th>
      <td>
        <c:forEach items="${cacheMissFireTrigger}" var="domaine">
          <sofi-nested:radio value="${domaine.value.valeur}" property="appeleManque"
            libelle="${domaine.value.listeDescriptionLangue[codeLangueEnCours]}" />
          <sofi:libelle
            identifiant="infra_sofi.libelle.gestion_declencheur.champ.cron_missfired.${domaine.value.valeur}"
            iconeAide="true" />
          <br />
        </c:forEach>
      </td>
    </tr>
  </table>
</fieldset>
