<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion.declencheur.section.declencheur.simple" />
  </legend>
  <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
    <sofi-nested:nest property="enfant">
      <sofi-nested:text libelle="infra_sofi.libelle.gestion_declencheur.champ.nombre_execution"
        property="nombreExecution" obligatoire="true" size="20" maxlength="7" formatSaisie="0" />

      <sofi-nested:text libelle="infra_sofi.libelle.gestion_declencheur.champ.interval"
        property="interval" obligatoire="true" size="20" maxlength="12" formatSaisie="0" />
    </sofi-nested:nest>

    <tr>
      <th>
        <sofi:libelle identifiant="infra_sofi.libelle.gestion_declencheur.champ.missfired"
          obligatoire="true" nomAttribut="appeleManque" />
      </th>
      <td>
        <sofi:cache cache="listeMissFireSimpleTrigger" var="cacheMissFireTrigger" />
        <c:forEach items="${cacheMissFireTrigger}" var="domaine">
          <sofi-nested:radio value="${domaine.value.valeur}" property="appeleManque"
            libelle=" ${domaine.value.listeDescriptionLangue[codeLangueEnCours]}" />
          <sofi:libelle
            identifiant="infra_sofi.libelle.gestion_declencheur.champ.simple_missfired.${domaine.value.valeur}"
            iconeAide="true" />
          <br />
        </c:forEach>
      </td>
    </tr>
  </table>
</fieldset>
