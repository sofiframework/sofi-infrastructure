<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion_tache.section.recherche"/>
  </legend>
  
  <sofi-html:form action="/rechercheJob.do?methode=rechercher" transactionnel="false">
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
    
      <sofi-html:select 
          property="codeApplication" 
          ligneVide="false"
          libelle="infra_sofi.libelle.gestion_tache.champ.codeApplication" 
          collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          proprieteEtiquette="[nom] ([codeApplication])"
          proprieteValeur="codeApplication"
          href="rechercheJob.do?methode=fixerApplication"
          ajax="true"
          nomParametreValeur="codeApplication"
          divRetourAjax="liste_deroulante_ordonnanceur"
          />
    <tr>
    
        <th><sofi:libelle identifiant="infra_sofi.libelle.gestion_tache.champ.codeScheduler"/></th>
        <td>
        <div id="liste_deroulante_ordonnanceur">
        <sofi-html:select
          property="codeScheduler" 
          ligneVide="false"
          collection="${LISTE_ORDONNANCEUR}"
          proprieteEtiquette="nom"
          proprieteValeur="codeScheduler"
          ouvrirLigne="false"
          fermerLigne="false"
          />
          </div>
          </td>
     
   </tr>
      <sofi-html:text libelle="infra_sofi.libelle.gestion_tache.champ.groupe" 
                      property="groupe" size="80"/>
      
      <sofi-html:text libelle="infra_sofi.libelle.gestion_tache.champ.nom" 
                      property="nom" size="80"/>
                      
      
    </table>
    <div class="barreBouton">
      <sofi-html:bouton libelle="infra_sofi.libelle.gestion_tache.action.creer" href="job.do?methode=creer"/>
      <sofi-html:submit libelle="infra_sofi.libelle.commun.action.rechercher" />
    </div>
  </sofi-html:form>
</fieldset>

<c:if test="${listeJob != null}">
  <fieldset>
    <legend>
      <sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche"/>
    </legend>
    <jsp:include page="ajax_liste_job.jsp" />
  </fieldset>
</c:if>