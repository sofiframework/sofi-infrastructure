<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

  <sofi-html:select
          property="codeScheduler" 
          ligneVide="false"
          collection="${LISTE_ORDONNANCEUR}"
          proprieteEtiquette="nom"
          proprieteValeur="codeScheduler"
          ouvrirLigne="false"
          fermerLigne="false"
          />