<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<script>
function notifierFormulaireReference(){
  notifierModificationFormulaire('jobForm');
  }
</script>
  <p align="right" class="champsobligatoire">
    <span class="ind_obligatoire">*</span> 
    <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire"/>
  </p>
  <fieldset>
    <legend>
      <sofi:libelle identifiant="infra_sofi.libelle.gestion.tache.section.detail.objet.java"/>
    </legend>
    <sofi-html:form action="/job.do?methode=enregistrer" transactionnel="true">
      
      <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
        
        <c:if test="${formulaireEnCours.nouveauFormulaire}">
          <sofi-html:select 
            property="codeApplication" 
            ligneVide="false"
            libelle="infra_sofi.libelle.gestion_tache.champ.codeApplication" 
            collection="${LISTE_APPLICATIONS_UTILISATEUR}"
            proprieteEtiquette="[nom] ([codeApplication])"
            proprieteValeur="codeApplication"
            href="job.do?methode=fixerApplication"
            ajax="true"
            nomParametreValeur="codeApplication"
            divRetourAjax="liste_deroulante_ordonnanceur"
            />
          
          <tr>
            <th><sofi:libelle identifiant="infra_sofi.libelle.gestion_tache.champ.codeScheduler" obligatoire="true" nomAttribut="codeScheduler"/></th>
            <td>
              <div id="liste_deroulante_ordonnanceur">
                <sofi-html:select
                      property="codeScheduler" 
                      ligneVide="false"
                      collection="${LISTE_ORDONNANCEUR}"
                      proprieteEtiquette="nom"
                      proprieteValeur="codeScheduler"
                      ouvrirLigne="false"
                      fermerLigne="false" />
              </div>
            </td>
          </tr>
        </c:if>
        
        <c:if test="${!formulaireEnCours.nouveauFormulaire}">
          <sofi-html:text libelle="infra_sofi.libelle.gestion_tache.champ.codeApplication" 
                        property="codeApplication" obligatoire="true" size="25" maxlength="20" 
                        affichageSeulement="${formulaireEnCours.nouveauFormulaire != 'true'}" />
                        
          <sofi-html:text libelle="infra_sofi.libelle.gestion_tache.champ.codeScheduler" 
                        property="codeScheduler" obligatoire="true" size="25" maxlength="20" 
                        affichageSeulement="${formulaireEnCours.nouveauFormulaire != 'true'}" />                      
        </c:if>              
                        
        <sofi-html:text libelle="infra_sofi.libelle.gestion_tache.champ.nom" 
                        property="nom" obligatoire="true" size="80" maxlength="200" 
                        affichageSeulement="${!formulaireEnCours.nouveauFormulaire and liste_job_trigger.nbListe > 0}"/>
                        
        <sofi-html:text libelle="infra_sofi.libelle.gestion_tache.champ.groupe" 
                        property="groupe" obligatoire="true" size="80" maxlength="200" 
                        affichageSeulement="${!formulaireEnCours.nouveauFormulaire and liste_job_trigger.nbListe > 0}"/>                      
                        
        <sofi-html:textarea libelle="infra_sofi.libelle.gestion_tache.champ.description" 
                            property="description" rows="4" cols="100"/>
                            
        <sofi-html:text libelle="infra_sofi.libelle.gestion_tache.champ.nom_classe" 
                        property="nomClasse" obligatoire="true" size="80" maxlength="250" >
          
          <a href="javascript:consulterProprieteDynamique('1');">
            <sofi:libelle identifiant="infra_sofi.libelle.gestion_tache.hashmap"/>
          </a>
        </sofi-html:text>
                                                     
        
        <sofi-html:checkbox property="jobDurable" libelle="infra_sofi.libelle.gestion_tache.champ.durable"/>                  
        <sofi-html:checkbox property="jobVolatile" libelle="infra_sofi.libelle.gestion_tache.champ.volatile"/>
        <c:if test="${!formulaireEnCours.nouveauFormulaire}">
	        <sofi-html:checkbox property="jobPermanente" libelle="infra_sofi.libelle.gestion_tache.champ.etat_persistent" disabled="true"> 
	          <sofi:libelle identifiant="infra_sofi.libelle.gestion_tache.aide.statefuljob"/>
	        </sofi-html:checkbox>
        </c:if>
        <sofi-html:checkbox property="jobRequestRecovery" libelle="infra_sofi.libelle.gestion_tache.champ.relancer_interuption"/>
        
      </table>
      
      <div class="barreBouton">
        <c:if test="${!formulaireEnCours.nouveauFormulaire}">
          <sofi-html:bouton libelle="infra_sofi.libelle.gestion_tache.action.supprimer" 
                            disabled ="${liste_job_trigger.nbListe > 0}"
                            href="job.do?methode=supprimer" 
                            messageConfirmation="infra_sofi.confirmation.gestion_tache.suppression" />  
          <sofi-html:bouton libelle="infra_sofi.libelle.gestion_tache.action.creer" href="job.do?methode=creer" 
                            actifSiFormulaireLectureSeulement="true" />
        </c:if>
        <sofi-html:submit styleId="boutonSubmit" libelle="infra_sofi.libelle.gestion.tache.action.enregistrer" />  
      </div>    
    </sofi-html:form>
  </fieldset>
  
  <c:if test="${!formulaireEnCours.nouveauFormulaire}">
    <fieldset>
      <legend>
        <sofi:libelle identifiant="infra_sofi.libelle.gestion.tache.section.declencheur.objet.java"/>
      </legend>
      <div id="liste_declencheur" >
        <jsp:include page="ajax_liste_declencheur.jsp" />
      </div>
      <div class="barreBouton">
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion_tache.action.ajouter.declencheur" href="declencheur.do?methode=creer" />
      </div>  
     </fieldset>
  </c:if>

<div id="DIV_PROPRIETE_DYNAMIQUE">
</div>
