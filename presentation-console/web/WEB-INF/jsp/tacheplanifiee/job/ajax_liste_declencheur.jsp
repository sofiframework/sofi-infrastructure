 <%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
 
<sofi-liste:listeNavigation action="job.do?methode=getListeTrigger"
                          nomListe="${liste_job_trigger}"
                          triDefaut="1"
                          triDescendantDefaut="false"
                          class="tableresultat"
                          id="declencheur"
                          trierPageSeulement="false"
                          ajax="true"
                          libelleResultat="infra_sofi.libelle.gestion_tache.liste_declencheur_element"
                          iterateurMultiPage="true"
                          colgroup="10%,15%,15%,60%"
                          colonneLienDefaut="3"
                          >
  
  <sofi-liste:colonne property="type" 
                      libelle="infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.type"/>
  
  <sofi-liste:colonne property="groupe" 
                      libelle="infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.groupe"/>
                      
  <sofi-liste:colonne property="nom" trier="true" 
                      libelle="infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.nom"
                      href="declencheur.do?methode=consulter"
                      paramId="declencheurId"
                      paramProperty="id">
  </sofi-liste:colonne>
  
  <sofi-liste:colonne property="description" 
                      libelle="infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.description" >
  </sofi-liste:colonne>
  
  <sofi-liste:colonneLibre libelle="infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.actif" >
    ${declencheur.actif} (${declencheur.etat})
  </sofi-liste:colonneLibre>
  
 </sofi-liste:listeNavigation> 
 