<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi-liste:listeNavigation action="rechercheJob.do?methode=rechercher"
                          nomListe="${listeJob}"
                          triDefaut="1"
                          triDescendantDefaut="false"
                          class="tableresultat"
                          id="job"
                          trierPageSeulement="false"
                          ajax="true"
                          libelleResultat="infra_sofi.libelle.gestion_tache.liste_resultat_element"
                          iterateurMultiPage="true"
                          colgroup="20%,20%,60%"
                          colonneLienDefaut="2"
                          >
  
  <sofi-liste:colonne property="groupe" 
                      libelle="infra_sofi.libelle.gestion_tache.colonne.groupe"/>
                      
  <sofi-liste:colonne property="nom" trier="true" 
                      libelle="infra_sofi.libelle.gestion_tache.colonne.nom"
                      href="job.do?methode=consulter"
                      paramId="jobId"
                      paramProperty="id">
  </sofi-liste:colonne>
  
  <sofi-liste:colonne property="description" 
                      libelle="infra_sofi.libelle.gestion_ordonnanceur.colonne.description">
  </sofi-liste:colonne>
  
  <sofi-liste:colonne property="actif" 
                      libelle="infra_sofi.libelle.gestion_tache.colonne.actif">
  </sofi-liste:colonne>
  
</sofi-liste:listeNavigation>  