<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@page import="java.util.Date"%>

<%-- Liste des job  
////////////////// --%>
<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion.ordonnanceur.section.tableau_de_bord.liste_tache"/>
  </legend>

  <sofi-liste:listeNavigation 
    action="ordonnanceur.do?methode=acceder"
    nomListe="${listeJobTableauDeBord}"
    triDefaut="1"
    triDescendantDefaut="false"
    class="tableresultat"
    id="job"
    ajax="true"
    libelleResultat="infra_sofi.libelle.gestion_tache.liste_resultat_element"
    colgroup="45%,15%,15%,15%,10%">
    
    <sofi-liste:colonne property="nom"
                        libelle="infra_sofi.libelle.gestion_tache.colonne.nom" />
    
    <sofi-liste:colonne property="tempDerniereExecution"
                        libelle="infra_sofi.libelle.gestion_tache.colonne.temps_derniere_execution" />

    <sofi-liste:colonneLibre libelle="infra_sofi.libelle.gestion_tache.colonne.temps_moyen_execution">
      <c:choose>
	      <c:when test="${job.jobStateful || job.jobPermanente}">
	        <a href="javascript:consulterTempsExecutions('${job.id}');">
	          ${job.tempMoyenExecution}
	        </a>
	      </c:when>
	      <c:otherwise>
          <sofi:libelle identifiant="infra_sofi.libelle.gestion_tache.colonne.tache_non_stateful"/> 
	      </c:otherwise>
      </c:choose>
    </sofi-liste:colonneLibre>
       
    <sofi-liste:colonne property="actif" 
                        libelle="infra_sofi.libelle.gestion_tache.colonne.actif" />
    
    <sofi-liste:colonneLibre libelle="infra_sofi.libelle.gestion_ordonnanceur.section.tableau_de_bord.liste_declencheur.colonne.action" >
      <c:url var="urlActiver" value="tableauDeBord.do" >
        <c:param name="methode" value="activerJob" />
        <c:param name="id" value="${job.id}" />
        <c:param name="schedulerId" value="${job.schedulerId}" />
      </c:url> 
  
      <c:url var="urlDesactiver" value="tableauDeBord.do" >
        <c:param name="methode" value="desactiverJob" />
        <c:param name="id" value="${job.id}" />
        <c:param name="schedulerId" value="${job.schedulerId}" />
      </c:url> 
  
      <sofi-html:link
        var="ajaxActiverJob"
        ajax="true"
        href="${urlActiver}" 
        divRetourAjax="liste_job_et_declencheur" />  
        
      <sofi-html:link
        var="ajaxDesactiverJob"
        ajax="true"
        href="${urlDesactiver}" 
        divRetourAjax="liste_job_et_declencheur" />  
      
      <sofi-html:bouton 
        libelle=">"
        aideContextuelle="infra_sofi.libelle.gestion_ordonnanceur.liste_tache.colonne.action.activer"
        styleClass="bouton"  
        onclick="${ajaxActiverJob}" 
        disabled="${job.actif}" />  
      
      <sofi-html:bouton 
        libelle="||"
        aideContextuelle="infra_sofi.libelle.gestion_ordonnanceur.liste_tache.colonne.action.desactiver"
        styleClass="bouton"  
        onclick="${ajaxDesactiverJob}" 
        disabled="${!job.actif}" />  
      
    </sofi-liste:colonneLibre>
    
  </sofi-liste:listeNavigation>  
</fieldset>

<%-- Liste des déclencheurs  
/////////////////////////// --%>  
<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion.ordonnanceur.section.tableau_de_bord.liste_declencheur"/>
  </legend>
  <sofi-liste:listeNavigation 
    action="ordonnanceur.do?methode=acceder"
    nomListe="${listeDeclencheurTableauDeBord}"
    triDefaut="1"
    triDescendantDefaut="false"
    class="tableresultat"
    id="declencheur"
    ajax="true"
    libelleResultat="infra_sofi.libelle.gestion_tache.liste_declencheur_element"
    colgroup="20%,15%,20%,20%,15%,10%">
  
    <sofi-liste:colonne property="nom"
                        libelle="infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.nom" />
    
    <sofi-liste:colonne property="job.nom"
                        libelle="infra_sofi.libelle.gestion_tache.colonne.nom" />
    
    <sofi-liste:colonneLibre libelle="infra_sofi.libelle.gestion_ordonnanceur.liste_declencheur.colonne.date_derniere_execution">
      <fmt:formatDate value="${declencheur.dateDerniereExecution}" pattern="yyyy-MM-dd HH:mm:ss"/>
    </sofi-liste:colonneLibre>
    
    <sofi-liste:colonneLibre libelle="infra_sofi.libelle.gestion_ordonnanceur.liste_declencheur.colonne.date_prochaine_execution">
       <fmt:formatDate value="${declencheur.dateProchaineExecution}" pattern="yyyy-MM-dd HH:mm:ss"/>
    </sofi-liste:colonneLibre>
    
    
    <sofi-liste:colonneLibre libelle="infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.actif" >
      ${declencheur.actif} (${declencheur.etat})
    </sofi-liste:colonneLibre>
    
    <sofi-liste:colonneLibre libelle="infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.action" >
      <c:url var="urlActiver" value="tableauDeBord.do" >
        <c:param name="methode" value="activerDeclencheur" />
        <c:param name="id" value="${declencheur.id}" />
        <c:param name="schedulerId" value="${declencheur.job.schedulerId}" />
      </c:url> 
  
      <c:url var="urlDesactiver" value="tableauDeBord.do" >
        <c:param name="methode" value="desactiverDeclencheur" />
        <c:param name="id" value="${declencheur.id}" />
        <c:param name="schedulerId" value="${declencheur.job.schedulerId}" />
      </c:url> 
  
      <sofi-html:link
        var="ajaxActiverDeclencheur"
        ajax="true"
        href="${urlActiver}" 
        divRetourAjax="liste_job_et_declencheur" />  
        
      <sofi-html:link
        var="ajaxDesactiverDeclencheur"
        ajax="true"
        href="${urlDesactiver}" 
        divRetourAjax="liste_job_et_declencheur" />  
      
      <sofi-html:bouton 
        libelle=">"
        aideContextuelle="infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.action.activer"
        styleClass="bouton"  
        onclick="${ajaxActiverDeclencheur}" 
        disabled="${declencheur.actif}" />  
      
      <sofi-html:bouton 
        libelle="||"
        aideContextuelle="infra_sofi.libelle.gestion_tache.liste_declencheur.colonne.action.desactiver"
        styleClass="bouton"  
        onclick="${ajaxDesactiverDeclencheur}" 
        disabled="${!declencheur.actif}" />  
      
    </sofi-liste:colonneLibre>
    
  </sofi-liste:listeNavigation>  
</fieldset>

<%
  Date maintenant = new Date();
  pageContext.setAttribute("maintenant", maintenant);
%>
<fmt:formatDate value="${maintenant}" pattern="HH:mm:ss" var="heure"/>
<sofi:libelle 
  identifiant="infra_sofi.libelle.gestion_ordonnanceur.tableau_de_bord.page_rafraichie"
  parametre1="${heure}" />
         
