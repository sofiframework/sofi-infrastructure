<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion.section.temps_execution"/>
  </legend>
    <table id="tableTempsExecution" class="tableresultat">
        <tr>
          <th><sofi:libelle identifiant="infra_sofi.libelle.gestion.temps_execution.derniereExecution"/></th>
          <th><sofi:libelle identifiant="infra_sofi.libelle.gestion.temps_execution.temps"/></th>
        </tr>
        <c:forEach items="${listeTempExecution}" var="stat" varStatus="i">
          <fmt:formatDate value="${stat.dateDebut}" pattern="dd-MM-yyyy HH:mm:ss" var="dateDebut"/>
          <c:choose>
            <c:when test="${(i.index % 2) eq 0}"> 
              <tr class="couleurpair">
            </c:when>
            <c:otherwise>
              <tr class="couleurimpair">
            </c:otherwise>
          </c:choose>
            <td>${dateDebut}</td>
            <td>${stat.tempsExecution}</td>
          </tr>
        </c:forEach>
    </table>
  
</fieldset>

<div class="barreBouton">
  <sofi-html:bouton onclick="$('#DIV_TEMPS_EXECUTION').dialog('close');" libelle="infra_sofi.libelle.gestion.tache.action.fermer"/>
</div>