<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<p align="right" class="champsobligatoire">
  <span class="ind_obligatoire">*</span> 
  <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire"/>
</p>
<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion.ordonnanceur.section.detail.objet.java"/>
  </legend>
  <sofi-html:form action="/ordonnanceur.do?methode=enregistrer" transactionnel="true">
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <c:if test="${formulaireEnCours.nouveauFormulaire}">
        <sofi-html:select 
          property="codeApplication" 
          ligneVide="false"
          libelle="infra_sofi.libelle.gestion_ordonnanceur.champ.codeApplication" 
          collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          proprieteEtiquette="[nom] ([codeApplication])"
          proprieteValeur="codeApplication"
          href="rechercheOrdonnanceur.do?methode=fixerApplication"
          ajax="true"
          nomParametreValeur="codeApplication"
          />
      </c:if>
      
      <c:if test="${!formulaireEnCours.nouveauFormulaire}">
        <sofi-html:text libelle="infra_sofi.libelle.gestion_ordonnanceur.champ.codeApplication" 
                      property="codeApplication" obligatoire="true" size="25" maxlength="20" 
                      affichageSeulement="${formulaireEnCours.nouveauFormulaire != 'true'}" />
       </c:if>              
      
       <sofi-html:text libelle="infra_sofi.libelle.gestion_ordonnanceur.champ.codeOrdonnanceur" 
                      property="codeScheduler" obligatoire="true" size="80" maxlength="20" 
                      affichageSeulement="${formulaireEnCours.nouveauFormulaire != 'true'}"/>
                      
      <sofi-html:text libelle="infra_sofi.libelle.gestion_ordonnanceur.champ.nom" 
                      property="nom" obligatoire="true" size="80" maxlength="200" />
                      
      <sofi-html:textarea libelle="infra_sofi.libelle.gestion_ordonnanceur.champ.description" 
                          property="description" rows="4" cols="100"/>

      <sofi-html:select libelle="infra_sofi.libelle.gestion_ordonnanceur.champ.statut" 
                        cache="listeStatutOrdonnanceur" 
                        property="statut" 
                        affichageSeulement="true" />

      <tr>
        <th class="libelle-formulaire"><sofi:libelle identifiant="infra_sofi.libelle.gestion_ordonnanceur.champ.heberge_par" /></th>
        <td>
          <c:forTokens items="${formulaireEnCours.objetTransfert.hebergePar}" delims="," var="hote">
            ${hote}<br />
          </c:forTokens>
        </td>
      </tr>
    </table>
    <div class="barreBouton">
      <c:if test="${!formulaireEnCours.nouveauFormulaire}">
        <sofi-html:bouton 
          libelle="infra_sofi.libelle.gestion_ordonnanceur.action.tableau_de_bord" 
          href="tableauDeBord.do?methode=consulter&schedulerId=${ordonnanceurForm.objetTransfert.id}" />  
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion_ordonnanceur.action.supprimer" href="ordonnanceur.do?methode=supprimer" messageConfirmation="infra_sofi.confirmation.gestion_ordonnanceur.suppression" />  
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion_ordonnanceur.action.creer" href="ordonnanceur.do?methode=creer"/>
      </c:if>
      <sofi-html:submit libelle="infra_sofi.libelle.gestion.ordonnanceur.action.enregistrer" href="ordonnanceur.do?methode=enregistrer" />  
    </div>    
  </sofi-html:form>
</fieldset>
