<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion_ordonnanceur.section.recherche"/>
  </legend>
  
  <sofi-html:form action="/rechercheOrdonnanceur.do?methode=rechercher" transactionnel="false">
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
    
      <sofi-html:select 
          property="codeApplication" 
          ligneVide="false"
          libelle="infra_sofi.libelle.gestion_ordonnanceur.champ.codeApplication" 
          collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          proprieteEtiquette="[nom] ([codeApplication])"
          proprieteValeur="codeApplication"
          href="rechercheOrdonnanceur.do?methode=fixerApplication"
          ajax="true"
          nomParametreValeur="codeApplication"
          />
    
      <sofi-html:text libelle="infra_sofi.libelle.gestion_ordonnanceur.champ.nom" 
                      property="nom" size="80"/>
                      
      <sofi-html:select property="statut" ligneVide="true" libelle="infra_sofi.libelle.gestion_ordonnanceur.champ.statut" cache="listeStatutOrdonnanceur" codeClient="CLI11"/>
    
    </table>
    <div class="barreBouton">
      <sofi-html:bouton libelle="infra_sofi.libelle.gestion_ordonnanceur.action.creer" href="ordonnanceur.do?methode=creer"/>
      <sofi-html:submit libelle="infra_sofi.libelle.commun.action.rechercher" />
    </div>
  </sofi-html:form>
</fieldset>

<c:if test="${listeOrdonnanceur != null}">
  <fieldset>
    <legend>
      <sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche"/>
    </legend>
    <jsp:include page="ajax_liste_ordonnanceur.jsp" />
  </fieldset>
</c:if>