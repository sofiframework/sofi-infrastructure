<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<sofi-html:form action="/tableauDeBord.do?methode=appliquer" transactionnel="false">
  <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
    <sofi-html:text libelle="infra_sofi.libelle.gestion_ordonnanceur.tableau_de_bord.delai" 
                    property="delaiRafraissementPage" size="5" maxlength="5" />
  </table>
  <div class="barreBouton">
    <sofi-html:submit libelle="infra_sofi.libelle.gestion.ordonnanceur.tableau_de_bord.action.appliquer" />         
  </div>
</sofi-html:form>

<sofi:div 
  id="liste_job_et_declencheur" 
  href="tableauDeBord.do?methode=rafraichir&schedulerId=${ordonnanceurForm.objetTransfert.id}"
  tempsRafraichissement="${tableauDeBordForm.delaiRafraissementPage}"
  ouvrirFermerDiv="false">
  <jsp:include page="ajax_liste_job_et_declencheur.jsp" />
</sofi:div>

<div id="DIV_TEMPS_EXECUTION">
</div>