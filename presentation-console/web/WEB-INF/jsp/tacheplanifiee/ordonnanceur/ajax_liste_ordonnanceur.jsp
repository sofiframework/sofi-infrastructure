<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<sofi-liste:listeNavigation action="rechercheOrdonnanceur.do?methode=rechercher"
                          nomListe="${listeOrdonnanceur}"
                          triDefaut="1"
                          triDescendantDefaut="false"
                          class="tableresultat"
                          id="ordonnanceur"
                          trierPageSeulement="false"
                          ajax="true"
                          libelleResultat="infra_sofi.libelle.gestion_ordonnanceur.liste_resultat_element"
                          iterateurMultiPage="true"
                          colgroup="30%,70%"
                          colonneLienDefaut="1"
                          >
  <sofi-liste:colonne property="nom" trier="true" 
                      libelle="infra_sofi.libelle.gestion_ordonnanceur.colonne.nom"
                      href="ordonnanceur.do?methode=consulter"
                      paramId="schedulerId,codeApplication"
                      paramProperty="id,codeApplication">
  </sofi-liste:colonne>
  <sofi-liste:colonne property="description" 
                      libelle="infra_sofi.libelle.gestion_ordonnanceur.colonne.description">
  </sofi-liste:colonne>
  
</sofi-liste:listeNavigation>  