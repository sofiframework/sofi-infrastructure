<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi-liste:listeNavigation action="rechercheAide.do?methode=rechercher"
                            nomListe="${listeTextesAide}"
                            colgroup="35%,45%,10%,10%"
                            triDefaut="4" ajax="true"
                            triDescendantDefaut="true"
                            class="tableresultat" 
                            id="aide"
                            iterateurMultiPage="true"
                            libelleResultat="infra_sofi.libelle.gestion_aide.compteur.element.textes">
  <sofi-liste:colonne property="cleAide" href="aide.do?methode=consulter" 
                      paramId="cleAide, codeLangue" paramProperty="cleAide, codeLangue" trier="true" 
                      libelle="infra_sofi.libelle.gestion_aide.entete.cleAide" traiterCorps="true">
      ${aide.cleAide}
  </sofi-liste:colonne>
  <sofi-liste:colonne property="titre" trier="true" 
                      libelle="infra_sofi.libelle.gestion_aide.entete.titre" />
  <sofi-liste:colonne property="dateCreation"
                      libelle="infra_sofi.libelle.commun.entete.creation" 
                      trier="true"
                      traiterCorps="true">  
    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" 
                    value="${aide.dateCreation}"/>
  </sofi-liste:colonne> 
  <sofi-liste:colonne property="dateModification"
                      libelle="infra_sofi.libelle.commun.entete.maj" 
                      trier="true"
                      traiterCorps="true">  
    <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" 
                    value="${aide.dateModification}"/>
  </sofi-liste:colonne>   
</sofi-liste:listeNavigation>