<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<p align="right" class="champsobligatoire">
  <span class="ind_obligatoire">*</span> 
  <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire"/>
</p>

<!-- Section formulaire -->
<sofi-html:form action="aide.do?methode=enregistrer" transactionnel="true">
    <fieldset class="formulaire">
    <legend><sofi:libelle identifiant="infra_sofi.libelle.gestion_aide.bloc.detail"/></legend>
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <sofi-html:text property="cleAide" libelle="infra_sofi.libelle.gestion_aide.champ.cleAide" obligatoire="true" size="100" maxlength="300"/>
      <sofi-html:text property="aideExterne" 
                      libelle="infra_sofi.libelle.gestion_aide.champ.aideContextuelleExterne" 
                      size="50" 
                      maxlength="300">
      </sofi-html:text>    
      <sofi-html:select 
        property="codeApplication" 
        ligneVide="false"
        libelle="infra_sofi.libelle.gestion_aide.champ.codeApplication" 
        obligatoire="true"
        collection="${LISTE_APPLICATIONS_UTILISATEUR}"
        proprieteEtiquette="[nom] ([codeApplication])"
        proprieteValeur="codeApplication" />    
    </table>
   
    <br/>
    <fieldset class="cadreNavigationSousSection">
    <legend>Contenu par langue</legend>  
      <div class="zoneNavigationSousSection">
        <ul>
            <sofi:cache cache="listeLocale" var="listeLocaleApplication" />
            <c:forEach items="${listeLocaleApplication }" var="localeApplication">
              <li>
	              <c:set var="stylelangue" value="langue" />
	              <c:if test="${localeApplication.key == aideForm.codeLangue}">
	                <c:set var="stylelangue" value="langueSelectionne" />
	              </c:if>
                <sofi-html:link styleClass="${stylelangue}"
			                          styleId="langue_${localeApplication.key}"
			                          libelle="${localeApplication.value}"
			                          href="aide.do?methode=changerLangue&codeLangue=${localeApplication.key}"
			                          ajax="true"
			                          idRetourMultipleAjax="true"
			                          activerConfirmationModificationFormulaire="true"
                                ajaxJSApresChargement="initialiserLangue();setStyleClass('langue_${localeApplication.key}','langueSelectionne');modifierContenuCKEditor('texteAide');" />  
              </li>
            </c:forEach>
        </ul>
      </div>
    </fieldset>
	   <div class="fond_sous_section" >
	      <table border="0" cellspacing="0" cellpadding="0" class="tableformulaireinterne">    
	        <sofi-html:text property="titre" libelle="infra_sofi.libelle.gestion_aide.champ.titre" obligatoire="true" size="100" maxlength="300"/>              
	        <sofi-html:ckeditor property="texteAide" 
	                            libelle="infra_sofi.libelle.gestion_aide.champ.texteAide" 
	                            obligatoire="true" rows="30" cols="90" />
	        <jsp:include page="../trace.jsp" />
	      </table>
	      </div>
	      <div class="barreBouton">
	        <c:if test="${!aideForm.nouveauFormulaire}">
	          <sofi-html:submit libelle="infra_sofi.libelle.gestion_aide.action.creer" 
	                            href="aide.do?methode=ajouter" 
	                            ignorerValidation="true"/>
	          <!-- Configurer l'URL permettant de supprimer un texte d'aide -->
	          <c:set var="original" value="${aideForm.objetTransfertOriginal}"/>
	          <c:url value="aide.do?methode=supprimer" var="urlSupprimerTexteAide" />
	          <!-- Configurer un message de confirmation avec variable dynamique -->
	          <c:if test="${!aideForm.nouveauFormulaire}">
		          <sofi-html:bouton href="${urlSupprimerTexteAide}" 
		                            libelle="infra_sofi.libelle.gestion_aide.action.supprimer" 
		                            messageConfirmation="infra_sofi.confirmation.gestion_aide.suppression" />
	          </c:if>
	        </c:if>
	        <sofi-html:submit libelle="infra_sofi.libelle.gestion_aide.action.enregistrer" />
	      </div>
	   </fieldset>
</sofi-html:form>