<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<script>
function miseAJourOnglet(cellule, index) {
  var url = "afficherAideService.do?methode=getListeOnglets&id=" + index + "&serviceId=" + cellule.value + "&ajax=true&ajaxDiv=true";
  ajaxUpdate(url, 'idOnglet' + index + '');
  traiterAffichageDivAjaxAsynchrone(url,'idOnglet' + index + '','null','null');
}
</script>

<sofi-html:form action="/associerAideService.do?methode=enregistrer" 
                transactionnel="true">
  <div align="left">
    <fieldset style="border:0px;">
      <table cellspacing="10" cellpadding="0" class="tableaffichage">
        <sofi:affichageEL libelle="infra_sofi.libelle.gestion_aide.champ.cleAide" 
                          valeur="${aideForm.cleAide}"/>    
        <sofi:affichageEL libelle="infra_sofi.libelle.gestion_aide.champ.aideContextuelle" 
                          valeur="${aideForm.titre}"/>
      </table>
    </fieldset>
  </div>
  <fieldset>
    <legend>
      <sofi:libelle identifiant="infra_sofi.libelle.gestion_aide.bloc.services"/>
    </legend>
    <div id="association">
      <table border="0" cellspacing="0" cellpadding="0" class="tableresultat">
      <tr>
        <th>
          <sofi:libelle identifiant="infra_sofi.libelle.gestion_aide.entete.service"
              nomAttribut="idService"
              nomListe="listeServices"
              obligatoire="true"/>
        </th>
        <th>
          <sofi:libelle identifiant="infra_sofi.libelle.gestion_aide.entete.onglet"
              nomAttribut="idOnglet"
              nomListe="listeServices"
              obligatoire="false"/>
        </th>
        <th>
          <sofi:libelle identifiant="infra_sofi.libelle.gestion_aide.entete.composant"
              nomAttribut="idComposant"
              nomListe="listeServices"
              obligatoire="false"/>
        </th>        
        <th>
          <sofi:libelle identifiant="infra_sofi.libelle.gestion_aide.entete.action.dissocier"/>
        </th>
      </tr>
      <c:if test="${aideServiceForm.objetTransfert.listeAssociationAideContextuelle != null}">
        <c:set var="selected" value="${aideServiceForm.objetTransfert.listeAssociationAideContextuelle[index]}" />
        <!-- Iteration sur toutes les associations  -->
        <sofi-nested:iterate property="listeAssociationAideContextuelle" 
                             collection="${aideServiceForm.objetTransfert.listeAssociationAideContextuelle}"
                             indexId="index" 
                             id="association">
          <tr>
            <td>
              <sofi-nested:select ligneVide="true"
							                    property="idService"
							                    collection="${listeCompleteServices}"
							                    proprieteEtiquette="nom"
							                    proprieteValeur="id"
							                    obligatoire="true"
                                  href="afficherAideService.do?methode=getListeOnglets&id=${index}"
                                  nomParametreValeur="serviceId"
                                  nomAttributDestinationAjax="idOnglet" />
            </td>
            <td>
              <div id="idOnglet${index}">
	              <% java.lang.String libelle = null; %>
								<c:choose>
								  <c:when test="${empty selected.idService}">
								    <% libelle = "--Choisir un service--"; %>
								  </c:when>
								  <c:otherwise>
								    <% libelle = "--Tous les onglets--"; %>
								  </c:otherwise>
								</c:choose>
								<sofi-nested:select ligneVide="true"
								                    ligneVideLibelle="<%= libelle %>"
								                    collection="${association.listeOnglet}"
								                    property="idOnglet"
								                    proprieteEtiquette="nom"
								                    proprieteValeur="id"
								                    href="afficherAideService.do?methode=getListeComposants&id=${index}"
								                    nomParametreValeur="ongletId"
								                    nomAttributDestinationAjax="idComposant"
								                    obligatoire="false"
								                    style="width: 220;"/>
              </div>
            </td>
            <td>
              <sofi-nested:select ligneVide="true"
							                    ligneVideLibelle="-- Aucun --"
							                    collection="${association.listeComposant}"
							                    property="idComposant"
							                    proprieteEtiquette="nom"
							                    proprieteValeur="id"
							                    obligatoire="false"
							                    style="width: 220;"/>
            </td>            
            <td>
              <c:url var="urlDissocier" value="afficherAideService.do">
                <c:param name="methode" value="dissocierService"/>
                <c:param name="index" value="${index}"/>
              </c:url>
              <c:if test="${selected.nouveauFormulaire}">
                <sofi-html:submit libelle="" href="${urlDissocier}"
                    notifierModificationFormulaire="true"
                    styleClass="boutonSuppression"/>
              </c:if>
              <c:if test="${!selected.nouveauFormulaire}">
                <sofi-html:submit 
                      styleClass="boutonSuppression"
                      libelle="" href="${urlDissocier}" 
                      notifierModificationFormulaire="true"
                      messageConfirmation="infra_sofi.confirmation.gestion_aide.association.suppression"/>
              </c:if>
            </td>
          </tr>
        </sofi-nested:iterate>
      </c:if>
      </table>
      <div class="barreIcone">
        <sofi-html:submit libelle="" 
                          property="ajouter"
                          styleClass="boutonAjouter"
                          href="afficherAideService.do?methode=ajouterService"/>
      </div>
    </div> 
    <div class="barreBouton">
      <sofi-html:submit libelle="infra_sofi.libelle.gestion_aide.action.enregistrer" 
                        property="enregistrer" 
                        styleClass="bouton" />
    </div>    
  </fieldset>
</sofi-html:form>