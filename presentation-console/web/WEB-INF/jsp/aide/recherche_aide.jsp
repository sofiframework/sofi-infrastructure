<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<!-- Section filtre formulaire -->
<sofi-html:form action="rechercheAide.do?methode=rechercher#listeTexteAide" transactionnel="false" focus="titre">
  <fieldset class="formulaire">
  <legend><sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.criteres.recherche"/></legend>
    <table cellpadding="0" cellspacing="0" class="tableformulaire">
      <sofi-html:text property="titre" libelle="infra_sofi.libelle.gestion_aide.champ.titre" size="70">
           <sofi-html:select property="codeLangue" 
          cache="listeLocale" 
         />
      </sofi-html:text>
      <sofi-html:text property="cleAide" libelle="infra_sofi.libelle.gestion_aide.champ.cleAide" size="70"/>
      <sofi-html:select 
          property="codeApplication" 
          ligneVide="false"
          libelle="infra_sofi.libelle.gestion_aide.champ.codeApplication"
          collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          proprieteEtiquette="[nom] ([codeApplication])"
          proprieteValeur="codeApplication"
          href="rechercheAide.do?methode=fixerApplication"
          ajax="true"
          nomParametreValeur="codeApplication" />
   
    </table>
    <div class="barreBouton">
      <sofi-html:bouton libelle="infra_sofi.libelle.gestion_aide.action.creer" 
      	href="aide.do?methode=ajouter" styleClass="bouton" />
      <sofi-html:submit libelle="infra_sofi.libelle.gestion_aide.action.rechercher" 
      	styleClass="bouton" />  
    </div>
  </fieldset>
</sofi-html:form>

<!-- Section liste de navigation -->
<c:if test="${listeTextesAide != null}">
  <fieldset>
    <legend><sofi:libelle identifiant="infra_sofi.libelle.commun.bloc.resultat_recherche"/></legend>
    <jsp:include page="ajax_liste_aide.jsp" />
  </fieldset>
</c:if>
