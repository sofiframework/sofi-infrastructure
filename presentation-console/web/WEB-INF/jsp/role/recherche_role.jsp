<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion_journalisation.critere_recherche"/>
  </legend>
  <sofi-html:form action="/rechercheRole.do?methode=rechercher" transactionnel="false" >  
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <sofi-html:select 
          property="codeApplication" 
          ligneVide="false"
          libelle="infra_sofi.libelle.gestion_journalisation.champ.systeme"
          collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          proprieteEtiquette="[nom] ([codeApplication])"
          proprieteValeur="codeApplication" 
          href="rechercheReferentiel.do?methode=fixerApplication"
          ajax="true"
          nomParametreValeur="codeApplication"           
          />
      <sofi-html:select 
          property="codeStatut" ligneVide="true"
          libelle="infra_sofi.libelle.gestion_referentiel.champ.statutActivation"
          cache="listeStatutActivation"/>        
    </table>  
    <div class="barreBouton">
      <sofi-html:bouton libelle="infra_sofi.libelle.gestion.role.bouton.creer" 
                 href="role.do?methode=creer"/>
      <sofi-html:submit libelle="infra_sofi.libelle.commun.action.rechercher"/>      
    </div> 
  </sofi-html:form>
</fieldset>
<c:if test="${GESTION_ROLE_RESULTAT_RECHERCHE != null}">
  <div id="vueHierarchique">
    <jsp:include page="ajax_liste_role.jsp" />
  </div>
</c:if>