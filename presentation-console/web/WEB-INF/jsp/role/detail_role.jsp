<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-nested" prefix="sofi-nested"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<p align="right" class="champsobligatoire">
  <span class="ind_obligatoire">*</span> 
  <sofi:libelle identifiant="infra_sofi.libelle.commun.champ.obligatoire"/>
</p>
<fieldset>
  <legend>
    <sofi:libelle identifiant="infra_sofi.libelle.gestion.role.section.detail.role"/>
  </legend>
  <sofi-html:form action="/role.do?methode=enregistrer" transactionnel="true">
    <table border="0" cellspacing="0" cellpadding="0" class="tableformulaire">
      <sofi-html:select property="codeApplication" 
          ligneVide="true"
          libelle="infra_sofi.libelle.gestion_role.champ.systeme"
          collection="${LISTE_APPLICATIONS_UTILISATEUR}"
          proprieteEtiquette="[nom] ([codeApplication])"
          proprieteValeur="codeApplication"
          affichageSeulement="true"/>
      <sofi-html:text libelle="infra_sofi.libelle.gestion_role.champ.codeRole" 
                      property="codeRole" obligatoire="true" size="30" maxlength="40" 
                      disabledEL="${!roleForm.nouveauFormulaire}" />
      <sofi-html:text libelle="infra_sofi.libelle.gestion_role.champ.nom" 
                      property="nom" obligatoire="true" size="70" maxlength="100" />
      <sofi-html:textarea libelle="infra_sofi.libelle.gestion_role.champ.description" 
                      property="description" rows="15" cols="100"/>
      <sofi-html:listeValeurs libelle="infra_sofi.libelle.gestion_role.champ.codeRoleParent" 
                              href="rechercheRole.do?methode=rechercherParent" 
                              property="nomRoleParent"
                              identifiant="codeRoleParent" 
                              size="40"
                              maxlength="40"
                              paramId="paramNomRoleParent" 
                              paramProperty="nomRoleParent"
                              largeurfenetre="550" 
                              hauteurfenetre="265"
                              attributRetour="codeRoleParent,nomRoleParent"
                              ajax="true"    
                              activerFiltreSurEvenementOnkeyup="true">
        <sofi-html:hidden property="codeRoleParent" />
      </sofi-html:listeValeurs>
      <sofi-html:select property="codeStatut" 
                        obligatoire="true"
                        libelle="infra_sofi.libelle.commun.champ.statutActivation"
                        cache="listeStatutActivation"
                        ligneVide="true"/>          
      <jsp:include page="../trace.jsp" />                        
    </table>
    <div class="barreBouton">
      <c:if test="${!formulaireEnCours.nouveauFormulaire}">
        <sofi:message var="confirmationSupressionRole"
                      identifiant="infra_sofi.confirmation.gestion_role.suppression_role"
                      parametre1="${detailRoleForm.nom}"/>
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion.role.bouton.supprimer" 
                          href="role.do?methode=supprimer" 
                          notifierModificationFormulaire="true" 
                          messageConfirmation="${confirmationSupressionRole}"/>
        <sofi-html:bouton libelle="infra_sofi.libelle.gestion.role.bouton.creer" 
                          href="role.do?methode=creer"/>
      </c:if>
      <sofi-html:submit libelle="infra_sofi.libelle.gestion.role.bouton.enregistrer"/>
    </div>    
  </sofi-html:form>
</fieldset>
