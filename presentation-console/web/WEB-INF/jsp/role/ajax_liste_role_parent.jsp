<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<sofi-liste:listeNavigation action="rechercheRole.do?methode=rechercherParent"
                            nomListe="${listeRoleParent}"
                            triDefaut="1"
                            triDescendantDefaut="false"
                            class="tableresultat"
                            id="role"
                            trierPageSeulement="false"
                            ajax="true"
                            libelleResultat="infra_sofi.libelle.gestion.role.section.resultat.element"
                            iterateurMultiPage="true"
                            colgroup="40%,60%"
                            fonctionRetourValeurAjax="true" 
                            colonneLienDefaut="1">
  <sofi-liste:colonne property="codeRole" trier="true" 
                      libelle="infra_sofi.libelle.gestion_role.colonne.codeRole"
                      parametresFonction="codeRole, nom" 
                      fonctionRetourValeurAjax="true"/>
  <sofi-liste:colonne property="nom" trier="true" 
                      libelle="infra_sofi.libelle.gestion.role.colonne.nom"
                      parametresFonction="codeRole, nom" 
                      fonctionRetourValeurAjax="true"/>
</sofi-liste:listeNavigation>