<%@ page contentType="text/html;charset=UTF-8"%>

<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-liste" prefix="sofi-liste"%>

<fieldset>
  <sofi:libelle var="titreVueDetaille"
                identifiant="infra_sofi_libelle.gestion_role.titre.vue_hierarchique_detaillee"/>
  <sofi:libelle var="titreVuesimplifiee"
                identifiant="infra_sofi_libelle.gestion_role.titre.vue_hierarchique_simplifiee"/>
  <c:if test="${formulaireEnCours.vue == 'detaillee'}">
    <c:set var="libelleTitre" value="${titreVueDetaille}"/>
  </c:if>
  <c:if test="${formulaireEnCours.vue == 'simplifiee'}">
    <c:set var="libelleTitre" value="${titreVuesimplifiee}"/>
  </c:if>
  <legend>
    <sofi:libelle identifiant="${libelleTitre}"/>
  </legend>
  <div class="barreBouton">
      <sofi-html:link var="lienVueDetailleeAjax"
                      href="rechercheRole.do?methode=rechercher&vue=detaillee" 
                      ajax="true"
                      divRetourAjax="vueHierarchique" />
      <sofi-html:link var="lienVueSimplifieeAjax"
                      href="rechercheRole.do?methode=rechercher&vue=simplifiee" 
                      ajax="true"
                      divRetourAjax="vueHierarchique" />
      <sofi-html:bouton libelle="infra_sofi_libelle.gestion_role.action.vue_detaillee" onclick="${lienVueDetailleeAjax}"/>
      <sofi-html:bouton libelle="infra_sofi_libelle.gestion_role.action.vue_simplifiee" onclick="${lienVueSimplifieeAjax}"/>  
    </div> 
    <div class="gestionRole_titreApplication" style="padding-top:10px;margin-bottom:-10px;height:30px">
      <sofi:cache cache="listeApplication" valeur="${formulaireEnCours.codeApplication}" proprieteAffichage="nom"/>
      -
      <sofi:taille valeur="${GESTION_ROLE_RESULTAT_RECHERCHE}"/> <sofi:libelle identifiant="infra_sofi_libelle.gestion_role.resultat_role" />
    </div>
  <c:set var="niveau" value="0"/>
  <table width="100%">
  <tr><td></td><td>
  <c:forEach var="role" items="${GESTION_ROLE_RESULTAT_RECHERCHE}">
    <c:if test="${role.niveau > niveau}">
      <ul>
      <c:set var="niveau" value="${role.niveau}"/>
    </c:if>
    <c:if test="${niveau > role.niveau}">
      </ul>
      <c:set var="niveau" value="${role.niveau}"/>    
    </c:if>
     <li style="clear:both">
       <c:url var="lienConsultation" value="role.do?methode=consulter">
        <c:param name="codeRole" value="${role.codeRole}" />
        <c:param name="codeApplication" value="${role.codeApplication}" />
      </c:url>
      <c:if test="${role.codeStatut != 'A'}">
       <span style="color:red"><sofi:libelle identifiant="infra_sofi.libelle.commun.element_inactif" /></span> &nbsp;&nbsp;   
      </c:if> 
      <sofi-html:link href="${lienConsultation}" libelle="${role.nom}"/>          
      <c:if test="${formulaireEnCours.vue == 'detaillee'}">
        <div style="margin-top:5px;margin-bottom:10px;height:100%;width:100%">
          <div style="float:left;width:100%;padding-top:2px;padding-left:2px;background-color:#E7F0F7;">
            <div style="padding-bottom:5px;padding-left:1px;width:100%;">
              <c:if test="${role.description == null}">
                <sofi:libelle identifiant="infra_sofi_libelle.gestion_role.champ.aucune_description"/>
              </c:if>
              <c:if test="${role.description != null}">
                <sofi:texte valeur="${role.description}" convertirRetourChariotEnSautLigneHTML="true" />           
              </c:if>
            </div>
            <c:url var="lienUtilisateurRole" value="/rechercheUtilisateur.do?methode=rechercher">
              <c:param name="codeRole" value="${role.codeRole}" />
              <c:param name="codeApplication" value="${role.codeApplication}" />    
            </c:url>
            <c:url var="lienRoleReferentiel" value="/rechercheSecuriteReferentiel.do?methode=rechercher">
              <c:param name="codeRole" value="${role.codeRole}" />
              <c:param name="codeApplication" value="${role.codeApplication}" />    
            </c:url>
            <div style="padding-left:2px;padding-top:5px;padding-bottom:5px;font-size:10px">
              <sofi:libelle identifiant="infra_sofi.libelle.gestion_role.texte.acces"/>&nbsp;
              <sofi-html:link href="${lienUtilisateurRole}" libelle="infra_sofi.libelle.gestion_role.action.acces_utilisateurs" memoriserHrefActuel="true"/>&nbsp;|&nbsp;
              <sofi-html:link href="${lienRoleReferentiel}" libelle="infra_sofi.libelle.gestion_role.action.acces_composant_referentiel" memoriserHrefActuel="true"/>              
            </div>
            <div style="float:right;margin-top:-20px;">
	           <c:url var="lienAjout" value="role.do?methode=creer">
	             <c:param name="codeRoleParent" value="${role.codeRole}" />
	             <c:param name="codeApplicationParent" value="${role.codeApplication}" />
	           </c:url>
			       <sofi:libelle var="aideRoleEnfant"
			                     identifiant="infra_sofi.aide.gestion_role.ajouter_role_enfant"
			                     parametre1="${role.nom}"/>
			       <sofi-html:bouton styleClass="boutonAjouter" libelle="" 
			                         href="${lienAjout}" aideContextuelle="${aideRoleEnfant}"/>
			       <c:url var="supprimerRole" value="/role.do?methode=supprimer">
			         <c:param name="codeRole" value="${role.codeRole}" />
			         <c:param name="codeApplication" value="${role.codeApplication}" />
			         <c:param name="vue" value="recherche" />              
			       </c:url>
			       <sofi:message var="messageSuppressionRole"
			                     identifiant="infra_sofi.confirmation.gestion_role.suppression_role"
			                     parametre1="${role.nom}"/>
			       <sofi-html:bouton libelle="" 
			                         href="${supprimerRole}" 
			                         styleClass="boutonSuppression" 
			                         messageConfirmation="${messageSuppressionRole}" 
			                         notifierModificationFormulaire="true"/>
            </div>
          </div> 
        </div>  
      </c:if>
     </li>
  </c:forEach>
  </td></tr></table>
  </ul>
</ul>
</fieldset>