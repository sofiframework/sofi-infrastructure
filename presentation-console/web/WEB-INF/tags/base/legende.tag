<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles"  prefix="tiles"%>

<%@ attribute name="libelle" required="true" type="java.lang.String"%>

<legend>
  <sofi:libelle identifiant="${libelle}" />
</legend>