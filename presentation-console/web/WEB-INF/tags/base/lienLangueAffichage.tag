<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:choose>
  <c:when test="${utilisateur.langue == 'FR'}">
    <sofi-html:link href="javascript:changerLangue('EN');" libelle="infra_sofi.libelle.en_tete.english" />
  </c:when>
  <c:when test="${utilisateur.langue == 'EN'}">
    <sofi-html:link href="javascript:changerLangue('FR');" libelle="infra_sofi.libelle.en_tete.francais" />
  </c:when>
</c:choose>