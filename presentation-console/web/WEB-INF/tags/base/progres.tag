<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ attribute name="id" required="false" type="java.lang.String"%>
<%@ attribute name="alt" required="false" type="java.lang.String"%>
<c:set var="id" value="${id eq null ? 'progres' : id}" />
<span id="${id}">
 <img class="roller" src="images/roller.gif" alt="${alt}" />
</span>