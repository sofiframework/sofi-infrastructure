<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>

<%@ attribute name="libelle" required="true" type="java.lang.String"%>
<%@ attribute name="styleId" required="false" type="java.lang.String"%>

<c:if test="${styleId == null}">
  <c:set var="styleId" value="fenetreAttente" />
</c:if>

<div style="display:none;">
  <div id="${styleId}">
    <p><sofi:libelle identifiant="${libelle}" /></p>
    <center>
      <sofi:libelle var="altImageAttente" identifiant="infra_sofi.libelle.alt_image_attente" />
      <img src="images/sofi/roller.gif" alt="${altImageAttente}" />
    </center>
  </div>
</div>