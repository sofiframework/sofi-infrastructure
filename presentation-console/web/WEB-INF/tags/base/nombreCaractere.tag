<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ attribute name="property" required="true" type="java.lang.String"%>
<%@ attribute name="maxlength" required="true" type="java.lang.String"%>
<c:set 
  var="valeurPropriete" 
  value="${nom_formulaire_imbrique eq null ? formulaireEnCours.attributs[property] : formulaireEnCours.attributs[nom_formulaire_imbrique].attributs[property]}"
 />
<span id="${property}-compteur-caractere" class="compteur-caractere">${fn:length(valeurPropriete)}/${maxlength}</span><br/>