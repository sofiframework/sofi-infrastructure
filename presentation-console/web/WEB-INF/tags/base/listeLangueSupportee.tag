
<%@tag import="org.sofiframework.application.cache.ObjetCache"%>
<%@tag import="org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur"%>
<%@tag import="org.sofiframework.application.cache.GestionCache"%>
<%@tag import="org.sofiframework.modele.spring.application.ConfigurationServices"%>
<%@tag import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@tag import="org.springframework.web.context.WebApplicationContext"%>
<%@ tag body-content="scriptless" %>
<%@tag import="org.sofiframework.presentation.struts.controleur.UtilitaireControleur"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles"  prefix="tiles"%>

<%@ attribute name="libelle" required="false" type="java.lang.String" rtexprvalue="true"%>
<%@ attribute name="styleClass" required="true" type="java.lang.String" rtexprvalue="true"%>

<div class="${styleClass}">
<sofi-html:select libelle=""
  cache="listeLocale" value="${codeLangueEnCours}" 
  onchange="changerLocale($(this).val());" transactionnel="false"/>
</div>