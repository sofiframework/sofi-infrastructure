<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ attribute name="libelle" required="true" type="java.lang.String"%>
<%@ attribute name="styleId" required="false" type="java.lang.String"%>
<%@ attribute name="styleClass" required="false" type="java.lang.String"%>

<c:if test="${styleId == null}">
  <c:set var="styleId" value="btFermer" />
</c:if>

<sofi-html:bouton onclick="closeDialog(this);" 
                  libelle="${libelle}"
                  styleId="${styleId}"
                  styleClass="${styleClass}"
                  actifSiFormulaireLectureSeulement="true" 
                  activerConfirmationModificationFormulaire="true" />