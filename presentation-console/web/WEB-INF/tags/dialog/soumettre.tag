<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ attribute name="libelle" required="true" type="java.lang.String"%>
<%@ attribute name="urlSoumettre" required="false" type="java.lang.String"%>
<%@ attribute name="urlMajParent" required="false" type="java.lang.String"%>
<%@ attribute name="divIdParent" required="false" type="java.lang.String"%>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean"%>
<%@ attribute name="formulaireParent" required="false" type="java.lang.String"%>
<%@ attribute name="notifierFormulaireParent" required="false" type="java.lang.Boolean"%>
<%@ attribute name="activerConfirmationModificationFormulaire" required="false" type="java.lang.Boolean"%>

<sofi:parametreSysteme var="cleMessageConfirmation" code="cleMessageAvertisementFormulaireModifie" />
<sofi:message var="messageConfirmation" identifiant="${cleMessageConfirmation}" />

  <c:set var="jsNotifier" value="" />
  <c:if test="${notifierFormulaireParent}">
    <c:set var="jsNotifier" value="notifierModificationFormulaire('${formulaireParent}');" />
  </c:if>
  
  <c:if test="${activerConfirmationModificationFormulaire == null}">
    <c:set var="activerConfirmationModificationFormulaire" value="false" />
  </c:if>
  
  <c:set var="fonctionMajParent" value="function(){}" />
  <c:if test="${urlMajParent != null}">
    <c:set var="fonctionMajParent" value="function() {ajaxUpdate('${urlMajParent}', '${divIdParent}');};" />
  </c:if>
  <c:set var="jsSoumission" value="soumettreFormulaireAjax('${urlSoumettre}', this, ${fonctionMajParent}, ${activerConfirmationModificationFormulaire}, '${messageConfirmation}', event);" />

  <sofi-html:bouton onclick="${jsNotifier}${jsSoumission}"
                    libelle="${libelle}" 
                    disabled="${disabled}" />