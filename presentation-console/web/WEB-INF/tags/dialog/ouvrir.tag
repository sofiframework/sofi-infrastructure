<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ attribute name="libelle" required="true" type="java.lang.String"%>
<%@ attribute name="url" required="true" type="java.lang.String"%>
<%@ attribute name="divId" required="true" type="java.lang.String"%>
<%@ attribute name="hauteur" required="true" type="java.lang.String"%>
<%@ attribute name="largeur" required="true" type="java.lang.String"%>

<sofi-html:link 
  href="javascript:void(0);"
  libelle="${libelle}" 
  onclick="openDialog('${divId}', ${hauteur}, ${largeur}, true, '${url}');"/>