<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ attribute name="divId" required="true" type="java.lang.String"%>
<%@ attribute name="titre" required="true" type="java.lang.String"%>

<sofi:libelle var="titreFenetre" identifiant="${titre}" />

<div id="${divId}" title="${titreFenetre}" class="dialog">
  <jsp:doBody />
</div>
