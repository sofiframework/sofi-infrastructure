<%@ tag body-content="scriptless" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi" prefix="sofi"%>
<%@ taglib uri="http://www.sofiframework.org/taglib/sofi-html" prefix="sofi-html"%>

<%@ attribute name="urlSuppression" required="true" type="java.lang.String"%>
<%@ attribute name="libelle" required="true" type="java.lang.String"%>

<%@ attribute name="urlMajParent" required="false" type="java.lang.String"%>
<%@ attribute name="divIdParent" required="false" type="java.lang.String"%>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean"%>

<sofi:message var="messageConfirmation" identifiant="infra_sofi.message.commun.confirmation_supprimer" />

<c:set var="fonctionMajParent" value="function(){}" />
<c:if test="${urlMajParent != null}">
  <c:set var="fonctionMajParent" value="function() {ajaxUpdate('${urlMajParent}', '${divIdParent}');};" />
</c:if>
<c:set var="jsSupprimer" value="
    var url = '${urlSuppression}&ignorerValidation=true';
    supprimerFormulaireAjax(url, this, '${messageConfirmation}', ${fonctionMajParent});"
 />
<sofi-html:bouton onclick="${jsSupprimer}" libelle="${libelle}" disabled="${disabled}" />