/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.decorator;

import java.util.Collection;

import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.presentation.balisesjsp.arbre.NoeudDecorator;

/**
 * Permet d'ajouter la décoration de type noeud à
 * un objet Java ou une application.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ObjetJavaNoeudDecorator implements NoeudDecorator {

  @Override
  public String getIdentifiantNoeud(Object noeud) {
    if (this.isApplication(noeud)) {
      return getApplication(noeud).getCodeApplication();
    } else {
      return this.getObjetJava(noeud).getId().toString();
    }
  }

  @Override
  public Collection getListeEnfants(Object noeud) {
    if (isApplication(noeud)) {
      return getApplication(noeud).getListeObjetJava();
    } else {
      return this.getObjetJava(noeud).getListeObjetJavaEnfant();
    }
  }

  @Override
  public boolean isFeuille(Object noeud) {
    return this.getListeEnfants(noeud) == null || this.getListeEnfants(noeud).isEmpty() ;
  }

  private boolean isApplication(Object noeud) {
    return noeud instanceof Application;
  }

  private Application getApplication(Object noeud) {
    return (Application) noeud;
  }

  private ObjetJava getObjetJava(Object noeud) {
    return (ObjetJava) noeud;
  }
}
