/**
 * 
 */
package org.sofiframework.infrasofi.presentation.decorator;

import org.sofiframework.application.securite.objetstransfert.Client;
import org.sofiframework.presentation.balisesjsp.html.SelectDecorator;

/**
 * Décorateur gérant l'affichage en indentation de la liste de clients.
 * 
 * @author remi.mercier
 * 
 */
public class ClientSelectDecorator implements SelectDecorator {

  @Override
  public String getEtiquette(Object objet) {
    Client client = (Client) objet;
    StringBuilder etiquette = new StringBuilder();
    Client parent = client.getClientParent();
    if (parent != null) {
      do {
        etiquette.append("&nbsp;&nbsp;");
      } while ((parent = parent.getClientParent()) != null);
    }
    etiquette.append(String.format("%s (%s)", client.getNom(),
        client.getNoClient()));
    return etiquette.toString();
  }

  @Override
  public String getValeur(Object objet) {
    Client client = (Client) objet;

    return client.getNoClient();
  }

}
