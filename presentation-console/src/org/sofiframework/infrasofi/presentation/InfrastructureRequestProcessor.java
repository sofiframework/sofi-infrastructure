/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.cache.GestionCache;
import org.sofiframework.application.securite.objetstransfert.Client;
import org.sofiframework.application.surveillance.GestionSurveillance;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.presentation.struts.controleur.spring.BaseTilesRequestProcessor;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireListe;

/**
 * Processeur de requêtes de la gestion de l'infrastructure.
 * 
 * @author Jean-Maxime Pelletier
 */
public class InfrastructureRequestProcessor extends BaseTilesRequestProcessor {

  /**
   * Insérer les traitements qui doivent être nécessaire avant toute requête vers une action précise.
   * 
   * @return null si aucune vue doit etre appelé apres un traitement spécifie, sinon specifié un forward spécifique.
   * @param mapping
   *          l'instance de mapping en cours.
   * @param form
   *          le formulaire en cours d'utilisation.
   * @param action
   *          l'action appellé.
   * @param response
   *          la réponse en traitement.
   * @param request
   *          la requête en traitement.
   */
  @Override
  protected ActionForward traitementAvantAction(HttpServletRequest request, HttpServletResponse response,
      Action action, ActionForm form, ActionMapping mapping) {

    if (request.getSession().getAttribute(InfrastructureAction.LISTE_APPLICATIONS_UTILISATEUR) == null) {
      configurerListeApplicationDansSession(request);
    }

    if (request.getSession().getAttribute(InfrastructureAction.LISTE_CLIENT) == null) {
      configurerListeClientDansSession(request);
    }

//    String codeApplicationSelectionnee = (String) request.getSession().getAttribute("systeme_selectionne");
//
//    if (codeApplicationSelectionnee == null) {
//      @SuppressWarnings("unchecked")
//      List<Application> listeApplication = (List<Application>) request.getSession().getAttribute(
//          "LISTE_APPLICATIONS_UTILISATEUR");
//
//      if (listeApplication != null && listeApplication.size() > 0) {
//        Application premiereApplication = listeApplication.get(0);
//        request.getSession().setAttribute("systeme_selectionne", premiereApplication.getCodeApplication());
//      }
//    }

    // Le nombre d'utilisateur en ligne
    request.getSession().setAttribute("nbUtilisateurEnLigne",
        String.valueOf(GestionSurveillance.getInstance().getListeUtilisateurEnLigneUnique().size()));

    String appStartDate = GestionSurveillance.getInstance().getDateDepartApplication();
    request.getSession().setAttribute("appStartDate", appStartDate);

    return null;
  }

  /**
   * Insérer les traitements qui doivent être nécessaire après toute requête vers une action précise.
   * 
   * @return null si aucune vue doit etre appelé apres un traitement spécifie, sinon specifié un forward spécifique.
   * @param mapping
   *          l'instance de mapping en cours.
   * @param form
   *          le formulaire en cours d'utilisation.
   * @param action
   *          l'action appellé.
   * @param response
   *          la réponse en traitement.
   * @param request
   *          la requête en traitement.
   */
  @Override
  protected ActionForward traitementApresAction(HttpServletRequest request, HttpServletResponse response,
      Action action, ActionForm form, ActionMapping mapping) {
    return null;
  }

  /**
   * Met en session la liste des clients.
   * 
   * @param request
   */
  public static void configurerListeClientDansSession(HttpServletRequest request) {
    ModeleInfrastructure modele = (ModeleInfrastructure) UtilitaireControleur.getModele(request);
    List<Client> listeClient = modele.getServiceClient().getListeClientDescendant(null);
    request.getSession().setAttribute(InfrastructureAction.LISTE_CLIENT,
        UtilitaireListe.getListeParentEnfantsEn1Niveau(listeClient, "listeClientEnfant"));
  }

  /**
   * Méthode qui sert à mettre dans la session utilisateur les applications auquel l'utilisateur à droit.
   * <p>
   * 
   * @param request
   *          la requête utilisateur en traitement
   */
  public static void configurerListeApplicationDansSession(HttpServletRequest request) {
    ModeleInfrastructure modele = (ModeleInfrastructure) UtilitaireControleur.getModele(request);

    if (UtilitaireControleur.isActionSecurise(request)) {
      List<Application> listeApplicationsUtilisateur = null;

      listeApplicationsUtilisateur = modele.getServiceApplication().getListeApplicationAccessiblePilotage();

      if (listeApplicationsUtilisateur != null) {
        for (Iterator<Application> iterateurApplications = listeApplicationsUtilisateur.iterator(); iterateurApplications
            .hasNext();) {
          Application application = iterateurApplications.next();
          GestionCache.getInstance().ajouterObjetCache("listeApplication", application.getCodeApplication(),
              application);
//          if (request.getSession().getAttribute("systeme_selectionne") == null) {
//            request.getSession().setAttribute("systeme_selectionne", application.getCodeApplication());
//          }
        }
      }

      request.getSession().setAttribute(InfrastructureAction.LISTE_APPLICATIONS_UTILISATEUR,
          listeApplicationsUtilisateur);
    }
  }
}
