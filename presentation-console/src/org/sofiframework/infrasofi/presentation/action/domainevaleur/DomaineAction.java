/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.domainevaleur;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.DefinitionDomaineValeur;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.modele.exception.ModeleException;

public class DomaineAction extends InfrastructureAction {

  private static final Log log = LogFactory.getLog(ValeurAction.class);

  /**
   * Cette redirige vers la page d'affichage de la définition d'un domaine de
   * valeur.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    return mapping.findForward("page");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur utilise le bouton "Créer"
   * afin de créer une nouvelle définition de domaine de valeur.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward ajouter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    // Récupérer et initialiser le formulaire
    DomaineForm formulaire = (DomaineForm) form;

    formulaire.initialiserFormulaire(request, mapping,
        new DefinitionDomaineValeur(), true);

    DefinitionDomaineValeur definitionDomaineValeur = (DefinitionDomaineValeur) formulaire
        .getObjetTransfert();
    definitionDomaineValeur.setValeur("DEFINITION");

    // Message informatif du mode de création d'une définition de domaine.
    formulaire.ajouterMessageInformatif(
        "infra_sofi.information.domaine_valeur.creation_definition", request);
    setAttributTemporairePourAction("detailDefinitionTraite", Boolean.TRUE, request);
    definitionDomaineValeur.setCodeLangue(this.getUtilisateur(request).getCodeLocale());

    if (isVide(formulaire.getCodeApplication())) {
      String codeApplicationSelectionne = (String) request.getSession()
          .getAttribute("systeme_selectionne");
      formulaire.setCodeApplication(codeApplicationSelectionne);
    }

    // Enlever la liste des domaine de valeur d'une définition précédente en
    // session
    request.getSession().removeAttribute("LISTE_DEFINITION_DOMAINE_ENFANT");

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur accède au détail d'une
   * définition de domaine de valeur.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    DomaineForm formulaire = (DomaineForm) form;
    this.setLangueEnCours(formulaire.getCodeLangue(), request);
    this.populer(formulaire, request);
    this.setAttributTemporairePourAction("detailDefinitionTraite",
        Boolean.TRUE, request);
    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lors de la sauvegarde de la définition d'un domaine
   * de valeur.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    // Récupérer les données de base
    ModeleInfrastructure modele = (ModeleInfrastructure) this
        .getModele(request);
    DomaineForm formulaire = (DomaineForm) form;

    // Récupérer la définition du domaine de valeur et effectuer la sauvegarde
    try {
      DefinitionDomaineValeur definition = (DefinitionDomaineValeur) formulaire
          .getObjetTransfert();
      DefinitionDomaineValeur definitionOriginal = (DefinitionDomaineValeur) formulaire
          .getObjetTransfertOriginal();
      modele.getServiceDomaineValeur().enregistrerDefinitionDomaineValeur(
          definition, definitionOriginal);
      this.commit(request, formulaire);
      formulaire.populerFormulaire(definition, request);
    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour enregistrer une définition.", e);
      }
      formulaire.ajouterMessageErreur(e, request);
      this.rollback(request);
    }

    return mapping.findForward("afficher");
  }

  private void populer(DomaineForm formulaire, HttpServletRequest request) {
    // Récupérer les objets de base
    ModeleInfrastructure modele = (ModeleInfrastructure) this
        .getModele(request);

    try {
      // Récupérer la définition et populer le formulaire avec
      DefinitionDomaineValeur definition = modele.getServiceDomaineValeur()
          .getDefinitionDomaineValeur(formulaire.getCodeApplication(),
              formulaire.getNom(), formulaire.getCodeLangue());

      if (definition == null) {
        log.debug("definition == null ");
      }

      // Populer le formulaire avec la définition.
      formulaire.populerFormulaire(definition, request, true);

      // Obtenir la liste des domaine de valeurs enfants
      List listeDomainesValeurEnfants = modele.getServiceDomaineValeur()
          .getListeDomaineValeur(formulaire.getCodeApplication(),
              formulaire.getNom(), null, formulaire.getCodeLangue());

      // Fixer la liste temporaire pour l'action.
      this.setAttributTemporairePourService("listeValeurDomaineEnfant",
          listeDomainesValeurEnfants, request);
    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour populerDefinition.", e);
      }
      formulaire.ajouterMessageErreur(e, request);
    }
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur supprime une définition de
   * domaine de valeur.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward supprimer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    // Récupérer les données de base
    ModeleInfrastructure modele = (ModeleInfrastructure) this
        .getModele(request);
    DomaineForm formulaire = (DomaineForm) form;
    DefinitionDomaineValeur definition = (DefinitionDomaineValeur) formulaire
        .getObjetTransfert();

    try {
      // Effectuer la suppression. On supprime aussi les enfants
      modele.getServiceDomaineValeur().supprimerDefinitionDomaineValeur(
          definition);
      this.commit(request,
          "infra_sofi.information.domaine_valeur.supression_succes", null);
    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour supprimer", e);
      }

      formulaire.ajouterMessageErreur(
          "infra_sofi.erreur.commun.suppression.impossible", request);
      this.rollback(request);
      return mapping.findForward("afficher");
    }

    return mapping.findForward("retourRecherche");
  }
}
