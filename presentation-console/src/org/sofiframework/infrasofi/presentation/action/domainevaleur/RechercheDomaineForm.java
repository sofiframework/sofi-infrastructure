/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.domainevaleur;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.presentation.struts.form.formatter.FormatterBoolean;

/**
 * Objet d'affaire faisant represetant un les critères utilisés pour recherche un domaine de valeur.
 * <p>
 * 
 * @author Maxime Ouellet, Nurun inc.
 * @version 1.0
 */
public class RechercheDomaineForm extends BaseForm {

  private static final long serialVersionUID = -6036359034015445928L;

  /** l'identifiant unique du code de l'application du domaine de valeur */
  private String codeApplication;

  /** le nom du domaine de valeur */
  private String nom;

  /** la valeur du domaine de valeur */
  private String valeur;

  /** le type de recherche a effectuer */
  private String typeDomaine;

  private String codeLangue;

  private String actif;

  private String codeClient;

  /**
   * Constructeur par défaut.
   */
  public RechercheDomaineForm() {
    this.ajouterTypeFormatter("actif", FormatterBoolean.class);
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un <code>submit</code>.
   * <p>
   * 
   * @param request
   *          la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  /**
   * Obtenir l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @return l'identifiant unique du code de l'application du domaine de valeur.
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @param codeApplication
   *          l'identifiant unique du code de l'application du domaine de valeur.
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir le nom du domaine de valeur.
   * 
   * @return le nom du domaine de valeur.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du domaine de valeur.
   * 
   * @param nom
   *          le nom du domaine de valeur.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir la valeur du domaine de valeur.
   * 
   * @return la valeur du domaine de valeur.
   */
  @Override
  public String getValeur() {
    return valeur;
  }

  /**
   * Fixer la valeur du domaine de valeur.
   * 
   * @param valeur
   *          la valeur du domaine de valeur.
   */
  @Override
  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  /**
   * Obtenir le type de recherche a effectuer.
   * 
   * @return le type de recherche a effectuer.
   */
  public String getTypeDomaine() {
    return typeDomaine;
  }

  /**
   * Fixer le type de recherche a effectuer.
   * 
   * @param typeDomaine
   *          le type de recherche a effectuer.
   */
  public void setTypeDomaine(String typeDomaine) {
    this.typeDomaine = typeDomaine;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setActif(String actif) {
    this.actif = actif;
  }

  public String getActif() {
    return actif;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }


}
