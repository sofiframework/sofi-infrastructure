package org.sofiframework.infrasofi.presentation.action.domainevaleur;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.infrasofi.modele.entite.DomaineValeur;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.objetstransfert.ObjetTransfert;

public class ListeValeurAction extends InfrastructureAction {

  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {

    DomaineForm formulaireDomaine = (DomaineForm) request.getSession().getAttribute("domaineForm");
    String nom = formulaireDomaine.getNom();
    String codeApplication = formulaireDomaine.getCodeApplication();
    String codeLangue = (String) request.getSession().getAttribute("langueEnCours");

    if (codeLangue == null) {
      codeLangue = formulaireDomaine.getCodeLangue();
    }
    this.setAttributTemporairePourAction("langueEnCours", codeLangue, request);


    // on doit charger la liste des valeurs de ce domaine.
    ListeValeurForm formulaire = (ListeValeurForm) form;

    List<DomaineValeur> listeValeur = this.getModeleInfrastructure(request).getServiceDomaineValeur()
        .getListeDomaineValeurPourDefinition(codeApplication, nom, codeLangue,true);
    this.setAttributTemporairePourAction("listeValeurDomaine", listeValeur, request);
    this.initialiserLigneSelectionneePourListeNavigation(request, "listeValeurDomaine");

    return mapping.findForward("page");
  }

  public ActionForward rafraichir(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    this.afficher(mapping, form, request, response);
    return mapping.findForward("listeValeur");
  }

  public ActionForward selectionner(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    int index = Integer.parseInt(request.getParameter("i")) - 1;

    List<DomaineValeur> liste = (List<DomaineValeur>) request.getSession().getAttribute("listeValeurDomaine");

    DomaineValeur valeur = liste.get(index);

    this.specifierLigneSelectionneePourListeNavigation(request, "listeValeurDomaine", valeur);

    return mapping.findForward("listeValeur");
  }

  public ActionForward deplacer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    int index = Integer.parseInt(request.getParameter("i")) - 1;

    String type = request.getParameter("type");
    int indexDestination = index + (type.equals("monter") ? -1 : +1);

    ObjetTransfert selection = null;

    if (indexDestination >= 0) {
      List<DomaineValeur> listeValeur = (List<DomaineValeur>) request.getSession().getAttribute("listeValeurDomaine");
      DomaineValeur valeurSource = listeValeur.get(index);
      DomaineValeur valeurDestination = listeValeur.get(indexDestination);
      Integer ordreSource = valeurSource.getOrdreAffichage();
      Integer ordreDestination = valeurDestination.getOrdreAffichage();
      valeurSource.setOrdreAffichage(ordreDestination);
      valeurDestination.setOrdreAffichage(ordreSource);
      this.getModeleInfrastructure(request).getServiceDomaineValeur().enregistrerDomaineValeur(valeurSource, null);
      this.getModeleInfrastructure(request).getServiceDomaineValeur().enregistrerDomaineValeur(valeurDestination, null);
      selection = valeurSource;
    }

    ActionForward direction = this.rafraichir(mapping, form, request, response);

    if (selection != null) {
      this.specifierLigneSelectionneePourListeNavigation(request, "listeValeurDomaine", selection);
    }

    return direction;
  }

  public ActionForward supprimer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {

    int index = Integer.parseInt(request.getParameter("i")) - 1;
    List<DomaineValeur> listeValeur = (List<DomaineValeur>) request.getSession().getAttribute("listeValeurDomaine");
    DomaineValeur v = listeValeur.get(index);

    getModeleInfrastructure(request).getServiceDomaineValeur().supprimer(v);

    ActionForward direction = this.rafraichir(mapping, form, request, response);
    return direction;
  }

  public ActionForward changerLangue(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {

    String codeLangue = request.getParameter("codeLangue");
    this.setAttributTemporairePourAction("langueEnCours", codeLangue, request);

    return this.rafraichir(mapping, form, request, response);
  }

  public ActionForward ordonner(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    List<DomaineValeur> listeValeur = (List<DomaineValeur>) request.getSession().getAttribute("listeValeurDomaine");

    for (int i = 0; i < listeValeur.size(); i++) {
      listeValeur.get(i).setOrdreAffichage(i + 1);
      this.getModeleInfrastructure(request).getServiceDomaineValeur().enregistrerDomaineValeur(listeValeur.get(i), null);
    }

    return this.rafraichir(mapping, form, request, response);
  }

  public ActionForward effacerOrdres(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    List<DomaineValeur> listeValeur = (List<DomaineValeur>) request.getSession().getAttribute("listeValeurDomaine");

    for (int i = 0; i < listeValeur.size(); i++) {
      listeValeur.get(i).setOrdreAffichage(null);
      this.getModeleInfrastructure(request).getServiceDomaineValeur().enregistrerDomaineValeur(listeValeur.get(i), null);
    }


    return this.rafraichir(mapping, form, request, response);
  }
}
