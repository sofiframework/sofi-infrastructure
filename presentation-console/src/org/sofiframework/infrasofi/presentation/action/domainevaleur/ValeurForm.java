/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.domainevaleur;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * Objet d'affaire faisant represetant un domaine de valeur.
 * <p>
 * 
 * @author Maxime Ouellet, Nurun inc.
 * @version 1.0
 */
public class ValeurForm extends BaseForm {

  /**
   * 
   */
  private static final long serialVersionUID = -6441185907194004921L;

  /** l'identifiant unique du code de l'application du domaine de valeur */
  private String codeApplication;

  /** le nom du domaine de valeur */
  private String nom;

  /** la valeur du domaine de valeur */
  private String valeur;

  /** Le code de langue de la valeur du domaine **/
  private String codeLangue;

  /** la description du domaine de valeur */
  private String description;

  /** l'ordre d'affichage du domaine de valeur */
  private String ordreAffichage;

  /** le nom du domaine parent du domaine de valeur */
  private String nomParent;

  /** la valeur du domaine parent du domaine de valeur */
  private String valeurParent;

  /** l'identifiant unique du code d'application du domaine de valeur parent */
  private String codeApplicationParent;

  /** Le code de langue de la valeur du domaine **/
  private String codeLangueParent;

  /** la liste des pages d'application qui représentent les pages de sécurité de l'application */
  private ArrayList listePageSecurite;

  private String codeClient;

  /** Détail sur la journalisation du domaine de valeur **/
  private String creePar;
  private String dateCreation;
  private String modifiePar;
  private String dateModification;

  /**
   * Constructeur par défaut.
   */
  public ValeurForm() {
    ajouterFormatterDateHeureSeconde("dateCreation");
    ajouterFormatterDateHeureSeconde("dateModification");
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un <code>submit</code>.
   * <p>
   * 
   * @param request
   *          la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  /**
   * Obtenir l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @return l'identifiant unique du code de l'application du domaine de valeur.
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique du code de l'application du domaine de valeur.
   * 
   * @param codeApplication
   *          l'identifiant unique du code de l'application du domaine de valeur.
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir le nom du domaine de valeur.
   * 
   * @return le nom du domaine de valeur.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du domaine de valeur.
   * 
   * @param nom
   *          le nom du domaine de valeur.
   */
  public void setNom(String nom) {
    if (nom != null) {
      this.nom = nom.toUpperCase();
    } else {
      this.nom = null;
    }
  }

  /**
   * Obtenir la valeur du domaine de valeur.
   * 
   * @return la valeur du domaine de valeur.
   */
  @Override
  public String getValeur() {
    return valeur;
  }

  /**
   * Fixer la valeur du domaine de valeur.
   * 
   * @param valeur
   *          la valeur du domaine de valeur.
   */
  @Override
  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  /**
   * Obtenir la description du domaine de valeur.
   * 
   * @return la description du domaine de valeur.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Fixer la description du domaine de valeur.
   * 
   * @param description
   *          la description du domaine de valeur.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Obtenir l'ordre d'affichage du domaine de valeur.
   * 
   * @return l'ordre d'affichage du domaine de valeur.
   */
  public String getOrdreAffichage() {
    return ordreAffichage;
  }

  /**
   * Fixer l'ordre d'affichage du domaine de valeur.
   * 
   * @param ordreAffichage
   *          l'ordre d'affichage du domaine de valeur.
   */
  public void setOrdreAffichage(String ordreAffichage) {
    this.ordreAffichage = ordreAffichage;
  }

  /**
   * Obtenir le nom du domaine parent du domaine de valeur.
   * 
   * @return le nom du domaine parent du domaine de valeur.
   */
  public String getNomParent() {
    return nomParent;
  }

  /**
   * Fixer le nom du domaine parent du domaine de valeur.
   * 
   * @param nomParent
   *          le nom du domaine parent du domaine de valeur.
   */
  public void setNomParent(String nomParent) {
    this.nomParent = nomParent;
  }

  /**
   * Obtenir la valeur du domaine parent du domaine de valeur.
   * 
   * @return la valeur du domaine parent du domaine de valeur.
   */
  public String getValeurParent() {
    return valeurParent;
  }

  /**
   * Fixer la valeur du domaine parent du domaine de valeur.
   * 
   * @param valeurParent
   *          la valeur du domaine parent du domaine de valeur.
   */
  public void setValeurParent(String valeurParent) {
    this.valeurParent = valeurParent;
  }

  /**
   * Obtenir l'identifiant unique du code d'application du domaine de valeur parent.
   * 
   * @return l'identifiant unique du code d'application du domaine de valeur parent.
   */
  public String getCodeApplicationParent() {
    return codeApplicationParent;
  }

  /**
   * Fixer l'identifiant unique du code d'application du domaine de valeur parent.
   * 
   * @param codeApplicationParent
   *          l'identifiant unique du code d'application du domaine de valeur parent.
   */
  public void setCodeApplicationParent(String codeApplicationParent) {
    this.codeApplicationParent = codeApplicationParent;
  }

  /**
   * Spécifie s'il existe un domaine Parent.
   * 
   * @return true s'il existe un domaine parent.
   */
  public boolean isDomaineParent() {
    return getValeurParent() != null && !getValeurParent().equals("DEFINITION");
  }

  public void setCreePar(String creePar) {
    this.creePar = creePar;
  }

  public String getCreePar() {
    return creePar;
  }

  public void setDateCreation(String dateCreation) {
    this.dateCreation = dateCreation;
  }

  public String getDateCreation() {
    return dateCreation;
  }

  public void setModifiePar(String modifiePar) {
    this.modifiePar = modifiePar;
  }

  public String getModifiePar() {
    return modifiePar;
  }

  public void setDateModification(String dateModification) {
    this.dateModification = dateModification;
  }

  public String getDateModification() {
    return dateModification;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodeLangueParent(String codeLangueParent) {
    this.codeLangueParent = codeLangueParent;
  }

  public String getCodeLangueParent() {
    return codeLangueParent;
  }

  public ArrayList getListePageSecurite() {
    return listePageSecurite;
  }

  public void setListePageSecurite(ArrayList listePageSecurite) {
    this.listePageSecurite = listePageSecurite;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

}
