/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.domainevaleur;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.DomaineValeur;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.modele.exception.ModeleException;

public class ValeurAction extends InfrastructureAction {

  private static final Log log = LogFactory.getLog(ValeurAction.class);

  /**
   * Afficher/réafficher la page.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {

    ValeurForm formulaire = (ValeurForm) form;

    // Fixer le code de langue en cours.
    setLangueEnCours(formulaire.getCodeLangue(), request);

    return mapping.findForward("page");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur utilise le bouton "Créer" afin
   * de créer un nouveau domaine de valeur.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward creer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    // Récupérer et initialiser le formulaire
    ValeurForm formulaire = (ValeurForm) form;

    //Obtenir les identifiant du domaine de valeur à charger
    String codeApplication = request.getParameter("codeApplication");
    String nom = request.getParameter("nom");
    String valeur = request.getParameter("valeur");
    String codeLangue = request.getParameter("codeLangue");
    Integer ordreAffichage = null;

    if (codeLangue == null) {
      codeLangue = GestionLibelle.getInstance().getLocaleParDefaut().toString();
    }

    if (!isVide(formulaire.getOrdreAffichage())) {
      ordreAffichage = Integer.parseInt(formulaire.getOrdreAffichage()) + 1;
    }
    formulaire.setOrdreAffichage(null);

    DomaineValeur domaineValeur = new DomaineValeur();
    domaineValeur.setCodeApplicationParent(codeApplication);
    domaineValeur.setCodeApplication(codeApplication);
    domaineValeur.setNomParent(nom);
    domaineValeur.setCodeLangue(codeLangue);
    domaineValeur.setCodeLangueParent(codeLangue);
    domaineValeur.setOrdreAffichage(ordreAffichage);

    if (valeur != null && !valeur.equals("DEFINITION")) {
      domaineValeur.setValeurParent(formulaire.getValeur());
    } else {
      domaineValeur.setNom(nom);
      domaineValeur.setValeurParent("DEFINITION");
    }

    List listeDomaineValeurEnfants = new ArrayList();
    domaineValeur.setListeDomaineValeurEnfants(listeDomaineValeurEnfants);
    formulaire.initialiserFormulaire(request, mapping, domaineValeur, true);
    formulaire.populerFormulaire(domaineValeur, request);

    formulaire.ajouterMessageInformatif("infra_sofi.information.domaine_valeur.nouvelle_valeur",
        request);
    formulaire.setNouveauFormulaire(true);

    request.getSession().removeAttribute("listeValeurDomaineEnfant");

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur accède au détail d'un domaine de valeur.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    // Récupérer les objets de base
    ModeleInfrastructure modele = (ModeleInfrastructure) this.getModele(request);
    ValeurForm formulaire = (ValeurForm) form;

    // Récupérer le domaine et populer le formulaire avec
    DomaineValeur valeur = modele.getServiceDomaineValeur().getDomaineValeur(formulaire.getCodeApplication(),
        formulaire.getNom(), formulaire.getValeur(),
        formulaire.getCodeLangue());

    if (valeur == null) {
      valeur = new DomaineValeur();
      valeur.setCodeApplication(formulaire.getCodeApplication());
      valeur.setCodeApplicationParent(formulaire.getCodeApplicationParent());
      valeur.setNom(formulaire.getNom());
      valeur.setNomParent(formulaire.getNomParent());
      valeur.setCodeLangueParent(formulaire.getCodeLangue());
      valeur.setValeur(formulaire.getValeur());
      valeur.setValeurParent(formulaire.getValeurParent());
    }
    valeur.setCodeLangue(formulaire.getCodeLangue());
    formulaire.populerFormulaire(valeur, request, true);

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lors de la sauvegarde d'un domaine de valeur.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward enregistrer(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    // Récupérer les données de base
    ModeleInfrastructure modele = (ModeleInfrastructure) this.getModele(request);
    ValeurForm formulaire = (ValeurForm) form;
    boolean erreur = false;

    // Récupérer le domaine de valeur et effectuer la sauvegarde
    DomaineValeur valeur = (DomaineValeur) formulaire.getObjetTransfert();
    DomaineValeur valeurOriginale = (DomaineValeur) formulaire.getObjetTransfertOriginal();

    if (valeur.getDateCreation() == null) {
      try {
        modele.getServiceDomaineValeur().ajouterDomaineValeur(valeur);
      } catch (Exception e) {
        this.rollback(request,
            "infra_sofi.erreur.domaine_valeur.description_langue_parent_obligatoire",
            new String[] {valeur.getCodeLangue()});
        erreur = true;
      }
    } else {
      try {
        modele.getServiceDomaineValeur().enregistrerDomaineValeur(valeur, valeurOriginale);
      } catch (ModeleException e) {
        if (log.isErrorEnabled()) {
          log.error("Erreur pour enregistrerDomaine.", e);
        }
        formulaire.ajouterMessageErreur(e, request);
        this.rollback(request);
        erreur = true;
      }
    }

    if (!erreur) {
      this.commit(request, formulaire);
      formulaire.populerFormulaire(valeur, request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur supprime un domaine de valeur.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward supprimer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    // Récupérer les données de base
    ModeleInfrastructure modele = (ModeleInfrastructure) this.getModele(request);
    ValeurForm formulaire = (ValeurForm) form;
    DomaineValeur domaine = (DomaineValeur) formulaire.getObjetTransfert();

    try {
      // Effectuer la suppression. On supprime aussi les sous-domaines
      modele.getServiceDomaineValeur().supprimerDomaineValeur(domaine);
      this.commit(request,
          "infra_sofi.information.domaine_valeur.supression_valeur_succes",
          new String[] { domaine.getValeur() });
    } catch (ModeleException e) {
      formulaire.ajouterMessageErreur("infra_sofi.erreur.commun.suppression.impossible",
          request);
      this.rollback(request);
    }

    return null;
  }
}
