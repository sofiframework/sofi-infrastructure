/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.domainevaleur;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.filtre.FiltreDomaineValeur;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.utilitaire.UtilitaireString;
import org.sofiframework.utilitaire.documentelectronique.DocumentElectronique;

public class RechercheDomaineAction extends InfrastructureAction {

  private static final Log log = LogFactory
      .getLog(RechercheDomaineAction.class);

  private static final String ATTRIBUT_LISTE = "LISTE_RECHERCHE_DOMAINE_VALEUR";

  /**
   * Méthode utilisée pour l'accès initial à la recherche des domaines de
   * valeur.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    // Acceder au formulaire
    RechercheDomaineForm formulaire = (RechercheDomaineForm) form;
    // Initialiser le formulaire
    formulaire.initialiserFormulaire(request, mapping,
        new FiltreDomaineValeur(), true);
    String codeApplicationSelectionne = (String) request.getSession()
        .getAttribute("systeme_selectionne");
    formulaire.setCodeApplication(codeApplicationSelectionne);

    Utilisateur utilisateur = this.getUtilisateur(request);
    formulaire.setCodeLangue(utilisateur.getLangue());

    formulaire.setActif("O");

    // Fixer la liste temporaire pour l'action.
    setAttributTemporaireListeNavigationPourAction(ATTRIBUT_LISTE, null, request);
    return mapping.findForward("afficher");
  }

  /**
   * Méthode utilisée pour rechercher des domaines de valeur.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward rechercher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    // Accéder aux données de base
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    RechercheDomaineForm formulaire = (RechercheDomaineForm) form;

    String paramActif = request.getParameter("actif");
    if (paramActif != null) {
      formulaire.setActif(paramActif.equals("") ? null : paramActif);
      Boolean actif = paramActif.equals("") ? null : paramActif.equals("O");
      FiltreDomaineValeur filtre = (FiltreDomaineValeur) formulaire.getObjetTransfert();
      filtre.setActif(actif);
    }

    // Appliquer la liste de navigation page par page.
    ListeNavigation liste = appliquerListeNavigation(request);

    // Fixer l'objet de filtre dans la liste de navigation avec l'aide du
    // formulaire.
    liste.setFiltre((FiltreDomaineValeur) formulaire.getObjetTransfert());
    Boolean obtenirDefinitions = formulaire.getTypeDomaine() != null
        && formulaire.getTypeDomaine().equals("DEFINITION");
    // Extraire le résultat de la recherche depuis le modèle.
    liste = modele.getServiceDomaineValeur().getListeDomaineValeurPilotage(
        liste, obtenirDefinitions);
    // Fixer la liste temporaire pour l'action.
    this.setAttributTemporairePourService(ATTRIBUT_LISTE, liste, request);
    String direction = request.getParameter("ajaxDiv") != null ? "liste" : "formulaire_recherche";
    return mapping.findForward(direction);
  }

  public ActionForward afficherResultat(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    return mapping.findForward("resultat");
  }

  /**
   * Méthode qui sert à afficher la recherche.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    // Traitement des différents cas d'affichage de la liste
    // de résultat.
    // On doit rappeler le traitement de recherché avant d’afficher la page.
    return mapping.findForward(isTraiterResultatRecherche(ATTRIBUT_LISTE,
        request) ? "rechercher" : "page");
  }

  /**
   * Méthode permettant de générer un document pdf. Le système appelera JAPSER
   * afin d'obtenir le document à générer.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donnée à l'utilisateur.
   * @throws java.io.IOException
   *           Exception entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward exporterEnPdf(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {

    // Accèder au modèle.
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    // Accéder au formulaire
    RechercheDomaineForm formulaire = (RechercheDomaineForm) form;
    // Appliquer la liste de navigation page par page.
    ListeNavigation listeLibelles = appliquerListeNavigation(request);
    // Obtenir le filtre de recherche
    FiltreDomaineValeur filtre = (FiltreDomaineValeur) formulaire
        .getObjetTransfert();

    DocumentElectronique document = null;
    try {
      String rapport = null;
      if (!UtilitaireString.isVide(formulaire.getTypeDomaine())
          && formulaire.getTypeDomaine().equals("DOMAINE")) {
        rapport = "rapport_domaine_valeur.jasper";
      } else {
        rapport = "rapport_definition_domaine_valeur.jasper";
      }

      JasperReport jasperReport = (JasperReport) JRLoader.loadObject(servlet
          .getServletContext().getRealPath("")
          + "/WEB-INF/jasper/" + rapport);

      // Créer les paramètres pour affichage dynamique du rapport.
      Map parametres = new HashMap();
      parametres.put("TITRE_RAPPORT", "Rapport domaines de valeur");

      parametres.put("LOGO_SOFI", getServlet().getServletContext().getRealPath(
          File.separator + "images" + File.separator + "rapport"
              + File.separator + "entete_rapport_sofi.jpg"));

      if (listeLibelles.getTriAttribut() != null) {
        parametres.put("ORDER_BY", listeLibelles.getTriAttribut() + " "
            + listeLibelles.getOrdreTri());
      }

      // Extraire le résultat de la recherche depuis le modèle.
      boolean domaine = !UtilitaireString.isVide(formulaire.getTypeDomaine())
          && formulaire.getTypeDomaine().equals("DOMAINE");

      // FIXME Règler un problème de dépendance vers JasperReport pour
      // Compilation d'un war de Gestion.
      // byte[] rapportRetour =
      // modele.getServiceRapport().getRapportDomaineValeur(jasperReport,
      // parametres, filtre, !domaine);

      DocumentElectronique documentpdf = new DocumentElectronique();
      documentpdf.setNomFichier("Rapport domaines de valeur.pdf");
      // documentpdf.setValeurDocument(rapportRetour);
      this.setDocumentElectronique(documentpdf, request);
      formulaire.ajouterMessageInformatif(
          "infra_sofi.information.rapport.genere_succes", request);
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour produire le rapport de domaines de valeurs.", e);
      }
    }

    return mapping.findForward("afficher");
  }
}
