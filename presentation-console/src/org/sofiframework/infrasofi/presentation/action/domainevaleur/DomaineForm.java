/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.domainevaleur;

import javax.servlet.http.HttpServletRequest;

/**
 * Formulaire de saisi de la définition d'un domaine de valeuirs.
 * <p>
 * 
 * @author Maxime Ouellet, Nurun inc.
 * @author Jean-Maxime Pelletier
 */
public class DomaineForm extends ValeurForm {

  /**
   * 
   */
  private static final long serialVersionUID = -6340609738843357558L;

  /** le code de type de tri des domaines de valeur */
  private String codeTypeTri;

  /**
   * Constructeur par défaut.
   */
  public DomaineForm() {
    this.ajouterFormatterDateHeureSeconde("dateCreation");
    this.ajouterFormatterDateHeureSeconde("dateModification");
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un
   * <code>submit</code>.
   * <p>
   * 
   * @param request
   *          la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  /**
   * Obtenir le code de type de tri des domaines de valeur.
   * 
   * @return le code de type de tri des domaines de valeur.
   */
  public String getCodeTypeTri() {
    return codeTypeTri;
  }

  /**
   * Fixer le code de type de tri des domaines de valeur.
   * 
   * @param codeTypeTri
   *          le code de type de tri des domaines de valeur.
   */
  public void setCodeTypeTri(String codeTypeTri) {
    this.codeTypeTri = codeTypeTri;
  }
}
