/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseMultiLigneForm;

/**
 * Formulaire qui contient comme critère l'application.
 * 
 * @author Jean-Maxime Pelletier
 */
public abstract class RechercheApplicativeForm extends BaseMultiLigneForm {

  private static final long serialVersionUID = 5521421756457682591L;

  /**
   * Le code d'application
   */
  private String codeApplication;

  /**
   * Fixe le code d'application
   * 
   * @param codeApplication
   *          le code d'application
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Retourne le code d'application
   * 
   * @return le code d'application
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Permet de concerver un code d'application au cours des recherches de
   * l'utilisateur.
   * 
   * @param request
   *          Requête HTTP
   */
  public void configurerCodeApplicationSelectionne(HttpServletRequest request) {
    if ("GET".equals(request.getMethod())) {
      this.setCodeApplication((String) request.getSession().getAttribute("systeme_selectionne"));
    } else {
      if (this.getCodeApplication() != null) {
        request.getSession().setAttribute("systeme_selectionne",
            this.getCodeApplication());
      }
    }
  }
}
