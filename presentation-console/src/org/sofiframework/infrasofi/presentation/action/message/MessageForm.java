/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.message;

import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Message;
import org.sofiframework.infrasofi.presentation.action.FormulaireTrace;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;

/**
 * Formulaire de détail d'un message.
 * 
 * @author Jean-Maxime Pelletier
 */
public class MessageForm extends FormulaireTrace {

  private static final long serialVersionUID = -2680433109616867101L;

  private static final Log log = LogFactory.getLog(MessageForm.class);

  private String id;

  private String cleMessage;

  private String codeLangue;

  private String codeApplication;

  private String texteMessage;

  private String codeSeverite;

  private String aideContextuelle;

  /**
   * Lors d'une modification, ce champs conserve la valeur de l'cleMessage afin de savoir s'il a été modifier par
   * l'utilisateur.
   */
  private String ancienneCleMessage;

  /**
   * Lors d'une modification, ce champs conserve la valeur du code de langue afin de savoir s'il a été modifier par
   * l'utilisateur.
   */
  private String ancienCodeLangue;

  private String aideContextuelleHtml;

  private String reference;

  private String codeClient;

  public MessageForm() {
    super();
  }

  /**
   * Valide le formulaire.
   * 
   * @param request
   *          la requete HTTP en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
    try {
      ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);

      if (isNouveauFormulaire() || this.isCleObjetTransfertModifie()) {
        Message message = null;
        if (StringUtils.isEmpty(getCodeClient())) {
          message = modele.getServiceMessage().get(getCleMessage(), getCodeLangue());
        } else {
          message = modele.getServiceMessage().get(getCleMessage(), getCodeLangue(), getCodeClient());
        }
        if (message != null) {
          this.ajouterMessageErreur("infra_sofi.erreur.gestion_message.identifiant.unique", "identifiant", request);
        }
      }

      if (getCleMessage() != null && getCleMessage().indexOf(getCodeApplication().toLowerCase()) == -1) {
        ajouterMessageErreur("infra_sofi.erreur.gestion_aide.cleAide.code_systeme_inexistant", request);
      }
    } catch (ModeleException e) {
      log.error("Erreur lors de la validation du Message", e);
      ajouterMessageErreurGeneral(e.getMessage(), request);
    }
  }

  /**
   * Indique si la clé primaire a été modifié lors de l'édition, ce qui obligerais de faire d'abord une suppression et
   * un ajout, plutôt qu'une simple sauvegarde.
   * 
   * @return vrai si la clé primaire a été modifié, faux sinon.
   */
  public boolean isClePrimaraireChange() {
    return !ancienneCleMessage.equals(getCleMessage()) || !ancienCodeLangue.equals(getCodeLangue());
  }

  /**
   * Appel la population d'un objet de transfert tout en initialisant le formulaire à neuf avant la population.
   * <p>
   * Par défaut, un formulaire placé dans la session n'est pas remis à blanc avant la population du formulaire.
   * 
   * @param objetTransfert
   *          L'objet de transfert avec lequel populer le formulaire
   * @param request
   *          la requête HTTP en traitement
   * @param initialiserFormulaireDansSession
   *          true si on désire initialiser le formulaire dans la session
   * @return les erreurs de validation
   */
  @Override
  @SuppressWarnings("unchecked")
  public LinkedHashMap populerFormulaire(ObjetTransfert objetTransfert, HttpServletRequest request,
      boolean initialiserFormulaireDansSession) {
    Message message = (Message) objetTransfert;

    LinkedHashMap valeur = super.populerFormulaire(objetTransfert, request, initialiserFormulaireDansSession);
    // La clé primaire composé est conservé afin de savoir si elle a été
    // modifier par l'utilisateur et de supprimé l'ancien message lors
    // que cette clé primaire est modifiée.
    this.ancienneCleMessage = message.getCleMessage();
    this.ancienCodeLangue = message.getCodeLangue();
    return valeur;
  }

  public void setCleMessage(String cleMessage) {
    this.cleMessage = cleMessage == null ? null : cleMessage.trim();
  }

  public String getCleMessage() {
    return cleMessage;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setTexteMessage(String texteMessage) {
    this.texteMessage = texteMessage;
  }

  public String getTexteMessage() {
    return texteMessage;
  }

  public void setCodeSeverite(String codeSeverite) {
    this.codeSeverite = codeSeverite;
  }

  public String getCodeSeverite() {
    return codeSeverite;
  }

  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  public String getAideContextuelle() {
    return aideContextuelle;
  }

  public void setAncienneCleMessage(String ancienneCleMessage) {
    this.ancienneCleMessage = ancienneCleMessage;
  }

  public String getAncienneCleMessage() {
    return ancienneCleMessage;
  }

  public void setAncienCodeLangue(String ancienCodeLangue) {
    this.ancienCodeLangue = ancienCodeLangue;
  }

  public String getAncienCodeLangue() {
    return ancienCodeLangue;
  }

  public void setAideContextuelleHtml(String aideContextuelleHtml) {
    this.aideContextuelleHtml = aideContextuelleHtml;
  }

  public String getAideContextuelleHtml() {
    return aideContextuelleHtml;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public String getReference() {
    return reference;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


}
