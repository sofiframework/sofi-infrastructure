/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.message;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.cache.UtilitaireCache;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.entite.Message;
import org.sofiframework.infrasofi.modele.service.ServiceMessage;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.utilitaire.UtilitaireHttp;

/**
 * Action de la page de détail d'un message.
 * 
 * @author Jean-Maxime Pelletier
 */
public class MessageAction extends InfrastructureAction {

  private static final Log log = LogFactory.getLog(MessageAction.class);

  /**
   * Réaffiche la page en spécifiant les styles en fonction de la langue sélectionnée par l'utilisateur.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    MessageForm formulaire = (MessageForm) form;

    if (formulaire.getCodeLangue() == null) {
      formulaire.setCodeLangue("fr_CA");
    }

    return mapping.findForward("page");
  }

  /**
   * Affiche le détail d'un message pour la consultation ou la modification.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    String direction = null;
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    MessageForm formulaire = (MessageForm) form;

    try {
      String id = formulaire.getId();
      Message message = (Message) modele.getServiceMessage().get(new Long(id));
      formulaire.initialiserFormulaire(request, mapping, true);
      formulaire.populerFormulaire(message, request, true);

      if (message == null) {
        // Traiter l'inexistance du message
        formulaire.ajouterMessageErreur("infra_sofi.erreur.commun.element.non.trouve", request);
        direction = "recherche";
      } else {
        // Appel de la page du détail
        direction = "afficher";
      }
    } catch (ModeleException e) {
      // Traiter le problème venant du modèle.
      formulaire.ajouterMessageErreur(e, request);
      direction = "recherche";
    }

    return mapping.findForward(direction);
  }

  /**
   * Affiche le détail d'un message pour la consultation ou la modification.
   */
  public ActionForward changerLangue(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    MessageForm formulaire = (MessageForm) form;
    String cle = formulaire.getCleMessage();
    String langue = formulaire.getCodeLangue();
    String codeClient = formulaire.getCodeClient();
    formulaire.initialiserFormulaire(request, mapping, true);
    try {
      Message message = modele.getServiceMessage().get(cle, langue, codeClient);
      formulaire.populerFormulaire(message == null ? new Message(cle, langue, codeClient) : message, request, true);
      this.genererReponseAjax(formulaire, message, request, response);
    } catch (ModeleException e) {
      // Traiter le problème venant du modèle.
      formulaire.ajouterMessageErreur(e, request);
    }

    return null;
  }

  /**
   * Affiche le formulaire de création d'un message.
   */
  public ActionForward ajouter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    MessageForm formulaire = (MessageForm) form;
    formulaire.initialiserFormulaire(request, mapping, new Message(), true);
    formulaire.ajouterMessageInformatif("infra_sofi.information.gestion_message.creation", request);
    String codeApplicationSelectionne = (String) request.getSession().getAttribute("systeme_selectionne");
    formulaire.setCodeApplication(codeApplicationSelectionne);
    return mapping.findForward("afficher");
  }

  /*
   * Génère la réponse ajax pour un objet message.
   */
  private void genererReponseAjax(MessageForm formulaire, Message message, HttpServletRequest request,
      HttpServletResponse response) {
    if (message == null) {
      formulaire.setTexteMessage("");
      formulaire.setNouveauFormulaire(true);
      formulaire.setCreePar("");
      formulaire.setDateCreation("");
      formulaire.setModifiePar("");
      formulaire.setDateModification("");
      formulaire.setNouveauFormulaire(true);
    }

    // Ajouter une valeur dans la réponse pour un identifiant.
    ajouterValeurDansReponse("texteMessage", formulaire.getTexteMessage(), request, response);
    ajouterValeurDansReponse("creePar", formulaire.getCreePar(), request, response);
    ajouterValeurDansReponse("dateCreation", formulaire.getDateCreation(), request, response);
    ajouterValeurDansReponse("modifiePar", formulaire.getModifiePar(), request, response);
    ajouterValeurDansReponse("dateModification", formulaire.getDateModification(), request, response);
    genererReponseXMLAvecValeurs(request, response);
  }

  /**
   * Traite le formulaire de modification/création d'un message.
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    ServiceMessage serviceMessage = modele.getServiceMessage();
    MessageForm formulaire = (MessageForm) form;
    Message message = (Message) formulaire.getObjetTransfert();
    message.setCodeLangue(formulaire.getCodeLangue());

    try {
      String ancienneCleMessage = null;
      /**
       * On doit spécifier une ancienne clé primaire si celle-ci est modifiée par l'utilisateur
       */
      if (!formulaire.isNouveauFormulaire()) {
        ancienneCleMessage = formulaire.isCleObjetTransfertModifie() ? formulaire.getAncienneCleMessage() : null;
      }

      serviceMessage.enregistrer(message, ancienneCleMessage);

      this.commit(request, formulaire);

      // Populer le formulaire après enregistrement.
      formulaire.populerFormulaire(message, request);
    } catch (ModeleException e) {
      this.rollback(request, e.getMessage(), null);
      if (log.isErrorEnabled()) {
        log.error("Erreur lors de l'enregistrement d'un Message: " + message, e);
      }
      formulaire.ajouterMessageErreur(e, request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Exécute la suppression d'un message.
   */
  public ActionForward supprimer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    String direction = null;
    MessageForm formulaire = (MessageForm) form;
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);

    try {
      modele.getServiceMessage().supprimerMessage(formulaire.getCleMessage(), formulaire.getCodeClient());
      this.commit(request, "infra_sofi.information.gestion_message.suppression.succes",
          new String[] { formulaire.getCleMessage() });
      direction = "recherche";
    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur lors de la suppression d'un message, identifiant : '" + formulaire.getCleMessage()
            + "', code de langue : '" + formulaire.getCodeLangue() + "'", e);
      }
      this.rollback(request, e.getMessage(), null);
      direction = "afficher";
    }

    return mapping.findForward(direction);
  }

  /**
   * Exécute l'initialisation d'un message.
   */
  public ActionForward initialiserMessage(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    MessageForm formulaire = (MessageForm) form;
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);

    Application application = modele.getServiceApplication().getApplication(formulaire.getCodeApplication());

    String[] listeNoeudCache = getUrlApplication(formulaire.getCodeApplication(), request);

    try {
      for (int i = 0; i < listeNoeudCache.length; i++) {
        StringBuffer adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[i]);
        adresseInitialisation.append(UtilitaireCache.getUrlMajMessage(formulaire.getCodeApplication()));
        adresseInitialisation.append(this.ajouterContenuSecuriteUrl(application));
        
        if (!isVide(formulaire.getCodeClient())) {
          adresseInitialisation.append("?codeClient=");
          adresseInitialisation.append(formulaire.getCodeClient());
        }

        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString());
        
        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

      }

      formulaire.ajouterMessageInformatif("infra_sofi.information.message.message.succes",
          new String[] { formulaire.getCleMessage() }, request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur("infra_sofi.erreur.gestion.application.communication" + e.getMessage(), request);
    }

    return mapping.findForward("afficher");
  }
  
  public ActionForward copier(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
    MessageForm messageForm = (MessageForm)form;
    
    Message message = new Message((Message) messageForm.getObjetTransfert());
    messageForm.populerFormulaire(message, request);
    return mapping.findForward("afficher");
    
  }
}
