/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.message;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.presentation.action.RechercheApplicativeForm;

/**
 * Formulaire qui contient les critères de recherche des messages.
 * 
 * @author Jean-Maxime Pelletier
 */
public class RechercheMessageForm extends RechercheApplicativeForm {

  private static final long serialVersionUID = 3716843343699191754L;

  /**
   * L'identifiant du message
   */
  private String cleMessage;

  /**
   * Le code de langue
   */
  private String codeLangue = "fr_CA";

  /**
   * Le contenu du message
   */
  private String texteMessage;

  /**
   * Le code de sévérité
   */
  private String codeSeverite;

  /**
   * L'aide contextuelle
   */
  private String aideContextuelle;

  private String codeClient;
  
  private String reference;

  public RechercheMessageForm() {
    this.setFormulaireIdentifiant("cleMessage");
  }

  /**
   * Valide le formulaire.
   * 
   * @param request
   *          la requete HTTP en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {

  }

  /**
   * Fixe l'identifiant du message
   * 
   * @param identifiant
   *          l'identifiant du message
   */
  public void setCleMessage(String cleMessage) {
    this.cleMessage = cleMessage == null ? null : cleMessage.trim();
  }

  /**
   * Retourne l'identifiant du message
   * 
   * @return l'identifiant du message
   */
  public String getCleMessage() {
    return cleMessage;
  }

  /**
   * Fixe le code de langue
   * 
   * @param codeLangue
   *          le code de langue
   */
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  /**
   * Retourne le code de langue
   * 
   * @return le code de langue
   */
  public String getCodeLangue() {
    return codeLangue;
  }

  /**
   * Fixe le contenu du message
   * 
   * @param texte
   *          le contenu du message
   */
  public void setTexteMessage(String texteMessage) {
    this.texteMessage = texteMessage;
  }

  /**
   * Retourne le contenu du message
   * 
   * @return le contenu du message
   */
  public String getTexteMessage() {
    return texteMessage;
  }

  /**
   * Fixe le code de sévérité
   * 
   * @param codeSeverite
   *          le code de sévérité
   */
  public void setCodeSeverite(String codeSeverite) {
    this.codeSeverite = codeSeverite;
  }

  /**
   * Retourne le code de sévérité
   * 
   * @return le code de sévérité
   */
  public String getCodeSeverite() {
    return codeSeverite;
  }

  /**
   * Fixe l'aide contextuelle
   * 
   * @param aideContextuelle
   *          l'aide contextuelle
   */
  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  /**
   * Retourne l'aide contextuelle
   * 
   * @return l'aide contextuelle
   */
  public String getAideContextuelle() {
    return aideContextuelle;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

}