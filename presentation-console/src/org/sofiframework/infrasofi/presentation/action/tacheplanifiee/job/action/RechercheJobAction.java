/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.job.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzJob;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzScheduler;
import org.sofiframework.infrasofi.presentation.action.BaseRechercheAction;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.job.formulaire.RechercheJobForm;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class RechercheJobAction extends BaseRechercheAction{

  private String LISTE_ORDONNANCEUR = "LISTE_ORDONNANCEUR";

  @Override
  public ActionForward afficher(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {
    chargerListeOrdonnanceur((RechercheJobForm)form,request);
    return mapping.findForward("page");
  }

  @Override
  public ObjetTransfert construireNouveauFiltre() {
    return new FiltreQuartzJob();
  }

  @Override
  public void effectuerRecherche(ListeNavigation liste, BaseForm formulaire,
      ModeleInfrastructure modele, HttpServletRequest request,
      HttpServletResponse response) throws ModeleException {
    liste.ajouterTriInitial("nom", false);
    liste.setMaxParPage(10);
    // TODO : a améliorer
    // On injecte les critères de recherche pour le filtre parent "manuellement". Ya surement une façon plus correcte
    // de faire
    FiltreQuartzJob filtre = (FiltreQuartzJob) liste.getFiltre();
    RechercheJobForm formulaireRechercheJob = (RechercheJobForm)formulaire;
    filtre.getScheduler().setCodeApplication(formulaireRechercheJob.getCodeApplication());
    filtre.getScheduler().setCodeScheduler(formulaireRechercheJob.getCodeScheduler());
    modele.getServiceJob().getListe(liste);
  }

  @Override
  public String getNomListeDansSession() {
    return "listeJob";
  }

  @Override
  public ActionForward fixerApplication(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    chargerListeOrdonnanceur((RechercheJobForm)form,request);
    super.fixerApplication( mapping,  form,  request,  response);
    return mapping.findForward("ajax_liste_ordonnanceur");
  }

  @SuppressWarnings({ "rawtypes" })
  private void chargerListeOrdonnanceur(RechercheJobForm formulaire,HttpServletRequest request){
    List ordonnanceurs = new ArrayList();
    List applications = (List) request.getSession().getAttribute(LISTE_APPLICATIONS_UTILISATEUR);

    if(applications != null && applications.size() > 0) {
      ModeleInfrastructure modele = getModeleInfrastructure(request);
      String codeApplication = ((Application)applications.get(0)).getCodeApplication() ;

      if(formulaire.getCodeApplication() != null
          && !formulaire.getCodeApplication().equals("")) {
        codeApplication = formulaire.getCodeApplication();
      }

      FiltreQuartzScheduler filtre = new FiltreQuartzScheduler();
      filtre.setCodeApplication(codeApplication);
      ordonnanceurs = modele.getServiceOrdonnanceur().getListe(filtre);
    }

    this.setAttributTemporairePourService(LISTE_ORDONNANCEUR, ordonnanceurs, request);
  }
}
