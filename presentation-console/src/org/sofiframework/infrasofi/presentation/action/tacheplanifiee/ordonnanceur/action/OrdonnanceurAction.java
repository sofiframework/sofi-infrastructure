/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.ordonnanceur.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.QuartzScheduler;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.ordonnanceur.formulaire.OrdonnanceurForm;
import org.sofiframework.modele.exception.ModeleException;


public class OrdonnanceurAction extends InfrastructureAction {

  public ActionForward afficher(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {
    return mapping.findForward("page");
  }

  public ActionForward creer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    OrdonnanceurForm formulaire = (OrdonnanceurForm) form;
    formulaire.initialiserFormulaire(request, mapping, new QuartzScheduler(), true);
    formulaire.ajouterMessageInformatif("infra_sofi.information.gestion_ordonnanceur.confirmation_ajout.ordonnanceur", request);
    return mapping.findForward("afficher");
  }

  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    ActionForward result = null;

    ModeleInfrastructure modele = getModeleInfrastructure(request);
    OrdonnanceurForm formulaire = (OrdonnanceurForm) form;

    String schedulerId = request.getParameter("schedulerId");
    String codeApplication = request.getParameter("codeApplication");

    // On valide la securite
    if(isApplicationPermise(request,codeApplication)){
      try {
        QuartzScheduler ordonnanceur = (QuartzScheduler) modele.getServiceOrdonnanceur().get(Long.parseLong(schedulerId));
        formulaire.initialiserFormulaire(request, mapping, true);
        formulaire.populerFormulaire(ordonnanceur, request);
      } catch (ModeleException e) {
        e.printStackTrace();
        formulaire.ajouterMessageErreur(e, request);
      }
      result = mapping.findForward("afficher");
    }
    return result;
  }

  public ActionForward enregistrer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    ModeleInfrastructure modele = getModeleInfrastructure(request);
    OrdonnanceurForm formulaire = (OrdonnanceurForm) form;
    QuartzScheduler ordonnanceur = (QuartzScheduler) formulaire.getObjetTransfert();

    // Effectuer l'ajout ou la modification
    try {
      if(formulaire.isNouveauFormulaire()) {
        modele.getServiceOrdonnanceur().ajouter(ordonnanceur);
      } else {
        modele.getServiceOrdonnanceur().modifier(ordonnanceur);
      }
      this.commit(request, formulaire);

      // Populer le formulaire en traitement.
      formulaire.populerFormulaire(ordonnanceur, request);
    } catch (ModeleException e) {
      this.rollback(request);
      formulaire.ajouterMessageErreur(e, request);
    }

    return mapping.findForward("afficher");
  }

  public ActionForward supprimer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    ModeleInfrastructure modele = getModeleInfrastructure(request);
    OrdonnanceurForm formulaire = (OrdonnanceurForm) form;
    QuartzScheduler ordonnanceur = (QuartzScheduler) formulaire.getObjetTransfert();
    String direction = null;

    try {
      modele.getServiceOrdonnanceur().supprimer(ordonnanceur) ;
      this.commit(request,
          "infra_sofi.information.gestion_ordonnanceur.confirmation_suppression",
          new Object[]{formulaire.getNom()});

      direction = "rechercher";
    } catch (ModeleException e) {
      this.rollback(request);
      formulaire.ajouterMessageErreur("infra_sofi.erreur.gestion.ordonnanceur.supprimer.dependances", request);

      direction = "afficher";
    }

    return mapping.findForward(direction);
  }
}
