/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.job.formulaire;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.modele.entite.QuartzJob;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.FormulaireHashMap;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class JobForm extends FormulaireHashMap{

  private static final long serialVersionUID = -4554513306985063549L;

  private String codeApplication;
  private String codeScheduler;
  private String nom, groupe, description, nomClasse ;
  private boolean jobDurable, jobVolatile ;
  private boolean jobRequestRecovery;

  public JobForm(){
    super();
  }

  @Override
  public void validerFormulaire(HttpServletRequest request) {
    if (!isNouveauFormulaire()) {
      QuartzJob job = (QuartzJob) this.getObjetTransfert();
      if (job != null && job.getActif()) {
        this.ajouterMessageErreur("Impossible de modifier une job qui est active (avec des déclencheurs actifs).", request);
      }
    }
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getGroupe() {
    return groupe;
  }

  public void setGroupe(String groupe) {
    this.groupe = groupe;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getNomClasse() {
    return nomClasse;
  }

  public void setNomClasse(String nomClasse) {
    this.nomClasse = nomClasse;
  }

  public boolean isJobDurable() {
    return jobDurable;
  }

  public void setJobDurable(boolean jobDurable) {
    this.jobDurable = jobDurable;
  }

  public boolean isJobVolatile() {
    return jobVolatile;
  }

  public void setJobVolatile(boolean jobVolatile) {
    this.jobVolatile = jobVolatile;
  }

  public boolean isJobPermanente() {
    return ((QuartzJob)this.getObjetTransfert()).isJobPermanente();
  }

  public boolean isJobRequestRecovery() {
    return jobRequestRecovery;
  }

  public void setJobRequestRecovery(boolean jobRequestRecovery) {
    this.jobRequestRecovery = jobRequestRecovery;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeScheduler() {
    return codeScheduler;
  }

  public void setCodeScheduler(String codeScheduler) {
    this.codeScheduler = codeScheduler;
  }

}