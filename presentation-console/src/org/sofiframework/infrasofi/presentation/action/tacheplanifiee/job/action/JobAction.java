/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.job.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.quartz.JobDataMap;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.entite.QuartzJob;
import org.sofiframework.infrasofi.modele.entite.QuartzScheduler;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzScheduler;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzTrigger;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.job.formulaire.JobForm;
import org.sofiframework.modele.exception.ModeleException;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class JobAction extends InfrastructureAction{

  private String LISTE_ORDONNANCEUR = "LISTE_ORDONNANCEUR";
  private String LISTE_JOB_TRIGGER = "liste_job_trigger";

  public ActionForward afficher(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    getListeTrigger( mapping,  form,  request, response);
    return mapping.findForward("page");
  }

  @Override
  public ActionForward fixerApplication(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    chargerListeOrdonnanceur((JobForm)form,request);
    super.fixerApplication( mapping,  form,  request,  response);
    return mapping.findForward("ajax_liste_ordonnanceur");
  }

  @SuppressWarnings({ "rawtypes" })
  private void chargerListeOrdonnanceur(JobForm formulaire,HttpServletRequest request){
    List ordonnanceurs = new ArrayList();
    List applications = (List) request.getSession().getAttribute(LISTE_APPLICATIONS_UTILISATEUR);

    if (applications != null && applications.size() > 0) {
      ModeleInfrastructure modele = getModeleInfrastructure(request);
      String codeApplication = ((Application)applications.get(0)).getCodeApplication() ;

      if(formulaire.getCodeApplication() != null && !formulaire.getCodeApplication().equals("")) {
        codeApplication = formulaire.getCodeApplication();
      }

      FiltreQuartzScheduler filtre = new FiltreQuartzScheduler();
      filtre.setCodeApplication(codeApplication);
      ordonnanceurs = modele.getServiceOrdonnanceur().getListe(filtre);
    }

    this.setAttributTemporairePourService(LISTE_ORDONNANCEUR, ordonnanceurs, request);
  }

  public ActionForward creer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    JobForm formulaire = (JobForm) form;
    formulaire.initialiserFormulaire(request, mapping, new QuartzJob(), true);
    formulaire.ajouterMessageInformatif("infra_sofi.information.gestion_tache.confirmation_ajout.tache", request);
    formulaire.setProprietesDynamiques( new JobDataMap());
    formulaire.setModeLectureSeulement(Boolean.FALSE);
    chargerListeOrdonnanceur(formulaire,request);
    return mapping.findForward("afficher");
  }

  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ActionForward result = null;
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    JobForm formulaire = (JobForm) form;
    String jobId = request.getParameter("jobId");
    try {
      QuartzJob job = (QuartzJob) modele.getServiceJob().get(Long.parseLong(jobId));
      String codeApplication = job.getScheduler().getCodeApplication();
      // On valide la securite
      if (isApplicationPermise(request,codeApplication)) {
        formulaire.initialiserFormulaire(request, mapping, true);
        formulaire.populerFormulaire(job, request);
        formulaire.setCodeApplication(codeApplication);
        formulaire.setCodeScheduler(job.getScheduler().getCodeScheduler());
        formulaire.setProprietesDynamiques((JobDataMap) job.getJobDataInMap());
        formulaire.setModeLectureSeulement(job.getActif());
        getListeTrigger( mapping,  form, request,  response);
        result = mapping.findForward("afficher");
      }
    } catch (ModeleException e) {
      formulaire.ajouterMessageErreur(e, request);
    }
    return result;
  }

  public ActionForward enregistrer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    JobForm formulaire = (JobForm) form;
    QuartzJob job = (QuartzJob) formulaire.getObjetTransfert();

    // On injecte l'identifiant du scheduler dans le job
    job.setSchedulerId(this.getSchedulerId(formulaire.getCodeApplication(), formulaire.getCodeScheduler(), request));

    // Faire le traitement pour mettre la liste de propriete dans le hashmap
    job.setJobDatabyMap(formulaire.getProprietesDynamiques());

    // Effectuer l'ajout ou la modification
    try {
      modele.getServiceJob().enregistrer(job,(QuartzJob)formulaire.getObjetTransfertOriginal());
      this.commit(request, formulaire);
      // Populer le formulaire en traitement.
      formulaire.populerFormulaire(job, request);
    } catch (ModeleException e) {
      this.rollback(request);
      formulaire.ajouterMessageErreur(e, request);
    }

    return mapping.findForward("afficher");
  }

  public ActionForward supprimer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws ServletException {
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    JobForm formulaire = (JobForm) form;
    QuartzJob job = (QuartzJob) formulaire.getObjetTransfert();
    String direction = null;

    try {
      modele.getServiceJob().supprimer(job.getId()) ;
      this.commit(request,
          "infra_sofi.information.gestion_tache.confirmation_suppression",
          new Object[]{formulaire.getNom()});
      direction = "rechercher";
    } catch (ModeleException e) {
      this.rollback(request);
      formulaire.ajouterMessageErreur("infra_sofi.erreur.gestion.tache.supprimer.dependances", request);
      direction = "afficher";
    }

    return mapping.findForward(direction);
  }

  public ActionForward afficherProprieteDynamique(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws ServletException {
    return mapping.findForward("ajax_propriete_dynamique");
  }

  public ActionForward getListeTrigger(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws ServletException {
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    JobForm formulaire = (JobForm) form;
    QuartzJob job = (QuartzJob) formulaire.getObjetTransfert();
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(job.getSchedulerId());
    filtre.getJob().setGroupe(job.getGroupe());
    filtre.getJob().setNom(job.getNom());
    ListeNavigation liste = this.appliquerListeNavigation(request);
    liste.setObjetFiltre(filtre);
    modele.getServiceDeclencheur().getListe(liste);
    this.setAttributTemporaireListeNavigationPourAction(LISTE_JOB_TRIGGER, liste, request);

    return mapping.findForward("ajax_liste_declencheur");
  }

  /**
   * Permet de récupérer un identifiant d'ordonnanceur à partir de la liste en session
   * et des valeurs passées en paramètre
   * @param codeApplication
   * @param codeScheuler
   * @param request
   * @return
   */
  private Long getSchedulerId(String codeApplication, String codeScheduler, HttpServletRequest request) {
    Long schedulerId = null;
    List<QuartzScheduler> liste = (List<QuartzScheduler>) request.getSession().getAttribute(LISTE_ORDONNANCEUR);
    if(liste != null) {
      for(Iterator<QuartzScheduler> i = liste.iterator(); i.hasNext() && schedulerId == null; ) {
        QuartzScheduler scheduler = i.next();
        if(scheduler.getCodeApplication().equals(codeApplication) && scheduler.getCodeScheduler().equals(codeScheduler)) {
          schedulerId = scheduler.getId();
        }
      }
    }
    return schedulerId;
  }

}