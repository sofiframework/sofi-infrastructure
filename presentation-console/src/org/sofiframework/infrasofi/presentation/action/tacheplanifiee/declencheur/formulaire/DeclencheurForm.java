/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.declencheur.formulaire;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.QuartzCronTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzJob;
import org.sofiframework.infrasofi.modele.entite.QuartzSimpleTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzTrigger;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzTrigger;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.FormulaireHashMap;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.job.formulaire.JobForm;
import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class DeclencheurForm extends FormulaireHashMap{

  private static final long serialVersionUID = -2458649634959472296L;

  private String nom,groupe,description,type;
  private boolean triggerVolatile;
  private String priorite;
  private String appeleManque;

  private BaseForm enfant;

  public DeclencheurForm(){
    super();

    ajouterCorrespondanceFormulaireEnfants(DeclencheurSimpleForm.class, QuartzSimpleTrigger.class );
    ajouterCorrespondanceFormulaireEnfants(DeclencheurCronForm.class, QuartzCronTrigger.class );
  }

  @SuppressWarnings("unchecked")
  @Override
  public void validerFormulaire(HttpServletRequest request) {
    // Deux triggers ne peuvent pas avoir le meme nom et groupe
    if(this.isNouveauFormulaire()) {
      JobForm jobForm = (JobForm)request.getSession().getAttribute("jobForm");
      QuartzJob jobParent = (QuartzJob) jobForm.getObjetTransfert();
      FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(jobParent.getSchedulerId(), this.getNom(), this.getGroupe());
      List<QuartzTrigger> liste = ((ModeleInfrastructure)this.getModele(request)).getServiceDeclencheur().getListe(filtre);
      if(liste != null && liste.size() > 0) {
        this.ajouterMessageErreur("Un déclencheur du même nom existe déjà. Veuillez choisir un autre nom.", request);
      }
    }
  }

  public String getNom() {
    return nom;
  }
  public void setNom(String nom) {
    this.nom = nom;
  }
  public String getGroupe() {
    return groupe;
  }
  public void setGroupe(String groupe) {
    this.groupe = groupe;
  }
  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }
  public boolean isTriggerVolatile() {
    return triggerVolatile;
  }
  public void setTriggerVolatile(boolean triggerVolatile) {
    this.triggerVolatile = triggerVolatile;
  }

  public BaseForm getEnfant() {
    return enfant;
  }

  public void setEnfant(BaseForm enfant) {
    this.enfant = enfant;
  }

  public String getPriorite() {
    return priorite;
  }

  public void setPriorite(String priorite) {
    this.priorite = priorite;
  }

  public String getAppeleManque() {
    return appeleManque;
  }

  public void setAppeleManque(String appeleManque) {
    this.appeleManque = appeleManque;
  }
}
