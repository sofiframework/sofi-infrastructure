/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.proprietedynamique.formulaire;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.quartz.JobDataMap;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.FormulaireHashMap;
import org.sofiframework.presentation.struts.form.BaseForm;


public class ListeProprieteDynamiqueForm extends BaseForm{

  private static final long serialVersionUID = 4555013764429681892L;

  private FormulaireHashMap formulaireDeReference;
  private JobDataMap proprieteNonString;

  private List<ProprieteDynamiqueForm> proprieteDynamiqueForm;

  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  public void setProprieteDynamiqueForm(List<ProprieteDynamiqueForm> proprieteDynamiqueForm) {
    this.proprieteDynamiqueForm = proprieteDynamiqueForm;
  }

  public List<ProprieteDynamiqueForm> getProprieteDynamiqueForm() {
    return proprieteDynamiqueForm;
  }

  public JobDataMap getMapProprieteDynamique(){
    JobDataMap map = new JobDataMap();
    if(proprieteDynamiqueForm != null){
      for(ProprieteDynamiqueForm formulaire:proprieteDynamiqueForm) {
        map.put(formulaire.getKey(), formulaire.getValue());
      }
    }
    if(proprieteNonString != null) {
      map.putAll(proprieteNonString);
    }
    return map;
  }

  @SuppressWarnings("unchecked")
  public void setProprieteDynamiqueForm(Map map){

    proprieteDynamiqueForm = new ArrayList();
    proprieteNonString = new JobDataMap();
    if(map != null){
      Set proprietes = map.entrySet();
      Iterator it = proprietes.iterator();
      while (it.hasNext()) {
        Map.Entry entry = (Map.Entry) it.next();
        if(entry.getKey() instanceof String
            && entry.getValue() instanceof String) {
          proprieteDynamiqueForm.add(new ProprieteDynamiqueForm((String)entry.getKey(),(String)entry.getValue()));
        } else {
          proprieteNonString.put(entry.getKey(),entry.getValue());
        }
      }
    }
  }

  public FormulaireHashMap getFormulaireDeReference() {
    return formulaireDeReference;
  }

  public void setFormulaireDeReference(FormulaireHashMap formulaireDeReference) {
    this.formulaireDeReference = formulaireDeReference;
  }

}
