/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.proprietedynamique.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.quartz.JobDataMap;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.FormulaireHashMap;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.proprietedynamique.formulaire.ListeProprieteDynamiqueForm;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.proprietedynamique.formulaire.ProprieteDynamiqueForm;


public class ProprieteDynamiqueAction extends InfrastructureAction{

  private static final int JOB = 1;
  private static final int TRIGGER = 2;

  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    return mapping.findForward("page_ajax");
  }

  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    ListeProprieteDynamiqueForm formulaire = (ListeProprieteDynamiqueForm)form;

    FormulaireHashMap formulaireParent = null;
    String direction = "";
    String param = request.getParameter("proprietaire");

    if(param != null){
      Integer proprietaire  = Integer.valueOf(param);
      switch(proprietaire){
      case JOB:
        formulaireParent = (FormulaireHashMap)request.getSession().getAttribute("jobForm");
        break;
      case TRIGGER:
        formulaireParent = (FormulaireHashMap)request.getSession().getAttribute("declencheurForm");
        break;
      default:
        direction = "erreur";
        break;
      }
    }

    if(formulaireParent != null){
      formulaire.setProprieteDynamiqueForm(formulaireParent.getProprietesDynamiques());
      direction = "afficher";
    }
    formulaire.setFormulaireDeReference(formulaireParent);
    return mapping.findForward(direction);
  }

  public ActionForward ajouterProprieteDynamique(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    ListeProprieteDynamiqueForm formulaire = (ListeProprieteDynamiqueForm)form;
    formulaire.getProprieteDynamiqueForm().add(new ProprieteDynamiqueForm(null,null));
    return mapping.findForward("page_ajax");
  }

  public ActionForward supprimerProprieteDynamique(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    String indexParam = request.getParameter("index");
    ListeProprieteDynamiqueForm formulaire = (ListeProprieteDynamiqueForm) form;
    Integer index = Integer.valueOf(indexParam);

    if(index != null
        && formulaire.getProprieteDynamiqueForm().size() > index){
      formulaire.getProprieteDynamiqueForm().remove(index.intValue());
    }
    return mapping.findForward("page_ajax");
  }

  public ActionForward enregistrerProprieteDynamique(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    ListeProprieteDynamiqueForm formulaire = (ListeProprieteDynamiqueForm) form;
    JobDataMap map = formulaire.getMapProprieteDynamique();

    FormulaireHashMap formulaireDeReference = formulaire.getFormulaireDeReference();
    formulaireDeReference.setProprietesDynamiques(map);

    return null;
  }

  public ActionForward modifierKey(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    String indexParam = request.getParameter("index");
    String key = request.getParameter("key");

    int index = Integer.valueOf(indexParam).intValue();
    ListeProprieteDynamiqueForm formulaire = (ListeProprieteDynamiqueForm) form;
    formulaire.getProprieteDynamiqueForm().get(index).setKey(key);

    return null;
  }

  public ActionForward modifierValue(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    String indexParam = request.getParameter("index");
    String value = request.getParameter("value");

    int index = Integer.valueOf(indexParam).intValue();
    ListeProprieteDynamiqueForm formulaire = (ListeProprieteDynamiqueForm) form;
    formulaire.getProprieteDynamiqueForm().get(index).setValue(value);

    return null;
  }

}
