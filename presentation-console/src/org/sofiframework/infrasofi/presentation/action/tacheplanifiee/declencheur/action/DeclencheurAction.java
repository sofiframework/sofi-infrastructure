/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.declencheur.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.jdbcjobstore.Constants;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.QuartzCronTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzJob;
import org.sofiframework.infrasofi.modele.entite.QuartzSimpleTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzTypeTrigger;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.declencheur.formulaire.DeclencheurForm;
import org.sofiframework.infrasofi.presentation.action.tacheplanifiee.job.formulaire.JobForm;
import org.sofiframework.modele.exception.ModeleException;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class DeclencheurAction extends InfrastructureAction {

  public ActionForward afficher(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {

    return mapping.findForward("page");
  }

  public ActionForward creer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    String direction = "erreur";
    JobForm jobForm = (JobForm) request.getSession().getAttribute("jobForm");
    QuartzJob jobParent = (QuartzJob) jobForm.getObjetTransfert();
    DeclencheurForm formulaire = (DeclencheurForm) form;

    if (jobForm != null && isApplicationPermise(request, jobForm.getCodeApplication())) {
      QuartzTrigger trigger = getNouveauQuartzTrigger(jobParent.getId(), Constants.TTYPE_SIMPLE);
      formulaire.initialiserFormulaire(request, mapping, true);
      formulaire.populerFormulaire(trigger, request);
      formulaire.setProprietesDynamiques(new JobDataMap());
      formulaire.setNouveauFormulaire(true);
      formulaire.setModeLectureSeulement(Boolean.FALSE);
      formulaire.ajouterMessageInformatif("infra_sofi.information.gestion_declencheur.confirmation_ajout.declencheur",
          request);
      direction = "afficher";
    }

    return mapping.findForward(direction);
  }

  public ActionForward consulter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    String direction = "erreur";
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    JobForm jobForm = (JobForm) request.getSession().getAttribute("jobForm");
    DeclencheurForm formulaire = (DeclencheurForm) form;
    String declencheurId = request.getParameter("declencheurId");

    if (jobForm != null && isApplicationPermise(request, jobForm.getCodeApplication())) {
      formulaire.initialiserFormulaire(request, mapping, true);
      QuartzTrigger declencheur = modele.getServiceDeclencheur().getDeclencheur(
          Long.parseLong(declencheurId));

      formulaire.populerFormulaire(declencheur, request);
      formulaire.setProprietesDynamiques((JobDataMap) declencheur.getJobDataInMap());
      formulaire.setModeLectureSeulement(declencheur.isActif());

      direction = "afficher";
    }

    return mapping.findForward(direction);
  }

  public ActionForward fixerTypeDeclencheur(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    String direction = "";
    DeclencheurForm formulaire = (DeclencheurForm) form;
    QuartzTrigger trigger = (QuartzTrigger) formulaire.getObjetTransfert();
    String type = request.getParameter("typeDeclencheur");

    if (type != null) {
      if (type.equals(Constants.TTYPE_SIMPLE)) {
        direction = "ajax_declencheur_simple";
        trigger.setEnfant(this.getNouveauTriggerType(trigger, Constants.TTYPE_SIMPLE));
      } else if (type.equals(Constants.TTYPE_CRON)) {
        direction = "ajax_declencheur_cron";
        trigger.setEnfant(this.getNouveauTriggerType(trigger, Constants.TTYPE_CRON));
      }
    }

    if (direction != "") {
      formulaire.initialiserFormulaire(request, mapping, true);
      formulaire.populerFormulaire(trigger, request);
      formulaire.setNouveauFormulaire(true);
    }

    return mapping.findForward(direction);
  }

  public ActionForward enregistrer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {

    ModeleInfrastructure modele = getModeleInfrastructure(request);
    DeclencheurForm formulaire = (DeclencheurForm) form;
    QuartzTrigger trigger = (QuartzTrigger) formulaire.getObjetTransfert();
    trigger.setJobDatabyMap(formulaire.getProprietesDynamiques());

    try {
      if (formulaire.isNouveauFormulaire()) {
        modele.getServiceDeclencheur().ajouter(trigger);
      } else {
        QuartzTrigger triggerOriginal = (QuartzTrigger) formulaire.getObjetTransfertOriginal();
        // On réinitialise la prochaine date d'exécution pour que les nouveaux paramètres d'exécution
        // du trigger soient pris en compte
        if (this.isTriggerExecutionModifiee(trigger, triggerOriginal)) {
          trigger.setProchaineExecution(System.currentTimeMillis());
        }
        modele.getServiceDeclencheur().modifier(trigger);
      }

      this.commit(request, formulaire);

      // Populer le formulaire en traitement.
      formulaire.populerFormulaire(trigger, request);
    } catch (ModeleException e) {
      this.rollback(request);
      formulaire.ajouterMessageErreur(e, request);
    }

    return mapping.findForward("afficher");
  }

  public ActionForward supprimer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {

    ModeleInfrastructure modele = getModeleInfrastructure(request);
    DeclencheurForm formulaire = (DeclencheurForm) form;
    QuartzTrigger declencheur = (QuartzTrigger) formulaire.getObjetTransfert();
    String direction = null;

    try {
      modele.getServiceDeclencheur().supprimerDeclencheur(declencheur);
      this.commit(request, "infra_sofi.information.gestion_declencheur.confirmation_suppression",
          new Object[] { formulaire.getNom() });
      direction = "rechercher";
    } catch (ModeleException e) {
      this.rollback(request);
      formulaire.ajouterMessageErreur("infra_sofi.erreur.gestion.declencheur.supprimer.dependances", request);
      direction = "afficher";
    }

    return mapping.findForward(direction);
  }

  /**
   * Contruit un objet de type {@link QuartzTrigger}
   * 
   * @param jobId
   *          l'identifiant du job parent du trigger
   * @param typeTrigger
   * @return un objet de type {@link QuartzTrigger}
   */
  private QuartzTrigger getNouveauQuartzTrigger(Long jobId, String typeTrigger) {
    QuartzTrigger trigger = new QuartzTrigger();
    trigger.setJobId(jobId);
    trigger.setDebut(System.currentTimeMillis());
    trigger.setProchaineExecution(System.currentTimeMillis());
    trigger.setType(typeTrigger);
    trigger.setEtat(Constants.STATE_WAITING);
    trigger.setPriorite(new Long(Trigger.DEFAULT_PRIORITY));
    trigger.setEnfant(getNouveauTriggerType(trigger, typeTrigger));
    return trigger;
  }

  /**
   * Retourne un objet de super type {@link QuartzTypeTrigger}. Suivant le type passé en paramètre, l'instance créé sera
   * un {@link QuartzSimpleTrigger}, un {@link QuartzCronTrigger}
   * 
   * @param entiteTrigger
   * @param typeTrigger
   * @return
   */
  private QuartzTypeTrigger getNouveauTriggerType(QuartzTrigger entiteTrigger, String typeTrigger) {
    QuartzTypeTrigger enfant = null;

    if (typeTrigger.equals(Constants.TTYPE_SIMPLE)) {
      entiteTrigger.setType(Constants.TTYPE_SIMPLE);
      entiteTrigger.setAppeleManque(new Long(SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW));
      enfant = new QuartzSimpleTrigger();
    } else if (typeTrigger.equals(Constants.TTYPE_CRON)) {
      entiteTrigger.setType(Constants.TTYPE_CRON);
      entiteTrigger.setAppeleManque(new Long(CronTrigger.MISFIRE_INSTRUCTION_DO_NOTHING));
      enfant = new QuartzCronTrigger();
    }
    return enfant;
  }

  /**
   * Permet de savoir si les paramètres d'exécution d'un trigger ont changé
   * 
   * @param trigger
   *          un objet de type {@link QuartzTrigger}
   * @param triggerOriginal
   *          un objet de type {@link QuartzTrigger}
   * @return un objet de type {@link Boolean}
   */
  private Boolean isTriggerExecutionModifiee(QuartzTrigger trigger, QuartzTrigger triggerOriginal) {
    Boolean resultat = Boolean.FALSE;

    if (Constants.TTYPE_SIMPLE.equals(trigger.getType())) {
      Long interval = ((QuartzSimpleTrigger) trigger.getEnfant()).getInterval();
      Long intervalOriginal = ((QuartzSimpleTrigger) triggerOriginal.getEnfant()).getInterval();
      Long nombreExecution = ((QuartzSimpleTrigger) trigger.getEnfant()).getNombreExecution();
      Long nombreExecutionOriginal = ((QuartzSimpleTrigger) triggerOriginal.getEnfant()).getNombreExecution();
      resultat = !interval.equals(intervalOriginal) || !nombreExecution.equals(nombreExecutionOriginal);
    } else if (Constants.TTYPE_CRON.equals(trigger.getType())) {
      String cronExpression = ((QuartzCronTrigger) trigger.getEnfant()).getExpression();
      String cronExpressionOrignal = ((QuartzCronTrigger) triggerOriginal.getEnfant()).getExpression();
      resultat = !cronExpression.equals(cronExpressionOrignal);
    }
    return resultat;
  }
}