/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.ordonnanceur.formulaire;

import org.sofiframework.infrasofi.presentation.action.FormulaireTrace;


public class OrdonnanceurForm  extends FormulaireTrace{

  private static final long serialVersionUID = 5540226286724281959L;

  private Long schedulerId;
  private String codeApplication;
  private String codeScheduler;
  private String nom,description,statut;
  private String hebergePar;

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeScheduler() {
    return codeScheduler;
  }

  public void setCodeScheduler(String codeScheduler) {
    this.codeScheduler = codeScheduler;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStatut() {
    return statut;
  }

  public void setStatut(String statut) {
    this.statut = statut;
  }

  public String getHebergePar() {
    return hebergePar;
  }

  public void setHebergePar(String hebergePar) {
    this.hebergePar = hebergePar;
  }

  public Long getSchedulerId() {
    return schedulerId;
  }

  public void setSchedulerId(Long schedulerId) {
    this.schedulerId = schedulerId;
  }

}
