/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.ordonnanceur.formulaire;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseForm;


/**
 * @author rmercier
 *
 */
public class TableauDeBordForm extends BaseForm {

  /**
   * Délai de rafraichissement de la page par défaut (en secondes)
   */
  private static final String DELAI_PAR_DEFAUT = "60";
  private String delaiRafraissementPage = DELAI_PAR_DEFAUT;
  private List<String> listeJobAffiche = new ArrayList<String>();

  /**
   * 
   */
  private static final long serialVersionUID = 5820541897833665956L;

  /* (non-Javadoc)
   * @see com.nurun.sofi.presentation.struts.form.BaseForm#validerFormulaire(javax.servlet.http.HttpServletRequest)
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {

  }

  /**
   * @return the delaiRafraissementPage
   */
  public String getDelaiRafraissementPage() {
    return delaiRafraissementPage;
  }

  /**
   * @param delaiRafraissementPage the delaiRafraissementPage to set
   */
  public void setDelaiRafraissementPage(String delaiRafraissementPage) {
    this.delaiRafraissementPage = delaiRafraissementPage;
  }

  /**
   * @param listeJobAffiche the listeJobAffiche to set
   */
  public void setListeJobAffiche(List<String> listeJobAffiche) {
    this.listeJobAffiche = listeJobAffiche;
  }

  /**
   * @return the listeJobAffiche
   */
  public List<String> getListeJobAffiche() {
    return listeJobAffiche;
  }

}
