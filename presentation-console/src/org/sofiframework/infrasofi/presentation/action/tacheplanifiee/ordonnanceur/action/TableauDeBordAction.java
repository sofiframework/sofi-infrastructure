/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.ordonnanceur.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.quartz.JobDataMap;
import org.sofiframework.application.tacheplanifiee.job.TacheStateful.ParametreTacheStateful;
import org.sofiframework.infrasofi.modele.entite.QuartzJob;
import org.sofiframework.infrasofi.modele.entite.QuartzTrigger;
import org.sofiframework.infrasofi.modele.service.ServiceDeclencheur;
import org.sofiframework.infrasofi.modele.service.ServiceJob;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * @author rmercier
 *
 */
public class TableauDeBordAction extends InfrastructureAction {

  private enum TypeEntite {JOB, DECLENCHEUR};
  private enum TypeAction {ACTIVER, DESACTIVER};

  /**
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {
    return mapping.findForward("page");
  }

  /**
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {

    chargerListeJob(request);
    chargerListeDeclencheur(request);
    return mapping.findForward("afficher");
  }

  /**
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   */
  public ActionForward appliquer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {

    return mapping.findForward("afficher");
  }

  /**
   * Rafraichissement des listes de jobs et de déclencheurs en ajax
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   */
  public ActionForward rafraichir(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {

    chargerListeJob(request);
    chargerListeDeclencheur(request);
    return mapping.findForward("ajax_liste_job_et_declencheur");
  }

  /**
   * Activation ajax d'un déclencheur
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   */
  public ActionForward activerDeclencheur(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {
    return actionnerEntite(mapping, request, TypeEntite.DECLENCHEUR, TypeAction.ACTIVER);
  }

  /**
   * Désactivation ajax d'un déclencheur
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   */
  public ActionForward desactiverDeclencheur(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {
    return actionnerEntite(mapping, request, TypeEntite.DECLENCHEUR, TypeAction.DESACTIVER);
  }

  /**
   * Activation ajax d'un job
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   */
  public ActionForward activerJob(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {
    return actionnerEntite(mapping, request, TypeEntite.JOB, TypeAction.ACTIVER);
  }

  /**
   * Désactivation ajax d'un job
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   */
  public ActionForward desactiverJob(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {
    return actionnerEntite(mapping, request, TypeEntite.JOB, TypeAction.DESACTIVER);
  }

  /**
   * Permet d'activer ou de désactiver un job ou un déclencheur
   * @param mapping
   * @param request
   * @param entite
   * @param action
   * @return
   */
  private ActionForward actionnerEntite(ActionMapping mapping, HttpServletRequest request, TypeEntite entite, TypeAction action) {
    String id = request.getParameter("id");

    if(!UtilitaireString.isVide(id)) {
      if(TypeEntite.DECLENCHEUR.equals(entite)) {
        ServiceDeclencheur service = this.getModeleInfrastructure(request).getServiceDeclencheur();
        if(TypeAction.ACTIVER.equals(action)) {
          service.activerDeclencheur(Long.parseLong(id));
        } else {
          service.desactiverDeclencheur(Long.parseLong(id));
        }
      }

      if(TypeEntite.JOB.equals(entite)) {
        ServiceJob service = this.getModeleInfrastructure(request).getServiceJob();
        if(TypeAction.ACTIVER.equals(action)) {
          service.activerJob(Long.parseLong(id));
        } else {
          service.desactiverJob(Long.parseLong(id));
        }
      }
      chargerListeJob(request);
      chargerListeDeclencheur(request);
    }

    return mapping.findForward("ajax_liste_job_et_declencheur");
  }

  /**
   * Charge la liste des jobs (objets de type {@link QuartzJob}) de l'ordonnanceur en session
   * @param request
   */
  private void chargerListeJob(HttpServletRequest request) {
    String schedulerId = request.getParameter("schedulerId");
    if(!UtilitaireString.isVide(schedulerId)) {
      List<QuartzJob> listeJob = this.getModeleInfrastructure(request)
          .getServiceJob().getListeJob(Long.parseLong(schedulerId));
      this.setAttributTemporairePourAction("listeJobTableauDeBord", listeJob, request);
    }
  }

  /**
   * Charge la liste des déclencheurs (objets de type {@link QuartzTrigger}) de l'ordonnanceur en session
   * @param request
   */
  private void chargerListeDeclencheur(HttpServletRequest request) {
    String schedulerId = request.getParameter("schedulerId");
    if(!UtilitaireString.isVide(schedulerId)) {
      List<QuartzTrigger> listeDeclencheur = this.getModeleInfrastructure(request)
          .getServiceDeclencheur().getListeDeclencheur(Long.parseLong(schedulerId));
      this.setAttributTemporairePourAction("listeDeclencheurTableauDeBord", listeDeclencheur, request);
    }
  }

  @SuppressWarnings("unchecked")
  private Object getEntiteDansListeSession(HttpServletRequest request, String nomListe, int index) {
    List liste = (List) request.getSession().getAttribute(nomListe);
    return liste.get(index);
  }

  public ActionForward obtenirTempsExecution(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) {
    String jobId = request.getParameter("jobId");
    if(!UtilitaireString.isVide(jobId)) {
      QuartzJob job = (QuartzJob) this.getModeleInfrastructure(request)
          .getServiceJob().get(Long.parseLong(jobId));

      JobDataMap map = (JobDataMap) job.getJobDataInMap();
      List listeTempExecution = (List) map.get(ParametreTacheStateful.LISTE_DUREE_EXECUTION.getCle());
      if(listeTempExecution == null) {
        listeTempExecution = new ArrayList();
      }

      request.setAttribute("listeTempExecution", listeTempExecution);
    }
    return mapping.findForward("ajax_liste_temps_execution");
  }

}
