/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.tacheplanifiee.ordonnanceur.formulaire;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.presentation.action.RechercheApplicativeForm;


public class RechercheOrdonnanceurForm extends RechercheApplicativeForm{

  private static final long serialVersionUID = 5939329285329177034L;

  private String codeApplication,codeScheduler,nom;
  private String statut;

  @Override
  public void validerFormulaire(HttpServletRequest request) {
    // TODO Auto-generated method stub
  }

  @Override
  public String getCodeApplication() {
    return codeApplication;
  }

  @Override
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeScheduler() {
    return codeScheduler;
  }

  public void setCodeScheduler(String codeScheduler) {
    this.codeScheduler = codeScheduler;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getStatut() {
    return statut;
  }

  public void setStatut(String statut) {
    this.statut = statut;
  }

}
