/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action;

import java.io.IOException;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.ObjetFiltre;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;

public abstract class BaseRechercheAction extends InfrastructureAction {

  private static final Log log = LogFactory.getLog(BaseRechercheAction.class);

  /**
   * Méthode utilisée pour l'accÃ¨s initial Ã  la recherche.
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant Ã  l'action (facultatif).
   * @param request requÃªte HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donnée Ã  l'utilisateur.
   * @throws java.io.IOException Exception entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    BaseForm formulaire = (BaseForm) form;
    formulaire.initialiserFormulaire(request, mapping,
        this.construireNouveauFiltre(), true);

    HashSet listeActionRechercheLance = (HashSet) request.getSession()
        .getAttribute("SOFI_ListeActionRecherche");

    if (listeActionRechercheLance != null) {
      listeActionRechercheLance.remove(getNomListeDansSession());
    }

    this.traiterRechercheApplicative(formulaire, request);

    return mapping.findForward("afficher");
  }

  private void traiterRechercheApplicative(BaseForm form, HttpServletRequest request) {
    if (form instanceof RechercheApplicativeForm) {
      ((RechercheApplicativeForm) form).configurerCodeApplicationSelectionne(request);
    }
  }

  /**
   * Méthode utilisée pour effectuer la recherche.
   * @param mapping actionMapping utilisée pour trouver cette action.
   * @param form formulaire correspondant Ã  l'action (facultatif).
   * @param request requÃªte HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné Ã  l'utilisateur.
   * @throws java.io.IOException Exception entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward rechercher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    // Récupérer le filtre de recherche et la liste de navigation
    BaseForm formulaire = (BaseForm) form;
    this.traiterRechercheApplicative(formulaire, request);

    ObjetFiltre filtre = (ObjetFiltre) formulaire.getObjetTransfert();
    if (filtre == null) {
      this.acceder(mapping, form, request, response);
      filtre = (ObjetFiltre) formulaire.getObjetTransfert();
    }

    ListeNavigation liste = this.appliquerListeNavigation(request);
    liste.setObjetFiltre(filtre);
    try {
      this.effectuerRecherche(liste, formulaire, getModeleInfrastructure(request), request, response);
      this.setAttributTemporaireListeNavigationPourAction(getNomListeDansSession(), liste, request);
    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur lors de la recherche " + getNomListeDansSession(), e);
      }
      formulaire.ajouterMessageErreur(e, request);
    }
    return mapping.findForward(this.isAjax(request) ? "ajax" : "afficher");
  }

  public ActionForward afficher(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    if (this.isTraiterResultatRecherche(getNomListeDansSession(), request)) {
      this.rechercher(mapping, form, request, response);
    }
    return mapping.findForward("page");
  }

  public abstract String getNomListeDansSession();

  public abstract ObjetTransfert construireNouveauFiltre();

  public abstract void effectuerRecherche(ListeNavigation liste, BaseForm formulaire, ModeleInfrastructure modele,
      HttpServletRequest request, HttpServletResponse response) throws ModeleException;
}
