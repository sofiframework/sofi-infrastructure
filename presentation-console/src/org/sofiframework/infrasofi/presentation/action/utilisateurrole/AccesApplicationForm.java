package org.sofiframework.infrasofi.presentation.action.utilisateurrole;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * Formulaire permettant d'afficher les données de base d'un utilisateur ainsi
 * que la liste de tous les rôles qui lui sont assignés
 * <p>
 */
public class AccesApplicationForm extends BaseForm {

  private static final long serialVersionUID = 4153510264326501474L;

  /** Le code utilisateur */
  private String codeUtilisateur;

  /**
   * Le type d'utilisateur qui accède à l'application.
   */
  private String codeTypeUtilisateur;

  /**
   * Le statut de l'utilisateur qui accède à l'application.
   */
  private String codeStatut;

  /**
   * Le code d'organisation associé à l'utilisateur qui accède à l'application.
   */
  private String codeOrganisation;

  /**
   * Le type d'organisation associé à l'utilisateur qui accède à l'application.
   */
  private String typeOrganisation;

  /** Constructeur par défaut */
  public AccesApplicationForm() {
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un
   * <code>submit</code>.
   * <p>
   * 
   * @param request
   *          la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  /**
   * Obtenir l'identifiant unique de l'utilisateur auquel sont associés des
   * rôles.
   * <p>
   * 
   * @return l'identifiant unique de l'utilisateur auquel sont associés des
   *         rôles
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * Fixer l'identifiant unique de l'utilisateur auquel sont associés des rôles.
   * <p>
   * 
   * @param codeUtilisateur
   *          l'identifiant unique de l'utilisateur auquel sont associés des
   *          rôles
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * Fixer le type d'utilisateur qui accède à l'application.
   * 
   * @param typeUtilisateur
   *          le type d'utilisateur qui accède à l'application.
   */
  public void setCodeTypeUtilisateur(String codeTypeUtilisateur) {
    this.codeTypeUtilisateur = codeTypeUtilisateur;
  }

  /**
   * Retourne le type d'utilisateur qui accède à l'application.
   * 
   * @return le type d'utilisateur qui accède à l'application.
   */
  public String getCodeTypeUtilisateur() {
    return codeTypeUtilisateur;
  }

  /**
   * Fixer le statut de l'utilisateur qui accède à l'application.
   * 
   * @param statut
   */
  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  /**
   * Retourne le statut de l'utilisateur qui accède à l'application.
   * 
   * @return le statut de l'utilisateur qui accède à l'application.
   */
  public String getCodeStatut() {
    return codeStatut;
  }

  /**
   * Fixer le code d'organisation associé à l'utilisateur qui accède à
   * l'application.
   * 
   * @param codeOrganisation
   */
  public void setCodeOrganisation(String codeOrganisation) {
    this.codeOrganisation = codeOrganisation;
  }

  /**
   * Retourne le code de l'organisation qui est strictement lié à l'utilisateur
   * pour l'application.
   * 
   * @return le code de l'organisation qui est strictement lié à l'utilisateur
   *         pour l'application.
   */
  public String getCodeOrganisation() {
    return codeOrganisation;
  }

  /**
   * Fixer le type de l'organisation qui est strictement lié à l'utilisateur
   * pour l'application.
   * 
   * @param typeOrganisation
   *          le code de l'organisation qui est strictement lié à l'utilisateur
   *          pour l'application.
   */
  public void setTypeOrganisation(String typeOrganisation) {
    this.typeOrganisation = typeOrganisation;
  }

  /**
   * Retourne le type de l'organisation qui est strictement lié à l'utilisateur
   * pour l'application.
   * 
   * @return le type de l'organisation qui est strictement lié à l'utilisateur
   *         pour l'application.
   */
  public String getTypeOrganisation() {
    return typeOrganisation;
  }
}
