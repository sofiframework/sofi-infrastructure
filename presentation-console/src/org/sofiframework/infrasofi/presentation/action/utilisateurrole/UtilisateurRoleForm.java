/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.utilisateurrole;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseDynaForm;

/**
 * Objet d'affaire faisant le lien entre un utilisateur ainsi que les rôles des
 * applications qui s'offrent à lui.
 * <p>
 */
public class UtilisateurRoleForm extends BaseDynaForm {

  private static final long serialVersionUID = 3314186769867816960L;


  /** Constructeur par défaut */
  public UtilisateurRoleForm() {
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un
   * <code>submit</code>.
   * <p>
   * 
   * @param request
   *          la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }
}