/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.utilisateurrole;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.presentation.action.RechercheApplicativeForm;


/**
 * Formulaire permettant la recherche d'un utilisateur par rapport aux rôles qui lui
 * sont assignés.
 * <p>
 */
public class RechercheUtilisateurRoleForm extends RechercheApplicativeForm {

  private static final long serialVersionUID = 3306799505939935487L;

  /** le nom de famille de l'utilisateur à rechercher */
  private String nom;

  /** le prénom de l'utilisateur à rechercher */
  private String prenom;

  /** l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel */
  private String codeRole;

  /** Spécifie si on désire les objet qui n'inclus le role spécifié **/
  private boolean codeRoleNonInclus;

  /** Constructeur par défaut */
  public RechercheUtilisateurRoleForm() {
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un <code>submit</code>.
   * <p>
   * @param request la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  /**
   * Obtenir le nom de famille de l'utilisateur à rechercher.
   * <p>
   * @return le nom de famille de l'utilisateur à rechercher
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de famille de l'utilisateur à rechercher.
   * <p>
   * @param nom le nom de famille de l'utilisateur à rechercher
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le prénom de l'utilisateur à rechercher.
   * <p>
   * @return le prénom de l'utilisateur à rechercher
   */
  public String getPrenom() {
    return prenom;
  }

  /**
   * Fixer le prénom de l'utilisateur à rechercher.
   * <p>
   * @param prenom le prénom de l'utilisateur à rechercher
   */
  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  /**
   * Obtenir l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel.
   * <p>
   * @return l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel
   */
  public String getCodeRole() {
    return codeRole;
  }

  /**
   * Fixer l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel.
   * <p>
   * @param codeRole l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel
   */
  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  /**
   * Obtenir spécifie si on désire les objet qui n'inclus le role spécifié.
   * <p>
   * @return codeRoleNonInclus spécifie si on désire les objet qui n'inclus le role spécifié
   */
  public void setCodeRoleNonInclus(boolean codeRoleNonInclus) {
    this.codeRoleNonInclus = codeRoleNonInclus;
  }

  /**
   * Fixer spécifie si on désire les objet qui n'inclus le role spécifié.
   * <p>
   * @param spécifie si on désire les objet qui n'inclus le role spécifié
   */
  public boolean isCodeRoleNonInclus() {
    return codeRoleNonInclus;
  }
}