/*

 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.utilisateurrole;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Utilisateur;
import org.sofiframework.infrasofi.modele.entite.UtilisateurRole;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.infrasofi.presentation.action.utilisateur.UtilisateurForm;

/**
 * Classe d'action qui gère les accès à l'unité de traitement qui fait la
 * gestion entre les utilisateurs et les rôles qui leur sont assignés.
 * <p>
 */
public class DetailUtilisateurRoleAction extends InfrastructureAction {

  /**
   * Méthode qui s'exécute quand l'utilisateur modifie l'application dans la
   * page de détail des rôles pour un utilisateur.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward modifierApplication(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    DetailUtilisateurForm formulaire = (DetailUtilisateurForm) form;
    String codeApplication = request.getParameter("codeApplication");
    formulaire.set("codeApplication", codeApplication);
    this.fixerApplication(mapping, form, request, response);
    return mapping.findForward("afficher");
  }

  private void chargerCodeClientPilote(HttpServletRequest request) {
    org.sofiframework.application.securite.objetstransfert.Utilisateur pilote =
        this
        .getUtilisateur(request);

    /*
     * On devrait avoir seulement un code client pour un role
     */
    List<String> listeCodeClient = pilote
        .getListeClient(GestionSecurite.getInstance()
            .getCodeApplication());
    String codeClient = null;
    /*
     * Si le code est null alors on est un pilote qui peut saisir le code client
     */
    if (listeCodeClient != null && !listeCodeClient.isEmpty()) {
      /*
       * C'est un client cloisonné. Les autres utilisateurs vont hériter du même
       * code client.
       */
      codeClient = listeCodeClient.get(0);
    }

    this.setAttributTemporairePourService("codeClientPilote", codeClient, request);
  }

  @SuppressWarnings("unchecked")
  private void chargerRoles(String codeUtilisateur, String codeApplication,
      HttpServletRequest request) {
    // Rechercher les rôles disponible pour l'utilisateur.
    List listeRoleDisponible = getModeleInfrastructure(request)
        .getServiceRole().getListeRoleDisponible(codeUtilisateur,
            codeApplication);
    setAttributTemporairePourAction(
        "GESTION_UTILISATEUR_LISTE_ROLE_DISPONIBLE", listeRoleDisponible,
        request);

    // Rechercher la liste des rôles autorisés.
    List listeUtilisateurRole = getModeleInfrastructure(request)
        .getServiceUtilisateurRole().getListeUtilisateurRole(codeUtilisateur,
            codeApplication);
    setAttributTemporairePourAction("GESTION_UTILISATEUR_LISTE_ROLE_AUTORISE",
        listeUtilisateurRole, request);
  }

  /**
   * Méthode qui sert à afficher la page de détail.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    // Initialiser le formulaire
    DetailUtilisateurForm formulaire = (DetailUtilisateurForm) form;
    formulaire.initialiserFormulaire(request, mapping, true);

    // Extraire le formulaire de l'utilisateur
    UtilisateurForm utilisateurForm = (UtilisateurForm) request.getSession()
        .getAttribute("utilisateurForm");

    String codeApplicationSelectionne = (String) request.getSession()
        .getAttribute("systeme_selectionne");
    String test = this.getApplicationSelectionne(request);

    formulaire.set("codeApplication", codeApplicationSelectionne);

    String codeUtilisateur = utilisateurForm.getCodeUtilisateur();

    this.chargerUtilisateur(formulaire, request, codeUtilisateur,
        codeApplicationSelectionne);

    this.chargerCodeClientPilote(request);

    // Appel de la vue qui permet d'afficher la page de recherche.
    return mapping.findForward("page");
  }

  private void chargerUtilisateur(DetailUtilisateurForm formulaire,
      HttpServletRequest request, String codeUtilisateur,
      String codeApplicationSelectionne) {
    ModeleInfrastructure modele = this.getModeleInfrastructure(request);
    Utilisateur utilisateur = modele.getServiceUtilisateur().getUtilisateur(
        codeUtilisateur, codeApplicationSelectionne);
    formulaire.set("nom", utilisateur.getNom());
    formulaire.set("prenom", utilisateur.getPrenom());
    this.chargerRoles(codeUtilisateur, codeApplicationSelectionne, request);
    formulaire.populerFormulaire(utilisateur, request);
  }

  public ActionForward afficherPeriodeActivation(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    // Initialiser le formulaire
    ModeleInfrastructure modele = (ModeleInfrastructure) this
        .getModele(request);

    // Accès au formulaire.
    UtilisateurRoleForm formulaire = (UtilisateurRoleForm) form;

    // Extraire les paramètres.
    String id = formulaire.getString("id");

    // Extraire le détail du rôle autorisé.
    UtilisateurRole utilisateurRole = modele.getServiceUtilisateurRole()
        .getUtilisateurRole(Long.parseLong(id));

    formulaire.populerFormulaire(utilisateurRole, request);

    // Appel de la vue qui permet d'afficher la page de recherche.
    return mapping.findForward("page_periode_activation");
  }

  public ActionForward enregistrerPeriodeActivation(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    // Initialiser le formulaire
    ModeleInfrastructure modele = (ModeleInfrastructure) this
        .getModele(request);

    // Accès au formulaire.
    UtilisateurRoleForm formulaire = (UtilisateurRoleForm) form;

    // Extraire le détail du rôle autorisé.
    UtilisateurRole utilisateurRole = (UtilisateurRole) formulaire
        .getObjetTransfert();

    modele.getServiceUtilisateurRole().modifier(utilisateurRole);
    commit(request, formulaire);

    // Appel de la vue qui permet d'afficher la page de recherche.
    return mapping.findForward("afficher");
  }

  public ActionForward afficherRoleAutorises(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    // Appel de la vue qui permet d'afficher la page de recherche.
    return mapping.findForward("ajax_liste_role_autorise");
  }

  public ActionForward ajouter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    // Initialiser le formulaire
    ModeleInfrastructure modele = (ModeleInfrastructure) this
        .getModele(request);

    // Accès au formulaire.
    DetailUtilisateurForm formulaire = (DetailUtilisateurForm) form;

    // Extraire les paramètres.
    String codeRole = request.getParameter("codeRole");
    String codeUtilisateur = request.getParameter("codeUtilisateur");
    String codeApplication = request.getParameter("codeApplication");

    UtilisateurRole ur = new UtilisateurRole();
    ur.setCodeRole(codeRole);
    ur.setCodeUtilisateur(codeUtilisateur);
    ur.setCodeApplication(codeApplication);

    String codeClientPilote = (String) request.getSession().getAttribute("codeClientPilote");

    if (codeClientPilote != null) {
      ur.setCodeClient(codeClientPilote);
    }

    modele.getServiceUtilisateurRole().ajouter(ur);
    commit(request, formulaire,
        "Le rôle '{0}' a été ajouté dans les rôles autorisés avec succès.",
        new Object[] { codeRole });

    // Appel de la vue qui permet d'afficher la page de recherche.
    return mapping.findForward("afficher");
  }

  public ActionForward enregistrer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    DetailUtilisateurForm formulaire = (DetailUtilisateurForm) form;
    Utilisateur utilisateur = (Utilisateur) formulaire.getObjetTransfert();
    this.getModeleInfrastructure(request).getServiceUtilisateur()
    .enregistrerUtilisateur(utilisateur);
    this.commit(request, formulaire);
    return mapping.findForward("afficher");
  }

  public ActionForward supprimer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    ModeleInfrastructure modele = (ModeleInfrastructure) this
        .getModele(request);
    DetailUtilisateurForm formulaire = (DetailUtilisateurForm) form;

    Long id = Long.parseLong(request.getParameter("id"));
    String codeRole = request.getParameter("codeRole");
    modele.getServiceUtilisateurRole().supprimer(id);

    this.commit(request, formulaire,
        "Le rôle '{0}' a été supprimé des rôle autorisés avec succès.",
        new Object[] { codeRole });

    // Appel de la vue qui permet d'afficher la page de recherche.
    return mapping.findForward("afficher");
  }
}
