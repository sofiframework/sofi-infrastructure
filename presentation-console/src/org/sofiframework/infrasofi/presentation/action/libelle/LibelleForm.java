/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.libelle;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Libelle;
import org.sofiframework.infrasofi.presentation.action.FormulaireTrace;
import org.sofiframework.modele.exception.ModeleException;

/**
 * Formulaire de détail d'un libellé.
 * 
 * @author Jean-Maxime Pelletier
 */
public class LibelleForm extends FormulaireTrace {

  private static final long serialVersionUID = -4306654292595189708L;

  private String id;

  private String cleLibelle;

  private String codeLangue;

  private String codeApplication;

  private String texteLibelle;

  private String aideContextuelle;

  private String aideContextuelleHtml;

  private String aideContextuelleExterne;

  private String reference;

  private Boolean libelleHtml;

  private String placeholder;

  private String codeClient;

  public LibelleForm() {
    super();
  }

  @Override
  public void validerFormulaire(HttpServletRequest request) {
    try {
      ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);

      if (isNouveauFormulaire() || this.isCleObjetTransfertModifie()) {
        Libelle libelle = null;
        if (StringUtils.isEmpty(getCodeClient())) {
          libelle = modele.getServiceLibelle().get(getCleLibelle(), getCodeLangue());
        } else {
          libelle = modele.getServiceLibelle().get(getCleLibelle(), getCodeLangue(), getCodeClient());
        }
        if (libelle != null) {
          this.ajouterMessageErreur("infra_sofi.erreur.gestion_libelle.cleLibelle.unique", "cleLibelle", request);
        }
      }

      if (getCleLibelle() != null && getCleLibelle().indexOf(getCodeApplication().toLowerCase()) == -1) {
        ajouterMessageErreur("infra_sofi.erreur.gestion_libelle.cleLibelle.code_systeme_inexistant", request);
      }
    } catch (ModeleException e) {
      log.error("Erreur lors de la validation du Libellé", e);
      ajouterMessageErreurGeneral(e.getMessage(), request);
    }
  }

  public void setCleLibelle(String cleLibelle) {
    this.cleLibelle = cleLibelle;
  }

  public String getCleLibelle() {
    return cleLibelle;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  public String getAideContextuelle() {
    return aideContextuelle;
  }

  public void setTexteLibelle(String texteLibelle) {
    this.texteLibelle = texteLibelle;
  }

  public String getTexteLibelle() {
    return texteLibelle;
  }

  /**
   * Fixer l'aide contextuelle
   * 
   * @param aideContextuelleHtml
   *          l'aide contextuelle
   */
  public void setAideContextuelleHtml(String aideContextuelleHtml) {
    this.aideContextuelleHtml = aideContextuelleHtml;
  }

  /**
   * Retourne l'aide contextuelle.
   * 
   * @return l'aide contextuelle Html
   */
  public String getAideContextuelleHtml() {
    return aideContextuelleHtml;
  }

  public void setAideContextuelleExterne(String aideContextuelleExterne) {
    this.aideContextuelleExterne = aideContextuelleExterne;
  }

  public String getAideContextuelleExterne() {
    return aideContextuelleExterne;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public String getReference() {
    return reference;
  }

  public void setLibelleHtml(Boolean libelleHtml) {
    this.libelleHtml = libelleHtml;
  }

  public Boolean getLibelleHtml() {
    return libelleHtml;
  }

  public void setPlaceholder(String placeholder) {
    this.placeholder = placeholder;
  }

  public String getPlaceholder() {
    return placeholder;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

}
