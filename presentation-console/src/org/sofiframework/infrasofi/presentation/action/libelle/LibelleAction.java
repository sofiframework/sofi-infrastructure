/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.libelle;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.cache.UtilitaireCache;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.entite.Libelle;
import org.sofiframework.infrasofi.modele.service.ServiceLibelle;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.utilitaire.UtilitaireHttp;

/**
 * Action de la page de détail d'un libellé.
 * 
 * @author Jean-Maxime Pelletier
 */
public class LibelleAction extends InfrastructureAction {

  private static final Log log = LogFactory.getLog(LibelleAction.class);

  /**
   * Réaffiche la page en spécifiant les styles en fonction de la langue sélectionnée par l'utilisateur.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    LibelleForm formulaire = (LibelleForm) form;

    if (formulaire.getCodeLangue() == null) {
      formulaire.setCodeLangue("fr_CA");
    }

    return mapping.findForward("page");
  }

  /**
   * Méthode utilisée pour visualiser le détail d'un libellé.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    String direction = null;
    LibelleForm formulaire = (LibelleForm) form;

    try {
      Libelle libelle = getLibelle(formulaire, request);
      formulaire.initialiserFormulaire(request, mapping, true);
      formulaire.populerFormulaire(libelle, request, true);

      if (libelle == null) {
        // Traiter l'inexistance du libellé
        this.rollback(request, "infra_sofi.erreur.commun.element.non.trouve", null);
        direction = "recherche";
      } else {
        // Appel de la page du détail
        direction = "afficher";
      }
    } catch (ModeleException e) {
      // Traiter le problème venant du modèle.
      formulaire.ajouterMessageErreur(e, request);
      direction = "recherche";
    }

    return mapping.findForward(direction);
  }
  

  /**
   * Permet d'obtenir un libelle via les 2 cas possibles :
   * <ul>
   * <li>depuis la recherche de libellé</li>
   * <li>depuis le détail d'un objet java</li>
   * </ul>
   * 
   * @param formulaire
   * @param request
   * @return
   */
  private Libelle getLibelle(LibelleForm formulaire, HttpServletRequest request) {
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    String id = formulaire.getId();
    if (id != null) {
      return (Libelle) modele.getServiceLibelle().get(new Long(id));
    } else {
      return modele.getServiceLibelle().get(formulaire.getCleLibelle(), formulaire.getCodeLangue(),
          formulaire.getCodeClient());
    }
  }

  /**
   * Affiche le détail d'un libellé pour la consultation ou la modification.
   */
  public ActionForward changerLangue(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    LibelleForm formulaire = (LibelleForm) form;
    String cle = formulaire.getCleLibelle();
    String langue = formulaire.getCodeLangue();
    String codeClient = formulaire.getCodeClient();
    formulaire.initialiserFormulaire(request, mapping, true);
    try {
      Libelle libelle = modele.getServiceLibelle().get(cle, langue, codeClient);
      formulaire.populerFormulaire(libelle == null ? new Libelle(cle, langue, codeClient) : libelle, request, true);
      this.genererReponseAjax(formulaire, libelle, request, response);
    } catch (ModeleException e) {
      // Traiter le problème venant du modèle.
      formulaire.ajouterMessageErreur(e, request);
    }

    return null;
  }

  private void genererReponseAjax(LibelleForm formulaire, Libelle libelle, HttpServletRequest request,
      HttpServletResponse response) {
    if (libelle == null) {
      formulaire.setAideContextuelle("");
      formulaire.setAideContextuelleHtml("");
      formulaire.setTexteLibelle("");
      formulaire.setPlaceholder("");
      formulaire.setCreePar("");
      formulaire.setDateCreation("");
      formulaire.setModifiePar("");
      formulaire.setDateModification("");
      formulaire.setNouveauFormulaire(true);
    }

    // Ajouter une valeur dans la réponse pour un identifiant.
    ajouterValeurDansReponse("texteLibelle", formulaire.getTexteLibelle(), request, response);
    ajouterValeurDansReponse("placeholder", formulaire.getPlaceholder(), request, response);
    ajouterValeurDansReponse("aideContextuelle", formulaire.getAideContextuelle(), request, response);
    ajouterValeurDansReponse("aideContextuelleHtml", formulaire.getAideContextuelleHtml(), request, response);
    ajouterValeurDansReponse("creePar", formulaire.getCreePar(), request, response);
    ajouterValeurDansReponse("dateCreation", formulaire.getDateCreation(), request, response);
    ajouterValeurDansReponse("modifiePar", formulaire.getModifiePar(), request, response);
    ajouterValeurDansReponse("dateModification", formulaire.getDateModification(), request, response);
    genererReponseXMLAvecValeurs(request, response);
  }

  /**
   * Méthode utilisée pour enregistrer le détail d'un libellé.
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    ServiceLibelle serviceLibelle = modele.getServiceLibelle();
    LibelleForm formulaire = (LibelleForm) form;
    Libelle libelle = (Libelle) formulaire.getObjetTransfert();
    libelle.setCodeLangue(formulaire.getCodeLangue());
    try {
      String ancienneCleLibelle = null;
      /**
       * On doit spécifier une ancienne clé primaire si celle-ci est modifiée par l'utilisateur
       */
      if (!formulaire.isNouveauFormulaire()) {
        ancienneCleLibelle = formulaire.isCleObjetTransfertModifie() ? ((Libelle) formulaire
            .getObjetTransfertOriginal()).getCleLibelle() : null;
      }

      String texteLibelle = libelle.getTexteLibelle();

      serviceLibelle.enregistrer(libelle, ancienneCleLibelle);
      this.commit(request, formulaire);

      libelle.setTexteLibelle(texteLibelle);
      libelle.setTexteLibelle2(null);
      libelle.setTexteLibelle3(null);

      // Populer le formulaire après enregistrement.
      formulaire.populerFormulaire(libelle, request);
    } catch (ModeleException e) {
      this.rollback(request, e.getMessage(), null);
      if (log.isErrorEnabled()) {
        log.error("Erreur lors de l'enregistrement d'un libellé: " + libelle, e);
      }
      formulaire.ajouterMessageErreur(e, request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Méthode utilisée pour ajouter un nouveau libellé.
   */
  public ActionForward ajouter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    LibelleForm formulaire = (LibelleForm) form;
    formulaire.initialiserFormulaire(request, mapping, new Libelle(), true);
    formulaire.ajouterMessageInformatif("infra_sofi.information.gestion_libelle.creation", request);
    String codeApplicationSelectionne = (String) request.getSession().getAttribute("systeme_selectionne");
    formulaire.setCodeApplication(codeApplicationSelectionne);
    return mapping.findForward("afficher");
  }

  /**
   * Méthode utilisée pour supprimer un libellé.
   */
  public ActionForward supprimer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    String direction = null;
    LibelleForm formulaire = (LibelleForm) form;
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);

    try {
      modele.getServiceLibelle().supprimerLibelle(formulaire.getCleLibelle(), formulaire.getCodeClient());
      commit(request, "infra_sofi.information.gestion_libelle.suppression.succes",
          new Object[] { formulaire.getCleLibelle() });
      direction = "recherche";
    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur lors de la suppression d'un libellé, identifiant : '" + formulaire.getCleLibelle()
            + "', code de langue : '" + formulaire.getCodeLangue() + "'", e);
      }
      this.rollback(request, e.getMessage(), null);
      direction = "afficher";
    }

    return mapping.findForward(direction);
  }

  /**
   * Exécute l'initialisation d'un libellé.
   */
  public ActionForward initialiserLibelle(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    LibelleForm formulaire = (LibelleForm) form;
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);

    Application application = modele.getServiceApplication().getApplication(formulaire.getCodeApplication());

    String[] listeNoeudCache = getUrlApplication(formulaire.getCodeApplication(), request);

    try {
      for (int i = 0; i < listeNoeudCache.length; i++) {
        StringBuffer adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[i]);
        adresseInitialisation.append(UtilitaireCache.getUrlMajLibelle(formulaire.getCodeApplication()));
        adresseInitialisation.append(formulaire.getCleLibelle());
        adresseInitialisation.append(this.ajouterContenuSecuriteUrl(application));
        
        if (!isVide(formulaire.getCodeClient())) {
          adresseInitialisation.append("?codeClient=");
          adresseInitialisation.append(formulaire.getCodeClient());
        }

        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString());

        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

      }

      formulaire.ajouterMessageInformatif("infra_sofi.information.gestion_libelle.libelle.succes",
          new String[] { formulaire.getCleLibelle() }, request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur("infra_sofi.erreur.gestion.application.communication" + e.getMessage(), request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Copier un libellé
   * 
   */
  public ActionForward copier(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
    
    LibelleForm libelleForm = (LibelleForm) form;
    Libelle libelle = new Libelle((Libelle) libelleForm.getObjetTransfert());
    libelleForm.populerFormulaire(libelle, request);
    return mapping.findForward("afficher");
  }

  
}