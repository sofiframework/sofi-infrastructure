/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.libelle;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.presentation.action.RechercheApplicativeForm;

/**
 * Formulaire de recherche des libellés.
 * 
 * @author Jean-Maxime Pelletier
 */
public class RechercheLibelleForm extends RechercheApplicativeForm {

  private static final long serialVersionUID = 1445604662754385798L;

  private String cleLibelle;
  private String codeLangue = "fr_CA";
  private String texteLibelle;
  private String aideContextuelle;
  private Boolean libelleHtml;
  private String codeClient;
  private String reference;


  public RechercheLibelleForm() {
    this.setFormulaireIdentifiant("cleLibelle");
  }

  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  public void setCleLibelle(String cleLibelle) {
    this.cleLibelle = cleLibelle == null ? null : cleLibelle.trim();
  }

  public String getCleLibelle() {
    return cleLibelle;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setTexteLibelle(String texteLibelle) {
    this.texteLibelle = texteLibelle;
  }

  public String getTexteLibelle() {
    return texteLibelle;
  }

  public void setAideContextuelle(String aideContextuelle) {
    this.aideContextuelle = aideContextuelle;
  }

  public String getAideContextuelle() {
    return aideContextuelle;
  }

  public void setLibelleHtml(Boolean libelleHtml) {
    this.libelleHtml = libelleHtml;
  }

  public Boolean getLibelleHtml() {
    return libelleHtml;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

}
