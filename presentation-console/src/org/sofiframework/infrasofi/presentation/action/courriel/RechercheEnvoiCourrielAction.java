package org.sofiframework.infrasofi.presentation.action.courriel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.filtre.FiltreEnvoiCourriel;
import org.sofiframework.infrasofi.presentation.action.BaseRechercheAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;

public class RechercheEnvoiCourrielAction extends BaseRechercheAction {

  @Override
  public String getNomListeDansSession() {
    return "listeEnvoiCourriel";
  }

  @Override
  public ObjetTransfert construireNouveauFiltre() {
    return new FiltreEnvoiCourriel();
  }

  @Override
  public void effectuerRecherche(ListeNavigation liste, BaseForm formulaire,
      ModeleInfrastructure modele, HttpServletRequest request,
      HttpServletResponse response) throws ModeleException {
    this.getModeleInfrastructure(request).getServiceEnvoiCourriel().getListeEnvoiCourriel(liste);
  }
}
