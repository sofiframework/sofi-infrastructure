package org.sofiframework.infrasofi.presentation.action.courriel;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseDynaForm;

public class RechercheEnvoiCourrielForm extends BaseDynaForm {

  private static final long serialVersionUID = -3471641487355976419L;

  @Override
  public void validerFormulaire(HttpServletRequest request) {
    super.validerFormulaire(request);
  }
}
