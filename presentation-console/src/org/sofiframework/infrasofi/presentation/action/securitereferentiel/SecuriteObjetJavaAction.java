/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.securitereferentiel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.modele.entite.RoleObjetJava;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.utilitaire.UtilitaireString;

public class SecuriteObjetJavaAction extends InfrastructureAction {

  /**
   * Cette méthode s'exécute lorsque l'utilisateur accède au détail d'un objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws ServletException {
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    SecuriteObjetJavaForm formulaire = (SecuriteObjetJavaForm) form;
    Long id = new Long(request.getParameter("seqObjetSecurisable"));
    formulaire.initialiserFormulaire(request, mapping, true);
    ObjetJava objetJava = modele.getServiceReferentiel().getObjetJava(id);

    if (objetJava.getListeRoleObjetJavaAutorise() == null) {
      objetJava.setListeRoleObjetJavaAutorise(new ArrayList<RoleObjetJava>());
    }

    formulaire.populerFormulaire(objetJava, request);
    List listeRoleApplication = modele.getServiceRole().getListeRole(objetJava.getCodeApplication());
    request.getSession().setAttribute("LISTE_ROLE_POUR_APPLICATION", listeRoleApplication);
    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur utilise le bouton "Nouveau" afin
   * de créer un nouvel objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward creer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws ServletException {
    SecuriteObjetJavaForm formulaire = (SecuriteObjetJavaForm) form;
    ObjetJava nouvelObjet = new ObjetJava();
    formulaire.initialiserFormulaire(request, mapping, true);
    nouvelObjet.setListeRoleObjetJavaAutorise(new ArrayList<RoleObjetJava>());
    formulaire.populerFormulaire(nouvelObjet, request);
    formulaire.ajouterMessageInformatif("infra_sofi.information.gestion_referentiel.onglet.detail.ajout_nouvel_objet_java", request);
    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur enregistre les modifications des rôles pour un objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws ServletException {
    ModeleInfrastructure modele = (ModeleInfrastructure) this.getModele(request);
    SecuriteObjetJavaForm formulaire = (SecuriteObjetJavaForm) form;
    ObjetJava objetJava = (ObjetJava) formulaire.getObjetTransfert();

    List<RoleObjetJava> listeRoleObjetJava = objetJava.getListeRoleObjetJavaAutorise();
    // Supprimer les objets vide de la liste avant la sauvegarde.
    Iterator iterateur = listeRoleObjetJava.iterator();
    while (iterateur.hasNext()) {
      RoleObjetJava objet = (RoleObjetJava)iterateur.next();
      if (UtilitaireString.isVide(objet.getCodeRole())) {
        iterateur.remove();
      }
    }

    modele.getServiceReferentiel().enregistrerListeRoleObjetJavaPourUnObjetJava(
        listeRoleObjetJava,objetJava.getId(),
        objetJava.getCodeApplication());
    this.commit(request, formulaire);
    return mapping.findForward("afficher");
  }



  /**
   * Cette méthode s'exécute lorsque l'utilisateur supprime un objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward supprimer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws ServletException {
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    SecuriteObjetJavaForm formulaire = (SecuriteObjetJavaForm) form;
    Serializable id = ((ObjetJava) formulaire.getObjetTransfert()).getId();
    modele.getServiceReferentiel().supprimerObjetJava(id);
    this.commit(request, formulaire,
        "infra_sofi.information.gestion_referentiel.suppression.succes",
        new String[] {});
    return mapping.findForward("recherche");
  }

  /**
   * Cette redirige vers la page d'affichage du détail d'un objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    return mapping.findForward("page");
  }


  /**
   * Cette méthode s'exécute lorsque l'utilisateur ajoute un nouveau nested de rôle objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward ajouterRoleObjetJava(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    SecuriteObjetJavaForm formulaire = (SecuriteObjetJavaForm) form;
    ObjetJava o = (ObjetJava) formulaire.getObjetTransfert();
    RoleObjetJava roj = new RoleObjetJava();
    roj.setIdObjetJava(o.getId());
    formulaire.ajouterLigne("listeRoleObjetJavaAutorise", roj, request);
    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque supprime un nested de rôle objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward supprimerRoleObjetJava(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    SecuriteObjetJavaForm formulaire = (SecuriteObjetJavaForm) form;
    int index = Integer.valueOf(request.getParameter("index")).intValue();
    formulaire.supprimerLigne("listeRoleObjetJavaAutorise", index, request);
    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur enregistre les modifications des rôles pour un objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward reporterSecuriteVersEnfants(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    SecuriteObjetJavaForm formulaire = (SecuriteObjetJavaForm) form;
    ObjetJava objetJava = (ObjetJava) formulaire.getObjetTransfert();
    modele.getServiceReferentiel().enregistrerListeRoleObjetJavaPourEnfants(
        objetJava.getId(),
        objetJava.getListeRoleObjetJavaAutorise(),
        objetJava.getCodeApplication());
    this.commit(request, formulaire);
    return mapping.findForward("afficher");
  }
}