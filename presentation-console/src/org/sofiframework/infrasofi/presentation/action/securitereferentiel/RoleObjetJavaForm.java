/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.securitereferentiel;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseForm;

public class RoleObjetJavaForm extends BaseForm {

  private static final long serialVersionUID = -2667096622723749795L;

  /** l'identifiant unique de l'application auquel on ajoute un droit d'accès */
  private String codeApplication;

  /** l'identifiant unique de l'objet java auquel on donne des droits */
  private String idObjetJava;

  /** la valeur qui indique si l'objet java est en consultation ou non */
  private Boolean indicateurConsultation;

  /** l'identifiant unique du rôle auquel on donne droit à la fonctionnalité */
  private String codeRole;

  public RoleObjetJavaForm() {
  }

  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getIdObjetJava() {
    return idObjetJava;
  }

  public void setIdObjetJava(String idObjetJava) {
    this.idObjetJava = idObjetJava;
  }

  public Boolean getIndicateurConsultation() {
    return indicateurConsultation;
  }

  public void setIndicateurConsultation(Boolean indicateurConsultation) {
    this.indicateurConsultation = indicateurConsultation;
  }

  public String getCodeRole() {
    return codeRole;
  }

  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

}
