/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.securitereferentiel;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseForm;

public class RechercheSecuriteReferentielForm extends BaseForm{

  private static final long serialVersionUID = -2667096622723749795L;

  /** l'identifiant unique de l'application pour lequel on fait une recherche */
  private String codeApplication;

  /** l'identifiant unique de l'objet java à rechercher */
  private String nom;

  /**
   * Le statut d'activation, A (Actif) ou I (Inactif).
   */
  private String codeStatut;

  /** le type de composant du référentiel à rechercher */
  private String type;

  /** l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel */
  private String codeRole;

  /** Spécifie si on désire les objet qui n'inclus le role spécifié **/
  private boolean codeRoleNonInclus;

  public RechercheSecuriteReferentielForm() {
    super();
  }

  @Override
  public void validerFormulaire(HttpServletRequest request) {

  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getCodeStatut() {
    return codeStatut;
  }

  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCodeRole() {
    return codeRole;
  }

  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  public boolean isCodeRoleNonInclus() {
    return codeRoleNonInclus;
  }

  public void setCodeRoleNonInclus(boolean codeRoleNonInclus) {
    this.codeRoleNonInclus = codeRoleNonInclus;
  }


}
