/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.securitereferentiel;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.filtre.FiltreObjetReferentielRole;
import org.sofiframework.infrasofi.presentation.action.BaseRechercheAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;

public class RechercheSecuriteReferentielAction extends BaseRechercheAction {

  @Override
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    this.chargerListeRole(form, request, response, false);
    ActionForward retour = super.acceder(mapping, form, request, response);
    return retour;
  }

  @Override
  public void effectuerRecherche(ListeNavigation liste, BaseForm form, ModeleInfrastructure modele, HttpServletRequest request, HttpServletResponse response) throws ModeleException  {
    liste.ajouterTriInitial("nom", true);
    this.chargerListeRole(form, request, response, false);

    RechercheSecuriteReferentielForm rechercheSecuriteReferentielForm = (RechercheSecuriteReferentielForm)form;
    FiltreObjetReferentielRole filtre = (FiltreObjetReferentielRole)rechercheSecuriteReferentielForm.getObjetTransfert();
    filtre.setCodeApplication(rechercheSecuriteReferentielForm.getCodeApplication());
    liste.setObjetFiltre(filtre);
    liste = modele.getServiceReferentiel().getListeObjetJavaPourSecurite(liste);
  }

  @Override
  public String getNomListeDansSession() {
    return "listeSecuriteReferentiel";
  }

  public ActionForward chargerListeRole(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) {
    this.chargerListeRole(form, request, response, true);
    return mapping.findForward("liste_role");
  }

  private void chargerListeRole(ActionForm form, HttpServletRequest request,
      HttpServletResponse response, boolean ajax)  {

    // Accéder au modèle.
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    RechercheSecuriteReferentielForm formulaire = (RechercheSecuriteReferentielForm) form;
    FiltreObjetReferentielRole filtre;

    if (!ajax) {
      filtre = new FiltreObjetReferentielRole();
      filtre.setCodeApplication("INFRA_SOFI");
    } else {
      filtre = (FiltreObjetReferentielRole) formulaire.getObjetTransfert();
    }

    try {
      List listeRole = modele.getServiceRole().getListeRole(filtre.getCodeApplication());

      if (ajax) {
        // Génération de la réponse Ajax pour remplir la liste déroulante.
        this.genererReponseListeDeroulante(listeRole, "nom", "code", "", "",
            false, request, response);
      }

      // Fixer la liste dans la session temporaire pour service en cours.
      this.setAttributTemporairePourService("LISTE_ROLE", listeRole, request);
    } catch (ModeleException e) {
      throw e;
    }
  }

  @Override
  public ObjetTransfert construireNouveauFiltre() {
    FiltreObjetReferentielRole filtre =  new FiltreObjetReferentielRole();
    return filtre;
  }
}
