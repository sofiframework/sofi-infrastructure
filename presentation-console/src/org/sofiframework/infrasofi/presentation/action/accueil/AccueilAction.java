/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.accueil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;

/**
 * Action d'accueil.
 */
public class AccueilAction extends InfrastructureAction {

  private static final Integer NOMBRE_JOURS = new Integer(10);

  /**
   * Porte d'entrer à l'unité de traitement.
   */
  public ActionForward acceder(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {

    AccueilForm formulaire = (AccueilForm) form;
    // Fixer l'application sélectionné et la conservé dans la session pour
    // sélection
    // par défaut des autres services.
    if (formulaire.getCodeApplication() != null) {
      request.getSession().setAttribute("systeme_selectionne", formulaire.getCodeApplication());
    }
    return mapping.findForward("afficher");
  }

  public ActionForward afficherJournalisation(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    chargementJournalisation(request, response, false);
    return mapping.findForward("ajax_journalisation");
  }

  public ActionForward afficherMessage(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    chargementMessage(request);
    return mapping.findForward("ajax_message");
  }

  public ActionForward afficherLibelle(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    chargementLibelle(request);
    return mapping.findForward("ajax_libelle");
  }

  public ActionForward afficherAide(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    chargementAide(request);
    return mapping.findForward("ajax_aide");
  }

  public ActionForward afficherParametre(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    chargementParametre(request);
    return mapping.findForward("ajax_parametre");
  }

  public ActionForward afficher(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    AccueilForm formulaire = (AccueilForm) form;
    formulaire.setCodeApplication(getApplicationSelectionne(request));
    
    chargementAide(request);
    chargementJournalisation(request, response, true);
    chargementLibelle(request);
    chargementMessage(request);
    chargementParametre(request);
    
    return mapping.findForward("page_accueil");
  }

  private void chargementAide(HttpServletRequest request) {
    if (chargerDiv("sectionTableauDeBordAide", request)) {
      ListeNavigation liste = this.appliquerListeNavigation("listeAideTableauBord",request);
      liste.setMaxParPage(10);
      String codeApplication = getApplicationSelectionne(request);
      liste.ajouterTriInitial("dateCreation", false);
      getModeleInfrastructure(request).getServiceAideContextuelle()
      .getListeAidePourTableauBord(liste, NOMBRE_JOURS, codeApplication);
      setAttributTemporaireListeNavigationPourAction("listeAideTableauBord", liste, request);
    }
  }

  private void chargementJournalisation(HttpServletRequest request, HttpServletResponse response, boolean ouvertParDefaut) throws ServletException {
    if (chargerDiv("sectionTableauDeBordJournalisation", request) || ouvertParDefaut) {
      ListeNavigation listeJournalisation = this.appliquerListeNavigation("listeJournalisationTableauBord", request);
      listeJournalisation.ajouterTriInitial("dateHeure", false);
      String codeApplication = getApplicationSelectionne(request);
      listeJournalisation.setMaxParPage(10);
      getModeleInfrastructure(request).getServiceJournalisation()
      .getListeJournalisationPourTableauBord(listeJournalisation, codeApplication);
      setAttributTemporaireListeNavigationPourAction("listeJournalisationTableauBord", listeJournalisation, request);
    }
  }

  private void chargementMessage(HttpServletRequest request) {
    if (chargerDiv("sectionTableauDeBordMessage", request)) {
      ListeNavigation listeMessage = this.appliquerListeNavigation("listeMessageTableauBord",request);
      listeMessage.setMaxParPage(10);
      listeMessage.ajouterTriInitial("dateCreation", false);
      getModeleInfrastructure(request).getServiceMessage()
      .getListeMessagesPourTableauBord(listeMessage,
          NOMBRE_JOURS, getApplicationSelectionne(request));
      setAttributTemporaireListeNavigationPourAction("listeMessageTableauBord", listeMessage, request);
    }
  }

  private void chargementLibelle(HttpServletRequest request) {
    if (chargerDiv("sectionTableauDeBordLibelle", request)) {
      ListeNavigation listeLibelle = this.appliquerListeNavigation("listeLibelleTableauBord",request);
      listeLibelle.setMaxParPage(10);
      listeLibelle.ajouterTriInitial("dateCreation", false);
      getModeleInfrastructure(request).getServiceLibelle()
      .getListeLibellesPourTableauBord(
          listeLibelle, NOMBRE_JOURS, getApplicationSelectionne(request));
      setAttributTemporaireListeNavigationPourAction("listeLibelleTableauBord", listeLibelle, request);
    }
  }

  private void chargementParametre(HttpServletRequest request) {
    if (chargerDiv("sectionTableauDeBordParametre", request)) {
      ListeNavigation listeParametre = this.appliquerListeNavigation("listeParametreTableauBord",request);
      listeParametre.setMaxParPage(10);
      listeParametre.ajouterTriInitial("dateCreation", false);
      getModeleInfrastructure(request).getServiceParametreSysteme()
      .getListeParametresPourTableauBord(
          listeParametre, NOMBRE_JOURS, getApplicationSelectionne(request));
      setAttributTemporaireListeNavigationPourAction("listeParametreTableauBord", listeParametre, request);
    }
  }

  private boolean chargerDiv(String id, HttpServletRequest request) {
    return !isDivInitialise(id, request) || isDivOuvert(id, request);
  }
}
