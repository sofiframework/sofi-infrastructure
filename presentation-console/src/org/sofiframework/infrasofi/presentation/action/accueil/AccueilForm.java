/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.accueil;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * Formulaire pour la page d'accueil.
 * 
 * @author Jean-Maxime Pelletier
 */
public class AccueilForm extends BaseForm {

  private static final long serialVersionUID = 1202059261317793228L;

  public AccueilForm() {
    super();
  }

  /**
   * Le code d'application à afficher dans le tableau de bord.
   */
  private String codeApplication;


  /**
   * Valide le formulaire.
   * @param request la requete HTTP en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }
}
