/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.utilisateur;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.filtre.FiltreUtilisateurRole;
import org.sofiframework.infrasofi.presentation.action.BaseRechercheAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * Recherche des utilisateurs.
 * 
 * @author Jean-Maxime Pelletier
 */
public class RechercheUtilisateurAction extends BaseRechercheAction {

  @Override
  public ActionForward acceder(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    ActionForward direction = super.acceder(mapping, form, request, response);
    List listeRole = getModeleInfrastructure(request).getServiceRole()
        .getListeRole(this.getApplicationSelectionne(request));
    setAttributTemporairePourService("LISTE_ROLE", listeRole, request);
    return direction;
  }

  @Override
  public ObjetTransfert construireNouveauFiltre() {
    return new FiltreUtilisateurRole();
  }

  @Override
  public void effectuerRecherche(ListeNavigation liste, BaseForm form, ModeleInfrastructure modele,
      HttpServletRequest request, HttpServletResponse response) throws ModeleException {

    FiltreUtilisateurRole filtre = (FiltreUtilisateurRole) liste.getFiltre();
    RechercheUtilisateurForm rechercheUtiliateurForm = (RechercheUtilisateurForm) form;
    filtre.setCodeApplication(rechercheUtiliateurForm.getCodeApplication());
    filtre.setCodeRole(rechercheUtiliateurForm.getCodeRole());
    liste.setFiltre(filtre);

    liste = modele.getServiceUtilisateur().getListeUtilisateurPilotage(liste);

    chargerListeRole(rechercheUtiliateurForm, request, response, false);
  }

  @Override
  public String getNomListeDansSession() {
    return "listeRechercheUtilisateur";
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur sélectionne une application.
   * <p>
   * Cette action met à jour le contenu de la liste déroulante des rôles.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  private void chargerListeRole(ActionForm form,
      HttpServletRequest request, HttpServletResponse response, boolean ajax) {
    RechercheUtilisateurForm formulaire = (RechercheUtilisateurForm) form;
    // Extraire la liste des équipes pour la catégorie de ligue du joueur.
    List listeRole = getModeleInfrastructure(request).getServiceRole().getListeRole(formulaire.getCodeApplication());
    if (ajax) {
      // Génération de la réponse Ajax pour remplir la liste déroulante.
      genererReponseListeDeroulante(listeRole, "nom", "code", "", "", false, request,
          response);
    }
    // Fixer la liste dans la session temporaire pour service en cours.
    setAttributTemporairePourService("LISTE_ROLE", listeRole, request);

  }


  /**
   * Cette méthode s'exécute lorsque l'utilisateur sélectionne une application.
   * <p>
   * Cette action met à jour le contenu de la liste déroulante des rôles.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward chargerListeRole(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    chargerListeRole(form, request,response, true);
    return null;
  }
}