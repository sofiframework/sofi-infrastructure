/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.utilisateur;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.presentation.action.RechercheApplicativeForm;


/**
 * Objet d'affaire faisant represetant un les critères
 * utilisés pour recherche un utilisateur.
 * <p>
 * @author Maxime Ouellet, Nurun inc.
 * @version 1.0
 */
public class RechercheUtilisateurForm extends RechercheApplicativeForm {

  private static final long serialVersionUID = 3127943404136060592L;

  /** l'identifiant unique de l'utilisateur */
  private String codeUtilisateur;

  /** l'ID unique de l'utilisateur */
  private String id;

  /** le nom de l'utilisateur */
  private String nom;

  /** le prenom de l'utilisateur */
  private String prenom;

  /** le courriel de l'utilisateur */
  private String courriel;

  /** le code de statut de l'utilisateur dans l'application */
  private String codeStatut;

  /** l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel */
  private String codeRole;

  /** Spécifie si on désire les objet qui n'inclus le role spécifié **/
  private boolean codeRoleNonInclus;

  /**
   * Spécifie le code utilisateur applicatif en référence au code utilisateur
   * qui est plus technique.
   */
  private String codeUtilisateurApplicatif;

  /**
   * Constructeur par défaut.
   */
  public RechercheUtilisateurForm() {
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un <code>submit</code>.
   * <p>
   * @param request la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  /**
   * Obtenir l'identifiant unique de l'utilisateur.
   * @return l'identifiant unique de l'utilisateur.
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * Fixer l'identifiant unique de l'utilisateur.
   * @param codeUtilisateur l'identifiant unique de l'utilisateur.
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * Obtenir le nom de l'utilisateur.
   * @return le nom de l'utilisateur.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de l'utilisateur.
   * @param nom le nom de l'utilisateur.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le prenom de l'utilisateur.
   * @return le prenom de l'utilisateur.
   */
  public String getPrenom() {
    return prenom;
  }

  /**
   * Fixer le prenom de l'utilisateur.
   * @param prenom le prenom de l'utilisateur.
   */
  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  /**
   * Obtenir le courriel de l'utilisateur.
   * @return le courriel de l'utilisateur.
   */
  public String getCourriel() {
    return courriel;
  }

  /**
   * Fixer le courriel de l'utilisateur.
   * @param courriel le courriel de l'utilisateur.
   */
  public void setCourriel(String courriel) {
    this.courriel = courriel;
  }

  /**
   * Obtenir le code de statut de l'utilisateur dans l'application.
   * @return le code de statut de l'utilisateur dans l'application.
   */
  public String getCodeStatut() {
    return codeStatut;
  }

  /**
   * Fixer le code de statut de l'utilisateur dans l'application.
   * @param codeStatut le code de statut de l'utilisateur dans l'application.
   */
  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  /**
   * Obtenir l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel.
   * <p>
   * @return l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel
   */
  public String getCodeRole() {
    return codeRole;
  }

  /**
   * Fixer l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel.
   * <p>
   * @param codeRole l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel
   */
  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  /**
   * Obtenir spécifie si on désire les objet qui n'inclus le role spécifié.
   * <p>
   * @return codeRoleNonInclus spécifie si on désire les objet qui n'inclus le role spécifié
   */
  public void setCodeRoleNonInclus(boolean codeRoleNonInclus) {
    this.codeRoleNonInclus = codeRoleNonInclus;
  }

  /**
   * Fixer spécifie si on désire les objet qui n'inclus le role spécifié.
   * <p>
   * @param spécifie si on désire les objet qui n'inclus le role spécifié
   */
  public boolean isCodeRoleNonInclus() {
    return codeRoleNonInclus;
  }

  public void setCodeUtilisateurApplicatif(String codeUtilisateurApplicatif) {
    this.codeUtilisateurApplicatif = codeUtilisateurApplicatif;
  }

  public String getCodeUtilisateurApplicatif() {
    return codeUtilisateurApplicatif;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }
}
