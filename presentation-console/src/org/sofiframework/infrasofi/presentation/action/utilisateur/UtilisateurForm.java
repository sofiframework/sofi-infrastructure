/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.utilisateur;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Utilisateur;
import org.sofiframework.infrasofi.presentation.action.FormulaireTrace;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Objet d'affaire faisant represetant un utilisateur.
 * <p>
 * @author Maxime Ouellet, Nurun inc.
 * @version 1.0
 */
public class UtilisateurForm extends FormulaireTrace {

  private static final long serialVersionUID = -7468647771109741785L;

  /** l'identifiant unique de l'utilisateur */
  private String codeUtilisateur;

  /** l'ID unique de l'utilisateur */
  private String id;

  /** le mot de passe pour l'authentification de l'utilisateur */
  private String motPasse;

  /** l'identifiant d'un nouvel utilisateur */
  private String nouveauCodeUtilisateur;

  /** le mot de passe d'un nouvel utilisateur */
  private String nouveauMotPasse;

  /** le nom de l'utilisateur */
  private String nom;

  /** le prenom de l'utilisateur */
  private String prenom;

  /** le courriel de l'utilisateur */
  private String courriel;

  /** le code de statut de l'utilisateur dans l'application */
  private String codeStatut;

  /** l'identifiant unique de l'application pour lequel on fait une recherche */
  private String codeApplication;

  /** l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel */
  private String codeRole;

  /** Spécifie si on désire les objet qui n'inclus le role spécifié **/
  private boolean codeRoleNonInclus;

  /** Spécifie que l'on doit appliquer un changement de mot de passe **/
  private boolean changementMotPasse;

  private String langue;

  private String fuseauHoraire;

  private String sexe;

  private String urlPhoto;

  private String codeFournisseurAcces;

  private String dateNaissance;

  private String description;

  private String codeGroupeAge;

  /**
   * Constructeur par défaut.
   */
  public UtilisateurForm() {
    super();
    this.ajouterAttributAExclure("listeUtilisateurRole");
    this.ajouterFormatterDateHeureSeconde("dateCreation");
    this.ajouterFormatterDateHeureSeconde("dateModification");
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un <code>submit</code>.
   * <p>
   * @param request la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
    ModeleInfrastructure modele = (ModeleInfrastructure) this.getModele(request);

    //Validation du format du courriel si fourni.
    if (!UtilitaireString.isVide(this.getCourriel())) {
      if (!UtilitaireString.isCourriel(this.getCourriel())) {
        this.ajouterMessageErreur("infra_sofi.erreur.commun.format.courriel",
            "courriel", request);
      }
    }

    //Valider si le code utilisateur est déjà utilisé dans la base de données
    if (request.getParameter("methode").equals("enregistrer")) {

      if (nouveauCodeUtilisateur != null) {
        Utilisateur utilisateur = modele.getServiceUtilisateur().getUtilisateur(nouveauCodeUtilisateur);
        if (utilisateur != null) {
          this.ajouterMessageErreur("infra_sofi.erreur.gestion_utilisateur.code.utilisateur","nouveauCodeUtilisateur", request);
        }
      }
    }
  }

  /**
   * Obtenir l'identifiant unique de l'utilisateur.
   * @return l'identifiant unique de l'utilisateur.
   */
  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  /**
   * Fixer l'identifiant unique de l'utilisateur.
   * @param codeUtilisateur l'identifiant unique de l'utilisateur.
   */
  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  /**
   * Obtenir le mot de passe pour l'authentification de l'utilisateur.
   * @return le mot de passe pour l'authentification de l'utilisateur.
   */
  public String getMotPasse() {
    return motPasse;
  }

  /**
   * Fixer le mot de passe pour l'authentification de l'utilisateur.
   * @param modeDePasse le mot de passe pour l'authentification de l'utilisateur.
   */
  public void setMotPasse(String motPasse) {
    this.motPasse = motPasse;
  }

  /**
   * Obtenir le nom de l'utilisateur.
   * @return le nom de l'utilisateur.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de l'utilisateur.
   * @param nom le nom de l'utilisateur.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le prenom de l'utilisateur.
   * @return le prenom de l'utilisateur.
   */
  public String getPrenom() {
    return prenom;
  }

  /**
   * Fixer le prenom de l'utilisateur.
   * @param prenom le prenom de l'utilisateur.
   */
  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  /**
   * Obtenir le courriel de l'utilisateur.
   * @return le courriel de l'utilisateur.
   */
  public String getCourriel() {
    return courriel;
  }

  /**
   * Fixer le courriel de l'utilisateur.
   * @param courriel le courriel de l'utilisateur.
   */
  public void setCourriel(String courriel) {
    this.courriel = courriel;
  }

  /**
   * Obtenir le code de statut de l'utilisateur dans l'application.
   * @return le code de statut de l'utilisateur dans l'application.
   */
  public String getCodeStatut() {
    return codeStatut;
  }

  /**
   * Fixer le code de statut de l'utilisateur dans l'application.
   * @param codeStatut le code de statut de l'utilisateur dans l'application.
   */
  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  /**
   * Obtenir l'identifiant unique de l'application pour lequel on fait une recherche.
   * <p>
   * @return l'identifiant unique de l'application pour lequel on fait une recherche
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique de l'application pour lequel on fait une recherche.
   * <p>
   * @param codeApplication l'identifiant unique de l'application pour lequel on fait une recherche
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel.
   * <p>
   * @return l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel
   */
  public String getCodeRole() {
    return codeRole;
  }

  /**
   * Fixer l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel.
   * <p>
   * @param codeRole l'identifiant unique du rôle pour lequel on recherche les éléments du référentiel
   */
  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  /**
   * Obtenir spécifie si on désire les objet qui n'inclus le role spécifié.
   * <p>
   * @return codeRoleNonInclus spécifie si on désire les objet qui n'inclus le role spécifié
   */
  public void setCodeRoleNonInclus(boolean codeRoleNonInclus) {
    this.codeRoleNonInclus = codeRoleNonInclus;
  }

  /**
   * Fixer spécifie si on désire les objet qui n'inclus le role spécifié.
   * <p>
   * @param spécifie si on désire les objet qui n'inclus le role spécifié
   */
  public boolean isCodeRoleNonInclus() {
    return codeRoleNonInclus;
  }

  public void setNouveauCodeUtilisateur(String nouveauCodeUtilisateur) {
    this.nouveauCodeUtilisateur = nouveauCodeUtilisateur;
  }

  public String getNouveauCodeUtilisateur() {
    return nouveauCodeUtilisateur;
  }

  public void setNouveauMotPasse(String nouveauMotPasse) {
    this.nouveauMotPasse = nouveauMotPasse;
  }

  public String getNouveauMotPasse() {
    return nouveauMotPasse;
  }

  public void setChangementMotPasse(boolean changementMotPasse) {
    this.changementMotPasse = changementMotPasse;
  }

  public boolean isChangementMotPasse() {
    return changementMotPasse;
  }

  public void setLangue(String langue) {
    this.langue = langue;
  }

  public String getLangue() {
    return langue;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public String getFuseauHoraire() {
    return fuseauHoraire;
  }

  public void setFuseauHoraire(String fuseauHoraire) {
    this.fuseauHoraire = fuseauHoraire;
  }

  public String getSexe() {
    return sexe;
  }

  public void setSexe(String sexe) {
    this.sexe = sexe;
  }

  public String getCodeFournisseurAcces() {
    return codeFournisseurAcces;
  }

  public void setCodeFournisseurAcces(String codeFournisseurAcces) {
    this.codeFournisseurAcces = codeFournisseurAcces;
  }

  public String getDateNaissance() {
    return dateNaissance;
  }

  public void setDateNaissance(String dateNaissance) {
    this.dateNaissance = dateNaissance;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getCodeGroupeAge() {
    return codeGroupeAge;
  }

  public void setCodeGroupeAge(String codeGroupeAge) {
    this.codeGroupeAge = codeGroupeAge;
  }

  public String getUrlPhoto() {
    return urlPhoto;
  }

  public void setUrlPhoto(String urlPhoto) {
    this.urlPhoto = urlPhoto;
  }
}
