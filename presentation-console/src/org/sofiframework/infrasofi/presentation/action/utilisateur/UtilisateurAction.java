/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.utilisateur;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.infrasofi.modele.entite.Utilisateur;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.modele.exception.ModeleException;


public class UtilisateurAction extends InfrastructureAction {

  /**
   * Cette méthode s'exécute lorsque l'utilisateur utilise le bouton "Créer" afin
   * de créer un nouvel utilisateur.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward ajouter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    // Récupérer et initialiser le formulaire
    UtilisateurForm formulaire = (UtilisateurForm) form;
    formulaire.initialiserFormulaire(request, mapping, new Utilisateur(), true);
    formulaire.ajouterMessageInformatif("Ajout d'un nouvel utilisateur.",
        request);

    exclureComposantInterface("infra_sofi.libelle.gestion_utilisateur.onglet.role",
        request);

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur accède au détail d'un utilisateur.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    UtilisateurForm formulaire = (UtilisateurForm) form;
    try {
      String codeUtilisateur = formulaire.getCodeUtilisateur();
      // Récupérer le rôle et populer le formulaire avec
      Utilisateur utilisateur = getModeleInfrastructure(request)
          .getServiceUtilisateur().getUtilisateur(codeUtilisateur);
      inclureComposantInterface("infra_sofi.libelle.gestion_utilisateur.onglet.role",
          request);
      setAttributTemporairePourService("codeUtilisateur", utilisateur.getCodeUtilisateur(), request);
      formulaire.populerFormulaire(utilisateur, request, true);
    } catch (ModeleException e) {
      e.printStackTrace();
      formulaire.ajouterMessageErreur(e, request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lors de la sauvegarde d'un utilisateur.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    UtilisateurForm formulaire = (UtilisateurForm) form;

    // Récupérer l'utilisateur et effectuer la sauvegarde
    try {
      Utilisateur utilisateur = (Utilisateur) formulaire.getObjetTransfert();

      if (formulaire.isNouveauFormulaire()) {
        utilisateur.setCodeUtilisateur(formulaire.getNouveauCodeUtilisateur());
        utilisateur.setMotPasse(formulaire.getNouveauMotPasse());
        utilisateur.setChangementMotPasse(true);
        getModeleInfrastructure(request).getServiceUtilisateur()
        .ajouterUtilisateur(utilisateur);
        inclureComposantInterface("infra_sofi.libelle.gestion_utilisateur.onglet.role",
            request);
      } else {
        if (!isVide(formulaire.getNouveauMotPasse())) {
          utilisateur.setMotPasse(formulaire.getNouveauMotPasse());
          utilisateur.setChangementMotPasse(true);
        }

        getModeleInfrastructure(request).getServiceUtilisateur().modifierUtilisateur(utilisateur);
      }

      formulaire.setChangementMotPasse(false);
      this.commit(request, formulaire);
      formulaire.setCodeUtilisateur(utilisateur.getCodeUtilisateur());
    } catch (ModeleException e) {
      e.printStackTrace();
      formulaire.ajouterMessageErreur(e, request);
      this.rollback(request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur supprime un utilisateur.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward supprimer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    UtilisateurForm formulaire = (UtilisateurForm) form;
    Utilisateur utilisateur = (Utilisateur) formulaire.getObjetTransfert();

    try {
      // Effectuer la suppression
      getModeleInfrastructure(request).getServiceUtilisateur()
      .supprimerUtilisateur(utilisateur.getId());
      this.commit(request,
          "L'utilisateur '{0} {1}' a été supprimé avec succès",
          new String[] { formulaire.getPrenom(), formulaire.getNom() });
    } catch (ModeleException e) {
      e.printStackTrace();
      formulaire.ajouterMessageErreur("infra_sofi.erreur.commun.suppression.impossible",
          request);
      this.rollback(request);

      return mapping.findForward("afficher");
    }

    return mapping.findForward("recherche");
  }

  /**
   * Cette redirige vers la page d'affichage de la recherche.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward changerMotPasse(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    UtilisateurForm formulaire = (UtilisateurForm) form;
    formulaire.setChangementMotPasse(true);

    formulaire.ajouterMessageInformatif("infra_sofi.information.securite.utilisateur.nouveau.mot_passe",
        request);

    return mapping.findForward("afficher");
  }

  public ActionForward annulerChangerMotPasse(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    UtilisateurForm formulaire = (UtilisateurForm) form;
    formulaire.setChangementMotPasse(false);

    return mapping.findForward("afficher");
  }

  /**
   * Cette redirige vers la page d'affichage du détail d'un rôle.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    return mapping.findForward("page");
  }
}
