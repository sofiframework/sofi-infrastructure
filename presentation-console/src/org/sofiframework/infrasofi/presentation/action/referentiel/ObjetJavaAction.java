/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.referentiel;

import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Libelle;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.modele.exception.ModeleException;

public class ObjetJavaAction extends InfrastructureAction {



  /**
   * Cette méthode s'exécute lorsque l'utilisateur accède au détail d'un objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    // Récupérer les objets de base
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    ObjetJavaForm formulaire = (ObjetJavaForm) form;

    try {
      // Récupérer l'Objet Java et populer le formulaire avec

      Long id = new Long(request.getParameter("seqObjetSecurisable"));
      formulaire.initialiserFormulaire(request, mapping, true);
      ObjetJava objetJava = modele.getServiceReferentiel().getObjetJava(id);

      formulaire.populerFormulaire(objetJava, request);
      if (objetJava.getObjetJavaParent() != null){
        formulaire.set("nomParent", objetJava.getObjetJavaParent().getNom());
        formulaire.set("typeObjetParent", objetJava.getObjetJavaParent().getType());
      }
    } catch (ModeleException e) {
      formulaire.ajouterMessageErreur(e, request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur utilise le bouton "Nouveau" afin
   * de créer un nouvel objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward creer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    // Récupérer et initialiser le formulaire
    ObjetJavaForm formulaire = (ObjetJavaForm) form;
    ObjetJava nouvelObjet = new ObjetJava();
    ObjetJava objetJavaParent = null;
    String idParent = request.getParameter("idObjetJavaParent");
    if (idParent != null){
      nouvelObjet.setIdParent(new Long(idParent));
      objetJavaParent = getModeleInfrastructure(request).getServiceReferentiel().getObjetJava(nouvelObjet.getIdParent());

      if (objetJavaParent.getNom() != null && objetJavaParent.getNom().indexOf(".") != -1){
        int index1 = objetJavaParent.getNom().indexOf(".");
        int index2 = objetJavaParent.getNom().indexOf(".", index1 + 1 );
        nouvelObjet.setNom(objetJavaParent.getNom().substring(0,objetJavaParent.getNom().indexOf(".",index2 +1)));
      }
      nouvelObjet.setCodeApplication(objetJavaParent.getCodeApplication());
    }

    formulaire.initialiserFormulaire(request, mapping, true);
    formulaire.populerFormulaire(nouvelObjet, request);
    formulaire.ajouterMessageInformatif("infra_sofi.information.gestion_referentiel.onglet.detail.ajout_nouvel_objet_java", request);

    if (objetJavaParent != null){

      nouvelObjet.setObjetJavaParent(objetJavaParent);
      if (objetJavaParent != null){
        formulaire.set("nomParent", objetJavaParent.getNom());
        formulaire.set("typeObjetParent", objetJavaParent.getType());
      }
    }



    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur sauvegarde un objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    // Récupérer les données de base
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    ObjetJavaForm formulaire = (ObjetJavaForm) form;

    try {
      ObjetJava objetJava = (ObjetJava) formulaire.getObjetTransfert();
      ObjetJava objetJavaOriginal = (ObjetJava) formulaire.getObjetTransfertOriginal();
      String libelle = formulaire.getString("libelle");
      modele.getServiceReferentiel().enregistrerObjetJava(objetJava, objetJavaOriginal, libelle);
      this.commit(request, formulaire);
      formulaire.populerFormulaire(objetJava, request);
    } catch (ModeleException e) {
      formulaire.ajouterMessageErreur(e, request);
      this.rollback(request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur supprime un objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward supprimer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    // Récupérer les données de base
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    ObjetJavaForm formulaire = (ObjetJavaForm) form;
    ObjetJava objetJava = (ObjetJava) formulaire.getObjetTransfert();

    if (request.getParameter("seqObjetSecurisable") != null) {
      objetJava = modele.getServiceReferentiel().getObjetJava(new Long(request.getParameter("seqObjetSecurisable")));
    }
    Serializable id = objetJava.getId();

    try {
      modele.getServiceReferentiel().supprimerObjetJava(id);
      this.commit(request, formulaire,
          "infra_sofi.information.gestion_referentiel.suppression.succes",
          new String[] {objetJava.getNom()});
    } catch (ModeleException e) {
      e.printStackTrace();
      formulaire.ajouterMessageErreurGeneral("infra_sofi.erreur.gestion_referentiel.suppression.impossible",
          request);
      this.rollback(request);
    }

    return mapping.findForward("recherche");
  }

  /**
   * Cette redirige vers la page d'affichage du détail d'un objet java.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    return mapping.findForward("page");
  }

  public ActionForward fixerLibelle(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    ObjetJavaForm formulaire = (ObjetJavaForm) form;

    if (formulaire.isNouveauFormulaire()) {
      ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
      Libelle  libelle = null;

      try {
        libelle = modele.getServiceLibelle().get(formulaire.getString("nom"),
            GestionLibelle.getInstance().getLocaleParDefaut().toString());
        formulaire.setLibelleExistant(true);
        genererReponseAvecValeur(libelle.getTexteLibelle(), false, response);
      } catch (Exception e) {
        formulaire.setLibelleExistant(false);
      }
    }

    // Aucune vue n'est retournÃ©e.
    return mapping.findForward(null);
  }

  public ActionForward traiterDetailPresentation(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    ObjetJavaForm formulaire = (ObjetJavaForm) form;

    if (request.getParameter("type") != null) {
      formulaire.set("type", request.getParameter("type"));
    }

    formulaire.initialiserAttributsObligatoire(new String[] {"ordrePresentation"});
    String type = formulaire.getString("type");

    return mapping.findForward(type.equals("SC") || type.equals("SE") || type.equals("ON")
        ? "ajax_detail_presentation" : null);
  }

  // On peut populer un attribut du form si il se nomme correctement sans code supplémentaire.
  public ActionForward changerApplication(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    return null;
  }
}