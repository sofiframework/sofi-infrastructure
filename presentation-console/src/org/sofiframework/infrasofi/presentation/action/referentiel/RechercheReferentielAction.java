/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.referentiel;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.filtre.FiltreObjetJava;
import org.sofiframework.infrasofi.modele.service.ServiceApplication;
import org.sofiframework.infrasofi.modele.service.ServiceReferentiel;
import org.sofiframework.infrasofi.presentation.action.BaseRechercheAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireString;

public class RechercheReferentielAction extends BaseRechercheAction {

  private static final Log log = LogFactory
      .getLog(RechercheReferentielAction.class);

  private static final String ATTRIBUT_STRUCTURE = "structureReferentiel";
  private static final String ATTRIBUT_FILTRE = "filtreRecherche";
  public static final String ATTRIBUT_SELECTION = "selectionStructure";

  @Override
  public ObjetTransfert construireNouveauFiltre() {
    return new FiltreObjetJava();
  }

  @Override
  public void effectuerRecherche(ListeNavigation liste, BaseForm formulaire,
      ModeleInfrastructure modele, HttpServletRequest request,
      HttpServletResponse response) throws ModeleException {
    liste.ajouterTriInitial("nom", true);
    modele.getServiceReferentiel().getListeObjetJavaPilotage(liste);
  }

  @Override
  public String getNomListeDansSession() {
    return "listeReferentiel";
  }

  /**
   * Action de liste pour la liste de valeurs ajax d'objets java.
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   */
  public ActionForward rechercherParent(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) {

    ObjetJavaForm formulaire = (ObjetJavaForm) request.getSession()
        .getAttribute("objetJavaForm");
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    ServiceReferentiel service = modele.getServiceReferentiel();
    ListeNavigation liste = appliquerListeNavigation(request);
    FiltreObjetJava filtre = (FiltreObjetJava) request.getSession()
        .getAttribute(ATTRIBUT_FILTRE);

    if (filtre == null) {
      filtre = new FiltreObjetJava();
      this.setAttributTemporairePourAction(ATTRIBUT_FILTRE, filtre, request);
    }

    String paramSeq = request.getParameter("paramSeqObjetSecurisable");
    String paramNom = request.getParameter("paramNom");
    String paramTypeObjetParent = request.getParameter("paramTypeObjetParent");

    Boolean parametrePresent = !UtilitaireString.isVide(paramSeq)
        || !UtilitaireString.isVide(paramNom)
        || !UtilitaireString.isVide(paramTypeObjetParent);

    if (parametrePresent || isInitialiserListeValeur(request)) {
      if (!UtilitaireString.isVide(paramSeq)) {
        filtre.setId(new Long(paramSeq));
      }
      filtre.setNom(paramNom);
      filtre.setType(paramTypeObjetParent);
    }

    filtre.setCodeApplication(formulaire.getString("codeApplication"));
    liste.setObjetFiltre(filtre);
    liste.ajouterTriInitial("id", true);
    liste = service.getListeObjetJava(liste);

    this.setAttributTemporaireListeNavigationPourAction(
        getNomListeDansSession(), liste, request);

    return genererReponseListeValeurAjax("ajax_liste_parent", new String[] {
        "id", "nom" },
        "infra_sofi.erreur.gestion_referentiel.nomParent.inexistant", liste,
        mapping, form, request, response);
  }

  public ActionForward structure(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ServiceApplication service = getModeleInfrastructure(request)
        .getServiceApplication();
    fixerApplication(mapping, form, request, response);
    String codeApplication = getApplicationSelectionne(request);
    String codeFacette = request.getParameter("codeFacette");

    if (!UtilitaireString.isVide(codeApplication)) {
      Application application = service.getApplication(codeApplication, codeFacette);
      fixerFacetteApplication(request);
      List listeApplication = new ArrayList();
      listeApplication.add(application);

      String idObjet = null;

      if (request.getParameter("seqObjetSecurisable") != null) {
        idObjet = request.getParameter("seqObjetSecurisable");
      }

      if (request.getParameter("id") != null) {
        idObjet = request.getParameter("id");
      }

      request.getSession().setAttribute(ATTRIBUT_SELECTION, idObjet);
      request.setAttribute(ATTRIBUT_STRUCTURE, listeApplication);
    }

    return mapping.findForward("page_structure");
  }
}
