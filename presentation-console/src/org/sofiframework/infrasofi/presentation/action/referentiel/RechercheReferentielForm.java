/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.referentiel;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.presentation.action.RechercheApplicativeForm;

public class RechercheReferentielForm extends RechercheApplicativeForm{

  private static final long serialVersionUID = -2667096622723749795L;

  /**
   * L'identifiant
   */
  private String nom;

  /**
   * Le type d'objet référentiel
   * e.g. SE (Service), ON (Onglet), etc.
   */
  private String type;

  /**
   * Le statut d'activation, A (Actif) ou I (Inactif).
   */
  private String codeStatut;

  /**
   * La clé de l'aide contextuelle.
   */
  private String cleAide;

  private String versionLivraison;

  /**
   * Le filtre de rechercher sélectionné.
   */
  private String filtreOperateur;

  public RechercheReferentielForm() {
    super();
  }

  @Override
  public void validerFormulaire(HttpServletRequest request) {

  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getNom() {
    return nom;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  public String getCodeStatut() {
    return codeStatut;
  }

  public void setCleAide(String cleAide) {
    this.cleAide = cleAide;
  }

  public String getCleAide() {
    return cleAide;
  }

  public void setVersionLivraison(String versionLivraison) {
    this.versionLivraison = versionLivraison;
  }

  public String getVersionLivraison() {
    return versionLivraison;
  }

  public void setFiltreOperateur(String filtreOperateur) {
    this.filtreOperateur = filtreOperateur;
  }

  public String getFiltreOperateur() {
    return filtreOperateur;
  }

}
