/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.application;

import java.util.Date;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.entite.PageApplication;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireString;
/**
 * Classe d'action qui gère les accès à la partie de l'unité de traitement qui
 * gère les pages de sécurité.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 * @version 1.0
 */
public class PageSecuriteAction extends InfrastructureAction  {

  /**
   * Cette méthode s'exécute lorsque l'utilisateur accède au détail d'une page
   * de sécurité.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  throws ServletException {
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    ApplicationForm formulaire = (ApplicationForm) form;

    // Configurer la langue
    String langue = request.getParameter("codeLangue");
    if (UtilitaireString.isVide(langue)) {
      langue = (String) request.getSession().getAttribute("APPLICATION_LANGUE");
      request.getSession().removeAttribute("APPLICATION_LANGUE");
      if (UtilitaireString.isVide(langue)) {
        langue = "FR";
      }
    }

    // Rechercher le formulaire à modifier
    PageApplicationForm formulairePage = null;
    if (formulaire.getListePageSecurite() != null && formulaire.getListePageSecurite().size() > 0) {
      Iterator iterateur = formulaire.getListePageSecurite().iterator();
      while (iterateur.hasNext() && formulairePage == null)  {
        PageApplicationForm formulairePageSecurite = (PageApplicationForm) iterateur.next();
        if (formulairePageSecurite.getCodeLangue().equals(langue)) {
          formulairePage = formulairePageSecurite;
        }
      }
    }

    if (formulairePage == null) {
      formulaire.ajouterLigne("listePageSecurite", new PageApplication(), request);
      formulairePage = (PageApplicationForm) formulaire.getListePageSecurite().get(formulaire.getListePageSecurite().size() - 1);
      formulairePage.setCodeLangue(langue);
      // Dans le cas où il existe déjà une page d'une autre langue, il faut faire une petite gestion
      if (formulaire.getListePageSecurite().size() > 1) {
        PageApplicationForm formulaireDeBase = (PageApplicationForm) formulaire.getListePageSecurite().get(0);
        formulairePage.setNouveauFormulaire(false);
        formulairePage.setClePage(formulaireDeBase.getClePage());
      }
    }

    // Faire la gestion des modes d'affichage des langues
    if (langue.equals("FR")) {
      request.setAttribute("styleFrancais", "langueSelectionne");
      request.setAttribute("styleAnglais", "langue");
    } else {
      request.setAttribute("styleFrancais", "langue");
      request.setAttribute("styleAnglais", "langueSelectionne");
    }

    // Mettre le formulaire de page dans le formulaire principal
    formulaire.setTextePage(formulairePage.getTextePage());
    formulaire.setFormulairePageEnModification(formulairePage);

    if (this.isAjax(request)) {
      // Ajouter une valeur dans la réponse pour un identifiant.
      ajouterValeurDansReponse("formulairePageEnModification.codeLangue", langue, request, response);
      ajouterValeurDansReponse("formulairePageEnModification.titrePage", formulairePage.getTitrePage(), request, response);
      ajouterValeurDansReponse("textePage", UtilitaireString.convertirEnHtml(formulairePage.getTextePage()), request, response);
      ajouterValeurDansReponse("formulairePageEnModification.creePar", formulairePage.getCreePar(), request, response);
      ajouterValeurDansReponse("formulairePageEnModification.dateCreation", formulairePage.getDateCreation(), request, response);
      ajouterValeurDansReponse("formulairePageEnModification.modifiePar", formulairePage.getModifiePar(), request, response);
      ajouterValeurDansReponse("formulairePageEnModification.dateModification", formulairePage.getDateModification(), request, response);
      genererReponseXMLAvecValeurs(request, response);

      return mapping.findForward(null);
    }

    return mapping.findForward("page");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur enregistre une page de
   * sécurité.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  throws ServletException {
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    ApplicationForm formulaire = (ApplicationForm) form;
    Utilisateur utilisateur = UtilitaireControleur.getUtilisateur(request.getSession());

    // Récupérer l'objet de page d'application à sauvegarder
    PageApplication pageApplication = (PageApplication) formulaire.getFormulairePageEnModification().getObjetTransfert();
    pageApplication.setTextePage(formulaire.getTextePage());

    try {
      if (UtilitaireString.isVide(formulaire.getFormulairePageEnModification().getCreePar())) {
        // Effectuer l'ajout
        pageApplication.setCreePar(utilisateur.getCodeUtilisateur());
        pageApplication.setDateCreation(new Date(UtilitaireDate.getDateJourAvecHeure().getTime()));
        modele.getServiceReferentiel().enregistrerPageApplication(pageApplication);
        Application application = (Application) formulaire.getObjetTransfert();
        application.setClePageSecurite(pageApplication.getClePage());
        modele.getServiceApplication().enregistrer(application, null);
      } else {
        // Effectuer la modification
        pageApplication.setModifiePar(utilisateur.getCodeUtilisateur());
        pageApplication.setDateModification(new Date(UtilitaireDate.getDateJourAvecHeure().getTime()));
        modele.getServiceReferentiel().enregistrerPageApplication(pageApplication);
      }
      this.commit(request, formulaire);
      // Reprendre l'application et populer le formulaire afin d'avoir des données fraiches
      Application application = modele.getServiceApplication().getApplication(formulaire.getCodeApplication());
      formulaire.initialiserFormulaire(request, mapping, new Application(), true);
      formulaire.populerFormulaire(application, request);
      request.getSession().setAttribute("APPLICATION_LANGUE", pageApplication.getCodeLangue());
    } catch (ModeleException e) {
      formulaire.ajouterMessageErreur(e, request);
    }
    return mapping.findForward("afficher");
  }
}