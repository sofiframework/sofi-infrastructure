package org.sofiframework.infrasofi.presentation.action.application;

import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.cache.UtilitaireCache;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.filtre.FiltreApplication;
import org.sofiframework.infrasofi.presentation.action.BaseDetailAction;
import org.sofiframework.objetstransfert.ObjetCleValeur;
import org.sofiframework.utilitaire.UtilitaireHttp;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Classe d'action qui gère les accès à l'unité de traitement qui fait la gestion
 * des caches d'une application dans l'infrastructure de SOFI.
 * <p>
 * @author Jean-François Brassard, Nurun inc.
 * @version 1.0
 */
public class CacheAction extends BaseDetailAction {

  private static final Log log = LogFactory.getLog(CacheAction.class);

  /**
   * Cette méthode s'exécute lors de l'accès à l'unité de traitement.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    // Initialiser le formulaire
    RechercheApplicationForm formulaire = (RechercheApplicationForm) form;
    formulaire.initialiserFormulaire(request, mapping, new FiltreApplication(),
        true);

    return mapping.findForward("afficher");
  }


  /**
   * Action de redirection qui n'effectue aucun traitement sauf faire la redirection vers la page de détail d'une application.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  @Override
  public ActionForward afficher(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ApplicationForm formulaire = (ApplicationForm) form;

    if (!formulaire.isNouveauFormulaire()) {
      ArrayList listeCaches = null;

      try {
        listeCaches = this.getListeCachePourApplication(formulaire, request);
      } catch (Exception e) {
        formulaire.ajouterMessageAvertissement("infra_sofi.avertissement.gestion_application.gestion_cache_avertissement",
            request);
      }

      if (listeCaches != null && listeCaches.size() > 0) {
        ObjetCleValeur cleValeur = (ObjetCleValeur) listeCaches.get(0);

        if (cleValeur.getObjetCle().toString().indexOf("<!DOCTYPE") != -1) {
          request.getSession().setAttribute("listeCachesApplication", null);
        } else {
          request.getSession().setAttribute("listeCachesApplication",
              listeCaches);
        }
      } else {
        request.getSession().setAttribute("listeCachesApplication", null);
      }
    }

    return mapping.findForward("page");
  }


  /**
   * Exécute l'initialisation de tous les libellés d'une application.
   */
  public ActionForward initialiserTousLibelles(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ApplicationForm formulaire = (ApplicationForm) form;

    Application application = (Application)formulaire.getObjetTransfert();

    String[] listeNoeudCache = getUrlApplication(
        formulaire.getCodeApplication(), request);

    try {
      for (int i = 0; i < listeNoeudCache.length; i++) {
        StringBuffer adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[i]);
        adresseInitialisation.append(UtilitaireCache.getUrlMaj(formulaire.getCodeApplication()) + "LIBELLE");
        adresseInitialisation.append(this.ajouterContenuSecuriteUrl(application));

        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString());

        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

      }

      formulaire.ajouterMessageInformatif("infra_sofi.information.gestion.application.libelles.succes",
          new String[] { formulaire.getNom() }, request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          e.getMessage(), request);
    }


    return mapping.findForward("afficher");
  }

  /**
   * Exécute l'initialisation de tous les messages d'une application.
   */
  public ActionForward initialiserTousMessages(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ApplicationForm formulaire = (ApplicationForm) form;

    Application application = (Application)formulaire.getObjetTransfert();

    String[] listeNoeudCache = getUrlApplication(
        formulaire.getCodeApplication(), request);

    try {
      for (int i = 0; i < listeNoeudCache.length; i++) {
        StringBuffer adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[i]);
        adresseInitialisation.append(UtilitaireCache.getUrlMaj(formulaire.getCodeApplication()) + "MESSAGE");
        adresseInitialisation.append(this.ajouterContenuSecuriteUrl(application));

        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString());

        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

      }

      formulaire.ajouterMessageInformatif("infra_sofi.information.gestion.application.message.succes",
          new String[] { formulaire.getNom() }, request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          e.getMessage(), request);
    }


    return mapping.findForward("afficher");
  }

  /**
   * Réinitialiser un objet cache.
   * @return la page originale avec une erreur en cas de problème
   * @param response la réponse HTTP
   * @param request la requête HTTP
   * @param form le formulaire
   * @param mapping le répertoire de lien struts de l'action
   * @throws javax.servlet.ServletException
   */
  public ActionForward rechargerCache(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    StringBuffer adresseInitialisation = null;

    ApplicationForm formulaire = (ApplicationForm) form;

    Application application = (Application)formulaire.getObjetTransfert();

    String nomCache = request.getParameter("nomCache");

    String[] listeNoeudCache = getUrlApplication(
        formulaire.getCodeApplication(), request);

    try {
      for (int i = 0; i < listeNoeudCache.length; i++) {
        adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[i]);

        String url = UtilitaireCache.getUrlRecharger(formulaire
            .getCodeApplication()) + nomCache;
        adresseInitialisation.append(url);
        adresseInitialisation.append(this.ajouterContenuSecuriteUrl(application));


        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString());

        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

      }

      formulaire.ajouterMessageInformatif(
          "infra_sofi.information.gestion.application.cache.succes",
          new String[] { nomCache, formulaire.getNom() }, request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          e.getMessage() + " " + adresseInitialisation, request);
    }

    return mapping.findForward(null);

  }

  /**
   * Exécute l'initialisation de tous les aides en ligne d'une application.
   */
  public ActionForward initialiserTousAides(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ApplicationForm formulaire = (ApplicationForm) form;

    Application application = (Application)formulaire.getObjetTransfert();

    String[] listeNoeudCache = getUrlApplication(
        formulaire.getCodeApplication(), request);

    try {
      for (int i = 0; i < listeNoeudCache.length; i++) {
        StringBuffer adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[i]);

        String url = UtilitaireCache.getUrlMaj(formulaire.getCodeApplication()) + "AIDE";

        adresseInitialisation.append(url);
        adresseInitialisation.append(this.ajouterContenuSecuriteUrl(application));

        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString());

        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

      }

      formulaire.ajouterMessageInformatif("infra_sofi.information.gestion.application.aide.ligne.succes",
          new String[] { formulaire.getNom() }, request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          e.getMessage(), request);
    }

    return mapping.findForward("afficher");
  }

  public ArrayList getListeCachePourApplication(
      ApplicationForm formulaire, HttpServletRequest request)
          throws Exception {
    ArrayList listeCaches = new ArrayList();

    Application application = (Application)formulaire.getObjetTransfert();

    StringBuffer adresseInitialisation = new StringBuffer();
    String urlCaching = getUrlApplication(
        formulaire.getCodeApplication(), formulaire.getCodeFacette(), request)[0];

    adresseInitialisation.append(urlCaching);

    if (!adresseInitialisation.toString().equals("null")) {

      String url = UtilitaireCache.getUrlListeNomCache(formulaire.getCodeApplication()) ;
      adresseInitialisation.append(url);
      adresseInitialisation.append(this.ajouterContenuSecuriteUrl(application));

      String contenuRetour = null;

      try {

        contenuRetour = UtilitaireHttp.getContenuHtml(adresseInitialisation.toString());

        // Enlever les caractères \r et \n
        contenuRetour = UtilitaireString.remplacerTous(contenuRetour, "\r", "");
        contenuRetour = UtilitaireString.remplacerTous(contenuRetour, "\n", "");
      } catch (Exception e) {
      }

      StringTokenizer st = new StringTokenizer(contenuRetour, "|");

      while (st.hasMoreElements()) {
        String cache = (String) st.nextElement();

        if (!"aideEnLigne".equals(cache) && cache.indexOf("[") == -1) {
          listeCaches.add(new ObjetCleValeur(cache, cache));
        }
      }
    } else {
      listeCaches = null;
    }

    return listeCaches;
  }
}
