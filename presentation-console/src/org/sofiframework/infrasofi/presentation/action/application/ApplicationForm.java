/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.application;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.entite.PageApplication;
import org.sofiframework.presentation.struts.form.BaseMultiLigneForm;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Formulaire représentant le détail d'une application.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 */
public class ApplicationForm extends BaseMultiLigneForm {

  private static final long serialVersionUID = -823856009472786213L;

  /** l'identifiant unique de l'application */
  private String codeApplication;

  /** le nom de l'application */
  private String nom;

  /** un texte qui décrit l'application */
  private String description;

  /** la date à laquelle l'application commence à être active */
  private String dateDebutActivite;

  /** la date à laquelle l'application cesse d'être active */
  private String dateFinActivite;

  /** le statut qui indique si l'application est active ou pas */
  private String statut;

  /** le code utilisateur de l'utilisateur qui est administrateur de l'application */
  private String codeUtilisateurAdministrateur;

  /** le mot de passe de l'utilisateur qui est administrateur de l'application */
  private String motPasseAdministrateur;

  /** la confirmation du mot de passe de l'administrateur de l'application */
  private String confirmationMotPasseAdministrateur;

  /** l'adresse URL à appeler afin d'accéder au site */
  private String adresseSite;

  /** la liste des pages d'application qui représentent les pages d'erreur de l'application */
  private ArrayList listePageErreur;

  /** la liste des pages d'application qui représentent les pages de maintenance de l'application */
  private ArrayList listePageMaintenance;

  /** la liste des pages d'application qui représentent les pages de sécurité de l'application */
  private ArrayList listePageSecurite;

  /** le formulaire de la page de détail en modification */
  private PageApplicationForm formulairePageEnModification;

  /** le texte d'une page d'application pour l'application */
  private String textePage;

  /** le nom de la cache a réinitialiser */
  private String nomCacheReinitialisee;

  /** la version dont le système est en cours de livraison ou livré **/
  private String versionLivraison;

  /** L'acronyme du nom d'application **/
  private String acronyme;

  /** Est-ce une application commun **/
  private Boolean commun;

  /** Le code de la facette de l'application */
  private String codeFacette;

  /** l'adresse URL à appeler afin d'accéder au application pour rafraichir la cache */
  private String adresseCache;

  //trace
  private String creePar;
  private String dateCreation;
  private String modifiePar;
  private String dateModification;

  /** Constructeur par défaut */
  public ApplicationForm() {
    this.ajouterCorrespondanceFormulaireEnfants(PageApplicationForm.class, PageApplication.class);
    this.ajouterFormatterDateHeureSeconde("dateCreation");
    this.ajouterFormatterDateHeureSeconde("dateModification");
    this.setFormulaireIdentifiant("codeApplication");
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un <code>submit</code>.
   * <p>
   * @param request la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
    // La date de début doit être présente si la date de fin est présente
    if (UtilitaireString.isVide(this.getDateDebutActivite()) &&
        !UtilitaireString.isVide(this.getDateFinActivite())) {
      this.ajouterMessageErreur("infra_sofi.erreur.gestion.application.date.debut.obligatoire.si.date.fin",
          "dateDebutActivite", request);
    }

    // La date de fin d'activation (si présente) doit être supérieure à la date de début
    if (!this.isFormulaireEnErreur()) {
      if (!UtilitaireString.isVide(this.getDateFinActivite()) &&
          !UtilitaireString.isVide(this.getDateDebutActivite())) {
        Date debut = UtilitaireDate.convertir_AAAA_MM_JJ_En_UtilDate(this.getDateDebutActivite());
        Date fin = UtilitaireDate.convertir_AAAA_MM_JJ_En_UtilDate(this.getDateFinActivite());

        if (fin.before(debut)) {
          this.ajouterMessageErreur("infra_sofi.erreur.gestion.application.date.debut.superieur.date.fin",
              "dateFinActivite", request);
        }
      }
    }

    // Valider que l'application à ajouter ne possède pas le même code d'application qu'une déjà existante
    if (!this.isFormulaireEnErreur() && this.isNouveauFormulaire()) {
      ModeleInfrastructure modele = (ModeleInfrastructure) UtilitaireControleur.getModele(request);
      Application application = modele.getServiceApplication().getApplication(this.getCodeApplication().toUpperCase());

      if (application != null) {
        this.ajouterMessageErreur("infra_sofi.erreur.gestion.application.application.deja.existante",
            "codeApplication", request);
      }
    }

    if (this.isNouveauFormulaire() && this.isFormulaireEnErreur()) {
      this.setNouveauFormulaire(true);
    }
  }

  /**
   * Obtenir l'identifiant unique de l'application.
   * <p>
   * @return l'identifiant unique de l'application
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique de l'application.
   * <p>
   * @param codeApplication l'identifiant unique de l'application
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Obtenir le nom de l'application.
   * <p>
   * @return le nom de l'application
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom de l'application.
   * <p>
   * @param nom le nom de l'application
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir un texte qui décrit l'application.
   * <p>
   * @return un texte qui décrit l'application
   */
  public String getDescription() {
    return description;
  }

  /**
   * Fixer un texte qui décrit l'application.
   * <p>
   * @param description un texte qui décrit l'application
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Obtenir la date à laquelle l'application commence à être active.
   * <p>
   * @return la date à laquelle l'application commence à être active
   */
  public String getDateDebutActivite() {
    return dateDebutActivite;
  }

  /**
   * Fixer la date à laquelle l'application commence à être active.
   * <p>
   * @param dateDebutActivite la date à laquelle l'application commence à être active
   */
  public void setDateDebutActivite(String dateDebutActivite) {
    this.dateDebutActivite = dateDebutActivite;
  }

  /**
   * Obtenir la date à laquelle l'application cesse d'être active.
   * <p>
   * @return la date à laquelle l'application cesse d'être active
   */
  public String getDateFinActivite() {
    return dateFinActivite;
  }

  /**
   * Fixer la date à laquelle l'application cesse d'être active.
   * <p>
   * @param dateFinActivite la date à laquelle l'application cesse d'être active
   */
  public void setDateFinActivite(String dateFinActivite) {
    this.dateFinActivite = dateFinActivite;
  }

  /**
   * Obtenir le statut qui indique si l'application est active ou pas.
   * <p>
   * @return le statut qui indique si l'application est active ou pas
   */
  public String getStatut() {
    return statut;
  }

  /**
   * Fixer le statut qui indique si l'application est active ou pas.
   * <p>
   * @param statut le statut qui indique si l'application est active ou pas
   */
  public void setStatut(String statut) {
    this.statut = statut;
  }

  /**
   * Obtenir le code utilisateur de l'utilisateur qui est administrateur de l'application.
   * <p>
   * @return le code utilisateur de l'utilisateur qui est administrateur de l'application
   */
  public String getCodeUtilisateurAdministrateur() {
    return codeUtilisateurAdministrateur;
  }

  /**
   * Fixer le code utilisateur de l'utilisateur qui est administrateur de l'application.
   * <p>
   * @param codeUtilisateurAdministrateur le code utilisateur de l'utilisateur qui est administrateur de l'application
   */
  public void setCodeUtilisateurAdministrateur(
      String codeUtilisateurAdministrateur) {
    this.codeUtilisateurAdministrateur = codeUtilisateurAdministrateur;
  }

  /**
   * Obtenir le mot de passe de l'utilisateur qui est administrateur de l'application.
   * <p>
   * @return le mot de passe de l'utilisateur qui est administrateur de l'application
   */
  public String getMotPasseAdministrateur() {
    return motPasseAdministrateur;
  }

  /**
   * Fixer le mot de passe de l'utilisateur qui est administrateur de l'application.
   * <p>
   * @param motPasseAdministrateur le mot de passe de l'utilisateur qui est administrateur de l'application
   */
  public void setMotPasseAdministrateur(String motPasseAdministrateur) {
    this.motPasseAdministrateur = motPasseAdministrateur;
  }

  /**
   * Obtenir la confirmation du mot de passe de l'administrateur de l'application.
   * <p>
   * @return la confirmation du mot de passe de l'administrateur de l'application
   */
  public String getConfirmationMotPasseAdministrateur() {
    return confirmationMotPasseAdministrateur;
  }

  /**
   * Fixer la confirmation du mot de passe de l'administrateur de l'application.
   * <p>
   * @param confirmationMotPasseAdministrateur la confirmation du mot de passe de l'administrateur de l'application
   */
  public void setConfirmationMotPasseAdministrateur(
      String confirmationMotPasseAdministrateur) {
    this.confirmationMotPasseAdministrateur = confirmationMotPasseAdministrateur;
  }

  /**
   * Obtenir la liste des pages d'application qui représentent les pages d'erreur de l'application.
   * <p>
   * @return la liste des pages d'application qui représentent les pages d'erreur de l'application
   */
  public ArrayList getListePageErreur() {
    return listePageErreur;
  }

  /**
   * Fixer la liste des pages d'application qui représentent les pages d'erreur de l'application.
   * <p>
   * @param listePageErreur la liste des pages d'application qui représentent les pages d'erreur de l'application
   */
  public void setListePageErreur(ArrayList listePageErreur) {
    this.listePageErreur = listePageErreur;
  }

  /**
   * Obtenir la liste des pages d'application qui représentent les pages de maintenance de l'application.
   * <p>
   * @return la liste des pages d'application qui représentent les pages de maintenance de l'application
   */
  public ArrayList getListePageMaintenance() {
    return listePageMaintenance;
  }

  /**
   * Fixer la liste des pages d'application qui représentent les pages de maintenance de l'application.
   * <p>
   * @param listePageMaintenance la liste des pages d'application qui représentent les pages de maintenance de l'application
   */
  public void setListePageMaintenance(ArrayList listePageMaintenance) {
    this.listePageMaintenance = listePageMaintenance;
  }

  /**
   * Obtenir la liste des pages d'application qui représentent les pages de sécurité de l'application.
   * <p>
   * @return la liste des pages d'application qui représentent les pages de sécurité de l'application
   */
  public ArrayList getListePageSecurite() {
    return listePageSecurite;
  }

  /**
   * Fixer la liste des pages d'application qui représentent les pages de sécurité de l'application.
   * <p>
   * @param listePageSecurite la liste des pages d'application qui représentent les pages de sécurité de l'application
   */
  public void setListePageSecurite(ArrayList listePageSecurite) {
    this.listePageSecurite = listePageSecurite;
  }

  /**
   * Obtenir l'adresse URL à appeler afin d'accéder au site.
   * <p>
   * @return l'adresse URL à appeler afin d'accéder au site
   */
  public String getAdresseSite() {
    return adresseSite;
  }

  /**
   * Fixer l'adresse URL à appeler afin d'accéder au site.
   * <p>
   * @param adresseSite l'adresse URL à appeler afin d'accéder au site
   */
  public void setAdresseSite(String adresseSite) {
    this.adresseSite = adresseSite;
  }

  /**
   * Obtenir le formulaire de la page de détail en modification.
   * <p>
   * @return le formulaire de la page de détail en modification
   */
  public PageApplicationForm getFormulairePageEnModification() {
    return formulairePageEnModification;
  }

  /**
   * Fixer le formulaire de la page de détail en modification.
   * <p>
   * @param formulairePageEnModification le formulaire de la page de détail en modification
   */
  public void setFormulairePageEnModification(
      PageApplicationForm formulairePageEnModification) {
    this.formulairePageEnModification = formulairePageEnModification;
  }

  /**
   * Obtenir le texte d'une page d'application pour l'application.
   * <p>
   * Cet attribut est placé là pour palier au fait que la balise texteHTML n'existe
   * pas en format <code>Nested</code> ainsi il est impossible de l'afficher lorsqu'elle
   * est dans un formulaire <code>Nested</code>. Pour cette raison nous plaçons
   * l'attribut dans le formulaire parent.
   * <p>
   * @return le texte d'une page d'application pour l'application
   */
  public String getTextePage() {
    return textePage;
  }

  /**
   * Fixer le texte d'une page d'application pour l'application.
   * <p>
   * Cet attribut est placé là pour palier au fait que la balise texteHTML n'existe
   * pas en format <code>Nested</code> ainsi il est impossible de l'afficher lorsqu'elle
   * est dans un formulaire <code>Nested</code>. Pour cette raison nous plaçons
   * l'attribut dans le formulaire parent.
   * <p>
   * @param textePage le texte d'une page d'application pour l'application
   */
  public void setTextePage(String textePage) {
    this.textePage = textePage;
  }

  public void setNomCacheReinitialisee(String nomCacheReinitialisee) {
    this.nomCacheReinitialisee = nomCacheReinitialisee;
  }

  public String getNomCacheReinitialisee() {
    return nomCacheReinitialisee;
  }

  public void setVersionLivraison(String versionLivraison) {
    this.versionLivraison = versionLivraison;
  }

  public String getVersionLivraison() {
    return versionLivraison;
  }

  public String getCreePar() {
    return creePar;
  }

  public void setCreePar(String creePar) {
    this.creePar = creePar;
  }

  public String getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(String dateCreation) {
    this.dateCreation = dateCreation;
  }

  public String getModifiePar() {
    return modifiePar;
  }

  public void setModifiePar(String modifiePar) {
    this.modifiePar = modifiePar;
  }

  public String getDateModification() {
    return dateModification;
  }

  public void setDateModification(String dateModification) {
    this.dateModification = dateModification;
  }

  public void setAcronyme(String acronyme) {
    this.acronyme = acronyme;
  }

  public String getAcronyme() {
    return acronyme;
  }

  public void setCommun(Boolean commun) {
    this.commun = commun;
  }

  public Boolean getCommun() {
    return commun;
  }

  public void setCodeFacette(String codeFacette) {
    this.codeFacette = codeFacette;
  }

  public String getCodeFacette() {
    return codeFacette;
  }

  public String getAdresseCache() {
    return adresseCache;
  }

  public void setAdresseCache(String adresseCache) {
    this.adresseCache = adresseCache;
  }
}
