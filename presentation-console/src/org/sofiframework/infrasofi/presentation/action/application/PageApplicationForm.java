/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.application;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.presentation.action.FormulaireTrace;
import org.sofiframework.utilitaire.UtilitaireString;


/**
 * Formulaire représentant le détail d'une page d'application.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 * @version 1.0
 */
public class PageApplicationForm extends FormulaireTrace {

  private static final long serialVersionUID = 3473458444660594030L;

  /** l'identifiant de la page d'application */
  private String clePage;

  /** l'identifiant unique de la langue pour lequel la page existe */
  private String codeLangue;

  /** le titre de la page d'application */
  private String titrePage;

  /** le texte représentant la page d'application */
  private String textePage;

  /** Constructeur par défaut */
  public PageApplicationForm() {
  }

  /**
   * Obtenir l'identifiant de la page d'application.
   * <p>
   * @return l'identifiant de la page d'application
   */
  public String getClePage() {
    return clePage;
  }

  /**
   * Fixer l'identifiant de la page d'application.
   * <p>
   * @param clePage l'identifiant de la page d'application
   */
  public void setClePage(String clePage) {
    this.clePage = clePage;
  }

  /**
   * Obtenir l'identifiant unique de la langue pour lequel la page existe.
   * <p>
   * @return l'identifiant unique de la langue pour lequel la page existe
   */
  public String getCodeLangue() {
    return codeLangue;
  }

  /**
   * Fixer l'identifiant unique de la langue pour lequel la page existe.
   * <p>
   * @param codeLangue l'identifiant unique de la langue pour lequel la page existe
   */
  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  /**
   * Obtenir le titre de la page d'application.
   * <p>
   * @return le titre de la page d'application
   */
  public String getTitrePage() {
    return titrePage;
  }

  /**
   * Fixer le titre de la page d'application.
   * <p>
   * @param titrePage le titre de la page d'application
   */
  public void setTitrePage(String titrePage) {
    this.titrePage = titrePage;
  }

  /**
   * Obtenir le texte représentant la page d'application.
   * <p>
   * @return le texte représentant la page d'application
   */
  public String getTextePage() {
    return textePage;
  }

  /**
   * Fixer le texte représentant la page d'application.
   * <p>
   * @param le texte représentant la page d'application
   */
  public void setTextePage(String textePage) {
    this.textePage = textePage;
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un <code>submit</code>.
   * <p>
   * @param request la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
    if (this.isFormulaireEnErreur() &&
        UtilitaireString.isVide(this.getClePage())) {
      this.setNouveauFormulaire(true);
    }
  }
}
