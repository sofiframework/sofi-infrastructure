/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.application;

import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.cache.UtilitaireCache;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetCleValeur;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.sofiframework.utilitaire.UtilitaireHttp;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Classe d'action qui gère les accès à l'unité de traitement qui fait la
 * gestion des applications disponibles dans l'infrastructure de SOFI.
 * <p>
 * 
 * @author jfbrassard
 * @version 3.0
 */
public class ApplicationAction extends InfrastructureAction {

  private static final Log log = LogFactory.getLog(ApplicationAction.class);

  /**
   * Cette méthode s'exécute lorsque l'utilisateur accède au détail d'une
   * application.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward creer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ApplicationForm formulaire = (ApplicationForm) form;
    formulaire.initialiserFormulaire(request, mapping, new Application(), true);
    // Mettre la date du jour comme date d'activité par défaut
    formulaire.setDateDebutActivite(UtilitaireDate.getDateJourEn_AAAA_MM_JJ());
    formulaire
    .ajouterMessageInformatif(
        "infra_sofi.information.gestion_application.confirmation_ajout.systeme",
        request);
    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur accède au détail d'une
   * application.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    ApplicationForm formulaire = (ApplicationForm) form;

    // Récupérer l'application et en populer le formulaire
    Application application = modele.getServiceApplication().getApplication(
        formulaire.getCodeApplication());
    formulaire.initialiserFormulaire(request, mapping, true);
    formulaire.populerFormulaire(application, request);

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur enregistre une application.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    ApplicationForm formulaire = (ApplicationForm) form;
    Application application = (Application) formulaire.getObjetTransfert();

    // Effectuer l'ajout ou la modification
    try {
      modele.getServiceApplication().enregistrer(application,
          (Application) formulaire.getObjetTransfertOriginal());
      this.commit(request, formulaire);

      // Mettre à jour la liste des applications accessibles à l'utilisateur.
      this.configurerListeApplicationUtilisateur(request);

      // Populer le formulaire en traitement.
      formulaire.populerFormulaire(application, request);
    } catch (ModeleException e) {
      this.rollback(request);
      formulaire.ajouterMessageErreur(e, request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Action de redirection qui n'effectue aucun traitement sauf faire la
   * redirection vers la page de détail d'une application.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ApplicationForm formulaire = (ApplicationForm) form;

    // Exclure des onglets qui ne sont pas encore disponible dans SOFI.
    exclureComposantInterface(
        "infra_sofi.libelle.gestion.application.onglet.page.erreur", request);
    exclureComposantInterface(
        "infra_sofi.libelle.gestion.application.onglet.page.maintenance",
        request);
    exclureComposantInterface(
        "infra_sofi.libelle.gestion.application.onglet.page.securite", request);

    if (!formulaire.isNouveauFormulaire()) {
      ArrayList<ObjetCleValeur> listeCaches = null;

      try {
        listeCaches = this.getListeCachePourApplication(formulaire, request);
      } catch (Exception e) {
        e.printStackTrace();
        formulaire
        .ajouterMessageAvertissement(
            "infra_sofi.avertissement.gestion_application.gestion_cache_avertissement",
            request);
      }

      if (listeCaches != null && listeCaches.size() > 0) {
        request.getSession()
        .setAttribute("listeCachesApplication", listeCaches);
        // Afficher l'onglet
        this.inclureComposantInterface(
            "infra_sofi.libelle.gestion_application.onglet.cache", request);
      } else {
        request.getSession().setAttribute("listeCachesApplication", null);
        // Enlever l'onglet
        this.exclureComposantInterface(
            "infra_sofi.libelle.gestion_application.onglet.cache", request);
      }
    }

    return mapping.findForward("page");
  }

  /**
   * Action de redirection qui n'effectue aucun traitement sauf faire la
   * redirection vers la page de détail d'une page d'application.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward afficherMaintenance(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    return mapping.findForward("page_detail_page_application");
  }

  /**
   * Action de redirection qui n'effectue aucun traitement sauf faire la
   * redirection vers la page de détail d'une page d'application.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward afficherSecurite(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    return mapping.findForward("page_detail_page_application");
  }

  /**
   * Exécute l'initialisation de tous les libellés d'une application.
   */
  public ActionForward initialiserTousLibelles(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ApplicationForm formulaire = (ApplicationForm) form;

    String[] listeNoeudCache = getUrlApplication(
        formulaire.getCodeApplication(), request);

    try {
      for (int i = 0; i < listeNoeudCache.length; i++) {
        StringBuffer adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[i]);

        String url = UtilitaireCache.getUrlMaj(formulaire.getCodeApplication())
            + "LIBELLE";
        adresseInitialisation.append(url);

        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString()
                + this.ajouterContenuSecuriteUrl(formulaire));

        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

      }

      formulaire.ajouterMessageInformatif(
          "infra_sofi.information.gestion.application.libelles.succes",
          new String[] { formulaire.getNom() }, request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          "infra_sofi.erreur.gestion.application.communication"
              + e.getMessage(), request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Exécute l'initialisation de tous les messages d'une application.
   */
  public ActionForward initialiserTousMessages(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ApplicationForm formulaire = (ApplicationForm) form;

    String[] listeNoeudCache = getUrlApplication(
        formulaire.getCodeApplication(), request);

    try {
      for (int i = 0; i < listeNoeudCache.length; i++) {
        StringBuffer adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[i]);

        String url = UtilitaireCache.getUrlMaj(formulaire.getCodeApplication())
            + "MESSAGE";
        adresseInitialisation.append(url);



        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString()
                + this.ajouterContenuSecuriteUrl(formulaire));

        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

      }

      formulaire.ajouterMessageInformatif(
          "infra_sofi.information.gestion.application.message.succes",
          new String[] { formulaire.getNom() }, request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          "infra_sofi.erreur.gestion.application.communication"
              + e.getMessage(), request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Réinitialiser un objet cache.
   * 
   * @return la page originale avec une erreur en cas de problème
   * @param response
   *          la réponse HTTP
   * @param request
   *          la requête HTTP
   * @param form
   *          le formulaire
   * @param mapping
   *          le répertoire de lien struts de l'action
   * @throws javax.servlet.ServletException
   */
  public ActionForward rechargerCache(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {

    StringBuffer adresseInitialisation = null;

    ApplicationForm formulaire = (ApplicationForm) form;

    String nomCache = request.getParameter("nomCache");

    String[] listeNoeudCache = getUrlApplication(
        formulaire.getCodeApplication(), request);

    try {
      for (int i = 0; i < listeNoeudCache.length; i++) {
        adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[i]);

        String url = UtilitaireCache.getUrlRecharger(formulaire
            .getCodeApplication()) + nomCache;
        adresseInitialisation.append(url);

        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString()
                + this.ajouterContenuSecuriteUrl(formulaire));

        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

      }

      formulaire.ajouterMessageInformatif(
          "infra_sofi.information.gestion.application.cache.succes",
          new String[] { nomCache, formulaire.getNom() }, request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          "infra_sofi.erreur.gestion.application.communication"
              + e.getMessage() + adresseInitialisation, request);
    }

    return mapping.findForward(null);
  }

  /**
   * Exécute l'initialisation de tous les aides en ligne d'une application.
   */
  public ActionForward initialiserTousAides(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ApplicationForm formulaire = (ApplicationForm) form;

    String[] listeNoeudCache = getUrlApplication(
        formulaire.getCodeApplication(), request);

    try {
      for (int i = 0; i < listeNoeudCache.length; i++) {
        StringBuffer adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[i]);

        String url = UtilitaireCache.getUrlRecharger(formulaire
            .getCodeApplication()) + "AIDE";

        adresseInitialisation.append(url);

        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString()
                + this.ajouterContenuSecuriteUrl(formulaire));

        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

      }

      formulaire.ajouterMessageInformatif(
          "infra_sofi.information.gestion.application.aide.ligne.succes",
          new String[] { formulaire.getNom() }, request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          "infra_sofi.erreur.gestion.application.communication"
              + e.getMessage(), request);
    }

    return mapping.findForward("afficher");
  }

  public String ajouterContenuSecuriteUrl(ApplicationForm formulaire) {
    StringBuffer paramSecurite = new StringBuffer("/");
    paramSecurite.append(formulaire.getCodeUtilisateurAdministrateur());
    paramSecurite.append("/");
    paramSecurite.append(formulaire.getMotPasseAdministrateur());
    return paramSecurite.toString();
  }

  public ArrayList<ObjetCleValeur> getListeCachePourApplication(
      ApplicationForm formulaire, HttpServletRequest request) throws Exception {

    StringBuffer adresseInitialisation = null;

    ArrayList<ObjetCleValeur> listeCaches = new ArrayList<ObjetCleValeur>();

    String[] listeNoeudCache = getUrlApplication(
        formulaire.getCodeApplication(), request);

    try {
      if (listeNoeudCache != null && listeNoeudCache[0] != null) {
        adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[0]);
        adresseInitialisation.append(UtilitaireCache
            .getUrlListeNomCache(formulaire.getCodeApplication()));

        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString()
                + this.ajouterContenuSecuriteUrl(formulaire));

        // Enlever les caractères \r et \n
        contenuRetour = UtilitaireString.remplacerTous(contenuRetour, "\r", "");
        contenuRetour = UtilitaireString.remplacerTous(contenuRetour, "\n", "");

        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

        StringTokenizer st = new StringTokenizer(contenuRetour, "|");

        while (st.hasMoreElements()) {
          String cache = (String) st.nextElement();

          if (!"aideEnLigne".equals(cache)) {
            listeCaches.add(new ObjetCleValeur(cache, cache));
          }
        }

      } else {
        listeCaches = null;
      }

    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          "Communication problem with caching: "
              + e.getMessage() + adresseInitialisation.toString(), request);
    }

    return listeCaches;
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur supprimer une application.
   * <p>
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception Entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward supprimer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    ApplicationForm formulaire = (ApplicationForm) form;
    Application application = (Application) formulaire.getObjetTransfert();
    String direction = null;

    // Effectuer l'ajout ou la modification
    try {
      modele.getServiceApplication().supprimerApplication(
          application.getCodeApplication());
      this.commit(
          request,
          "infra_sofi.information.gestion_application.confirmation_suppression",
          new Object[] { formulaire.getNom() });
      // Mettre à jour la liste des applications accessibles à l'utilisateur.
      this.configurerListeApplicationUtilisateur(request);
      direction = "rechercher";
    } catch (ModeleException e) {
      this.rollback(request);
      formulaire.ajouterMessageErreur(
          "infra_sofi.erreur.gestion.application.supprimer.dependances",
          request);

      direction = "afficher";
    }

    return mapping.findForward(direction);
  }
}
