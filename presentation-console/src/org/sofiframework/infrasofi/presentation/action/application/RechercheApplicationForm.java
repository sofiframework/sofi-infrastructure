/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.application;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.presentation.struts.form.BaseDynaForm;

/**
 * Formulaire permettant d'effectuer une recherche pour l'unité de traitement
 * "Gérer les applications".
 * <p>
 * 
 * @author Jean-Maxime Pelletier
 * @author Pierre-Frédérick Duret
 */
public class RechercheApplicationForm extends BaseDynaForm {

  private static final long serialVersionUID = 7110325363441563574L;

  /** Constructeur par défaut */
  public RechercheApplicationForm() {
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un
   * <code>submit</code>.
   * <p>
   * 
   * @param request
   *          la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
    // Ne rien effectuer, c'est un formulaire de recherche
  }
}
