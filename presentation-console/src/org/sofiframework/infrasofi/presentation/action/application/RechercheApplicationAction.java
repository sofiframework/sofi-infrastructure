/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.application;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.filtre.FiltreApplication;
import org.sofiframework.infrasofi.presentation.action.BaseRechercheAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * Action de recherche des applications.
 * 
 * @author Jean-Maxime Pelletier
 */
public class RechercheApplicationAction extends BaseRechercheAction {

  @Override
  public ObjetTransfert construireNouveauFiltre() {
    return new FiltreApplication();
  }

  @Override
  public void effectuerRecherche(ListeNavigation liste, BaseForm formulaire,
      ModeleInfrastructure modele, HttpServletRequest request, HttpServletResponse response)
          throws ModeleException {

    FiltreApplication filtre = (FiltreApplication) formulaire.getObjetTransfert();


    liste.ajouterTriInitial("Nom", true);

    if (filtre.getCommun() != null && !filtre.getCommun()) {
      filtre.setCommun(null);
    }

    modele.getServiceApplication().getListeApplicationAccessiblePilotage(liste);
  }

  @Override
  public String getNomListeDansSession() {
    return "listeApplication";
  }
}
