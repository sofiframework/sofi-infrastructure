/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.journalisation;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.presentation.action.RechercheApplicativeForm;

/**
 * 
 * @author Jean-Maxime Pelletier
 */
public class RechercheJournalisationForm extends RechercheApplicativeForm {

  private static final long serialVersionUID = -3266285494102198521L;

  private String codeUtilisateur;
  private String seqObjetSecurisable;
  private String dateDebut;
  private String dateFin;
  private String detail;
  private String type;
  private String nom;
  private String prenom;
  private String courriel;

  public RechercheJournalisationForm() {
    ajouterFormatterDateHeure("dateDebut");
    ajouterFormatterDateHeure("dateFin");
  }

  /**
   * Valide le formulaire.
   * @param request la requete HTTP en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  public void setSeqObjetSecurisable(String seqObjetSecurisable) {
    this.seqObjetSecurisable = seqObjetSecurisable;
  }

  public String getSeqObjetSecurisable() {
    return seqObjetSecurisable;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public String getDetail() {
    return detail;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setDateDebut(String dateDebut) {
    this.dateDebut = dateDebut;
  }

  public String getDateDebut() {
    return dateDebut;
  }

  public void setDateFin(String dateFin) {
    this.dateFin = dateFin;
  }

  public String getDateFin() {
    return dateFin;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getNom() {
    return nom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setCourriel(String courriel) {
    this.courriel = courriel;
  }

  public String getCourriel() {
    return courriel;
  }
}
