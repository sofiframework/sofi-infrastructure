/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.journalisation;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * 
 * @author Jean-Maxime Pelletier
 */
public class JournalisationForm extends BaseForm {

  /**
   * 
   */
  private static final long serialVersionUID = -3997039585299394867L;

  private static final Log log = LogFactory.getLog(JournalisationForm.class);

  private String codeApplication;
  private String codeUtilisateur;
  private String seqObjetSecurisable;
  private String dateHeure;
  private String detail;
  private String type;
  private String idJournalisation;
  private String nom;
  private String prenom;
  private String courriel;
  private String nomObjet;
  private String typeObjet;

  public JournalisationForm() {
    this.setFormulaireIdentifiant("idJournalisation");
    ajouterFormatterDateHeureSeconde("dateHeure");
  }

  /**
   * Valide le formulaire.
   * 
   * @param request
   *          la requete HTTP en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  public void setSeqObjetSecurisable(String seqObjetSecurisable) {
    this.seqObjetSecurisable = seqObjetSecurisable;
  }

  public String getSeqObjetSecurisable() {
    return seqObjetSecurisable;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public String getDetail() {
    return detail;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setIdJournalisation(String idJournalisation) {
    this.idJournalisation = idJournalisation;
  }

  public String getIdJournalisation() {
    return idJournalisation;
  }

  public void setDateHeure(String dateHeure) {
    this.dateHeure = dateHeure;
  }

  public String getDateHeure() {
    return dateHeure;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getNom() {
    return nom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setCourriel(String courriel) {
    this.courriel = courriel;
  }

  public String getCourriel() {
    return courriel;
  }

  public String getNomComplet() {
    StringBuffer nomComplet = new StringBuffer();
    if (getPrenom() != null) {
      nomComplet.append(getPrenom());
      nomComplet.append(" ");
    }
    nomComplet.append(getNom() == null ? "" : getNom());
    return nomComplet.toString();
  }

  public void setNomObjet(String nomObjet) {
    this.nomObjet = nomObjet;
  }

  public String getNomObjet() {
    return nomObjet;
  }

  public void setTypeObjet(String typeObjet) {
    this.typeObjet = typeObjet;
  }

  public String getTypeObjet() {
    return typeObjet;
  }
}
