/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.journalisation;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Journalisation;
import org.sofiframework.infrasofi.modele.filtre.FiltreJournalisation;
import org.sofiframework.infrasofi.modele.service.ServiceJournalisation;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;

/**
 * 
 * @author Jean-Maxime Pelletier
 */
public class JournalisationAction extends InfrastructureAction {

  private static final Log log = LogFactory.getLog(JournalisationAction.class);
  private static final String ATTRIBUT_LISTE = "listeJournalisation";

  /**
   * Porte d'entrer à l'unité de traitement, réinitalise les valiables
   * et affiche le formulaire de recherche.
   */
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    RechercheJournalisationForm formulaire = (RechercheJournalisationForm) form;
    formulaire.initialiserFormulaire(request, mapping, new FiltreJournalisation(), true);
    formulaire.setCodeApplication(getApplicationSelectionne(request));
    chargerListeTypeJournalisation(formulaire.getCodeApplication(), request, response, false);
    return mapping.findForward("page");
  }

  /**
   * Affiche le détail d'un élément de journalisation pour la consultation.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    JournalisationForm formulaire = (JournalisationForm) form;
    Long idJournalisation = new Long(formulaire.getIdJournalisation());
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    ServiceJournalisation service = modele.getServiceJournalisation();
    Journalisation journalisation = (Journalisation) service.get(idJournalisation);
    formulaire.populerFormulaire(journalisation, request, true);

    return mapping.findForward("afficher");
  }

  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    return mapping.findForward("page");
  }

  public ActionForward chargerListeTypeJournalisation(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    JournalisationForm formulaire = (JournalisationForm) form;
    fixerApplication(mapping, form, request, response);
    chargerListeTypeJournalisation(formulaire.getCodeApplication(), request, response, true);

    return null;
  }
}
