/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.journalisation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.filtre.FiltreJournalisation;
import org.sofiframework.infrasofi.presentation.action.BaseRechercheAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;

/**
 * 
 * @author Jean-Maxime Pelletier
 */
public class RechercheJournalisationAction extends BaseRechercheAction {

  /**
   * Porte d'entrer à l'unité de traitement, réinitalise les valiables
   * et affiche le formulaire de recherche.
   */
  @Override
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
    super.acceder(mapping, form, request, response);
    RechercheJournalisationForm formulaire = (RechercheJournalisationForm) form;
    chargerListeTypeJournalisation(formulaire.getCodeApplication(), request, response, false);
    FiltreJournalisation filtre = (FiltreJournalisation) formulaire.getObjetTransfert();
    filtre.setCodeApplication(new String[]{formulaire.getCodeApplication()});
    return rechercher(mapping, form, request, response);
  }

  @Override
  public ObjetTransfert construireNouveauFiltre() {
    return new FiltreJournalisation();
  }


  @Override
  public void effectuerRecherche(ListeNavigation liste, BaseForm form, ModeleInfrastructure modele,
      HttpServletRequest request, HttpServletResponse response) throws ModeleException {
    RechercheJournalisationForm formulaire = (RechercheJournalisationForm) form;
    try {
      chargerListeTypeJournalisation(formulaire.getCodeApplication(), request, response, false);
    } catch (ServletException e) {
      throw new SOFIException("Erreur pour charger la liste des types de journalisation.");
    }
    liste = modele.getServiceJournalisation().getListeJournalisationPilotage(liste);
  }


  @Override
  public String getNomListeDansSession() {
    return "listeJournalisation";
  }

  public ActionForward chargerListeTypeJournalisation(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws Exception {
    RechercheJournalisationForm formulaire = (RechercheJournalisationForm) form;
    fixerApplication(mapping, form, request, response);
    chargerListeTypeJournalisation(formulaire.getCodeApplication(), request, response, true);
    return null;
  }
}
