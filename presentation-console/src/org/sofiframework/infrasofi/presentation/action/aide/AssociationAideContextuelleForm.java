/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.aide;

import org.sofiframework.presentation.struts.form.BaseDynaForm;

/**
 * @author Jérôme Fiolleau, Nurun Inc.
 * @date 2009-12-16
 *
 */
public class AssociationAideContextuelleForm extends BaseDynaForm {

  private static final long serialVersionUID = -4628402610757500318L;

  public AssociationAideContextuelleForm() {}
}
