/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.aide;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.AideContextuelle;
import org.sofiframework.infrasofi.modele.entite.AssociationAideContextuelle;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.modele.service.ServiceReferentiel;
import org.sofiframework.modele.exception.ModeleException;

/**
 * Action de la page de la liste des services d'un aide contextuelle.
 * 
 * author Jean-Maxime Pelletier
 */
public class AideServiceAction extends BaseAideAction {

  private static final Log log = LogFactory.getLog(AideServiceAction.class);

  /**
   * Afficher la page après une redirection.
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    AideServiceForm formulaire = (AideServiceForm) form;
    if (!formulaire.isFormulaireEnErreur()) {
      remplirListeService(form, request);
    }
    return mapping.findForward("page");

  }

  /**
   * Ajouter un nouveau service a la liste.
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward ajouterService(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    AideServiceForm formulaire = (AideServiceForm) form;
    AideContextuelle aideContextuelle = (AideContextuelle) formulaire.getObjetTransfert();
    AssociationAideContextuelle associationAideContextuelle = new AssociationAideContextuelle();
    aideContextuelle.getListeAssociationAideContextuelle().add(associationAideContextuelle);

    formulaire.ajouterLigne("listeAssociationAideContextuelle", associationAideContextuelle, request);
    formulaire.ajouterMessageInformatif("infra_sofi.information.gestion_aide.ajout.nouvelle_association", request);

    formulaire.populerFormulaire(aideContextuelle, request);

    return mapping.findForward("page");
  }

  /**
   * Dissocier un service de l'aide contextuelle.
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward dissocierService(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    ServiceReferentiel service = modele.getServiceReferentiel();
    AideServiceForm formulaire = (AideServiceForm) form;
    int index = Integer.parseInt(request.getParameter("index"));
    AideContextuelle aideContextuelle = (AideContextuelle) formulaire.getObjetTransfert();
    List<AssociationAideContextuelle> liste = aideContextuelle.getListeAssociationAideContextuelle();

    if (liste != null && index < liste.size()) {
      AssociationAideContextuelle associationAideContextuelle = liste.get(index);

      // Enregistrement à mettre à jour en BD
      if (associationAideContextuelle.getIdOriginal() != null) {
        service.supprimerAssociationAide(associationAideContextuelle.getIdOriginal());
        this.commit(request, "infra_sofi.information.gestion_aide.dissociation.succes", null);
      }

      formulaire.supprimerLigne("listeAssociationAideContextuelle", index, request);

    }

    return mapping.findForward("afficher");
  }

  /**
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    AideServiceForm formulaire = (AideServiceForm) form;
    AideContextuelle aideContextuelle = (AideContextuelle) formulaire.getObjetTransfert();
    List<AssociationAideContextuelle> listeAssociation = aideContextuelle.getListeAssociationAideContextuelle();
    try {
      ServiceReferentiel service = modele.getServiceReferentiel();
      for (AssociationAideContextuelle associationAideContextuelle : listeAssociation) {
        // Nouvelle association
        if (associationAideContextuelle.getIdOriginal() == null) {
          Long idObjetJava = null;
          if (associationAideContextuelle.getIdComposant() != null) {
            idObjetJava = associationAideContextuelle.getIdComposant();
          }
          else if (associationAideContextuelle.getIdOnglet() != null) {
            idObjetJava = associationAideContextuelle.getIdOnglet();
          }
          else if (associationAideContextuelle.getIdService() != null) {
            idObjetJava = associationAideContextuelle.getIdService();
          }

          service.enregistrerCleAide(idObjetJava, aideContextuelle.getCleAide());
        }
        // Association existante
        else {
          boolean modification = false;
          Long idObjetJava = null;
          if (associationAideContextuelle.getIdComposant() != null &&
              associationAideContextuelle.getIdComposant() != associationAideContextuelle.getIdOriginal()) {
            modification = true;
            idObjetJava = associationAideContextuelle.getIdComposant();
          }
          else if (associationAideContextuelle.getIdOnglet() != null &&
              associationAideContextuelle.getIdOnglet() != associationAideContextuelle.getIdOriginal()) {
            modification = true;
            idObjetJava = associationAideContextuelle.getIdOnglet();
          }
          else if (associationAideContextuelle.getIdService() != null &&
              associationAideContextuelle.getIdService() != associationAideContextuelle.getIdOriginal()) {
            modification = true;
            idObjetJava = associationAideContextuelle.getIdService();
          }

          if (modification) {
            service.enregistrerCleAide(idObjetJava, associationAideContextuelle.getIdOriginal(), aideContextuelle.getCleAide());
          }
        }
      }

      this.commit(request, formulaire);
    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur lors de l'enregistrement", e);
      }
      this.rollback(request, e.getMessage(), null);
    }

    return mapping.findForward("afficher");
  }


  private void remplirListeService(ActionForm form, HttpServletRequest request) {

    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);

    AideServiceForm formulaire = (AideServiceForm) form;
    HttpSession session = request.getSession();

    AideContextuelle aideContextuelle = (AideContextuelle) session.getAttribute(AideAction.ATTRIBUT_AIDE_CONTEXTUELLE);

    List<AssociationAideContextuelle> listeAssociation = modele.getServiceAideContextuelle().getListeAssociation(aideContextuelle.getCleAide());

    if (listeAssociation == null) {
      listeAssociation = new ArrayList<AssociationAideContextuelle>();
    }

    aideContextuelle.setListeAssociationAideContextuelle(listeAssociation);
    formulaire.populerFormulaire(aideContextuelle, request);

    // Chargement de tous les services
    setDonneesReference(request);
  }

  public ActionForward getListeOnglets(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

    String id = request.getParameter("id");
    String serviceId = request.getParameter("serviceId");

    if (id != null) {
      int index = Long.valueOf(id).intValue();
      Long idService = Long.valueOf(serviceId);
      ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
      AideServiceForm formulaire = (AideServiceForm) form;
      AideContextuelle aideContextuelle = (AideContextuelle) formulaire.getObjetTransfert();

      AssociationAideContextuelle associationAideContextuelle = aideContextuelle.getListeAssociationAideContextuelle().get(index);

      // FIXME Gérer tous les niveaus enfants d'onglets
      List<ObjetJava> liste = modele.getServiceReferentiel().getListeObjetJavaParParent(idService, "ON");
      associationAideContextuelle.setListeOnglet(liste);
      associationAideContextuelle.setListeComposant(new ArrayList<ObjetJava>());
      formulaire.populerFormulaire(aideContextuelle, request);
      genererReponseListeDeroulante(liste, "nom", "id", "--Tous les onglets--", null, false, request, response);
    }

    return null;
  }


  public ActionForward getListeComposants(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    String id = request.getParameter("id");
    String ongletId = request.getParameter("ongletId");

    if (id != null) {
      int index = Long.valueOf(id).intValue();
      Long idOnglet = Long.valueOf(ongletId);
      ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
      AideServiceForm formulaire = (AideServiceForm) form;
      AideContextuelle aideContextuelle = (AideContextuelle) formulaire.getObjetTransfert();
      AssociationAideContextuelle associationAideContextuelle = aideContextuelle.getListeAssociationAideContextuelle().get(index);

      // FIXME Gérer tous les niveaus enfants de composants (IT)
      List<ObjetJava> liste = modele.getServiceReferentiel().getListeObjetJavaParParent(idOnglet, "IT");
      associationAideContextuelle.setListeComposant(liste);
      formulaire.populerFormulaire(aideContextuelle, request);
      genererReponseListeDeroulante(liste, "nom", "id", "--Aucun--", null, false, request, response);
    }

    return null;
  }

}
