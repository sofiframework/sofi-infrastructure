/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.aide;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.AideContextuelle;
import org.sofiframework.infrasofi.presentation.action.FormulaireTrace;
import org.sofiframework.modele.exception.ModeleException;

/**
 * Formulaire de détail d'une aide contextuelle.
 * 
 * @author Jean-Maxime Pelletier
 */
public class AideForm extends FormulaireTrace {

  private static final long serialVersionUID = 7280924090677867346L;

  private String cleAide;
  private String codeLangue;
  private String codeApplication;
  private String texteAide;
  private String aideExterne;
  private String titre;

  public AideForm() {
    super();
  }

  @Override
  public void validerFormulaire(HttpServletRequest request) {
    try {
      ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);

      if (this.isCleObjetTransfertModifie()) {
        AideContextuelle aide = modele.getServiceAideContextuelle().getAide(this.getCleAide(), this.getCodeLangue());
        if (aide != null) {
          this.ajouterMessageErreur("infra_sofi.erreur.gestion_aide.cleAide.unique", "cleAide", request);
        }
      }

      if (getCleAide() != null && getCleAide().indexOf(getCodeApplication().toLowerCase()) == -1) {
        ajouterMessageErreur("infra_sofi.erreur.gestion_libelle.cleLibelle.code_systeme_inexistant", request);
      }
    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur lors de la validation du texte d'aide", e);
      }
      ajouterMessageErreurGeneral(e.getMessage(), request);
    }
  }

  public void setCleAide(String cleAide) {
    this.cleAide = cleAide;
  }

  public String getCleAide() {
    return cleAide;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setTexteAide(String texteAide) {
    this.texteAide = texteAide;
  }

  public String getTexteAide() {
    return texteAide;
  }

  public void setTitre(String titre) {
    this.titre = titre;
  }

  public String getTitre() {
    return titre;
  }

  public void setAideExterne(String aideExterne) {
    this.aideExterne = aideExterne;
  }

  public String getAideExterne() {
    return aideExterne;
  }
}
