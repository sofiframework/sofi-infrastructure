/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.aide;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.AideContextuelle;
import org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle;
import org.sofiframework.infrasofi.modele.service.ServiceReferentiel;
import org.sofiframework.modele.exception.ModeleException;

/**
 * Action de la page de détail d'une aide contextuelle.
 * 
 * @author Jean-Maxime Pelletier
 */
public class AideAction extends BaseAideAction {

  private static final Log log = LogFactory.getLog(AideAction.class);
  public static final String ATTRIBUT_AIDE_CONTEXTUELLE = "aideContextuelleEnCours";

  /**
   * Afficher la page de détail après une redirection.
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    AideForm formulaire = (AideForm) form;

    if (formulaire.isNouveauFormulaire()) {
      this.exclureComposantInterface("infra_sofi.libelle.gestion_aide.onglet.association", request);
    } else {
      this.inclureComposantInterface("infra_sofi.libelle.gestion_aide.onglet.association", request);
    }

    if (formulaire.getCodeLangue() == null) {
      formulaire.setCodeLangue("fr_CA");
    }

    if (formulaire.getCodeLangue().equals("fr_CA")) {
      request.setAttribute("styleFrancais", "langueSelectionne");
      request.setAttribute("styleAnglais", "langue");
    } else {
      request.setAttribute("styleFrancais", "langue");
      request.setAttribute("styleAnglais", "langueSelectionne");
    }

    return mapping.findForward("page");
  }

  /**
   * Méthode utilisée pour visualiser le détail d'un texte d'aide contextuelle.
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    String direction = null;
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    ServiceAideContextuelle serviceAide = modele.getServiceAideContextuelle();
    AideForm formulaire = (AideForm) form;

    /*
     * Réinitialise toutes les valeurs du formulaire avant de consulter le message.
     */
    formulaire.initialiserFormulaire(request, mapping, true);

    try {
      String cleAide = request.getParameter("cleAide");
      String codeLangue = request.getParameter("codeLangue");

      AideContextuelle aideContextuelle = serviceAide.getAide(cleAide, codeLangue);
      formulaire.populerFormulaire(aideContextuelle, request, true);

      this.setAttributTemporairePourService(ATTRIBUT_AIDE_CONTEXTUELLE, aideContextuelle, request);

      this.setDonneesReference(request);

      if (aideContextuelle == null) {
        // Traiter l'inexistance du libellé
        formulaire.ajouterMessageErreur("infra_sofi.erreur.commun.element.non.trouve", request);
        direction = "recherche";
      } else {
        direction = "afficher";
      }
    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Une erreur s'est produite lors de la lecture "
            + "de l'aide contextuelle pour l'identifiant '"
            + formulaire.getCleAide() + "' et le code de langue '"
            + formulaire.getCodeLangue() + "'.", e);
      }
      formulaire.ajouterMessageErreur(e, request);
      direction = "recherche";
    }

    return mapping.findForward(direction);
  }

  /**
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws IOException
   * @throws ServletException
   */
  public ActionForward changerLangue(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {

    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    AideForm formulaire = (AideForm) form;
    String cleAide = formulaire.getCleAide();
    String langue = formulaire.getCodeLangue();
    formulaire.initialiserFormulaire(request, mapping, true);

    try {
      AideContextuelle aide = modele.getServiceAideContextuelle().getAide(cleAide, langue);
      formulaire.populerFormulaire(aide == null ? new AideContextuelle(cleAide, langue) : aide, request, true);
      this.genererReponseAjax(formulaire, aide, request, response);
    } catch (ModeleException e) {
      // Traiter le problème venant du modèle.
      formulaire.ajouterMessageErreur(e, request);
    }

    return null;
  }

  /**
   * Méthode utilisée pour enregistrer le détail d'un texte d'aide contextuelle.
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    ServiceAideContextuelle serviceAide = modele.getServiceAideContextuelle();
    AideForm formulaire = (AideForm) form;
    AideContextuelle aideContextuelle = (AideContextuelle) formulaire.getObjetTransfert();
    aideContextuelle.setCodeLangue(formulaire.getCodeLangue());
    try {
      String ancienneCleAide = null;
      /**
       * On doit spécifier une ancienne clé primaire si celle-ci
       * est modifiée par l'utilisateur
       */
      if (!formulaire.isNouveauFormulaire()) {
        ancienneCleAide = formulaire.isCleObjetTransfertModifie() ?
            ((AideContextuelle) formulaire.getObjetTransfertOriginal()).getCleAide() : null;
      }

      serviceAide.enregistrer(aideContextuelle, ancienneCleAide);

      this.commit(request, formulaire);

      // Populer le formulaire après enregistrement.
      formulaire.populerFormulaire(aideContextuelle, request);

      this.setAttributTemporairePourService(ATTRIBUT_AIDE_CONTEXTUELLE, aideContextuelle.clone(), request);

    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Une erreur c'est produite lors de la sauvegarde " + "de l'aide contextuelle : " + aideContextuelle, e);
      }
      this.rollback(request, e.getMessage(), null);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Méthode utilisée pour ajouter un nouveau texte d'aide contextuelle.
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  public ActionForward ajouter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    AideForm formulaire = (AideForm) form;
    formulaire.initialiserFormulaire(request, mapping, new AideContextuelle(), true);
    formulaire.ajouterMessageInformatif("infra_sofi.information.gestion_aide.creation", request);
    String codeApplicationSelectionne = request.getParameter("codeApplication");

    if (codeApplicationSelectionne == null) {
      codeApplicationSelectionne = (String) request.getSession().getAttribute("systeme_selectionne");
    }
    formulaire.setCodeApplication(codeApplicationSelectionne);
    return mapping.findForward("afficher");
  }

  /**
   * Méthode utilisée pour supprimer un texte d'aide contextuelle.
   * 
   * @param mapping
   *          actionMapping utilisé pour trouver cette action.
   * @param form
   *          formulaire correspondant à l'action (facultatif).
   * @param request
   *          requête HTTP qui est traitée.
   * @param response
   *          réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException
   *           Exception entrée/sortie.
   * @throws javax.servlet.ServletException
   *           Exception du servlet.
   */
  @SuppressWarnings("rawtypes")
  public ActionForward supprimer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    ServiceAideContextuelle serviceAide = modele.getServiceAideContextuelle();
    ServiceReferentiel serviceReferentiel = modele.getServiceReferentiel();
    AideForm formulaire = (AideForm) form;

    try {
      List liste = serviceReferentiel.getListeServiceParCleAide(formulaire.getCleAide());
      if (liste != null && liste.size() > 0) {
        throw new ModeleException("infra_sofi.erreur.gestionAide.association_services");
      }

      serviceAide.supprimerAide(formulaire.getCleAide());
      this.commit(request,
          "infra_sofi.information.gestion_aide.suppression.succes",
          new Object[]{formulaire.getCleAide()});
    } catch (ModeleException e) {
      // Traiter le problème venant du modèle.
      formulaire.ajouterMessageErreur(e, request);
      this.rollback(request);

      return mapping.findForward("afficher");
    }

    // Appel de la page du détail du texte d'aide contextuelle
    return mapping.findForward("recherche");
  }

  private void genererReponseAjax(AideForm formulaire, AideContextuelle aide, HttpServletRequest request, HttpServletResponse response) {
    if (aide == null) {
      formulaire.setTitre("");
      formulaire.setTexteAide("");
      formulaire.setCreePar("");
      formulaire.setDateCreation("");
      formulaire.setModifiePar("");
      formulaire.setDateModification("");
      formulaire.setNouveauFormulaire(true);
    }

    // Ajouter une valeur dans la réponse pour un identifiant.
    ajouterValeurDansReponse("titre", formulaire.getTitre(), request, response);
    ajouterValeurDansReponse("texteAide", formulaire.getTexteAide(), request, response);
    ajouterValeurDansReponse("creePar", formulaire.getCreePar(), request, response);
    ajouterValeurDansReponse("dateCreation", formulaire.getDateCreation(), request, response);
    ajouterValeurDansReponse("modifiePar", formulaire.getModifiePar(), request, response);
    ajouterValeurDansReponse("dateModification", formulaire.getDateModification(), request, response);

    genererReponseXMLAvecValeurs(request, response);
  }
}
