/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.aide;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.presentation.action.RechercheApplicativeForm;

public class RechercheAideForm extends RechercheApplicativeForm {

  private static final long serialVersionUID = 1932582855630633186L;

  private String cleAide;
  private String codeLangue;
  private String texteAide;
  private String titre;

  public RechercheAideForm() {
    this.setFormulaireIdentifiant("cleAide");
  }

  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  public void setCleAide(String cleAide) {
    this.cleAide = cleAide;
  }

  public String getCleAide() {
    return cleAide;
  }

  public void setCodeLangue(String codeLangue) {
    this.codeLangue = codeLangue;
  }

  public String getCodeLangue() {
    return codeLangue;
  }

  public void setTexteAide(String texteAide) {
    this.texteAide = texteAide;
  }

  public String getTexteAide() {
    return texteAide;
  }

  public void setTitre(String titre) {
    this.titre = titre;
  }

  public String getTitre() {
    return titre;
  }
}