/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.aide;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.AideContextuelle;
import org.sofiframework.infrasofi.modele.util.ComparateurLibelle;
import org.sofiframework.infrasofi.modele.util.ComparateurPropriete;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.modele.exception.ModeleException;

public class BaseAideAction extends InfrastructureAction {

  @SuppressWarnings({ "rawtypes", "unchecked" })
  protected void setDonneesReference(HttpServletRequest request) throws ModeleException {
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    AideContextuelle aide = (AideContextuelle) request.getSession().getAttribute(AideAction.ATTRIBUT_AIDE_CONTEXTUELLE);
    List liste = modele.getServiceReferentiel().getListeServiceParApplication(aide.getCodeApplication());

    if (liste != null) {
      Comparator comparateurReferentiel = new ComparateurPropriete("nom", new ComparateurLibelle(request));
      Collections.sort(liste, comparateurReferentiel);
      this.setAttributTemporairePourService("listeCompleteServices", liste, request);
    }
  }

}
