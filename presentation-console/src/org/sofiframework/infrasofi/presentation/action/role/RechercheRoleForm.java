/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.role;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.presentation.action.RechercheApplicativeForm;


/**
 * Formulaire de donnée permettant la recherche de rôles.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 * @version 1.0
 */
public class RechercheRoleForm extends RechercheApplicativeForm {
  /**
   * 
   */
  private static final long serialVersionUID = 4286562575141037970L;

  /** l'identifiant unique du rôle */
  private String codeRole;

  /** le nom du rôle */
  private String nom;

  /** La vue hiérarchique à utiliser **/
  private String vue;

  /** Le code de statut du role **/
  private String codeStatut;

  /** Constructeur par défaut */
  public RechercheRoleForm() {
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un <code>submit</code>.
   * <p>
   * @param request la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  /**
   * Obtenir le nom du rôle.
   * <p>
   * @return le nom du rôle
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du rôle.
   * <p>
   * @param nom le nom du rôle
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  public void setVue(String vue) {
    this.vue = vue;
  }

  public String getVue() {
    return vue;
  }

  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  public String getCodeStatut() {
    return codeStatut;
  }

  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  public String getCodeRole() {
    return codeRole;
  }
}
