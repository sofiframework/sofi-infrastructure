/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.role;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.composantweb.liste.AttributTri;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Role;
import org.sofiframework.infrasofi.modele.filtre.FiltreRole;
import org.sofiframework.infrasofi.modele.service.ServiceRole;
import org.sofiframework.infrasofi.presentation.action.BaseRechercheAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireListe;

public class RechercheRoleAction extends BaseRechercheAction {

  private static final Log log = LogFactory.getLog(RechercheRoleAction.class);

  @Override
  public ActionForward acceder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    ActionForward direction = super.acceder(mapping, form, request, response);
    RechercheRoleForm formulaire = (RechercheRoleForm) form;
    if (formulaire.getVue() == null) {
      formulaire.setVue("detaillee");
    }
    return direction;
  }

  @Override
  public ActionForward rechercher(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    ActionForward direction = super.rechercher(mapping, form, request, response);

    RechercheRoleForm formulaire = (RechercheRoleForm) form;

    List<Role> listeRole = getModeleInfrastructure(request).getServiceRole()
        .getListeRolePilotage(formulaire.getCodeApplication(), formulaire.getCodeStatut());
    if (listeRole == null) {
      formulaire.ajouterMessageAvertissement(
          "infra_sofi.avertissement.gestion_role.onglet.recherche.aucun_role_defini",
          request);
      listeRole = new ArrayList();
    }

    for (int i=0;i<listeRole.size();i++) {
      Role role = listeRole.get(i);
      role.fixerNiveau();
    }

    listeRole = UtilitaireListe.trierListe(listeRole, new AttributTri[] { new AttributTri("niveau", true) });

    List<Role> listeRoleAjuste = new ArrayList<Role>();
    for (int i=0;i<listeRole.size();i++) {
      Role role = listeRole.get(i);
      if (role.getNiveau().intValue()==1) {
        listeRoleAjuste.add(role);
        ajouterEnfantRole(listeRoleAjuste, role, 2);
      }
    }

    // Fixer la liste temporaire pour l'action.
    setAttributTemporaireListeNavigationPourAction(getNomListeDansSession(), listeRoleAjuste, request);

    return direction;
  }

  private void ajouterEnfantRole(List listeRole, Role role, Integer niveau) {
    //Ajouter les enfants s'il y a lieu.
    Iterator<Role> iterateur = role.getListeRole().iterator();
    while (iterateur.hasNext()) {
      Role roleEnfant = iterateur.next();
      roleEnfant.setNiveau(niveau);
      listeRole.add(roleEnfant);
      if (roleEnfant.getListeRole() != null && roleEnfant.getListeRole().size() > 0) {
        niveau = niveau + 1;
        ajouterEnfantRole(listeRole, roleEnfant, niveau);
      }
    }

  }

  @Override
  public String getNomListeDansSession() {
    return "GESTION_ROLE_RESULTAT_RECHERCHE";
  }

  @Override
  public ObjetTransfert construireNouveauFiltre() {
    return new FiltreRole();
  }

  @Override
  public void effectuerRecherche(ListeNavigation liste, BaseForm form, ModeleInfrastructure modele, HttpServletRequest request, HttpServletResponse response) throws ModeleException {
  }

  public ActionForward rechercherParent(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ModeleException {

    ModeleInfrastructure modele = getModeleInfrastructure(request);
    ServiceRole service = modele.getServiceRole();
    ListeNavigation liste = appliquerListeNavigation(request);
    FiltreRole filtre = (FiltreRole) request.getSession().getAttribute(ATTRIBUT_FILTRE);

    if (filtre == null) {
      filtre = new FiltreRole();
      this.setAttributTemporairePourAction(ATTRIBUT_FILTRE, filtre, request);
    }

    String paramCodeRoleParent = request.getParameter("paramCodeRoleParent");
    filtre.setCodeRole(paramCodeRoleParent);
    String paramNomRoleParent = request.getParameter("paramNomRoleParent");
    filtre.setNom(paramNomRoleParent);
    filtre.setCodeApplication(getApplicationSelectionne(request));
    liste.setObjetFiltre(filtre);
    liste.ajouterTriInitial("nom", true);
    liste = service.getListeRolePilotage(liste);

    this.setAttributTemporaireListeNavigationPourAction("listeRoleParent", liste,
        request);

    /**
     * Réponse Ajax pour la liste de valeur.
     */
    return genererReponseListeValeurAjax("ajax_liste_role_parent",
        new String[] { "codeRole", "nom" },
        "infra_sofi.erreur.gestion_role.codeRoleParent.inexistant", liste,
        mapping, form, request, response);
  }
}
