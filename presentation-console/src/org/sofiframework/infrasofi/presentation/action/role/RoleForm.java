/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.role;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Role;
import org.sofiframework.infrasofi.presentation.action.FormulaireTrace;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.exception.ObjetTransfertNotFoundException;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;

/**
 * Formulaire de donnée permettant la modification du détail d'un rôle.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 * @version 1.0
 */
public class RoleForm extends FormulaireTrace {

  /**
   * 
   */
  private static final long serialVersionUID = 5421098867037117909L;

  /** l'identifiant unique du rôle */
  private String codeRole;

  /** le nom du rôle */
  private String nom;

  /** l'identifiant unique de l'application auquel est associé le rôle */
  private String codeApplication;

  /** La description du rôle **/
  private String description;

  /** Le code de statut du rôle **/
  private String codeStatut;

  /** Le code du role parent **/
  private String codeRoleParent;

  /** Le code du role application parent **/
  private String codeApplicationParent;

  /** Le nom de role du parent **/
  private String nomRoleParent;

  /** Constructeur par défaut */
  public RoleForm() {
    super();
  }

  /**
   * Méthode qui sert à effectuer les validations du formulaire suite à un <code>submit</code>.
   * <p>
   * @param request la requête en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
    // Valider que le rôle n'est pas déjà existant
    if (!isFormulaireEnErreur()) {
      ModeleInfrastructure modele = (ModeleInfrastructure) UtilitaireControleur.getModele(request);

      try {
        if (isNouveauFormulaire()) {
          Role role = modele.getServiceRole().getRole(this.getCodeRole().toUpperCase(), this.getCodeApplication());
          if (role != null) {
            this.ajouterMessageErreur("infra_sofi.erreur.gestion.role.role.deja.existante",
                "codeRole", request);
          }
        }

        if (!isVide(getCodeRoleParent())) {
          Role roleParent = modele.getServiceRole().getRole(this.getCodeRoleParent().toUpperCase(), this.getCodeApplication());
          if (roleParent == null) {
            this.ajouterMessageErreur("infra_sofi.erreur.gestion_role.codeRoleParent.inexistant",
                "codeRoleParent", request);
          }
        }

        if (!isVide(getCodeRoleParent()) &&
            getCodeRole().equals(getCodeRoleParent())) {
          this.ajouterMessageErreur("infra_sofi.erreur.gestion_role.codeRoleParent.identiqueCodeRole",
              "codeRoleParent", request);
        }
      } catch (ObjetTransfertNotFoundException e) {
        // C'est correcte, c'est que l'application n'existe pas déjà
      } catch (ModeleException e) {
        this.ajouterMessageErreur(e, request);
      }
    }

    if (this.isNouveauFormulaire() && this.isFormulaireEnErreur()) {
      this.setNouveauFormulaire(true);
    }
  }

  /**
   * Obtenir l'identifiant unique du rôle.
   * <p>
   * @return l'identifiant unique du rôle
   */
  public String getCodeRole() {
    return codeRole;
  }

  /**
   * Fixer l'identifiant unique du rôle.
   * <p>
   * @param codeRole l'identifiant unique du rôle
   */
  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }

  /**
   * Obtenir le nom du rôle.
   * <p>
   * @return le nom du rôle
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du rôle.
   * <p>
   * @param nom le nom du rôle
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir l'identifiant unique de l'application auquel est associé le rôle.
   * <p>
   * @return l'identifiant unique de l'application auquel est associé le rôle
   */
  public String getCodeApplication() {
    return codeApplication;
  }

  /**
   * Fixer l'identifiant unique de l'application auquel est associé le rôle.
   * <p>
   * @param codeApplication l'identifiant unique de l'application auquel est associé le rôle
   */
  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  /**
   * Fixer la description du role.
   * @param description la description du role.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Retourne la description du role.
   * @return la description du role.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Fixer le code de statut du role.
   * @param codeStatut le code de statut du role.
   */
  public void setCodeStatut(String codeStatut) {
    this.codeStatut = codeStatut;
  }

  /**
   * Retourne le code de statut du role.
   * @return le code de statut du role.
   */
  public String getCodeStatut() {
    return codeStatut;
  }

  /**
   * Fixer le code de role parent.
   * @param codeRoleParent le code de role parent.
   */
  public void setCodeRoleParent(String codeRoleParent) {
    this.codeRoleParent = codeRoleParent;
  }

  /**
   * Retourne le code du role parent.
   * @return le code du role parent.
   */
  public String getCodeRoleParent() {
    return codeRoleParent;
  }

  /**
   * Fixer le code d'application parent.
   * @param codeApplicationParent le code d'application parent.
   */
  public void setCodeApplicationParent(String codeApplicationParent) {
    this.codeApplicationParent = codeApplicationParent;
  }

  /**
   * Retourne le code d'application parent.
   * @return  le code d'application parent.
   */
  public String getCodeApplicationParent() {
    return codeApplicationParent;
  }

  public void setNomRoleParent(String nomRoleParent) {
    this.nomRoleParent = nomRoleParent;
  }

  public String getNomRoleParent() {
    return nomRoleParent;
  }
}
