/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.role;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Role;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.modele.exception.ModeleException;


/**
 * Classe d'action qui gère les accès à l'unité de traitement qui effectue
 * la gestion des rôles des applications.
 * <p>
 * @author Pierre-Frédérick Duret, Nurun inc.
 * @version 1.0
 */
public class RoleAction extends InfrastructureAction {

  private static final Log log = LogFactory.getLog(RoleAction.class);

  /**
   * Cette méthode s'exécute lorsque l'utilisateur accède au détail d'un rôle.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    // Récupérer les objets de base
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    RoleForm formulaire = (RoleForm) form;

    try {
      // Récupérer le rôle et populer le formulaire avec
      Role role = modele.getServiceRole().getRole(formulaire.getCodeRole(),
          formulaire.getCodeApplication());

      formulaire.populerFormulaire(role, request, true);

      if (formulaire.getCodeRoleParent() != null) {
        Role roleParent = modele.getServiceRole().getRole(formulaire.getCodeRoleParent(),
            formulaire.getCodeApplication());
        formulaire.setNomRoleParent(roleParent.getNom());
      } else {
        formulaire.setNomRoleParent("");
      }
    } catch (ModeleException e) {
      e.printStackTrace();
      formulaire.ajouterMessageErreur(e, request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur utilise le bouton "Nouveau" afin
   * de créer un nouveau rôle.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward creer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    String codeApplicationSelectionne = (String) request.getSession()
        .getAttribute("systeme_selectionne");

    // Récupérer et initialiser le formulaire
    RoleForm formulaire = (RoleForm) form;
    Role nouveauRole = new Role();
    nouveauRole.setCodeRoleParent(formulaire.getCodeRoleParent());
    nouveauRole.setCodeApplicationParent(codeApplicationSelectionne);
    formulaire.populerFormulaire(nouveauRole, request, true);
    formulaire.ajouterMessageInformatif("infra_sofi.informatif.gestion_role.onglet.detail.ajout_nouveau_role", request);

    String codeRoleParent = request.getParameter("codeRoleParent");
    Role roleParent = getModeleInfrastructure(request).getServiceRole().getRole(codeRoleParent, codeApplicationSelectionne);

    if (codeRoleParent != null) {
      formulaire.setCodeRoleParent(codeRoleParent);
      formulaire.setNomRoleParent(roleParent.getNom());
    }

    formulaire.setCodeApplication(codeApplicationSelectionne);

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur sauvegarde un rôle.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    // Récupérer les données de base
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    RoleForm formulaire = (RoleForm) form;

    // Récupérer le rôle et effectuer la sauvegarde
    try {
      Role role = (Role) formulaire.getObjetTransfert();
      role.setCodeApplication(formulaire.getCodeApplication());

      if (role.getCodeRoleParent() != null) {
        if (!role.getCodeRoleParent().trim().equals("")) {
          role.setCodeApplicationParent(formulaire.getCodeApplication());
        } else {
          role.setCodeRoleParent(null);
          role.setCodeApplicationParent(null);
        }
      }

      modele.getServiceRole().enregistrerRole(role);
      this.commit(request, formulaire);
      formulaire.populerFormulaire(role, request);

    } catch (ModeleException e) {
      if (e.getMessage().indexOf("INFRA_SOFI.ROLE_FK") != -1) {
        formulaire.ajouterMessageErreur("infra_sofi.erreur.gestion_role.onglet.detail.code_role_non_valide",
            "codeRoleParent", request);
      } else {
        e.printStackTrace();
        formulaire.ajouterMessageErreur(e, request);
      }

      this.rollback(request);
    }

    return mapping.findForward("afficher");
  }

  /**
   * Cette méthode s'exécute lorsque l'utilisateur supprime un rôle.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward supprimer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    // Récupérer les données de base
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    RoleForm formulaire = (RoleForm) form;

    try {
      Role roleASupprimer = modele.getServiceRole().getRole(formulaire.getCodeRole(),
          formulaire.getCodeApplication());

      // Effectuer la suppression
      modele.getServiceRole().supprimerRole(formulaire.getCodeRole(),
          formulaire.getCodeApplication());
      this.commit(request, formulaire,
          "infra_sofi.information.gestion_role.suppression_role_succes",
          new String[] { roleASupprimer.getNom() });
    } catch (ModeleException e) {
      e.printStackTrace();
      formulaire.ajouterMessageErreurGeneral("infra_sofi.erreur.gestion.role.suppression.impossible",
          request);
      this.rollback(request);

      if (request.getParameter("vue") == null) {
        return mapping.findForward("afficher");
      } else {
        return mapping.findForward("retourRecherche");
      }
    }

    return mapping.findForward("retourRecherche");
  }

  /**
   * Cette redirige vers la page d'affichage du détail d'un rôle.
   * <p>
   * @param mapping actionMapping utilisé pour trouver cette action.
   * @param form formulaire correspondant à l'action (facultatif).
   * @param request requête HTTP qui est traitée.
   * @param response réponse HTTP qui est traitée.
   * @return nouvelle direction donné à l'utilisateur.
   * @throws java.io.IOException Exception Entrée/sortie.
   * @throws javax.servlet.ServletException Exception du servlet.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    return mapping.findForward("page");
  }


}
