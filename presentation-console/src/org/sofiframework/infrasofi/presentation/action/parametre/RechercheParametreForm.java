/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.parametre;

import javax.servlet.http.HttpServletRequest;

import org.sofiframework.infrasofi.modele.entite.ParametreSysteme;
import org.sofiframework.infrasofi.presentation.action.RechercheApplicativeForm;

/**
 * Formulaire des critères de recherche de paramètres système.
 * 
 * @author Jean-Maxime Pelletier
 */
public class RechercheParametreForm extends RechercheApplicativeForm {

  private static final long serialVersionUID = -8330228810617751389L;

  private String id;

  private String codeParametre;

  private String codeType;

  private String codeClient;

  private String dateDebutActivite;

  private String dateFinActivite;

  private Boolean differentEnvironnement;

  private Boolean modificationADistance;


  public RechercheParametreForm() {
    this.ajouterCorrespondanceFormulaireEnfants(ParametreForm.class, ParametreSysteme.class);
  }

  /**
   * Valide le formulaire.
   * 
   * @param request
   *          la requete HTTP en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
  }

  public void setCodeParametre(String codeParametre) {
    this.codeParametre = codeParametre;
  }

  public String getCodeParametre() {
    return codeParametre;
  }

  public void setCodeType(String codeType) {
    this.codeType = codeType;
  }

  public String getCodeType() {
    return codeType;
  }

  public void setDifferentEnvironnement(Boolean differentEnvironnement) {
    this.differentEnvironnement = differentEnvironnement;
  }

  public Boolean getDifferentEnvironnement() {
    return differentEnvironnement;
  }

  public void setModificationADistance(Boolean modificationADistance) {
    this.modificationADistance = modificationADistance;
  }

  public Boolean getModificationADistance() {
    return modificationADistance;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCodeClient() {
    return codeClient;
  }

  public void setCodeClient(String codeClient) {
    this.codeClient = codeClient;
  }

  public String getDateDebutActivite() {
    return dateDebutActivite;
  }

  public void setDateDebutActivite(String dateDebutActivite) {
    this.dateDebutActivite = dateDebutActivite;
  }

  public String getDateFinActivite() {
    return dateFinActivite;
  }

  public void setDateFinActivite(String dateFinActivite) {
    this.dateFinActivite = dateFinActivite;
  }
}
