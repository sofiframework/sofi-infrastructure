/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.parametre;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.cache.UtilitaireCache;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.entite.ParametreSysteme;
import org.sofiframework.infrasofi.modele.service.ServiceParametreSysteme;
import org.sofiframework.infrasofi.presentation.action.InfrastructureAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.utilitaire.UtilitaireHttp;

/**
 * Action de recherche des paramètres système.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ParametreAction extends InfrastructureAction {

  private static final Log log = LogFactory.getLog(ParametreAction.class);

  /**
   * Affiche la page après une redirection.
   */
  public ActionForward afficher(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    return mapping.findForward("page");
  }

  /**
   * Affiche le formulaire de création d'un paramètre système.
   */
  public ActionForward ajouter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    ParametreForm formulaire = (ParametreForm) form;
    formulaire.initialiserFormulaire(request, mapping, new ParametreSysteme(), true);

    // Patch car SOFI possède déjà un setValeur
    formulaire.setValeur("");
    formulaire.ajouterMessageInformatif("Ajout d'un nouveau paramètre système.", request);
    formulaire.setCodeApplication(getApplicationSelectionne(request));

    return mapping.findForward("afficher");
  }

  /**
   * Affiche le détail d'un message pour la consultation ou la modification.
   */
  public ActionForward consulter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    ParametreForm formulaire = (ParametreForm) form;
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    ServiceParametreSysteme service = modele.getServiceParametreSysteme();

    if (!formulaire.isNouveauFormulaire()) {
      String id = formulaire.getId();
      ParametreSysteme parametre = service.get(new Long(id));
      formulaire.populerFormulaire(parametre, request, true);
    } else {
      formulaire.ajouterMessageInformatif("infra_sofi.information.parametre.nouveau.parametre_systeme", request);
      formulaire.setCodeApplication(getApplicationSelectionne(request));
    }

    return mapping.findForward("afficher");
  }

  /**
   * Traite le formulaire de modification/création d'un paramètre système.
   */
  public ActionForward enregistrer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    String direction = null;
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    ParametreForm formulaire = (ParametreForm) form;
    ParametreSysteme parametre = (ParametreSysteme) formulaire.getObjetTransfert();
    ServiceParametreSysteme service = modele.getServiceParametreSysteme();

    try {
      // Vérifier tout d'abord si la clé du paramètre système
      ParametreSysteme parametreOriginal = null;
      /**
       * On doit spécifier une ancienne clé primaire si celle-ci est modifiée
       * par l'utilisateur
       */
      if (!formulaire.isNouveauFormulaire() && formulaire.isCleObjetTransfertModifie()) {
        parametreOriginal = (ParametreSysteme) formulaire.getObjetTransfertOriginal();
      }
      service.enregistrer(parametre, parametreOriginal);

      this.commit(request, formulaire);
      formulaire.populerFormulaire(parametre, request);
      direction = "afficher";
    } catch (ModeleException e) {
      this.rollback(request, e.getMessage(), null);
      direction = mapping.getInput();
    }

    return mapping.findForward(direction);
  }

  /**
   * Exécute la suppression d'un message.
   */
  public ActionForward supprimer(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    String direction = null;
    ParametreForm formulaire = (ParametreForm) form;
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    ServiceParametreSysteme service = modele.getServiceParametreSysteme();

    try {
      service.supprimer(new Long(formulaire.getId()));
      this.commit(request,
          "Le paramètre système '{0}' a été supprimé avec succès.",
          new String[] { formulaire
          .getCodeParametre() });
      direction = "recherche";
    } catch (ModeleException e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour supprimer un paramètre.", e);
      }
      direction = mapping.getInput();
    }

    return mapping.findForward(direction);
  }

  /**
   * Exécute l'initialisation d'un paramètre système
   */
  public ActionForward initialiserParametreSysteme(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    ParametreForm formulaire = (ParametreForm) form;
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);

    Application application = modele.getServiceApplication().getApplication(formulaire.getCodeApplication());

    String[] listeNoeudCache = getUrlApplication(
        formulaire.getCodeApplication(), request);

    try {
      for (int i = 0; i < listeNoeudCache.length; i++) {
        StringBuffer adresseInitialisation = new StringBuffer();
        adresseInitialisation.append(listeNoeudCache[i]);
        adresseInitialisation.append(UtilitaireCache.getUrlMajParametreSysteme(formulaire.getCodeApplication()));
        adresseInitialisation.append(formulaire.getCodeParametre());
        adresseInitialisation.append(this.ajouterContenuSecuriteUrl(application));
        
        if (!isVide(formulaire.getCodeClient())) {
          adresseInitialisation.append("?codeClient=");
          adresseInitialisation.append(formulaire.getCodeClient());
        }

        String contenuRetour = UtilitaireHttp
            .getContenuHtml(adresseInitialisation.toString());

        if (log.isInfoEnabled()) {
          log.info(contenuRetour);
        }

      }

      formulaire.ajouterMessageInformatif("infra_sofi.information.parametre.initialisation.parametre",
          new String[] { formulaire.getCodeParametre() }, request);
    } catch (Exception e) {
      formulaire.ajouterMessageErreur(
          "infra_sofi.erreur.gestion.application.communication"
              + e.getMessage(), request);
    }


    return mapping.findForward("afficher");
  }
}
