/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.parametre;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.DomaineValeur;
import org.sofiframework.infrasofi.modele.filtre.FiltreParametreSysteme;
import org.sofiframework.infrasofi.presentation.action.BaseRechercheAction;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.presentation.struts.form.BaseForm;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Action de recherche des paramètres système.
 * 
 * @author Jean-Maxime Pelletier
 */
public class RechercheParametreAction extends BaseRechercheAction {

  @Override
  public ObjetTransfert construireNouveauFiltre() {
    return new FiltreParametreSysteme();
  }

  @Override
  public ActionForward acceder(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {

    RechercheParametreForm formulaire = (RechercheParametreForm) form;

    this.chargerListeTypeParametre(formulaire, request, response);

    request.getSession().setAttribute("listeParametreSysteme", null);

    return super.acceder(mapping, form, request, response);
  }

  public ActionForward chargerListeType(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {

    RechercheParametreForm formulaire = (RechercheParametreForm) form;
    List<DomaineValeur> listeType = this.chargerListeTypeParametre(formulaire, request, response);

    this.genererReponseListeDeroulante(listeType, "description", "valeur", "", "", false, request, response);

    return null;
  }

  private List<DomaineValeur> chargerListeTypeParametre(RechercheParametreForm formulaire, HttpServletRequest request,
      HttpServletResponse response) {

    String codeApplication = request.getParameter("codeApplication");
    if (codeApplication == null) {
      codeApplication = formulaire.getCodeApplication();
    }

    if (UtilitaireString.isVide(codeApplication)) {
      codeApplication = (String) request.getSession().getAttribute("systeme_selectionne");
    }

    formulaire.setCodeApplication(codeApplication);

    List<DomaineValeur> listeTypeParametre = null;

    if (!UtilitaireString.isVide(codeApplication)) {
      listeTypeParametre = this.getModeleInfrastructure(request).getServiceDomaineValeur()
          .getListeDomaineValeur(codeApplication, "LISTE_TYPE_PARAMETRE_SYSTEME", null, getLocale(request).toString());
    }

    this.setAttributTemporairePourService("LISTE_TYPE_PARAMETRE", listeTypeParametre, request);

    return listeTypeParametre;
  }

  @Override
  public void effectuerRecherche(ListeNavigation liste, BaseForm formulaire, ModeleInfrastructure modele,
      HttpServletRequest request, HttpServletResponse response) throws ModeleException {

    FiltreParametreSysteme filtre = (FiltreParametreSysteme) liste.getFiltre();
    if (filtre.getDifferentEnvironnement() != null && !filtre.getDifferentEnvironnement()) {
      filtre.setDifferentEnvironnement(null);
    }

    if (filtre.getModificationADistance() != null && !filtre.getModificationADistance()) {
      filtre.setModificationADistance(null);
    }

    modele.getServiceParametreSysteme().getListeParametrePilotage(liste);
  }

  @Override
  public String getNomListeDansSession() {
    return "listeParametreSysteme";
  }
}