/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action.parametre;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.ParametreSysteme;
import org.sofiframework.presentation.struts.form.BaseDynaForm;

/**
 * Formulaire de détail d'un paramètre système.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ParametreForm extends BaseDynaForm {

  /**
   * 
   */
  private static final long serialVersionUID = -6896364965528542401L;

  public ParametreForm() {
    super();

    this.setFormulaireIdentifiant("id");

    this.ajouterFormatterDateHeureSeconde("dateCreation");
    this.ajouterFormatterDateHeureSeconde("dateModification");
  }

  /**
   * Valide le formulaire.
   * 
   * @param request
   *          la requete HTTP en traitement
   */
  @Override
  public void validerFormulaire(HttpServletRequest request) {
    ModeleInfrastructure modele = (ModeleInfrastructure) getModele(request);
    if (isNouveauFormulaire()) {
      ParametreSysteme p = null;
      if (StringUtils.isEmpty(getCodeClient())) {
        p = modele.getServiceParametreSysteme().get(getCodeApplication(),
            getCodeParametre());

      } else {
        p = modele.getServiceParametreSysteme().get(getCodeApplication(),
            getCodeParametre(), getCodeClient());

      }

      if (p != null) {
        ajouterMessageErreur("infra_sofi.erreur.parametre.parametre.existant",
            "codeParametre", request);
      }
    }
  }

  public String getCodeParametre() {
    return this.getString("codeParametre");
  }

  public String getCodeApplication() {
    return this.getString("codeApplication");
  }

  public void setCodeApplication(String codeApplication) {
    this.set("codeApplication", codeApplication);
  }

  public String getId() {
    return this.getString("id");
  }

  public void setId(String id) {
    this.set("id", id);
  }

  public String getCodeClient() {
    return this.getString("codeClient");
  }

  public void setCodeClient(String codeClient) {
    this.set("codeClient", codeClient);
  }
}
