/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.presentation.action;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.sofiframework.application.domainevaleur.GestionDomaineValeur;
import org.sofiframework.application.securite.objetstransfert.Role;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.presentation.struts.controleur.BaseDispatchAction;
import org.sofiframework.presentation.utilitaire.UtilitaireControleur;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Action abstraite de l'application de gestion de l'infrastructure.
 * 
 * @author Jean-Maxime Pelletier
 */
public abstract class InfrastructureAction extends BaseDispatchAction {

  public static final String LISTE_APPLICATIONS_UTILISATEUR = "LISTE_APPLICATIONS_UTILISATEUR";
  public static final String LISTE_CLIENT = "LISTE_CLIENT";
  public static final String ATTRIBUT_FILTRE = "filtreRecherche";
  public static final String ADRESSE_INITIALISER_CACHE = "adresseInitialisationCache";

  private static final Log log = LogFactory.getLog(InfrastructureAction.class);

  /**
   * Obtenir le modèle.
   * 
   * @param request
   *          Requete HTTP
   * @return Modele
   */
  public ModeleInfrastructure getModeleInfrastructure(HttpServletRequest request) {
    return (ModeleInfrastructure) this.getModele(request);
  }

  /**
   * Ãcrire une entrée dans le journal.
   * 
   * @param request
   *          Requête HTTP
   * @param mode
   *          mode du Journal (type de log)
   * @param detail
   *          Détail de l'entrée
   * @param parametres
   *          Paramètre
   */
  @Override
  public void ecrireJournal(HttpServletRequest request, String mode, String detail, String[] parametres) {
    try {
      super.ecrireJournal(request, mode, detail, parametres);
    } catch (Exception e) {
      if (log.isErrorEnabled()) {
        log.error("Erreur pour journaliser.", e);
      }
    }
  }

  /**
   * Fixer le code de l'application à partir d'un paramètre de la requête dans la session HTTP.
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws ServletException
   */
  public ActionForward fixerApplication(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException {
    String codeApplication = request.getParameter("codeApplication");

    if (codeApplication != null) {
      request.getSession().setAttribute("systeme_selectionne", codeApplication);
    }

    return null;
  }

  /**
   * Obtenir le code d'application sélectionnée par l'utilisateur.
   * 
   * @param request
   *          Requête HTTP
   * @return Code de l'application
   */
  public String getApplicationSelectionne(HttpServletRequest request) {
    return (String) request.getSession().getAttribute("systeme_selectionne");
  }

  /**
   * Charger la liste des types de journalisation.
   * 
   * @param codeApplication
   *          Code de l'application
   * @param request
   *          Requête HTTP
   * @param response
   *          Réponse HTTP
   * @param ajax
   *          Est-ce que la Requête est Ajax
   * @throws ServletException
   *           Erreur servlet
   */
  protected void chargerListeTypeJournalisation(String codeApplication, HttpServletRequest request,
      HttpServletResponse response, boolean ajax) throws ServletException {
    codeApplication = getApplicationSelectionne(request);
    ModeleInfrastructure modele = getModeleInfrastructure(request);
    setAttributTemporairePourService("codeApplicationTypeJournal", codeApplication, request);
    String nomListeDansCache = "LISTE_TYPE_JOURNALISATION_" + codeApplication;
    List listeTypeJournalisation = modele.getServiceDomaineValeur().getListeDomaineValeurPourDefinition(
        codeApplication, "LISTE_TYPE_JOURNALISATION", getLocale(request).toString(), false);

    if (ajax) {
      // Génération de la Réponse Ajax pour remplir la liste déroulante.
      genererReponseListeDeroulante(listeTypeJournalisation, "description", "valeur", "", "", false, request, response);
    }

    // Fixer la liste dans la session temporaire pour service en cours.
    setAttributTemporairePourService("LISTE_TYPE_JOURNALISATION", listeTypeJournalisation, request);
  }

  public void configurerListeApplicationUtilisateur(HttpServletRequest request) {
    ModeleInfrastructure modele = getModeleInfrastructure(request);

    if (UtilitaireControleur.isActionSecurise(request)) {
      Utilisateur utilisateur = getUtilisateur(request);
      List listeApplicationsUtilisateur = null;
      try {
        listeApplicationsUtilisateur = modele.getServiceApplication().getListeApplicationAccessiblePilotage();

        if (listeApplicationsUtilisateur != null) {
          HashSet listeApplicationUnique = new HashSet();
          Iterator iterateurApplications = listeApplicationsUtilisateur.iterator();

          while (iterateurApplications.hasNext()) {
            Application application = (Application) iterateurApplications.next();

            if (request.getSession().getAttribute("systeme_selectionne") == null) {
              request.getSession().setAttribute("systeme_selectionne", application.getCodeApplication());
            }
          }
        }
      } catch (ModeleException e) {
      }

      request.getSession().setAttribute("LISTE_APPLICATIONS_UTILISATEUR", listeApplicationsUtilisateur);
    }
  }

  /**
   * Méthode qui sert à déterminer si un utilisateur est le pilote d'une application en particulier
   * <p>
   * 
   * @param utilisateur
   *          l'utilisateur pour lequel on désire s'avoir s'il est pilote
   * @param codeApplication
   *          l'identifiant unique de l'application pour lequel on veut s'avoir si l'utilisateur est pilote
   */
  protected Boolean isPilote(Utilisateur utilisateur, String codeApplication) {
    List listeRole = utilisateur.getListeRoles();
    Iterator iterateurRole = listeRole.iterator();
    boolean trouvePilote = false;
    boolean trouveApplication = false;

    while (iterateurRole.hasNext()) {
      Role role = (Role) iterateurRole.next();

      if (role.getCodeApplication().equals(codeApplication)) {
        trouveApplication = true;

        if (role.getCode().indexOf("PILOTE") != -1) {
          trouvePilote = true;
        }
      }
    }

    Boolean pilote = null;

    if (trouveApplication && trouvePilote) {
      pilote = Boolean.TRUE;
    }

    if (trouveApplication && !trouvePilote) {
      pilote = Boolean.FALSE;
    }

    return pilote;
  }

  protected void setLangueEnCours(String codeLangue, HttpServletRequest request) {
    if (codeLangue == null || codeLangue.equals("fr_CA")) {
      setAttributTemporairePourAction("styleFrancais", "langueSelectionne", request);
      setAttributTemporairePourAction("styleAnglais", "langue", request);
    } else {
      setAttributTemporairePourAction("styleFrancais", "langue", request);
      setAttributTemporairePourAction("styleAnglais", "langueSelectionne", request);
    }
  }

  /**
   * Obtenir l'URL d'une application.
   * 
   * @param codeApplication
   * @param request
   * @return
   */
  public String[] getUrlApplication(String codeApplication, HttpServletRequest request) {
    return getUrlApplication(codeApplication, null, request);
  }

  /**
   * Obtenir l'URL d'une application.
   * 
   * @param codeApplication
   *          code de l'application
   * @param request
   *          Requête HTTP
   * @return URL de l'application
   */
  public String[] getUrlApplication(String codeApplication, String codeFacette, HttpServletRequest request) {
    // Extraire l'application.
    Application application = getModeleInfrastructure(request).getServiceApplication().getApplication(codeApplication);

    String adresseCache = application.getAdresseCache();

    if (adresseCache == null) {
      adresseCache = application.getAdresseSite();
    }

    if (application != null && application.getAdresseCache() != null) {

      String[] listeNoeudCache = application.getAdresseCache().split(",");
      for (int i = 0; i < listeNoeudCache.length; i++) {
        String adresseSite = listeNoeudCache[i];

        StringBuffer adresseInitialisation = new StringBuffer(adresseSite);

        if (codeFacette != null && !codeFacette.toLowerCase().equals("accueil")) {
          adresseInitialisation.append(UtilitaireString.convertirPremiereLettreEnMajuscule(codeFacette.toLowerCase()));
        }

        String dernierCaractere = adresseSite.substring(adresseSite.length() - 1);

        if (!dernierCaractere.equals("/")) {
          adresseInitialisation.append("/");
        }

        listeNoeudCache[i] = adresseInitialisation.toString();

      }

      return listeNoeudCache;
    }

    return null;
  }

  protected void fixerFacetteApplication(HttpServletRequest request) {

    String codeApplication = getApplicationSelectionne(request);

    List listeFacetteApplication = GestionDomaineValeur.getInstance().getListeValeur(codeApplication,
        "LISTE_FACETTE_APPLICATION", null, "fr_CA", "listeFacetteApplication");

    setAttributTemporairePourService("LISTE_FACETTE_APPLICATION", listeFacetteApplication, request);

  }

  @SuppressWarnings("unchecked")
  public boolean isApplicationPermise(HttpServletRequest request, String codeApplication) {
    boolean result = false;

    List<Application> applications = (List<Application>) request.getSession().getAttribute(
        LISTE_APPLICATIONS_UTILISATEUR);

    if (applications != null && codeApplication != null) {
      for (Application application : applications) {
        if (codeApplication.equals(application.getCodeApplication())) {
          result = true;
        }
      }
    }

    return result;
  }

  public String ajouterContenuSecuriteUrl(Application application) {
    StringBuffer paramSecurite = new StringBuffer("/");
    paramSecurite.append(application.getCodeUtilisateurAdministrateur());
    paramSecurite.append("/");
    paramSecurite.append(application.getMotPasseAdministrateur());
    return paramSecurite.toString();
  }
}
