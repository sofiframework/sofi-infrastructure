package org.sofiframework.infrasofi.serviceweb.securite.test;

//import java.net.URL;
//
//import javax.xml.namespace.QName;
//import javax.xml.ws.Service;
//
//import junit.framework.TestCase;
//
//import org.apache.cxf.endpoint.Client;
//import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
//import org.sofiframework.application.securite.objetstransfert.Utilisateur;
//import org.sofiframework.infrasofi.serviceweb.AlloServiceWeb;
//import org.sofiframework.infrasofi.serviceweb.securite.AuthentificationServiceWeb;
//import org.sofiframework.utilitaire.UtilitaireXml;
//
//public class AuthentificationServiceWebTest extends TestCase {
//
//  /**
//   * Test d'instanciation d'un service avec son interface en java.
//   */
//  public void testauthentififierJava() throws Exception {
//    String codeUtilisateur = "gilles.cote";
//    String motPasse = "asdf1234";
//
//    URL wsdlURL = new URL("http://localhost:8080/service-web/authentification?wsdl");
//    QName nomService = new QName("http://securite.serviceweb.infrasofi.sofiframework.org/", "AuthentificationServiceWebImplService");
//    Service service = Service.create(wsdlURL, nomService);
//    AuthentificationServiceWeb client = service.getPort(AuthentificationServiceWeb.class);
//    
//    String certificat = client.authentififier(codeUtilisateur, motPasse);
//    assertNotNull(certificat);
//    
//    String code = client.getCodeUtilisateur(certificat);
//    assertNotNull(code);
//
//    client.terminerCertificat(certificat);
//
//    String codeTerminer = client.getCodeUtilisateur(certificat);
//    assertNull(codeTerminer);
//    
//    String xmlUtilisateur = client.getUtilisateur("INFRA_SOFI", null, codeUtilisateur, null);
//    assertNotNull(xmlUtilisateur);
//    
//    Utilisateur utilisateur = (Utilisateur) UtilitaireXml.genererObjetAvecXML(xmlUtilisateur, new Utilisateur());
//    assertNotNull(utilisateur);
//  }
//
//  public void testUtilisateurDynamic() throws Exception {
//    URL wsdlURL = new URL("http://localhost:8080/service-web/authentification?wsdl");
//    JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
//    Client client = dcf.createClient(wsdlURL);
//    Object response = client.invoke("getUtilisateur", "INFRA_SOFI", null, "gilles.cote", null);
//    assertNotNull(response);
//  }
//  
//  /**
//   * Exemple d'instanciation d'un service avec son interface.
//   * 
//   * @throws Exception
//   */
//  public void testServiceCreateAllo() throws Exception {
//    URL wsdlURL = new URL("http://localhost:8080/service-web/allo?wsdl");
//    QName nomService = new QName("http://serviceweb.infrasofi.sofiframework.org/", "AlloServiceWebImplService");
//    Service service = Service.create(wsdlURL, nomService);
//    AlloServiceWeb client = service.getPort(AlloServiceWeb.class);
//
//    String result = client.dit("test");
//    assertNotNull(result);
//  }
//
//  /**
//   * Invocation dynamique d'un service seulement à partir de son wsdl.
//   */
//  public void testDynamicClientAllo() throws Exception {
//    URL wsdlURL = new URL("http://localhost:8080/service-web/allo?wsdl");
//    JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
//    Client client = dcf.createClient(wsdlURL);
//    Object response= client.invoke("dit", "robert");
//    assertNotNull(response);
//  }
//}
