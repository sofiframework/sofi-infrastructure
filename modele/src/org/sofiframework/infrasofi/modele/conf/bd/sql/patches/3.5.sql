-- Ajout de libellés bidons pour tester le cloisonnement par client
Insert into LIBELLE_JAVA (ID_LIBELLE, CLE_LIBELLE, CODE_CLIENT, CODE_LOCALE, CODE_APPLICATION, LIBELLE, AIDE_CONTEXTUELLE, AIDE_CONTEXTUELLE_HTML, AIDE_CONTEXTUELLE_EXTERNE, REFERENCE, VERSION) 
values (  -700 ,'infra_sofi.libelle.surveillance.service', 'CLI', 'fr_CA', 'INFRA_SOFI', 'CLI - La surveillance en direct', null, null, null, null, 1);
Insert into LIBELLE_JAVA (ID_LIBELLE, CLE_LIBELLE, CODE_CLIENT, CODE_LOCALE, CODE_APPLICATION, LIBELLE, AIDE_CONTEXTUELLE, AIDE_CONTEXTUELLE_HTML, AIDE_CONTEXTUELLE_EXTERNE, REFERENCE, VERSION) 
values (  -701 ,'infra_sofi.libelle.surveillance.service', 'CLI1', 'fr_CA', 'INFRA_SOFI', 'CLI1 - La surveillance en direct', null, null, null, null, 1);
Insert into LIBELLE_JAVA (ID_LIBELLE, CLE_LIBELLE, CODE_CLIENT, CODE_LOCALE, CODE_APPLICATION, LIBELLE, AIDE_CONTEXTUELLE, AIDE_CONTEXTUELLE_HTML, AIDE_CONTEXTUELLE_EXTERNE, REFERENCE, VERSION) 
values (  -702 ,'infra_sofi.libelle.surveillance.service', 'CLI2', 'fr_CA', 'INFRA_SOFI', 'CLI2 - La surveillance en direct', null, null, null, null, 1);


-- Ajout de messages bidons pour tester le cloisonnement par client
Insert into MESSAGE_JAVA (ID_MESSAGE, CLE_MESSAGE,CODE_CLIENT,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) 
values (-200, 'infra_sofi.libelle.gestion.application.gestion.cache.avertissement','CLI', 'fr_CA','INFRA_SOFI','CLI - La gestion de la cache n''est pas disponible pour l''adresse du site spécifiée.','A',null,null,null,null,1);
Insert into MESSAGE_JAVA (ID_MESSAGE, CLE_MESSAGE,CODE_CLIENT,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) 
values (-201, 'infra_sofi.libelle.gestion.application.gestion.cache.avertissement','CLI1', 'fr_CA','INFRA_SOFI','CLI1 - La gestion de la cache n''est pas disponible pour l''adresse du site spécifiée.','A',null,null,null,null,1);
Insert into MESSAGE_JAVA (ID_MESSAGE, CLE_MESSAGE,CODE_CLIENT,CODE_LOCALE,CODE_APPLICATION,MESSAGE,CODE_SEVERITE,AIDE_CONTEXTUELLE,AIDE_CONTEXTUELLE_HTML,AIDE_CONTEXTUELLE_EXTERNE,REFERENCE,VERSION) 
values (-202, 'infra_sofi.libelle.gestion.application.gestion.cache.avertissement','CLI2', 'fr_CA','INFRA_SOFI','CLI2 - La gestion de la cache n''est pas disponible pour l''adresse du site spécifiée.','A',null,null,null,null,1);

-- Ajout de domaines de valeur test pour le cloisonnement par client.
Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION, CODE_CLIENT) 
values ('INFRA_SOFI','STATUT_ORDONNANCEUR','B','fr_CA','Bidon',3,null,'INFRA_SOFI','STATUT_ORDONNANCEUR','DEFINITION',null,'sofi',sysdate,null,null, 'CLI1');
Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION, CODE_CLIENT)
values ('INFRA_SOFI','MISSFIRE_CRON_TRIGGER','3','fr_CA','Test avec code client',2,null,'INFRA_SOFI','MISSFIRE_CRON_TRIGGER','DEFINITION',null,'sofi',sysdate,null,null, 'CLI1');