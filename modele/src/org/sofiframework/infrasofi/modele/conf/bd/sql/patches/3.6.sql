alter table
   ENTETE_COURRIEL
add
   NOM varchar2(100) NOT NULL;
   
alter table
   COURRIEL
MODIFY
   MESSAGE CLOB;
   
alter table
   TENTATIVE_ENVOI_COURRIEL
MODIFY
   MESSAGE_ERREUR CLOB;
   
alter table
   COURRIEL
ADD
   CHARSET varchar2(100) NOT NULL;