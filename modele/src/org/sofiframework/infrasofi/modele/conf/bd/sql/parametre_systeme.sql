/* -------------------------------------------- */
/* Section Infrastructure SOFI                  */
/* -------------------------------------------- */
INSERT INTO PARAMETRE_SYSTEME ( ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (1,
'INFRA_SOFI', 'authentificationDomaineCookie', '.sofiframework.org', 'Le domaine dont le cookie d''authentification est accessible');
INSERT INTO PARAMETRE_SYSTEME ( ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (2,
'INFRA_SOFI', 'authentificationNomCookie', 'certificat', 'Le nom du cookie.');
INSERT INTO PARAMETRE_SYSTEME ( ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (3,
'INFRA_SOFI', 'authentificationServeur', 'localhost:8080', 'Le serveur d''authentification');
INSERT INTO PARAMETRE_SYSTEME ( ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (4,
'INFRA_SOFI', 'authentificationAgeCookie', '-1', 'La durée de vie du cookie de l''authentification SOFI. Pour que le cookie ait la durée de vie de la session, utiliser la valeur -1.');
INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (5,
'INFRA_SOFI', 'authentificationUrl', '/authentification/accueil.do?methode=acceder'
, 'Url du serveur d''authentification');
INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (6,
'INFRA_SOFI', 'authentificationNomParamUrlRetour', 'url_retour'
, 'Paramètre utilisé par les applications pour être rappelé par le login après une authentification réussie.');
INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (7,
'INFRA_SOFI', 'urlProfilUtilisateur', 'http://localhost:8080/authentification/changementMotDePasse.do'
, 'L''url de changement de mot de passe.');
INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (8,
'INFRA_SOFI', 'codeUtilisateurApplicatif', 'false', 'Utilisation d''un code utilisateur applicatif autre que celui qui est technique.');
INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (9,
'INFRA_SOFI', 'authentificationSecure', 'false', 'Permet d''activer ou pas le mode HTTPS lors de l''authentification.');
INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION) VALUES (10,
'INFRA_SOFI', 'DUREE_MOT_PASSE', '90', 'Nombre de jours pendant lesquels un mot de passe usager est valide.');
INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION) VALUES (11,
'INFRA_SOFI', 'NB_MOT_PASSE_HISTORIQUE', '5', 'Nombre de mot de passe sauvegardé pour l''historique.');

INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION, IND_MODIFICATION_A_DISTANCE) VALUES (12,
'INFRA_SOFI', 'testModifiable', '0', 'Donnée de test pour la modification à distance d''un paramètre.', 'O');
INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION, IND_MODIFICATION_A_DISTANCE) VALUES (13,
'INFRA_SOFI', 'testNonModifiable', '0', 'Donnée de test pour la modification à distance d''un paramètre.', 'N');

INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (17,
'INFRA_SOFI', 'clePriveeCertificat', 'clePriveeADefinir', 'clé privée permettant d''encrypter le code utilisateur à ajouter en prefix du certificat');

/* -------------------------------------------- */
/* Section Authentification                     */
/* -------------------------------------------- */

INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (14,
'LOGIN', 'codeApplicationCommun', 'INFRA_SOFI', 'Le nom du code d''application qui est utilisé pour les paramètres communs.');

INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (15,
'LOGIN', 'smtp', 'smtp.gmail.com', 'Le serveur smtp qui doit être utilisé pour l''envoi des courriels de changement de mot de passe.');

INSERT INTO PARAMETRE_SYSTEME (ID_PARAMETRE_SYSTEME, CODE_APPLICATION, CODE_PARAMETRE, VALEUR,
DESCRIPTION ) VALUES (16,
'LOGIN', 'courrielAdministrateur', 'jean-maxime.pelletier@sofiframework.org', 'Courriel qui est utilisé comme origine des envois de courriel effectués par l''application.');

UPDATE PARAMETRE_SYSTEME SET DATE_CREATION = SYSDATE;
UPDATE PARAMETRE_SYSTEME SET CREE_PAR = 'sofi';
UPDATE PARAMETRE_SYSTEME SET VERSION = 1;
