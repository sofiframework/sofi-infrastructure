-- NUB-37 Rendre le flush de cache fonctionnel

UPDATE parametre_systeme SET valeur = 'app/majParametreSysteme/' WHERE code_parametre = 'cacheUrlMajParametreSysteme';
UPDATE parametre_systeme SET valeur = 'app/majObjetCache/' WHERE code_parametre = 'cacheUrlRecharger';
UPDATE parametre_systeme SET valeur = 'app/majLibelle/' WHERE code_parametre = 'cacheUrlMajLibelle';
UPDATE parametre_systeme SET valeur = 'app/majMessage/' WHERE code_parametre = 'cacheUrlMajMessage';
UPDATE parametre_systeme SET valeur = 'app/getListeCaches' WHERE code_parametre = 'cacheUrlListeNom';
UPDATE parametre_systeme SET valeur = 'app/viderCache/' WHERE code_parametre = 'cacheUrlMaj';