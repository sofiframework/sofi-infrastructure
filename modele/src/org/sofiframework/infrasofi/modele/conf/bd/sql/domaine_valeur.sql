/* -------------------------------------------- */
/* Section Infrastructure SOFI                  */
/* -------------------------------------------- */
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_ACTIVATION_UTILISATEUR', 'DEFINITION', 'fr_CA', 'Liste des statut d''activation d''un utilisateur.'
, NULL, 'A', NULL, NULL, NULL, 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'fr_CA', 'La liste des types de journalisation disponibles pour l''application.'
, NULL, 'A', NULL, NULL, NULL, 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA', 'La liste des types de messages disponibles'
, NULL, 'A', NULL, NULL, NULL, 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'en_US', 'La liste des types de journalisation disponibles pour l''application.'
, NULL, 'A', NULL, NULL, NULL, 'en_US');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'M', 'fr_CA', 'Modification', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'M', 'en_US', 'Update', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'en_US');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'A', 'fr_CA', 'Ajout', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_ACTIVATION_UTILISATEUR', 'I', 'fr_CA', 'Inactif', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_ACTIVATION_UTILISATEUR', 'DEFINITION', 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'C', 'fr_CA', 'Consultation', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_JOURNALISATION', 'S', 'fr_CA', 'Suppression', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_JOURNALISATION', 'DEFINITION', 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_ACTIVATION_UTILISATEUR', 'A', 'fr_CA', 'Actif', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_ACTIVATION_UTILISATEUR', 'DEFINITION', 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'I', 'fr_CA', 'Informatif', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'C', 'fr_CA', 'Confirmation', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'J', 'fr_CA', 'Journalisation', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'E', 'fr_CA', 'Erreur', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_TYPE_MESSAGE', 'A', 'fr_CA', 'Avertisement', NULL, NULL, 'INFRA_SOFI'
, 'LISTE_TYPE_MESSAGE', 'DEFINITION', 'fr_CA');


Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','TYPE_TRIGGER','DEFINITION','fr_CA','Définition',1,null,null,null,null,null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','TYPE_TRIGGER','SIMPLE','fr_CA','Simple',1,null,'INFRA_SOFI','TYPE_TRIGGER','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','TYPE_TRIGGER','CRON','fr_CA','Expression cron',2,null,'INFRA_SOFI','TYPE_TRIGGER','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','MISSFIRE_CRON_TRIGGER','DEFINITION','fr_CA','Définition',1,null,null,null,null,null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','MISSFIRE_CRON_TRIGGER','1','fr_CA','Lancer une fois dès la reprise',1,null,'INFRA_SOFI','MISSFIRE_CRON_TRIGGER','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','MISSFIRE_CRON_TRIGGER','2','fr_CA','Ne rien faire à la reprise',2,null,'INFRA_SOFI','MISSFIRE_CRON_TRIGGER','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION','fr_CA','Définition',1,null,null,null,null,null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','1','fr_CA','Lancer une fois dès la reprise',1,null,'INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','2','fr_CA','Exécution immédiate avec le compteur remis à zéros',2,null,'INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','3','fr_CA','Exécution immédiate avec le compteur sauvegardé',3,null,'INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','4','fr_CA','Prochaine exécution avec le compteur sauvegardé',4,null,'INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION)
values ('INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','5','fr_CA','Prochaine exécution avec le compteur remis à zéros',5,null,'INFRA_SOFI','MISSFIRE_SIMPLE_TRIGGER','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','STATUT_ORDONNANCEUR','DEFINITION','fr_CA','Définition',1,null,null,null,null,null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','STATUT_ORDONNANCEUR','A','fr_CA','Arrêté',1,null,'INFRA_SOFI','STATUT_ORDONNANCEUR','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','STATUT_ORDONNANCEUR','D','fr_CA','Démarré',2,null,'INFRA_SOFI','STATUT_ORDONNANCEUR','DEFINITION',null,'sofi',sysdate,null,null);



INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_ACTIVITE_DOMAINE_VALEUR', 'DEFINITION', 'fr_CA', 'Liste des statut d''activité d''un domaine de valeur.'
, NULL, 'A', NULL, NULL, NULL, 'fr_CA');
INSERT INTO DOMAINE_VALEUR ( CODE_APPLICATION, NOM, VALEUR, CODE_LOCALE, DESCRIPTION,
ORDRE_AFFICHAGE, CODE_TYPE_TRI, CODE_APPLICATION_PARENT, NOM_PARENT, VALEUR_PARENT,
CODE_LOCALE_PARENT ) VALUES (
'INFRA_SOFI', 'LISTE_ACTIVITE_DOMAINE_VALEUR', 'DEFINITION', 'en_US', 'Activation states for list of values.'
, NULL, 'A', NULL, NULL, NULL, 'en_US');

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','LISTE_ACTIVITE_DOMAINE_VALEUR','O','fr_CA','Actif', 1, null,'INFRA_SOFI','LISTE_ACTIVITE_DOMAINE_VALEUR','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','LISTE_ACTIVITE_DOMAINE_VALEUR','N','fr_CA','Inactif', 2, null,'INFRA_SOFI','LISTE_ACTIVITE_DOMAINE_VALEUR','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','LISTE_ACTIVITE_DOMAINE_VALEUR','O','en_US','Active', 1, null,'INFRA_SOFI','LISTE_ACTIVITE_DOMAINE_VALEUR','DEFINITION',null,'sofi',sysdate,null,null);

Insert into DOMAINE_VALEUR (CODE_APPLICATION,NOM,VALEUR,CODE_LOCALE,DESCRIPTION,ORDRE_AFFICHAGE,CODE_TYPE_TRI
                            ,CODE_APPLICATION_PARENT,NOM_PARENT,VALEUR_PARENT,CODE_LOCALE_PARENT,CREE_PAR,DATE_CREATION,MODIFIE_PAR,DATE_MODIFICATION) 
values ('INFRA_SOFI','LISTE_ACTIVITE_DOMAINE_VALEUR','N','en_US','Inactive', 2, null,'INFRA_SOFI','LISTE_ACTIVITE_DOMAINE_VALEUR','DEFINITION',null,'sofi',sysdate,null,null);

UPDATE DOMAINE_VALEUR SET DATE_CREATION = SYSDATE;
UPDATE DOMAINE_VALEUR SET DATE_DEBUT_ACTIVITE = '1900-01-01';
UPDATE DOMAINE_VALEUR SET VERSION = 1;