INSERT INTO QUARTZ_SCHEDULER VALUES(1,
                'ORDONNANCEUR_SOFI',
                'LOGIN',
                'Ordonnanceur SOFI',
                'Ordonnanceur de SOFI qui permet de faire certaines tâches automatiques. Par exemple, le nettoyage dans les certificats',
                'A',null,null,null,null,null);

INSERT INTO QUARTZ_JOB_DETAILS VALUES(1,1,
                'NETTOYAGE_CERTIFICAT',
                'DEFAULT',
                'Tâche qui nettoie les certificats une fois par jour (1:00 am).',
                'org.sofiframework.infrasofi.modele.securite.tache.TacheNettoyageCertificat',
                '1','0','0','1',null,null,null,null,null);

INSERT INTO QUARTZ_TRIGGERS VALUES(1,1,NULL,
                'UNE_HEURE_AM',
                'DEFAULT',
                '0','Déclencheur une fois par jour a 1:00 am',
                -1,null,5,'WAITING','CRON',0,NULL,1,NULL,NULL,NULL,NULL,NULL);

INSERT INTO QUARTZ_CRON_TRIGGERS VALUES(1,1,'0 0 1 * * ?',NULL,NULL,NULL,NULL,NULL);