-- Supprimer tous les libellés corresponds aux pays
DELETE FROM LIBELLE_JAVA WHERE CLE_LIBELLE LIKE 'infra_sofi.libelle.pays.%';

-- Supprimer tous les libellés corresponds aux regions
DELETE FROM LIBELLE_JAVA WHERE CLE_LIBELLE LIKE 'infra_sofi.libelle.region.%';

-- Supprimer la colonne CLE_LIBELLE de la table PAYS
ALTER TABLE PAYS DROP COLUMN CLE_LIBELLE;

-- Supprimer la colonne CLE_LIBELLE de la table REGION
ALTER TABLE REGION DROP COLUMN CLE_LIBELLE;