/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele;

import org.sofiframework.application.courriel.service.ServiceCourriel;
import org.sofiframework.application.securite.service.ServiceClient;
import org.sofiframework.infrasofi.modele.conversion.service.local.ServiceConversion;
import org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle;
import org.sofiframework.infrasofi.modele.service.ServiceApplication;
import org.sofiframework.infrasofi.modele.service.ServiceCertificat;
import org.sofiframework.infrasofi.modele.service.ServiceDeclencheur;
import org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur;
import org.sofiframework.infrasofi.modele.service.ServiceEnvoiCourriel;
import org.sofiframework.infrasofi.modele.service.ServiceJob;
import org.sofiframework.infrasofi.modele.service.ServiceJournalisation;
import org.sofiframework.infrasofi.modele.service.ServiceLibelle;
import org.sofiframework.infrasofi.modele.service.ServiceMessage;
import org.sofiframework.infrasofi.modele.service.ServiceOrdonnanceur;
import org.sofiframework.infrasofi.modele.service.ServiceParametreSysteme;
import org.sofiframework.infrasofi.modele.service.ServiceReferentiel;
import org.sofiframework.infrasofi.modele.service.ServiceRole;
import org.sofiframework.infrasofi.modele.service.ServiceUtilisateur;
import org.sofiframework.infrasofi.modele.service.ServiceUtilisateurRole;
import org.sofiframework.modele.spring.ModeleImpl;

/**
 * Modèle qui offre une façade vers les services applicatifs de la gestion
 * de l'infrastructure.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ModeleInfrastructureImpl extends ModeleImpl implements
ModeleInfrastructure {

  private ServiceReferentiel serviceReferentiel;
  private ServiceApplication serviceApplication;
  private ServiceLibelle serviceLibelle;
  private ServiceMessage serviceMessage;
  private ServiceAideContextuelle serviceAideContextuelle;
  private ServiceJournalisation serviceJournalisation;
  private ServiceParametreSysteme serviceParametreSysteme;
  private ServiceUtilisateur serviceUtilisateur;
  private ServiceRole serviceRole;
  private ServiceDomaineValeur serviceDomaineValeur;
  private ServiceUtilisateurRole serviceUtilisateurRole;
  private ServiceConversion serviceConversion;
  private ServiceOrdonnanceur serviceOrdonnanceur;
  private ServiceJob serviceJob;
  private ServiceDeclencheur serviceDeclencheur;
  private ServiceCertificat serviceCertificat;
  private ServiceEnvoiCourriel serviceEnvoiCourriel;
  private ServiceClient serviceClient;
  private ServiceCourriel serviceCourriel;

  public ServiceReferentiel getServiceReferentiel() {
    return this.serviceReferentiel;
  }

  public ServiceLibelle getServiceLibelle() {
    return this.serviceLibelle;
  }

  public ServiceAideContextuelle getServiceAideContextuelle() {
    return this.serviceAideContextuelle;
  }

  public ServiceMessage getServiceMessage() {
    return this.serviceMessage;
  }

  public ServiceJournalisation getServiceJournalisation() {
    return this.serviceJournalisation;
  }

  public ServiceParametreSysteme getServiceParametreSysteme() {
    return this.serviceParametreSysteme;
  }

  public ServiceUtilisateur getServiceUtilisateur() {
    return this.serviceUtilisateur;
  }

  public ServiceRole getServiceRole() {
    return this.serviceRole;
  }

  public ServiceDomaineValeur getServiceDomaineValeur() {
    return this.serviceDomaineValeur;
  }

  public void setServiceReferentiel(ServiceReferentiel serviceReferentiel) {
    this.serviceReferentiel = serviceReferentiel;
  }

  public void setServiceLibelle(ServiceLibelle serviceLibelle) {
    this.serviceLibelle = serviceLibelle;
  }

  public void setServiceMessage(ServiceMessage serviceMessage) {
    this.serviceMessage = serviceMessage;
  }

  public void setServiceAideContextuelle(
      ServiceAideContextuelle serviceAideContextuelle) {
    this.serviceAideContextuelle = serviceAideContextuelle;
  }

  public void setServiceJournalisation(
      ServiceJournalisation serviceJournalisation) {
    this.serviceJournalisation = serviceJournalisation;
  }

  public void setServiceParametreSysteme(
      ServiceParametreSysteme serviceParametreSysteme) {
    this.serviceParametreSysteme = serviceParametreSysteme;
  }

  public void setServiceUtilisateur(ServiceUtilisateur serviceUtilisateur) {
    this.serviceUtilisateur = serviceUtilisateur;
  }

  public void setServiceRole(ServiceRole serviceRole) {
    this.serviceRole = serviceRole;
  }

  public void setServiceDomaineValeur(ServiceDomaineValeur serviceDomaineValeur) {
    this.serviceDomaineValeur = serviceDomaineValeur;
  }

  public ServiceApplication getServiceApplication() {
    return serviceApplication;
  }

  public void setServiceApplication(ServiceApplication serviceApplication) {
    this.serviceApplication = serviceApplication;
  }

  public ServiceUtilisateurRole getServiceUtilisateurRole() {
    return serviceUtilisateurRole;
  }

  public void setServiceUtilisateurRole(
      ServiceUtilisateurRole serviceUtilisateurRole) {
    this.serviceUtilisateurRole = serviceUtilisateurRole;
  }

  public ServiceConversion getServiceConversion() {
    return this.serviceConversion;
  }

  public void setServiceConversion(ServiceConversion serviceConversion) {
    this.serviceConversion = serviceConversion;
  }

  public ServiceOrdonnanceur getServiceOrdonnanceur() {
    return serviceOrdonnanceur;
  }

  public void setServiceOrdonnanceur(ServiceOrdonnanceur serviceOrdonnanceur) {
    this.serviceOrdonnanceur = serviceOrdonnanceur;
  }

  public ServiceJob getServiceJob() {
    return serviceJob;
  }

  public void setServiceJob(ServiceJob serviceJob) {
    this.serviceJob = serviceJob;
  }

  public ServiceDeclencheur getServiceDeclencheur() {
    return serviceDeclencheur;
  }

  public void setServiceDeclencheur(ServiceDeclencheur serviceDeclencheur) {
    this.serviceDeclencheur = serviceDeclencheur;
  }

  public ServiceCertificat getServiceCertificat() {
    return serviceCertificat;
  }

  public void setServiceCertificat(ServiceCertificat serviceCertificat) {
    this.serviceCertificat = serviceCertificat;
  }

  public ServiceEnvoiCourriel getServiceEnvoiCourriel() {
    return serviceEnvoiCourriel;
  }

  public void setServiceEnvoiCourriel(ServiceEnvoiCourriel serviceEnvoiCourriel) {
    this.serviceEnvoiCourriel = serviceEnvoiCourriel;
  }

  public ServiceClient getServiceClient() {
    return serviceClient;
  }

  public void setServiceClient(ServiceClient serviceClient) {
    this.serviceClient = serviceClient;
  }

  public ServiceCourriel getServiceCourriel() {
    return serviceCourriel;
  }

  public void setServiceCourriel(ServiceCourriel serviceCourriel) {
    this.serviceCourriel = serviceCourriel;
  }
}