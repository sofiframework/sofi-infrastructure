/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.aide.service.commun;

import org.apache.commons.beanutils.PropertyUtils;
import org.sofiframework.application.aide.objetstransfert.Aide;
import org.sofiframework.application.aide.service.ServiceAide;
import org.sofiframework.infrasofi.modele.entite.AideContextuelle;
import org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.service.local.BaseServiceImpl;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Classe implémentant l'interface ServiceAide et qui implémente la méthode
 * getAide(...).
 * 
 * @author Jean-Maxime Pelletier
 * @author Marie-Soleil L'Allier (Nurun inc.)
 */
public class ServiceAideImpl extends BaseServiceImpl implements ServiceAide {

  private String langueParDefaut = "fr_CA";

  private ServiceAideContextuelle serviceAideContextuelle = null;

  /**
   * Méthode retournant l'aide contextuelle associée à l'objet sécurisable et la
   * langue demandée.
   * 
   * @return l'aide contextuelle associée à l'objet sécurisable et la langue
   *         demandée.
   * @param codeLangue
   *          le code de la langue désirée.
   * @param seqObjetSecurisable
   *          l'identifiant de l'objet sécurisable.
   */
  public Aide getAide(Long seqObjetSecurisable, String codeLangue) {
    // Si aucune langue spécifié, demander la langue par défaut
    codeLangue = UtilitaireString.isVide(codeLangue) ? langueParDefaut : codeLangue;

    AideContextuelle aideContextuelle = this.getServiceAideContextuelle()
        .getAide(seqObjetSecurisable, codeLangue);
    Aide aide = convertirAideContextuelle(aideContextuelle);
    return aide;
  }

  private Aide convertirAideContextuelle(AideContextuelle aideContextuelle) {
    Aide aide = null;
    if (aideContextuelle != null) {
      try {
        aide = new Aide();
        PropertyUtils.copyProperties(aide, aideContextuelle);
      } catch (Exception e) {
        throw new ModeleException(e);
      }
    }
    return aide;
  }

  /**
   * Obtenir le service d'aide contextuelle
   * @return Service
   */
  public ServiceAideContextuelle getServiceAideContextuelle() {
    return serviceAideContextuelle;
  }

  /**
   * Fixer le service d'aide contextuelle.
   * @param serviceAideContextuelle Service
   */
  public void setServiceAideContextuelle(ServiceAideContextuelle serviceAideContextuelle) {
    this.serviceAideContextuelle = serviceAideContextuelle;
  }

  public String getLangueParDefaut() {
    return langueParDefaut;
  }

  public void setLangueParDefaut(String langueParDefaut) {
    this.langueParDefaut = langueParDefaut;
  }

  public Aide getAideSelonCle(String cleAide, String codeLangue) {
    AideContextuelle aideContextuelle = this.serviceAideContextuelle.getAide(cleAide, codeLangue);
    Aide aide = convertirAideContextuelle(aideContextuelle);
    return aide;
  }
}
