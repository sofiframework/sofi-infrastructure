/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.aide.service.local;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.aide.dao.AideContextuelleDao;
import org.sofiframework.infrasofi.modele.entite.AideContextuelle;
import org.sofiframework.infrasofi.modele.entite.AssociationAideContextuelle;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.modele.filtre.FiltreAideContextuelle;
import org.sofiframework.infrasofi.modele.filtre.FiltreObjetJava;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.ObjetJavaDao;
import org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle;

/**
 * Classe implémentant l'interface ServiceAide et qui implémente la méthode
 * getAide(...).
 * <p>
 * 
 * @author Marie-Soleil L'Allier (Nurun inc.)
 * @version SOFI 2.0
 */
public class ServiceAideContextuelleImpl extends BaseServiceInfrastructureImpl implements ServiceAideContextuelle {

  private ObjetJavaDao objetJavaDao;

  public ObjetJavaDao getObjetJavaDao() {
    return objetJavaDao;
  }

  public void setObjetJavaDao(ObjetJavaDao objetJavaDao) {
    this.objetJavaDao = objetJavaDao;
  }

  /*
   * (non-Javadoc)
   * @see org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle#getAide(java.lang.Long, java.lang.String)
   */
  public AideContextuelle getAide(Long seqObjetSecurisable, String codeLangue) {
    return getAideDao().getAide(seqObjetSecurisable, codeLangue);
  }

  /*
   * (non-Javadoc)
   * @see org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle#getTexteAide(java.lang.String, java.lang.String)
   */
  public AideContextuelle getAide(String cleAide, String codeLangue) {
    return getAideDao().getAide(cleAide, codeLangue);
  }

  /*
   * (non-Javadoc)
   * @see org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle#enregistrer(org.sofiframework.infrasofi.modele.entite.AideContextuelle, java.lang.String)
   */
  public void enregistrer(AideContextuelle aide, String ancienneCle) {
    if (ancienneCle != null) {
      this.modifierCleAide(ancienneCle, aide);
    } else {
      if (aide.getDateCreation() == null) {
        this.ajouter(aide);
      } else {
        this.modifier(aide);
      }
    }
  }

  /*
   * Mettre à jour une clé d'un aide contextuelle.
   */
  private void modifierCleAide(String cleOriginale, AideContextuelle aide) {
    /*
     * On doit aller chercher les message dans toutes les langues.
     */
    List listeAide = this.getListe(cleOriginale);
    for (Iterator i = listeAide.iterator(); i.hasNext();) {
      AideContextuelle a = (AideContextuelle) i.next();
      this.supprimer(a);

      /*
       * On met un trace de modification
       * meme si en réalité cest une création.
       */
      this.traceModification(a);

      /**
       * Pour la langue en cours on en profite pour ajouter le
       * message qui est modifié par l'utilisateur.
       */
      if (a.getCodeLangue().equalsIgnoreCase(aide.getCodeLangue())) {
        a = aide;
      } else {
        a.setCleAide(aide.getCleAide());
      }

      this.ajouter(a);
    }
  }

  private List getListe(String cleAide) {
    FiltreAideContextuelle filtre = new FiltreAideContextuelle();
    filtre.setCleAide(cleAide);
    List liste = this.getAideDao().getListe(filtre);
    return liste;
  }

  /*
   * (non-Javadoc)
   * @see org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle#supprimerTexteAide(java.lang.String, java.lang.String)
   */
  public void supprimerAide(String cleAide) {
    List liste = this.getListe(cleAide);
    for (int i = 0; i < liste.size(); i++) {
      super.supprimer((AideContextuelle) liste.get(i));
    }
  }

  /*
   * (non-Javadoc)
   * @see org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle#getListeAidePourTableauBord(org.sofiframework.composantweb.liste.ListeNavigation, java.lang.Integer, java.lang.String)
   */
  public ListeNavigation getListeAidePourTableauBord(ListeNavigation ln, Integer nombreJours, String codeApplication) {
    return this.getAideDao().getListe(ln, nombreJours, codeApplication);
  }

  public ListeNavigation getListeAideContextuellePilotage(ListeNavigation liste) {
    String codeUtilisateur = this.getCodeUtilisateurEnCours();
    this.getAideContextuelleDao().getListeAideContextuellePilotage(liste, codeUtilisateur);
    return liste;
  }

  private AideContextuelleDao getAideContextuelleDao() {
    return (AideContextuelleDao) this.getDao();
  }

  /**
   * Obtenir l'accès de données d'aide contextuelle.
   * @return Accès de données
   */
  private AideContextuelleDao getAideDao() {
    return (AideContextuelleDao) this.getDao();
  }

  public List<AssociationAideContextuelle> getListeAssociation(String cleAide) {

    List<AssociationAideContextuelle> listeAssociation = this.getAideDao().getListeAssociation(cleAide);

    // Pour chaque association, mettre à jour la liste d'onglets (enfants du service), et la liste d'items (enfants de l'onglet)
    for (AssociationAideContextuelle association : listeAssociation) {
      // Chargement de la liste d'onglet enfant du service
      FiltreObjetJava filtre = new FiltreObjetJava();
      filtre.setIdParent(association.getIdService());
      filtre.setType("ON");
      List<ObjetJava> listeOnglet = new ArrayList<ObjetJava>();
      for (ObjetJava onglet : (List<ObjetJava>)this.getObjetJavaDao().getListe(filtre)) {
        listeOnglet.addAll(this.getObjetJavaDao().getListeObjetJavaParType(onglet, "ON"));
      }
      association.setListeOnglet(listeOnglet);

      if (association.getIdOnglet() != null) {
        filtre.setIdParent(association.getIdOnglet());
        filtre.setType("IT");
        List<ObjetJava> listeComposant = new ArrayList<ObjetJava>();
        for (ObjetJava item : (List<ObjetJava>)this.getObjetJavaDao().getListe(filtre)) {
          listeComposant.addAll(this.getObjetJavaDao().getListeObjetJavaParType(item, "IT"));
        }

        association.setListeComposant(listeComposant);
      }
    }

    return listeAssociation;
  }
}
