/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.aide.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseDaoInfrastructureImpl;
import org.sofiframework.infrasofi.modele.aide.dao.AideContextuelleDao;
import org.sofiframework.infrasofi.modele.entite.AideContextuelle;
import org.sofiframework.infrasofi.modele.entite.AssociationAideContextuelle;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.modele.filtre.FiltreObjetJava;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.ObjetJavaDao;

/**
 * Accès de données pour l'objet d'affaire Aide.
 * 
 * @author Jean-Maxime Pelletier
 * @author Marie-Soleil L'Allier (Nurun inc.)
 */
public class AideContextuelleDaoImpl extends BaseDaoInfrastructureImpl implements AideContextuelleDao {

  private  ObjetJavaDao objetJavaDao;

  public AideContextuelleDaoImpl() {
    super(AideContextuelle.class);
  }

  public ObjetJavaDao getObjetJavaDao() {
    return objetJavaDao;
  }

  public void setObjetJavaDao(ObjetJavaDao objetJavaDao) {
    this.objetJavaDao = objetJavaDao;
  }

  @SuppressWarnings("unchecked")
  public AideContextuelle getAide(Long seqObjetSecurisable, String codeLangue) {
    AideContextuelle aide = null;
    List<AideContextuelle> liste = getHibernateTemplate().find(
        "SELECT a " +
            "FROM AideContextuelle a, ObjetSecurisable o " +
            "WHERE a.cleAide = o.cleAideContextuelle " +
            "AND o.seqObjetSecurisable = ? " +
            "AND a.codeLangue = ?",
            new Object[] { seqObjetSecurisable, codeLangue });
    if (liste != null && liste.size() > 0) {
      aide = liste.get(0);
    }
    return aide;
  }

  @SuppressWarnings("unchecked")
  public AideContextuelle getAide(String cleAide, String codeLangue) {
    AideContextuelle aide = null;
    List<AideContextuelle> liste = getHibernateTemplate().find(
        "FROM AideContextuelle a " +
            "WHERE a.cleAide = ? " +
            "AND a.codeLangue = ?",
            new Object[] {cleAide, codeLangue});
    if (liste != null && liste.size() > 0) {
      aide = liste.get(0);
    }
    return aide;
  }

  public ListeNavigation getListe(ListeNavigation ln, Integer nombreJours, String codeApplication) {
    Date dj = new Date(System.currentTimeMillis() - nombreJours*24*60*60*1000);
    if (StringUtils.isNotEmpty(codeApplication)) {
      ln = this.getListe("FROM AideContextuelle a " + "WHERE a.codeApplication = ? " + "AND (a.dateModification >= ? "
          + "OR a.dateCreation >= ?)", new Object[] { codeApplication, dj, dj }, ln);
    }else {
      ln = this.getListe("FROM AideContextuelle a " + "WHERE (a.dateModification >= ? "
          + "OR a.dateCreation >= ?)", new Object[] { dj, dj }, ln);
    }
    return ln;
  }

  public ListeNavigation getListeAideContextuellePilotage(ListeNavigation liste, String codeUtilisateur) {
    this.activerFiltreApplicationUtilisateurPilote(codeUtilisateur);
    return this.getListe(liste);
  }

  @SuppressWarnings("unchecked")
  public List<AssociationAideContextuelle> getListeAssociation(String cleAide) {
    FiltreObjetJava filtre = new FiltreObjetJava();
    filtre.setCleAide(cleAide);

    List<ObjetJava> resultat = this.getObjetJavaDao().getListe(filtre);
    List<AssociationAideContextuelle> listeAssociation = new ArrayList<AssociationAideContextuelle>();

    for (ObjetJava objetJava : resultat) {

      AssociationAideContextuelle associationAideContextuelle = null;
      if ("IT".equals(objetJava.getType()) || "ON".equals(objetJava.getType()) || "SE".equals(objetJava.getType())) {
        associationAideContextuelle = new AssociationAideContextuelle();
        associationAideContextuelle.setIdOriginal(objetJava.getId());

        ObjetJava recherche = this.getObjetJavaDao().getObjetJavaParentParType(objetJava, "IT");
        associationAideContextuelle.setIdComposant(recherche != null ? recherche.getId(): null); // type IT

        recherche = this.getObjetJavaDao().getObjetJavaParentParType(objetJava, "ON");
        associationAideContextuelle.setIdOnglet(recherche != null ? recherche.getId(): null); // type  ON
        recherche = this.getObjetJavaDao().getObjetJavaParentParType(objetJava, "SE");
        associationAideContextuelle.setIdService(recherche != null ? recherche.getId(): null); // type SE
      }

      if (associationAideContextuelle != null) {
        listeAssociation.add(associationAideContextuelle);
      }
    }

    return listeAssociation;
  }

}