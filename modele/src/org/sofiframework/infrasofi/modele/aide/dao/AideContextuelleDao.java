/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.aide.dao;

import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.AideContextuelle;
import org.sofiframework.infrasofi.modele.entite.AssociationAideContextuelle;
import org.sofiframework.modele.spring.dao.BaseDao;

/**
 * Interface d'accès de données de l'entité Aide contextuelle.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface AideContextuelleDao extends BaseDao {
  /**
   * Obtenir une aide contextuelle à l'aide.
   * @param seqObjetSecurisable Séquence unique de l'objet
   * sécurisable qui possède l'aide.
   * @param codeLangue Langue de l'utilisateur
   * @return Entité d'aide contextuelle
   */
  AideContextuelle getAide (Long seqObjetSecurisable, String codeLangue);

  /**
   * Obtenir une liste de navigation qui contient les aides les plus récentes en fonction
   * d'un nombre de jours et pour une application en particulier.
   * 
   * @param ln Liste de navigation
   * @param nombreJours Nombre de jours
   * @param codeApplication Code de l'application
   * @return Liste de navigation
   */
  ListeNavigation getListe(ListeNavigation ln, Integer nombreJours, String codeApplication);

  /**
   * Obtenir la liste des aides contextuelles qui sont accèssible à un utilisateur pour le pilotage.
   * @param liste
   * @param codeUtilisateur
   * @return
   */
  ListeNavigation getListeAideContextuellePilotage(ListeNavigation liste, String codeUtilisateur);

  /**
   * Obtenir une aide contextuelle en fonction de sa clé et de la langue.
   * @param cleAide Clé de l'Aide
   * @param codeLangue Code de langue
   * @return Aide contextuelle
   */
  AideContextuelle getAide(String cleAide, String codeLangue);


  List<AssociationAideContextuelle> getListeAssociation(String cleAide);

}
