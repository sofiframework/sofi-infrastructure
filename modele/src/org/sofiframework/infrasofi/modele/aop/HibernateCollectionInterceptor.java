/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.aop;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.hibernate.collection.PersistentBag;
import org.springframework.beans.BeanUtils;

/**
 * Permet ded convertir les collections Hibernate pour le retour des méthode de services communs.
 * En particulier élimine la dépendance des clients envers hibernate.
 * 
 * @author Jean-Maxime Pelletier
 */
public class HibernateCollectionInterceptor {
  private static final Log log = LogFactory.getLog(HibernateCollectionInterceptor.class);

  public Object convertir(ProceedingJoinPoint pjp) throws Throwable {
    if (log.isDebugEnabled()) {
      log.debug("Avant remplacer collections...");
    }

    Object retour = pjp.proceed();
    this.convertirCollections(retour);

    if (log.isDebugEnabled()) {
      log.debug("Done");
    }
    return retour;
  }

  /**
   * Convertie les collections qui sont de types Hibernate en java.util.ArrayList
   * @param o Objet dont les propriétés doivent être converties
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  private void convertirCollections(Object o) {
    if (o != null) {
      PropertyDescriptor[] listePropriete = BeanUtils.getPropertyDescriptors(o.getClass());
      for (int i = 0; i < listePropriete.length; i++) {
        PropertyDescriptor propriete = listePropriete[i];

        try {

          /* Obtenir la méthode Get de la propriété
           */
          Method get = propriete.getReadMethod();
          if (get != null) {
            Object valeur = get.invoke(o, (Object[]) null);
            /*
             * La valeur est une collection Hibernate
             */
            if (valeur instanceof PersistentBag) {
              PersistentBag bag = (PersistentBag) valeur;
              if (bag != null) {

                /*
                 * Déplacer les éléments de la collection dans l'ArryList
                 */
                List liste = new ArrayList();
                for (Iterator j = bag.iterator(); j.hasNext();) {
                  Object v = j.next();
                  this.convertirCollections(v);
                  liste.add(v);
                }

                /*
                 * Remplacer la propriété
                 */
                Method set = propriete.getWriteMethod();
                set.invoke(o, new Object[] { liste });
              }
            } else if (valeur instanceof Collection) {
              Collection liste = (Collection) valeur;
              if (liste != null) {
                for (Iterator j = liste.iterator(); j.hasNext();) {
                  this.convertirCollections(j.next());
                }
              }
            }
          }
        } catch (Exception e) {
          throw new RuntimeException("Erreur lors de la conversion des collections.", e);
        }
      }
    }
  }
}
