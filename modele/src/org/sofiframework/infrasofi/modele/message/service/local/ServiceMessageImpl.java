/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.message.service.local;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.Message;
import org.sofiframework.infrasofi.modele.filtre.FiltreMessage;
import org.sofiframework.infrasofi.modele.message.dao.MessageDao;
import org.sofiframework.infrasofi.modele.service.ServiceMessage;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Service de gestion des messages.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceMessageImpl extends BaseServiceInfrastructureImpl implements ServiceMessage {

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceMessage#get(java.lang.String, java.lang.String)
   */
  public Message get(String identifiant, String codeLangue) {
    return get(identifiant, codeLangue, null);
  }

  public Message get(String identifiant, String codeLangue, String codeClient) {
    return ((MessageDao) getDao()).get(identifiant, codeLangue, codeClient);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceMessage#supprimer(java.lang.String, java.lang.String)
   */
  public void supprimerMessage(String identifiant) {
    throw new UnsupportedOperationException(
        "Ne pas utiliser cette méthode. Préférer supprimerMessage(String cleMessage, String codeClient).");
  }

  public void supprimerMessage(String cleMessage, String codeClient) {
    List<Message> liste = getListe(cleMessage, codeClient);
    for (int i = 0; i < liste.size(); i++) {
      super.supprimer(liste.get(i).getId());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceMessage#getListeMessagesPourTableauBord(org.sofiframework.
   * composantweb.liste.ListeNavigation, java.lang.Integer, java.lang.String)
   */
  public ListeNavigation getListeMessagesPourTableauBord(ListeNavigation liste, Integer nombreJours,
      String codeApplication) {
    return ((MessageDao) this.getDao()).getListe(liste, nombreJours, codeApplication);
  }

  public ListeNavigation getListeMessagePilotage(ListeNavigation liste) {
    String codeUtilisateur = this.getCodeUtilisateurEnCours();
    return this.getMessageDao().getListeMessagePilotage(liste, codeUtilisateur);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.sofiframework.infrasofi.modele.service.ServiceMessage#enregistrer(org.sofiframework.infrasofi.modele.entite
   * .Message, java.lang.String)
   */
  public void enregistrer(Message message, String ancienneCle) {
    if (!UtilitaireString.isVide(ancienneCle)) {
      this.modifierCleMessage(ancienneCle, message);
    } else {
      if (message.getDateCreation() == null) {
        this.ajouter(message);
      } else {
        this.modifier(message);
      }
    }
  }

  private List<Message> getListe(final String cle, final String codeClient) {
    FiltreMessage filtre = new FiltreMessage();
    filtre.setRechercheAvantEtApresAttribut(false);
    filtre.setCleMessage(cle);
    if (StringUtils.isEmpty(codeClient)) {
      filtre.ajouterAttributRechercheValeurNull("codeClient");
    } else {
      filtre.setCodeClient(codeClient);
    }
    @SuppressWarnings("unchecked")
    List<Message> liste = this.getDao().getListe(filtre);
    return liste;
  }

  private void modifierCleMessage(String cleOriginale, Message message) {
    /*
     * On doit aller chercher les messages dans toutes les langues.
     */
    List<Message> listeMessage = this.getListe(cleOriginale, null);
    for (Iterator<Message> i = listeMessage.iterator(); i.hasNext();) {
      Message m = i.next();
      this.supprimer(m);

      /*
       * On met un trace de modification meme si en réalité cest une création.
       */
      this.traceModification(m);

      /**
       * Pour la langue en cours on en profite pour ajouter le message qui est modifié par l'utilisateur.
       */
      if (m.getCodeLangue().equalsIgnoreCase(message.getCodeLangue())) {
        m = message;
      } else {
        m.setCleMessage(message.getCleMessage());
      }

      this.ajouter(m);
    }
  }

  private MessageDao getMessageDao() {
    return (MessageDao) this.getDao();
  }

}
