/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.message.service.commun;

import org.apache.commons.beanutils.PropertyUtils;
import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.securite.service.ServiceClient;
import org.sofiframework.infrasofi.modele.service.ServiceLibelle;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.service.local.BaseServiceImpl;

/**
 * Classe implémentant l'interface ServiceLibelleLocal et qui implémente la méthode getMessage(...).
 * <p>
 * 
 * @author Jean-Maxime Pelletier
 * @author Marie-Soleil L'Allier (Nurun inc.)
 * @version SOFI 2.0
 */
public class ServiceLibelleImpl extends BaseServiceImpl implements
org.sofiframework.application.message.service.ServiceLibelle {

  private String langueParDefaut = "fr_CA";

  private ServiceLibelle serviceLibelle = null;

  private ServiceClient serviceClient;

  /**
   * Obtenir un libellé spécifique.
   * 
   * @param identifiant
   *          idenitifiant unique du message
   * @param codeLangue
   *          code de langue du message
   * @param codeSexe
   *          sexe de l'utilisateur
   * @return un libellé
   * @deprecated depuis 3.2, remplacée par {@link ServiceLibelleImpl#get(String, String, String)}
   */
  @Deprecated
  public Libelle getLibelle(String identifiant, String codeLangue, String codeSexe) {
    return get(identifiant, codeLangue, null);
  }

  /**
   * Obtenir un libellé spécifique.
   * 
   * @param identifiant
   *          idenitifiant unique du message
   * @param codeLangue
   *          code de langue du message
   * @param codeClient
   *          code du client
   * @return un libellé
   * @since 3.2
   */
  public Libelle get(String identifiant, String codeLangue, String codeClient) {
    // Si aucune langue spécifié, demander la langue par défaut
    codeLangue = codeLangue == null ? langueParDefaut : codeLangue;
    Libelle libelle = null;
    try {
      org.sofiframework.infrasofi.modele.entite.Libelle entite = serviceLibelle
          .get(identifiant, codeLangue, codeClient);

      if (entite != null) {
        libelle = new Libelle();
        PropertyUtils.copyProperties(libelle, entite);
        libelle.setMessage(entite.getTexteLibelle());
        libelle.setCleMessage(entite.getCleLibelle());
        libelle.setPlaceholder(entite.getPlaceholder());
      }
    } catch (Exception e) {
      throw new ModeleException(e);
    }
    return libelle;
  }

  public void setLangueParDefaut(String langueParDefaut) {
    this.langueParDefaut = langueParDefaut;
  }

  public String getLangueParDefaut() {
    return langueParDefaut;
  }

  public ServiceLibelle getServiceLibelle() {
    return serviceLibelle;
  }

  public void setServiceLibelle(ServiceLibelle serviceLibelle) {
    this.serviceLibelle = serviceLibelle;
  }

  public ServiceClient getServiceClient() {
    return serviceClient;
  }

  public void setServiceClient(ServiceClient serviceClient) {
    this.serviceClient = serviceClient;
  }

}