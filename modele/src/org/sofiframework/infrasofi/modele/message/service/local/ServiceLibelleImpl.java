/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.message.service.local;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.Libelle;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.modele.filtre.FiltreLibelle;
import org.sofiframework.infrasofi.modele.ioc.DependServiceReferentiel;
import org.sofiframework.infrasofi.modele.message.dao.LibelleDao;
import org.sofiframework.infrasofi.modele.service.ServiceLibelle;
import org.sofiframework.infrasofi.modele.service.ServiceReferentiel;

/**
 * Service de gestion des libellés.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceLibelleImpl extends BaseServiceInfrastructureImpl implements ServiceLibelle,
DependServiceReferentiel {

  private ServiceReferentiel serviceReferentiel = null;

  public Libelle get(String cleLibelle, String codeLangue) {
    return get(cleLibelle, codeLangue, null);
  }

  public Libelle get(String cleLibelle, String codeLangue, String codeClient) {
    return getLibelleDao().get(cleLibelle, codeLangue, codeClient);
  }

  public void supprimerLibelle(String cleLibelle) {
    throw new UnsupportedOperationException(
        "Ne pas utiliser cette méthode. Préférer supprimerLibelle(String cleLibelle, String codeClient).");
  }

  public void supprimerLibelle(String cleLibelle, String codeClient) {
    List<Libelle> liste = this.getListe(cleLibelle, codeClient);
    for (int i = 0; i < liste.size(); i++) {
      super.supprimer(liste.get(i).getId());
    }
  }

  public ListeNavigation getListeLibellesPourTableauBord(ListeNavigation liste, Integer nombreJours,
      String codeApplication) {
    return getLibelleDao().getListe(liste, nombreJours, codeApplication);
  }

  public ListeNavigation getListeLibellePilotage(ListeNavigation liste) {
    String codeUtilisateur = this.getCodeUtilisateurEnCours();
    getLibelleDao().getListeLibellePilotage(liste, codeUtilisateur);
    return liste;
  }

  public void enregistrer(Libelle libelle, String ancienneCle) {
    if (ancienneCle != null) {
      this.modifierCleLibelle(ancienneCle, libelle);
      /*
       * Si objet de référentiel dépendant du libellé alors modifier le nom de l'objet avec le nouvel identifiant du
       * libellé.
       */
      ObjetJava objetJava = serviceReferentiel.getObjetJava(ancienneCle);
      if (objetJava != null) {
        objetJava.setNom(libelle.getCleLibelle());
        serviceReferentiel.enregistrerObjetJava(objetJava);
      }
    } else {
      if (libelle.getDateCreation() == null) {
        this.ajouter(libelle);
      } else {
        this.modifier(libelle);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private List<Libelle> getListe(final String cle, final String codeClient) {
    FiltreLibelle filtre = new FiltreLibelle();
    filtre.setRechercheAvantEtApresAttribut(false);
    filtre.setCleLibelle(cle);
    if (StringUtils.isEmpty(codeClient)) {
      filtre.ajouterAttributRechercheValeurNull("codeClient");
    } else {
      filtre.setCodeClient(codeClient);
    }
    List<Libelle> liste = this.getDao().getListe(filtre);
    return liste;
  }

  public void modifierCleLibelle(String cleOriginale, Libelle libelle) {
    /*
     * On doit aller chercher les message dans toutes les langues.
     */
    List<Libelle> listeLibelle = this.getListe(cleOriginale, null);
    for (Iterator<Libelle> i = listeLibelle.iterator(); i.hasNext();) {
      Libelle l = i.next();
      this.supprimer(l);

      /*
       * On met un trace de modification meme si en réalité cest une création.
       */
      this.traceModification(l);

      /**
       * Pour la langue en cours on en profite pour ajouter le message qui est modifié par l'utilisateur.
       */
      if (l.getCodeLangue().equalsIgnoreCase(libelle.getCodeLangue())) {
        l = libelle;
      } else {
        l.setCleLibelle(libelle.getCleLibelle());
      }

      this.ajouter(l);
    }
  }

  /**
   * Est-ce que le libellé avec clé et valeur spécifiques existent.
   * 
   * @param cle
   *          Clé du libellé
   * @param codeApplication
   *          Code de l'application
   * @return true existe, false sinon
   */
  @SuppressWarnings("unchecked")
  public boolean existe(String cle, String codeApplication) {
    FiltreLibelle filtre = new FiltreLibelle();
    filtre.setCleLibelle(cle);
    filtre.setCodeApplication(codeApplication);
    List<Libelle> liste = this.getDao().getListe(filtre);
    return liste != null && liste.size() > 0;
  }

  private LibelleDao getLibelleDao() {
    return (LibelleDao) this.getDao();
  }

  public ServiceReferentiel getServiceReferentiel() {
    return serviceReferentiel;
  }

  public void setServiceReferentiel(ServiceReferentiel serviceReferentiel) {
    this.serviceReferentiel = serviceReferentiel;
  }
}