/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.message.service.commun;

import org.apache.commons.beanutils.PropertyUtils;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.message.objetstransfert.MessageAvertissement;
import org.sofiframework.application.message.objetstransfert.MessageConfirmation;
import org.sofiframework.application.message.objetstransfert.MessageErreur;
import org.sofiframework.application.message.objetstransfert.MessageInformatif;
import org.sofiframework.application.message.objetstransfert.MessageJournalisation;
import org.sofiframework.application.securite.service.ServiceClient;
import org.sofiframework.infrasofi.modele.service.ServiceMessage;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.service.local.BaseServiceImpl;

/**
 * Classe implémentant l'interface ServiceMessageLocal et qui implémente la méthode getMessage(...).
 * <p>
 * 
 * @author Marie-Soleil L'Allier (Nurun inc.)
 * @version SOFI 2.0
 */
public class ServiceMessageImpl extends BaseServiceImpl implements
org.sofiframework.application.message.service.ServiceMessage {

  private String langueParDefaut = "fr_CA";

  private ServiceMessage serviceMessage = null;

  private ServiceClient serviceClient;

  /**
   * Obtenir un message spécifique.
   * 
   * @param identifiant
   *          idenitifiant unique du message
   * @param codeLangue
   *          code de langue du message
   * @param codeSexe
   *          sexe de l'utilisateur
   * @return un message
   * @deprecated depuis 3.2, remplacée par {@link ServiceMessage#get(String, String, String)}
   */
  @Deprecated
  public Message getMessage(String identifiant, String codeLangue, String codeSexe) {
    return get(identifiant, codeLangue, null);
  }

  /**
   * Obtenir un message spécifique.
   * 
   * @param identifiant
   *          idenitifiant unique du message
   * @param codeLangue
   *          code de langue du message
   * @param codeSexe
   *          sexe de l'utilisateur
   * @return un message
   * @since 3.2
   */
  public Message get(String identifiant, String codeLangue, String codeClient) {
    // Si aucune langue spécifié, demander la langue par défaut
    codeLangue = codeLangue == null ? langueParDefaut : codeLangue;
    Message message = null;
    try {
      org.sofiframework.infrasofi.modele.entite.Message entite = serviceMessage
          .get(identifiant, codeLangue, codeClient);

      if (entite != null) {
        String codeSeverite = entite.getCodeSeverite();

        if ("I".equals(codeSeverite)) {
          message = new MessageInformatif();
        }

        if ("A".equals(codeSeverite)) {
          message = new MessageAvertissement();
        }

        if ("C".equals(codeSeverite)) {
          message = new MessageConfirmation();
        }

        if ("E".equals(codeSeverite)) {
          message = new MessageErreur();
        }

        if ("J".equals(codeSeverite)) {
          message = new MessageJournalisation();
        }

        if (message == null) {
          message = new Message();
        }

        PropertyUtils.copyProperties(message, entite);
        message.setMessage(entite.getTexteMessage());
      }
    } catch (Exception e) {
      throw new ModeleException(e);
    }
    return message;
  }

  public void setLangueParDefaut(String langueParDefaut) {
    this.langueParDefaut = langueParDefaut;
  }

  public String getLangueParDefaut() {
    return langueParDefaut;
  }

  public ServiceMessage getServiceMessage() {
    return serviceMessage;
  }

  public void setServiceMessage(ServiceMessage serviceMessage) {
    this.serviceMessage = serviceMessage;
  }

  public ServiceClient getServiceClient() {
    return serviceClient;
  }

  public void setServiceClient(ServiceClient serviceClient) {
    this.serviceClient = serviceClient;
  }

}