/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.message.dao;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Libelle;

/**
 * Accès de données de l'entité de libellés.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface LibelleDao {

  /**
   * Permet d'obtenir un libellé à partir de sa clé de libellé et du code de langue (couple de propriétés anciennement
   * clé de l'entité Libelle)..<br>
   * Si une des 2 propriétés est null, la méthode retourne null.
   * 
   * @param cleLibelle
   * @param codeLangue
   * @param codeClient
   * @return un objet {@link Libelle}
   * @since 3.2
   */
  Libelle get(String cleLibelle, String codeLangue, String codeClient);

  /**
   * Obtenir la liste des libellés en fonction depuis un nombre de jour et d'une application.
   * 
   * @param ln
   *          Liste de navigation
   * @param nombreJour
   *          Nombre de jour
   * @param codeApplication
   *          COde unique de l'applciation
   * @return Liste de navigation
   */
  ListeNavigation getListe(ListeNavigation ln, Integer nombreJour, String codeApplication);

  /**
   * Permet d'obtenir les libellés qui disponible en pilotage pour un utilisateur.
   * 
   * @param liste
   * @param codeUtilisateur
   * @return
   */
  ListeNavigation getListeLibellePilotage(ListeNavigation liste, String codeUtilisateur);

}