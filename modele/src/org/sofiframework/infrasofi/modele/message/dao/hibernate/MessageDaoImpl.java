/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.message.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseDaoInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.Message;
import org.sofiframework.infrasofi.modele.filtre.FiltreMessage;
import org.sofiframework.infrasofi.modele.message.dao.MessageDao;

/**
 * Accès de données pour l'objet d'affaire message.
 * 
 * @author Jean-Maxime Pelletier
 */
public class MessageDaoImpl extends BaseDaoInfrastructureImpl implements MessageDao {

  /** constructeur par défaut */
  public MessageDaoImpl() {
    super(Message.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.sofiframework.infrasofi.modele.message.dao.hibernate.MessageDao#getListe(org.sofiframework.composantweb.liste
   * .ListeNavigation, java.lang.Integer, java.lang.String)
   */
  public ListeNavigation getListe(ListeNavigation ln, Integer nombreJour, String codeApplication) {
    Date date = new Date(System.currentTimeMillis() - nombreJour * 24 * 60 * 60 * 1000);
    if (StringUtils.isNotEmpty(codeApplication)) {
      ln = this.getListe("FROM Message m " + "WHERE m.codeApplication = ? " + "AND (m.dateModification >= ? "
          + "OR m.dateCreation >= ?) ", new Object[] { codeApplication, date, date }, ln);
    }else {
      ln = this.getListe("FROM Message m " + "WHERE m.dateModification >= ? "
          + "OR m.dateCreation >= ?) ", new Object[] {date, date }, ln);
    }
    return ln;
  }

  public ListeNavigation getListeMessagePilotage(ListeNavigation liste, String codeUtilisateur) {
    this.activerFiltreApplicationUtilisateurPilote(codeUtilisateur);
    return this.getListe(liste);
  }

  public Message get(String cleMessage, String codeLangue, String codeClient) {
    if (StringUtils.isEmpty(cleMessage) || StringUtils.isEmpty(codeLangue)) {
      return null;
    }
    FiltreMessage filtre = new FiltreMessage();
    filtre.ajouterExclureRechercheAvantApresValeur(new String[] {"cleMessage"});
    filtre.setCleMessage(cleMessage);
    filtre.setCodeLangue(codeLangue);
    if (StringUtils.isEmpty(codeClient)) {
      filtre.ajouterAttributRechercheValeurNull("codeClient");
    } else {
      filtre.setCodeClient(codeClient);
    }
    @SuppressWarnings("unchecked")
    List<Message> liste = getListe(filtre);
    return liste.isEmpty() ? null : liste.get(0);
  }
}
