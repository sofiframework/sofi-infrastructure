/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.message.dao.hibernate;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseDaoInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.Libelle;
import org.sofiframework.infrasofi.modele.filtre.FiltreLibelle;
import org.sofiframework.infrasofi.modele.message.dao.LibelleDao;

/**
 * Accès de données pour l'objet d'affaire libelle.
 * 
 * @author Jean-Maxime Pelletier
 */
public class LibelleDaoImpl extends BaseDaoInfrastructureImpl implements LibelleDao {

  /** constructeur par défaut */
  public LibelleDaoImpl() {
    super(Libelle.class);
  }

  public ListeNavigation getListe(ListeNavigation ln, Integer nombreJour, String codeApplication) {
    Date date = new Date(System.currentTimeMillis() - nombreJour * 24 * 60 * 60 * 1000);
    
    if (StringUtils.isNotEmpty(codeApplication)) {
      ln = this.getListe("FROM Libelle l " + "WHERE l.codeApplication = ? " + "AND (l.dateModification >= ? "
          + "OR l.dateCreation >= ?) ", new Object[] { codeApplication, date, date }, ln);
    }else {
      ln = this.getListe("FROM Libelle l " + "WHERE (l.dateModification >= ? "
          + "OR l.dateCreation >= ?) ", new Object[] {date, date }, ln);
    }

    return ln;
  }

  public ListeNavigation getListeLibellePilotage(ListeNavigation liste, String codeUtilisateur) {
    this.activerFiltreApplicationUtilisateurPilote(codeUtilisateur);
    return this.getListe(liste);
  }

  @Override
  public Serializable ajouter(Object entite) {
    Libelle l = (Libelle) entite;
    ajusterTexteLibelle(l);
    super.ajouter(entite);
    return (Serializable) entite;
  }

  @Override
  public void modifier(Object entite) {
    Libelle l = (Libelle) entite;
    ajusterTexteLibelle(l);
    super.modifier(entite);
  }

  /**
   * Retourne le libellé correspondant aux paramètres ; si plusieurs libellés sont trouvés (cela ne devrait pas
   * arriver), le premier libellé est retourné.
   * 
   * @param cleLibelle
   * @param codeLangue
   * @param codeClient
   * @return un objet {@link Libelle}.
   */
  public Libelle get(String cleLibelle, String codeLangue, String codeClient) {
    if (StringUtils.isEmpty(cleLibelle) || StringUtils.isEmpty(codeLangue)) {
      return null;
    }
    FiltreLibelle filtre = new FiltreLibelle();
    filtre.ajouterExclureRechercheAvantApresValeur(new String[] {"cleLibelle"});
    filtre.setCleLibelle(cleLibelle);
    filtre.setCodeLangue(codeLangue);
    if (StringUtils.isEmpty(codeClient)) {
      filtre.ajouterAttributRechercheValeurNull("codeClient");
    } else {
      filtre.setCodeClient(codeClient);
    }
    @SuppressWarnings("unchecked")
    List<Libelle> liste = getListe(filtre);
    Libelle l = liste.isEmpty() ? null : liste.get(0);
    concatenerTexteLibelle(l);
    return l;
  }

  @Override
  public Object get(Serializable id) {
    Libelle l = (Libelle) super.get(id);
    concatenerTexteLibelle(l);
    return l;
  }

  /**
   * Concatène les propriétés <code>texteLibelle2</code> et <code>texteLibelle3</code> avec le contenu de la propriété
   * <code>texteLibelle</code>.
   * 
   * @param l
   */
  private void concatenerTexteLibelle(Libelle l) {
    if (l != null && StringUtils.isNotEmpty(l.getTexteLibelle())) {
      StringBuilder message = new StringBuilder(l.getTexteLibelle());
      if (!StringUtils.isEmpty(l.getTexteLibelle2())) {
        message.append(l.getTexteLibelle2());
      }
      if (!StringUtils.isEmpty(l.getTexteLibelle3())) {
        message.append(l.getTexteLibelle3());
      }
      l.setTexteLibelle(message.toString());
    }
  }

  private void ajusterTexteLibelle(Libelle l) {

    if (l != null) {
      String texteLibelle = l.getTexteLibelle();
      if (texteLibelle.length() > 3800) {
        if (texteLibelle.length() > 7600) {
          l.setTexteLibelle2(texteLibelle.substring(3800, 7600));
        } else {
          l.setTexteLibelle2(texteLibelle.substring(3800, texteLibelle.length()));
        }
      }
      if (texteLibelle.length() > 7600) {
        l.setTexteLibelle3(texteLibelle.substring(7600, texteLibelle.length()));
      }
      if (l.getTexteLibelle().length() > 3800) {
        l.setTexteLibelle(texteLibelle.substring(0, 3800));
      } else {
        l.setTexteLibelle(texteLibelle);
      }
    }
  }

}