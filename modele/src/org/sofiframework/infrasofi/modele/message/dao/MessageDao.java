/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.message.dao;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Libelle;
import org.sofiframework.infrasofi.modele.entite.Message;

/**
 * Accès de données de l'entité de messages.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface MessageDao {

  /**
   * Permet d'obtenir un message à partir de sa clé de message et du code de langue (couple de propriétés anciennement
   * clé de l'entité Message).<br>
   * Si une des 2 propriétés est null, la méthode retourne null.
   * 
   * @param cleMessage
   * @param codeLangue
   * @param codeClient
   * @return un objet {@link Libelle}
   * @since 3.2
   */
  Message get(String cleMessage, String codeLangue, String codeClient);

  /**
   * Obtenir la liste des messages depuis un nombre de jours et pour une applicatrion.
   * 
   * @param ln
   *          Liste de navigation
   * @param nombreJour
   *          Nombre de jours
   * @param codeApplication
   *          Code unique de l'application
   * @return
   */
  ListeNavigation getListe(ListeNavigation ln, Integer nombreJour, String codeApplication);

  /**
   * Obtenir la liste des message qui peuvent être pilotés par un utilisateur.
   * 
   * @param liste
   * @param codeUtilisateur
   * @return
   */
  ListeNavigation getListeMessagePilotage(ListeNavigation liste, String codeUtilisateur);

}