/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.conversion.dao.hibernate;

import org.sofiframework.application.conversion.objettransfert.Conversion;
import org.sofiframework.infrasofi.modele.conversion.dao.ConversionDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

public class ConversionDaoImpl extends BaseDaoImpl implements ConversionDao {
  /**
   * Constructeur par défaut
   * Ce Dao est lié à l'entité Conversion
   */
  public ConversionDaoImpl(){
    super(Conversion.class);
  }
}
