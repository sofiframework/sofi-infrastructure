/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.conversion.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * Filtre de recherche permettant de rechercher une liste d'objet de conversion
 * par rapport à une série de critères.
 * 
 * @author Sébastien Cayer
 */
public class FiltreConversion extends ObjetFiltre {

  private static final long serialVersionUID = 3909921385419245644L;

  private String nom;
  private String codeApplicationSource;
  private String codeApplicationDestination;
  private String sourceId;

  public FiltreConversion() {
    super();
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getCodeApplicationSource() {
    return codeApplicationSource;
  }

  public void setCodeApplicationSource(String codeApplicationSource) {
    this.codeApplicationSource = codeApplicationSource;
  }

  public String getCodeApplicationDestination() {
    return codeApplicationDestination;
  }

  public void setCodeApplicationDestination(String codeApplicationDestination) {
    this.codeApplicationDestination = codeApplicationDestination;
  }

  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }

  public String getSourceId() {
    return sourceId;
  }
}