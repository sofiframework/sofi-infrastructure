/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.conversion.service.local;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sofiframework.application.conversion.objettransfert.Conversion;
import org.sofiframework.infrasofi.modele.conversion.filtre.FiltreConversion;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;

public class ServiceConversionImpl extends DaoServiceImpl implements
org.sofiframework.infrasofi.modele.conversion.service.local.ServiceConversion {

  @SuppressWarnings("unchecked")
  public Map<String, String> getListeConversion(String nom,
      String codeApplicationSource, String codeApplicationDestination) {
    FiltreConversion filtre = new FiltreConversion();
    filtre.setNom(nom);
    filtre.setCodeApplicationSource(codeApplicationSource);
    filtre.setCodeApplicationDestination(codeApplicationDestination);
    List<Conversion> listeConversion = this.getDao()
        .getListe(filtre);

    Map<String, String> mapConversion = new HashMap<String, String>();

    if (listeConversion != null) {
      for (Conversion conversion : listeConversion) {
        mapConversion.put(conversion.getSourceId(), conversion
            .getDestinationId());
      }
    }

    return mapConversion;
  }

}
