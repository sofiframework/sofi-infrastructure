/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.conversion.service.local;

import java.util.Map;

import org.sofiframework.modele.spring.service.DaoService;

public interface ServiceConversion extends DaoService {

  /**
   * Permet de retourne liste contenant l'identifiant de la source
   * comme clé et l'identifiant de destination comme
   * valeur.
   * 
   * @param nom
   * @param codeApplicationSource
   * @param codeApplicationDestination
   * @return
   */
  Map<String, String> getListeConversion(String nom, String codeApplicationSource, String codeApplicationDestination);
}
