/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.tache;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.sofiframework.application.tacheplanifiee.job.TacheStateful;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;

/**
 * Exécution du nettoyage des certificats. Consulter l'implémentation
 * du service / dao pour plus de détail.
 * 
 * @author j-m.pelletier
 */
public class TacheNettoyageCertificat extends TacheStateful {

  private static final Log log = LogFactory
      .getLog(TacheNettoyageCertificat.class);

  @Override
  public void executer(JobExecutionContext context) {
    if (log.isWarnEnabled()) {
      log.warn("Nettoyage des certificats...");
    }

    ModeleInfrastructure modele = (ModeleInfrastructure) this.getModele(context, "modeleInfrastructure");
    modele.getServiceCertificat().nettoyerCertificats();


    if (log.isWarnEnabled()) {
      log.warn("Nettoyage des certificats terminé.");
    }

  }
}
