package org.sofiframework.infrasofi.modele.securite.dao.commun;

import java.util.Set;

import org.sofiframework.modele.spring.dao.BaseDao;

public interface AccesExterneDao extends BaseDao {
 /**
  * Supprimer les acces externe d'un utilisateur non inclus dans la
  * liste spécifiés (not in).
  * @param utilisateurId l'identifiant de l'utilisateur
  * @param excluantIds liste d'identifiants utlisée pour la comparaison.
  */
  public void supprimerAccesExterne(final Long utilisateurId, final Set<Long> excluantIds);

}
