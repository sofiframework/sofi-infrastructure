package org.sofiframework.infrasofi.modele.securite.dao.gestion;

import java.util.List;
import java.util.Set;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.ProprieteUtilisateur;
import org.sofiframework.infrasofi.modele.filtre.FiltreProprieteUtilisateur;

public interface ProprieteUtilisateurDao {

  void ajouterProprieteUtilisateur(ProprieteUtilisateur propriete);

  void modifierProprieteUtilisateur(ProprieteUtilisateur propriete);

  ListeNavigation getListeProprieteUtilisateur(ListeNavigation ln);

  ProprieteUtilisateur getProprieteUtilisateur(Long id);

  List<ProprieteUtilisateur> getListeProprieteUtilisateur(FiltreProprieteUtilisateur filtre);

  void supprimerProprieteUtilisateur(final Long utilisateurId, final Set<Long> excluantIds);

}
