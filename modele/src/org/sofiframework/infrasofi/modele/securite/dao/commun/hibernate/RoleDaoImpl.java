package org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sofiframework.application.securite.filtre.FiltreRole;
import org.sofiframework.application.securite.objetstransfert.Role;
import org.sofiframework.infrasofi.modele.securite.dao.commun.RoleDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.sofiframework.utilitaire.UtilitaireListe;

public class RoleDaoImpl extends BaseDaoImpl implements RoleDao {

  /** constructeur */
  public RoleDaoImpl() {
    super(Role.class, "RoleCommun");
  }


  public List<Role> getListeRolePilotage(FiltreRole filtre, String codeUtilisateur) {
    String codeApplication = filtre.getCodeApplication();

    String[] roles = UtilitaireListe.getTableau(getListeRoleAccessible(codeUtilisateur, codeApplication),"codeRole");

    this.activerFiltreRolePilotage(codeApplication, codeUtilisateur, roles);
    return this.getListe(filtre);
  }


  public List getListeRoleDisponible(String codeUtilisateur, String codeApplication, String codeUtilisateurPilote) {
    String[] roles = UtilitaireListe.getTableau(getListeRoleAccessible(codeUtilisateur, codeApplication),"codeRole");

    this.activerFiltreRolePilotage(codeApplication, codeUtilisateurPilote, roles);

    List listeRole = this.getHibernateTemplate().findByNamedQueryAndNamedParam(
        "listeRoleDisponibleCommun",
        new String[] { "codeUtilisateur", "codeApplication" },
        new Object[] { codeUtilisateur, codeApplication });
    return listeRole;
  }


  public List<Role> getListeRolePilotage(String codeApplication, String codeStatut, String codeUtilisateur) {
    String[] roles = UtilitaireListe.getTableau(getListeRoleAccessible(codeUtilisateur, codeApplication),"codeRole");
    this.activerFiltreRolePilotage(codeApplication, codeUtilisateur, roles);

    List<Role> listeRole = this.getListe();

    return listeRole;
  }

  private List getListeRoleAccessible(String codeUtilisateur, String codeApplication) {
    List listeRole = this.getHibernateTemplate().findByNamedQueryAndNamedParam(
        "listeRoleUtilisateur",
        new String[] { "codeUtilisateur", "codeApplication" },
        new Object[] { codeUtilisateur, codeApplication });
    return listeRole;
  }


  /**
   * Les roles d'une application dont l'utilisateur possède ce rôle actif.
   * 
   * @param codeApplication
   * @param codeUtilisateur
   */
  private void activerFiltreRolePilotage(String codeApplication,
      String codeUtilisateur, String[] roles) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("codeUtilisateur", codeUtilisateur);
    params.put("codeApplication", codeApplication);
    params.put("dateCourante", new Date());
    if (roles.length > 0) {
      params.put("roles", roles[0]);
    }else {
      params.put("roles", "vide");
    }
    this.activerFiltreSession("filtreRolePilotage", params);
  }
}
