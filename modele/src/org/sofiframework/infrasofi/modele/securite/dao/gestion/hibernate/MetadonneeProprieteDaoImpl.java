/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.gestion.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.infrasofi.modele.BaseDaoInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.MetadonneePropriete;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.MetadonneeProprieteDao;

/**
 * Accès de données Metadonnées Propriétés.
 * 
 * @author Jean-Maxime Pelletier
 */
public class MetadonneeProprieteDaoImpl extends BaseDaoInfrastructureImpl implements MetadonneeProprieteDao {

  public MetadonneeProprieteDaoImpl() {
    super(MetadonneePropriete.class);
  }

  public MetadonneePropriete getMetadonneePropriete(Long id) {
    return (MetadonneePropriete) this.get(id);
  }
  
  /**
   * Switch if we need to exclude newsletter
   * @return true if we want to exclude the newsletter otherwise false;
   */
  private Boolean excludeNewsletter(){
    Boolean isExcludeNewsletter = GestionParametreSysteme.getInstance().getBoolean("excludeNewsletter");
    return  isExcludeNewsletter != null ? isExcludeNewsletter : false;  
  }


  @SuppressWarnings("unchecked")
  public List<MetadonneePropriete> getListeMetadonneePropriete(Boolean actif) {
    
    Criteria criteria = this.getSession().createCriteria(MetadonneePropriete.class);
    criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
    
    if (excludeNewsletter()) {
      criteria.add(Restrictions.isNull("referenceExterne"));
    }
    
    if (actif != null) {
      criteria.add(Restrictions.eq("actif", actif));
    }
    
    criteria.addOrder(Order.asc("ordrePresentation"));

    return criteria.list();
  }

  public void ajouterMetadonneePropriete(MetadonneePropriete metadonnee) {
    this.ajouter(metadonnee);
  }

  public void modifierMetadonneePropriete(MetadonneePropriete metadonnee) {
    this.modifier(metadonnee);
  }
}
