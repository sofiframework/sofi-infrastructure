/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.gestion.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseDaoInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.modele.filtre.FiltreObjetReferentielRole;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.ObjetJavaDao;
import org.sofiframework.modele.spring.dao.hibernate.CritereSupplementaire;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Accès de données de l'entité Objet Java.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ObjetJavaDaoImpl extends BaseDaoInfrastructureImpl implements ObjetJavaDao {

  public ObjetJavaDaoImpl() {
    super(ObjetJava.class);
  }

  public ObjetJava getObjetJavaParentParType(ObjetJava parent, String type) {
    ObjetJava retour = parent;

    if (parent != null && !parent.getType().equalsIgnoreCase(type)) {
      retour = getObjetJavaParentParType(parent.getObjetJavaParent(), type);
    }

    return retour;
  }

  public List<ObjetJava> getListeObjetJavaParType(ObjetJava parent, String type) {
    List<ObjetJava> liste = new ArrayList<ObjetJava>();

    if (parent.getType().equalsIgnoreCase(type)) {
      liste.add(parent);
    }

    for (ObjetJava enfant : parent.getListeObjetJavaEnfant()) {
      liste.addAll(this.getListeObjetJavaParType(enfant, type));
    }

    return liste;
  }

  @SuppressWarnings("unchecked")
  public List<ObjetJava> getListeServiceParApplication(String codeApplication) {
    String hql = "from ObjetJava where codeApplication = :codeApplication"
        + "and type = 'SE' and codeStatut = 'A')";
    List<ObjetJava> listeObjetJava = getHibernateTemplate().find(hql);
    return listeObjetJava;
  }

  public ListeNavigation getListeObjetJavaPourSecurite(ListeNavigation liste, String codeUtilisateur) {
    // Appliquer la sécurité sur les applications et les roles qui sont accessibles à l'utilisateur.
    this.activerFiltreApplicationUtilisateurPilote(codeUtilisateur);

    final FiltreObjetReferentielRole filtre = (FiltreObjetReferentielRole) liste.getFiltre();
    final Boolean nonInclu = filtre.getCodeRoleNonInclus();
    filtre.setCodeRoleNonInclus(null);
    final String codeRole = filtre.getCodeRole();
    filtre.setCodeRole(null);

    liste = this.getListe(liste, new CritereSupplementaire() {
      @Override
      public void ajouter(DetachedCriteria criteres) {
        if (!UtilitaireString.isVide(codeRole)) {
          Criterion critere = Restrictions.eq("codeRole", codeRole);
          if (nonInclu != null && nonInclu) {
            critere = Restrictions.not(critere);
          }
          criteres.add(critere);
        }
      }
    });

    filtre.setCodeRoleNonInclus(nonInclu);
    filtre.setCodeRole(codeRole);

    return liste;
  }

  @SuppressWarnings("unchecked")
  public List<ObjetJava> getListeObjetJavaParParent(Long idService, String type) {
    String hql = "FROM ObjetJava WHERE idParent = :idParent AND type = :type ";
    List<ObjetJava> listeObjet = getHibernateTemplate().findByNamedParam(hql,
        new String[] { "idParent", "type" }, new Object[] { idService, type });
    return listeObjet;
  }

  @SuppressWarnings("unchecked")
  public List<ObjetJava> getListeObjetJavaEnfantsPourObjetJava(
      Long idObjetJavaSource) {
    String hql = "FROM ObjetJava WHERE idParent = :idParent ";
    List<ObjetJava> listeObjet = getHibernateTemplate().findByNamedParam(hql,
        new String[] { "idParent" }, new Object[] { idObjetJavaSource });
    return listeObjet;
  }

  public ListeNavigation getListeObjetJavaPilotage(
      ListeNavigation liste, String codeUtilisateur) {
    this.activerFiltreApplicationUtilisateurPilote(codeUtilisateur);
    return this.getListe(liste);
  }
}