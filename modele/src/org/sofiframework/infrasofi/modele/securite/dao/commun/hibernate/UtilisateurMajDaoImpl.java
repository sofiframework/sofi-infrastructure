/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.sofiframework.application.securite.objetstransfert.Propriete;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.infrasofi.modele.securite.dao.commun.UtilisateurMajDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.sofiframework.securite.encryption.UtilitaireEncryption;

/**
 * Accès de données pour l'objet d'affaire pour la mise à jour d'utilisateur.
 * <p> *
 * 
 * @author Jean-Francois Brassard (Nurun inc.)
 * @version SOFI 3.0.02
 */
public class UtilisateurMajDaoImpl extends BaseDaoImpl implements UtilisateurMajDao {

  /** constructeur */
  public UtilisateurMajDaoImpl() {
    super(Utilisateur.class, "UtilisateurMajCommun");
  }

  /**
   * Méthode redéfinie pour encrypter le mot de passe avant d'appeler la méthode
   * ajouter de la classe mère
   */
  @Override
  public Serializable ajouter(Object entite) {
    this.setSessionAutoFlush();
    Utilisateur utilisateur = (Utilisateur) entite;
    String motPasse = utilisateur.getMotPasse();

    if (motPasse != null) {
      utilisateur.setMotPasse(UtilitaireEncryption.digest(motPasse));
    }

    boolean utilisateurExterne = false;

    if (utilisateur.getCodeUtilisateur() == null) {
      Random random = new Random(1000000000);

      utilisateur.setCodeUtilisateur("ext"+random.nextLong());

      utilisateurExterne = true;
    }

    utilisateur.setDateCreation(new java.util.Date());
    Long id = (Long)super.ajouter(entite);

    if (utilisateurExterne) {
      utilisateur.setCodeUtilisateur(id.toString());
    }
    return utilisateur;
  }

  /**
   * Méthode redéfinie pour encrypter le mot de passe avant d'appeler la méthode
   * modifier de la classe mère
   */
  @Override
  public void modifier(Object entite) {
    this.setSessionAutoFlush();
    Utilisateur utilisateur = (Utilisateur) entite;

    if (utilisateur.isNouveauMotPasse()) {
      if (!StringUtils.isEmpty(utilisateur.getMotPasse())) {
        utilisateur.setMotPasse(UtilitaireEncryption.digest(utilisateur.getMotPasse()));
      }else {
        utilisateur.setMotPasse(null);
      }
    
    } else {
      Utilisateur ancienUtilisateur = getById(utilisateur.getId());
      this.getHibernateTemplate().evict(ancienUtilisateur);
      if (ancienUtilisateur != null) {
        utilisateur.setMotPasse(ancienUtilisateur.getMotPasse());
      }
    }

    super.modifier(entite);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Object get(Serializable codeUtilisateur) {
    Utilisateur utilisateur = null;
    String hqlId = "from UtilisateurMajCommun u "
        + "where u.codeUtilisateur = ? ";
    List<Utilisateur> listeUtilisateur = this.getHibernateTemplate().find(
        hqlId, codeUtilisateur);

    if (listeUtilisateur != null && !listeUtilisateur.isEmpty()) {
      utilisateur = listeUtilisateur.get(0);
    }

    return utilisateur;
  }

  public Utilisateur getById(Serializable id) {
    Utilisateur utilisateur = null;
    String hqlId = "from UtilisateurMajCommun u "
        + "where u.id = ? ";
    List<Utilisateur> listeUtilisateur = this.getHibernateTemplate().find(
        hqlId, id);

    if (listeUtilisateur != null && !listeUtilisateur.isEmpty()) {
      utilisateur = listeUtilisateur.get(0);
    }

    return utilisateur;
  }


  public Utilisateur supprimerUtilisateur(Serializable id) {
    Utilisateur utilisateur = getById(id);
    utilisateur.setIsDeleted(Boolean.TRUE);
    modifier(utilisateur);
    return utilisateur;
  }

  private void setSessionAutoFlush() {
    try {
      getSession().setFlushMode(FlushMode.AUTO);
    } catch (HibernateException e) {
      throw convertHibernateAccessException(e);
    }
  }
  
  @SuppressWarnings("unchecked")
  public List<Utilisateur> getListeUtilisateurPourPropriete(String listeId, Integer indexFirstResult, Integer nbResult) {

    Criteria criteria = this.getSession().createCriteria("UtilisateurMajCommun");
    criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

    criteria.add(Restrictions.eq("isDeleted", "N"));

    DetachedCriteria propertiesSubquery = DetachedCriteria.forEntityName("Propriete", "p")
        .add(Restrictions.eq("p.valeur", "1")).add(Restrictions.ilike("p.refProperieteExterne", listeId, MatchMode.ANYWHERE));
    propertiesSubquery.setProjection(Projections.property("p.utilisateurId"));

    criteria.add(Subqueries.propertyIn("id", propertiesSubquery));
    
    if (nbResult != null) {
      if (indexFirstResult != null) {
        criteria.setFirstResult(indexFirstResult);
      }
      criteria.setMaxResults(nbResult);

      criteria.addOrder(Order.asc("dateCreation"));
      criteria.addOrder(Order.asc("id"));
    }

    return criteria.list();
  }
}