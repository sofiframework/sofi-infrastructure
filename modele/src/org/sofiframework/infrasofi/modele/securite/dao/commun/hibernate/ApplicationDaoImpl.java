/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.infrasofi.modele.securite.dao.commun.ApplicationDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.springframework.orm.hibernate3.HibernateAccessor;

/**
 * Accès de données pour l'objet d'affaire Application.
 * <p>
 * 
 * @author Marie-Soleil L'Allier (Nurun inc.)
 * @version SOFI 2.0
 */
public class ApplicationDaoImpl extends BaseDaoImpl implements ApplicationDao {

  /* constructeur */
  public ApplicationDaoImpl() {
    super(Application.class, "ApplicationCommun");
  }

  public Application getApplication(String codeApplication) {
    Application application = (Application) getHibernateTemplate().get(
        "ApplicationCommun", codeApplication);
    return application;
  }

  public Application getApplication(String codeApplication,
      boolean objetActifSeulement) {
    Application application = this.getApplication(codeApplication, null,
        objetActifSeulement);
    return application;
  }

  @SuppressWarnings("unchecked")
  public Application getApplication(String codeApplication, String codeFacette,
      boolean objetActifSeulement) {
    getHibernateTemplate().setFlushMode(HibernateAccessor.FLUSH_NEVER);

    Application application = (Application) getHibernateTemplate().get(
        "ApplicationCommun", codeApplication);

    if (application != null) {
      List<String> listeParametre = new ArrayList<String>();

      String hql = "from ObjetSecurisable o where o.codeApplication = ? ";
      listeParametre.add(codeApplication);

      if (objetActifSeulement) {
        hql += "and (dateDebutActivite is null or dateDebutActivite <= current_date) ";
        hql += "and (dateFinActivite is null or dateFinActivite >= current_date) ";
      }

      if (codeFacette != null) {
        hql += "and (o.codeFacette is null or o.codeFacette = ?) ";
        listeParametre.add(codeFacette);
      }

      hql += "order by o.ordrePresentation";

      List<ObjetSecurisable> liste = getHibernateTemplate().find(hql,
          listeParametre.toArray());
      List<ObjetSecurisable> racines = new ArrayList<ObjetSecurisable>();

      for (Iterator<ObjetSecurisable> it = liste.iterator(); it.hasNext();) {
        ObjetSecurisable obj = it.next();
        creerListeEnfant(obj);
        ObjetSecurisable parent = obj.getObjetSecurisableParent();
        if (parent != null) {
          creerListeEnfant(parent).add(obj);
        } else {
          racines.add(obj);
        }
      }
      application.setListeObjetSecurisables(racines);
    }

    return application;
  }

  @SuppressWarnings("unchecked")
  private static List<ObjetSecurisable> creerListeEnfant(ObjetSecurisable parent) {
    List<ObjetSecurisable> enfants = parent.getListeObjetSecurisableEnfants();
    if (!(enfants instanceof ArrayList)) {
      enfants = new ArrayList<ObjetSecurisable>();
      parent.setListeObjetSecurisableEnfants(enfants);
    }
    return enfants;
  }
}