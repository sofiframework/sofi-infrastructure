/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.gestion;

import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.modele.spring.dao.BaseDao;

public interface ApplicationDao extends BaseDao {
  /**
   * Liste des application paginée qui est accessible à l'utilisateur. Un
   * utilisateur ne peut voir que les applications qu'il a créé ou dont il
   * possède un rôle de premier niveau valide.
   * 
   * @param liste
   *          Liste de navigation
   * 
   * @return Liste de navigation
   */
  @SuppressWarnings("unchecked")
  List getListeApplicationAccessible(String codeUtilisateur,
      Boolean accesRoleParent);

  /**
   * Obtenir une application pour un code application et un code de facette.
   * 
   * @param codeApplication
   * @param codeFacette
   * @return
   */
  Application getApplication(String codeApplication, String codeFacette);

  /**
   * Permet d'obtenir la liste des applications qui sont accessibles.
   * Essentiellement c'est les applications que l'utilisateur à lui même créé ou
   * dont il possède un rôle. Si accesRole parent est a vrai alors il faut que
   * se soit un rôle de premier niveau (sans parent).
   * 
   * @return
   */
  ListeNavigation getListeApplicationAccessible(String codeUtilisateur,
      ListeNavigation liste, Boolean accesRoleParent);
}