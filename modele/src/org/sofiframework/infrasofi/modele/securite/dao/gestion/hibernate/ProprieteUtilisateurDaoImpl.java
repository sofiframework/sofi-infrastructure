package org.sofiframework.infrasofi.modele.securite.dao.gestion.hibernate;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseDaoInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.ProprieteUtilisateur;
import org.sofiframework.infrasofi.modele.filtre.FiltreProprieteUtilisateur;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.ProprieteUtilisateurDao;
import org.springframework.orm.hibernate3.HibernateCallback;

public class ProprieteUtilisateurDaoImpl extends BaseDaoInfrastructureImpl implements ProprieteUtilisateurDao {

  public ProprieteUtilisateurDaoImpl() {
    super(ProprieteUtilisateur.class);
  }

  public void ajouterProprieteUtilisateur(ProprieteUtilisateur propriete) {
    this.ajouter(propriete);
  }

  public void modifierProprieteUtilisateur(ProprieteUtilisateur propriete) {
    this.modifier(propriete);
  }

  public ListeNavigation getListeProprieteUtilisateur(ListeNavigation ln) {
    return this.getListe(ln);
  }

  @SuppressWarnings("unchecked")
  public List<ProprieteUtilisateur> getListeProprieteUtilisateur(FiltreProprieteUtilisateur filtre) {
    return this.getListe(filtre);
  }

  public ProprieteUtilisateur getProprieteUtilisateur(Long id) {
    return (ProprieteUtilisateur) this.get(id);
  }

  /**
   * Supprimer les propriétés d'un utilisateur qui ne sont pas inclus dans la
   * liste spécifiés (not in).
   */
  public void supprimerProprieteUtilisateur(final Long utilisateurId, final Set<Long> excluantIds) {
    this.getHibernateTemplate().execute(new HibernateCallback<Boolean>() {
      public Boolean doInHibernate(Session session) throws HibernateException, SQLException {
        Query delete = session.createQuery(
            "delete from ProprieteUtilisateur pu " +
                "where pu.id not in (:listeIds) " +
            "and pu.utilisateurId = :utilisateurId");
        delete.setParameterList("listeIds", Arrays.asList(excluantIds.toArray()));
        delete.setLong("utilisateurId", utilisateurId);
        delete.executeUpdate();
        return Boolean.TRUE;
      }
    });
  }
}
