package org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.sofiframework.application.securite.objetstransfert.AccesExterne;
import org.sofiframework.infrasofi.modele.securite.dao.commun.AccesExterneDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.springframework.orm.hibernate3.HibernateCallback;

public class AccesExterneDaoImpl extends BaseDaoImpl implements AccesExterneDao {


  /** constructeur */
  public AccesExterneDaoImpl() {
    super(AccesExterne.class, "AccesExterneCommun");
  }
  
  /**
   * Supprimer les acces externes d'un utilisateur qui ne sont pas inclus dans la
   * liste spécifiés (not in).
   */
  public void supprimerAccesExterne(final Long utilisateurId,
      final Set<Long> excluantIds) {
    this.getHibernateTemplate().execute(new HibernateCallback<Boolean>() {
      public Boolean doInHibernate(Session session) throws HibernateException,
          SQLException {
        
        Query delete = null;
        if (excluantIds != null && !excluantIds.isEmpty()) {
          delete = session.createQuery("delete from AccesExterneCommun ae "
              + "where ae.id not in (:listeIds)"
              + "and ae.idUtilisateur = :utilisateurId");
          delete.setParameterList("listeIds",
              Arrays.asList(excluantIds.toArray()));
        } else {
          delete = session
              .createQuery("delete from AccesExterneCommun ae where"
                  + " ae.idUtilisateur = :utilisateurId");

        }
        delete.setLong("utilisateurId", utilisateurId);
        delete.executeUpdate();
        return Boolean.TRUE;
      }
    });
  }

}
