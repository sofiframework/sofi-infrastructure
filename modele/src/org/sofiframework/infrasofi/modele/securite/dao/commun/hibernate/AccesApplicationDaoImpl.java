package org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate;

import org.sofiframework.application.securite.objetstransfert.AccesApplication;
import org.sofiframework.infrasofi.modele.securite.dao.commun.AccesApplicationDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

public class AccesApplicationDaoImpl extends BaseDaoImpl implements AccesApplicationDao {


  /** constructeur */
  public AccesApplicationDaoImpl() {
    super(AccesApplication.class, "AccesApplication");
  }

}
