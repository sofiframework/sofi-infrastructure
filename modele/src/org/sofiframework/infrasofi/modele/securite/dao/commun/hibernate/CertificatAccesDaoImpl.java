/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.sofiframework.application.securite.objetstransfert.CertificatAcces;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.infrasofi.modele.securite.dao.commun.CertificatAccesDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.sofiframework.modele.spring.dao.hibernate.TraitementJdbc;
import org.sofiframework.utilitaire.UtilitaireDate;
import org.springframework.orm.hibernate3.HibernateCallback;

/**
 * Accès de données associé à l'entité CertificatAcces
 * 
 * @author Jean-Maxime Pelletier
 */
public class CertificatAccesDaoImpl extends BaseDaoImpl implements CertificatAccesDao {

  private static final Log log = LogFactory.getLog(CertificatAccesDaoImpl.class);

  /* Constructeur par défaut */
  public CertificatAccesDaoImpl() {
    super(CertificatAcces.class);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.securite.dao.hibernate.
   * CertificatAccesDao#getCertificatAccesValide(java.lang.String)
   */
  public CertificatAcces getCertificatAccesValide(String codeUtilisateur) {
    CertificatAcces certificat = null;
    List<CertificatAcces> listeCertificat = getListeCertificatAccesValide(codeUtilisateur);
    if (listeCertificat != null && listeCertificat.size() > 0) {
      certificat = listeCertificat.get(0);
    }
    return certificat;
  }

  public List<CertificatAcces> getListeCertificatAccesValide(String codeUtilisateur) {
    @SuppressWarnings("unchecked")
    List<CertificatAcces> listeCertificat = getHibernateTemplate().find(
        "from CertificatAcces c " + "where c.codeUtilisateur = ? "
            + "and (c.dateDeconnexion is null OR c.dateDeconnexion > ?) " + "order by c.dateConnexion desc",
        new Object[] { codeUtilisateur, new Date() });
    return listeCertificat;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate.
   * CertificatAccesDao#nettoyerCertificats(java.lang.Long, java.lang.Long)
   */
  @SuppressWarnings("unchecked")
  public void nettoyerCertificats(Integer nombreHeureInvalidationCertificatValide,
      Integer nombreJourDestructionCertificatDeconnete) {
    final Date dateConnexion = new Date(System.currentTimeMillis() - nombreHeureInvalidationCertificatValide * 3600000);
    final Date dateDeconnexion = UtilitaireDate.ajouter_jr_AAAA_MM_JJ_En_UtilDate(new Date(),
        nombreJourDestructionCertificatDeconnete * -1);
    getHibernateTemplate().execute(new HibernateCallback() {
      public Object doInHibernate(Session session) throws HibernateException, SQLException {

        /*
         * On doit mettre une date de déconnexion aux certificats qui ont plus
         * d'un certain nombre d'heure d'age.
         */
        Query modifications = session.createQuery("update CertificatAcces c " + "set c.dateDeconnexion = :dateDuJour "
            + "where (c.dateDeconnexion is null and c.dateConnexion <= :dateConnexion)");
        modifications.setDate("dateDuJour", new Date());
        modifications.setDate("dateConnexion", dateConnexion);
        modifications.executeUpdate();

        /*
         * On doit supprimer les certificat qui sont trop vieux et qui ont une
         * date déconnexion plus ancienne que la date calculée avec le nombr de
         * jours.
         */
        Query suppressions = session.createQuery("delete from CertificatAcces c "
            + "where (c.dateDeconnexion is not null and c.dateDeconnexion <= :dateDeconnexion)");
        suppressions.setDate("dateDeconnexion", dateDeconnexion);
        suppressions.executeUpdate();

        return null;
      }
    });
  }

  public boolean notifierUtilisateurDernierAcces(final String codeUtilisateur, final String codeFournisseurAcces,
      final String codeClient, final String certificat, final String ip, final boolean majCertificatAcces) {

    this.effectuerTraitementJdbc(new TraitementJdbc() {
      @Override
      public Object effectuer(Connection connexion) throws SQLException {

        // Create PrepareStatement object
        Statement statement = connexion.createStatement();

        try {
          String SQL_UTILISATEUR = "UPDATE utilisateur SET date_dernier_acces = SYSDATE, code_dernier_acces_externe = '"
              + codeFournisseurAcces + "', code_dernier_acces_client = '" + codeClient
              + "', certificat_dernier_acces = '" + certificat + "' WHERE code_utilisateur = '" + codeUtilisateur + "'";

          // Set auto-commit to false
          connexion.setAutoCommit(false);

          // Add it to the batch
          statement.addBatch(SQL_UTILISATEUR);

          if (majCertificatAcces) {

            String SQL_CERTIFICAT_ACCES = "UPDATE certificat_acces SET date_dernier_acces = SYSDATE, nb_acces = nb_acces + 1"
                + " WHERE certificat = '" + certificat + "'";

            if (ip != null) {
              SQL_CERTIFICAT_ACCES = "UPDATE certificat_acces SET date_dernier_acces = SYSDATE, nb_acces = nb_acces + 1, ip = '"
                  + ip + "'" + " WHERE certificat = '" + certificat + "'";
            }

            // Add it to the batch
            statement.addBatch(SQL_CERTIFICAT_ACCES);

          }

          int[] retour = statement.executeBatch();

          connexion.commit();

          if (retour.length > 0) {
            return true;
          } else {
            return false;
          }

        } catch (SQLException e) {
          log.error(new SOFIException(e));
          throw e;
        } finally {
          if (statement != null) {
            statement.close();
          }
        }
      }
    });

    return false;
  }

  public boolean notifierUtilisateurDernierAcces(final String codeUtilisateur) {

    this.effectuerTraitementJdbc(new TraitementJdbc() {
      @Override
      public Object effectuer(Connection connexion) throws SQLException {
        CallableStatement stmt = null;
        try {

          stmt = connexion
              .prepareCall("UPDATE utilisateur SET date_dernier_acces = SYSDATE " + "WHERE code_utilisateur = ? ");
          stmt.setString(1, codeUtilisateur);

          int retour = stmt.executeUpdate();

          if (retour > 0) {
            return true;
          } else {
            return false;
          }

        } catch (SQLException e) {
          log.error(new SOFIException(e));
          throw e;
        } finally {
          if (stmt != null) {
            stmt.close();
          }
        }
      }
    });
    return false;
  }

  public boolean modifierCertificat(final String certificatCourant, final String certificatNouveau) {

    this.effectuerTraitementJdbc(new TraitementJdbc() {
      @Override
      public Object effectuer(Connection connexion) throws SQLException {
        CallableStatement stmt = null;
        try {

          stmt = connexion.prepareCall("UPDATE certificat_acces SET certificat = ? " + "WHERE certificat = ? ");
          stmt.setString(1, certificatNouveau);
          stmt.setString(2, certificatCourant);
          int retour = stmt.executeUpdate();

          if (retour > 0) {
            return true;
          } else {
            return false;
          }

        } catch (SQLException e) {
          log.error(new SOFIException(e));
          throw e;
        } finally {
          if (stmt != null) {
            stmt.close();
          }
        }
      }
    });
    return false;
  }
}