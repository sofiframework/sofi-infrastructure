/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.gestion.hibernate;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Role;
import org.sofiframework.infrasofi.modele.filtre.FiltreRole;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.RoleDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.sofiframework.utilitaire.UtilitaireListe;

/**
 * Accès de données de gestion des rôles utilisateurs.
 * 
 * @author Jean-Maxime.Pelletier
 */
public class RoleDaoImpl extends BaseDaoImpl implements RoleDao {
  public RoleDaoImpl() {
    super(Role.class);
  }

  public ListeNavigation getListeRolePilotage(ListeNavigation liste, String codeUtilisateur) {
    FiltreRole filtre = (FiltreRole) liste.getFiltre();
    String codeApplication = filtre.getCodeApplication();
    String[] roles = UtilitaireListe.getTableau(getListeRoleAccessible(codeUtilisateur, codeApplication),"codeRole");

    this.activerFiltreRolePilotage(codeApplication, codeUtilisateur, roles);
    return this.getListe(liste);
  }

  public List getListeRoleDisponible(String codeUtilisateur, String codeApplication, String codeUtilisateurPilote) {
    String[] roles = UtilitaireListe.getTableau(getListeRoleAccessible(codeUtilisateur, codeApplication),"codeRole");

    this.activerFiltreRolePilotage(codeApplication, codeUtilisateurPilote, roles);

    List listeRole = this.getHibernateTemplate().findByNamedQueryAndNamedParam(
        "listeRoleDisponible",
        new String[] { "codeUtilisateur", "codeApplication" },
        new Object[] { codeUtilisateur, codeApplication });
    return listeRole;
  }

  @SuppressWarnings("unchecked")
  public List getListeRolePilotage(String codeApplication, String codeStatut, String codeUtilisateur) {
    String[] roles = UtilitaireListe.getTableau(getListeRoleAccessible(codeUtilisateur, codeApplication),"codeRole");
    this.activerFiltreRolePilotage(codeApplication, codeUtilisateur, roles);

    List<Role> listeRole = this.getListe();

    return listeRole;
  }

  /**
   * Les roles d'une application dont l'utilisateur possède ce rôle actif.
   * 
   * @param codeApplication
   * @param codeUtilisateur
   */
  private void activerFiltreRolePilotage(String codeApplication,
      String codeUtilisateur, String[] roles) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("codeUtilisateur", codeUtilisateur);
    params.put("codeApplication", codeApplication);
    params.put("dateCourante", new Date());
    if (roles.length > 0) {
      params.put("roles", roles[0]);
    }else {
      params.put("roles", "vide");
    }

    this.activerFiltreSession("filtreRolePilotage", params);
  }


  private List getListeRoleAccessible(String codeUtilisateur, String codeApplication) {
    List listeRole = this.getHibernateTemplate().findByNamedQueryAndNamedParam(
        "listeRoleUtilisateur",
        new String[] { "codeUtilisateur", "codeApplication" },
        new Object[] { codeUtilisateur, codeApplication });
    return listeRole;
  }

}
