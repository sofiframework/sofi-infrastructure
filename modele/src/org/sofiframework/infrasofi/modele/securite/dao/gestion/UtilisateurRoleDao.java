/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.gestion;

import java.util.List;

import org.sofiframework.modele.spring.dao.BaseDao;

/**
 * Accès de données de l'entité utilisateur-role.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface UtilisateurRoleDao extends BaseDao {

  /**
   * Méthode qui retourne la liste de tous les UtilisateurRole pour un utilisateur en particulier et ce
   * pour une application en particulier.
   * <p>
   * @param codeUtilisateur l'identifiant unique de l'utilisateur pour lequel on récupère les
   * @param codeApplication l'identifiant unique de l'application pour lequel on recherche des rôles
   * @return la liste de tous les rôles disponibles pour l'utilisateur et l'application
   * @throws org.sofiframework.modele.exception.ModeleException Erreur générique lors de l'appel du modèle
   */
  List getListeUtilisateurRole(String codeUtilisateur, String codeApplication);
}