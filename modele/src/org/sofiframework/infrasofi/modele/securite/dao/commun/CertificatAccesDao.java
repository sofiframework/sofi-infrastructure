/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.commun;

import java.util.List;

import org.sofiframework.application.securite.objetstransfert.CertificatAcces;
import org.sofiframework.modele.spring.dao.BaseDao;

public interface CertificatAccesDao extends BaseDao {

  public abstract CertificatAcces getCertificatAccesValide(String codeUtilisateur);

  public abstract void nettoyerCertificats(
      Integer nombreJourInvalidationCertificatValide,
      Integer nombreJourDestructionCertificatDeconnete);

  public List<CertificatAcces> getListeCertificatAccesValide(String codeUtilisateur);
  public boolean notifierUtilisateurDernierAcces(String codeUtilisateur, String codeFournisseurAcces, String codeClient, String certificat, String ip, boolean majCertificatAcces); 
  public boolean notifierUtilisateurDernierAcces(String codeUtilisateur);    
  public boolean modifierCertificat(String certificatCourant, String certificatNouveau);   
}