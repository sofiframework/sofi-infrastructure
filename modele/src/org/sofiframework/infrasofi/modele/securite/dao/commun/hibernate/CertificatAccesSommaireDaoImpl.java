package org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.hibernate.Query;
import org.sofiframework.application.securite.filtre.FiltreCertificatAccesSommaire;
import org.sofiframework.application.securite.objetstransfert.CertificatAccesSommaire;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.securite.dao.commun.CertificatAccesSommaireDao;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

public class CertificatAccesSommaireDaoImpl extends BaseDaoImpl implements
CertificatAccesSommaireDao {

  /** constructeur */
  public CertificatAccesSommaireDaoImpl() {
    super(CertificatAccesSommaire.class);
  }
  
  private long getRowCount(String requete, FiltreCertificatAccesSommaire filtre) {
    String sqlCount = "SELECT COUNT(*) FROM (" + requete + ")";
    
    Query query = getSession().createSQLQuery(sqlCount);
    setParameterInQuery(query, filtre);
    Number nbr = (Number) query.list().get(0);
    return nbr.intValue();
  }
  
  private void setParameterInQuery(Query query, FiltreCertificatAccesSommaire filtre ) {
    
    query.setTimestamp("date_debut", filtre.getDateDebut());
    query.setTimestamp("date_fin",filtre.getDateFin());
    
    if (filtre.getCodeClient() != null && filtre.getCodeClient()[0] != null ) {
      query.setParameterList("liste_client", filtre.getCodeClient());
      query.setString("liste_client_tous", "-2");
    }else {
      query.setParameterList("liste_client", new String[]{"-1"});
      query.setString("liste_client_tous", "-1");
    }
    
    if (filtre.getUserAgent() != null) {
      query.setString("user_agent", "%" + filtre.getUserAgent().toLowerCase() +"%");
    }else {
      query.setString("user_agent", "-1");
    }
    
    if (filtre.getUrlReferer() != null) {
      query.setString("url_referer", "%" + filtre.getUrlReferer().toLowerCase() +"%");
    }else {
      query.setString("url_referer", "-1");
    }
    
    if (filtre.getCourriel() != null) {
     query.setString("courriel", "%" + filtre.getCourriel().toLowerCase() +"%");
    }else {
     query.setString("courriel", "-1");
    }
    
    if (filtre.getApplicationSeulement() != null) {
      if (filtre.getApplicationSeulement()) {
        query.setString("application_seulement", "webView");
        query.setString("exclure_application_seulement", "-1");
      }else {
        query.setString("exclure_application_seulement", "webView");
        query.setString("application_seulement", "-1");
      }
    }else {
      query.setString("application_seulement", "-1");
      query.setString("exclure_application_seulement", "-1");
    }
    
    if (filtre.getNbConnexionMin() != null) {
      query.setInteger("nb_connexion_min", filtre.getNbConnexionMin());
     }else {
      query.setInteger("nb_connexion_min", new Integer("-1"));
     } 

    
  }

  @Override
  public ListeNavigation getListe(ListeNavigation ln) {

    ln.ajouterTriInitial("nbr_conn", Boolean.FALSE);

    FiltreCertificatAccesSommaire filtre = (FiltreCertificatAccesSommaire)ln.getObjetFiltre();

    Query queryTmp = getSession().getNamedQuery("selectSommaireCertificatAcces");

    StringBuffer tri = appliquerTri(ln);

    String requete = queryTmp.getQueryString() + tri.toString();

    Query query = getSession().createSQLQuery(requete).addEntity("CertificatAccesSommaireTmp",CertificatAccesSommaire.class);

    setParameterInQuery(query, filtre);
    
    // Count du nombre de résultat
    Long count = getRowCount(requete, filtre);
    ln.setNbListe(count);

    if (ln.getMaxParPage() > 0) {
      try {
        BigDecimal maxParPage = new BigDecimal(ln.getMaxParPage());
        BigDecimal noPageMaximumTmp = new BigDecimal(count).divide(maxParPage);
        int noPageMaximum = noPageMaximumTmp.intValue();
        float reste = BigDecimal.valueOf(noPageMaximumTmp.doubleValue())
            .divideAndRemainder(BigDecimal.ONE)[1].floatValue();
        if (reste > 0) {
          noPageMaximum++;
        }
        if (noPageMaximum < ln.getNoPage()) {
          ln.setNoPage(ln.getNbPage() - 1);
          if (ln.getNoPage() < 1) {
            ln.setNoPage(1);
          }
        }
      } catch (Exception e) {
        // ne rien faire et laisser comportement par défaut.
      }
    }
    
    int firstResultRow = (ln.getNoPage() - 1) * ln.getMaxParPage();

    query.setFirstResult(firstResultRow);
    query.setFetchSize(ln.getMaxParPage());
    
    if (ln.getMaxResultatTotal() != 0) {
      query.setMaxResults(ln.getMaxResultatTotal());
    }else {
      query.setMaxResults(ln.getMaxParPage());
    }
    
    @SuppressWarnings("rawtypes")
    List resultat = query.list();

    if (ln.getMaxResultatTotal() != 0) {
      ln.setNbListe(ln.getMaxResultatTotal());
    }else {
      ln.setNbListe(getRowCount(requete, filtre));
    }
    
    ln.setListe(resultat);

    return ln;
  }

  /**
   * Permet de connaitre le nombre de résultat total de la requête.
   * 
   * @param hql
   *          Requête HQL
   * @param params
   *          Paramètres passés à la requête
   * @return Le nombre de résultat de la requête
   */
  @Override
  protected int compterLigne(String sql) {

    StringBuffer requete = new StringBuffer();
    Boolean resultatMultiple = construireRequeteCompte(requete, sql);

    int nbResultat = 0;
    try {
      Query q = this.getSession().createQuery(requete.toString());

      if (resultatMultiple) {
        Object[] resultat = (Object[]) q.iterate().next();
        List<Object> listeResultat = Arrays.asList(resultat);
        // Tri descendant de la liste
        Collections.sort(listeResultat, new Comparator<Object>() {

          public int compare(Object o1, Object o2) {
            return ((Long) o2).compareTo((Long) o1);
          }
        });
        // Prendre le resultat le plus élevé (le premier de la liste triée)
        nbResultat = ((Long) listeResultat.get(0)).intValue();
      } else {
        nbResultat = ((Long) q.iterate().next()).intValue();
      }
    } catch (Exception e) {
      throw new ModeleException("Impossible d'executer le COUNT sur la requete : " + requete, e.getCause());
    }

    return nbResultat;
  }






}
