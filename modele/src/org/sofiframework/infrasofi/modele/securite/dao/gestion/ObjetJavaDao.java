/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.gestion;

import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.modele.spring.dao.BaseDao;

/**
 * Accès de données de l'entité objet Java.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface ObjetJavaDao extends BaseDao {

  /**
   * Créer une liste d'objet java dont le type spécifier. Parcours tout les
   * {@link ObjetJava} enfant recursivement jusqu'a ce qu'il n'y est plus
   * d'enfant.
   * 
   * @param parent
   *          Objet java parent
   * @param type
   *          Le type de l'objet java
   * @return
   */
  List<ObjetJava> getListeObjetJavaParType(ObjetJava parent, String type);

  /**
   * Retourne le premier {@link ObjetJava} parent dont le type est celui
   * spécifier
   * 
   * @param parent
   *          L'objet java enfant
   * @param type
   *          Type de l'objet java
   * @return Un {@link ObjetJava}
   */
  ObjetJava getObjetJavaParentParType(ObjetJava parent, String type);

  /**
   * Obtenir la liste des services pour une application.
   * 
   * @param codeApplication
   * @return
   */
  List<ObjetJava> getListeServiceParApplication(String codeApplication);

  /**
   * Retourne la liste des objets java pour la page de sécurité du référentiel.
   * 
   * @param filtre
   * @return
   */
  ListeNavigation getListeObjetJavaPourSecurite(ListeNavigation liste, String codeUtilisateur);

  /**
   * 
   * @param idService
   * @param type
   * @return
   */
  List<ObjetJava> getListeObjetJavaParParent(Long idService, String type);

  /**
   * Liste des objets java d'une objet java parent.
   * 
   * @param idObjetJavaSource
   *          Id objet Java
   * @return
   */
  List<ObjetJava> getListeObjetJavaEnfantsPourObjetJava(Long idObjetJavaSource);

  /**
   * Obtenir une liste paginée d'objets java accessibles en pilotage pour un
   * utilisateur. L'objet Java doit être lié à une application dont
   * d'utilisateur est le créateur ou dont il possède un rôle de premier niveau.
   * 
   * @param liste
   * @param codeUtilisateur
   * @return
   */
  ListeNavigation getListeObjetJavaPilotage(ListeNavigation liste,
      String codeUtilisateur);
}
