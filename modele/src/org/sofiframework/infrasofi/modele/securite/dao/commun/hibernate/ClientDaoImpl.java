/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate;

import java.util.List;

import org.sofiframework.application.securite.objetstransfert.Client;
import org.sofiframework.infrasofi.modele.securite.dao.commun.ClientDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

/**
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 * 
 */
public class ClientDaoImpl extends BaseDaoImpl implements ClientDao {

  public ClientDaoImpl() {
    super(Client.class);
  }

  public Client getClient(String codeClient) {
    return (Client) get(codeClient);
  }

  @SuppressWarnings("unchecked")
  public List<Client> getListeClientParents() {
    return getListe("FROM Client WHERE codeClientParent is null", null);
  }
}
