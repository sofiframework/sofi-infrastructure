/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.gestion.hibernate;

import org.sofiframework.infrasofi.modele.entite.UtilisateurApplication;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.UtilisateurApplicationDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

/**
 * Accès de données de l'entité Utilisateur Application.
 * 
 * @author Jean-Maxime Pelletier
 */
public class UtilisateurApplicationDaoImpl extends BaseDaoImpl implements UtilisateurApplicationDao {

  public UtilisateurApplicationDaoImpl() {
    super(UtilisateurApplication.class);
  }

  /**
   * Obtenir l'entité UtilisateurApplication à partir de sa clé composée.
   * @param codeUtilisateur
   * @param codeApplication
   * @return Entité Utilisateur Application
   */
  public UtilisateurApplication getUtilisateurApplication(String codeUtilisateur, String codeApplication) {
    UtilisateurApplication utilisateurApplication = new UtilisateurApplication();
    utilisateurApplication.setCodeUtilisateur(codeUtilisateur);
    utilisateurApplication.setCodeApplication(codeApplication);
    return (UtilisateurApplication) this.get(utilisateurApplication);
  }
}
