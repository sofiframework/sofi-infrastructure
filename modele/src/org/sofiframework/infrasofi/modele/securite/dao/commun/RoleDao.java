package org.sofiframework.infrasofi.modele.securite.dao.commun;

import java.util.List;

import org.sofiframework.application.securite.filtre.FiltreRole;
import org.sofiframework.application.securite.objetstransfert.Role;
import org.sofiframework.modele.spring.dao.BaseDao;

public interface RoleDao extends BaseDao {

  public List<Role> getListeRolePilotage(FiltreRole filtre, String codeUtilisateur);

  public List<Role> getListeRolePilotage(String codeApplication,
      String codeStatut, String codeUtilisateurPilote);

  public List<Role> getListeRoleDisponible(String codeUtilisateur, String codeApplication, String codeUtilisateurPilote);

}
