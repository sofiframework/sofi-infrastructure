/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.sofiframework.application.securite.filtre.FiltrePropriete;
import org.sofiframework.application.securite.filtre.FiltreUtilisateur;
import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.Filtre;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.exception.SOFIException;
import org.sofiframework.infrasofi.modele.securite.dao.commun.UtilisateurDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.sofiframework.modele.spring.dao.hibernate.CritereSupplementaire;
import org.sofiframework.modele.spring.dao.hibernate.TraitementJdbc;
import org.sofiframework.securite.encryption.UtilitaireEncryption;
import org.sofiframework.utilitaire.UtilitaireListe;
import org.sofiframework.utilitaire.UtilitaireString;
import org.springframework.orm.hibernate3.HibernateCallback;

/**
 * Accès de données pour l'objet d'affaire utilisateur.
 * <p>
 * *
 * 
 * @author Marie-Soleil L'Allier (Nurun inc.)
 * @author Jean-Maxime Pelletier
 * @version SOFI 2.0
 */
public class UtilisateurDaoImpl extends BaseDaoImpl implements UtilisateurDao {
  
  private static final Log log = LogFactory.getLog(UtilisateurDaoImpl.class);

  /** constructeur */
  public UtilisateurDaoImpl() {
    super(Utilisateur.class, "UtilisateurCommun");
  }

  @Override
  public List getListe(Filtre filtre) {
    List liste = null;

    FiltreUtilisateur filtreUtilisateur = (FiltreUtilisateur) filtre;
    String[] codeApplication = filtreUtilisateur.getCodeApplication();
    filtreUtilisateur.setCodeApplication(null);

    if (filtreUtilisateur.getIsDeleted() == null) {
      filtreUtilisateur.setIsDeleted(Boolean.FALSE);
    }

    DetachedCriteria criteres = this.appliquerFiltre(filtre);

    if (codeApplication != null && codeApplication.length > 0) {
      criteres.add(Restrictions.in("listeApplications", codeApplication));
    }

    filtreUtilisateur.setCodeApplication(codeApplication);

    return liste;
  }

  @Override
  public ListeNavigation getListe(ListeNavigation ln) {
    return getListeSecurise(ln, null);
  }

  public ListeNavigation getListeSecurise(ListeNavigation ln,
      String codeUtilisateur) {
    FiltreUtilisateur filtreUtilisateur = (FiltreUtilisateur) ln.getFiltre();
    final String[] codeApplication = filtreUtilisateur.getCodeApplication();
    final String[] codeRole = filtreUtilisateur.getCodeRole();
    final String[] codeClient = filtreUtilisateur.getCodeClient();
    final Boolean roleAvecCodeClientSeulement = filtreUtilisateur
        .getRoleAvecCodeClientSeulement();
    final String codeUtilisateurEnTraitement = codeUtilisateur;
    final Boolean sansRoleEtCreeParUtilisateurEnTraitement = filtreUtilisateur
        .getSansRoleEtCreeParUtilisateurEnTraitement();
    final String[] codeAccesExterne = filtreUtilisateur.getCodeAccesExterne();
    final String typeAccesExterne = filtreUtilisateur.getTypeAccesExterne();
    final Boolean aucunAccesExterne = filtreUtilisateur.getAucunAccesExterne();
    final Boolean isVilleExiste = filtreUtilisateur.getIsVilleExiste();
    
    filtreUtilisateur.setCodeApplication(null);
    filtreUtilisateur.setCodeRole(null);
    filtreUtilisateur.setCodeClient(null);
    filtreUtilisateur.setRoleAvecCodeClientSeulement(null);
    filtreUtilisateur.setSansRoleEtCreeParUtilisateurEnTraitement(null);
    filtreUtilisateur.setCodeAccesExterne(null);
    filtreUtilisateur.setAucunAccesExterne(null);

    this.getListe(ln, new CritereSupplementaire() {
      @Override
      public void ajouter(DetachedCriteria criteres) {
        if (codeAccesExterne != null && codeApplication.length > 0 && !aucunAccesExterne) {
          String requete = null;
          if (StringUtils.isEmpty(typeAccesExterne)) {
            requete = "exists (select 'x' from UTILISATEUR_ACCES_EXTERNE uae "
                + "where ({alias}.id_utilisateur = uae.id_utilisateur "
                + "and uae.CODE_FOURNISSEUR_ACCES in("
                + toInParam(codeAccesExterne) + ")))";
          }else {
            requete = "exists (select 'x' from UTILISATEUR_ACCES_EXTERNE uae "
                + "where ({alias}.id_utilisateur = uae.id_utilisateur "
                + "and uae.CODE_FOURNISSEUR_ACCES in("
                + toInParam(codeAccesExterne) + ") AND this_.TYPE_FOUNISSEUR_TYPE = '" + typeAccesExterne + "'))";
          }         
          criteres.add(Restrictions
              .sqlRestriction(requete));
        }
        
        if (aucunAccesExterne) {
          String requete = null;
          if (StringUtils.isEmpty(typeAccesExterne)) {
            requete = "not exists (select 'x' from UTILISATEUR_ACCES_EXTERNE uae "
                + "where ({alias}.id_utilisateur = uae.id_utilisateur "
                +"))";
          }else {
            requete = "not exists (select 'x' from UTILISATEUR_ACCES_EXTERNE uae "
                + "where ({alias}.id_utilisateur = uae.id_utilisateur "
                + "AND this_.TYPE_FOUNISSEUR_TYPE = '" + typeAccesExterne + "'))";
          }         
          criteres.add(Restrictions
              .sqlRestriction(requete));
          
        }
        
        if (isVilleExiste){
          criteres.add(Restrictions.isNotNull("idVille"));
        }
        
        if (sansRoleEtCreeParUtilisateurEnTraitement == null
            || !sansRoleEtCreeParUtilisateurEnTraitement) {
          if (codeApplication != null && codeApplication.length > 0) {
            criteres.add(Restrictions
                .sqlRestriction("exists (select 'x' from UTILISATEUR_ROLE ur "
                    + "where ({alias}.code_utilisateur = ur.code_utilisateur "
                    + "and ur.CODE_APPLICATION in("
                    + toInParam(codeApplication) + ")))"));
          }
          if (codeRole != null && codeRole.length > 0) {
            if (roleAvecCodeClientSeulement == null
                || roleAvecCodeClientSeulement.booleanValue()) {
              criteres.add(Restrictions
                  .sqlRestriction("exists(select 'x' from UTILISATEUR_ROLE ur "
                      + "where ({alias}.code_utilisateur = ur.code_utilisateur "
                      + "and ur.code_role in(" + toInParam(codeRole)
                      + ") and ur.code_client is not null))"));
            } else {
              criteres.add(Restrictions.sqlRestriction(""
                  + "exists(select 'x' from UTILISATEUR_ROLE ur "
                  + "where ({alias}.code_utilisateur = ur.code_utilisateur "
                  + "and ur.code_role in(" + toInParam(codeRole) + ")))"));
            }
          }
          if (codeClient != null && codeClient.length > 0) {
            criteres.add(Restrictions
                .sqlRestriction("exists(select 'x' from UTILISATEUR_ROLE ur "
                    + "where ({alias}.code_utilisateur = ur.code_utilisateur "
                    + "and ur.code_client in(" + toInParam(codeClient) + ")))"));
          }
        } else {
          criteres.add(Restrictions
              .sqlRestriction("(not exists(select 'x' from UTILISATEUR_ROLE ur "
                  + "where ({alias}.code_utilisateur = ur.code_utilisateur "
                  + "and ur.code_role in("
                  + toInParam(codeRole)
                  + ") and ur.code_client is not null)) "
                  + "and this_.cree_par = '"
                  + codeUtilisateurEnTraitement
                  + "')"));
        }
      }
    });

    filtreUtilisateur.setCodeApplication(codeApplication);
    filtreUtilisateur
        .setRoleAvecCodeClientSeulement(roleAvecCodeClientSeulement);
    filtreUtilisateur.setCodeRole(codeRole);
    filtreUtilisateur.setCodeClient(codeClient);
    filtreUtilisateur.setTypeAccesExterne(typeAccesExterne);
    filtreUtilisateur.setCodeAccesExterne(codeAccesExterne);
    filtreUtilisateur.setAucunAccesExterne(aucunAccesExterne);

    return ln;
  }

  @SuppressWarnings("unchecked")
  public Utilisateur getUtilisateurAvecObjetsSecurises(String codeApplication,
      String codeFacette, String identifiant, String codeClient) {

    Utilisateur utilisateur = this.getUtilisateur(codeApplication, identifiant,
        codeClient);

    // Obtenir les objets
    if (utilisateur != null && utilisateur.getIsDeleted() != null
        && !utilisateur.getIsDeleted().booleanValue()) {
      List listeObjets = this.getListeObjetSecurise(codeApplication,
          codeFacette, identifiant, codeClient);

      // fixer la liste d'objets sécurisables de l'utilisateur
      utilisateur.setListeObjetSecurisables(listeObjets);

      // Supprimer les sections vide
      supprimerSectionsVide(utilisateur.getListeObjetSecurisables().iterator());

      // Ajouter les applications disponibles.
      Query queryTmp = getSession().getNamedQuery(
          "selectUtilisateurApplicationDisponible");
      queryTmp.setDate("dateCourante", new java.util.Date());
      queryTmp.setString("codeUtilisateur", identifiant);

      List listeApplication = queryTmp.list();
      utilisateur.setListeApplications(listeApplication);

      return utilisateur;
    } else {
      return null;
    }

  }

  @SuppressWarnings("unchecked")
  private List getListeObjetSecurise(final String codeApplication,
      final String codeFacette, final String identifiant,
      final String codeClient) {

    /*
     * On doit passer defaut pour signaler au filtre qu'il n'y a pas de critère
     * de facette ou client
     */
    Date dateCourante = new Date();

    List<Long> listeId = this.getListeIdObjetJava(codeApplication, codeFacette,
        codeClient, identifiant, dateCourante, null, true);

    List<ObjetSecurisable> listeObjets = null;
    if (listeId != null && listeId.size() > 0) {
      listeObjets = this.getListeObjetSecurise(listeId);

      for (ObjetSecurisable objet : listeObjets) {
        this.completerListeObjetSecuriseEnfant(objet, codeApplication,
            codeFacette, codeClient, identifiant, dateCourante);
      }

      this.ajouterLectureSeule(listeObjets, codeApplication, identifiant,
          Boolean.FALSE);
    } else {
      listeObjets = new ArrayList<ObjetSecurisable>();
    }

    return listeObjets;
  }

  private List<ObjetSecurisable> getListeObjetSecurise(final List<Long> listeId) {
    List<ObjetSecurisable> liste = (List) this.getHibernateTemplate().execute(
        new HibernateCallback() {
          public Object doInHibernate(Session session)
              throws HibernateException, SQLException {
            Query q = session.createQuery("from ObjetSecurisableUtilisateur u "
                + "where u.seqObjetSecurisable " + "in (:listeId) "
                + "order by ordrePresentation asc");
            q.setParameterList("listeId", listeId);
            return q.list();
          }
        });
    return liste;
  }

  private void completerListeObjetSecuriseEnfant(
      ObjetSecurisable objetSecurisable, String codeApplication,
      String codeFacette, String codeClient, String identifiant,
      Date dateCourante) {

    List<Long> listeId = this.getListeIdObjetJava(codeApplication, codeFacette,
        codeClient, identifiant, dateCourante,
        objetSecurisable.getSeqObjetSecurisable(), false);
    if (listeId != null && listeId.size() > 0) {
      List<ObjetSecurisable> listeEnfant = this.getListeObjetSecurise(listeId);

      for (ObjetSecurisable enfant : listeEnfant) {

        Long idObjet = enfant.getSeqObjetSecurisable();
        this.completerListeObjetSecuriseEnfant(enfant, codeApplication,
            codeFacette, codeClient, identifiant, dateCourante);
      }

      objetSecurisable.setListeObjetSecurisableEnfants(listeEnfant);
    } else {
      objetSecurisable.setListeObjetSecurisableEnfants(new ArrayList());
    }
  }

  private List<Long> getListeIdObjetJava(final String codeApplication,
      final String codeFacette, final String codeClient,
      final String codeUtilisateur, final Date dateCourante,
      final Long idObjetSecuriseParent, final Boolean serviceOngletSeulement) {
    TraitementJdbc t = new TraitementJdbc() {
      @Override
      public Object effectuer(Connection connexion) throws SQLException {
        String sql = "select o.id_objet_java from OBJET_JAVA o where "
            + "o.code_application = ? ";

        if (idObjetSecuriseParent == null) {
          sql += "and ID_OBJET_JAVA_EST_ENFANT is null ";
        } else {
          sql += "and ID_OBJET_JAVA_EST_ENFANT = ? ";
        }

        if (serviceOngletSeulement) {
          sql += " and o.type in ('SE','SC') ";
        }

        sql += " and ('DEFAUT' = ? or ( "
            + " (CODE_FACETTE = '' OR CODE_FACETTE is null) or CODE_FACETTE = ?)) "
            + "and (DATE_DEBUT_ACTIVITE is null or DATE_DEBUT_ACTIVITE <= ?)"
            + "and (DATE_FIN_ACTIVITE is null or DATE_FIN_ACTIVITE > ?)"
            + " and ( VERSION_LIVRAISON is null or VERSION_LIVRAISON <= ( "
            + " select a.VERSION_LIVRAISON" + " from APPLICATION a "
            + " where a.CODE_APPLICATION = ?) " + " ) "
            + " and ( IND_SECURITE = 'N' or " + " exists ( " + " select 'X' "
            + " from ROLE_OBJET_JAVA ro, ( " + " select distinct "
            + " url.code_role, " + " url.code_application, "
            + " url.code_utilisateur, " + " url.code_client " + " from ( "
            + " select r.code_role code_role, "
            + "        r.code_application code_application, "
            + "        u.code_utilisateur code_utilisateur, "
            + "        u.code_client code_client "
            + " from ROLE r, UTILISATEUR_ROLE u "
            + " where r.code_role = u.code_role "
            + " and r.code_application = u.code_application " + " union all "
            + " select rl.code_role_lie code_role, "
            + "        rl.code_application_lie code_application, "
            + "        u.code_utilisateur code_utilisateur, "
            + "        u.code_client code_client "
            + " from ROLE_LIE rl, UTILISATEUR_ROLE u "
            + " where rl.code_application = u.code_application "
            + " and rl.code_role = u.code_role " + " ) url ) ur "
            + " where ro.CODE_ROLE = ur.CODE_ROLE "
            + " and ro.CODE_APPLICATION = ur.CODE_APPLICATION "
            + " and ro.ID_OBJET_JAVA = o.ID_OBJET_JAVA "
            + " and ur.CODE_APPLICATION = ? " + " and ur.CODE_UTILISATEUR = ? "
            + " and ('DEFAUT' = ? or ur.CODE_CLIENT = ?)))"
            + " order by o.ordre_affichage asc";

        int i = 1;
        PreparedStatement stmt = connexion.prepareCall(sql);
        stmt.setString(i++, codeApplication);

        if (idObjetSecuriseParent != null) {
          stmt.setLong(i++, idObjetSecuriseParent);
        }

        stmt.setString(i++, codeFacette != null ? codeFacette : "DEFAUT");
        stmt.setString(i++, codeFacette != null ? codeFacette : "DEFAUT");
        stmt.setDate(i++, new java.sql.Date(System.currentTimeMillis()));
        stmt.setDate(i++, new java.sql.Date(System.currentTimeMillis()));
        stmt.setString(i++, codeApplication);
        stmt.setString(i++, codeApplication);
        stmt.setString(i++, codeUtilisateur);
        stmt.setString(i++, codeClient != null ? codeClient : "DEFAUT");
        stmt.setString(i++, codeClient != null ? codeClient : "DEFAUT");

        ResultSet rs = stmt.executeQuery();
        List<Long> listeId = new ArrayList<Long>();

        while (rs.next()) {
          Long id = rs.getLong(1);
          listeId.add(id);
        }

        rs.close();

        return listeId;
      }
    };

    this.effectuerTraitementJdbc(t);

    return (List<Long>) t.getResultat();
  }

  private Utilisateur getUtilisateur(final String codeApplication,
      final String identifiant, final String codeClient) {

    Utilisateur utilisateur = (Utilisateur) getHibernateTemplate().execute(
        new HibernateCallback() {
          public Object doInHibernate(Session session) {
            session.setFlushMode(FlushMode.MANUAL);

            session.enableFilter("application").setParameter("application",
                codeApplication);
            if (!UtilitaireString.isVide(codeClient)) {
              session.enableFilter("client").setParameter("client", codeClient);
            }
            Query requete = session
                .createQuery("from UtilisateurCommun u where codeUtilisateur = :codeUtilisateur");
            requete.setString("codeUtilisateur", identifiant);
            List<Utilisateur> liste = requete.list();
            if (liste != null && liste.size() > 0) {
              return liste.get(0);
            }
            return null;
          }
        });

    return utilisateur;
  }

  /**
   * Retourne un utilisateur avec sa liste de rôles, d'application ainsi que la
   * liste d'objets sécurisés pour le code d'application reçu.
   * 
   * @return l'utilisateur
   * @param identifiant
   *          de l'utilisateur
   * @param codeApplication
   *          pour lequel on veut la liste d'objets sécurisés
   */
  public Utilisateur getUtilisateurAvecObjetsSecurises(String codeApplication,
      String identifiant) {
    // récupérer l'utisateur (incluant sa liste de rôles et d'application
    Utilisateur utilisateur = this.getUtilisateurAvecObjetsSecurises(
        codeApplication, null, identifiant, null);
    return utilisateur;
  }

  @SuppressWarnings("unchecked")
  private void ajouterLectureSeule(List listeObjects,
      final String codeApplication, final String codeUtilisateur,
      Boolean lectureSeulement) {
    Boolean lecteureSeulementObjetParent = lectureSeulement;

    for (Iterator i = listeObjects.iterator(); i.hasNext();) {
      ObjetSecurisable element = (ObjetSecurisable) i.next();
      final Long sequence = element.getSeqObjetSecurisable();

      if (!lecteureSeulementObjetParent.booleanValue()) {
        lectureSeulement = (Boolean) getHibernateTemplate().execute(
            new HibernateCallback() {
              public Object doInHibernate(Session session) {
                Boolean lecture = (Boolean) session
                    .createQuery(
                        "select min(ro.lectureSeulement) "
                            + "from RoleObjetSecurisable ro, "
                            + "     UtilisateurRoleCommun ur "
                            + "where ro.codeRole = ur.codeRole "
                            + "and ro.seqObjetSecurisable = :seqObjet "
                            + "and ur.codeUtilisateur = :codeUtilisateur "
                            + "and ur.codeApplication = :codeApplication ")
                    .setParameter("seqObjet", sequence)
                    .setParameter("codeUtilisateur", codeUtilisateur)
                    .setParameter("codeApplication", codeApplication)
                    .uniqueResult();
                return lecture;
              }
            });
        lectureSeulement = lectureSeulement == null ? Boolean.FALSE
            : lectureSeulement;
      }

      element.setLectureSeulement(lectureSeulement.booleanValue());

      if (element.getListeObjetSecurisableEnfants() != null
          && element.getListeObjetSecurisableEnfants().size() > 0) {
        this.ajouterLectureSeule(element.getListeObjetSecurisableEnfants(),
            codeApplication, codeUtilisateur, lectureSeulement);
      }
    }
  }

  /**
   * Supprimer les sections vide de l'utilisateur.
   * 
   * @param iterateur
   *          un iterateur sur les objets sécurisables.
   */
  @SuppressWarnings("unchecked")
  private void supprimerSectionsVide(Iterator iterateur) {
    while (iterateur.hasNext()) {
      ObjetSecurisable objetSecurisable = (ObjetSecurisable) iterateur.next();

      if (objetSecurisable.getListeObjetSecurisableEnfants() == null
          || objetSecurisable.getListeObjetSecurisableEnfants().size() == 0
          && objetSecurisable.getType().equals("SC")) {
        iterateur.remove();
      } else {
        supprimerSectionsVide(objetSecurisable
            .getListeObjetSecurisableEnfants().iterator());
      }
    }
  }

  @SuppressWarnings("unchecked")
  public List getListe(final String codeApplication, final String codeRole) {
    List liste = (List) getHibernateTemplate().execute(new HibernateCallback() {
      public Object doInHibernate(Session session) {
        session.enableFilter("application").setParameter("application",
            codeApplication);
        Query requete = session.createQuery("from UtilisateurCommun u "
            + "where exists (select 'x' " + "from UtilisateurRoleCommun ur "
            + "where ur.codeRole = :codeRole "
            + "and ur.codeUtilisateur = u.codeUtilisateur)");
        requete.setParameter("codeRole", codeRole);
        return requete.list();
      }
    });
    return liste;
  }

  /**
   * Méthode redéfinie pour encrypter le mot de passe avant d'appeler la méthode
   * ajouter de la classe mère
   */
  @Override
  public Serializable ajouter(Object entite) {
    this.setSessionAutoFlush();
    Utilisateur utilisateur = (Utilisateur) entite;
    String motPasse = utilisateur.getMotPasse();
    if (motPasse != null) {
      utilisateur.setMotPasse(UtilitaireEncryption.digest(motPasse));
    }

    if (utilisateur.getCodeFournisseurAcces() != null) {
      utilisateur.setCodeUtilisateur(utilisateur.getCourriel());
    }

    super.ajouter(entite);

    return utilisateur;
  }

  /**
   * Méthode redéfinie pour encrypter le mot de passe avant d'appeler la méthode
   * modifier de la classe mère
   */
  @Override
  public void modifier(Object entite) {
    this.setSessionAutoFlush();
    Utilisateur utilisateur = (Utilisateur) entite;

    Utilisateur ancienUtilisateur = (Utilisateur) get(utilisateur
        .getCodeUtilisateur());
    utilisateur.setMotPasse(ancienUtilisateur.getMotPasse());
    super.modifier(entite);
  }

  @SuppressWarnings("unchecked")
  public Utilisateur getUtilisateur(String id) {
    Utilisateur utilisateur = null;
    String hqlCodeUtilisateurApplicatif = "from UtilisateurCommun u "
        + "where u.codeUtilisateurApplicatif = ? and u.isDeleted = 'N' ";
    List<Utilisateur> listeUtilisateur = this.getHibernateTemplate().find(
        hqlCodeUtilisateurApplicatif, id);

    if (listeUtilisateur == null || listeUtilisateur.isEmpty()) {
      String hqlCodeUtilisateur = "from UtilisateurCommun u "
          + "where u.codeUtilisateur = ? and u.isDeleted = 'N' ";
      listeUtilisateur = this.getHibernateTemplate().find(hqlCodeUtilisateur,
          id);
    }

    if (listeUtilisateur != null && !listeUtilisateur.isEmpty()) {
      utilisateur = listeUtilisateur.get(0);
    }

    if (utilisateur == null || utilisateur.getIsDeleted() != null
        && utilisateur.getIsDeleted().booleanValue()) {
      // Utilisateur supprimé.
      return null;
    } else {
      return utilisateur;
    }

  }

  @SuppressWarnings("unchecked")
  @Override
  public Object get(Serializable id) {
    Utilisateur utilisateur = null;
    String hqlId = "from UtilisateurCommun u " + "where u.id = ? ";
    List<Utilisateur> listeUtilisateur = this.getHibernateTemplate().find(
        hqlId, id);

    if (listeUtilisateur != null && !listeUtilisateur.isEmpty()) {
      utilisateur = listeUtilisateur.get(0);
    }

    return utilisateur;
  }

  @SuppressWarnings("unchecked")
  public Utilisateur getUtilisateurPourCourriel(String courriel,
      String codeApplication) {
    Utilisateur utilisateur = null;
    String hqlId = "from UtilisateurCommun u "
        + "where lower(u.courriel) = ? and u.isDeleted = 'N'";
    List<Utilisateur> listeUtilisateur = this.getHibernateTemplate().find(
        hqlId, courriel.toLowerCase());

    if (listeUtilisateur != null && !listeUtilisateur.isEmpty()) {
      boolean trouve = false;
      for (int i = 0; i < listeUtilisateur.size() && !trouve; i++) {
        utilisateur = listeUtilisateur.get(i);
        Application application = (Application) UtilitaireListe
            .getValeurDansListe(utilisateur.getListeApplications(),
                "codeApplication", codeApplication);
        if (application != null) {
          trouve = true;
        } else {
          utilisateur = null;
        }
      }

    }

    if (utilisateur == null || utilisateur.getIsDeleted() != null
        && utilisateur.getIsDeleted().booleanValue()) {
      // Utilisateur supprimé.
      return null;
    } else {
      return utilisateur;
    }
  }

  @SuppressWarnings("unchecked")
  public Utilisateur getUtilisateurPourAccesExterne(String identifiant) {
    Utilisateur utilisateur = null;
    Query qry = this.getSession().createQuery("from UtilisateurCommun u " +
        "where u.codeUtilisateurApplicatif like ? and u.isDeleted = 'N'");
    qry.setString(0, "%"+identifiant+"%");
    
    List<Utilisateur> listeUtilisateur = qry.list();
    if (listeUtilisateur != null && !listeUtilisateur.isEmpty()) {
      utilisateur = listeUtilisateur.get(0);
    }

    if (utilisateur == null || utilisateur.getIsDeleted() != null
        && utilisateur.getIsDeleted().booleanValue()) {
      // Utilisateur supprimé.
      return null;
    } else {
      return utilisateur;
    }
  }
  
  public void ajouterProprieteUtilisateur(final Long idUtilisateur,
      final String valeur, final Long metadonneProprieteId) {
    this.effectuerTraitementJdbc(new TraitementJdbc() {
      @Override
      public Object effectuer(Connection connexion) throws SQLException {
        CallableStatement stmt = null;
        try {
          stmt = connexion
              .prepareCall("INSERT INTO propriete_utilisateur(ID, VALEUR, UTILISATEUR_ID, METADONNEE_PROPRIETE_ID, CREE_PAR, DATE_CREATION, VERSION) "
                  + " VALUES (PROPRIETE_UTILISATEUR_SEQ.nextval, ?, ?, ?, ?, SYSDATE, 0)");
          stmt.setString(1, valeur);
          stmt.setLong(2, idUtilisateur);
          stmt.setLong(3, metadonneProprieteId);
          stmt.setLong(4, idUtilisateur);
          stmt.executeUpdate();
        } catch (SQLException e) {
          log.error(new SOFIException(e));
          throw e;
        } finally {
          if (stmt != null) {
            stmt.close();
          }
        }
        return null;
      }
    });
  }

  public void modifieProprieteUtilisateur(final Long idUtilisateur,
      final String valeur, final Long metadonneProprieteId) {
    this.effectuerTraitementJdbc(new TraitementJdbc() {
      @Override
      public Object effectuer(Connection connexion) throws SQLException {
        CallableStatement stmt = null;
        try {
          java.util.Date now = new java.util.Date();
          java.sql.Date sqlDate = new java.sql.Date(now.getTime());
          stmt = connexion
              .prepareCall("UPDATE propriete_utilisateur SET valeur = ?, modifie_par = ?, date_modification = ? "
                  + "WHERE utilisateur_id = ? AND metadonnee_propriete_id = ? ");
          stmt.setString(1, valeur);
          stmt.setLong(2, idUtilisateur);
          stmt.setDate(3, sqlDate);
          stmt.setLong(4, idUtilisateur);
          stmt.setLong(5, metadonneProprieteId);
          stmt.executeUpdate();
        } catch (SQLException e) {
          log.error(new SOFIException(e));
          throw e;
        } finally {
          if (stmt != null) {
            stmt.close();
          }
        }
        return null;
      }
    });
  }
  
  public boolean isUtilisateurSupprime(Long utilisateurId) {
    
    Criteria criteria = this.getSession().createCriteria("UtilisateurCommun");
    criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

    criteria.add(Restrictions.eq("id", utilisateurId));
    
    criteria.setProjection(Projections.property("isDeleted"));
    
    return (Boolean) criteria.uniqueResult();

  }

  private void setSessionAutoFlush() {
    try {
      getSession().setFlushMode(FlushMode.AUTO);
    } catch (HibernateException e) {
      throw convertHibernateAccessException(e);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Utilisateur> getListeUtilisateurPourFiltrePropriete(FiltrePropriete filtrePropriete, Date dateDebutCreationMAJ,
      Date dateFinCreationMAJ) {

    Criteria criteria = this.getSession().createCriteria("UtilisateurCommun");
    criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
    
    criteria.add(Restrictions.eq("isDeleted", "N"));
    
    criteria.createAlias("listePropriete", "p");
    criteria.add(Restrictions.or(
        Restrictions.and(Restrictions.isNull("p.dateModification"),
            Restrictions.between("p.dateCreation", dateDebutCreationMAJ, dateFinCreationMAJ)),
        Restrictions.and(Restrictions.isNotNull("p.dateModification"),
            Restrictions.between("p.dateModification", dateDebutCreationMAJ, dateFinCreationMAJ))));
    
    if (StringUtils.isNotBlank(filtrePropriete.getCodeClient())) {
      criteria.add(Restrictions.eq("p.codeClient", filtrePropriete.getCodeClient())); 
    }
    
    if (StringUtils.isNotBlank(filtrePropriete.getCodeDomaine())) {
      criteria.add(Restrictions.eq("p.codeDomaine", filtrePropriete.getCodeDomaine())); 
    }
    
    if (StringUtils.isNotBlank(filtrePropriete.getValeur())) {
      criteria.add(Restrictions.eq("p.valeur", filtrePropriete.getValeur())); 
    }
    
    return criteria.list();
  }

}