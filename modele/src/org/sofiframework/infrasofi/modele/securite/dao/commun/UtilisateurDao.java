/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.commun;

import java.util.Date;
import java.util.List;

import org.sofiframework.application.securite.filtre.FiltrePropriete;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.modele.spring.dao.BaseDao;

public interface UtilisateurDao extends BaseDao {

  Utilisateur getUtilisateurAvecObjetsSecurises(String codeApplication, String identifiant);

  Utilisateur getUtilisateur(String identifiant);

  List getListe(String codeApplication, String codeRole);

  Utilisateur getUtilisateurAvecObjetsSecurises(String codeApplication,
      String codeFacette, String identifiant, String codeClient);

  ListeNavigation getListeSecurise(ListeNavigation ln, String codeUtilisateur);

  Utilisateur getUtilisateurPourCourriel(String courriel, String codeApplication) ;
  
  Utilisateur getUtilisateurPourAccesExterne(String identifiant) ;
  
  void modifieProprieteUtilisateur(final Long idUtilisateur, final String valeur, final Long metadonneProprieteId);
  
  void ajouterProprieteUtilisateur(final Long idUtilisateur, final String valeur, final Long metadonneProprieteId);
  
  boolean isUtilisateurSupprime(Long utilisateurId);

  List<Utilisateur> getListeUtilisateurPourFiltrePropriete(FiltrePropriete filtrePropriete, Date dateDebutCreationMAJ,
      Date dateFinCreationMAJ);
  
}