/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.gestion.hibernate;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Filter;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Utilisateur;
import org.sofiframework.infrasofi.modele.entite.UtilisateurRole;
import org.sofiframework.infrasofi.modele.filtre.FiltreUtilisateurRole;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.UtilisateurDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.sofiframework.modele.spring.dao.hibernate.CritereSupplementaire;
import org.sofiframework.modele.spring.dao.hibernate.TraitementJdbc;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Accès de données pour l'objet d'affaire utilisateur.
 * <p>
 * *
 * 
 * @author Jean-Maxime Pelletier
 */
public class UtilisateurDaoImpl extends BaseDaoImpl implements UtilisateurDao {

  /** constructeur */
  public UtilisateurDaoImpl() {
    super(Utilisateur.class);
  }

  public ListeNavigation getListeUtilisateurPilotage(ListeNavigation ln, String codeUtilisateur, List<String> listeCleRolePilote) {
    if (listeCleRolePilote != null && !listeCleRolePilote.isEmpty()) {
      this.activerFiltrePilote(codeUtilisateur, listeCleRolePilote);
    }

    if (ln.getObjetFiltre() instanceof FiltreUtilisateurRole) {
      final FiltreUtilisateurRole filtre = (FiltreUtilisateurRole) ln
          .getObjetFiltre();
      FiltreUtilisateurRole filtreClone = (FiltreUtilisateurRole) filtre
          .clone();
      filtreClone.setCodeRole(null);
      filtreClone.setCodeApplication(null);
      filtreClone.setCodeRoleNonInclus(null);
      ln.setFiltre(filtreClone);

      ln = this.getListe(ln, new CritereSupplementaire() {
        @Override
        public void ajouter(DetachedCriteria criteres) {
          Boolean codeRoleNonInclus = filtre.isCodeRoleNonInclus();
          String codeRole = filtre.getCodeRole();
          String codeApplication = filtre.getCodeApplication();
          if (!UtilitaireString.isVide(codeRole)
              || !UtilitaireString.isVide(codeApplication)) {
            // Sous requête pour le code de role et le code application
            DetachedCriteria sousCriteria = DetachedCriteria.forClass(
                UtilisateurRole.class, "ur");
            sousCriteria = sousCriteria.setProjection(Property
                .forName("codeUtilisateur"));
            sousCriteria.add(Property.forName("codeUtilisateur").eqProperty(
                "this.codeUtilisateur"));

            if (!UtilitaireString.isVide(codeRole)) {
              sousCriteria.add(Restrictions.eq("codeRole", codeRole));
            }

            if (!UtilitaireString.isVide(codeApplication)) {
              sousCriteria.add(Restrictions.eq("codeApplication",
                  codeApplication));
            }

            if (codeRoleNonInclus != null) {
              if (codeRoleNonInclus) {
                criteres.add(Subqueries.notExists(sousCriteria));
              } else {
                criteres.add(Subqueries.exists(sousCriteria));
              }
            }
          }
        }
      });
      ln.setFiltre(filtre);
    } else {
      ln = super.getListe(ln);
    }

    return ln;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Object get(Serializable id) {
    Utilisateur utilisateur = null;
    String hqlCodeUtilisateurApp = "from Utilisateur u "
        + "where u.codeUtilisateurApp = ? ";
    List<Utilisateur> listeUtilisateur = this.getHibernateTemplate().find(
        hqlCodeUtilisateurApp, id);

    if (listeUtilisateur == null || listeUtilisateur.isEmpty()) {
      String hqlCodeUtilisateur = "from Utilisateur u "
          + "where u.codeUtilisateur = ? ";
      listeUtilisateur = this.getHibernateTemplate().find(hqlCodeUtilisateur,
          id);
    }

    if (listeUtilisateur != null && !listeUtilisateur.isEmpty()) {
      utilisateur = listeUtilisateur.get(0);
    }

    return utilisateur;
  }

  public Utilisateur getUtilisateurActif(String identifiant) {
    Utilisateur utilisateur = (Utilisateur) this.get(identifiant);

    if (!(utilisateur != null && utilisateur.getCodeStatut() != null && utilisateur
        .getCodeStatut().equalsIgnoreCase("A"))) {
      utilisateur = null;
    }
    return utilisateur;
  }

  public void forcerChangementMotPasse(final String codeUtilisateur) {
    this.effectuerTraitementJdbc(new TraitementJdbc() {
      @Override
      public Object effectuer(Connection connexion) throws SQLException {
        CallableStatement stmt = null;
        try {
          stmt = connexion
              .prepareCall("update utilisateur set date_mot_passe = null "
                  + "where coalesce(code_utilisateur_app, code_utilisateur) = ? ");
          stmt.setString(1, codeUtilisateur);
          stmt.executeUpdate();
        } finally {
          if (stmt != null) {
            stmt.close();
          }
        }
        return null;
      }
    });
  }

  /**
   * Les utilisateurs créé par le pilote ou qui partage un rôle avec le pilote.
   * 
   * @param codeUtilisateur
   */
  private void activerFiltrePilote(String codeUtilisateur, List<String> listeCleRolePilote) {
    Filter filtre = getSession().enableFilter("filtreUtilisateurPilotage");
    filtre.setParameter("codeUtilisateur", codeUtilisateur);
    String[] cles = listeCleRolePilote.toArray(new String[]{});
    filtre.setParameterList("listeCleRolePilote", cles);
  }

  @SuppressWarnings("unchecked")
  public Utilisateur getUtilisateur(String codeUtilisateur) {
    List<Utilisateur> l = this.getListe("from Utilisateur u where u.codeUtilisateur = ?", new Object[]{codeUtilisateur});

    //Il devrait y avoir seulement un utilisateur avec un code.
    if (l != null && l.size() == 1) {
      return l.get(0);
    }

    return null;
  }
}