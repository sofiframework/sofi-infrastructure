/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.gestion.hibernate;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.Tri;
import org.sofiframework.infrasofi.modele.BaseDaoInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.filtre.FiltreApplication;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.ApplicationDao;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Accès de données de l'entité Application.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ApplicationDaoImpl extends BaseDaoInfrastructureImpl implements ApplicationDao {

  public ApplicationDaoImpl() {
    super(Application.class);
  }

  public ListeNavigation getListeApplicationAccessible(String codeUtilisateur,
      ListeNavigation liste, Boolean accesRoleParent) {
    this.activerFiltreUtilisateur(codeUtilisateur, accesRoleParent);
    return this.getListe(liste);
  }

  @Override
  public Object get(Serializable id) {
    Application entite = (Application) super.get(id);
    // Forcer le get des objets java dans le cas d'un detail
    if (entite != null) {
      entite.getListeObjetJava().isEmpty();
    }
    return entite;
  }

  public Application getApplication(String codeApplication, String codeFacette) {
    if (!UtilitaireString.isVide(codeFacette)) {
      getSession().enableFilter("filtreFacette").setParameter("codeFacette",
          codeFacette);
    }
    Application entite = (Application) super.get(codeApplication);

    // Forcer le get des objets java dans le cas d'un detail
    if (entite != null) {
      entite.getListeObjetJava().isEmpty();
    }

    return entite;
  }

  @SuppressWarnings("unchecked")
  public List<Application> getListeApplicationAccessible(String codeUtilisateur,
      Boolean accesRoleParent) {
    this.activerFiltreUtilisateur(codeUtilisateur, Boolean.TRUE);
    FiltreApplication filtre = new FiltreApplication();
    Tri tri = new Tri("Nom", true);
    return this.getListe(filtre, tri);
  }


  /**
   * Un utilisateur a accès si il a créé l'entité ou si il possède un rôle pour
   * l'application.
   * 
   * Le droit de pilotage est lié au fait que l'utilisateur possède un rôle de
   * premier niveau (sans parent).
   * 
   * @param codeUtilisateur
   * @param accesRoleParent
   */
  protected void activerFiltreUtilisateur(String codeUtilisateur, Boolean accesRoleParent) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("codeUtilisateur", codeUtilisateur);
    params.put("accesRoleParent", accesRoleParent ? "O" : "N");
    params.put("dateCourante", new Date());
    this.activerFiltreSession("filtreUtilisateurRole", params);
  }
}
