/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.sofiframework.application.securite.objetstransfert.Role;
import org.sofiframework.application.securite.objetstransfert.UtilisateurRole;
import org.sofiframework.infrasofi.modele.securite.dao.commun.UtilisateurRoleDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.springframework.orm.hibernate3.HibernateCallback;

public class UtilisateurRoleDaoImpl extends BaseDaoImpl implements UtilisateurRoleDao {
  public UtilisateurRoleDaoImpl() {
    super(UtilisateurRole.class, "UtilisateurRoleCommun");
  }

  public List<Role> getListeRole(String codeUtilisateur) {
    List<Role> listeRole = null;

    this.getHibernateTemplate().executeFind(new HibernateCallback<List<Role>>() {
      public List<Role> doInHibernate(Session session) throws HibernateException,
      SQLException {
        Criteria c = session.createCriteria(Role.class);
        c.add(Restrictions.sqlRestriction(""));
        return c.list();
      }
    });

    return listeRole;
  }

  public List<UtilisateurRole> getListeUtilisateurRole(String codeUtilisateur, String codeApplication) {
    List<UtilisateurRole> listeUtilisateurRole = getHibernateTemplate().find(
        "FROM UtilisateurRoleCommun ur " +
            "WHERE ur.codeUtilisateur = ? " +
            "AND ur.codeApplication = ? ",
            new Object[]{codeUtilisateur, codeApplication});
    return listeUtilisateurRole;
  }

}
