/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.local;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.Tri;
import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.Libelle;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.modele.entite.PageApplication;
import org.sofiframework.infrasofi.modele.entite.RoleObjetJava;
import org.sofiframework.infrasofi.modele.filtre.FiltreObjetJava;
import org.sofiframework.infrasofi.modele.ioc.DependServiceLibelle;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.ObjetJavaDao;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.RoleObjetJavaDao;
import org.sofiframework.infrasofi.modele.service.ServiceLibelle;
import org.sofiframework.infrasofi.modele.service.ServiceReferentiel;
import org.sofiframework.modele.exception.ModeleException;

/**
 * Service de gestion du référentiel.
 */
public class ServiceReferentielImpl extends BaseServiceInfrastructureImpl implements ServiceReferentiel, DependServiceLibelle {

  private ServiceLibelle serviceLibelle = null;

  private RoleObjetJavaDao roleObjetJavaDao = null;

  public ListeNavigation getListeObjetJavaPilotage(ListeNavigation liste) {
    String codeUtilisateur = this.getGestionnaireUtilisateur().getUtilisateur().getCodeUtilisateur();
    this.getObjetJavaDao().getListeObjetJavaPilotage(liste, codeUtilisateur);
    return liste;
  }

  private ObjetJavaDao getObjetJavaDao() {
    return (ObjetJavaDao) this.getDao();
  }

  public ObjetJava getObjetJava(String nom) {
    //TODO A faire
    return null;
  }


  public void enregistrerObjetJava(ObjetJava objet, ObjetJava objetOriginal, String texteLibelle) {
    if (objetOriginal != null) {
      if (objet.getDateCreation() == null) {
        if (!getServiceLibelle().existe(objet.getNom(), objet.getCodeApplication())) {
          Libelle libelle = new Libelle();
          libelle.setCleLibelle(objet.getNom());
          libelle.setCodeLangue(this.getUtilisateur().getCodeLocale());
          libelle.setCodeApplication(objet.getCodeApplication());
          libelle.setTexteLibelle(texteLibelle);
          getServiceLibelle().ajouter(libelle);
        }
      } else {
        if (!objet.getNom().equals(objetOriginal.getNom())) {
          Libelle nouveauLibelle = this.getServiceLibelle().get(objet.getNom(), null);
          // Si le nouveau libelle n'existe pas
          if (nouveauLibelle == null) {
            Libelle libelleAModifier = this.getServiceLibelle().get(objetOriginal.getNom(), null);
            getServiceLibelle().modifierCleLibelle(objetOriginal.getNom(), libelleAModifier);
          } else {
            throw new ModeleException("infra_sofi.erreur.referentiel.libelle.existant");
          }
        }
      }
    }

    if (objet.getId() != null) {
      this.modifier(objet);
    } else {
      this.ajouter(objet);
    }
  }

  public void enregistrerObjetJava(ObjetJava objet) {
    this.enregistrerObjetJava(objet, null, null);
  }

  public ListeNavigation getListeObjetJava(ListeNavigation liste) {
    return this.getListe(liste);
  }

  @SuppressWarnings("unchecked")
  public List<ObjetJava> getListeObjetJava(Long idObjetJavaParent) {
    FiltreObjetJava filtre = new FiltreObjetJava();
    filtre.setIdParent(idObjetJavaParent);
    return this.getDao().getListe(filtre, new Tri("nom", true));
  }

  @SuppressWarnings("unchecked")
  public List<ObjetJava> getListeOnglet(Long idService) {
    FiltreObjetJava filtre = new FiltreObjetJava();
    filtre.setIdParent(idService);
    filtre.setType("ON");
    filtre.setCodeStatut("A");
    return this.getDao().getListe(filtre);
  }

  @SuppressWarnings("unchecked")
  public List<ObjetJava> getListeServiceParApplication(String codeApplication) {
    FiltreObjetJava filtre = new FiltreObjetJava();
    filtre.setCodeApplication(codeApplication);
    filtre.setType("SE");
    filtre.setCodeStatut("A");
    return this.getDao().getListe(filtre);
  }

  @SuppressWarnings("unchecked")
  public List<ObjetJava> getListeServiceParCleAide(String cleAide) {
    FiltreObjetJava filtre = new FiltreObjetJava();
    filtre.setCleAide(cleAide);
    return this.getDao().getListe(filtre);
  }

  public ObjetJava getObjetJava(Long id) {
    ObjetJava objetJava = (ObjetJava) this.getDao().get(id);
    List<RoleObjetJava> listeRoleObjetJava = roleObjetJavaDao.getListeRoleObjetJava(id);
    objetJava.setListeRoleObjetJavaAutorise(listeRoleObjetJava);
    return objetJava;
  }

  public void enregistrerListeRoleObjetJavaPourUnObjetJava(List<RoleObjetJava> listeRoleObjetJava, Long idObjetJava, String codeApplication) {
    if (listeRoleObjetJava != null) {
      roleObjetJavaDao.supprimerListeRoleObjetJava(idObjetJava);
      for (RoleObjetJava roj : listeRoleObjetJava) {
        RoleObjetJava roleObjetJava = new RoleObjetJava();
        roleObjetJava.setIdObjetJava(idObjetJava);
        roleObjetJava.setCodeApplication(codeApplication);
        roleObjetJava.setCodeRole(roj.getCodeRole());
        if (roj.getIndicateurConsultation() == null || roj.getIndicateurConsultation() == Boolean.FALSE) {
          roleObjetJava.setIndicateurConsultation(Boolean.FALSE);
        } else {
          roleObjetJava.setIndicateurConsultation(Boolean.TRUE);
        }

        roleObjetJavaDao.ajouter(roleObjetJava);

      }
    }
  }

  public void enregistrerListeRoleObjetJavaPourEnfants(Long idObjetJavaSource, List<RoleObjetJava> listeRoleObjetJava, String codeApplication)
      throws ModeleException {

    List<ObjetJava> listeObjetJavaEnfant = this.getListeObjetJavaEnfantsPourObjetJava(idObjetJavaSource);

    if (listeObjetJavaEnfant != null && listeObjetJavaEnfant.size() > 0) {
      for (ObjetJava objetJava : listeObjetJavaEnfant) {

        this.enregistrerListeRoleObjetJavaPourUnObjetJava(listeRoleObjetJava, objetJava.getId(), codeApplication);

        this.enregistrerListeRoleObjetJavaPourEnfants(objetJava.getId(), listeRoleObjetJava, codeApplication);
      }
    }
  }

  private List<ObjetJava> getListeObjetJavaEnfantsPourObjetJava(Long idObjetJavaSource) {
    return ((ObjetJavaDao) this.getDao()).getListeObjetJavaEnfantsPourObjetJava(idObjetJavaSource);
  }

  public void enregistrerPageApplication(PageApplication pageApplication) {
  }

  public void supprimerObjetJava(Serializable id) {
    this.getDao().supprimer(id);
  }

  public ServiceLibelle getServiceLibelle() {
    return serviceLibelle;
  }

  public void setServiceLibelle(ServiceLibelle serviceLibelle) {
    this.serviceLibelle = serviceLibelle;
  }

  public ListeNavigation getListeObjetJavaPourSecurite(ListeNavigation liste) {
    String codeUtilisateur = this.getCodeUtilisateurEnCours();
    return this.getObjetJavaDao().getListeObjetJavaPourSecurite(liste, codeUtilisateur);
  }

  /**
   * {@inheritDoc}
   */
  public List<ObjetJava> getListeObjetJavaParParent(Long idService, String type) {
    List<ObjetJava> liste = new ArrayList<ObjetJava>();

    for (ObjetJava objetJava : ((ObjetJavaDao) this.getDao()).getListeObjetJavaParParent(idService, type)) {
      liste.addAll(((ObjetJavaDao) this.getDao()).getListeObjetJavaParType(objetJava, type));
    }

    return liste;
  }

  /**
   * {@inheritDoc}
   */
  public void enregistrerCleAide(Long idObjetJava, String cleAide) {
    ObjetJava objetJava = (ObjetJava) this.get(idObjetJava);
    objetJava.setCleAide(cleAide);
    this.modifier(objetJava);
  }

  /**
   * {@inheritDoc}
   */
  public void supprimerAssociationAide(Long idObjetJava) {
    ObjetJava objetJava = (ObjetJava) this.get(idObjetJava);
    objetJava.setCleAide(null);
    this.modifier(objetJava);
  }

  /**
   * {@inheritDoc}
   */
  public void enregistrerCleAide(Long idObjetJava, Long idAncienObjetJava, String cleAide) {
    this.supprimerAssociationAide(idAncienObjetJava);
    this.enregistrerCleAide(idObjetJava, cleAide);
  }

  public void setRoleObjetJavaDao(RoleObjetJavaDao roleObjetJavaDao) {
    this.roleObjetJavaDao = roleObjetJavaDao;
  }

  public RoleObjetJavaDao getRoleObjetJavaDao() {
    return roleObjetJavaDao;
  }
}
