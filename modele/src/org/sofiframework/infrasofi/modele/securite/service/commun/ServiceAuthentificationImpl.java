/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.commun;

import java.security.Key;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.objetstransfert.CertificatAcces;
import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.securite.dao.commun.CertificatAccesDao;
import org.sofiframework.infrasofi.modele.securite.dao.commun.CertificatAccesSommaireDao;
import org.sofiframework.infrasofi.modele.service.ServiceCertificat;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;
import org.sofiframework.securite.IdentifiantUnique;
import org.sofiframework.securite.encryption.EncryptionException;
import org.sofiframework.securite.encryption.UtilitaireEncryption;
import org.sofiframework.utilitaire.UtilitaireDate;

/**
 * Service de gestion des cerificats.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceAuthentificationImpl extends DaoServiceImpl implements ServiceAuthentification, ServiceCertificat {

  private static final String SEPARATEUR_CODE_UTILISATEUR_CERTIFICAT = "_";

  private static final Log log = LogFactory.getLog(ServiceAuthentificationImpl.class);

  private Integer nombreHeureInvalidationCertificatValide = 24;// Heures
  private Integer nombreJourDestructionCertificatDeconnete = 90;// Jours
  private Boolean inactiverAncienCertificat = Boolean.TRUE;

  private CertificatAccesSommaireDao certificatAccesSommaireDao;

  public CertificatAcces getCertificatAcces(String certificat) {
    return getCertificatAcces(certificat, true);
  }

  public CertificatAcces getCertificatAcces(String certificat, boolean majAcces) {
    CertificatAcces certificatAcces = getCertificatAcces(certificat, null, false, majAcces);
    return certificatAcces;
  }

  public CertificatAcces getCertificatAcces(String certificat, String ip, boolean actifSeulement, boolean majAcces) {
    CertificatAcces certificatAcces = (CertificatAcces) this.getDao().get(certificat);

    if (actifSeulement) {
      Long now = new java.util.Date().getTime();
      Long dateDeconnexion = null;
      if (certificatAcces != null && certificatAcces.getDateDeconnexion() != null) {
        dateDeconnexion = certificatAcces.getDateDeconnexion().getTime();
      }
      if (dateDeconnexion != null && dateDeconnexion < now) {
        // Invalider le certificat, on retourne null;
        certificatAcces = null;
      }
    }

    if (majAcces && certificatAcces != null) {
      if (certificatAcces.getCodeClient() != null) {
        this.getCertificatAccesDao().notifierUtilisateurDernierAcces(certificatAcces.getCodeUtilisateur(),
            certificatAcces.getCodeFournisseurAcces(), certificatAcces.getCodeClient(), certificatAcces.getCertificat(),
            ip, true);
      } else {
        this.getCertificatAccesDao().notifierUtilisateurDernierAcces(certificatAcces.getCodeUtilisateur());
      }
    }
    return certificatAcces;
  }

  public String getCodeUtilisateur(String certificat, String ip, boolean majAcces) {
    String codeUtilisateur = null;
    CertificatAcces certificatAcces = (CertificatAcces) this.getDao().get(certificat);

    if (certificatAcces != null) {
      Long now = new java.util.Date().getTime();
      Long dateDeconnexion = null;
      if (certificatAcces != null && certificatAcces.getDateDeconnexion() != null) {
        dateDeconnexion = certificatAcces.getDateDeconnexion().getTime();
      }
      if (certificatAcces != null && (dateDeconnexion == null || dateDeconnexion > now)) {
        codeUtilisateur = certificatAcces.getCodeUtilisateur();
      }
      if (majAcces) {
        if (certificatAcces.getCodeClient() != null) {
          this.getCertificatAccesDao().notifierUtilisateurDernierAcces(codeUtilisateur,
              certificatAcces.getCodeFournisseurAcces(), certificatAcces.getCodeClient(),
              certificatAcces.getCertificat(), ip, true);
        } else {
          this.getCertificatAccesDao().notifierUtilisateurDernierAcces(codeUtilisateur);
        }
      }
    }
    return codeUtilisateur;
  }

  public String getCodeUtilisateur(String certificat, boolean majAcces) {
    return getCodeUtilisateur(certificat, null, majAcces);
  }

  public String getCodeUtilisateur(String certificat) {
    return getCodeUtilisateur(certificat, false);
  }

  public void terminerCertificat(String certificat) {
    if (certificat != null) {
      CertificatAcces certificatAcces = (CertificatAcces) this.getDao().get(certificat);
      // Si le certificat existe
      if (certificatAcces != null) {
        certificatAcces.setDateDeconnexion(new Date());
        this.getDao().modifier(certificatAcces);
      }
    } else {
      if (log.isInfoEnabled()) {
        log.info("Appel de terminerCertificat avec un certification null");
      }
    }
  }

  public void terminerCertificatPourCode(String codeUtilisateur) {
    // Invalider l'ancien certificat s'il y a lieu
    List<CertificatAcces> listeCertificatValide = this.getCertificatAccesDao()
        .getListeCertificatAccesValide(codeUtilisateur);
    Iterator<CertificatAcces> iterateur = listeCertificatValide.iterator();
    while (iterateur.hasNext()) {
      CertificatAcces certificat = iterateur.next();
      terminerCertificat(certificat.getCertificat());
    }
  }

  @Override
  public String genererCertificat(String codeUtilisateur) {
    return genererCertificat(codeUtilisateur, null);
  }

  @Override
  public String genererCertificat(String codeUtilisateur, Integer nbJourExpiration) {
    return genererCertificat(codeUtilisateur, nbJourExpiration, null, null, null);
  }

  @Override
  public String genererCertificat(String codeUtilisateur, Integer nbJourExpiration, String ip, String userAgent,
      String urlReferer, String codeClient) {
    return genererCertificat(codeUtilisateur, nbJourExpiration, ip, userAgent, urlReferer, codeClient, null);
  }

  public CertificatAcces getCertificatAccesValide(String codeUtilisateur) {
    return this.getCertificatAccesDao().getCertificatAccesValide(codeUtilisateur);
  }

  @Override
  public String genererCertificat(String codeUtilisateur, Integer nbJourExpiration, String ip, String userAgent,
      String urlReferer, String codeClient, String codeFournisseurAcces) {
    // Invalider l'ancien certificat s'il y a lieu
    CertificatAcces ancienCertificatAcces = this.getCertificatAccesDao().getCertificatAccesValide(codeUtilisateur);
    if (nbJourExpiration == null && inactiverAncienCertificat && ancienCertificatAcces != null) {
      ancienCertificatAcces.setDateDeconnexion(new Date());
      this.getDao().modifier(ancienCertificatAcces);
    }

    String certificat = creerCertificat(codeUtilisateur);

    CertificatAcces authentification = new CertificatAcces();
    authentification.setCertificat(certificat);
    authentification.setCodeUtilisateur(codeUtilisateur);
    authentification.setDateConnexion(new Date());
    authentification.setIp(ip);
    authentification.setUserAgent(userAgent);
    authentification.setUrlReferer(urlReferer);
    authentification.setCodeFournisseurAcces(codeFournisseurAcces);
    authentification.setNbAcces(new Integer(1));
    authentification.setDateDernierAcces(new java.util.Date());

    if (codeClient != null) {
      codeClient = codeClient.toUpperCase();
    }
    authentification.setCodeClient(codeClient);

    if (nbJourExpiration != null) {
      java.util.Date dateExpiration = UtilitaireDate.ajouter_jr_AAAA_MM_JJ_En_UtilDate(new java.util.Date(),
          nbJourExpiration);
      authentification.setDateDeconnexion(dateExpiration);
    }
    authentification.setModifie(true);
    this.getDao().ajouter(authentification);

    if (codeClient != null) {
      this.getCertificatAccesDao().notifierUtilisateurDernierAcces(codeUtilisateur, codeFournisseurAcces, codeClient,
          certificat, ip, false);
    } else {
      this.getCertificatAccesDao().notifierUtilisateurDernierAcces(codeUtilisateur);
    }

    return authentification.getCertificat();
  }

  public String modifierCertificat(String certificatCourant, String codeUtilisateur) {
    String certificatNouveau = creerCertificat(codeUtilisateur);

    getCertificatAccesDao().modifierCertificat(certificatCourant, certificatNouveau);

    return certificatNouveau;
  }

  private String creerCertificat(String codeUtilisateur) {
    // Ajouter le nouveau certificat
    // Création de l'objet de transfert de la nouvelle authentification.
    IdentifiantUnique identificationUnique = new IdentifiantUnique(codeUtilisateur);
    String certificat = identificationUnique.toString();

    // INFRA-176 - Ajouter le userId encrypté en prefix du certificat d'acces
    String clePrivee = (String) GestionParametreSysteme.getInstance().getParametreSysteme("clePriveeCertificat");
    if (StringUtils.isNotBlank(clePrivee)) {
      Key cle;
      try {
        cle = UtilitaireEncryption.generateKey("DES", clePrivee);
        String codeUtilisateurEncrypt = UtilitaireEncryption.encrypt(cle, codeUtilisateur);
        Encoder urlEncoder = Base64.getUrlEncoder().withoutPadding();
        String codeUtilisateurEncode = urlEncoder.encodeToString(codeUtilisateurEncrypt.getBytes());
        certificat = codeUtilisateurEncode + SEPARATEUR_CODE_UTILISATEUR_CERTIFICAT + certificat;
      } catch (EncryptionException e) {
        log.error("Erreur lors de la génération de la valeur encrypté du code utilisateur pour le certificat.", e);
      }
    }
    return certificat;
  }

  public String genererCertificat(String codeUtilisateur, Integer nbJourExpiration, String ip, String userAgent,
      String urlReferer) {
    return genererCertificat(codeUtilisateur, nbJourExpiration, ip, userAgent, urlReferer, null);
  }

  public void nettoyerCertificats() {
    this.getCertificatAccesDao().nettoyerCertificats(this.nombreHeureInvalidationCertificatValide,
        this.nombreJourDestructionCertificatDeconnete);
  }

  public Integer getNombreHeureInvalidationCertificatValide() {
    return nombreHeureInvalidationCertificatValide;
  }

  public void setNombreHeureInvalidationCertificatValide(Integer nombreHeureInvalidationCertificatValide) {
    this.nombreHeureInvalidationCertificatValide = nombreHeureInvalidationCertificatValide;
  }

  public Integer getNombreJourDestructionCertificatDeconnete() {
    return nombreJourDestructionCertificatDeconnete;
  }

  public void setNombreJourDestructionCertificatDeconnete(Integer nombreJourDestructionCertificatDeconnete) {
    this.nombreJourDestructionCertificatDeconnete = nombreJourDestructionCertificatDeconnete;
  }

  public Boolean getInactiverAncienCertificat() {
    return inactiverAncienCertificat;
  }

  public void setInactiverAncienCertificat(Boolean inactiverAncienCertificat) {
    this.inactiverAncienCertificat = inactiverAncienCertificat;
  }

  private CertificatAccesDao getCertificatAccesDao() {
    return (CertificatAccesDao) this.getDao();
  }

  public ListeNavigation getListeCertificat(ListeNavigation liste) {
    return this.getCertificatAccesDao().getListe(liste);
  }

  public ListeNavigation getListeSommaireCertificat(ListeNavigation liste) {
    return this.getCertificatAccesSommaireDao().getListe(liste);

  }

  public CertificatAccesSommaireDao getCertificatAccesSommaireDao() {
    return certificatAccesSommaireDao;
  }

  public void setCertificatAccesSommaireDao(CertificatAccesSommaireDao certificatAccesSommaireDao) {
    this.certificatAccesSommaireDao = certificatAccesSommaireDao;
  }

}