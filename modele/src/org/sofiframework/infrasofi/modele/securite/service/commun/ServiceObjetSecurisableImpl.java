/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.commun;

import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.service.ServiceObjetSecurisable;
import org.sofiframework.infrasofi.modele.securite.dao.commun.ApplicationDao;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;

/**
 * Classe implémentant l'interface ServiceObjetSecurisableLocal et qui
 * implémente la méthode getApplicationAvecObjetSecurisable(...).
 * 
 * @author Jean-Maxime Pelletier
 * @author Marie-Soleil L'Allier (Nurun inc.)
 */
public class ServiceObjetSecurisableImpl extends DaoServiceImpl implements
ServiceObjetSecurisable {

  public Application getApplicationAvecObjetSecurisable(String codeApplication,
      boolean applicationActiveSeulement) {
    Application application = getApplicationDao().getApplication(
        codeApplication, applicationActiveSeulement);
    return application;
  }

  private ApplicationDao getApplicationDao() {
    return (ApplicationDao) getDao();
  }

  public Application getApplicationAvecObjetSecurisable(String codeApplication,
      String codeFacette, boolean applicationActiveSeulement) {
    Application application = this.getApplicationDao().getApplication(codeApplication, codeFacette, applicationActiveSeulement);
    return application;
  }
}