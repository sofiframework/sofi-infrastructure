package org.sofiframework.infrasofi.modele.securite.service.local;

import java.util.List;
import java.util.Set;

import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.ProprieteUtilisateur;
import org.sofiframework.infrasofi.modele.filtre.FiltreProprieteUtilisateur;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.ProprieteUtilisateurDao;
import org.sofiframework.infrasofi.modele.service.ServiceProprieteUtilisateur;

public class ServiceProprieteUtilisateurImpl extends BaseServiceInfrastructureImpl implements ServiceProprieteUtilisateur {

  private ProprieteUtilisateurDao proprieteUtilisateurDao;

  public List<ProprieteUtilisateur> getListeProprieteUtilisateur(Long utilisateurId) {
    FiltreProprieteUtilisateur f = new FiltreProprieteUtilisateur();
    f.setUtilisateurId(utilisateurId);
    return this.getProprieteUtilisateurDao().getListeProprieteUtilisateur(f);
  }

  public void sauvegarderProprieteUtilisateur(ProprieteUtilisateur proprieteUtilisateur) {
    if (proprieteUtilisateur.getId() == null) {
      this.getProprieteUtilisateurDao().ajouterProprieteUtilisateur(proprieteUtilisateur);
    } else {
      this.getProprieteUtilisateurDao().modifierProprieteUtilisateur(proprieteUtilisateur);
    }
  }

  /**
   * Supprimer les propriétés qui ne sont pas incluses dans la liste spécifiée
   * (not in) pour un utilisateur.
   */
  public void supprimerProprieteUtilisateur(Long utilisateurId, Set<Long> exclureIds) {
    this.proprieteUtilisateurDao.supprimerProprieteUtilisateur(utilisateurId, exclureIds);
  }

  public ProprieteUtilisateurDao getProprieteUtilisateurDao() {
    return proprieteUtilisateurDao;
  }

  public void setProprieteUtilisateurDao(ProprieteUtilisateurDao proprieteUtilisateurDao) {
    this.proprieteUtilisateurDao = proprieteUtilisateurDao;
  }
}
