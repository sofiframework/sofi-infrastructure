/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.local;

import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.Role;
import org.sofiframework.infrasofi.modele.filtre.FiltreRole;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.RoleDao;
import org.sofiframework.infrasofi.modele.service.ServiceRole;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Service de gestion des rôles.
 */
public class ServiceRoleImpl extends BaseServiceInfrastructureImpl implements
ServiceRole {

  public ListeNavigation getListeRolePilotage(ListeNavigation liste) {
    return this.getRoleDao().getListeRolePilotage(liste, this.getCodeUtilisateurEnCours());
  }

  /**
   * Recherche une liste contenant les rôles disponible pour une application.
   * <p>
   * 
   * @param codeApplication
   *          l'identifiant unique de l'application pour lequel on recherche des
   *          rôles
   * @return la liste de tous les rôles disponible dans l'application
   */
  @SuppressWarnings("unchecked")
  public List<Role> getListeRole(String codeApplication) {
    FiltreRole filtre = new FiltreRole();
    filtre.setCodeApplication(codeApplication);
    return this.getDao().getListe(filtre);
  }

  public Role getRole(String codeRole, String codeApplication) {
    Role cle = new Role();
    cle.setCodeRole(codeRole);
    cle.setCodeApplication(codeApplication);
    return (Role) getDao().get(cle);
  }

  public void enregistrerRole(Role role) {
    this.enregistrer(role);
  }

  public void supprimerRole(String codeRole, String codeApplication) {
    this.supprimer(new Role(codeRole, codeApplication));
  }

  @SuppressWarnings("unchecked")
  public List<Role> getListeRolePilotage(String codeApplication, String codeStatut) {
    String codeUtilisateurPilote = this.getCodeUtilisateurEnCours();
    List<Role> liste = null;

    if (!UtilitaireString.isVide(codeApplication) && !UtilitaireString.isVide(codeUtilisateurPilote)) {
      liste = this.getRoleDao().getListeRolePilotage(codeApplication, codeStatut,
          codeUtilisateurPilote);
    }

    return liste;
  }

  @SuppressWarnings("unchecked")
  public List<Role> getListeRoleDisponible(String codeUtilisateur,
      String codeApplication) {
    String codeUtilisateurPilote = this.getCodeUtilisateurEnCours();
    return this.getRoleDao().getListeRoleDisponible(codeUtilisateur,
        codeApplication, codeUtilisateurPilote);
  }

  private RoleDao getRoleDao() {
    return (RoleDao) this.getDao();
  }
}