/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.local;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.objetstransfert.ChangementMotPasse;
import org.sofiframework.application.securite.objetstransfert.HistoriqueMotPasse;
import org.sofiframework.application.securite.objetstransfert.Role;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.commun.Domaine;
import org.sofiframework.infrasofi.modele.entite.Utilisateur;
import org.sofiframework.infrasofi.modele.entite.UtilisateurApplication;
import org.sofiframework.infrasofi.modele.entite.UtilisateurRole;
import org.sofiframework.infrasofi.modele.filtre.FiltreUtilisateurApplication;
import org.sofiframework.infrasofi.modele.filtre.FiltreUtilisateurRole;
import org.sofiframework.infrasofi.modele.securite.dao.commun.UtilisateurMajDao;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.HistoriqueMotPasseDao;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.UtilisateurApplicationDao;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.UtilisateurDao;
import org.sofiframework.infrasofi.modele.service.ServiceRole;
import org.sofiframework.infrasofi.modele.service.ServiceUtilisateur;
import org.sofiframework.infrasofi.modele.service.ServiceUtilisateurRole;
import org.sofiframework.infrasofi.modele.service.ServiceValidationAuthentification;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.securite.encryption.UtilitaireEncryption;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Service de gestion des utilisateurs.
 * 
 * @author Jean-Maxime Pelletier
 * @author sylvain Deschênes
 * @author Jérôme Fiolleau
 */
public class ServiceUtilisateurImpl extends BaseServiceInfrastructureImpl implements ServiceUtilisateur, ServiceValidationAuthentification {

  private HistoriqueMotPasseDao historiqueMotPasseDao = null;

  private UtilisateurApplicationDao utilisateurApplicationDao = null;

  private UtilisateurMajDao utilisateurMajDao = null;

  private ServiceUtilisateurRole serviceUtilisateurRole = null;

  private ServiceRole serviceRole = null;

  public void changerMotPasseExpirer(ChangementMotPasse changementMotPasse) {

    // Get system parameter
    Integer nbMdpHistorique = GestionParametreSysteme.getInstance().getInteger("NB_MOT_PASSE_HISTORIQUE");

    Utilisateur utilisateur = this.getUtilisateur(changementMotPasse.getCodeUtilisateur());

    if (changementMotPasse.getNouveauMotPasse().equals(changementMotPasse.getConfirmerMotPasse())) {

      List<HistoriqueMotPasse> listeMotPasse = this.getHistoriqueMotPasseDao().getListeHistoriqueMotPasse(utilisateur.getCodeUtilisateur());

      String encryption = UtilitaireEncryption.digest(changementMotPasse.getNouveauMotPasse());

      if (nbMdpHistorique > 0) {
        // Vérifie que le mot de passe n'existe pas déjà parmis les mots de
        // passe
        // sauvegardé
        for (HistoriqueMotPasse historiqueMotPasse : listeMotPasse) {
          if (encryption.equals(historiqueMotPasse.getMotPasse())) {
            throw new ModeleException("Le nouveau mot de passe est identique à un des " + nbMdpHistorique + " derniers mots de passes.");
          }
        }
      }
      // Mise à jour des informations de l'utilisateur
      utilisateur.setMotPasse(changementMotPasse.getNouveauMotPasse());
      utilisateur.setDateMotPasse(new Date());
      this.encrypterMotPasse(utilisateur);
      this.modifierUtilisateur(utilisateur);

      // Si le nombre de mots de passe déjà sauvegardé est égale au total de
      // mots de passe à sauvegarder,
      // alors supprimer le plus ancien de l'historique et sauvegarder le
      // nouveau
      // La methode qui retourne la liste des mots de passe sauvegardé les
      // retourne pra ordre de date,
      // donc il suffit de supprimer le premier enregistrement

      if (nbMdpHistorique > 0) {
        if (listeMotPasse.size() == nbMdpHistorique) {
          this.getHistoriqueMotPasseDao().supprimer(listeMotPasse.get(0));
        }

        HistoriqueMotPasse historiqueMotPasse = new HistoriqueMotPasse();
        historiqueMotPasse.setCodeUtilisateur(utilisateur.getCodeUtilisateur());
        historiqueMotPasse.setMotPasse(encryption);
        historiqueMotPasse.setDateCreation(new Date());
        this.getHistoriqueMotPasseDao().ajouter(historiqueMotPasse);
      }
    } else {
      throw new ModeleException("login.erreur.authentification.motPasse.non_identiques");
    }
  }

  public void forcerChangementMotPasse(String codeUtilisateur) {
    UtilisateurDao utilisateurDao = (UtilisateurDao) this.getDao();
    utilisateurDao.forcerChangementMotPasse(codeUtilisateur);
  }

  public void ajouterUtilisateur(Utilisateur utilisateur) {

    // Il faut toujours que la premiere lettre du prenom et du nom soit en majuscule
    utilisateur.setPrenom(UtilitaireString.convertirToutesPremieresLettreMajuscule(utilisateur.getPrenom()));
    utilisateur.setNom(UtilitaireString.convertirToutesPremieresLettreMajuscule(utilisateur.getNom()));
    
    // Encryption le mot de passe
    this.encrypterMotPasse(utilisateur);
    this.ajouter(utilisateur);
  }

  public void modifierUtilisateur(Utilisateur utilisateur) {
    
    // Il faut toujours que la premiere lettre du prenom et du nom soit en majuscule
    utilisateur.setPrenom(UtilitaireString.convertirToutesPremieresLettreMajuscule(utilisateur.getPrenom()));
    utilisateur.setNom(UtilitaireString.convertirToutesPremieresLettreMajuscule(utilisateur.getNom()));
    
    if (utilisateur.isChangementMotPasse()) {
      this.encrypterMotPasse(utilisateur);
    }
    this.modifier(utilisateur);
  }

  /**
   * Permet de mettre en lettre majuscule la première lettre de la valeur. <br/>
   * Si la valeur est un prenom (ou nom) composé (les séparations prisent en
   * compte sont le tiret et l'espace), on met en majuscule également chaque
   * lettre qui suit le séparateur.
   * 
   * @param valeur
   */
  private String capitalizePremiereLettre(String valeur) {

    String splitTiret = "-";
    String splitEspace = " ";
    
    String valeurTraitement = "";
    
    String[] valeurSplitTiret = valeur.split(splitTiret);
    for (String valeurTiret : valeurSplitTiret) {
      if (StringUtils.isNotBlank(valeurTraitement)) {
        valeurTraitement += splitTiret;
      }
      valeurTraitement += StringUtils.capitalize(valeurTiret);
    }
    
    String valeurRetour = "";
    
    String[] valeurSplitEspace = valeurTraitement.split(splitEspace);
    for (String valeurEspace : valeurSplitEspace) {
      if (StringUtils.isNotBlank(valeurRetour)) {
        valeurRetour += splitEspace;
      }
      valeurRetour += StringUtils.capitalize(valeurEspace);
    }
    
    return valeurRetour;
    
  }

  /**
   * Encrypter le mot de passe de l'utilisateur.
   * 
   * @param utilisateur
   *          Utilisateur
   */
  private void encrypterMotPasse(Utilisateur utilisateur) {
    String encryption = UtilitaireEncryption.digest(utilisateur.getMotPasse());
    utilisateur.setMotPasse(encryption);
  }

  public void changerMotPasse(ChangementMotPasse changementMotPasse) {
    Utilisateur utilisateur = this.getUtilisateur(changementMotPasse.getCodeUtilisateur());

    if (changementMotPasse.getNouveauMotPasse().equals(changementMotPasse.getConfirmerMotPasse())) {
      utilisateur.setMotPasse(changementMotPasse.getNouveauMotPasse());
      utilisateur.setDateMotPasse(new Date());
      this.encrypterMotPasse(utilisateur);
      this.modifierUtilisateur(utilisateur);
    } else {
      throw new ModeleException("login.erreur.authentification.motPasse.non_identiques");
    }
  }

  @SuppressWarnings("unchecked")
  public void supprimerUtilisateur(Long id) {

    FiltreUtilisateurApplication filtre = new FiltreUtilisateurApplication(getUtilisateurMajDao().supprimerUtilisateur(id).getCodeUtilisateur());
    List<UtilisateurApplication> listeUtilisateurApplication = getUtilisateurApplicationDao().getListe(filtre);

    if (listeUtilisateurApplication != null && !listeUtilisateurApplication.isEmpty()) {
      for (UtilisateurApplication ua : listeUtilisateurApplication) {
        this.getUtilisateurApplicationDao().supprimer(ua);
      }
    }
  }

  public ListeNavigation getListeUtilisateurPilotage(ListeNavigation liste) {
    String codeUtilisateur = this.getGestionnaireUtilisateur().getUtilisateur().getCodeUtilisateur();
    FiltreUtilisateurRole filtre = (FiltreUtilisateurRole) liste.getFiltre();
    String codeApplication = filtre.getCodeApplication();

    /*
     * Obtenir la liste des rôles de l'utilisateur pour l'application
     */
    List<String> listeCleRole = this.getListeCleRoleUtilisateur(codeApplication);

    // Liste des utilisateurs pour ses rôles.
    this.getUtilisateurDao().getListeUtilisateurPilotage(liste, codeUtilisateur, listeCleRole);
    return liste;
  }

  @SuppressWarnings("unchecked")
  private List<String> getListeCleRoleUtilisateur(String codeApplication) {
    List<Role> listeRole = null;

    // on doit faire de quoi qui sort les roles avec tous les enfants
    org.sofiframework.application.securite.objetstransfert.Utilisateur utilisateur = this.getGestionnaireUtilisateur().getUtilisateur();

    if (!StringUtils.isEmpty(codeApplication)) {
      listeRole = utilisateur.getListeRole(codeApplication);
    } else {
      listeRole = utilisateur.getListeRoles();
    }

    List<String> listeCleRole = new ArrayList<String>();

    /*
     * Obtenir récurcivement les rôles enfants
     */
    for (Role role : listeRole) {
      this.ajouterCleRoleAvecEnfants(listeCleRole, role);
    }

    return listeCleRole;
  }

  @SuppressWarnings("unchecked")
  private void ajouterCleRoleAvecEnfants(List<String> listeCleRole, Role role) {
    if (role.getListeClient() != null) {
      for (String codeClient : (List<String>) role.getListeClient()) {
        listeCleRole.add(role.getCode() + "/" + role.getCodeApplication() + "/" + codeClient);
      }
    } else {
      listeCleRole.add(role.getCode() + "/" + role.getCodeApplication());
    }

    for (Role roleEnfant : (List<Role>) role.getListeRoles()) {
      this.ajouterCleRoleAvecEnfants(listeCleRole, roleEnfant);
    }
  }

  public Utilisateur getUtilisateur(String codeUtilisateur) {
    return this.getUtilisateurDao().getUtilisateur(codeUtilisateur);
  }

  /**
   * Permet d'obtenir une entité utilisateur complétée avec une entité
   * utilisateur Application. Si l'utilisateurApplication n'existe pas, on crée
   * une nouvelle instance.
   * 
   * @param codeUtilisateur
   *          Code utilisateur
   * @param codeApplication
   *          Code de l'application
   * @return Entité utilisateur
   */
  public Utilisateur getUtilisateur(String codeUtilisateur, String codeApplication) {
    Utilisateur utilisateur = this.getUtilisateur(codeUtilisateur);
    if (utilisateur != null) {
      // Charger l'utilisateur application
      UtilisateurApplication utilisateurApplication = this.utilisateurApplicationDao.getUtilisateurApplication(codeUtilisateur, codeApplication);
      if (utilisateurApplication == null) {
        utilisateurApplication = new UtilisateurApplication();
        utilisateurApplication.setCodeUtilisateur(codeUtilisateur);
        utilisateurApplication.setCodeApplication(codeApplication);
      }
      utilisateur.setUtilisateurApplication(utilisateurApplication);
    }
    return utilisateur;
  }

  public Utilisateur getUtilisateurActif(String identifiant) {
    return getUtilisateurDao().getUtilisateurActif(identifiant);
  }

  private UtilisateurDao getUtilisateurDao() {
    UtilisateurDao utilisateurDao = (UtilisateurDao) this.getDao();
    return utilisateurDao;
  }

  public boolean validerAuthentification(String codeUtilisateur, String motDePasse) {
    Utilisateur utilisateur = (Utilisateur) this.getDao().get(codeUtilisateur);
    boolean valide = false;
    if (utilisateur != null) {
      valide = UtilitaireEncryption.isDigestsEquals(utilisateur.getMotPasse(), motDePasse);
    }
    return valide;
  }

  public HistoriqueMotPasseDao getHistoriqueMotPasseDao() {
    return historiqueMotPasseDao;
  }

  public void setHistoriqueMotPasseDao(HistoriqueMotPasseDao historiqueMotPasseDao) {
    this.historiqueMotPasseDao = historiqueMotPasseDao;
  }

  public void setUtilisateurApplicationDao(UtilisateurApplicationDao utilisateurApplicationDao) {
    this.utilisateurApplicationDao = utilisateurApplicationDao;
  }

  public UtilisateurApplicationDao getUtilisateurApplicationDao() {
    return utilisateurApplicationDao;
  }

  public void enregistrerUtilisateur(Utilisateur utilisateur) {
    
    // Il faut toujours que la premiere lettre du prenom et du nom soit en majuscule
    utilisateur.setPrenom(capitalizePremiereLettre(utilisateur.getPrenom()));
    utilisateur.setNom(capitalizePremiereLettre(utilisateur.getNom()));
    
    if (utilisateur.getDateCreation() != null) {
      this.modifier(utilisateur);
    } else {
      this.ajouter(utilisateur);
    }
    this.enregistrerUtilisateurApplication(utilisateur.getUtilisateurApplication());
  }

  public void enregistrerUtilisateurApplication(UtilisateurApplication utilisateurApplication) {
    if (utilisateurApplication.getDateCreation() != null) {
      utilisateurApplication.setDateModification(new Date());
      if (this.getUtilisateur() != null) {
        utilisateurApplication.setModifiePar(this.getUtilisateur().getCodeUtilisateur());
      }
      this.getUtilisateurApplicationDao().modifier(utilisateurApplication);
    } else {
      utilisateurApplication.setDateCreation(new Date());
      if (this.getUtilisateur() != null) {
        utilisateurApplication.setCreePar(this.getUtilisateur().getCodeUtilisateur());
      }
      this.getUtilisateurApplicationDao().ajouter(utilisateurApplication);
    }
  }

  public void inscrire(Utilisateur utilisateur, String codeClient) {
    // On doit encrypter le mot de passe.
    this.encrypterMotPasse(utilisateur);
    // Actif
    utilisateur.setCodeStatut(Domaine.statut.actif.getValeur());
    // On doit ajouter l'utilisateur
    this.ajouter(utilisateur);

    this.ajouterUtilisateurExterneApplication(utilisateur.getCodeUtilisateur(), Domaine.application.console.getValeur());

    UtilisateurRole droitConsole = new UtilisateurRole();
    droitConsole.setCodeRole(Domaine.role.accesDeveloppeur.getValeur());
    droitConsole.setCodeApplication(Domaine.application.console.getValeur());
    droitConsole.setCodeUtilisateur(utilisateur.getCodeUtilisateur());
    droitConsole.setCodeClient(codeClient);
    this.getServiceUtilisateurRole().ajouter(droitConsole);
  }

  private void ajouterUtilisateurExterneApplication(String codeUtilisateur, String codeApplication) {
    // Ajouter le lien ut. app. avec la console
    UtilisateurApplication utilisateurConsole = new UtilisateurApplication();
    utilisateurConsole.setCodeApplication(codeApplication);
    utilisateurConsole.setCodeStatut(Domaine.statut.actif.getValeur());
    utilisateurConsole.setCodeTypeUtilisateur(Domaine.typeUtilisateur.externe.getValeur());
    utilisateurConsole.setCodeUtilisateur(codeUtilisateur);
    utilisateurConsole.setCreePar(codeUtilisateur);
    utilisateurConsole.setDateCreation(new Date());
    this.getUtilisateurApplicationDao().ajouter(utilisateurConsole);
  }

  public void supprimerUtilisateur(String codeUtilisateur) {
    Utilisateur utilisateur = getUtilisateur(codeUtilisateur);
    supprimer(utilisateur.getId());
  }

  public void setServiceUtilisateurRole(ServiceUtilisateurRole serviceUtilisateurRole) {
    this.serviceUtilisateurRole = serviceUtilisateurRole;
  }

  public ServiceUtilisateurRole getServiceUtilisateurRole() {
    return serviceUtilisateurRole;
  }

  public void setServiceRole(ServiceRole serviceRole) {
    this.serviceRole = serviceRole;
  }

  public ServiceRole getServiceRole() {
    return serviceRole;
  }

  public void setUtilisateurMajDao(UtilisateurMajDao utilisateurMajDao) {
    this.utilisateurMajDao = utilisateurMajDao;
  }

  public UtilisateurMajDao getUtilisateurMajDao() {
    return utilisateurMajDao;
  }

}