/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.commun;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.sofiframework.application.securite.objetstransfert.AccesApplication;
import org.sofiframework.application.securite.objetstransfert.Role;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.objetstransfert.UtilisateurRole;
import org.sofiframework.application.securite.service.ServiceSecurite;
import org.sofiframework.infrasofi.modele.filtre.FiltreUtilisateurRole;
import org.sofiframework.infrasofi.modele.securite.dao.commun.AccesApplicationDao;
import org.sofiframework.infrasofi.modele.securite.dao.commun.UtilisateurDao;
import org.sofiframework.infrasofi.modele.securite.dao.commun.UtilisateurRoleDao;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;

/**
 * Classe implémentant l'interface ServiceSecuriteLocal et qui implémente la
 * méthode getUtilisateurAvecObjetsSecurises(...).
 * 
 * @author Jean-Maxime Pelletier
 * @author Marie-Soleil L'Allier Nurun inc.
 */
public class ServiceSecuriteImpl extends ServiceUtilisateurImpl implements ServiceSecurite {

  private UtilisateurRoleDao utilisateurRoleDao = null;

  private AccesApplicationDao accesApplicationDao = null;

  public Utilisateur getUtilisateurAvecObjetsSecurises(String codeApplication, String identifiant) {
    Utilisateur utilisateur = getUtilisateurDao().getUtilisateurAvecObjetsSecurises(codeApplication, identifiant);
    if (utilisateur != null) {
      this.completerListeClient(utilisateur);
      this.completerAccesApplication(utilisateur, codeApplication);
    }
    return utilisateur;
  }

  /**
   * Extraire l'utilisateur
   * 
   * @return l'utilisateur
   */
  public Utilisateur getUtilisateur(String identifiant) {
    if (identifiant != null) {
      identifiant = identifiant.toLowerCase();
    }

    // Extraire l'utilisateur.
    Utilisateur utilisateur = new Utilisateur();
    utilisateur.setCodeUtilisateur(identifiant);
    utilisateur = (Utilisateur) this.getDao().get(utilisateur);

    if (utilisateur != null) {
      this.completerListeClient(utilisateur);
    }

    return utilisateur;
  }


  public Utilisateur getUtilisateurAvecObjetsSecurises(String codeApplication, String codeFacette, String identifiant) {
    Utilisateur utilisateur = this.getUtilisateurSecuriseSelonClient(codeApplication, codeFacette, identifiant, null);

    if (utilisateur != null) {
      this.completerListeClient(utilisateur);
      this.completerAccesApplication(utilisateur, codeApplication);
    }

    return utilisateur;
  }

  public Utilisateur getUtilisateurSecuriseSelonClient(String codeApplication, String identifiant, String codeClient) {
    Utilisateur utilisateur = this.getUtilisateurSecuriseSelonClient(codeApplication, null, identifiant, codeClient);

    if (utilisateur != null) {
      this.completerListeClient(utilisateur);
      this.completerAccesApplication(utilisateur, codeApplication);
    }

    return utilisateur;
  }

  public Utilisateur getUtilisateurSecuriseSelonClient(String codeApplication, String codeFacette, String identifiant, String codeClient) {
    Utilisateur utilisateur = this.getUtilisateurDao().getUtilisateurAvecObjetsSecurises(codeApplication, codeFacette, identifiant, codeClient);

    if (utilisateur != null) {
      this.completerListeClient(utilisateur);
      this.completerAccesApplication(utilisateur, codeApplication);
    }

    return utilisateur;
  }

  private void completerAccesApplication(Utilisateur utilisateur, String codeApplication) {
    AccesApplication accesApplication = new AccesApplication(utilisateur.getCodeUtilisateur(), codeApplication);
    accesApplication = (AccesApplication) getAccesApplicationDao().get(accesApplication);
    utilisateur.setAccesApplication(accesApplication);
  }

  public void setUtilisateurRoleDao(UtilisateurRoleDao utilisateurRoleDao) {
    this.utilisateurRoleDao = utilisateurRoleDao;
  }

  public UtilisateurRoleDao getUtilisateurRoleDao() {
    return utilisateurRoleDao;
  }

  public void setAccesApplicationDao(AccesApplicationDao accesApplicationDao) {
    this.accesApplicationDao = accesApplicationDao;
  }

  public AccesApplicationDao getAccesApplicationDao() {
    return accesApplicationDao;
  }
}