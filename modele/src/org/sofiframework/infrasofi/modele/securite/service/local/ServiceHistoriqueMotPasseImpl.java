/**
 * 
 */
/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.local;

import java.util.List;

import org.sofiframework.application.securite.objetstransfert.HistoriqueMotPasse;
import org.sofiframework.application.securite.service.ServiceHistoriqueMotPasse;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.HistoriqueMotPasseDao;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;

/**
 * @author Jérôme Fiolleau, Nurun inc.
 * @date 09-10-07
 * 
 */
public class ServiceHistoriqueMotPasseImpl extends DaoServiceImpl implements ServiceHistoriqueMotPasse {

  /**
   * {@inheritDoc}
   */
  public List<HistoriqueMotPasse> getListeHistoriqueMotPasse(String codeUtilisateur) {
    return ((HistoriqueMotPasseDao) this.getDao()).getListeHistoriqueMotPasse(codeUtilisateur);
  }

}
