/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.local;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.Application;
import org.sofiframework.infrasofi.modele.entite.ObjetJava;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.ApplicationDao;
import org.sofiframework.infrasofi.modele.service.ServiceApplication;
import org.sofiframework.infrasofi.modele.service.ServiceReferentiel;

public class ServiceApplicationImpl extends BaseServiceInfrastructureImpl implements ServiceApplication {

  ServiceReferentiel serviceReferentiel = null;

  public Application getApplication(String code) {
    return (Application) this.get(code);
  }

  public Application getApplication(String codeApplication, String codeFacette) {
    return getApplicationDao().getApplication(codeApplication, codeFacette);
  }

  public void enregistrer(Application application, Application applicationOriginale) {
    // Déterminer le statut d'activation de l'application
    Date dateDuJour = new Date();
    if (application.getDateDebutActivite() != null && dateDuJour.before(application.getDateDebutActivite())
        || application.getDateFinActivite() != null && dateDuJour.after(application.getDateFinActivite())) {
      application.setStatut("I");
    } else {
      application.setStatut("A");
    }

    if (application.getDateCreation() == null) {
      this.ajouter(application);
    } else {
      if (applicationOriginale != null && !application.equals(applicationOriginale)) {
        this.modifierCleApplication(application, applicationOriginale);
      } else {
        this.modifier(application);
      }
    }
  }

  /**
   * Modifie la clé d'une application. - Ajuste les codes d'applications des
   * objets java. - Crée une nouvelle application avec le nouveau code -
   * Supprime l'ancienne application
   * 
   * @param application
   *          Application
   * @param applicationOriginale
   *          Application originale (avec ancien code)
   */
  private void modifierCleApplication(Application application, Application applicationOriginale) {
    List<ObjetJava> listeObjetJava = new ArrayList<ObjetJava>();

    /*
     * On doit modifier toute la liste des objets et ajuster le code de
     * l'application pour l'instance courante
     */
    listeObjetJava.addAll(application.getListeObjetJava());
    this.modifierCodeApplication(listeObjetJava, application.getCodeApplication());
    application.setListeObjetJava(listeObjetJava);

    /*
     * En réalité quand on modifie la clé on ajoute la nouvelle application et
     * on supprime l'ancienne
     */
    this.ajouter(application);
    this.supprimerApplication(applicationOriginale.getCodeApplication());
  }

  /**
   * Ajuste récursivement les codes d'application...a ce moment on doit créer de
   * nouvelles collecions puis ajouter tous les éléments (récursivement).
   * 
   * @param listeObjetJava
   *          Liste d'objets java d'un application
   * @param codeApplication
   *          Nouveau code d'application a assigner a toutes les entrées
   */
  private void modifierCodeApplication(List<ObjetJava> listeObjetJava, String codeApplication) {
    if (listeObjetJava != null) {
      for (Iterator<ObjetJava> i = listeObjetJava.iterator(); i.hasNext();) {
        ObjetJava o = i.next();
        o.setCodeApplication(codeApplication);
        List<ObjetJava> listeEnfant = null;
        if (o.getListeObjetJavaEnfant() != null) {
          listeEnfant = new ArrayList<ObjetJava>();
          listeEnfant.addAll(o.getListeObjetJavaEnfant());
          this.modifierCodeApplication(listeEnfant, codeApplication);
          o.setListeObjetJavaEnfant(listeEnfant);
        }
      }
    }
  }

  public void supprimerApplication(String codeApplication) {
    this.getApplicationDao().supprimer(codeApplication);
  }

  @SuppressWarnings("unchecked")
  public List<Application> getListeApplication() {
    return this.getApplicationDao().getListe();
  }

  public ListeNavigation getListeApplicationAccessible(ListeNavigation liste) {
    String codeUtilisateur = this.getGestionnaireUtilisateur().getUtilisateur().getCodeUtilisateur();
    liste.ajouterTriInitial("Nom", true);
    return this.getApplicationDao().getListeApplicationAccessible(codeUtilisateur, liste, Boolean.FALSE);
  }

  @SuppressWarnings("unchecked")
  public List<Application> getListeApplicationAccessible() {
    String codeUtilisateur = this.getGestionnaireUtilisateur().getUtilisateur().getCodeUtilisateur();
    return this.getApplicationDao().getListeApplicationAccessible(codeUtilisateur, Boolean.FALSE);
  }

  public ListeNavigation getListeApplicationAccessiblePilotage(ListeNavigation liste) {
    String codeUtilisateur = this.getGestionnaireUtilisateur().getUtilisateur().getCodeUtilisateur();
    liste.ajouterTriInitial("Nom", true);
    return this.getApplicationDao().getListeApplicationAccessible(codeUtilisateur, liste, Boolean.TRUE);
  }

  @SuppressWarnings("unchecked")
  public List<Application> getListeApplicationAccessiblePilotage() {
    String codeUtilisateur = this.getGestionnaireUtilisateur().getUtilisateur().getCodeUtilisateur();
    return this.getApplicationDao().getListeApplicationAccessible(codeUtilisateur, Boolean.TRUE);
  }

  public ApplicationDao getApplicationDao() {
    return (ApplicationDao) getDao();
  }

  public ServiceReferentiel getServiceReferentiel() {
    return serviceReferentiel;
  }

  public void setServiceReferentiel(ServiceReferentiel serviceReferentiel) {
    this.serviceReferentiel = serviceReferentiel;
  }
}
