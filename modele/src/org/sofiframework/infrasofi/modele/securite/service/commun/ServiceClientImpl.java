/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.commun;

import java.util.ArrayList;
import java.util.List;

import org.sofiframework.application.securite.objetstransfert.Client;
import org.sofiframework.application.securite.service.ServiceClient;
import org.sofiframework.infrasofi.modele.securite.dao.commun.ClientDao;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;

/**
 * Implémentation de l'interface d'accès aux clients.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 * 
 */
public class ServiceClientImpl extends DaoServiceImpl implements ServiceClient {

  private ClientDao getClientDao() {
    return (ClientDao) this.getDao();
  }

  public Client getClient(String codeClient) {
    return (Client) getDao().get(codeClient);
  }

  public List<Client> getListeClientDescendant(String codeClient) {

    List<Client> listeClient = new ArrayList<Client>();
    if (codeClient == null) {
      List<Client> listeClientParents = getClientDao().getListeClientParents();
      listeClient.addAll(listeClientParents);
    } else {
      Client client = getClient(codeClient);
      listeClient.addAll(client.getListeClientEnfant());
    }

    return listeClient;
  }

  public List<Client> getListeClientAscendant(String codeClient) {
    List<Client> listeClientParent = new ArrayList<Client>();
    if (codeClient != null) {
      Client clientEnfant = getClient(codeClient);
      getListeClientAscendant(listeClientParent, clientEnfant);

    }
    return listeClientParent;
  }

  public List<Client> getListeClientRacine() {
    return getClientDao().getListeClientParents();
  }

  /**
   * Méthode récursive permettant d'ajouter le parent du client passé en paramètre dans la liste.
   * 
   * @param listeClientParent
   * @param clientEnfant
   */
  private void getListeClientAscendant(List<Client> listeClientParent, Client clientEnfant) {
    if (clientEnfant != null && clientEnfant.getCodeClientParent() != null) {
      Client clientParent = getClient(clientEnfant.getCodeClientParent());
      listeClientParent.add(clientParent);
      getListeClientAscendant(listeClientParent, clientParent);
    }
  }
}
