/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.service.commun;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.application.securite.filtre.FiltrePropriete;
import org.sofiframework.application.securite.objetstransfert.AccesApplication;
import org.sofiframework.application.securite.objetstransfert.AccesExterne;
import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.objetstransfert.ChangementMotPasse;
import org.sofiframework.application.securite.objetstransfert.Client;
import org.sofiframework.application.securite.objetstransfert.Propriete;
import org.sofiframework.application.securite.objetstransfert.Role;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.objetstransfert.UtilisateurRole;
import org.sofiframework.application.securite.service.ServiceValidationAuthentification;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.MetadonneePropriete;
import org.sofiframework.infrasofi.modele.entite.ProprieteUtilisateur;
import org.sofiframework.infrasofi.modele.entite.UtilisateurApplication;
import org.sofiframework.infrasofi.modele.filtre.FiltreUtilisateurApplication;
import org.sofiframework.infrasofi.modele.filtre.FiltreUtilisateurRole;
import org.sofiframework.infrasofi.modele.securite.dao.commun.AccesApplicationDao;
import org.sofiframework.infrasofi.modele.securite.dao.commun.AccesExterneDao;
import org.sofiframework.infrasofi.modele.securite.dao.commun.ClientDao;
import org.sofiframework.infrasofi.modele.securite.dao.commun.UtilisateurDao;
import org.sofiframework.infrasofi.modele.securite.dao.commun.UtilisateurMajDao;
import org.sofiframework.infrasofi.modele.securite.dao.commun.UtilisateurRoleDao;
import org.sofiframework.infrasofi.modele.securite.dao.commun.hibernate.UtilisateurDaoImpl;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.ApplicationDao;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.MetadonneeProprieteDao;
import org.sofiframework.infrasofi.modele.securite.dao.gestion.UtilisateurApplicationDao;
import org.sofiframework.infrasofi.modele.service.ServiceProprieteUtilisateur;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.sofiframework.modele.spring.securite.GestionnaireUtilisateurThreadLocalImpl;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;
import org.sofiframework.securite.encryption.UtilitaireEncryption;
import org.sofiframework.utilitaire.UtilitaireObjet;
import org.sofiframework.utilitaire.UtilitaireString;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * Service commun d'utilisateurs.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceUtilisateurImpl extends DaoServiceImpl implements
    org.sofiframework.application.securite.service.ServiceUtilisateur,
    ServiceValidationAuthentification {

  private static final Log log = LogFactory
      .getLog(ServiceUtilisateurImpl.class);

  private UtilisateurRoleDao utilisateurRoleDao = null;

  private UtilisateurMajDao utilisateurMajDao = null;

  private ApplicationDao applicationDao = null;

  private org.sofiframework.infrasofi.modele.service.ServiceUtilisateur serviceUtilisateurGestion = null;

  private UtilisateurApplicationDao utilisateurApplicationDao = null;

  private AccesApplicationDao accesApplicationDao = null;

  private MetadonneeProprieteDao metadonneeProprieteDao;

  private ServiceProprieteUtilisateur serviceProprieteUtilisateur;
  
  private AccesExterneDao accesExterneDao;
  
  private ClientDao clientDao;
  


  /**
   * Permet d'ajouter un utilisateur. Le mot de passe est encrypté au niveau du
   * dao (de type dao {@link UtilisateurDaoImpl})
   */
  public Utilisateur ajouterUtilisateur(Utilisateur utilisateur) {
    
    // Il faut toujours que la premiere lettre du prenom et du nom soit en majuscule
    utilisateur.setPrenom(UtilitaireString.convertirToutesPremieresLettreMajuscule(utilisateur.getPrenom()));
    utilisateur.setNom(UtilitaireString.convertirToutesPremieresLettreMajuscule(utilisateur.getNom()));
    
    utilisateur = (Utilisateur) this.getUtilisateurMajDao()
        .ajouter(utilisateur);
    utilisateur.setModifiePar(utilisateur.getCreePar());

    // props
    this.sauvegarderProprietes(utilisateur);
    this.sauvegarderAccesExternes(utilisateur);

    AccesApplication accesApplication = utilisateur.getAccesApplication();
    if (accesApplication != null) {
      UtilisateurApplication utilisateurApplication = new UtilisateurApplication();
      utilisateurApplication.setCodeApplication(accesApplication
          .getCodeApplication());
      utilisateurApplication.setCodeUtilisateur(utilisateur
          .getCodeUtilisateur());
      utilisateurApplication.setCodeOrganisation(accesApplication
          .getCodeOrganisation());
      utilisateurApplication.setCodeStatut(accesApplication.getCodeStatut());
      utilisateurApplication.setCodeTypeUtilisateur(accesApplication
          .getCodeTypeUtilisateur());

      try {
        getServiceUtilisateurGestion().enregistrerUtilisateurApplication(
            utilisateurApplication);
      } catch (Exception e) {
        log.warn("Problème avec l'utilisateur :"
            + utilisateur.getCodeUtilisateur());
        log.warn(e);
      }
    }
    return utilisateur;
  }

  /**
   * Permet d'ajouter un accès vers une application.
   * 
   * @param accesApplication
   * @return
   */
  public AccesApplication ajouterAccesApplication(
      AccesApplication accesApplication) {
    UtilisateurApplication utilisateurApplication = new UtilisateurApplication();
    utilisateurApplication.setCodeApplication(accesApplication
        .getCodeApplication());
    utilisateurApplication.setCodeOrganisation(accesApplication
        .getCodeOrganisation());
    utilisateurApplication.setCodeUtilisateur(accesApplication
        .getCodeUtilisateur());
    utilisateurApplication.setCodeStatut(accesApplication.getCodeStatut());
    utilisateurApplication.setCodeTypeUtilisateur(accesApplication
        .getCodeTypeUtilisateur());
    try {
      getServiceUtilisateurGestion().enregistrerUtilisateurApplication(
          utilisateurApplication);
      accesApplication = (AccesApplication) getAccesApplicationDao().get(
          accesApplication);
    } catch (Exception e) {
      log.warn("Problème avec l'utilisateur :"
          + accesApplication.getCodeUtilisateur());
      log.warn(e);
    }

    return accesApplication;
  }

  public AccesApplication enregistrerAccesApplication(
      AccesApplication accesApplication) {
    if (accesApplication != null) {
      UtilisateurApplication utilisateurApplication = new UtilisateurApplication();
      utilisateurApplication.setCodeApplication(accesApplication
          .getCodeApplication());
      utilisateurApplication.setCodeUtilisateur(accesApplication
          .getCodeUtilisateur());
      utilisateurApplication.setCodeStatut(accesApplication.getCodeStatut());
      utilisateurApplication.setCodeTypeUtilisateur(accesApplication
          .getCodeTypeUtilisateur());
      utilisateurApplication.setCodeOrganisation(accesApplication
          .getCodeOrganisation());
      utilisateurApplication.setCreePar(accesApplication.getCreePar());
      utilisateurApplication
          .setDateCreation(accesApplication.getDateCreation());
      utilisateurApplication.setModifiePar(accesApplication.getModifiePar());
      utilisateurApplication.setDateModification(accesApplication
          .getDateModification());
      utilisateurApplication.setVersion(accesApplication.getVersion());
      getServiceUtilisateurGestion().enregistrerUtilisateurApplication(
          utilisateurApplication);
    }

    return accesApplication;
  }

  /**
   * Permet d'ajouter un accès vers une application.
   * 
   * @param accesApplication
   * @return
   */
  public void supprimerAccesApplication(String codeUtilisateur,
      String codeApplication) {
    UtilisateurApplication utilisateurApplication = new UtilisateurApplication();
    utilisateurApplication.setCodeUtilisateur(codeUtilisateur);
    utilisateurApplication.setCodeApplication(codeApplication);
    try {
      getUtilisateurApplicationDao().supprimer(utilisateurApplication);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public UtilisateurRole associerRole(UtilisateurRole utilisateurRole) {
    this.utilisateurRoleDao.ajouter(utilisateurRole);
    return utilisateurRole;
  }

  public UtilisateurRole enregistrerRole(UtilisateurRole utilisateurRole) {
    this.utilisateurRoleDao.modifier(utilisateurRole);
    return utilisateurRole;
  }

  public UtilisateurRole getUtilisateurRole(Long idUtilisateurRole) {
    UtilisateurRole utilisateurRole = (UtilisateurRole) this.utilisateurRoleDao
        .get(idUtilisateurRole);
    return utilisateurRole;
  }

  @SuppressWarnings("unchecked")
  public void dissocierRole(UtilisateurRole utilisateurRole) {
    FiltreUtilisateurRole f = new FiltreUtilisateurRole();
    f.setCodeRole(utilisateurRole.getCodeRole());
    f.setCodeUtilisateur(utilisateurRole.getCodeUtilisateur());
    f.setCodeApplication(utilisateurRole.getCodeApplication());
    f.setCodeClient(utilisateurRole.getCodeClient());

    List<UtilisateurRole> listeUtilisateurRole = utilisateurRoleDao.getListe(f);

    if (listeUtilisateurRole != null && !listeUtilisateurRole.isEmpty()) {
      for (UtilisateurRole ur : listeUtilisateurRole) {
        this.utilisateurRoleDao.supprimer(ur.getId());
      }
    }
  }

  public void dissocierRole(Long idUtilisateurRole) {
    this.utilisateurRoleDao.supprimer(idUtilisateurRole);
  }

  /**
   * Retour la listes des rôles de l'utilisateur <code>UtilisateurRole</code>.
   * 
   * @param codeUtilisateur
   * @param codeApplication
   * @return
   */
  public List<UtilisateurRole> getListeUtilisateurRole(String codeUtilisateur,
      String codeApplication) {
    List<UtilisateurRole> listeUtilisateurRole = this.utilisateurRoleDao
        .getListeUtilisateurRole(codeUtilisateur, codeApplication);
    return listeUtilisateurRole;
  }

  /**
   * Permet de modifier un utilisateur.
   */
  public Utilisateur enregistrerUtilisateur(Utilisateur utilisateur) {
    
    // Il faut toujours que la premiere lettre du prenom et du nom soit en majuscule
    utilisateur.setPrenom(UtilitaireString.convertirToutesPremieresLettreMajuscule(utilisateur.getPrenom()));
    utilisateur.setNom(UtilitaireString.convertirToutesPremieresLettreMajuscule(utilisateur.getNom()));
    
    utilisateur.setDateModification(new java.util.Date());
    this.getUtilisateurMajDao().modifier(utilisateur);
    try {
      this.sauvegarderProprietes(utilisateur);
      this.sauvegarderAccesExternes(utilisateur);
    } catch (Exception e) {
    }

    AccesApplication accesApplication = utilisateur.getAccesApplication();
    if (accesApplication != null) {
      UtilisateurApplication utilisateurApplication = new UtilisateurApplication();
      utilisateurApplication.setCodeApplication(accesApplication
          .getCodeApplication());
      utilisateurApplication.setCodeUtilisateur(utilisateur
          .getCodeUtilisateur());
      utilisateurApplication.setCodeStatut(accesApplication.getCodeStatut());
      utilisateurApplication.setCodeTypeUtilisateur(accesApplication
          .getCodeTypeUtilisateur());
      utilisateurApplication.setCodeOrganisation(accesApplication
          .getCodeOrganisation());
      utilisateurApplication.setCreePar(accesApplication.getCreePar());
      utilisateurApplication
          .setDateCreation(accesApplication.getDateCreation());
      utilisateurApplication.setModifiePar(accesApplication.getModifiePar());
      utilisateurApplication.setDateModification(accesApplication
          .getDateModification());
      utilisateurApplication.setVersion(accesApplication.getVersion());
      getServiceUtilisateurGestion().enregistrerUtilisateurApplication(
          utilisateurApplication);
      utilisateur = getUtilisateurPourId(utilisateur.getId(),
          accesApplication.getCodeApplication());
      
    } else {
      utilisateur = getUtilisateurPourId(utilisateur.getId());
    }

    return utilisateur;
  }
  
  @SuppressWarnings("unchecked")
  public List<Utilisateur> getListeUtilisateurPourApplicationEtRole(
      String codeApplication, String codeRole) {
    return this.getUtilisateurDao().getListe(codeApplication, codeRole);
  }

  public Utilisateur getUtilisateur(String identifiant) {
    Utilisateur u = this.getUtilisateurDao().getUtilisateur(identifiant);
    if (u != null) {
      this.completerProprietesUtilisateur(u);
      this.completerUrlPhotoAccesExterne(u);
    }

    return u;
  }
  
  
  @Override
  public boolean isUtilisateurSupprime(Long utilisateurId) {
    return this.getUtilisateurDao().isUtilisateurSupprime(utilisateurId);
  }

  /**
   * Met à jour la liste des acces externe pour un utilisateur.
   * 
   * @param utilisateur
   */
  private void sauvegarderAccesExternes(Utilisateur utilisateur) {
    List<AccesExterne> listeAccesExterne = utilisateur.getListeAccesExterne();
    if (listeAccesExterne != null) {
      Set<Long> ids = new HashSet<Long>();
      for (AccesExterne accesExterne : listeAccesExterne) {
        if (accesExterne.getId() != null){
          enregistrerAccesExterne(accesExterne);
        }else{
          accesExterne.setIdUtilisateur(utilisateur.getId());
          ajouterAccesExterne(accesExterne);
        }
        
        ids.add(accesExterne.getId());
      }
      accesExterneDao.supprimerAccesExterne(utilisateur.getId(), ids);
    }
  }
 
  
  @Override
  public void supprimerAccesExterneUtilisateur(Long utilisateurId) {
    accesExterneDao.supprimerAccesExterne(utilisateurId, null);
  }

  /**
   * Permet d'ajouter un accès à un utilisateur.
   * 
   * @param accesApplication
   * @return
   * @since 3.2
   */
  @Override
  public AccesExterne ajouterAccesExterne(AccesExterne accesExterne) {
    if (accesExterne != null) {
      Long id = (Long) this.accesExterneDao.ajouter(accesExterne);
      accesExterne.setId(id);
      return accesExterne;
    }
    return null;
  }

  /**
   * Permet d'enregistrer le détail d'un accès externe.
   * 
   * @param accesExterne
   * @return
   * @since 3.2
   */
  @Override
  public AccesExterne enregistrerAccesExterne(AccesExterne accesExterne) {
    this.accesExterneDao.modifier(accesExterne);
    return accesExterne;
  }

  /**
   * Permet de supprimer un accès externe d'un utilisateur.
   * 
   * @param id
   * @since 3.2
   */

  @Override
  public void supprimerAccesExterne(Long id) {
    this.accesExterneDao.supprimer(id);
  }

  public Utilisateur getUtilisateur(String identifiant, String codeApplication) {
    Utilisateur utilisateur = this.getUtilisateurDao().getUtilisateur(
        identifiant);
    if (utilisateur != null) {
      this.completerProprietesUtilisateur(utilisateur);
      this.completerUrlPhotoAccesExterne(utilisateur);
      AccesApplication accesApplication = new AccesApplication(
          utilisateur.getCodeUtilisateur(), codeApplication);
      accesApplication = (AccesApplication) getAccesApplicationDao().get(
          accesApplication);
      utilisateur.setAccesApplication(accesApplication);
    }
    return utilisateur;
  }
  
  /**Completer l'url photo en ajoutant les bons paramètres si applicable.
   * Les paramètres d'url sont définis en tant que paramètres système. 
   * 
   * @param utilisateur
   */
  private void completerUrlPhotoAccesExterne(Utilisateur utilisateur){
    if (utilisateur != null){
      for  (AccesExterne accesExterne : utilisateur.getListeAccesExterne()){
        String url = getAccesExterneImageUrl(accesExterne.getUrlPhoto(), accesExterne.getCodeFournisseurAcces());
        accesExterne.setUrlPhotoComplete(url);
      }
    }
  }
  
  /**
   * @param url l'URL original qui est persistée.
   * @param connectionProvider Le fournisseur d'accès de l'URL 
   * @return L'URL complet avec ses paramètres supplémentaires si applicable.
   */
  @Override
  public String getAccesExterneImageUrl(final String url, final String connectionProvider) { 
    String prefix = GestionParametreSysteme.getInstance().getString("prefixAccesExternePhotoURL");
    if (!StringUtils.isEmpty(prefix) && !StringUtils.isEmpty(connectionProvider) && !StringUtils.isEmpty(url)){
      String urlParams = GestionParametreSysteme.getInstance().getString(prefix.concat("_" + connectionProvider));      
      return !StringUtils.isEmpty(urlParams) ? (!url.contains("?") ? url
          .concat("?") : url.concat("&")) + urlParams : url;
    }
    return url;
  }

  private void completerProprietesUtilisateur(Utilisateur utilisateur) {
    if (utilisateur != null) {
      List<MetadonneePropriete> metadonnees = this.metadonneeProprieteDao
          .getListeMetadonneePropriete(Boolean.TRUE);
      for (MetadonneePropriete metadonneePropriete : metadonnees) {
        boolean isNotDefined = true;
        List<Propriete> props = utilisateur.getListePropriete();
        for (Iterator<Propriete> i = props.iterator(); i.hasNext() && isNotDefined;) {
          Propriete p = i.next();
          Long metaId = p.getMetadonneeProprieteId();
          if (metaId.equals(metadonneePropriete.getId())) {
            isNotDefined = false;
          }
        }

        if (isNotDefined) {
          Propriete nouvellePropriete = this.creerPropriete(metadonneePropriete);
          nouvellePropriete.setUtilisateurId(utilisateur.getId());
          utilisateur.getListePropriete().add(nouvellePropriete);
        }
      }
      // Order properties for display. Order by Category / publication and the
      // order defined in METADONNE_PROPRIETE table for each items
      Collections.sort(utilisateur.getListePropriete(),new Comparator<Propriete>(){
        @Override
        public int compare(Propriete o1, Propriete o2) {
          int c;
          c = o1.getOrdrePresentationCategorie() == null ? -1 :
            o2.getOrdrePresentationCategorie() == null ? + 1 :
            o1.getOrdrePresentationCategorie().compareTo(o2.getOrdrePresentationCategorie());
          if (c == 0){
           c = o1.getCodeClient() == null ? -1 : 
              o2.getCodeClient() == null ? +1 : 
                o1.getCodeClient().compareTo(o2.getCodeClient());
          }
          if (c == 0){
            c = o1.getOrdrePresentation().compareTo(o2.getOrdrePresentation());
          }
          return c;
        }
      });
    }
  }

  private Propriete creerPropriete(MetadonneePropriete metadonneePropriete) {
   Propriete nouvellePropriete = new Propriete();
    nouvellePropriete.setMetadonneeProprieteId(metadonneePropriete.getId());
    nouvellePropriete.setCodeClient(metadonneePropriete.getCodeClient());
    nouvellePropriete.setNom(metadonneePropriete.getNom());
    nouvellePropriete.setTypeDonnee(metadonneePropriete.getTypeDonnee());
    nouvellePropriete.setOrdrePresentation(metadonneePropriete
        .getOrdrePresentation());
    nouvellePropriete.setOrdrePresentationCategorie(metadonneePropriete.getOrdrePresentationCategorie());
    nouvellePropriete.setCodeDomaine(metadonneePropriete.getCodeDomaine());
    nouvellePropriete.setObligatoire(metadonneePropriete.getObligatoire());
    nouvellePropriete.setRefProperieteExterne(metadonneePropriete.getReferenceExterne());
    nouvellePropriete.setPrioritaire(metadonneePropriete.getPrioritaire());
    return nouvellePropriete;
  }

  /**
   * Obtenir la liste des propriété filtre possibles.
   */
  public List<FiltrePropriete> getListeFiltrePropriete() {
    List<MetadonneePropriete> listeMeta = this.metadonneeProprieteDao
        .getListeMetadonneePropriete(Boolean.TRUE);
    List<FiltrePropriete> liste = new ArrayList<FiltrePropriete>();

    for (MetadonneePropriete meta : listeMeta) {
      FiltrePropriete filtre = new FiltrePropriete();
      filtre.setNom(meta.getNom());
      filtre.setCodeClient(meta.getCodeClient());
      filtre.setTypeDonnee(meta.getTypeDonnee());

      liste.add(filtre);
    }

    return liste;
  }


  public List<Propriete> getListePropriete(String codeClient) {
    return getListePropriete(codeClient, Boolean.TRUE);
  }

  public List<Propriete> getListePropriete(String codeClient, Boolean actifSeulement) {
    List<MetadonneePropriete> listeMeta = this.metadonneeProprieteDao
        .getListeMetadonneePropriete(actifSeulement);
    List<Propriete> liste = new ArrayList<Propriete>();
    boolean demandeTousLesClients = codeClient == null;
    Client client = null;
    if (StringUtils.isNotEmpty(codeClient)){
      client = getClientDao().getClient(codeClient.toUpperCase());
    }
    
    for (MetadonneePropriete meta : listeMeta) {
      boolean isGlobale = meta.getCodeClient() == null;

      if (isGlobale
          || demandeTousLesClients
          || !demandeTousLesClients
          && (meta.getCodeClient().toUpperCase().equals(codeClient.toUpperCase()) || (client != null && meta
              .getCodeClient().toUpperCase().equals(client.getCodeClientParent().toUpperCase())))) {
        Propriete p = this.creerPropriete(meta);
        liste.add(p);
      }
    }

    return liste;
  }

  private void sauvegarderProprietes(Utilisateur utilisateur) {
    List<Propriete> props = utilisateur.getListePropriete();
    if (props != null) {
      Set<Long> propIds = new HashSet<Long>();
      for (Propriete p : props) {
        p.setUtilisateurId(utilisateur.getId());
        this.sauvegarderPropriete(p);
        propIds.add(p.getId());
      }
      // Supprimer les ids not in propIds.
      this.serviceProprieteUtilisateur.supprimerProprieteUtilisateur(
          utilisateur.getId(), propIds);
    }
  }

  private void sauvegarderPropriete(Propriete p) {
    ProprieteUtilisateur pu = new ProprieteUtilisateur();
    UtilitaireObjet.copierAttribut(pu, p);
    MetadonneePropriete mdp = this.metadonneeProprieteDao
        .getMetadonneePropriete(p.getMetadonneeProprieteId());
    pu.setMetadonnee(mdp);
    this.serviceProprieteUtilisateur.sauvegarderProprieteUtilisateur(pu);
    UtilitaireObjet.copierAttribut(p, pu);
  }

  @Override
  public void sauvegarderProprietesUtilisateur(Utilisateur utilisateur) {
    sauvegarderProprietes(utilisateur);
  }

  public Utilisateur getUtilisateurPourId(Long id) {
    
    Utilisateur u = (Utilisateur) this.getDao().get(id);
    completerProprietesUtilisateur(u);
    completerListeClient(u);
    return u;
  }

  @Override
  public Utilisateur getUtilisateurEpure(String codeUtilisateur) {
    return (Utilisateur) this.getUtilisateurMajDao().get(codeUtilisateur);
  }

  public Utilisateur getUtilisateurPourId(Long id, String codeApplication) {
    Utilisateur utilisateur = (Utilisateur) this.getUtilisateurDao().get(id);
    if (utilisateur != null) {
      this.completerProprietesUtilisateur(utilisateur);
      AccesApplication accesApplication = new AccesApplication(
          utilisateur.getCodeUtilisateur(), codeApplication);
      accesApplication = (AccesApplication) getAccesApplicationDao().get(
          accesApplication);
      utilisateur.setAccesApplication(accesApplication);
    }
    ((BaseDaoImpl)this.getUtilisateurDao()).getHibernateTemplate().evict(utilisateur);
    return utilisateur;
  }

  /**
   * Retourne la liste des applications disponible pour pilotage.
   * 
   * @param codeUtilisateur
   *          le code utilisateur.
   * @return la liste des applications disponible en pilotage.
   */
  public List<Application> getListeApplicationPourPilotage(
      String codeUtilisateur) {
    @SuppressWarnings("rawtypes")
    List listeApplicationLocal = getApplicationDao()
        .getListeApplicationAccessible(codeUtilisateur, Boolean.TRUE);
    List<Application> listeApplication = new ArrayList<Application>();
    for (int i = 0; i < listeApplicationLocal.size(); i++) {
      Application application = new Application();
      UtilitaireObjet.copierAttribut(application, listeApplicationLocal.get(i));
      listeApplication.add(application);
    }
    return listeApplication;
  }


  public UtilisateurDao getUtilisateurDao() {
    return (UtilisateurDao) getDao();
  }

  public UtilisateurMajDao getUtilisateurMajDao() {
    return utilisateurMajDao;
  }

  public void setUtilisateurMajDao(UtilisateurMajDao utilisateurMajDao) {
    this.utilisateurMajDao = utilisateurMajDao;
  }

  public UtilisateurRoleDao getUtilisateurRoleDao() {
    return utilisateurRoleDao;
  }

  public void setUtilisateurRoleDao(UtilisateurRoleDao utilisateurRoleDao) {
    this.utilisateurRoleDao = utilisateurRoleDao;
  }

  public ListeNavigation getListeUtilisateur(ListeNavigation liste) {
    return this.getUtilisateurDao().getListe(liste);
  }

  public ListeNavigation getListeUtilisateur(ListeNavigation liste,
      String codeUtilisateur) {
    return this.getUtilisateurDao().getListeSecurise(liste, codeUtilisateur);
  }

  public void forcerChangementMotPasse(String codeUtilisateur) {
    serviceUtilisateurGestion.forcerChangementMotPasse(codeUtilisateur);
  }

  public void setServiceUtilisateurGestion(
      org.sofiframework.infrasofi.modele.service.ServiceUtilisateur serviceUtilisateurGestion) {
    this.serviceUtilisateurGestion = serviceUtilisateurGestion;
  }

  public org.sofiframework.infrasofi.modele.service.ServiceUtilisateur getServiceUtilisateurGestion() {
    return serviceUtilisateurGestion;
  }

  public void changerMotPasse(ChangementMotPasse changementMotPasse) {
    getServiceUtilisateurGestion().changerMotPasse(changementMotPasse);
  }

  public void changerMotPasseExpirer(ChangementMotPasse changementMotPasse) {
    getServiceUtilisateurGestion().changerMotPasseExpirer(changementMotPasse);
  }

  public boolean validerAuthentification(String codeUtilisateur,
      String motDePasse) {
    UtilisateurDao utilisateurDao = (UtilisateurDao) this.getDao();
    Utilisateur utilisateur = utilisateurDao.getUtilisateur(codeUtilisateur);
    boolean valide = false;
    if (utilisateur != null) {
      valide = UtilitaireEncryption.isDigestsEquals(utilisateur.getMotPasse(),
          motDePasse);
    }
    return valide;
  }

  @SuppressWarnings("unchecked")
  public void supprimerUtilisateur(Long id) {
    FiltreUtilisateurApplication filtre = new FiltreUtilisateurApplication(
        getUtilisateurMajDao().supprimerUtilisateur(id).getCodeUtilisateur());

    List<AccesApplication> listeAccesApplication = getAccesApplicationDao()
        .getListe(filtre);

    if (listeAccesApplication != null && !listeAccesApplication.isEmpty()) {
      for (AccesApplication aa : listeAccesApplication) {
        this.getAccesApplicationDao().supprimer(aa);
      }
    }

    UtilisateurRole utilisateurRole = new UtilisateurRole();
    utilisateurRole.setCodeUtilisateur(filtre.getCodeUtilisateur());
    dissocierRole(utilisateurRole);
  }

  /**
   * Compléter la liste des objets roles de l'utilisateur avec la liste des
   * codes clients qui leur sont associés.
   * 
   * @param utilisateur
   *          Utilisateur
   */
  @SuppressWarnings({ "unchecked", "rawtypes" })
  protected void completerListeClient(Utilisateur utilisateur) {
    if (utilisateur != null) {
      FiltreUtilisateurRole filtre = new FiltreUtilisateurRole();
      filtre.setCodeUtilisateur(utilisateur.getCodeUtilisateur());
      List<UtilisateurRole> listeUtilisateurRole = this.getUtilisateurRoleDao()
          .getListe(filtre);
      Set<String> clients = new HashSet<String>();
      for (Iterator r = utilisateur.getListeRoles().iterator(); r.hasNext();) {
        Role role = (Role) r.next();
        for (UtilisateurRole ur : listeUtilisateurRole) {
          String codeRole = ur.getCodeRole();
          if (ur.getCodeClient() != null && role.getCode().equals(codeRole)) {
            List listeClientRole = role.getListeClient();
            if (listeClientRole == null) {
              listeClientRole = new ArrayList<String>();
              role.setListeClient(listeClientRole);
            }
            listeClientRole.add(ur.getCodeClient());
            clients.add(ur.getCodeClient());
          }
        }
      }
      if (clients != null && !clients.isEmpty()) {
        utilisateur.setListeClients(new ArrayList());
        utilisateur.getListeClients().addAll(clients);
      }
    }
  }

  public Utilisateur getUtilisateurPourCourriel(String courriel,
      String codeApplication) {

    Utilisateur u = getUtilisateurDao().getUtilisateurPourCourriel(courriel,
        codeApplication);
    completerListeClient(u);
    return u;
  }

  public Utilisateur getUtilisateurPourAccesExterne(String identifiant) {

    Utilisateur u = getUtilisateurDao().getUtilisateurPourAccesExterne(
        identifiant);
    completerListeClient(u);
    return u;
  }

  public void modifieProprieteUtilisateur(Long idUtilisateur,
      Long metadonneProprieteId, String valeur) {
    getUtilisateurDao().modifieProprieteUtilisateur(idUtilisateur, valeur,
        metadonneProprieteId);
  }

  public void ajouterProprieteUtilisateur(Long idUtilisateur,
      Long metadonneProprieteId, String valeur) {
    getUtilisateurDao().ajouterProprieteUtilisateur(idUtilisateur, valeur,
        metadonneProprieteId);
  }

  @Override
  public List<Utilisateur> getListeUtilisateurPourFiltrePropriete(FiltrePropriete filtrePropriete, Date dateDebutCreationMAJ,
      Date dateFinCreationMAJ) {
    return getUtilisateurDao().getListeUtilisateurPourFiltrePropriete(filtrePropriete, dateDebutCreationMAJ, dateFinCreationMAJ);
  }

  @Override
  public List<Utilisateur> getListeUtilisateurPourPropriete(String listeId, Integer indexFirstResult, Integer nbResult) {
    return this.getUtilisateurMajDao().getListeUtilisateurPourPropriete(listeId, indexFirstResult, nbResult);
  }

  public void setApplicationDao(ApplicationDao applicationDao) {
    this.applicationDao = applicationDao;
  }

  public ApplicationDao getApplicationDao() {
    return applicationDao;
  }

  public UtilisateurApplicationDao getUtilisateurApplicationDao() {
    return utilisateurApplicationDao;
  }

  public void setUtilisateurApplicationDao(
      UtilisateurApplicationDao utilisateurApplicationDao) {
    this.utilisateurApplicationDao = utilisateurApplicationDao;
  }

  public void setAccesApplicationDao(AccesApplicationDao accesApplicationDao) {
    this.accesApplicationDao = accesApplicationDao;
  }

  public AccesApplicationDao getAccesApplicationDao() {
    return accesApplicationDao;
  }

  public MetadonneeProprieteDao getMetadonneeProprieteDao() {
    return metadonneeProprieteDao;
  }

  public void setMetadonneeProprieteDao(
      MetadonneeProprieteDao metadonneeProprieteDao) {
    this.metadonneeProprieteDao = metadonneeProprieteDao;
  }

  public ServiceProprieteUtilisateur getServiceProprieteUtilisateur() {
    return serviceProprieteUtilisateur;
  }

  public void setServiceProprieteUtilisateur(
      ServiceProprieteUtilisateur serviceProprieteUtilisateur) {
    this.serviceProprieteUtilisateur = serviceProprieteUtilisateur;
  }
  
  public AccesExterneDao getAccesExterneDao() {
    return accesExterneDao;
  }

  public void setAccesExterneDao(AccesExterneDao accesExterneDao) {
    this.accesExterneDao = accesExterneDao;
  }

  public ClientDao getClientDao() {
    return clientDao;
  }

  public void setClientDao(ClientDao clientDao) {
    this.clientDao = clientDao;
  }

}
