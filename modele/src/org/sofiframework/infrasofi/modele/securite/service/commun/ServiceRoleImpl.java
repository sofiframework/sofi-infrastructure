package org.sofiframework.infrasofi.modele.securite.service.commun;

import java.util.ArrayList;
import java.util.List;

import org.sofiframework.application.securite.filtre.FiltreRole;
import org.sofiframework.application.securite.objetstransfert.Permission;
import org.sofiframework.application.securite.objetstransfert.Role;
import org.sofiframework.infrasofi.modele.securite.dao.commun.RoleDao;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;

public class ServiceRoleImpl extends DaoServiceImpl implements org.sofiframework.application.securite.service.ServiceRole {

  public void associerPermission(String codeRole, String codePermission) {
    // TODO Auto-generated method stub
  }

  public void dissocierPermission(String codeRole, String codePermission) {
    // TODO Auto-generated method stub
  }

  public List<Permission> getListePermission(String codeRole) {
    // TODO Auto-generated method stub
    return new ArrayList<Permission>();
  }

  public List<Role> getListeRolePilotage(String codeApplication, String codeUtilisateur, String codeStatut) {
    List<Role> listeRole = getRoleDao().getListeRolePilotage(codeApplication, codeStatut, codeUtilisateur);
    return listeRole;
  }

  public List<Role> getListeRolePilotage(FiltreRole filtre, String codeUtilisateur) {
    List<Role> listeRole = getRoleDao().getListeRolePilotage(filtre, codeUtilisateur);
    return listeRole;
  }

  private RoleDao getRoleDao() {
    return (RoleDao) getDao();
  }

}
