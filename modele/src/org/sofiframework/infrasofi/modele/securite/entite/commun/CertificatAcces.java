/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.entite.commun;

import java.util.Date;

import org.sofiframework.modele.spring.entite.Entite;

/**
 * Entité qui représente un certificat
 * d'authentification du SSO Sofi.
 * @author Jean-Maxime Pelletier
 */
public class CertificatAcces extends Entite  {

  private static final long serialVersionUID = 5889099182776389110L;

  private String certificat = null;

  private String codeUtilisateur = null;

  private Date dateConnexion = null;

  private Date dateDeconnexion = null;

  private String codeApplication = null;

  private String ip = null;

  private String userAgent = null;

  public void setCertificat(String certificat) {
    this.certificat = certificat;
  }

  public String getCertificat() {
    return certificat;
  }

  public void setCodeUtilisateur(String codeUtilisateur) {
    this.codeUtilisateur = codeUtilisateur;
  }

  public String getCodeUtilisateur() {
    return codeUtilisateur;
  }

  public void setDateConnexion(Date dateConnexion) {
    this.dateConnexion = dateConnexion;
  }

  public Date getDateConnexion() {
    return dateConnexion;
  }

  public void setDateDeconnexion(Date dateDeconnexion) {
    this.dateDeconnexion = dateDeconnexion;
  }

  public Date getDateDeconnexion() {
    return dateDeconnexion;
  }

  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }

  public String getCodeApplication() {
    return codeApplication;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getIp() {
    return ip;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public String getUserAgent() {
    return userAgent;
  }
}