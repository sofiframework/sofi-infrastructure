/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.securite.entite.commun;

import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.modele.spring.entite.Entite;


/**
 * Objet d'affaire représentant une association entre un role et un
 * objet securisable.
 * <p>
 * @author Marie-Soleil L'Allier (Nurun inc.)
 * @version 1.0
 */
public class RoleObjetSecurisable extends Entite  {

  private static final long serialVersionUID = 274584618586450970L;

  /** Attribut désignant le code du rôle de l'utilisateur */
  private String codeRole;

  /** Attribut désignant le code de l'application */
  private String codeApplication;

  /** Attribut désignant l'identifiant unique de l'objet java sécurisé */
  private Long seqObjetSecurisable;

  /** Attribut indiquant le type de consultation auquel a accès le rôle */
  private String indConsultation;

  /** Attribut désignant l'objet sécurisable de l'association */
  private ObjetSecurisable objetSecurisable;

  /** Attribut indiquant l'utilisateur a un accès en lecture seulement */
  private boolean lectureSeulement;


  /** constructeur par défaut */
  public RoleObjetSecurisable() {
  }


  public void setCodeRole(String codeRole) {
    this.codeRole = codeRole;
  }


  public String getCodeRole() {
    return codeRole;
  }


  public void setCodeApplication(String codeApplication) {
    this.codeApplication = codeApplication;
  }


  public String getCodeApplication() {
    return codeApplication;
  }


  public void setSeqObjetSecurisable(Long seqObjetSecurisable) {
    this.seqObjetSecurisable = seqObjetSecurisable;
  }


  public Long getSeqObjetSecurisable() {
    return seqObjetSecurisable;
  }

  public void setIndConsultation(String indConsultation) {
    this.indConsultation = indConsultation;
  }

  public String getIndConsultation() {
    return indConsultation;
  }


  public void setObjetSecurisable(ObjetSecurisable objetSecurisable) {
    this.objetSecurisable = objetSecurisable;
  }


  public ObjetSecurisable getObjetSecurisable() {
    return objetSecurisable;
  }

  public boolean isLectureSeulement() {
    return lectureSeulement;
  }

  public void setLectureSeulement(boolean lectureSeulement) {
    this.lectureSeulement = lectureSeulement;
  }

}