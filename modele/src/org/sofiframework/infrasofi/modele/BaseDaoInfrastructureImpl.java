package org.sofiframework.infrasofi.modele;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

public class BaseDaoInfrastructureImpl extends BaseDaoImpl {

  @SuppressWarnings("rawtypes")
  public BaseDaoInfrastructureImpl(Class classe, String nomEntite) {
    super(classe, nomEntite);
  }

  @SuppressWarnings("rawtypes")
  public BaseDaoInfrastructureImpl(Class classe)
      throws IllegalArgumentException, NullPointerException {
    super(classe);
  }

  /**
   * Un utilisateur a droit à aux entités d'une application en pilotage si il
   * possède un rôle de premier niveau (sans parent) pour cette application.
   * 
   * @param codeUtilisateur
   */
  protected void activerFiltreApplicationUtilisateurPilote(
      String codeUtilisateur) {
    this.activerFiltreApplicationUtilisateur(codeUtilisateur, Boolean.TRUE);
  }

  /**
   * Un utilisateur a doit aux entités d'une application si il possède un rôle
   * actif pour cette application.
   * 
   * Un utilisateur a droit à aux entités d'une application en pilotage si il
   * possède un rôle de premier niveau (sans parent) pour cette application.
   * 
   * @param codeUtilisateur
   * @param accesRoleParent
   */
  protected void activerFiltreApplicationUtilisateur(String codeUtilisateur,
      Boolean accesRoleParent) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("codeUtilisateur", codeUtilisateur);
    params.put("accesRoleParent", accesRoleParent ? "O" : "N");
    params.put("dateCourante", new Date());
    this.activerFiltreSession("filtreApplicationUtilisateurRole", params);
  }
}
