/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.service.commun;

import java.util.List;

import org.sofiframework.application.rapport.domaine.DomaineRapport.CodeEtatTraitementMetadonnee;
import org.sofiframework.application.rapport.filtre.FiltreRapportGabarit;
import org.sofiframework.application.rapport.filtre.FiltreRapportTraitement;
import org.sofiframework.application.rapport.objetstransfert.RapportGabarit;
import org.sofiframework.application.rapport.objetstransfert.RapportMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportResultat;
import org.sofiframework.application.rapport.objetstransfert.RapportTraitement;
import org.sofiframework.application.rapport.service.ServiceRapport;
import org.sofiframework.constante.Domaine;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportGabaritDao;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportMetadonneeDao;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportResultatDao;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportTraitementDao;
import org.sofiframework.modele.spring.service.local.BaseServiceImpl;

/**
 * Implémentation de l'interface {@link ServiceRapport} pour l'exécution des rapports.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * 
 */
public class ServiceRapportImpl extends BaseServiceImpl implements ServiceRapport {

  private RapportMetadonneeDao rapportMetadonneeDao;
  private RapportGabaritDao rapportGabaritDao;
  private RapportTraitementDao rapportTraitementDao;
  private RapportResultatDao rapportResultatDao;

  public RapportMetadonnee getRapportMetadonnee(Long id) {
    return (RapportMetadonnee) rapportMetadonneeDao.get(id);
  }

  public int supprimerRapportTraitement(String codeApplication, CodeEtatTraitementMetadonnee codeEtatTraitement) {
    return rapportTraitementDao.supprimerRapportTraitement(codeApplication, codeEtatTraitement);
  }

  @SuppressWarnings("unchecked")
  public List<RapportGabarit> getListeRapportGabaritActif(String codeApplication, String codeClient) {
    FiltreRapportGabarit filtre = new FiltreRapportGabarit();
    filtre.setCodeApplication(codeApplication);
    if (codeClient != null) {
      filtre.setCodeClient(codeClient); 
    }
    filtre.setCodeStatut(Domaine.CodeStatut.ACTIF.getValeur());
    return rapportGabaritDao.getListe(filtre);
  }

  public void enregistrerListeRapportTraitement(List<RapportTraitement> listeRapportTraitement) {
    if (listeRapportTraitement != null) {
      for (RapportTraitement rapportTraitement : listeRapportTraitement) {
        if (rapportTraitement.getId() == null) {
          rapportTraitementDao.ajouter(rapportTraitement);
        } else {
          rapportTraitementDao.modifier(rapportTraitement);
        }
      }
    }
  }

  @SuppressWarnings("unchecked")
  public List<RapportTraitement> getListeRapportTraitement(String codeApplication,
      CodeEtatTraitementMetadonnee codeEtatTraitement) {
    FiltreRapportTraitement filtre = new FiltreRapportTraitement();
    filtre.setCodeApplication(codeApplication);
    filtre.setCodeEtatTraitement(codeEtatTraitement.getValeur());
    return rapportTraitementDao.getListe(filtre);
  }

  public void enregistrerListeRapportResultat(List<RapportResultat> listeRapportResultat) {
    if (listeRapportResultat != null) {
      for (RapportResultat rapportResultat : listeRapportResultat) {
        if (rapportResultat.getId() == null) {
          rapportResultatDao.ajouter(rapportResultat);
        } else {
          rapportResultatDao.modifier(rapportResultat);
        }
      }
    }
  }

  public RapportMetadonneeDao getRapportMetadonneeDao() {
    return rapportMetadonneeDao;
  }

  public void setRapportMetadonneeDao(RapportMetadonneeDao rapportMetadonneeDao) {
    this.rapportMetadonneeDao = rapportMetadonneeDao;
  }

  public RapportGabaritDao getRapportGabaritDao() {
    return rapportGabaritDao;
  }

  public void setRapportGabaritDao(RapportGabaritDao rapportGabaritDao) {
    this.rapportGabaritDao = rapportGabaritDao;
  }

  public RapportTraitementDao getRapportTraitementDao() {
    return rapportTraitementDao;
  }

  public void setRapportTraitementDao(RapportTraitementDao rapportTraitementDao) {
    this.rapportTraitementDao = rapportTraitementDao;
  }

  public RapportResultatDao getRapportResultatDao() {
    return rapportResultatDao;
  }

  public void setRapportResultatDao(RapportResultatDao rapportResultatDao) {
    this.rapportResultatDao = rapportResultatDao;
  }

}
