/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.service.commun;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.sofiframework.application.rapport.objetstransfert.RapportMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportResultatAgrege;
import org.sofiframework.application.rapport.service.ServiceRapportResultat;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportGabaritDao;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportResultatDao;
import org.sofiframework.modele.spring.service.local.BaseServiceImpl;
import org.sofiframework.utilitaire.Periode;

/**
 * Implémentation du service d'accès aux résultats de rapport.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 * 
 */
public class ServiceRapportResultatImpl extends BaseServiceImpl implements ServiceRapportResultat {

  private RapportGabaritDao rapportGabaritDao;
  private RapportResultatDao rapportResultatDao;

  public List<RapportMetadonnee> getListeRapportMetadonneeOrdonnee(String codeApplication, String codeClient,
      String codeGabarit) {
    return rapportGabaritDao.getListeRapportMetadonneeOrdonnee(codeApplication, codeClient, codeGabarit);
  }

  public Map<Long, List<RapportResultatAgrege>> getListeRapportResultatAgrege(String codeApplication,
      List<String> codeClient, Periode periode) {
    return rapportResultatDao.getListeRapportResultatAgrege(codeApplication, codeClient, periode);
  }

  public Date getDateDernierResultat(String codeApplication, List<String> codeClient) {
    return rapportResultatDao.getDateDernierResultat(codeApplication, codeClient);
  }

  public RapportResultatDao getRapportResultatDao() {
    return rapportResultatDao;
  }

  public void setRapportResultatDao(RapportResultatDao rapportResultatDao) {
    this.rapportResultatDao = rapportResultatDao;
  }

  public RapportGabaritDao getRapportGabaritDao() {
    return rapportGabaritDao;
  }

  public void setRapportGabaritDao(RapportGabaritDao rapportGabaritDao) {
    this.rapportGabaritDao = rapportGabaritDao;
  }

}
