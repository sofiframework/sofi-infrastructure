/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.sofiframework.application.rapport.domaine.DomaineRapport.CodeTypeMetadonnee;
import org.sofiframework.application.rapport.execution.TacheRapport;
import org.sofiframework.application.rapport.objetstransfert.RapportResultatAgrege;
import org.sofiframework.modele.spring.dao.BaseDao;
import org.sofiframework.utilitaire.Periode;

/**
 * Interface d'accès de données pour l'entité {@link RapportResultatDao}.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public interface RapportResultatDao extends BaseDao {

  /**
   * Récupère la liste de {@link RapportResultatAgrege} correspondante aux paramètres spécifiés.
   * <p>
   * Dépendamment du type de métadonnée (voir {@link CodeTypeMetadonnee}), le résultat correspondra à une somme ou à une
   * moyenne.
   * 
   * @param codeApplication
   * @param listeCodeClient
   * @param periode
   * @return
   */
  Map<Long, List<RapportResultatAgrege>> getListeRapportResultatAgrege(String codeApplication,
      List<String> listeCodeClient, Periode periode);

  /**
   * Retourne la date des derniers résultats calculés par la tâche planifiée {@link TacheRapport} pour les paramètres
   * passés.
   * 
   * @param codeApplication
   *          le code de l'application appelante.
   * @param codeClient
   *          la liste des codes client dont on veut la date des derniers résultats.
   * 
   * @return une {@link Date}.
   */
  Date getDateDernierResultat(String codeApplication, List<String> codeClient);
}
