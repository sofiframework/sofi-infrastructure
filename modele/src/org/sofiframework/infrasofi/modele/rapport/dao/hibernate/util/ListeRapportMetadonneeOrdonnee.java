/*
 * Copyright 2003-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.dao.hibernate.util;

import java.util.Comparator;
import java.util.TreeSet;

import org.sofiframework.application.rapport.objetstransfert.RapportMetadonnee;

/**
 * Représente une liste de {@link RapportMetadonnee} ordonnée suivant la propriété
 * {@link RapportMetadonnee#getOrdreAffichage()}.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 * 
 */

public class ListeRapportMetadonneeOrdonnee extends TreeSet<RapportMetadonnee> {
  /**
   * 
   */
  private static final long serialVersionUID = 9148244944479345329L;

  /**
   * Appelle le constructeur parent {@link TreeSet#TreeSet(Comparator)}.
   */
  public ListeRapportMetadonneeOrdonnee() {
    super(new Comparator<RapportMetadonnee>() {
      public int compare(RapportMetadonnee o1, RapportMetadonnee o2) {
        Integer i1 = o1.getOrdreAffichage();
        Integer i2 = o2.getOrdreAffichage();
        if (i1 == null && i2 == null) {
          return 0;
        } else if (i2 == null) {
          return 1;
        } else if (i1 == null) {
          return -1;
        }

        return i1.compareTo(i2);
      }
    });
  }
}