/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.dao;

import org.sofiframework.application.rapport.domaine.DomaineRapport.CodeEtatTraitementMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportTraitement;
import org.sofiframework.modele.spring.dao.BaseDao;

/**
 * Interface d'accès de données pour l'entité {@link RapportTraitement}.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public interface RapportTraitementDao extends BaseDao {

  /**
   * Suppime les entités {@link RapportTraitement} correspondantes aux paramètres.
   * 
   * @param codeApplication
   * @param codeEtatTraitement
   * @return le nombre d'entités supprimées.
   */
  int supprimerRapportTraitement(String codeApplication, CodeEtatTraitementMetadonnee codeEtatTraitement);
}
