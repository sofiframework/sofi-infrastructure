/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.dao.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.sofiframework.application.rapport.filtre.FiltreRapportGabarit;
import org.sofiframework.application.rapport.objetstransfert.RapportGabarit;
import org.sofiframework.application.rapport.objetstransfert.RapportMetadonnee;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportGabaritDao;
import org.sofiframework.infrasofi.modele.rapport.dao.hibernate.util.ListeRapportMetadonneeOrdonnee;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

/**
 * Implémentation hibernate de l'accès de données pour l'entité {@link RapportGabarit}.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class RapportGabaritDaoImpl extends BaseDaoImpl implements RapportGabaritDao {

  /** constructeur par défaut */
  public RapportGabaritDaoImpl() {
    super(RapportGabarit.class);
  }

  @SuppressWarnings("unchecked")
  public List<RapportMetadonnee> getListeRapportMetadonneeOrdonnee(String codeApplication, String codeClient,
      String codeGabarit) {

    List<RapportMetadonnee> listeComplete = new ArrayList<RapportMetadonnee>();

    FiltreRapportGabarit filtre = new FiltreRapportGabarit();
    filtre.setCodeApplication(codeApplication);
    filtre.setCodeClient(codeClient);
    filtre.setCodeGabarit(codeGabarit);
    List<RapportGabarit> liste = getListe(filtre);

    if (!liste.isEmpty()) {
      RapportGabarit gabarit = liste.get(0);

      // On construit une table dont les clés seront les parents et les valeurs leurs enfants.
      MultiMap tableParentEnfant = MultiValueMap.decorate(new HashMap<Long, ListeRapportMetadonneeOrdonnee>(),
          ListeRapportMetadonneeOrdonnee.class);
      for (RapportMetadonnee metadonnee : gabarit.getListeRapportMetadonnee()) {
        Long parentId = metadonnee.getRapportMetadonneeParent() != null ? metadonnee.getRapportMetadonneeParent()
            .getId() : null;
            tableParentEnfant.put(parentId, metadonnee);
      }

      // Appel récursif
      construireListeOrdonnee(listeComplete, null, 0, tableParentEnfant);
    }

    return listeComplete;
  }

  /**
   * Méthode récursive parcourant la table de parent-enfant pour construire une liste plate et ordonnée de
   * {@link RapportMetadonnee}.
   * 
   * @param liste
   * @param parentId
   * @param tableParentEnfant
   */
  private void construireListeOrdonnee(List<RapportMetadonnee> liste, Long parentId, Integer niveau,
      MultiMap tableParentEnfant) {
    ListeRapportMetadonneeOrdonnee listeEnfant = (ListeRapportMetadonneeOrdonnee) tableParentEnfant.get(parentId);
    if (listeEnfant != null) {
      for (RapportMetadonnee metadonnee : listeEnfant) {
        metadonnee.setNiveau(niveau);
        liste.add(metadonnee);
        construireListeOrdonnee(liste, metadonnee.getId(), niveau + 1, tableParentEnfant);
      }
    }
  }

}