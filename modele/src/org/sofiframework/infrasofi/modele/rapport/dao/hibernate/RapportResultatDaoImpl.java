/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.dao.hibernate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Query;
import org.sofiframework.application.rapport.domaine.DomaineRapport.CodeModeResultat;
import org.sofiframework.application.rapport.objetstransfert.RapportResultat;
import org.sofiframework.application.rapport.objetstransfert.RapportResultatAgrege;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportResultatDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.sofiframework.utilitaire.Periode;

/**
 * Implémentation hibernate de l'accès de données pour l'entité {@link RapportResultat}.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
@SuppressWarnings("unchecked")
public class RapportResultatDaoImpl extends BaseDaoImpl implements RapportResultatDao {

  /**
   * Dictionnaire des requêtes nommées à appeler pour récupérer les résultats des rapports.
   */
  private static Map<CodeModeResultat, String> map = new HashMap<CodeModeResultat, String>();
  static {
    map.put(CodeModeResultat.SOMME, "selectRapportResultatSomme");
    map.put(CodeModeResultat.MOYENNE, "selectRapportResultatMoyenne");
    map.put(CodeModeResultat.SIMPLE, "selectRapportResultatSimple");
  }

  /** constructeur par défaut */
  public RapportResultatDaoImpl() {
    super(RapportResultat.class);
  }

  public Map<Long, List<RapportResultatAgrege>> getListeRapportResultatAgrege(String codeApplication,
      List<String> listeCodeClient, Periode periode) {

    List<RapportResultatAgrege> liste = new ArrayList<RapportResultatAgrege>();

    for (CodeModeResultat codeModeResultat : map.keySet()) {
      String namedQuery = map.get(codeModeResultat);

      Query query = getSession().getNamedQuery(namedQuery);
      query.setParameter("codeApplication", codeApplication);
      query.setParameterList("codeClient", listeCodeClient);
      // FIXME RM dynamiser le nombre de période.
      query.setParameter("nombrePeriode", 1);
      query.setParameter("dateDebutResultat", getDateDebut(periode, codeModeResultat));
      query.setParameter("dateFinResultat", getDateFin(periode, codeModeResultat));
      query.setParameter("codeModeResultat", codeModeResultat.getValeur());

      liste.addAll(query.list());
    }

    MultiMap result = new MultiValueMap();
    for (RapportResultatAgrege rapportResultatAgrege : liste) {
      result.put(rapportResultatAgrege.getRapportMetadonneeId(), rapportResultatAgrege);
    }

    return transformeMultiMapEnMap(result);
  }

  public Date getDateDernierResultat(String codeApplication, List<String> listeCodeClient) {
    Query query = getSession().getNamedQuery("selectDateDernierResultat");
    query.setParameter("codeApplication", codeApplication);
    query.setParameterList("codeClient", listeCodeClient);

    List<Date> liste = query.list();

    return liste.isEmpty() ? null : liste.get(0);
  }

  /**
   * Parcours l'objet {@link MultiMap} en paramètre et crée l'objet {@link Map} correspondant.
   * <p>
   * Le besoin de cette méthode est d'obtenir une map sérialisable ; dans la version 3.2 des commons collections de
   * apache, l'objet {@link MultiMap} ne l'est pas. Il le devient en version 4.0. Une fois upgradé à cette version,
   * cette méthode deviendra obsolète.
   * 
   * @param multiMap
   *          un objet {@link MultiMap}
   * @return une {@link Map}.
   */
  @SuppressWarnings({ "rawtypes" })
  private Map transformeMultiMapEnMap(MultiMap multiMap) {
    Map map = new HashMap();

    if (multiMap != null) {
      for (Object cle : multiMap.keySet()) {
        map.put(cle, multiMap.get(cle));
      }
    }

    return map;
  }

  /**
   * Dans le cas ou <code>codeModeResultat</code> vaut {@link CodeModeResultat#SIMPLE}, retourne la date de fin de la
   * période à minuit ; sinon retourne la date de début de la période.
   * 
   * @param periode
   * @param codeModeResultat
   * @return une Date
   */
  private Date getDateDebut(Periode periode, CodeModeResultat codeModeResultat) {
    if (codeModeResultat.equals(CodeModeResultat.SIMPLE)) {
      return DateUtils.truncate(periode.getDateFin(), Calendar.DATE);
    }
    return periode.getDateDebut();
  }

  /**
   * Retourne la date de fin de la période.
   * 
   * @param periode
   * @param codeModeResultat
   * @return une Date
   */
  private Date getDateFin(Periode periode, CodeModeResultat codeModeResultat) {
    return periode.getDateFin();
  }
}
