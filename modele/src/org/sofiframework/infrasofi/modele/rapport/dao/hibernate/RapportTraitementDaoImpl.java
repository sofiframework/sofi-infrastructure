/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.dao.hibernate;

import org.hibernate.Query;
import org.sofiframework.application.rapport.domaine.DomaineRapport.CodeEtatTraitementMetadonnee;
import org.sofiframework.application.rapport.objetstransfert.RapportTraitement;
import org.sofiframework.infrasofi.modele.rapport.dao.RapportTraitementDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

/**
 * Implémentation hibernate de l'accès de données pour l'entité {@link RapportTraitement}.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public class RapportTraitementDaoImpl extends BaseDaoImpl implements RapportTraitementDao {

  /** constructeur par défaut */
  public RapportTraitementDaoImpl() {
    super(RapportTraitement.class);
  }

  public int supprimerRapportTraitement(String codeApplication, CodeEtatTraitementMetadonnee codeEtatTraitement) {
    Query requete = getSession()
        .createQuery(
            "delete from RapportTraitement where codeApplication = :codeApplication and codeEtatTraitement = :codeEtatTraitement");
    requete.setParameter("codeApplication", codeApplication);
    requete.setParameter("codeEtatTraitement", codeEtatTraitement.getValeur());
    return requete.executeUpdate();
  }

}
