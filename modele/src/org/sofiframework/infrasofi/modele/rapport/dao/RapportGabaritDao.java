/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.rapport.dao;

import java.util.List;

import org.sofiframework.application.rapport.objetstransfert.RapportGabarit;
import org.sofiframework.application.rapport.objetstransfert.RapportMetadonnee;
import org.sofiframework.modele.spring.dao.BaseDao;

/**
 * Interface d'accès de données pour l'entité {@link RapportGabarit}.
 * 
 * @author Rémi Mercier (Nurun Inc.)
 * @since 3.2
 */
public interface RapportGabaritDao extends BaseDao {

  /**
   * Retourne la liste des métadonnées de rapport ordonnées suivant leur hiérarchie parent-enfant et suivant la
   * propriété ordre affichage.
   * <p>
   * Initialise la propriété <code>niveau</code> de chaque entité retournée.
   * <p>
   * Exemple : <br>
   * Nom | Parent | Ordre affichage<br>
   * ------------------------------<br>
   * s1 | null | 1<br>
   * s2 | null | 2<br>
   * s3 | null | 3<br>
   * s31 | s3 | 1<br>
   * s11 | s1 | 1<br>
   * s12 | s1 | 2<br>
   * Dans ce cas, la liste résultat sera : s1, s2, s3, s11, s12, s3.
   * 
   * @param codeApplication
   *          le code de l'application.
   * @param codeClient
   *          le code du client.
   * @param codeGabarit
   *          le code du gabarit.
   * @return une liste de {@link RapportMetadonnee} ; chaque entité de la liste a sa propriété <code>niveau</code>
   *         initialisée.
   */
  List<RapportMetadonnee> getListeRapportMetadonneeOrdonnee(String codeApplication, String codeClient,
      String codeGabarit);
}
