/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.parametresysteme.dao;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.ParametreSysteme;

/**
 * Accès de données de l'entité ParametreSysteme.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface ParametreSystemeDao {

  /**
   * Obtenir la liste des paramètres systèmes en fonction d'un nombre de jours
   * depuis leur date de création ou de modification.
   * 
   * @param ln
   * @param nombreJours
   * @param codeApplication
   * @return
   */
  ListeNavigation getListe(ListeNavigation ln, Integer nombreJours,
      String codeApplication, String codeUtilisateur);

  /**
   * Obtenir la liste des paramètres qui doivent être accessibles pour un
   * utilisateur. Si accesRole parent est a true, on doit fournir les paramètres
   * qui sont reliés aux application dont l'utilisateur possède un rôle de
   * premier niveau.
   * 
   * @param liste
   * @param codeUtilisateur
   * @param accesRoleParent
   * @return
   */
  ListeNavigation getListeParametrePilotage(ListeNavigation liste,
      String codeUtilisateur, Boolean accesRoleParent);

  /**
   * Obtenir un paramètre système avec un code d'application et un code de
   * paramètre. Vérifie les codes applications communs afin de donner les
   * valeurs de paramètres des applications communes.
   * 
   * @param code
   * @param codeApplication
   * @return
   */
  ParametreSysteme getParametreSysteme(String code, String codeApplication);

  public ParametreSysteme getParametreSysteme(String code,
      String codeApplication, String codeClient);

  /**
   * Obtenir un paramètre système avec son identifiant.
   * 
   * @param code
   * @param codeApplication
   * @return
   */
  ParametreSysteme getParametreSysteme(Long id);
}