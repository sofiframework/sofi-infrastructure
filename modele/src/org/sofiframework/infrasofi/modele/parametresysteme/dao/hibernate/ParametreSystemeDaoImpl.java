/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.parametresysteme.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseDaoInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.ParametreSysteme;
import org.sofiframework.infrasofi.modele.parametresysteme.dao.ParametreSystemeDao;
import org.sofiframework.utilitaire.UtilitaireString;
import org.springframework.util.StringUtils;

/**
 * Accès de données pour l'objet d'affaire paramètre système.
 * <p>
 * 
 * @author Marie-Soleil L'Allier
 * @author Jean-Maxime Pelletier
 */
public class ParametreSystemeDaoImpl extends BaseDaoInfrastructureImpl
implements ParametreSystemeDao {

  /** constructeur */
  public ParametreSystemeDaoImpl() {
    super(ParametreSysteme.class);
  }

  public ParametreSysteme getParametreSysteme(Long id) {
    return (ParametreSysteme) this.get(id);
  }

  public ListeNavigation getListe(ListeNavigation ln, Integer nombreJours,
      String codeApplication, String codeUtilisateur) {
    Date date = new Date(System.currentTimeMillis()
        - nombreJours * 24 * 60 * 60 * 1000);
    this.activerFiltreApplicationUtilisateurPilote(codeUtilisateur);
    if (!UtilitaireString.isVide(codeApplication)) {
      ln = this.getListe("FROM ParametreSysteme p " + "WHERE p.codeApplication = ? " + "AND (p.dateModification >= ? "
          + "OR p.dateCreation >= ?) ", new Object[] { codeApplication, date, date }, ln);
    }else {
      ln = this.getListe("FROM ParametreSysteme p WHERE p.dateModification >= ? "
          + "OR p.dateCreation >= ?) ", new Object[] {date, date }, ln);
    }
    return ln;
  }

  public ListeNavigation getListeParametrePilotage(ListeNavigation liste,
      String codeUtilisateur, Boolean accesRoleParent) {
    activerFiltreApplicationUtilisateur(codeUtilisateur, accesRoleParent);
    return this.getListe(liste);
  }

  public ParametreSysteme getParametreSysteme(String code,
      String codeApplication) {
    return getParametreSysteme(code, codeApplication, null);
  }

  public ParametreSysteme getParametreSysteme(String code,
      String codeApplication, String codeClient) {
    ParametreSysteme parametre = null;

    Object[] parametres = new Object[] {code, codeApplication };
    String hql = "from ParametreSysteme s "
        + "where s.codeParametre = ?  "
        + " and (s.codeClient is null)"
        + " and s.codeApplication in (?)";
    
    if (codeClient != null) {
      hql = "from ParametreSysteme s "
          + "where s.codeParametre = ?  "
          + " and (s.codeClient = ?)"
          + " and s.codeApplication in (?)";   
      parametres = new Object[] {code, codeClient, codeApplication };
    }


    String[] listeCodeApplication = new String[] { codeApplication };

    List<ParametreSysteme> listeParametre = getListe(
        hql,
        parametres);

    if (listeParametre == null || listeParametre.size() == 0) {
      String[] listeCodeApplicationCommun = this
          .getListeCodeApplicationCommun(codeApplication);
      if (listeCodeApplicationCommun != null) {
        
        Query q = null;
        
        String hql2 = "from ParametreSysteme s "
            + "where s.codeParametre = ?  "
            + " and (s.codeClient is null)"
            + " and s.codeApplication in (:listeCodeApplication)";   
        
        if (!UtilitaireString.isVide(codeClient)) {
          hql2 = "from ParametreSysteme s "
              + "where s.codeParametre = ?  "
              + " and (s.codeClient = ?)"
              + " and s.codeApplication in (:listeCodeApplication)";
          
          q = this.getSession().createQuery(hql2).setParameter(0, code).setParameter(1, codeClient).setParameterList("listeCodeApplication", listeCodeApplicationCommun);
        }else {
          q = this.getSession().createQuery(hql2).setParameter(0, code).setParameterList("listeCodeApplication", listeCodeApplicationCommun);
        }

        if (listeCodeApplicationCommun != null) {
          listeParametre = q.list();
        }

      }

    }

    if (listeParametre != null && listeParametre.size() > 0) {
      parametre = listeParametre.get(0);
    }

    return parametre;
  }

  public String[] getListeCodeApplicationCommun(String codeApplication) {
    String codeApplicationCommun = null;

    @SuppressWarnings("unchecked")
    List<ParametreSysteme> listeParam = getListe(
        "FROM ParametreSysteme p where p.codeApplication = ? and p.codeParametre = ?",
        new Object[] { codeApplication, "codeApplicationCommun" });
    if (listeParam != null && listeParam.size() == 1) {
      ParametreSysteme paramCodeAppCommun = listeParam.get(0);
      codeApplicationCommun = paramCodeAppCommun.getValeur();

    }

    String[] listeCodeApplication = StringUtils.tokenizeToStringArray(
        codeApplicationCommun, ",");

    return listeCodeApplication;
  }
}