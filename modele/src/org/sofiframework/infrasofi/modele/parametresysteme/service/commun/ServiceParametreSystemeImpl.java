/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.parametresysteme.service.commun;

import org.sofiframework.infrasofi.modele.entite.ParametreSysteme;
import org.sofiframework.infrasofi.modele.service.ServiceParametreSysteme;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.service.local.BaseServiceImpl;

/**
 * Service commun des paramètres systèmes.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceParametreSystemeImpl
extends BaseServiceImpl
implements org.sofiframework.application.parametresysteme.service.ServiceParametreSysteme {

  private ServiceParametreSysteme serviceParametreSysteme;

  /**
   * Méthode retournant la valeur du paramètre associé à au code de l'application
   * et à la clé reçue.
   * @return la valeur du paramètre
   * @param cle la clé du paramètre
   * @param codeApplication le code de l'application
   */
  public String getValeur(String codeApplication, String codeParametreSysteme) {
    return this.serviceParametreSysteme.getValeur(codeApplication, codeParametreSysteme);
  }

  /**
   * Méthode retournant la valeur du paramètre associé à au code de l'application
   * et à la clé reçue.
   * @return la valeur du paramètre
   * @param cle la clé du paramètre
   * @param codeApplication le code de l'application
   */
  public String getValeur(String codeApplication, String codeParametreSysteme, String codeClient) {
    return this.serviceParametreSysteme.getValeur(codeApplication, codeParametreSysteme, codeClient);
  }

  public ServiceParametreSysteme getServiceParametreSysteme() {
    return serviceParametreSysteme;
  }

  public void setServiceParametreSysteme(ServiceParametreSysteme serviceParametreSysteme) {
    this.serviceParametreSysteme = serviceParametreSysteme;
  }

  public void enregistrerValeur(String codeApplication, String code, String valeur) {
    ParametreSysteme parametre = this.serviceParametreSysteme.get(codeApplication, code);

    if (parametre == null) {
      throw new ModeleException("Le paramètre système " + code + " n'existe pas.");
    } else {
      if (parametre.getModificationADistance()) {
        parametre.setValeur(valeur);
        parametre.setModifie(true);
        this.serviceParametreSysteme.enregistrer(parametre, null);
      } else {
        throw new ModeleException("Le paramètre système " + code + " n'est pas modifiable à distance.");
      }
    }

  }

  public void enregistrerValeur(String codeApplication, String code, String valeur, String codeClient) {
    ParametreSysteme parametre = this.serviceParametreSysteme.get(codeApplication, code, codeClient);

    if (parametre == null) {
      throw new ModeleException("Le paramètre système " + code + " n'existe pas.");
    } else {
      if (parametre.getModificationADistance()) {
        parametre.setValeur(valeur);
        parametre.setModifie(true);
        this.serviceParametreSysteme.enregistrer(parametre, null);
      } else {
        throw new ModeleException("Le paramètre système " + code + " n'est pas modifiable à distance.");
      }
    }

  }
}
