/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.parametresysteme.service.local;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.ParametreSysteme;
import org.sofiframework.infrasofi.modele.parametresysteme.dao.ParametreSystemeDao;
import org.sofiframework.infrasofi.modele.service.ServiceParametreSysteme;

/**
 * Service de gestion des paramètres système.
 * <p>
 * 
 * @author Jean-Maxime Pelletier
 * @author Marie-Soleil L'Allier
 */
public class ServiceParametreSystemeImpl extends BaseServiceInfrastructureImpl
implements ServiceParametreSysteme {

  public String getValeur(String codeApplication, String codeParametreSysteme) {
    ParametreSysteme parametre = get(codeApplication, codeParametreSysteme);
    return parametre != null ? parametre.getValeur() : null;
  }

  public String getValeur(String codeApplication, String codeParametreSysteme, String codeClient) {
    ParametreSysteme parametre = get(codeApplication, codeParametreSysteme, codeClient);
    return parametre != null ? parametre.getValeur() : null;
  }

  public ParametreSysteme get(String codeApplication,
      String codeParametreSysteme) {

    return get(codeApplication, codeParametreSysteme, null);
  }

  public ParametreSysteme get(String codeApplication,
      String codeParametreSysteme, String codeClient) {
    ParametreSysteme parametre = this.getParametreSystemeDao()
        .getParametreSysteme(codeParametreSysteme, codeApplication, codeClient);
    return parametre;
  }

  public ParametreSysteme get(Long id) {
    ParametreSysteme parametre = this.getParametreSystemeDao()
        .getParametreSysteme(id);
    return parametre;
  }

  public void enregistrer(ParametreSysteme parametre,
      ParametreSysteme parametreOriginal) {
    if (parametre.getId() == null) {
      this.ajouter(parametre);
    } else {
      if (parametreOriginal != null) {
        this.supprimer(parametreOriginal);
        this.traceModification(parametre);
        this.getDao().ajouter(parametre);
      } else {
        this.modifier(parametre);
      }
    }
  }


  public void supprimer(Long id) {
    super.supprimer(id);
  }

  public ListeNavigation getListeParametresPourTableauBord(
      ListeNavigation liste, Integer nombreJours, String codeApplication) {
    String codeUtilisateur = this.getGestionnaireUtilisateur().getUtilisateur()
        .getCodeUtilisateur();
    this.getParametreSystemeDao().getListe(liste, nombreJours, codeApplication,
        codeUtilisateur);
    return liste;
  }

  public ListeNavigation getListeParametrePilotage(ListeNavigation liste) {
    String codeUtilisateur = this.getGestionnaireUtilisateur().getUtilisateur()
        .getCodeUtilisateur();
    this.getParametreSystemeDao().getListeParametrePilotage(liste,
        codeUtilisateur, Boolean.TRUE);
    return liste;
  }

  private ParametreSystemeDao getParametreSystemeDao() {
    return (ParametreSystemeDao) getDao();
  }

}