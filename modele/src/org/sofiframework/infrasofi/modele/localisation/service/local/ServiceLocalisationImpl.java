/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.localisation.service.local;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.Charsets;
import org.apache.commons.lang3.StringUtils;
import org.sofiframework.application.locale.objetstransfert.FuseauHoraire;
import org.sofiframework.application.locale.objetstransfert.Pays;
import org.sofiframework.application.locale.objetstransfert.Region;
import org.sofiframework.application.locale.objetstransfert.Ville;
import org.sofiframework.application.locale.objetstransfert.VilleRegion;
import org.sofiframework.application.locale.service.ServiceLocalisation;
import org.sofiframework.infrasofi.modele.localisation.dao.PaysDao;
import org.sofiframework.infrasofi.modele.localisation.dao.RegionDao;
import org.sofiframework.infrasofi.modele.localisation.dao.VilleDao;
import org.sofiframework.infrasofi.modele.service.ServiceLibelle;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.service.local.BaseServiceImpl;
import org.sofiframework.utilitaire.UtilitaireString;

public class ServiceLocalisationImpl extends BaseServiceImpl implements ServiceLocalisation {

  private PaysDao paysDao;

  private RegionDao regionDao;
  
  private VilleDao villeDao; 

  private ServiceLibelle serviceLibelle;

  /**
   * Retourne la liste
   * 
   * @return
   */
  public List<FuseauHoraire> getListeFuseauHoraire() {
    return this.getListeFuseauHoraire(null, Locale.getDefault());
  }

  /**
   * 
   * @param pays
   * @param locale
   * @return
   */
  public List<FuseauHoraire> getListeFuseauHoraire(String codePays, Locale locale) {
    String[] listeID = TimeZone.getAvailableIDs();
    List<FuseauHoraire> listeFuseau = new ArrayList<FuseauHoraire>();

    /*
     * On doit trier toutes les timezone en ordre de décalage par rapport au
     * GMT.
     */
    TreeSet<Integer> trier = new TreeSet<Integer>();

    for (int i = 0; i < listeID.length; i++) {
      TimeZone tz = TimeZone.getTimeZone(listeID[i]);
      int offset = tz.getRawOffset();
      trier.add(new Integer(offset));
    }

    /*
     * On récupère tous les fuseaux qui correspondent au même offset.
     */
    for (Iterator<Integer> i = trier.iterator(); i.hasNext();) {
      Integer offset = i.next();

      /*
       * On doit présenter chaque zone même si elles ont le même décalage. Les
       * zones peuvent aussi varier en fonction de leur heure d'été.
       */
      String[] listeParTZ = TimeZone.getAvailableIDs(offset.intValue());

      for (int j = 0; j < listeParTZ.length; j++) {
        String id = listeParTZ[j];
        if (id.indexOf("/") != -1) {
          String paysEnCours = id.substring(0, id.indexOf("/"));
          if (codePays == null || codePays.equals("") || codePays.indexOf(paysEnCours) != -1) {
            TimeZone tz = TimeZone.getTimeZone(id);
            listeFuseau.add(new FuseauHoraire(id, tz.getDisplayName(locale)));
          }
        }
      }
    }

    return listeFuseau;
  }

  public void chargerPaysEtRegions() {
    // pays
    String urlFr = "http://www.iso.org/iso/list-fr1-semic.txt";
    String urlEn = "http://www.iso.org/iso/list-en1-semic-3.txt";

    this.miseAJourListePays(urlFr, "fr_CA");
    this.miseAJourListePays(urlEn, "en_US");

    // subdivision
    String urlRegion = "http://www.statoids.com/u{0}.html";

    List<Pays> listePays = this.getListePays();
    for (Pays pays : listePays) {
      String url = MessageFormat.format(urlRegion, pays.getCode().toLowerCase());
      miseAJourListeRegion(url, pays.getCode(), "en_US");
    }
  }

  private void miseAJourListeRegion(String url, String codePays, String codeLocale) {
    String page = getHttp(url);
    System.out.println(url);
    System.out.println(page);
    int indexdebutTable = page.indexOf("<table class=\"st\">");
    int indexFinTable = page.indexOf("</table>", indexdebutTable);

    String table = page.substring(indexdebutTable, indexFinTable);

    System.out.println(table);

    String tagLigneFin = "</tr>";
    String tagCelluleFin = "</td>";

    String[] lignes = table.split(tagLigneFin);

    for (int i = 0; i < lignes.length; i++) {
      String ligne = lignes[i];
      // Si ce n'est pas la ligne d'entête
      if (ligne.indexOf(" class=\"hd\"") == -1 && ligne.indexOf(" class=\"ft\"") == -1 && ligne.indexOf(" class=\"cp\"") == -1) {
        String[] cellules = ligne.split(tagCelluleFin);
        String nom = cellules[0];
        int indexTd = nom.indexOf("<td");

        if (indexTd != -1) {
          int indexFinTd = nom.indexOf(">", indexTd);
          nom = nom.substring(indexFinTd + 1);
          String code = cellules[1];
          System.out.println(code);
          int indexDebutCode = code.indexOf("<code>");
          int indexFinCode = code.indexOf("</code>");
          if (indexDebutCode != -1) {
            if (indexFinCode == -1) {
              indexFinCode = code.length() - 1;
            }

            code = code.substring(indexDebutCode + 6, indexFinCode);

            // Libelle
            // Libelle l = enregistrerLibelle("infra_sofi.libelle.region.", code, nom, codeLocale);

            Region r = (Region) this.regionDao.get(code);

            if (r == null) {
              r = new Region();
              r.setCodePays(codePays);
              r.setCode(code);
              r.setDateCreation(new Date());
              r.setCreePar("sofi");
              regionDao.ajouter(r);
              r.miseAJourListeDescriptionLocale();
              regionDao.modifier(r);
            } else {
              r.setCode(code);
              r.setCodePays(codePays);
              r.miseAJourListeDescriptionLocale();
              r.setDateModification(new Date());
              r.setModifiePar("sofi");
              regionDao.modifier(r);
            }
          }
        }
      }
    }
  }

  private void miseAJourListePays(String url, String codeLocale) {
    String liste = getHttp(url);

    for (StringTokenizer tk = new StringTokenizer(liste, "\r\n"); tk.hasMoreTokens();) {
      // AFGHANISTAN;AF
      String part = tk.nextToken();
      int pos = part.indexOf(";");

      if (part != null && pos != -1 && part.indexOf("ISO 3166-1") == -1) {
        String nom = part.substring(0, pos);
        String code = part.substring(pos + 1);

        // Libelle
        // Libelle l = enregistrerLibelle("infra_sofi.libelle.pays.", code, nom, codeLocale);

        // Pays
        Pays p = (Pays) this.paysDao.get(code);
        if (p == null) {
          p = new Pays();
          p.setCode(code);
          p.setDateCreation(new Date());
          p.setCreePar("sofi");
          paysDao.ajouter(p);
          p.miseAJourListeDescriptionLocale();
          paysDao.modifier(p);
        } else {
          p.setCode(code);
          p.miseAJourListeDescriptionLocale();
          p.setDateModification(new Date());
          p.setModifiePar("sofi");
          paysDao.modifier(p);
        }
      }
    }
  }

  private String getHttp(String url) {
    String liste = null;
    try {
      HttpClient client = new HttpClient();
      GetMethod get = new GetMethod(url);
      client.executeMethod(get);
      liste = get.getResponseBodyAsString();
    } catch (Exception e) {
      throw new ModeleException("Error pour obtenir le contenu http.", e);
    }
    return liste;
  }

  /*private Libelle enregistrerLibelle(String prefix, String code, String nom, String codeLocale) {
    String cle = prefix + code;

    Libelle l = serviceLibelle.get(cle, codeLocale);
    if (l == null) {
      l = new Libelle();
      l.setCleLibelle(cle);
      l.setCodeLangue(codeLocale);
      l.setCodeApplication(GestionSecurite.getInstance().getCodeApplication());
      l.setCreePar("sofi");
    }

    l.setTexteLibelle(nom);

    this.serviceLibelle.enregistrer(l, null);
    return l;
  }*/

  public List<Pays> getListePays() {
    return this.paysDao.getListePays();
  }

  public List<Region> getListeRegion(String codePays) {
    return this.regionDao.getListeRegion(codePays);
  }
  
  public List<Region> getListeRegionEnfant(String codePays, String codeParent) {
    return this.regionDao.getListeRegionEnfant(codePays, codeParent);
  }
  
  public void modifierVille(Ville ville){
    this.getVilleDao().modifier(ville);
  }

  public void setPaysDao(PaysDao paysDao) {
    this.paysDao = paysDao;
  }

  public PaysDao getPaysDao() {
    return paysDao;
  }

  public void setRegionDao(RegionDao regionDao) {
    this.regionDao = regionDao;
  }

  public RegionDao getRegionDao() {
    return regionDao;
  }

  public VilleDao getVilleDao() {
    return villeDao;
  }

  public void setVilleDao(VilleDao villeDao) {
    this.villeDao = villeDao;
  }

  public void setServiceLibelle(ServiceLibelle serviceLibelle) {
    this.serviceLibelle = serviceLibelle;
  }

  public ServiceLibelle getServiceLibelle() {
    return serviceLibelle;
  }

  @Override
  public List<Ville> getListeVille(String codeRegionParent) {
   return this.villeDao.getListeVille(codeRegionParent);
  }
  
  @Override
  public List<Ville> getListeVille(String filtre, String locale, Long nbResult) {
    if (StringUtils.isEmpty(locale)) {
      locale = getUtilisateur() != null ? getUtilisateur().getLangue()
          : Locale.CANADA_FRENCH.getLanguage();
    }
    try {
      if (StringUtils.isNotEmpty(filtre)){
        filtre = UtilitaireString.toUpperSansAccents(filtre, "UTF-8");
      }
      return this.villeDao.getListeVille(filtre, locale, nbResult);
    } catch (Exception e) {
      throw new ModeleException("Error while getting cities", e);
    }
  }

  @Override
  public Region getRegion(String codeRegion) {
    return this .getRegionDao().getRegion(codeRegion);
  }

  @Override
  public Ville getVille(Long id) {
    return this.villeDao.getVille(id);
  }

  @Override
  public VilleRegion getVilleRegion(Long idVille) {
    VilleRegion villeRegion = new VilleRegion();
    Ville ville = this.getVille(idVille);
    if (ville != null){
     Region region = this.getRegion(ville.getCodeRegion());
     villeRegion.setVille(ville);
     villeRegion.setRegion(region);
    }
    return villeRegion;
  }
  
  @Override
  public List<Ville> getListeVilleParRegion(String codeRegion) {
    return this.villeDao.getListeVilleParRegion(codeRegion);
  }
  
  
}
