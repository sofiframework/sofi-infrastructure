package org.sofiframework.infrasofi.modele.localisation.dao.hibernate;

import java.util.List;

import org.sofiframework.application.locale.objetstransfert.Pays;
import org.sofiframework.infrasofi.modele.localisation.dao.PaysDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

public class PaysDaoImpl extends BaseDaoImpl implements PaysDao {

  public PaysDaoImpl() throws IllegalArgumentException, NullPointerException {
    super(Pays.class);
  }

  @SuppressWarnings("unchecked")
  public List<Pays> getListePays() {
    return this.getHibernateTemplate().find("from Pays order by ordre, code");
  }
}
