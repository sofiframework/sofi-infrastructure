package org.sofiframework.infrasofi.modele.localisation.dao;

import java.util.List;

import org.sofiframework.application.locale.objetstransfert.Ville;
import org.sofiframework.modele.spring.dao.BaseDao;

public interface VilleDao extends BaseDao {
  
  /**Récupérer la liste des villes selon le code de région parent
   * 
   * @param codeRegionParent
   * @return liste villes.
   */
  List<Ville> getListeVille(String codeRegionParent);
  
  
  /**Récupérer la liste des villes selon le code de région 
   * 
   * @param codeRegion
   * @return liste villes.
   */
  List<Ville> getListeVilleParRegion(String codeRegion);
  
  /**Récupérer une ville à partir de son identifiant.
   * 
   * @param id
   * @return Ville
   */
  Ville getVille(Long id);
  
  /**
   * Récupérer la liste des villes selon une description passée en paramètre.
   * 
   * @param filtre
   *          Description utilisée pour la recherche (la recherche est effectuée
   *          en comparant la chaîne de caractères passée en paramètre à la
   *          description de la ville )
   * @param nbResult
   *          Le nombre de résultat souhaité.
   * @param locale
   *          la locale utilisée pour la recherche
   * @return liste villes.
   */
  List<Ville> getListeVille(String filtre, String locale, Long nbResult);
    
  
}
