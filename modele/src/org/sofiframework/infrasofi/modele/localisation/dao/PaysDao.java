package org.sofiframework.infrasofi.modele.localisation.dao;

import java.util.List;

import org.sofiframework.application.locale.objetstransfert.Pays;
import org.sofiframework.modele.spring.dao.BaseDao;

public interface PaysDao extends BaseDao {
  List<Pays> getListePays();
}
