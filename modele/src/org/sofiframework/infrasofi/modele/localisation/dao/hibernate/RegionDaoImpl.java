package org.sofiframework.infrasofi.modele.localisation.dao.hibernate;

import java.util.List;

import org.sofiframework.application.locale.objetstransfert.Region;
import org.sofiframework.infrasofi.modele.localisation.dao.RegionDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

public class RegionDaoImpl extends BaseDaoImpl implements RegionDao {

  public RegionDaoImpl() throws IllegalArgumentException, NullPointerException {
    super(Region.class);
  }

  @SuppressWarnings("unchecked")
  public List<Region> getListeRegion(String codePays) {
    List<Region> listeRegion = this.getListe("from Region r where r.codePays = ? and r.codeParent is null order by ordre, code", new Object[]{codePays});
    return listeRegion;
  }
  
  @SuppressWarnings("unchecked")
  public List<Region> getListeRegionEnfant(String codePays, String codeParent) {
    List<Region> listeRegion = this.getListe("from Region r where r.codePays = ? and r.codeParent = ? order by ordre, code", new Object[]{codePays, codeParent});
    return listeRegion;
  }

  @Override
  public Region getRegion(String codeRegion) {
   return (Region)this.get(codeRegion);
  }
  
}
