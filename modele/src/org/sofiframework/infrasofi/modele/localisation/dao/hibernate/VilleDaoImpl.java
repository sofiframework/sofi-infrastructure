package org.sofiframework.infrasofi.modele.localisation.dao.hibernate;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.sofiframework.application.locale.objetstransfert.Ville;
import org.sofiframework.infrasofi.modele.localisation.dao.VilleDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

public class VilleDaoImpl extends BaseDaoImpl implements VilleDao {

  


  public VilleDaoImpl() throws IllegalArgumentException,NullPointerException {
    super(Ville.class);
  }


  @SuppressWarnings("unchecked")
  @Override
  public List<Ville> getListeVille(String codeRegionParent) {
    List<Ville> listeVille = this
        .getListe(
            "select v from Ville v, Region r where v.codeRegion = r.code and r.codeParent = ? order by v.ordre, v.codeVille",
            new Object[] { codeRegionParent });
    return listeVille;
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public List<Ville> getListeVilleParRegion(String codeRegion) {
    List<Ville> listeVille = this
        .getListe(
            "select v from Ville v where v.codeRegion = ? order by v.ordre, v.codeVille",
            new Object[] { codeRegion });
    return listeVille;
  }

  @Override
  public Ville getVille(Long id) {
    return (Ville) this.get(id);
  }


  @SuppressWarnings("unchecked")
  @Override
  public List<Ville> getListeVille(String filter, String locale, Long nbResult) {
    StringBuffer query = new StringBuffer("select v from Ville v inner join v.listeVilleLocale vl where vl.code = ?");
    Object[] parameters = null;
     
    if (StringUtils.isNotEmpty(filter)){
      query.append(" and convert (upper(vl.description), 'us7ascii')  like ? and rownum <= ? ");
      parameters = new Object[]{locale, filter + "%", nbResult};
    }else{
      parameters = new Object[] { locale };
    }
    
    query.append(" order by vl.description");
    List<Ville> listeVille = this.getListe(query.toString(), parameters);
    
    return listeVille;
  }


 
  
  

}
