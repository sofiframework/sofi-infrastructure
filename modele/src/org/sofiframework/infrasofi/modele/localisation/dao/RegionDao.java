package org.sofiframework.infrasofi.modele.localisation.dao;

import java.util.List;

import org.sofiframework.application.locale.objetstransfert.Region;
import org.sofiframework.modele.spring.dao.BaseDao;

public interface RegionDao extends BaseDao {
  List<Region> getListeRegion(String codePays);
  
  /**Récupérer la liste des régions selon le code de pays et le code de région parent
   * 
   * @param codePays
   * @param codeParent
   * @return liste région.
   */
  List<Region> getListeRegionEnfant(String codePays, String codeParent);
  
  /**
   * Récupérer la région par son code de région.
   * @param codeRegion
   * @return
   */
  Region getRegion(String codeRegion); 
}
