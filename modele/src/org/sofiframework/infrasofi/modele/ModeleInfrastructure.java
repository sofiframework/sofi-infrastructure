/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele;

import org.sofiframework.application.courriel.service.ServiceCourriel;
import org.sofiframework.application.securite.service.ServiceClient;
import org.sofiframework.infrasofi.modele.conversion.service.local.ServiceConversion;
import org.sofiframework.infrasofi.modele.service.ServiceAideContextuelle;
import org.sofiframework.infrasofi.modele.service.ServiceApplication;
import org.sofiframework.infrasofi.modele.service.ServiceCertificat;
import org.sofiframework.infrasofi.modele.service.ServiceDeclencheur;
import org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur;
import org.sofiframework.infrasofi.modele.service.ServiceEnvoiCourriel;
import org.sofiframework.infrasofi.modele.service.ServiceJob;
import org.sofiframework.infrasofi.modele.service.ServiceJournalisation;
import org.sofiframework.infrasofi.modele.service.ServiceLibelle;
import org.sofiframework.infrasofi.modele.service.ServiceMessage;
import org.sofiframework.infrasofi.modele.service.ServiceOrdonnanceur;
import org.sofiframework.infrasofi.modele.service.ServiceParametreSysteme;
import org.sofiframework.infrasofi.modele.service.ServiceReferentiel;
import org.sofiframework.infrasofi.modele.service.ServiceRole;
import org.sofiframework.infrasofi.modele.service.ServiceUtilisateur;
import org.sofiframework.infrasofi.modele.service.ServiceUtilisateurRole;

public interface ModeleInfrastructure {

  ServiceReferentiel getServiceReferentiel();

  ServiceApplication getServiceApplication();

  ServiceLibelle getServiceLibelle();

  ServiceAideContextuelle getServiceAideContextuelle();

  ServiceMessage getServiceMessage();

  ServiceJournalisation getServiceJournalisation();

  ServiceParametreSysteme getServiceParametreSysteme();

  ServiceUtilisateur getServiceUtilisateur();

  ServiceRole getServiceRole();

  ServiceDomaineValeur getServiceDomaineValeur();

  ServiceUtilisateurRole getServiceUtilisateurRole();

  ServiceConversion getServiceConversion();

  ServiceOrdonnanceur getServiceOrdonnanceur();

  ServiceJob getServiceJob();

  ServiceDeclencheur getServiceDeclencheur();

  ServiceCertificat getServiceCertificat();

  ServiceClient getServiceClient();

  ServiceCourriel getServiceCourriel();

  ServiceEnvoiCourriel getServiceEnvoiCourriel();
}