/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.journalisation.dao.hibernate;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Journalisation;
import org.sofiframework.infrasofi.modele.journalisation.dao.JournalisationDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.sofiframework.modele.spring.dao.hibernate.CritereSupplementaire;

/**
 * Accès de données pour l'objet d'affaire journalisation.
 * <p>
 * @author Jean-Maxime Pelletier
 */
public class JournalisationDaoImpl extends BaseDaoImpl implements JournalisationDao {

  /** constructeur */
  public JournalisationDaoImpl() {
    super(Journalisation.class);
  }

  /* (non-Javadoc)
   * @see org.sofiframework.infrasofi.modele.journalisation.dao.hibernate.JournalisationDao#getList(org.sofiframework.sofi.composantweb.liste.ListeNavigation, java.lang.String)
   */
  public ListeNavigation getList(ListeNavigation ln, String codeApplication) {
    return getListe(ln);
  }

  @Override
  public ListeNavigation getListe(ListeNavigation liste) {
    this.getListe(liste, new CritereSupplementaire() {
      @Override
      public void ajouter(DetachedCriteria criteres) {
      }
    });
    return liste;
  }

  public ListeNavigation getListeJournalisationPilotage(ListeNavigation liste, String codeUtilisateur) {
    Map<String, Object> params = new HashMap();
    params.put("codeUtilisateur", codeUtilisateur);
    params.put("dateCourante", new Date());

    //this.activerFiltreSession("filtreObjetJavaUtilisateurRole", params);

    return this.getListe(liste);
  }
}