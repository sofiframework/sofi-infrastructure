/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.journalisation.dao;

import org.sofiframework.composantweb.liste.ListeNavigation;

/**
 * Accès de données de l'e ntité journalisaiton.
 * 
 * @author Jean-Maxime Pelletier
 */
public interface JournalisationDao {

  /**
   * Obtenir la liste des entrées de journalisation pour une application.
   * 
   * @param ln
   *          Liste de navigation
   * @param codeApplication
   *          Code unique de l'application
   * @return Liste de navigation
   */
  ListeNavigation getList(ListeNavigation ln, String codeApplication);

  /**
   * Obtenir la liste des entrées de journalisations qui sont permises pour un
   * utilisateur.
   * 
   * Un utilisateur peut seulement voir les entrées pour les objets java qui
   * sont associés à ses rôles de premier niveau.
   * 
   * @param liste
   * @param codeUtilisateur
   * @return
   */
  ListeNavigation getListeJournalisationPilotage(ListeNavigation liste,
      String codeUtilisateur);
}