/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.journalisation.service.local;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.journalisation.dao.JournalisationDao;
import org.sofiframework.infrasofi.modele.service.ServiceJournalisation;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;

/**
 * Service local de gestion des entrées de journalisation.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceJournalisationImpl extends DaoServiceImpl implements
ServiceJournalisation {

  public ListeNavigation getListeJournalisationPourTableauBord(
      ListeNavigation liste, String codeApplication) {
    return this.getJournalisationDao().getList(liste, codeApplication);
  }

  public ListeNavigation getListeJournalisationPilotage(ListeNavigation liste) {
    String codeUtilisateur = this.getCodeUtilisateurEnCours();
    return this.getJournalisationDao().getListeJournalisationPilotage(liste,
        codeUtilisateur);
  }

  private JournalisationDao getJournalisationDao() {
    return (JournalisationDao) this.getDao();
  }
}