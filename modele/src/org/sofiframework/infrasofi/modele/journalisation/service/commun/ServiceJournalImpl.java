/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.journalisation.service.commun;

import org.apache.commons.beanutils.PropertyUtils;
import org.sofiframework.application.journalisation.objetstransfert.Journal;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.entite.Journalisation;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.service.DaoService;
import org.sofiframework.modele.spring.service.local.BaseServiceImpl;

/**
 * Classe implémentant l'interface ServiceJournalisation et qui implémente la méthode ecrire(...).
 * 
 * @author Jean-Maxime Pelletier
 * @author Marie-Soleil L'Allier (Nurun inc.)
 */
public class ServiceJournalImpl extends BaseServiceImpl implements
org.sofiframework.application.journalisation.service.ServiceJournalisation {

  /**
   * Service local de journalisation
   */
  private DaoService serviceJournalisation = null;

  /**
   * Ajoute une entrée de journal dans la base de données.
   * 
   * @param journal
   *          Objet décrivant une entrée de journal
   * @throws ModeleException
   *           Exception du modèle survenu lors du traitement
   */
  public Journal ecrire(Journal journal) {
    Journalisation j = null;
    try {
      j = new Journalisation();
      PropertyUtils.copyProperties(j, journal);
      this.getServiceJournalisation().ajouter(j);
    } catch (Exception e) {
      throw new ModeleException(e);
    }
    return journal;
  }

  /**
   * Obtenir le service local de journalisation
   * 
   * @return service de journalisation
   */
  public DaoService getServiceJournalisation() {
    return serviceJournalisation;
  }

  /**
   * Fixer le service local de journalisation.
   * 
   * @param serviceJournalisation
   *          service de journalisation
   */
  public void setServiceJournalisation(DaoService serviceJournalisation) {
    this.serviceJournalisation = serviceJournalisation;
  }

  public Journal getJournal(Long idJournalisation) {
    Journal journal = null;
    Journalisation journalisation = (Journalisation) this.getServiceJournalisation().get(idJournalisation);
    if (journalisation != null) {
      journal = construireJournal(journalisation);
    }
    return journal;
  }

  @SuppressWarnings("unchecked")
  public ListeNavigation getListeJournalisation(ListeNavigation liste) {
    liste = this.getServiceJournalisation().getListe(liste);
    for (int i = 0; i < liste.getListe().size(); i++) {
      Journalisation entite = (Journalisation) liste.getListe().get(i);
      Journal objetTransfert = construireJournal(entite);
      liste.getListe().set(i, objetTransfert);
    }

    return liste;
  }

  /**
   * Créer un objet {@link Journal} à partir d'un objet {@link Journalisation}.
   * 
   * @param entite
   * @return
   */
  private Journal construireJournal(Journalisation entite) {
    Journal objetTransfert = new Journal();
    try {
      PropertyUtils.copyProperties(objetTransfert, entite);
    } catch (Exception e) {
      throw new ModeleException(e);
    }
    return objetTransfert;
  }
}