package org.sofiframework.infrasofi.modele.courriel.service.commun;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.sofiframework.application.courriel.objetstransfert.Courriel;
import org.sofiframework.application.courriel.objetstransfert.EnvoiCourriel;
import org.sofiframework.application.courriel.objetstransfert.TentativeEnvoiCourriel;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.commun.Domaine;
import org.sofiframework.infrasofi.modele.commun.TypeRequete;
import org.sofiframework.infrasofi.modele.courriel.dao.EnvoiCourrielDao;
import org.sofiframework.infrasofi.modele.entite.Smtp;
import org.sofiframework.infrasofi.modele.filtre.FiltreEnvoiCourriel;
import org.sofiframework.infrasofi.modele.service.ServiceEnvoiCourriel;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;
import org.sofiframework.utilitaire.UtilitaireException;
import org.springframework.scheduling.annotation.Async;

/**
 * Logique d'affaire de gestion d'envoi de courriels.
 * 
 * @author jean-maxime.pelletier
 */
public class ServiceEnvoiCourrielImpl extends DaoServiceImpl implements
    ServiceEnvoiCourriel {

  public ServiceEnvoiCourrielImpl() {
  }

  /**
   * Envoi le courriel en sauvegardant les informations. Sauvegarde les données
   * du courriel et un envoi de courriel. Si l'envoi ne réussi pas, une tâche
   * asynchrone va refaire des tentatives jusqu'a un certain nombre maximal.
   * 
   * @param courriel
   */
  @Async
  public void envoyer(Courriel courriel, Smtp smtp) {
    EnvoiCourriel envoiCourriel = this.construireEnvoi(courriel, smtp);

    String courrielOrigi = courriel.getCorps();
    int courrielLength = courrielOrigi.length();

    if (courrielLength > 31999) {
      courriel.setCorps(courrielOrigi.substring(0, 32000));
      courriel.setCorps2(courrielOrigi.substring(32000));
    }

    this.effectuerTentativeEnvoi(envoiCourriel);
  }

  private void effectuerTentativeEnvoi(EnvoiCourriel envoiCourriel) {
    TentativeEnvoiCourriel tentative = new TentativeEnvoiCourriel();
    tentative.setDateEnvoi(new Date());

    // Augmenter le nombre de tentatives
    Integer nombreTentative = envoiCourriel.getNombreTentative();
    envoiCourriel.setNombreTentative(nombreTentative + 1);

    try {
      this.effectuerEnvoiCommonsEmail(envoiCourriel);
      tentative.setStatut(Domaine.statutTentativeEnvoiCourriel.succes
          .getValeur());
    } catch (Exception e) {
      tentative.setStatut(Domaine.statutTentativeEnvoiCourriel.echec
          .getValeur());
      String exception = UtilitaireException.getMessageExceptionAuComplet(e);
      tentative.setMessageErreur(exception);
    }

    this.ajouterDerniereTentative(envoiCourriel, tentative);
    this.sauvegarderEnvoiCourriel(envoiCourriel);
  }

  /**
   * Crée une instance du apache commons Email avec les informations de l'envoi
   * de courriel.
   */
  private void effectuerEnvoiCommonsEmail(EnvoiCourriel envoiCourriel) {
    MultiPartEmail mail = null;
    if (envoiCourriel.getCourriel().isTypeHtml()) {
      mail = new HtmlEmail();
    } else {
      mail = new MultiPartEmail();
    }
    mail.setHostName(envoiCourriel.getSmtpHost());
    mail.setAuthentication(envoiCourriel.getSmtpUsername(),
        envoiCourriel.getSmtpPassword());
    mail.setSSL(envoiCourriel.getSmtpSecure());

    Courriel courriel = envoiCourriel.getCourriel();

    mail.setCharset(courriel.getCharset());
    mail.setSubject(courriel.getSujet());
    try {
      if (envoiCourriel.getCourriel().isTypeHtml()) {
        ((HtmlEmail) mail).setHtmlMsg(courriel.getCorps());
      } else {
        StringBuffer messageCourriel = new StringBuffer(courriel.getCorps());
        if (courriel.getCorps2() != null) {
          messageCourriel.append(courriel.getCorps2());
        }
        mail.setMsg(messageCourriel.toString());
      }
    } catch (EmailException e) {
      throw new ModeleException("Erreur pour affecter le message", e);
    }

    try {
      mail.setFrom(courriel.getAdresseExpediteur(),
          courriel.getNomExpediteur());
    } catch (EmailException e) {
      throw new ModeleException("Erreur mettre à jour l'expéditeur "
          + courriel.getAdresseExpediteur(), e);
    }
    /**
     * @since 3.2.4 le support à plusieurs destinataire a été retiré.
     * 
     *        for (ContactCourriel destinataire :
     *        courriel.getListeDestinataire()) {
     * 
     *        }
     */
    try {
      mail.addTo(courriel.getAdresseDestinataire(),
          courriel.getNomDestinataire());
    } catch (EmailException e) {
      throw new ModeleException("Erreur ajouter un destinataire "
          + courriel.getAdresseDestinataire(), e);
    }

    try {
      mail.send();
    } catch (EmailException e) {
      throw new ModeleException("Erreur lors de l'envoi du courriel.", e);
    }
  }

  private void ajouterDerniereTentative(EnvoiCourriel envoiCourriel,
      TentativeEnvoiCourriel tentative) {
    if (envoiCourriel.getListeTentativeEnvoiCourriel() == null) {
      envoiCourriel
          .setListeTentativeEnvoiCourriel(new ArrayList<TentativeEnvoiCourriel>());
    }
    envoiCourriel.getListeTentativeEnvoiCourriel().add(tentative);

    // Déterminer le statut de l'envoi selon la derniere tentative

    if (tentative.getStatut().equals(
        Domaine.statutTentativeEnvoiCourriel.echec.getValeur())) {
      // Nombre d'echec maximal atteind
      if (envoiCourriel.getNombreTentative().equals(
          getMaxNombreTentativeEnvoi())) {
        envoiCourriel
            .setStatut(Domaine.statutEnvoiCourriel.abandon.getValeur());
      } else {
        envoiCourriel.setStatut(Domaine.statutEnvoiCourriel.echec.getValeur());
      }
    } else {
      // Si la dernière tentative est un succes envoi est un succes
      envoiCourriel.setStatut(Domaine.statutEnvoiCourriel.succes.getValeur());
    }
  }

  private void sauvegarderEnvoiCourriel(EnvoiCourriel envoiCourriel) {
    if (envoiCourriel.getId() != null) {
      this.getEnvoiCourrielDao().modifier(envoiCourriel);
    } else {
      this.getEnvoiCourrielDao().ajouter(envoiCourriel);
    }
  }

  private EnvoiCourriel construireEnvoi(Courriel courriel, Smtp smtp) {
    EnvoiCourriel envoiCourriel = new EnvoiCourriel();
    envoiCourriel.setCourriel(courriel);
    envoiCourriel.setSmtpHost(smtp.getHost());
    envoiCourriel.setSmtpUsername(smtp.getUsername());
    envoiCourriel.setSmtpPassword(smtp.getPassword());
    envoiCourriel.setSmtpSecure(smtp.getSecure());
    envoiCourriel.setNombreTentative(0);
    return envoiCourriel;
  }

  /**
   * Obtien la liste des envois de courriel en échec. Essais d'envoyer le
   * courriel à nouveau. Ajuste le statut et le nombre de tentatives.
   */
  public void effectuerTentatives() {
    Integer maxNombreTentative = getMaxNombreTentativeEnvoi();
    FiltreEnvoiCourriel filtre = new FiltreEnvoiCourriel();
    filtre.setNombreTentative(maxNombreTentative);
    filtre.setStatut(new String[] { Domaine.statutEnvoiCourriel.echec
        .toString() });
    filtre.setTypeRequeteNombreTentative(TypeRequete.lower);

    List<EnvoiCourriel> envoisATraiter = this.getEnvoiCourrielDao()
        .getListeEnvoiCourriel(filtre);
    for (EnvoiCourriel envoi : envoisATraiter) {
      this.effectuerTentativeEnvoi(envoi);
    }
  }

  /**
   * Destruction des anciens envois de emails qui dépasse la limite de nombre de
   * jour qui est configurée avec le paramètre système. Seulement les les envois
   * qui sont de statut abandon ou succes seront supprimés.
   * 
   * MAX_JOUR_CONSERVER_COURRIEL_SUCCES = Date limite pour les emails en succes.
   * MAX_JOUR_CONSERVER_COURRIEL_ABANDON - Date limite pour les emails en
   * abandon.
   */
  public void nettoyerAnciensEnvoisCourriels() {
    nettoyerAnciensEnvoisCourriels("MAX_JOUR_CONSERVER_COURRIEL_SUCCES",
        Domaine.statutEnvoiCourriel.succes);
    nettoyerAnciensEnvoisCourriels("MAX_JOUR_CONSERVER_COURRIEL_ABANDON",
        Domaine.statutEnvoiCourriel.abandon);
  }

  public void nettoyerAnciensEnvoisCourriels(String codeParamDateLimite,
      Domaine.statutEnvoiCourriel statut) {
    Date dateLimite = this
        .getDateLimiteAncienEnvoiCourriel(codeParamDateLimite);

    FiltreEnvoiCourriel filtre = new FiltreEnvoiCourriel();
    filtre.setStatut(new String[] { statut.getValeur() });
    filtre.setDateEnvoi(dateLimite);

    List<EnvoiCourriel> listeEnvoiASupprimer = this
        .getListeEnvoiCourriel(filtre);
    for (EnvoiCourriel envoiCourriel : listeEnvoiASupprimer) {
      this.getDao().supprimer(envoiCourriel.getId());
    }
  }

  public List<EnvoiCourriel> getListeEnvoiCourriel(FiltreEnvoiCourriel filtre) {
    return this.getEnvoiCourrielDao().getListeEnvoiCourriel(filtre);
  }

  public ListeNavigation getListeEnvoiCourriel(ListeNavigation ln) {
    // TODO Auto-generated method stub
    return null;
  }

  private Date getDateLimiteAncienEnvoiCourriel(String codeParametre) {
    Integer maxNombreJour = GestionParametreSysteme.getInstance().getInteger(
        codeParametre);
    if (maxNombreJour == null) {
      maxNombreJour = 30;
    }
    Date dateLimite = DateUtils.addDays(new Date(), maxNombreJour * -1);
    return dateLimite;
  }

  private Integer getMaxNombreTentativeEnvoi() {
    Integer maxNombreTentative = GestionParametreSysteme.getInstance()
        .getInteger("MAX_NOMBRE_TENTATIVE_ENVOI");
    if (maxNombreTentative == null) {
      maxNombreTentative = 5;
    }
    return maxNombreTentative;
  }

  private EnvoiCourrielDao getEnvoiCourrielDao() {
    return (EnvoiCourrielDao) this.getDao();
  }
}
