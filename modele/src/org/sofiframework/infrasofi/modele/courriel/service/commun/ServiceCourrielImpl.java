package org.sofiframework.infrasofi.modele.courriel.service.commun;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.Velocity;
import org.sofiframework.application.courriel.objetstransfert.Courriel;
import org.sofiframework.application.courriel.service.ServiceCourriel;
import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.commun.Domaine;
import org.sofiframework.infrasofi.modele.courriel.dao.CourrielDao;
import org.sofiframework.infrasofi.modele.entite.Smtp;
import org.sofiframework.infrasofi.modele.service.ServiceEnvoiCourriel;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;
import org.sofiframework.presentation.velocity.UtilitaireVelocity;
import org.sofiframework.utilitaire.UtilitaireString;

public class ServiceCourrielImpl extends DaoServiceImpl implements ServiceCourriel {

  private static final Log log = LogFactory.getLog(ServiceCourrielImpl.class);

  private ServiceEnvoiCourriel serviceEnvoiCourriel;

  private String getLibelle(String identifiant, String codeLocale, Object[] params, String codeClient, boolean escapeXml) {
    Message libelle = GestionLibelle.getInstance().get(identifiant, codeLocale, params, null);
    String valeurLibelle = libelle.getMessage();
    if (escapeXml) {
      valeurLibelle = UtilitaireString.convertirEnHtml(valeurLibelle);
    }
    return valeurLibelle;
  }

  private String getLibelle(String identifiant, String codeLocale, String codeClient, Object[] params) {
    return this.getLibelle(identifiant, codeLocale, params, codeClient, true);
  }

  public void envoyer(String cleSujet, Object[] sujetParams,
      String cleCorps, HashMap<String, Object> params,
      String courrielTo, String prenomTo, String nomTo,
      String courrielFrom, String descriptionFrom,
      String codeLocale, String codeClient) {

    params.put("prenom", UtilitaireString.convertirEnHtml(prenomTo));
    params.put("nom", UtilitaireString.convertirEnHtml(nomTo));
    String sujet = this.getLibelle(cleSujet, codeLocale, codeClient, sujetParams);
    params.put("subject", sujet);
    params.put("body", this.getLibelle(cleCorps, codeLocale, null, codeClient, false));

    String cleFooter = getCleFooter(params);
    if (cleFooter != null) {
      params.put("footer", this.getLibelle(cleFooter, codeLocale, null, codeClient, false));
    }

    String cleTemplate = getCleTemplate(params);
    String template = this.getLibelle(cleTemplate, codeLocale, null, codeClient, false);
    String message = this.mergeVelocityTemplate(template, params);

    @SuppressWarnings("unchecked")
    Map<String, String> headers = (Map<String, String>) params.get("headers");

    Courriel courriel = buildCourriel(courrielTo, prenomTo, nomTo, courrielFrom, descriptionFrom, sujet, message, headers);
    
    this.serviceEnvoiCourriel.envoyer(courriel, this.getSmtp());
  }

  public void envoyer(String sujet, String message,
      Map<String, String> headers,
      String courrielTo, String prenomTo, String nomTo,
      String courrielFrom, String descriptionFrom,
      String codeLocale, String codeClient) {

    Courriel courriel = buildCourriel(courrielTo, prenomTo, nomTo, courrielFrom, descriptionFrom, sujet, message, headers);

    this.serviceEnvoiCourriel.envoyer(courriel, this.getSmtp());
  }

  public ServiceEnvoiCourriel getServiceEnvoiCourriel() {
    return serviceEnvoiCourriel;
  }

  public void setServiceEnvoiCourriel(ServiceEnvoiCourriel serviceEnvoiCourriel) {
    this.serviceEnvoiCourriel = serviceEnvoiCourriel;
  }

  public void envoyer(
      org.sofiframework.application.courriel.objetstransfert.Courriel courriel) {
    
    buildCourriel(courriel);
    
    this.serviceEnvoiCourriel.envoyer(courriel, this.getSmtp());
    
  }

  @Override
  public ListeNavigation getListeCourriel(ListeNavigation liste) {
    return getCourrielDao().getListe(liste);
  }

  @Override
  public org.sofiframework.application.courriel.objetstransfert.Courriel getCourriel(
      Long id) {
    Courriel courriel = (Courriel) getCourrielDao().get(id);
    return courriel;
  }

  public CourrielDao getCourrielDao() {
    return (CourrielDao)getDao();
  }
  
  private Courriel buildCourriel(Courriel courriel) {
    courriel.setCharset("ISO-8859-1");
    courriel.setTypeContenu(Domaine.typeContenuCourriel.html.getValeur());
    courriel.setDateCreation(new java.util.Date());

    return courriel;
  }
  
  private Courriel buildCourriel(String courrielTo, String prenomTo, String nomTo, String courrielFrom, String descriptionFrom, String sujet, String message, Map<String, String> headers) {
    Courriel courriel = new Courriel();
//    courriel.setExpediteur(new ContactCourriel(descriptionFrom, courrielFrom));
//    courriel.setListeDestinataire(new ArrayList<ContactCourriel>());
//    courriel.getListeDestinataire().add(
//        new ContactCourriel(prenomTo + " " + nomTo, courrielTo));
    
    courriel.setPrenomDestinataire(prenomTo);
    courriel.setNomDestinataire(nomTo);
    courriel.setAdresseDestinataire(courrielTo);
    courriel.setAdresseExpediteur(courrielFrom);
    courriel.setNomExpediteur(descriptionFrom);
    courriel.setCharset("ISO-8859-1");
    courriel.setSujet(sujet);
    courriel.setCorps(message);
    courriel.setTypeContenu(Domaine.typeContenuCourriel.html.getValeur());
    
    courriel.setDateCreation(new java.util.Date());

    return courriel;
  }
  
  private String getCleTemplate(HashMap<String, Object> params) {
    String cleTemplate = (String) params.get("template");
    if (cleTemplate == null) {
      cleTemplate = GestionParametreSysteme.getInstance().getParametreSysteme("courrielTemplate").toString();
    }
    return cleTemplate;
  }

  private String getCleFooter(HashMap<String, Object> params) {
    String cleFooter = (String) params.get("footer");
    if (cleFooter == null) {
      if (GestionParametreSysteme.getInstance().getParametreSysteme("courrielTemplateFooter") != null) {
        cleFooter = GestionParametreSysteme.getInstance().getParametreSysteme("courrielTemplateFooter").toString();
      }
    }
    return cleFooter;
  }

  private Smtp getSmtp() {
    String smtpServeur = GestionParametreSysteme.getInstance().getString("smtpServer");
    String smtpUsager = GestionParametreSysteme.getInstance().getString("smtpUser");
    String smtpMotPasse = GestionParametreSysteme.getInstance().getString("smtpPassword");
    Boolean smtpSecure = GestionParametreSysteme.getInstance().getBoolean("smtpSecure");
    Smtp smtp = new Smtp(smtpServeur, smtpUsager, smtpMotPasse, smtpSecure);
    return smtp;
  }

  private String mergeVelocityTemplate(String template, Map<String, Object> params) {
    String content = null;
    try {
      log.info("Initialisation de velocity pour la conception du courriel...");
      UtilitaireVelocity.initialiser();
      log.info("Velocity initialisation complete");
      UtilitaireVelocity velocity = new UtilitaireVelocity();
      for (String key : params.keySet()) {
        velocity.ajouterAuContexte(key, params.get(key));
      }
      // Get a template as stream.
      StringWriter writer = new StringWriter();
      StringReader reader = new StringReader(template);
      // ask Velocity to evaluate it.
      boolean result = Velocity.evaluate(velocity.getContexte(), writer, null, reader);
      if (result) {
        content = writer.getBuffer().toString();
      }
    } catch (Exception e) {
      throw new ModeleException("Erreur pour obtenir le contenu du mail.", e);
    }
    return content;
  }
}
