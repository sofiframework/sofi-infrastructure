package org.sofiframework.infrasofi.modele.courriel.tache;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.sofiframework.application.tacheplanifiee.job.TacheStateful;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;

/**
 * Exécution de l'envoi des courriels. Effectue les nouvelles tentatives et
 * nettoi les anciens essais.
 * 
 * @author j-m.pelletier
 */
public class TacheEnvoiCourriel extends TacheStateful {

  private static final Log log = LogFactory.getLog(TacheEnvoiCourriel.class);

  @Override
  public void executer(JobExecutionContext context) throws JobExecutionException {

    if (log.isWarnEnabled()) {
      log.warn("Envoi des courriels...");
    }

    ModeleInfrastructure modele = (ModeleInfrastructure) this.getModele(context, "modeleInfrastructure");

    // Nettoyer les anciennes entrées
    modele.getServiceEnvoiCourriel().nettoyerAnciensEnvoisCourriels();

    // Refaire les essais d'envoi
    modele.getServiceEnvoiCourriel().effectuerTentatives();

    if (log.isWarnEnabled()) {
      log.warn("Envoi des courriels terminé.");
    }

  }
}
