package org.sofiframework.infrasofi.modele.courriel.dao.hibernate;

import org.sofiframework.application.courriel.objetstransfert.Courriel;
import org.sofiframework.infrasofi.modele.courriel.dao.CourrielDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

public class CourrielDaoImpl extends BaseDaoImpl implements CourrielDao {

  public CourrielDaoImpl() {
    super(Courriel.class);
  }

}
