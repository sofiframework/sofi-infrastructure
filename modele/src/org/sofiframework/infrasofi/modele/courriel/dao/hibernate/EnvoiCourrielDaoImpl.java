package org.sofiframework.infrasofi.modele.courriel.dao.hibernate;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.sofiframework.application.courriel.objetstransfert.EnvoiCourriel;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.Tri;
import org.sofiframework.infrasofi.modele.BaseDaoInfrastructureImpl;
import org.sofiframework.infrasofi.modele.commun.TypeRequete;
import org.sofiframework.infrasofi.modele.courriel.dao.EnvoiCourrielDao;
import org.sofiframework.infrasofi.modele.filtre.FiltreEnvoiCourriel;
import org.sofiframework.modele.spring.dao.hibernate.CritereSupplementaire;

public class EnvoiCourrielDaoImpl extends BaseDaoInfrastructureImpl implements EnvoiCourrielDao {

  public EnvoiCourrielDaoImpl() {
    super(EnvoiCourriel.class);
  }

  public EnvoiCourriel getEnvoiCourriel(Long id) {
    return (EnvoiCourriel) this.get(id);
  }

  public void ajouter(EnvoiCourriel envoiCourriel) {
    super.ajouter(envoiCourriel);
  }

  public void modifier(EnvoiCourriel envoiCourriel) {
    super.modifier(envoiCourriel);
  }

  /**
   * Obtenir la liste des envois de courriels qui correspondent au critères de
   * recherche. Trié par ordre descendant de date d'envoi
   */
  @SuppressWarnings("unchecked")
  public List<EnvoiCourriel> getListeEnvoiCourriel(FiltreEnvoiCourriel filtre) {
    DetachedCriteria criteres = creerCriteria();
    criteres = appliquerFiltre(filtre);
    appliquerCriteresFiltre(filtre, criteres);
    appliquerTri(criteres, new Tri("derniereDateTentative", false));
    List<EnvoiCourriel> resultats = getHibernateTemplate().findByCriteria(criteres);
    return resultats;
  }

  private void appliquerCriteresFiltre(FiltreEnvoiCourriel filtre, DetachedCriteria criteres) {
    if (filtre.getNombreTentative() != null) {
      if (filtre.getTypeRequeteNombreTentative().equals(TypeRequete.greaterOrEquals)) {
        criteres.add(Restrictions.ge("nombreTentative", filtre.getNombreTentative()));
      } else if (filtre.getTypeRequeteNombreTentative().equals(TypeRequete.lower)) {
        criteres.add(Restrictions.lt("nombreTentative", filtre.getNombreTentative()));
      }
    }

    // dateEnvoi
    if (filtre.getDateEnvoi() != null) {
      criteres.add(Restrictions.lt("derniereDateTentative", filtre.getDateEnvoi()));
    }
  }

  public ListeNavigation getListeEnvoiCourriel(final ListeNavigation liste) {
    return this.getListe(liste, new CritereSupplementaire() {
      @Override
      public void ajouter(DetachedCriteria criteres) {
        appliquerCriteresFiltre((FiltreEnvoiCourriel) liste.getFiltre(), criteres);
      }
    });
  }
}
