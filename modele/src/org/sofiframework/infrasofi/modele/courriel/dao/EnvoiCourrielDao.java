package org.sofiframework.infrasofi.modele.courriel.dao;

import java.util.List;

import org.sofiframework.application.courriel.objetstransfert.EnvoiCourriel;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.filtre.FiltreEnvoiCourriel;

public interface EnvoiCourrielDao {

  EnvoiCourriel getEnvoiCourriel(Long id);

  void ajouter(EnvoiCourriel envoiCourriel);

  void modifier(EnvoiCourriel envoiCourriel);

  List<EnvoiCourriel> getListeEnvoiCourriel(FiltreEnvoiCourriel filtre);

  ListeNavigation getListeEnvoiCourriel(final ListeNavigation liste);

}
