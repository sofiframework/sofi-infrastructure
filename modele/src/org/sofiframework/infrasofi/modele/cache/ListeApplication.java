/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.cache;

import java.util.Iterator;
import java.util.List;

import org.sofiframework.application.cache.ObjetCache;
import org.sofiframework.infrasofi.modele.ModeleInfrastructure;
import org.sofiframework.infrasofi.modele.entite.Application;

/**
 * Cette ObjetCache représente la liste des applications disponible pour la
 * création et la modification d'objet du référentiel.
 * 
 * @author : Guillaume Poirier, Nurun inc.
 */
public class ListeApplication extends ObjetCache {

  private static final long serialVersionUID = 4210246703442517797L;

  @Override
  public void chargerDonnees() {
    ModeleInfrastructure modele = (ModeleInfrastructure) this.getServiceLocal();
    List<Application> applications = modele.getServiceApplication().getListeApplication();
    if (applications != null) {
      for (Iterator<Application> i = applications.iterator(); i.hasNext(); ) {
        Application app = i.next();
        put(app.getCodeApplication(), app.getNom());
      }
    }
  }
}
