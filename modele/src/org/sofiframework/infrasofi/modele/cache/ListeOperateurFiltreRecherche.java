/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.cache;

import org.sofiframework.application.cache.ObjetCache;

/**
 * Objet servant à ajouter dans un Singleton de l'application les différentes
 * valeurs disponible comme langues dans la base de données.
 * <p>
 * 
 * @author Jean-François Brassard, Nurun inc.
 */
public class ListeOperateurFiltreRecherche extends ObjetCache {

  private static final long serialVersionUID = -3977029173912005631L;

  /**
   * Méthode qui charge les données dans le Singleton de la cache d'application.
   * <p>
   * Les champs sont placés selon leur clé unique ainsi que la description
   * complète du champ.
   */
  @Override
  public void chargerDonnees() {
    put(" ", "");
    put("<", "<");
    put("<=", "<=");
    put("=", "=");
    put(">=", ">=");
    put(">", ">");
  }
}
