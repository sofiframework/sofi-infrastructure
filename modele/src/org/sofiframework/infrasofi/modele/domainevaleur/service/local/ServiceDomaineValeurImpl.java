/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.domainevaleur.service.local;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.sofiframework.application.message.GestionLibelle;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.domainevaleur.dao.DefinitionDomaineValeurDao;
import org.sofiframework.infrasofi.modele.domainevaleur.dao.DomaineValeurDao;
import org.sofiframework.infrasofi.modele.entite.DefinitionDomaineValeur;
import org.sofiframework.infrasofi.modele.entite.DomaineValeur;
import org.sofiframework.infrasofi.modele.filtre.FiltreDomaineValeur;
import org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur;
import org.sofiframework.infrasofi.modele.service.ServiceParametreSysteme;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Service de gestion de l'entié domaine de valeur.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceDomaineValeurImpl extends BaseServiceInfrastructureImpl implements ServiceDomaineValeur {

  private ServiceParametreSysteme serviceParametreSysteme;

  private DefinitionDomaineValeurDao definitionDomaineValeurDao = null;

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.sofiframework.sofi.application.domainevaleur.service.ServiceDomaineValeur
   * # getListeValeur(org.sofiframework.sofi.composantweb.liste.ListeNavigation)
   */
  public ListeNavigation getListeActif(ListeNavigation liste, String valeurSelectionnee) {
    FiltreDomaineValeur filtre = (FiltreDomaineValeur) liste.getObjetFiltre();

    // Si le code d'application est null ou vide, lancer une exception
    if (UtilitaireString.isVide(filtre.getCodeApplication())) {
      throw new IllegalArgumentException("Afin d'effectuer une recherche de domaine de valeur, la donnée 'codeApplication' doit être spécifiée.");
    }

    // Si le code de langue est null ou vide, lancer une exception
    if (UtilitaireString.isVide(filtre.getCodeLangue())) {
      throw new IllegalArgumentException("Afin d'effectuer une recherche de domaine de valeur, la donnée 'codeLangue' doit être spécifiée.");
    }

    // Si le nom du domaine est null ou vide, lancer une exception
    if (UtilitaireString.isVide(filtre.getNom())) {
      throw new IllegalArgumentException("Afin d'effectuer une recherche de domaine de valeur, le nom du domaine de valeur doit être spécifié.");
    }

    return this.getDomaineValeurDao().getListeActif(liste, valeurSelectionnee);
  }

  public ListeNavigation getListeDomaineValeurEnfant(ListeNavigation liste) {
    return this.getDomaineValeurDao().getListeDomaineValeurEnfant(liste);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur#
   * ajouterDefinitionDomaineValeur
   * (org.sofiframework.infrasofi.modele.entite.DefinitionDomaineValeur)
   */
  public void ajouterDefinitionDomaineValeur(DefinitionDomaineValeur definition) {
    this.getDefinitionDomaineValeurDao().ajouter(definition);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur#
   * ajouterDomaineValeur
   * (org.sofiframework.infrasofi.modele.entite.DomaineValeur)
   */
  public void ajouterDomaineValeur(DomaineValeur domaine) {
    this.ajouter(domaine);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur#
   * getDefinitionDomaineValeur(java.lang.String, java.lang.String,
   * java.lang.String)
   */
  public DefinitionDomaineValeur getDefinitionDomaineValeur(String codeApplication, String nom, String codeLangue) {
    return (DefinitionDomaineValeur) getDefinitionDomaineValeurDao().get(new DefinitionDomaineValeur(codeApplication, nom, codeLangue));
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur#
   * getDomaineValeur(java.lang.String, java.lang.String, java.lang.String,
   * java.lang.String)
   */
  public DomaineValeur getDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue) {
    DomaineValeur cle = new DomaineValeur();
    cle.setCodeApplication(codeApplication);
    cle.setCodeLangue(codeLangue);
    cle.setValeur(valeur);
    cle.setNom(nom);
    return (DomaineValeur) this.get(cle);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur#
   * getListeDomaineValeur(java.lang.String, java.lang.String, java.lang.String)
   */
  public List getListeDomaineValeur(String codeApplicationParent, String nomParent, String valeurParent) {
    FiltreDomaineValeur filtre = new FiltreDomaineValeur();
    filtre.setCodeApplicationParent(codeApplicationParent);
    filtre.setNomParent(nomParent);
    filtre.setValeurParent(valeurParent);
    return this.getDao().getListe(filtre);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur#
   * getListeDomaineValeurEnfants(java.lang.String, java.lang.String,
   * java.lang.String, java.lang.String)
   */
  public List getListeDomaineValeur(String codeApplicationParent, String nomParent, String valeurParent, String codeLangueParent) {
    List liste = getDomaineValeurDao().getListe(codeApplicationParent, nomParent, valeurParent, codeLangueParent, true);
    return liste;
  }

  public List getListeDomaineValeur(String codeApplicationParent, String nomParent, String valeurParent, String codeLangueParent, boolean valeurInactive) {
    List liste = getDomaineValeurDao().getListe(codeApplicationParent, nomParent, valeurParent, codeLangueParent, valeurInactive);
    return liste;
  }

  /**
   * Obtenir la liste des valeurs de domaine et charger les liste des enfants de
   * cette liste.
   */
  public List getListeDomaineValeur(String codeApplicationParent, String nomParent, String valeurParent, String listeValeurChargement, String codeLangueParent) {
    HashMap<String, DomaineValeur> parents = new HashMap<String, DomaineValeur>();
    if (listeValeurChargement != null) {
      for (String paire : listeValeurChargement.split(",")) {
        String nomDomaine = paire.substring(0, paire.indexOf("-"));
        String valeurDomaine = paire.substring(paire.indexOf("-") + 1);
        DomaineValeur domaine = this.getDomaineValeur(codeApplicationParent, nomDomaine, valeurDomaine, codeLangueParent);
        this.chargerParentee(domaine, parents);
      }
    }
    List listeDomaineValeur = this.getListeDomaineValeur(codeApplicationParent, nomParent, valeurParent, codeLangueParent);

    return listeDomaineValeur;
  }

  private void chargerParentee(DomaineValeur domaine, Map<String, DomaineValeur> parents) {
    String nomParent = domaine.getNomParent();
    String valeurParent = domaine.getValeurParent();
    String codeApplicationParent = domaine.getCodeApplicationParent();
    String codeLangueParent = domaine.getCodeLangueParent();

    DomaineValeur parent = parents.get(nomParent + "-" + valeurParent);
    if (parent == null) {
      parent = this.getDomaineValeur(codeApplicationParent, nomParent, valeurParent, codeLangueParent);
      if (parent != null) {
        parents.put(parent.getNom() + "-" + parent.getValeur(), parent);
        // Si il s'agit d'un nouveau parent on doit charger jusqu'au debut.
        this.chargerParentee(parent, parents);
        // On doit déclancher la liste des enfants pour avoir la liste.
        List listeEnfant = parent.getListeDomaineValeurEnfants();
        listeEnfant.isEmpty();
        parent.setChargementEnfant(Boolean.TRUE);
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur#
   * getListeDomaineValeurEnfantsPourDefinition(java.lang.String,
   * java.lang.String, java.lang.String)
   */
  public List getListeDomaineValeurPourDefinition(String codeApplicationParent, String nomParent, String codeLangueParent, boolean valeurInactive) {
    List listeDomaineValeur = ((DomaineValeurDao)this.getDao()).getListe(codeApplicationParent, nomParent, null, codeLangueParent, valeurInactive);
    return listeDomaineValeur;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur#
   * rechercherDefinitionDomaineValeur
   * (org.sofiframework.sofi.composantweb.liste.ListeNavigation)
   */
  public ListeNavigation getListeDefinitionDomaineValeur(ListeNavigation liste) {
    liste = this.getDefinitionDomaineValeurDao().getListe(liste);
    return liste;
  }

  public List getListeDefinitionDomaineValeur(FiltreDomaineValeur filtre) {
    return this.getDefinitionDomaineValeurDao().getListe(filtre);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur#
   * rechercherDomaineValeur
   * (org.sofiframework.sofi.composantweb.liste.ListeNavigation)
   */
  public ListeNavigation getListeDomaineValeur(ListeNavigation liste) {
    return getDao().getListe(liste);
  }

  public List getListeDomaineValeur(org.sofiframework.application.domainevaleur.filtre.FiltreDomaineValeur filtre) {
    return getDao().getListe(filtre);
  }

  public ListeNavigation getListeDomaineValeurPilotage(ListeNavigation liste, Boolean definition) {
    String codeUtilisateur = this.getGestionnaireUtilisateur().getUtilisateur().getCodeUtilisateur();

    if (definition) {
      this.getDefinitionDomaineValeurDao().getListeDefinitionDomaineValeurPilotage(liste, codeUtilisateur);
    } else {
      this.getDomaineValeurDao().getListeDomaineValeurPilotage(liste, codeUtilisateur);
    }

    return liste;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur#
   * sauvegarderDefinitionDomaineValeur
   * (org.sofiframework.infrasofi.modele.entite.DefinitionDomaineValeur,
   * org.sofiframework.infrasofi.modele.entite.DefinitionDomaineValeur)
   */
  public void enregistrerDefinitionDomaineValeur(DefinitionDomaineValeur definition, DefinitionDomaineValeur definitionOriginale) {
    Iterator<String> iterateur = GestionLibelle.getInstance().getListeLocale().iterator();
    while (iterateur.hasNext()) {
      DefinitionDomaineValeur nouvelDefinition = (DefinitionDomaineValeur)definition.clone();
      String codeLocale = iterateur.next();

      if (definition.getDateCreation() == null) {
        this.traceCreation(nouvelDefinition);
        nouvelDefinition.setCodeLangue(codeLocale);
        this.getDefinitionDomaineValeurDao().ajouter(nouvelDefinition);

      } else {

        DefinitionDomaineValeur definitionAModifie = getDefinitionDomaineValeur(definitionOriginale.getCodeApplication(), definitionOriginale.getNom(), codeLocale);
        if (definitionAModifie != null) {
          definitionAModifie.setCodeTypeTri(definition.getCodeTypeTri());
          definitionAModifie.setNom(definition.getNom());
          definitionAModifie.setDescription(definition.getDescription());
          this.traceModification(definitionAModifie);
          this.getDefinitionDomaineValeurDao().modifier(definitionAModifie);
        }
      }
    }
  }

  public void enregistrerDefinitionDomaineValeur(DefinitionDomaineValeur definition) {
    DefinitionDomaineValeur definitionOriginale = this.getDefinitionDomaineValeur(definition.getCodeApplication(),
        definition.getNom(), definition.getCodeLangue());
    ((BaseDaoImpl) this.getDefinitionDomaineValeurDao()).getHibernateTemplate().evict(definitionOriginale);

    if (definitionOriginale != null) {
      this.getDefinitionDomaineValeurDao().modifier(definition);
    } else {
      this.getDefinitionDomaineValeurDao().ajouter(definition);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur#
   * sauvegarderDomaineValeur
   * (org.sofiframework.infrasofi.modele.entite.DomaineValeur,
   * org.sofiframework.infrasofi.modele.entite.DomaineValeur)
   */
  public void enregistrerDomaineValeur(DomaineValeur domaine, DomaineValeur domaineOriginial) {
    // TODO on doit refaire la gestion du changement de la clé d'un domaine.
    if (domaine.getDateCreation() == null) {
      this.ajouterDomaineValeur(domaine);
    } else {
      /*
       * if (domaineOriginial != null &&
       * !domaine.getNom().equals(domaineOriginial.getNom())) {
       * this.modifierCleDomaineValeur(domaine, domaineOriginial); } else {
       */
      this.modifier(domaine);
      /* } */
    }
  }

  public void enregistrerDomaineValeur(DomaineValeur domaineValeur) {
    DomaineValeur domaineValeurOriginal = this.getDomaineValeur(domaineValeur.getCodeApplication(), domaineValeur.getNom(),
        domaineValeur.getValeur(), domaineValeur.getCodeLangue());
    ((BaseDaoImpl) this.getDomaineValeurDao()).getHibernateTemplate().evict(domaineValeurOriginal);
    if (domaineValeurOriginal != null) {
      this.modifier(domaineValeur);
    } else {
      this.getDao().ajouter(domaineValeur);
    }
  }

  @SuppressWarnings("unchecked")
  private void modifierCleDomaineValeur(DomaineValeur domaine, DomaineValeur domaineOriginial) {
    // Modifier la liste des domaines enfants avec la nouvelle clé parent
    List<DomaineValeur> listeEnfant = this.getListeDomaineValeur(domaineOriginial.getCodeApplication(), domaineOriginial.getNom(),
        domaineOriginial.getValeur(), domaineOriginial.getCodeLangue());
    for (DomaineValeur d : listeEnfant) {
      d.setCodeApplicationParent(domaine.getCodeApplication());
      d.setNomParent(domaine.getNom());
      d.setValeurParent(domaine.getValeur());
      d.setCodeLangueParent(domaine.getCodeLangue());
      this.modifier(d);
    }
  }

  @SuppressWarnings("unchecked")
  public void supprimerDefinitionDomaineValeur(DefinitionDomaineValeur definition) {

    Iterator<String> iterateur = GestionLibelle.getInstance().getListeLocale().iterator();
    while (iterateur.hasNext()){
      String codeLocale = iterateur.next();

      DefinitionDomaineValeur definitionASupprimer = getDefinitionDomaineValeur(definition.getCodeApplication(), definition.getNom(), codeLocale);
      if (definitionASupprimer != null) {
        definitionASupprimer.setCodeLangue(codeLocale);

        List<DomaineValeur> liste = this.getListeDomaineValeur(definitionASupprimer.getCodeApplication(), definitionASupprimer.getNom(), definitionASupprimer.getValeur(),
            definitionASupprimer.getCodeLangue());
        if (liste != null) {
          for (DomaineValeur valeur : liste) {
            this.supprimerDomaineValeur(valeur);
          }
        }
        this.getDefinitionDomaineValeurDao().supprimer(definitionASupprimer);

      }

    }

  }

  public void supprimerDomaineValeur(DomaineValeur domaine) {
    this.getDao().supprimer(domaine);
  }

  public void activerDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue) {
    // Méthode temporaire. Déterminer les besoins de cette fonctionnalité
    DomaineValeur domaineValeur = getDomaineValeur(codeApplication, nom, valeur, codeLangue);
    domaineValeur.setDateFin(null);
    this.enregistrerDomaineValeur(domaineValeur);
  }

  public void desactiverDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue) {
    DomaineValeur domaineValeur = getDomaineValeur(codeApplication, nom, valeur, codeLangue);
    domaineValeur.setDateFin(new Date());
    this.enregistrerDomaineValeur(domaineValeur);
  }

  public DefinitionDomaineValeurDao getDefinitionDomaineValeurDao() {
    return definitionDomaineValeurDao;
  }

  public DomaineValeurDao getDomaineValeurDao() {
    return (DomaineValeurDao) this.getDao();
  }

  public void setDefinitionDomaineValeurDao(DefinitionDomaineValeurDao definitionDomaineValeurDao) {
    this.definitionDomaineValeurDao = definitionDomaineValeurDao;
  }

  public ServiceParametreSysteme getServiceParametreSysteme() {
    return serviceParametreSysteme;
  }

  public void setServiceParametreSysteme(ServiceParametreSysteme serviceParametreSysteme) {
    this.serviceParametreSysteme = serviceParametreSysteme;
  }

  public List getListeDefinitionDomaineValeur(org.sofiframework.application.domainevaleur.filtre.FiltreDomaineValeur filtre) {
    return this.getDefinitionDomaineValeurDao().getListe(filtre);
  }
}
