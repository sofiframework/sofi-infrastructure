/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.domainevaleur.service.commun;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.sofiframework.application.domainevaleur.filtre.FiltreDomaineValeur;
import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.application.spring.domainevaleur.service.ServiceDomaineValeur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.composantweb.liste.ObjetFiltre;
import org.sofiframework.infrasofi.modele.entite.DefinitionDomaineValeur;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.spring.service.local.BaseServiceImpl;
import org.sofiframework.utilitaire.UtilitaireObjet;

/**
 * Service commun pour l'accès aux aides contextuelles par les applications.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceDomaineValeurImpl extends BaseServiceImpl implements ServiceDomaineValeur {

  private String langueParDefaut = "fr_CA";

  /*
   * Service local de domaine de valeur
   */
  private org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur serviceDomaineValeur = null;

  /*
   * (non-Javadoc)
   * 
   * @seeorg.sofiframework.sofi.application.domainevaleur.service. ServiceDomaineValeur#
   * getListeValeur(org.sofiframework.sofi.composantweb.liste.ListeNavigation)
   */
  public ListeNavigation getListeActif(ListeNavigation liste, String valeurSelectionnee) {
    // Convertir le filtre de la liste en filtre de l'InfraSofi
    liste.setObjetFiltre((ObjetFiltre) UtilitaireObjet.convertirObjet(liste.getObjetFiltre(),
        org.sofiframework.infrasofi.modele.filtre.FiltreDomaineValeur.class));

    // Appeler le service Local afin d'effectuer la recherche
    liste = getServiceDomaineValeur().getListeActif(liste, valeurSelectionnee);

    // Convertir le résultat en Domaine de valeur SOFI
    liste.setListe(convertirListeDomaineValeur(liste.getListe()));

    // Reconvertir le filtre en une valeur SOFI (au cas où)
    liste
    .setObjetFiltre((ObjetFiltre) UtilitaireObjet.convertirObjet(liste.getObjetFiltre(), FiltreDomaineValeur.class));

    // Retourner le résultat
    return liste;
  }

  /*
   * (non-Javadoc)
   * 
   * @seeorg.sofiframework.sofi.application.domainevaleur.service. ServiceDomaineValeur#
   * getDomaineValeur(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
   */
  public DomaineValeur getDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue) {
    return getDomaineValeur(codeApplication, nom, valeur, codeLangue, null);
  }

  public DomaineValeur getDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue,
      String codeClient) {
    DomaineValeur domaine = null;
    org.sofiframework.infrasofi.modele.entite.DomaineValeur d = this.serviceDomaineValeur.getDomaineValeur(
        codeApplication, nom, valeur, codeLangue);
    if (d != null) {
      domaine = convertirDomaineValeur(d);
    }
    return domaine;
  }

  /*
   * (non-Javadoc)
   * 
   * @seeorg.sofiframework.sofi.application.domainevaleur.service. ServiceDomaineValeur#
   * getListeValeur(org.sofiframework.sofi.composantweb.liste.ListeNavigation)
   */
  public ListeNavigation getListeValeur(ListeNavigation liste) {
    FiltreDomaineValeur filtre = (FiltreDomaineValeur) liste.getObjetFiltre();
    // Correction pour la recherche des domaines de valeur sans code de nom
    // spécifié
    if (filtre.getNom() == null) {
      filtre.setNom("DEFINITION");
    }
    liste = getServiceDomaineValeur().getListe(liste);
    liste.setListe(convertirListeDomaineValeur(liste.getListe()));
    return liste;
  }

  public ListeNavigation getListeDomaineValeurEnfant(ListeNavigation liste) {
    ListeNavigation result = null;
    FiltreDomaineValeur filtre = (FiltreDomaineValeur) liste.getObjetFiltre();

    if (filtre.getValeurParent() != null && !"".equals(filtre.getValeurParent())) {
      result = getListeValeur(liste);
    } else {
      result = getServiceDomaineValeur().getListeDomaineValeurEnfant(liste);
      liste.setListe(convertirListeDomaineValeur(liste.getListe()));
      liste.setObjetFiltre((ObjetFiltre) UtilitaireObjet.convertirObjet(liste.getObjetFiltre(),
          FiltreDomaineValeur.class));
    }

    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @seeorg.sofiframework.sofi.application.domainevaleur.service. ServiceDomaineValeur#
   * getListeValeur(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
   */
  public List getListeValeur(String codeApplicationParent, String nomParent, String valeurParent,
      String codeLangueParent, boolean valeurInactive) {

    List liste = getServiceDomaineValeur().getListeDomaineValeur(codeApplicationParent, nomParent, valeurParent,
        codeLangueParent, valeurInactive);

    for (int i = 0; i < liste.size(); i++) {
      org.sofiframework.infrasofi.modele.entite.DomaineValeur valeur = (org.sofiframework.infrasofi.modele.entite.DomaineValeur) liste
          .get(i);
      valeur.setChargementEnfant(Boolean.TRUE);
    }

    liste = convertirListeDomaineValeur(liste);

    if (codeLangueParent == null) {
      liste = convertirEnDomaineValeurParLangue(liste);
    }

    return liste;
  }

  public List getListeValeur(String codeApplicationParent, String nomParent, String valeurParent,
      String codeLangueParent) {
    return getListeValeur(codeApplicationParent, nomParent, valeurParent, codeLangueParent, false);
  }

  public List getListeValeur(String codeApplicationParent, String nomParent, String valeurParent,
      String listeValeurChargement, String codeLangueParent) {
    codeLangueParent = codeLangueParent == null ? langueParDefaut : codeLangueParent;
    List liste = this.getServiceDomaineValeur().getListeDomaineValeur(codeApplicationParent, nomParent, valeurParent,
        listeValeurChargement, codeLangueParent);
    liste = this.convertirListeDomaineValeur(liste);
    return liste;
  }

  public void activerDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue) {
    this.getServiceDomaineValeur().activerDomaineValeur(codeApplication, nom, valeur, codeLangue);
  }

  public void desactiverDomaineValeur(String codeApplication, String nom, String valeur, String codeLangue) {
    this.getServiceDomaineValeur().desactiverDomaineValeur(codeApplication, nom, valeur, codeLangue);
  }

  public void enregistrerDomaineValeur(DomaineValeur domaine, DomaineValeur domaineOriginial) {
    this.getServiceDomaineValeur().enregistrerDomaineValeur(this.convertirDomaineValeur(domaine),
        this.convertirDomaineValeur(domaineOriginial));
  }

  public void enregistrerDefinitionDomaineValeur(DomaineValeur definition) {
    this.getServiceDomaineValeur().enregistrerDefinitionDomaineValeur(
        (DefinitionDomaineValeur) UtilitaireObjet.convertirObjet(definition, DefinitionDomaineValeur.class));
  }

  public void ajouterDefinitionDomaineValeur(DomaineValeur definition) {
    this.getServiceDomaineValeur().ajouterDefinitionDomaineValeur(
        (DefinitionDomaineValeur) UtilitaireObjet.convertirObjet(definition, DefinitionDomaineValeur.class));
  }

  public void enregistrerDomaineValeur(DomaineValeur domaineValeur) {
    this.getServiceDomaineValeur().enregistrerDomaineValeur(
        (org.sofiframework.infrasofi.modele.entite.DomaineValeur) UtilitaireObjet.convertirObjet(domaineValeur,
            org.sofiframework.infrasofi.modele.entite.DomaineValeur.class));
  }

  public void ajouterDomaineValeur(DomaineValeur domaine) {
    this.getServiceDomaineValeur().ajouterDomaineValeur(
        (org.sofiframework.infrasofi.modele.entite.DomaineValeur) UtilitaireObjet.convertirObjet(domaine,
            org.sofiframework.infrasofi.modele.entite.DomaineValeur.class));
  }

  public void supprimerDomaineValeur(DomaineValeur domaine) {
    this.getServiceDomaineValeur().supprimerDomaineValeur(this.convertirDomaineValeur(domaine));
  }

  /**
   * Obtenir le service local de domaine de valeur.
   * 
   * @return service
   */
  public org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur getServiceDomaineValeur() {
    return serviceDomaineValeur;
  }

  /**
   * Fixer le service local de domaine de valeur.
   * 
   * @param serviceDomaineValeur
   *          service
   */
  public void setServiceDomaineValeur(
      org.sofiframework.infrasofi.modele.service.ServiceDomaineValeur serviceDomaineValeur) {
    this.serviceDomaineValeur = serviceDomaineValeur;
  }

  /**
   * Convertir un domaine de valeur (entité locale) en objet de transfert de SOFI.
   * 
   * @param d
   *          Entité locale de domaine de valeur
   * @return Objet de transfert de SOFI
   */
  private DomaineValeur convertirDomaineValeur(org.sofiframework.infrasofi.modele.entite.DomaineValeur d) {
    DomaineValeur domaine = null;
    try {
      domaine = new DomaineValeur();
      PropertyUtils.copyProperties(domaine, d);
    } catch (Exception e) {
      throw new ModeleException(e);
    }
    return domaine;
  }

  /**
   * Convertir un objet de transfert SOFI DomaineValeur en un domaine de valeur (entité locale).
   * 
   * @param d
   *          Entité locale de domaine de valeur
   * @return Objet de transfert de SOFI
   */
  private org.sofiframework.infrasofi.modele.entite.DomaineValeur convertirDomaineValeur(DomaineValeur d) {
    org.sofiframework.infrasofi.modele.entite.DomaineValeur domaine = null;
    try {
      domaine = new org.sofiframework.infrasofi.modele.entite.DomaineValeur();
      PropertyUtils.copyProperties(domaine, d);
    } catch (Exception e) {
      throw new ModeleException(e);
    }
    return domaine;
  }

  /**
   * Convertir une liste d'entité locale Domaine de Valeur en une liste d'objet de transfert de SOFI
   * 
   * @param listeDomaine
   *          une liste d'entité locale Domaine de Valeur
   * @return liste d'objet de transfert de SOFI
   */
  private List convertirListeDomaineValeur(List listeDomaine) {
    List listeConvertie = null;

    if (listeDomaine != null) {
      listeConvertie = new ArrayList();
      for (Iterator i = listeDomaine.iterator(); i.hasNext();) {
        org.sofiframework.infrasofi.modele.entite.DomaineValeur domaineLocal = (org.sofiframework.infrasofi.modele.entite.DomaineValeur) i
            .next();
        DomaineValeur domaineCommun = this.convertirDomaineValeur(domaineLocal);
        listeConvertie.add(domaineCommun);
        List listeDomaineValeurEnfant = domaineLocal.getListeDomaineValeurEnfants();
        if (domaineLocal.getChargementEnfant() && listeDomaineValeurEnfant != null) {
          List listeConvertieEnfant = this.convertirListeDomaineValeur(listeDomaineValeurEnfant);
          domaineCommun.setListeDomaineValeurEnfant((ArrayList) listeConvertieEnfant);
        }
      }
    }

    return listeConvertie;
  }

  /**
   * Convertir une liste de domaine de valeur avec Langue en une seule liste avec toutes les descriptions par langue
   * d'inclus.
   * 
   * @return une liste de domaine de valeur avec descriptions par langue inclut dans un domaine.
   * @param listeAvecLangue
   *          une liste de domaine de valeur avec descriptions séparé par langue.
   */
  @SuppressWarnings("unchecked")
  private List convertirEnDomaineValeurParLangue(List listeAvecLangue) {
    ArrayList liste = new ArrayList();
    HashMap dictionnairePosition = new HashMap();

    for (Iterator i = listeAvecLangue.iterator(); i.hasNext();) {
      DomaineValeur domaineValeur = (DomaineValeur) i.next();
      DomaineValeur domaineLangues = null;

      if (dictionnairePosition.containsKey(domaineValeur.getValeur())) {
        int position = ((Integer) dictionnairePosition.get(domaineValeur.getValeur())).intValue();
        domaineLangues = (DomaineValeur) liste.get(position);
      } else {
        domaineLangues = new DomaineValeur();
        try {
          PropertyUtils.copyProperties(domaineLangues, domaineValeur);
          domaineLangues.setDescription(null);
          domaineLangues.setCodeLangue(null);
        } catch (Exception e) {
          throw new ModeleException("Erreur pour créer le domaine de valeur a plusieurs langues.", e);
        }
        liste.add(domaineLangues);
        dictionnairePosition.put(domaineValeur.getValeur(), new Integer(liste.indexOf(domaineLangues)));
      }

      domaineLangues.ajouterDescriptionLangue(domaineValeur.getCodeLangue(), domaineValeur.getDescription());
      List listeDomaineValeurEnfant = domaineValeur.getListeDomaineValeurEnfant();
      if (listeDomaineValeurEnfant != null) {
        this.convertirEnDomaineValeurParLangue(listeDomaineValeurEnfant);
      }
    }

    return liste;
  }

  public List getListeDefinitionDomaineValeur(FiltreDomaineValeur filtre) {
    return UtilitaireObjet.convertirListeObjet(this.getServiceDomaineValeur().getListeDefinitionDomaineValeur(filtre),
        DomaineValeur.class);
  }

  public ListeNavigation getListeDefinitionDomaineValeur(ListeNavigation liste) {
    liste = this.getServiceDomaineValeur().getListeDefinitionDomaineValeur(liste);
    liste.setListe(UtilitaireObjet.convertirListeObjet(liste.getListe(), DomaineValeur.class));
    return liste;
  }

  public DomaineValeur getDefinitionDomaineValeur(String codeApplication, String nom, String codeLangue) {
    DefinitionDomaineValeur definition = this.getServiceDomaineValeur().getDefinitionDomaineValeur(codeApplication,
        nom, codeLangue);
    return (DomaineValeur) UtilitaireObjet.convertirObjet(definition, DomaineValeur.class);
  }

  public List getListeDomaineValeur(FiltreDomaineValeur filtre) {
    return UtilitaireObjet.convertirListeObjet(this.getServiceDomaineValeur().getListeDomaineValeur(filtre),
        DomaineValeur.class);
  }

  public void setLangueParDefaut(String langueParDefaut) {
    this.langueParDefaut = langueParDefaut;
  }

  public String getLangueParDefaut() {
    return langueParDefaut;
  }
}
