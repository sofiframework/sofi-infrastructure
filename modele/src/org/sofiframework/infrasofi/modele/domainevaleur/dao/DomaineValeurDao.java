/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.domainevaleur.dao;

import java.util.List;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.modele.spring.dao.BaseDao;

public interface DomaineValeurDao extends BaseDao {

  /**
   * Méthode qui permet de rechercher des domaines de valeurs actifs.
   * 
   * @param ln
   *          l'objet <code>ListeNavigation</code> à retourner avec le résultat
   *          de la requête.
   * @param actifSeulement
   *          valeur qui indique si on doit retourner la liste des domaines de
   *          valeurs qui sont actifs seulement
   * @param valeurSelectionnee
   *          parametre utilisé seulement quand la valeur actifSeulement est à
   *          false. Cette valeur permet de spécifier une valeur qui doit
   *          apparaître dans la requête même si cette valeur est inactive.
   * @return le même objet <code>ListeNavigation</code> contenant le résultat de
   *         la recherche.
   */
  ListeNavigation getListeActif(ListeNavigation ln, String valeurSelectionnee);

  /**
   * Obtenir une liste de valeurs de domaine.
   * 
   * @param codeApplicationParent
   *          Code de l'application
   * @param nomParent
   *          Nom du domaine parent
   * @param valeurParent
   *          Valeur parent.
   * @param codeLangueParent
   *          Code de langue parent
   * @param valeurInactive
   *          Si l'on désire les valeurs actives et inactives
   * @return Liste de domaine valeur
   */
  List getListe(String codeApplicationParent, String nomParent, String valeurParent, String codeLangueParent, boolean valeurInactive);

  /**
   * Obtenir la liste navigation des valeurs de domaine enfant.
   * @param liste Liste de navigation
   * @return Liste
   */
  ListeNavigation getListeDomaineValeurEnfant(ListeNavigation liste);

  ListeNavigation getListeDomaineValeurPilotage(ListeNavigation liste,
      String codeUtilisateur);

}