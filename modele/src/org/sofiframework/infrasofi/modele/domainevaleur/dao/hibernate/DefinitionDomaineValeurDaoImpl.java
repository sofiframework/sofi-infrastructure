/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.domainevaleur.dao.hibernate;

import java.util.Date;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseDaoInfrastructureImpl;
import org.sofiframework.infrasofi.modele.domainevaleur.dao.DefinitionDomaineValeurDao;
import org.sofiframework.infrasofi.modele.entite.DefinitionDomaineValeur;
import org.sofiframework.infrasofi.modele.filtre.FiltreDomaineValeur;
import org.sofiframework.modele.spring.dao.hibernate.CritereSupplementaire;

public class DefinitionDomaineValeurDaoImpl extends BaseDaoInfrastructureImpl implements DefinitionDomaineValeurDao {

  public DefinitionDomaineValeurDaoImpl() {
    super(DefinitionDomaineValeur.class);
  }

  /* (non-Javadoc)
   * @see org.sofiframework.infrasofi.modele.domainevaleur.dao.hibernate.DefinitionDomaineValeurDao#getListeDefinitionDomaineValeurPilotage(org.sofiframework.composantweb.liste.ListeNavigation, java.lang.String)
   */
  public ListeNavigation getListeDefinitionDomaineValeurPilotage(final ListeNavigation liste, String codeUtilisateur) {
    this.activerFiltreApplicationUtilisateurPilote(codeUtilisateur);
    FiltreDomaineValeur filtre = (FiltreDomaineValeur) liste.getFiltre();
    final Boolean actif = filtre.getActif();
    filtre.setActif(null);

    this.getListe(liste, new CritereSupplementaire() {
      @Override
      public void ajouter(DetachedCriteria criteres) {
        if (actif != null) {
          if (actif) {
            criteres.add(Restrictions.or(Restrictions.isNull("dateDebut"), Restrictions.le("dateDebut", new Date())));
            criteres.add(Restrictions.or(Restrictions.isNull("dateFin"), Restrictions.ge("dateFin", new Date())));
          } else {
            criteres.add(
                Restrictions.or(
                    Restrictions.and(Restrictions.isNotNull("dateDebut"), Restrictions.gt("dateDebut", new Date())),
                    Restrictions.and(Restrictions.isNotNull("dateFin"), Restrictions.lt("dateFin", new Date()))
                    )
                );
          }
        }
      }
    });

    filtre.setActif(actif);

    return liste;
  }
}