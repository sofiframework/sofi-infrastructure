package org.sofiframework.infrasofi.modele.domainevaleur.dao;

import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.modele.spring.dao.BaseDao;

public interface DefinitionDomaineValeurDao extends BaseDao {

  public abstract ListeNavigation getListeDefinitionDomaineValeurPilotage(
      ListeNavigation liste, String codeUtilisateur);

}