/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.domainevaleur.dao.hibernate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.sofiframework.application.parametresysteme.GestionParametreSysteme;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.BaseDaoInfrastructureImpl;
import org.sofiframework.infrasofi.modele.domainevaleur.dao.DomaineValeurDao;
import org.sofiframework.infrasofi.modele.domainevaleur.entite.hibernate.CritereDomaineValeurActif;
import org.sofiframework.infrasofi.modele.entite.DomaineValeur;
import org.sofiframework.infrasofi.modele.filtre.FiltreDomaineValeur;
import org.sofiframework.modele.spring.dao.hibernate.CritereSupplementaire;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Accès de données qui gère l'entité Domaine de valeur
 * 
 * @author Jean-Maxime Pelletier
 */
public class DomaineValeurDaoImpl extends BaseDaoInfrastructureImpl implements DomaineValeurDao {

  private static final Log log = LogFactory.getLog(DomaineValeurDaoImpl.class);

  public DomaineValeurDaoImpl() {
    super(DomaineValeur.class);
  }

  /**
   * Méthode qui permet de rechercher des domaines de valeurs actifs seulement.
   * 
   * @param ln
   *          l'objet <code>ListeNavigation</code> à retourner avec le résultat
   *          de la requête.
   * @param valeurSelectionnee
   *          Cette valeur permet de spécifier une valeur qui doit apparaître
   *          dans la requête même si cette valeur est inactive.
   * @return le même objet <code>ListeNavigation</code> contenant le résultat de
   *         la recherche.
   */
  public ListeNavigation getListeActif(ListeNavigation ln, String valeurSelectionnee) {
    CritereSupplementaire critereSupplementaire = null;

    // Modifie le filtre afin d'éviter la répétition de critères.
    FiltreDomaineValeur filtre = (FiltreDomaineValeur) ln.getObjetFiltre();
    filtre.ajouterAttributAExclure("codeApplication");
    filtre.ajouterAttributAExclure("codeLangue");
    filtre.ajouterAttributAExclure("nom");
    filtre.ajouterAttributAExclure("valeur");

    // Construire les critères supplémentaires et effectuer la recherche
    critereSupplementaire = new CritereDomaineValeurActif(filtre.getNom(), valeurSelectionnee, filtre.getCodeApplication(), filtre.getCodeLangue());
    ln = this.getListe(ln, critereSupplementaire);

    // Ré-activer les paramètres afin d'éviter des problèmes potentiels ailleurs
    filtre.supprimerAttributAExclure("codeApplication");
    filtre.supprimerAttributAExclure("codeLangue");
    filtre.supprimerAttributAExclure("nom");
    filtre.supprimerAttributAExclure("valeur");

    return ln;
  }

  public ListeNavigation getListeDomaineValeurEnfant(ListeNavigation liste) {
    CritereSupplementaire critereSupplementaire = new CritereSupplementaire() {

      @Override
      public void ajouter(DetachedCriteria criteres) {
        Criterion enleverParentDefinition = Restrictions.ne("valeurParent", DomaineValeur.NOM_RACINE_DOMAINE_VALEUR);
        criteres.add(enleverParentDefinition);
      }
    };

    return this.getListe(liste, critereSupplementaire);
  }

  @Override
  public Object get(Serializable id) {

    DomaineValeur domaineId = (DomaineValeur) id;

    List<String> params = new ArrayList<String>();
    List<String> nomParams = new ArrayList<String>();
    StringBuffer hql = new StringBuffer();

    String codeApplication = domaineId.getCodeApplication() + ",";
    String codeApplicationCommun = GestionParametreSysteme.getInstance().getService().getValeur(codeApplication, "codeApplicationCommun");
    if (codeApplicationCommun != null) {
      codeApplication = domaineId.getCodeApplication() + "," + codeApplicationCommun;
    }

    hql.append("FROM DomaineValeur d WHERE d.codeApplication in (");

    if (codeApplication.contains(",")) {
      for (String code : codeApplication.split(",")) {
        hql.append("'").append(code.trim()).append("'").append(",");
      }
      hql.delete(hql.length() - 1, hql.length());
    }
    hql.append(") ");

    String codeLangue = domaineId.getCodeLangue();

    if (codeLangue != null) {
      hql.append("AND d.codeLangue = :codeLangue ");
      nomParams.add("codeLangue");
      params.add(codeLangue);
    }

    String nom = domaineId.getNom();
    if (nom != null) {
      hql.append("AND upper(d.nom) = upper(:nom) ");
      nomParams.add("nom");
      params.add(nom);
    }

    String valeur = domaineId.getValeur();
    if (valeur != null) {
      hql.append("AND upper(d.valeur) = upper(:valeur) ");
      nomParams.add("valeur");
      params.add(valeur);
    }

    List liste = getHibernateTemplate().findByNamedParam(hql.toString(), nomParams.toArray(new String[nomParams.size()]), params.toArray());

    return liste != null && liste.size() > 0 ? liste.get(0) : null;
  }

  /**
   * 
   * @param codeApplicationParent
   * @param nomParent
   * @param valeurParent
   * @param codeLangueParent
   */
  @SuppressWarnings("unchecked")
  public List getListe(String codeApplicationParent, String nomParent, String valeurParent, String codeLangueParent, boolean valeurInactive) {
    List<Object> params = new ArrayList<Object>();
    List<String> nomParams = new ArrayList<String>();
    StringBuffer hql = new StringBuffer();

    String codeApplicationCommun = GestionParametreSysteme.getInstance().getService().getValeur(codeApplicationParent, "codeApplicationCommun");
    codeApplicationParent = codeApplicationParent + ",";
    if (codeApplicationCommun != null) {
      codeApplicationParent += codeApplicationCommun;
    }

    hql.append("FROM DomaineValeur d WHERE d.codeApplication in (");

    if (codeApplicationParent.contains(",")) {

      for (String codeApplication : codeApplicationParent.split(",")) {
        hql.append("'").append(codeApplication.trim()).append("'").append(",");
      }
      hql.delete(hql.length() - 1, hql.length());

    }
    hql.append(")");

    if (codeLangueParent != null) {
      hql.append("AND d.codeLangue = :codeLangueParent ");
      nomParams.add("codeLangueParent");
      params.add(codeLangueParent);
    }

    if (!UtilitaireString.isVide(nomParent)) {
      hql.append("AND d.nomParent = :nomParent ");
      nomParams.add("nomParent");
      params.add(nomParent);
    }

    if (valeurParent == null) {
      hql.append("AND d.nomParent = :nomParent ");
      nomParams.add("nomParent");
      params.add(nomParent);
      hql.append("AND d.valeurParent = :valeurParent ");
      nomParams.add("valeurParent");
      params.add(DomaineValeur.NOM_RACINE_DOMAINE_VALEUR);
    } else {
      hql.append("AND d.nomParent = :nomParent ");
      nomParams.add("nomParent");
      params.add(nomParent);
      hql.append("AND d.valeurParent = :valeurParent ");
      nomParams.add("valeurParent");
      params.add(valeurParent);
    }

    if (!valeurInactive) {
      hql.append(" and (d.dateDebut is null or d.dateDebut <= :dateCourante) and (d.dateFin is null or d.dateFin >= :dateCourante) ");
      nomParams.add("dateCourante");
      params.add(new Date());
    }

    hql.append(" order by ordreAffichage asc, description asc");

    List liste = getHibernateTemplate().findByNamedParam(hql.toString(), nomParams.toArray(new String[nomParams.size()]), params.toArray());
    return liste;
  }

  public ListeNavigation getListeDomaineValeurPilotage(ListeNavigation liste, String codeUtilisateur) {
    this.activerFiltreApplicationUtilisateurPilote(codeUtilisateur);
    FiltreDomaineValeur filtre = (FiltreDomaineValeur) liste.getFiltre();
    final Boolean actif = filtre.getActif();
    filtre.setActif(null);

    this.getListe(liste, new CritereSupplementaire() {

      @Override
      public void ajouter(DetachedCriteria criteres) {
        if (actif != null) {
          if (actif) {
            criteres.add(Restrictions.or(Restrictions.isNull("dateDebut"), Restrictions.le("dateDebut", new Date())));
            criteres.add(Restrictions.or(Restrictions.isNull("dateFin"), Restrictions.ge("dateFin", new Date())));
          } else {
            criteres.add(
                Restrictions.or(
                    Restrictions.and(Restrictions.isNotNull("dateDebut"), Restrictions.gt("dateDebut", new Date())),
                    Restrictions.and(Restrictions.isNotNull("dateFin"), Restrictions.lt("dateFin", new Date()))
                    )
                );
          }
        }
      }
    });

    filtre.setActif(actif);

    return liste;
  }
}
