/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.domainevaleur.entite.hibernate;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.sofiframework.modele.spring.dao.hibernate.CritereSupplementaire;
import org.sofiframework.utilitaire.UtilitaireString;

/**
 * Critère de recherche permettant de rechercher seulement les valeurs de
 * domaine de valeur qui sont active.
 * <p>
 * Dans le cas d'une recherche des données actives, il faut aussi additionner
 * une valeur bien précise dans la liste qui correspond à une donnée
 * potentiellement déjà sélectionnée.
 * 
 * @author Pierre-Frédérick Duret, Nurun inc.
 */
public class CritereDomaineValeurActif extends CritereSupplementaire {

  private static final Log log = LogFactory.getLog(CritereDomaineValeurActif.class);

  /** la valeur sélectionnée dans la liste de critère */
  private String valeur;

  /** le nom du domaine de valeur sélectionné */
  private String nom;

  /** le code d'application sélectionné */
  private String application;

  /** le code de langue sélectionné */
  private String langue;

  /** Constructeur par défaut */
  public CritereDomaineValeurActif(String nomSelectionne, String valeurSelectionnee, String codeApplication, String codeLangue) {
    this.setValeur(valeurSelectionnee);
    this.setNom(nomSelectionne);
    this.setApplication(codeApplication);
    this.setLangue(codeLangue);
  }

  /**
   * Méthode qui crée et ajoute des critères supplémentaires à un objet qui
   * représente la requête Hibernate.
   * 
   * @param criteres
   *          un <code>DetachedCriteria</code> qui contient la spécification de
   *          tous les critères de la requête.
   */
  @Override
  public void ajouter(DetachedCriteria criteres) {
    // Construction du critère "DATE_FIN >= SYSDATE"
    Criterion actif = Restrictions.and(Restrictions.or(Restrictions.isNull("dateDebut"), Restrictions.le("dateDebut", new Date())),
        Restrictions.or(Restrictions.isNull("dateFin"), Restrictions.ge("dateFin", new Date())));

    // Construction du critère
    // "VALEUR = 'valeur' AND NOM = 'nom' AND CODE_APPLICATION = 'application' AND CODE_LOCALE = 'langue'"
    Criterion selection = Restrictions.and(Restrictions.eq("codeLangue", this.getLangue()),
        Restrictions.and(Restrictions.eq("codeApplication", this.getApplication()), Restrictions.eq("nom", this.getNom())));

    // Faire un AND de ces critères
    Criterion baseCritere = Restrictions.and(actif, selection);

    // Quand une valeur sélectionnée est spécifiée
    if (!UtilitaireString.isVide(this.valeur)) {
      // Construction du critère "VALEUR = valeurSelectionnee"
      Criterion valeurSelectionnee = Restrictions.and(selection, Restrictions.eq("valeur", this.getValeur()));

      // Faire un AND du critère précédent et de
      // "VALEUR = 'valeur' AND NOM = 'nom' AND CODE_APPLICATION = 'application' AND CODE_LOCALE = 'langue'"
      Criterion baseSelection = Restrictions.and(valeurSelectionnee, selection);

      // Faire un OR des deux critères précédents
      Criterion critere = Restrictions.or(baseCritere, baseSelection);
      criteres.add(critere);
    } else {
      criteres.add(baseCritere);
    }
    log.info(criteres.toString());
  }

  /**
   * Obtenir la valeur sélectionnée dans la liste de critère.
   * 
   * @return la valeur sélectionnée dans la liste de critère.
   */
  public String getValeur() {
    return valeur;
  }

  /**
   * Fixer la valeur sélectionnée dans la liste de critère.
   * 
   * @param valeur
   *          la valeur sélectionnée dans la liste de critère.
   */
  public void setValeur(String valeur) {
    this.valeur = valeur;
  }

  /**
   * Obtenir le nom du domaine de valeur sélectionné.
   * 
   * @return le nom du domaine de valeur sélectionné.
   */
  public String getNom() {
    return nom;
  }

  /**
   * Fixer le nom du domaine de valeur sélectionné.
   * 
   * @param nom
   *          le nom du domaine de valeur sélectionné.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }

  /**
   * Obtenir le code d'application sélectionné.
   * 
   * @return le code d'application sélectionné.
   */
  public String getApplication() {
    return application;
  }

  /**
   * Fixer le code d'application sélectionné.
   * 
   * @param application
   *          le code d'application sélectionné.
   */
  public void setApplication(String application) {
    this.application = application;
  }

  /**
   * Obtenir le code de langue sélectionné.
   * 
   * @return le code de langue sélectionné.
   */
  public String getLangue() {
    return langue;
  }

  /**
   * Fixer le code de langue sélectionné.
   * 
   * @param langue
   *          le code de langue sélectionné
   */
  public void setLangue(String langue) {
    this.langue = langue;
  }
}
