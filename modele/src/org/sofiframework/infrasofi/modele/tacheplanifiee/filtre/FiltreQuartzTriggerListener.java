/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class FiltreQuartzTriggerListener extends ObjetFiltre {

  private static final long serialVersionUID = 8754667390193703402L;

  private Long declencheurId;
  private String ecouteurTrigger;
  // Filtre sur le parent
  private FiltreQuartzTrigger trigger;

  public FiltreQuartzTriggerListener(Long schedulerId) {
    this.trigger = new FiltreQuartzTrigger(schedulerId);
  }

  public String getEcouteurTrigger() {
    return ecouteurTrigger;
  }

  public void setEcouteurTrigger(String ecouteurTrigger) {
    this.ecouteurTrigger = ecouteurTrigger;
  }

  public Long getDeclencheurId() {
    return declencheurId;
  }

  public void setDeclencheurId(Long declencheurId) {
    this.declencheurId = declencheurId;
  }

  public FiltreQuartzTrigger getTrigger() {
    return trigger;
  }

  public void setTrigger(FiltreQuartzTrigger trigger) {
    this.trigger = trigger;
  }

}
