/**
 * 
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * @author rmercier
 *
 */
public class FiltreTypeTrigger extends ObjetFiltre {

  /**
   * 
   */
  private static final long serialVersionUID = 987417598214442908L;

  private Long declencheurId;

  public Long getDeclencheurId() {
    return declencheurId;
  }

  public void setDeclencheurId(Long declencheurId) {
    this.declencheurId = declencheurId;
  }
}
