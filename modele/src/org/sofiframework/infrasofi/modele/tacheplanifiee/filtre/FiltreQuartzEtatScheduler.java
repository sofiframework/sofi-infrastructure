/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class FiltreQuartzEtatScheduler extends ObjetFiltre{

  private static final long serialVersionUID = -1672083044414020015L;

  private long schedulerId;
  private String nom;
  private Long derniereValidation,intervalValidation;

  public long getSchedulerId() {
    return schedulerId;
  }

  public void setSchedulerId(long schedulerId) {
    this.schedulerId = schedulerId;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public Long getDerniereValidation() {
    return derniereValidation;
  }

  public void setDerniereValidation(Long derniereValidation) {
    this.derniereValidation = derniereValidation;
  }

  public Long getIntervalValidation() {
    return intervalValidation;
  }

  public void setIntervalValidation(Long intervalValidation) {
    this.intervalValidation = intervalValidation;
  }

}