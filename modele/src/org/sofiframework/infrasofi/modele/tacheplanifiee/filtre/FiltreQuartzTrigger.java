/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class FiltreQuartzTrigger extends ObjetFiltre {

  private static final long serialVersionUID = -2344334606861579822L;

  private Long jobId;
  private String nom,groupe,etat;
  private String nomCalendrier;
  private Boolean triggerVolatile;
  private Long prochaineExecution;
  // Filtre sur le parent
  private FiltreQuartzJob job;

  public FiltreQuartzTrigger() {
  }

  public FiltreQuartzTrigger(Long schedulerId) {
    this.job = new FiltreQuartzJob();
    this.getJob().setSchedulerId(schedulerId);
  }

  public FiltreQuartzTrigger(Long schedulerId, String nomDeclencheur, String groupeDeclencheur) {
    this(schedulerId);
    this.nom = nomDeclencheur;
    this.groupe = groupeDeclencheur;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getGroupe() {
    return groupe;
  }

  public void setGroupe(String groupe) {
    this.groupe = groupe;
  }

  public String getEtat() {
    return etat;
  }

  public void setEtat(String etat) {
    this.etat = etat;
  }

  public String getNomCalendrier() {
    return nomCalendrier;
  }

  public void setNomCalendrier(String nomCalendrier) {
    this.nomCalendrier = nomCalendrier;
  }

  public Long getProchaineExecution() {
    return prochaineExecution;
  }

  public void setProchaineExecution(Long prochaineExecution) {
    this.prochaineExecution = prochaineExecution;
  }

  public Boolean getTriggerVolatile() {
    return triggerVolatile;
  }

  public void setTriggerVolatile(Boolean triggerVolatile) {
    this.triggerVolatile = triggerVolatile;
  }

  public FiltreQuartzJob getJob() {
    return job;
  }

  public void setJob(FiltreQuartzJob job) {
    this.job = job;
  }

  public Long getJobId() {
    return jobId;
  }

  public void setJobId(Long jobId) {
    this.jobId = jobId;
  }

}
