/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class FiltreQuartzFiredTrigger extends ObjetFiltre {

  private static final long serialVersionUID = -5734878292539653903L;

  private String id;
  private String entreeId;
  private Boolean relance;
  private String nomScheduler;
  private Boolean triggerVolatile;
  // Filtre sur le parent
  private FiltreQuartzTrigger trigger;

  public FiltreQuartzFiredTrigger(Long schedulerId) {
    this.trigger = new FiltreQuartzTrigger(schedulerId);
  }

  public Boolean getRelance() {
    return relance;
  }

  public void setRelance(Boolean relance) {
    this.relance = relance;
  }

  public String getNomScheduler() {
    return nomScheduler;
  }

  public void setNomScheduler(String nomScheduler) {
    this.nomScheduler = nomScheduler;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Boolean getTriggerVolatile() {
    return triggerVolatile;
  }

  public void setTriggerVolatile(Boolean triggerVolatile) {
    this.triggerVolatile = triggerVolatile;
  }

  public FiltreQuartzTrigger getTrigger() {
    return trigger;
  }

  public void setTrigger(FiltreQuartzTrigger trigger) {
    this.trigger = trigger;
  }

  public String getEntreeId() {
    return entreeId;
  }

  public void setEntreeId(String entreeId) {
    this.entreeId = entreeId;
  }
}
