/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.filtre;

import org.sofiframework.composantweb.liste.ObjetFiltre;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
// TODO : renommer la classe en FiltreQuartzJobDetail
public class FiltreQuartzJob extends ObjetFiltre {

  /**
   * 
   */
  private static final long serialVersionUID = -4537923577778589615L;

  private Long schedulerId;
  private String groupe;
  private String nom;
  private Boolean jobDurable,jobVolatile,jobStateful,jobRequestRecovery;
  // Filtre parent
  private FiltreQuartzScheduler scheduler;

  public FiltreQuartzJob() {
    this.scheduler = new FiltreQuartzScheduler();
  }

  public FiltreQuartzJob(Long schedulerId, String jobName, String groupName) {
    this();
    this.schedulerId = schedulerId;
    this.groupe = groupName;
    this.nom = jobName;
  }

  public String getGroupe() {
    return groupe;
  }

  public void setGroupe(String groupe) {
    this.groupe = groupe;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public Boolean getJobDurable() {
    return jobDurable;
  }

  public void setJobDurable(Boolean jobDurable) {
    this.jobDurable = jobDurable;
  }

  public Boolean getJobVolatile() {
    return jobVolatile;
  }

  public void setJobVolatile(Boolean jobVolatile) {
    this.jobVolatile = jobVolatile;
  }

  public Boolean getJobStateful() {
    return jobStateful;
  }

  public void setJobStateful(Boolean jobStateful) {
    this.jobStateful = jobStateful;
  }

  public Boolean getJobRequestRecovery() {
    return jobRequestRecovery;
  }

  public void setJobRequestRecovery(Boolean jobRequestRecovery) {
    this.jobRequestRecovery = jobRequestRecovery;
  }

  public Long getSchedulerId() {
    return schedulerId;
  }

  public void setSchedulerId(Long schedulerId) {
    this.schedulerId = schedulerId;
  }

  public FiltreQuartzScheduler getScheduler() {
    return scheduler;
  }

  public void setScheduler(FiltreQuartzScheduler scheduler) {
    this.scheduler = scheduler;
  }

}
