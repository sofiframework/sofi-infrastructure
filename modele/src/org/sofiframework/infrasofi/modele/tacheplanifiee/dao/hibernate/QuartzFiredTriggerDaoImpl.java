/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.dao.hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.sofiframework.infrasofi.modele.entite.QuartzFiredTrigger;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzFiredTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzFiredTrigger;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.springframework.orm.hibernate3.HibernateCallback;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class QuartzFiredTriggerDaoImpl extends BaseDaoImpl implements QuartzFiredTriggerDao {

  public QuartzFiredTriggerDaoImpl() {
    super(QuartzFiredTrigger.class,"QuartzFiredTrigger");
  }

  public int deleteFiredTrigger(Long schedulerId){
    return deleteFiredTrigger(schedulerId,null);
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public int deleteFiredTrigger(final Long schedulerId,final String nomScheduler) {
    int resultat = 0;
    FiltreQuartzFiredTrigger filtre = new FiltreQuartzFiredTrigger(schedulerId);
    filtre.setNomScheduler(nomScheduler);
    final List<QuartzFiredTrigger> liste = this.getListe(filtre);

    if(liste != null && liste.size() > 0) {
      resultat = (Integer)this.getHibernateTemplate().execute(new HibernateCallback() {
        public Object doInHibernate(Session session) throws HibernateException, SQLException {
          List<Long> listeId = new ArrayList<Long>();
          for(QuartzFiredTrigger firedTrigger : liste) {
            listeId.add(firedTrigger.getId());
          }

          Query requete = session.createQuery("Delete from QuartzFiredTrigger where id in (:listeId)");
          requete.setParameterList("listeId", listeId);
          return requete.executeUpdate();
        }
      });
    }
    return resultat;
  }
}
