/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.dao.hibernate;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.sofiframework.composantweb.liste.Filtre;
import org.sofiframework.infrasofi.modele.entite.QuartzTrigger;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzTrigger;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.sofiframework.modele.spring.dao.hibernate.CritereSupplementaire;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class QuartzTriggerDaoImpl extends BaseDaoImpl implements QuartzTriggerDao{

  public QuartzTriggerDaoImpl() {
    super(QuartzTrigger.class,"QuartzTrigger");
  }

  public List<QuartzTrigger> getMissFiredTrigger(FiltreQuartzTrigger filtre,Long prochaineExecution){
    return getTrigger(filtre,prochaineExecution,null);
  }

  public List<QuartzTrigger> getMissFiredTriggerInStates(FiltreQuartzTrigger filtre,Long prochaineExecution,String[] etats){
    return getTrigger(filtre,prochaineExecution,etats);
  }

  public List<QuartzTrigger> getMissFiredTriggersInGroupInStates(FiltreQuartzTrigger filtre,Long prochaineExecution,String[] etats){
    return getTrigger(filtre, prochaineExecution,etats);
  }

  public List<QuartzTrigger>  getTriggerIfIsInStates(FiltreQuartzTrigger filtre,String[] etats){
    return getTrigger(filtre,null,etats);
  }

  @SuppressWarnings("unchecked")
  private List<QuartzTrigger> getTrigger(FiltreQuartzTrigger filtre,final Long prochaineExecution,final String[] etats){

    return getListe(filtre, new CritereSupplementaire() {
      @Override
      public void ajouter(DetachedCriteria criteres) {
        if(etats != null) {
          criteres.add(Restrictions.in("etat", etats));
        }

        if(prochaineExecution != null) {
          criteres.add(Restrictions.lt("prochaineExecution", prochaineExecution));
          criteres.addOrder(getOrder(true, "prochaineExecution"));
        }
      }
    });

  }

  @SuppressWarnings({"rawtypes" })
  public Long getNextFireTime(Filtre filtre){
    Long result = 0L;

    List temp = getListe(filtre, new CritereSupplementaire() {
      @Override
      public void ajouter(DetachedCriteria criteres) {
        criteres.add(Restrictions.ge("prochaineExecution", 0L));
        criteres.addOrder(getOrder(true, "prochaineExecution"));
      }
    });

    if(temp != null && temp.size() > 0) {
      result = ((QuartzTrigger)temp.get(0)).getProchaineExecution();
    }

    return result;
  }

  @SuppressWarnings({ "rawtypes" })
  protected List getListe(Filtre filtre, CritereSupplementaire critereSupplementaire) {
    DetachedCriteria criteria = this.creerCriteria();
    this.appliquerFiltre(criteria, filtre);

    if (critereSupplementaire != null) {
      critereSupplementaire.ajouter(criteria);
    }

    return getHibernateTemplate().findByCriteria(criteria);
  }

  @SuppressWarnings({ "rawtypes" })
  public List getListeTriggerToAquire(Filtre filtre, final Long prochaineExecMin,final Long prochaineExecMax){
    return getListe(filtre, new CritereSupplementaire() {
      @Override
      public void ajouter(DetachedCriteria criteres) {
        criteres.add(Restrictions.ge("prochaineExecution", prochaineExecMin));
        criteres.add(Restrictions.lt("prochaineExecution", prochaineExecMax));
        criteres.addOrder(getOrder(true, "prochaineExecution"));
        criteres.addOrder(getOrder(false, "priorite"));
      }
    });
  }
}