/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.dao.hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.sofiframework.infrasofi.modele.entite.QuartzJob;
import org.sofiframework.infrasofi.modele.entite.QuartzJobListener;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzJobListenerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzJobListener;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.springframework.orm.hibernate3.HibernateCallback;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class QuartzJobListenerDaoImpl extends BaseDaoImpl implements QuartzJobListenerDao{

  public QuartzJobListenerDaoImpl() {
    super(QuartzJobListener.class,"QuartzJobListener");
  }

  public int supprimer(QuartzJob quartzJob){
    return supprimer(quartzJob.getSchedulerId(),quartzJob.getNom(),quartzJob.getGroupe());
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public int supprimer(Long schedulerId, String jobName, String groupName) {
    int resultat = 0;
    FiltreQuartzJobListener filtre = new FiltreQuartzJobListener(schedulerId);
    filtre.getJob().setNom(jobName);
    filtre.getJob().setGroupe(groupName);
    final List<QuartzJobListener> liste = this.getListe(filtre);

    if(liste != null && liste.size() > 0) {
      resultat = (Integer)this.getHibernateTemplate().execute(new HibernateCallback() {
        public Object doInHibernate(Session session) throws HibernateException, SQLException {
          List<Long> listeId = new ArrayList<Long>();
          for(QuartzJobListener JobListener : liste) {
            listeId.add(JobListener.getId());
          }
          Query requete = session.createQuery("Delete from QuartzJobListener where id in (:listeId)");
          requete.setParameterList("listeId", listeId);
          return requete.executeUpdate();
        }
      });
    }
    return resultat;
  }
}