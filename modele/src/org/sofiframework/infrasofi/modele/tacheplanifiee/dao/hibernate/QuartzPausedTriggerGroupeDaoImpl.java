/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.dao.hibernate;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.sofiframework.infrasofi.modele.entite.QuartzPauseTriggerGroupe;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzPausedTriggerGroupeDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;
import org.springframework.orm.hibernate3.HibernateCallback;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class QuartzPausedTriggerGroupeDaoImpl extends BaseDaoImpl implements QuartzPausedTriggerGroupeDao {

  public QuartzPausedTriggerGroupeDaoImpl() {
    super(QuartzPauseTriggerGroupe.class, "QuartzPauseTriggerGroupe");
  }

  /*
   * (non-Javadoc)
   * @see com.nurun.infrasofi.modele.quartz.dao.PausedTriggerGroupeDao#deleteAllPausedTriggerGroups(java.lang.String, java.lang.String)
   */
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public int deleteAllPausedTriggerGroups(final Long schedulerId) {

    return (Integer)this.getHibernateTemplate().execute(new HibernateCallback() {
      public Object doInHibernate(Session session) throws HibernateException, SQLException {
        StringBuffer hql = new StringBuffer()
        .append(" Delete from QuartzPauseTriggerGroupe ")
        .append(" where schedulerId = :schedulerId");

        Query requete = session.createQuery(hql.toString());
        requete.setParameter("schedulerId", schedulerId);

        return requete.executeUpdate();
      }
    });
  }
}
