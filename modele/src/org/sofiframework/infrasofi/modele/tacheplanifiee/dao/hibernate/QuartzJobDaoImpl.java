/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.sofiframework.infrasofi.modele.entite.QuartzJob;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzJobDao;
import org.sofiframework.modele.spring.dao.hibernate.BaseDaoImpl;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class QuartzJobDaoImpl extends BaseDaoImpl implements QuartzJobDao{

  public QuartzJobDaoImpl() {
    super(QuartzJob.class,"QuartzJob");
  }

  @SuppressWarnings("unchecked")
  public List<QuartzJob> getJobsStatefullFromTriggerGroup(Long schedulerId,String groupName){

    String hql = " select QuartzJob from QuartzJob job,QuartzTrigger trigger" +
        " where job.id = trigger.jobId" +
        " and job.schedulerId = :schedulerId" +
        " and job.jobStateful = 1" +
        " and trigger.groupe = :triggerGroupe ";

    Query requete = this.getSession().createQuery(hql);
    requete.setParameter("schedulerId", schedulerId);
    requete.setParameter("triggerGroupe", groupName);

    return requete.list();
  }
}
