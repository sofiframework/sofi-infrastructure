/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.service.local;

import java.io.Serializable;
import java.util.List;

import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.QuartzJob;
import org.sofiframework.infrasofi.modele.entite.QuartzTrigger;
import org.sofiframework.infrasofi.modele.service.ServiceDeclencheur;
import org.sofiframework.infrasofi.modele.service.ServiceJob;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzJob;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzTrigger;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class ServiceJobImpl extends BaseServiceInfrastructureImpl implements ServiceJob{

  private ServiceDeclencheur serviceDeclencheur;

  public Long enregistrer(QuartzJob nouvJob,QuartzJob ancJob){
    Long jobId = null;

    // si c est un insert, on insere
    if(ancJob == null){
      jobId = (Long) this.ajouter(nouvJob);
    } else {
      // si on a un changement de nom ou de group, on doit proceder a un delete puis a un insert
      if(!ancJob.getNom().equals(nouvJob.getNom()) || !ancJob.getGroupe().equals(nouvJob.getGroupe())){
        this.ajouter(nouvJob);
        this.supprimer(ancJob.getId());
      } else {
        this.modifier(nouvJob);
      }
      jobId = nouvJob.getId();
    }

    return jobId;
  }

  @SuppressWarnings("unchecked")
  public List<QuartzJob> getListeJob(Long schedulerId) {
    FiltreQuartzJob filtre = new FiltreQuartzJob();
    filtre.setSchedulerId(schedulerId);
    return this.getDao().getListe(filtre);
  }

  /*
   * (non-Javadoc)
   * @see com.nurun.infrasofi.modele.service.ServiceJob#activerJob(java.io.Serializable)
   */
  public void activerJob(Serializable id) {
    List<QuartzTrigger> listeDeclencheur = this.getListeDeclencheur(id);
    if(listeDeclencheur != null) {
      for(QuartzTrigger declencheur : listeDeclencheur) {
        this.getServiceDeclencheur().activerDeclencheur(declencheur.getId());
      }
    }
  }

  /*
   * (non-Javadoc)
   * @see com.nurun.infrasofi.modele.service.ServiceJob#desactiverJob(java.io.Serializable)
   */
  public void desactiverJob(Serializable id) {
    List<QuartzTrigger> listeDeclencheur = this.getListeDeclencheur(id);
    if(listeDeclencheur != null) {
      for(QuartzTrigger declencheur : listeDeclencheur) {
        this.getServiceDeclencheur().desactiverDeclencheur(declencheur.getId());
      }
    }
  }

  /**
   * Utile pour récupérer la liste des déclencheurs de la job d'identifiant passé en paramètre
   * @param jobId un identifiant de Job
   * @return une liste d'objet de type {@link QuartzTrigger}
   */
  @SuppressWarnings("unchecked")
  private List<QuartzTrigger> getListeDeclencheur(Serializable jobId) {
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger();
    filtre.setJobId((Long) jobId);
    List<QuartzTrigger> listeDeclencheur = this.getServiceDeclencheur()
        .getListe(filtre);
    return listeDeclencheur;
  }

  /**
   * @return the serviceDeclencheur
   */
  public ServiceDeclencheur getServiceDeclencheur() {
    return serviceDeclencheur;
  }

  /**
   * @param serviceDeclencheur the serviceDeclencheur to set
   */
  public void setServiceDeclencheur(ServiceDeclencheur serviceDeclencheur) {
    this.serviceDeclencheur = serviceDeclencheur;
  }

}
