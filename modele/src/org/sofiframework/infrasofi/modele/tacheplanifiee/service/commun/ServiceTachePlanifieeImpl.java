/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.service.commun;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Calendar;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.jdbcjobstore.Constants;
import org.sofiframework.application.tacheplanifiee.service.ServiceTachePlanifiee;
import org.sofiframework.application.tacheplanifiee.utils.CleSerialisable;
import org.sofiframework.application.tacheplanifiee.utils.TriggerStatutSerialisable;
import org.sofiframework.infrasofi.modele.commun.Domaine;
import org.sofiframework.infrasofi.modele.commun.QuartzEntite;
import org.sofiframework.infrasofi.modele.entite.QuartzBlobTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzCalendrier;
import org.sofiframework.infrasofi.modele.entite.QuartzCronTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzEtatScheduler;
import org.sofiframework.infrasofi.modele.entite.QuartzFiredTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzJob;
import org.sofiframework.infrasofi.modele.entite.QuartzJobListener;
import org.sofiframework.infrasofi.modele.entite.QuartzPauseTriggerGroupe;
import org.sofiframework.infrasofi.modele.entite.QuartzScheduler;
import org.sofiframework.infrasofi.modele.entite.QuartzSimpleTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzTriggerListener;
import org.sofiframework.infrasofi.modele.entite.QuartzTypeTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzVerrou;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzBlobTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzCalendrierDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzCronTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzEtatSchedulerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzFiredTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzJobDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzJobListenerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzPausedTriggerGroupeDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzSchedulerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzSimpleTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzTriggerListenerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzVerrouDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzCalendrier;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzEtatScheduler;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzFiredTrigger;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzJob;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzJobListener;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzPauseTrigger;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzScheduler;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzTrigger;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzTriggerListener;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreTypeTrigger;
import org.sofiframework.modele.exception.ModeleException;
import org.sofiframework.modele.exception.OrdonnanceurNotFoundException;
import org.sofiframework.modele.spring.dao.BaseDao;
import org.sofiframework.modele.spring.service.local.BaseServiceImpl;
import org.sofiframework.utilitaire.UtilitaireObjet;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class ServiceTachePlanifieeImpl extends BaseServiceImpl implements ServiceTachePlanifiee {

  private static final Log log =
      LogFactory.getLog(ServiceTachePlanifieeImpl.class);

  private static String NOM = "nom";
  private static String GROUPE = "groupe";
  private QuartzSchedulerDao schedulerDao;
  private QuartzTriggerDao triggerDao;
  private QuartzFiredTriggerDao firedTriggerDao;
  private QuartzJobDao jobDao;
  private QuartzJobListenerDao jobListenerDao;
  private QuartzTriggerListenerDao triggerListenerDao;
  private QuartzSimpleTriggerDao simpleTriggerDao;
  private QuartzCronTriggerDao cronTriggerDao;
  private QuartzBlobTriggerDao blobTriggerDao;
  private QuartzPausedTriggerGroupeDao pausedTriggerGroupeDao;
  private QuartzCalendrierDao calendrierDao;
  private QuartzEtatSchedulerDao etatSchedulerDao;
  private QuartzVerrouDao verrouDao;

  public void demanderVerrou(Long schedulerId, String nomVerrou) {
    QuartzVerrou entite = new QuartzVerrou(schedulerId, nomVerrou);
    verrouDao.ajouter(entite);
  }

  public void relacherVerrou(Long schedulerId, String nomVerrou) {
    QuartzVerrou entite = new QuartzVerrou(schedulerId, nomVerrou);
    entite = (QuartzVerrou) verrouDao.get(entite);

    if(entite != null) {
      verrouDao.supprimer(entite);
    }
  }

  public void signalerOrdonnanceurDemarre(Long schedulerId, String nomHote) {
    String hebergePar = null;
    QuartzScheduler ordonnanceur = (QuartzScheduler) this.getSchedulerDao().get(schedulerId);
    if(Domaine.statutOrdonnanceur.arrete.getValeur().equals(ordonnanceur.getStatut())) {
      hebergePar = nomHote;
    }
    // L'ordonnanceur est déja dans le statut démarré
    else {
      // Le nom d'hote n'est pas déjà dans le champs hebergePar
      if(ordonnanceur.getHebergePar() != null
          && ordonnanceur.getHebergePar().indexOf(nomHote) == -1) {
        hebergePar = ordonnanceur.getHebergePar() + "," + nomHote;
      } else {
        hebergePar = nomHote;
      }
    }
    ordonnanceur.setStatut(Domaine.statutOrdonnanceur.demarre.getValeur());
    ordonnanceur.setHebergePar(hebergePar);
    this.getSchedulerDao().modifier(ordonnanceur);
  }

  public void signalerOrdonnanceurArrete(Long schedulerId, String nomHote) {
    String hebergePar = "";
    String statut = Domaine.statutOrdonnanceur.arrete.getValeur();
    QuartzScheduler ordonnanceur = (QuartzScheduler) this.getSchedulerDao().get(schedulerId);
    // L'ordonnanceur peut-être null s'il n'a pas été créé dans l'infra
    if(ordonnanceur != null) {
      if(Domaine.statutOrdonnanceur.demarre.getValeur().equals(ordonnanceur.getStatut())) {
        String[] listeHebergePar = ordonnanceur.getHebergePar().split(",");
        if(listeHebergePar.length > 1) {
          statut = Domaine.statutOrdonnanceur.demarre.getValeur();
          for(String s : listeHebergePar) {
            hebergePar += s + ",";
          }
          hebergePar = hebergePar.substring(hebergePar.length()-2, hebergePar.length()-1);
        }
        ordonnanceur.setStatut(statut);
        ordonnanceur.setHebergePar(hebergePar);
      }
      this.getSchedulerDao().modifier(ordonnanceur);
    }
  }

  public Boolean verifierOrdonnanceurStatutDemarre(Long schedulerId, String nomHote) {
    Boolean retour = Boolean.FALSE;
    QuartzScheduler ordonnanceur = (QuartzScheduler) this.getSchedulerDao().get(schedulerId);
    if(ordonnanceur != null) {
      if(Domaine.statutOrdonnanceur.demarre.getValeur().equals(ordonnanceur.getStatut())) {
        retour = Boolean.TRUE;
      }
    } else {
      throw new OrdonnanceurNotFoundException("Ordonnanceur ["+schedulerId+"] introuvable dans l'infrastructure SOFI.");
    }
    return retour;
  }

  @SuppressWarnings("unchecked")
  public Long getSchedulerId(String codeApplication, String codeScheduler) throws OrdonnanceurNotFoundException {
    Long schedulerId = null;
    FiltreQuartzScheduler filtre = new FiltreQuartzScheduler();
    filtre.setCodeApplication(codeApplication.toLowerCase());
    filtre.setCodeScheduler(codeScheduler.toLowerCase());

    List<QuartzScheduler> liste = this.getSchedulerDao().getListe(filtre);
    if(liste != null && liste.size() > 0) {
      schedulerId = liste.get(0).getId();
    } else {
      throw new OrdonnanceurNotFoundException("Ordonnanceur ["+codeApplication+","+codeScheduler+"] introuvable dans l'infrastructure SOFI.");
    }
    return schedulerId;
  }

  public Integer updateTriggerStatesFromOtherStates(Long schedulerId,
      String nouvState, String ancState1, String ancState2){
    Integer result = new Integer(0);
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    List<QuartzTrigger> liste = getTriggerDao().getTriggerIfIsInStates(filtre, new String[]{ancState1,ancState2});
    for(QuartzTrigger quartzTrigger:liste){
      quartzTrigger.setEtat(nouvState);
      getTriggerDao().modifier(quartzTrigger);
      result ++;
    }
    return result;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public CleSerialisable[] selectMisfiredTriggers(Long schedulerId, Long date){
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    List liste = getTriggerDao().getMissFiredTrigger(filtre,date);
    return getKeyArray(liste,NOM,GROUPE);
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public CleSerialisable[] selectTriggersInState(Long schedulerId, String etat){
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setEtat(etat);
    List liste = getTriggerDao().getListe(filtre);
    return getKeyArray(liste,NOM,GROUPE);
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public CleSerialisable[] selectMisfiredTriggersInState(Long schedulerId, String etat,Long date){
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    List liste = getTriggerDao().getMissFiredTriggerInStates(filtre,date,new String[]{etat});
    return getKeyArray(liste,NOM,GROUPE);
  }

  public List<CleSerialisable> selectMisfiredTriggersInStates(Long schedulerId, String etat1,
      String etat2, Long date){
    List<CleSerialisable> result = new ArrayList<CleSerialisable>();
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    List<QuartzTrigger> triggers = getTriggerDao().getMissFiredTriggerInStates(filtre,date,new String[]{etat1,etat2});

    if(triggers.size() > 0){
      for(QuartzTrigger trigger:triggers) {
        result.add(new CleSerialisable(trigger.getNom(), trigger.getGroupe()));
      }
    }

    return result;
  }

  public Integer countMisfiredTriggersInStates(Long schedulerId, String etat1, String etat2, Long date) throws SQLException {
    Integer result = new Integer(0);
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    List<QuartzTrigger> triggers = getTriggerDao().getMissFiredTriggerInStates(filtre,date,new String[]{etat1,etat2});

    if(triggers != null && triggers.size() > 0) {
      result = triggers.size();
    }

    return result;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public CleSerialisable[] selectMisfiredTriggersInGroupInState(Long schedulerId, String groupe, String etat, Long date){
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setGroupe(groupe);
    List triggers = getTriggerDao().getMissFiredTriggersInGroupInStates(filtre,date,new String[]{etat});
    return getKeyArray(triggers,NOM,GROUPE);
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public Trigger[] selectTriggersForRecoveringJobs(Long schedulerId, String instanceId,Boolean useProperties){
    FiltreQuartzFiredTrigger filtre = new FiltreQuartzFiredTrigger(schedulerId);
    filtre.setRelance(Boolean.TRUE);
    filtre.setNomScheduler(instanceId);

    List<QuartzFiredTrigger> triggers = getFiredTriggerDao().getListe(filtre);

    Long dumId = System.currentTimeMillis();
    ArrayList resultTriggers = new ArrayList();

    for(QuartzFiredTrigger firedTrigger:triggers){
      String jobName = firedTrigger.getTrigger().getJob().getNom(); //rs.getString(COL_JOB_NAME);
      String jobGroup = firedTrigger.getTrigger().getJob().getGroupe(); //rs.getString(COL_JOB_GROUP);
      String trigName = firedTrigger.getTrigger().getNom(); //rs.getString(COL_TRIGGER_NAME);
      String trigGroup = firedTrigger.getTrigger().getGroupe();//rs.getString(COL_TRIGGER_GROUP);
      Long firedTime = firedTrigger.getDebutTraitement();//rs.getLong(COL_FIRED_TIME);
      Integer priority = firedTrigger.getPriorite().intValue(); //rs.getInt(COL_PRIORITY);
      SimpleTrigger rcvryTrig = new SimpleTrigger("recover_"
          + instanceId + "_" + String.valueOf(dumId++),
          Scheduler.DEFAULT_RECOVERY_GROUP, new Date(firedTime));
      rcvryTrig.setJobName(jobName);
      rcvryTrig.setJobGroup(jobGroup);
      rcvryTrig.setPriority(priority);
      rcvryTrig.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);

      JobDataMap jd = selectTriggerJobDataMap(schedulerId, trigName, trigGroup, useProperties);
      jd.put(Scheduler.FAILED_JOB_ORIGINAL_TRIGGER_NAME, trigName);
      jd.put(Scheduler.FAILED_JOB_ORIGINAL_TRIGGER_GROUP, trigGroup);
      jd.put(Scheduler.FAILED_JOB_ORIGINAL_TRIGGER_FIRETIME_IN_MILLISECONDS, String.valueOf(firedTime));
      rcvryTrig.setJobDataMap(jd);

      resultTriggers.add(rcvryTrig);
    }

    Object[] oArr = resultTriggers.toArray();
    Trigger[] tArr = new Trigger[oArr.length];
    System.arraycopy(oArr, 0, tArr, 0, oArr.length);

    return tArr;
  }

  public Integer deleteFiredTriggers(Long schedulerId){
    return getFiredTriggerDao().deleteFiredTrigger(schedulerId);
  }

  public Integer deleteFiredTriggers(Long schedulerId, String instanceId){
    return getFiredTriggerDao().deleteFiredTrigger(schedulerId, instanceId);
  }

  //---------------------------------------------------------------------------
  // jobs
  //---------------------------------------------------------------------------
  public Integer insertJobDetail(Long schedulerId, JobDetail job,Boolean useProperties){
    QuartzJob quartzJob = new QuartzJob(schedulerId,job,useProperties);
    getJobDao().ajouter(quartzJob);

    String[] jobListeners = job.getJobListenerNames();
    for (Integer i = 0; jobListeners != null && i < jobListeners.length; i++){
      QuartzJobListener listener = new QuartzJobListener(quartzJob,jobListeners[i]);
      getJobListenerDao().ajouter(listener);
    }

    return 1;
  }

  public Integer updateJobDetail(Long schedulerId, JobDetail job,Boolean useProperties){

    QuartzJob quartzJob = this.getQuartzJob(schedulerId, job.getName(), job.getGroup());
    quartzJob.initialiserProrpietes(job, useProperties);
    getJobDao().modifier(quartzJob);

    getJobListenerDao().supprimer(quartzJob);
    String[] jobListeners = job.getJobListenerNames();
    for (Integer i = 0; jobListeners != null && i < jobListeners.length; i++) {
      QuartzJobListener listener = new QuartzJobListener(quartzJob,jobListeners[i]);
      getJobListenerDao().ajouter(listener);
    }

    return 1;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public CleSerialisable[] selectTriggerNamesForJob(Long schedulerId, String jobName,String groupName) {
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.getJob().setNom(jobName);
    filtre.getJob().setGroupe(groupName);
    List liste = getTriggerDao().getListe(filtre);
    return getKeyArray(liste,NOM,GROUPE);
  }

  public Integer deleteJobListeners(Long schedulerId, String jobName, String groupName){
    return getJobListenerDao().supprimer(schedulerId, jobName, groupName);
  }

  public Integer deleteJobDetail(Long schedulerId, String jobName, String groupName){
    Integer result = new Integer(0);
    QuartzJob job = this.getQuartzJob(schedulerId, jobName, groupName);
    getJobDao().supprimer(job.getId());
    result = 1;
    return result;
  }

  public Boolean isJobStateful(Long schedulerId, String jobName,String groupName){
    QuartzJob job = this.getQuartzJob(schedulerId, jobName, groupName);
    return job.getJobStateful();
  }

  public Boolean jobExists(Long schedulerId, String jobName, String groupName){
    QuartzJob job = this.getQuartzJob(schedulerId, jobName, groupName);
    return job != null;
  }

  public Integer updateJobData(Long schedulerId, JobDetail job,Boolean useProperties){
    return updateJobDetail(schedulerId, job,useProperties);
  }

  @SuppressWarnings("unchecked")
  public Integer insertJobListener(Long schedulerId, JobDetail job, String listener){
    FiltreQuartzJob filtre = new FiltreQuartzJob(schedulerId, job.getName(), job.getGroup());
    List<QuartzJob> liste= getJobDao().getListe(filtre);
    QuartzJobListener quartzJobListener = new QuartzJobListener(liste.get(0).getId(), listener);
    getJobListenerDao().ajouter(quartzJobListener);
    return 1;
  }

  @SuppressWarnings("unchecked")
  public String[] selectJobListeners(Long schedulerId, String jobName, String groupName) {

    List<String> resultat = new ArrayList<String>();

    FiltreQuartzJobListener filtre = new FiltreQuartzJobListener(schedulerId);
    filtre.getJob().setGroupe(groupName);
    filtre.getJob().setNom(jobName);
    List<QuartzJobListener> liste = this.getJobListenerDao().getListe(filtre);

    if(liste != null) {
      for(QuartzJobListener ecouteur : liste) {
        resultat.add(ecouteur.getEcouteurJob());
      }
    }

    return resultat.toArray(new String[resultat.size()]);
  }

  @SuppressWarnings("unchecked")
  public JobDetail selectJobDetail(Long schedulerId, String jobName, String groupName, Boolean useProperties) {
    QuartzJob job= this.getQuartzJob(schedulerId, jobName, groupName);
    return (JobDetail) job.getQuartzObject(useProperties);
  }

  @SuppressWarnings("unchecked")
  public Integer selectNumJobs(Long schedulerId) {
    Integer compte = 0;

    FiltreQuartzJob filtre = new FiltreQuartzJob();
    filtre.setSchedulerId(schedulerId);
    List<QuartzJob> liste = this.getJobDao().getListe(filtre);
    if(liste != null) {
      compte = liste.size();
    }
    return compte;
  }

  @SuppressWarnings("unchecked")
  public String[] selectJobGroups(Long schedulerId) {

    Set<String> resultat = new HashSet<String>();

    FiltreQuartzJob filtre = new FiltreQuartzJob();
    filtre.setSchedulerId(schedulerId);
    List<QuartzJob> liste = this.getJobDao().getListe(filtre);
    if(liste != null) {
      for(QuartzJob job : liste) {
        resultat.add(job.getGroupe());
      }
    }
    return resultat.toArray(new String[resultat.size()]);
  }

  @SuppressWarnings("unchecked")
  public String[] selectJobsInGroup(Long schedulerId, String groupName) {

    List<String> resultat = new ArrayList<String>();

    FiltreQuartzJob filtre = new FiltreQuartzJob(schedulerId, null, groupName);
    List<QuartzJob> liste = this.getJobDao().getListe(filtre);
    if(liste != null) {
      for(QuartzJob job : liste) {
        resultat.add(job.getNom());
      }
    }

    return resultat.toArray(new String[resultat.size()]);
  }

  //---------------------------------------------------------------------------
  // triggers
  //---------------------------------------------------------------------------

  public Integer insertTrigger(Long schedulerId, Trigger trigger, String state,JobDetail jobDetail) {
    QuartzJob job = this.getQuartzJob(schedulerId, jobDetail.getName(), jobDetail.getGroup());
    QuartzTrigger entite = new QuartzTrigger(job.getId(), trigger);
    entite.setEtat(state);
    Long quartzTriggerId = (Long) this.getTriggerDao().ajouter(entite);

    String[] trigListeners = trigger.getTriggerListenerNames();
    for (Integer i = 0; trigListeners != null && i < trigListeners.length; i++) {
      QuartzTriggerListener triggerListener = new QuartzTriggerListener(quartzTriggerId, trigListeners[i]);
      this.getTriggerListenerDao().ajouter(triggerListener);
    }

    return 1;
  }

  public Integer insertSimpleTrigger(Long schedulerId, SimpleTrigger trigger) {
    QuartzTrigger entiteDeclencheur = getQuartzTrigger(schedulerId, trigger.getName(), trigger.getGroup());
    QuartzSimpleTrigger entite = new QuartzSimpleTrigger(entiteDeclencheur.getId(), trigger);
    this.getSimpleTriggerDao().ajouter(entite);
    return 1;
  }

  public Integer insertCronTrigger(Long schedulerId, CronTrigger trigger) {
    QuartzTrigger entiteDeclencheur = getQuartzTrigger(schedulerId, trigger.getName(), trigger.getGroup());
    QuartzCronTrigger entite = new QuartzCronTrigger(entiteDeclencheur.getId(), trigger);
    this.getCronTriggerDao().ajouter(entite);
    return 1;
  }

  public Integer insertBlobTrigger(Long schedulerId, Trigger trigger){
    QuartzTrigger entiteDeclencheur = getQuartzTrigger(schedulerId, trigger.getName(), trigger.getGroup());
    QuartzBlobTrigger entite = new QuartzBlobTrigger(entiteDeclencheur.getId(), trigger);
    this.getBlobTriggerDao().ajouter(entite);
    return 1;
  }

  public Integer updateTrigger(Long schedulerId, Trigger trigger, String etat, JobDetail jobDetail) {
    QuartzTrigger entiteDeclencheur = getQuartzTrigger(schedulerId, trigger.getName(), trigger.getGroup());
    entiteDeclencheur.initialiserProprietes(trigger);
    entiteDeclencheur.setEtat(etat);
    this.getTriggerDao().modifier(entiteDeclencheur);

    deleteTriggerListeners(schedulerId, trigger.getName(), trigger.getGroup());
    String[] trigListeners = trigger.getTriggerListenerNames();
    for (Integer i = 0; trigListeners != null && i < trigListeners.length; i++) {
      QuartzTriggerListener triggerListener = new QuartzTriggerListener(entiteDeclencheur.getId(), trigListeners[i]);
      this.getTriggerListenerDao().ajouter(triggerListener);
    }
    return 1;
  }

  public Integer updateSimpleTrigger(Long schedulerId, SimpleTrigger trigger){
    QuartzSimpleTrigger entite = (QuartzSimpleTrigger) this.getQuartzTypeTrigger(schedulerId, trigger.getName(), trigger.getGroup(), Constants.TTYPE_SIMPLE);
    entite.setNombreExecution(new Long(trigger.getRepeatCount()));
    entite.setInterval(trigger.getRepeatInterval());
    entite.setNombreFoisExecute(new Long(trigger.getTimesTriggered()));
    this.getSimpleTriggerDao().modifier(entite);
    return 1;
  }

  public Integer updateCronTrigger(Long schedulerId, CronTrigger trigger){
    QuartzCronTrigger entite = (QuartzCronTrigger) this.getQuartzTypeTrigger(schedulerId, trigger.getName(), trigger.getGroup(), Constants.TTYPE_CRON);
    entite.setExpression(trigger.getCronExpression());
    entite.setTimeZone(trigger.getTimeZone().getID());
    this.getCronTriggerDao().modifier(entite);
    return 1;
  }

  public Integer updateBlobTrigger(Long schedulerId, Trigger trigger){
    QuartzBlobTrigger entite = (QuartzBlobTrigger) this.getQuartzTypeTrigger(schedulerId, trigger.getName(), trigger.getGroup(), Constants.TTYPE_BLOB);
    // On se sert du constructeur pour sérialiser le trigger
    QuartzBlobTrigger temp = new QuartzBlobTrigger(schedulerId, trigger);
    entite.setTriggerData(temp.getTriggerData());
    this.getBlobTriggerDao().modifier(entite);
    return 1;
  }

  public Boolean triggerExists(Long schedulerId, String triggerName, String groupName,Boolean useProperties){
    Trigger trigger = selectTrigger(schedulerId, triggerName, groupName,useProperties);
    return new Boolean(trigger != null);
  }

  public Integer updateTriggerState(Long schedulerId, String triggerName,String groupName, String state,Boolean useProperties)  {

    Trigger trigger = selectTrigger(schedulerId, triggerName, groupName,useProperties);
    QuartzTrigger entite = this.getQuartzTrigger(schedulerId, triggerName, groupName);
    entite.initialiserProprietes(trigger);
    entite.setEtat(state);
    this.getTriggerDao().modifier(entite);
    return 1;
  }

  public Integer updateTriggerStateFromOtherStates(Long schedulerId,
      String triggerName, String groupName, String newState,String oldState1, String oldState2, String oldState3){
    Integer result = new Integer(0);
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setGroupe(groupName);
    filtre.setNom(triggerName);
    List<QuartzTrigger> liste = getTriggerDao().getTriggerIfIsInStates(filtre,new String[]{oldState1,oldState2,oldState3});

    for(QuartzTrigger quartzTrigger : liste ){
      quartzTrigger.setEtat(newState);
      this.getTriggerDao().modifier(quartzTrigger);
      result ++;
    }
    return result;
  }

  public Integer updateTriggerStateFromOtherStatesBeforeTime(Long schedulerId,
      String newState, String oldState1, String oldState2, Long time){

    Integer result =new Integer(0);
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    List<QuartzTrigger> liste = getTriggerDao().getMissFiredTriggerInStates(filtre, time,new String[]{oldState1,oldState2});

    for(QuartzTrigger quartzTrigger:liste){
      quartzTrigger.setEtat(newState);
      getTriggerDao().modifier(quartzTrigger);
      result ++;
    }
    return result;
  }

  public Integer updateTriggerGroupStateFromOtherStates(Long schedulerId,
      String groupName, String newState, String oldState1,String oldState2, String oldState3) {

    Integer result =new Integer(0);
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setGroupe(groupName);
    List<QuartzTrigger> liste = getTriggerDao().getMissFiredTriggersInGroupInStates(filtre,null
        ,new String[] {oldState1,oldState2,oldState3});
    for(QuartzTrigger quartzTrigger:liste){
      quartzTrigger.setEtat(newState);
      getTriggerDao().modifier(quartzTrigger);
      result ++;
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  public Integer updateTriggerStateFromOtherState(Long schedulerId,
      String triggerName, String groupName, String newState,String oldState){

    Integer result =new Integer(0);
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setNom(triggerName);
    filtre.setGroupe(groupName);
    filtre.setEtat(oldState);

    List<QuartzTrigger> liste = getTriggerDao().getListe(filtre);
    for(QuartzTrigger quartzTrigger:liste){
      quartzTrigger.setEtat(newState);
      getTriggerDao().modifier(quartzTrigger);
      result ++;
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  public Integer updateTriggerGroupStateFromOtherState(Long schedulerId,
      String groupName, String newState, String oldState){
    Integer result =new Integer(0);
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setGroupe(groupName);
    filtre.setEtat(oldState);

    List<QuartzTrigger> liste = getTriggerDao().getListe(filtre);
    for(QuartzTrigger quartzTrigger:liste){
      quartzTrigger.setEtat(newState);
      getTriggerDao().modifier(quartzTrigger);
      result ++;
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  public Integer updateTriggerStatesForJob(Long schedulerId, String jobName,
      String groupName, String state){

    Integer result =new Integer(0);
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.getJob().setGroupe(groupName);
    filtre.getJob().setNom(jobName);

    List<QuartzTrigger> liste = getTriggerDao().getListe(filtre);
    for(QuartzTrigger quartzTrigger:liste){
      quartzTrigger.setEtat(state);
      getTriggerDao().modifier(quartzTrigger);
      result ++;
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  public Integer updateTriggerStatesForJobFromOtherState(Long schedulerId,
      String jobName, String groupName, String state, String oldState){

    Integer result =new Integer(0);
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.getJob().setGroupe(groupName);
    filtre.getJob().setNom(jobName);
    filtre.setEtat(oldState);

    List<QuartzTrigger> liste = getTriggerDao().getListe(filtre);
    for(QuartzTrigger quartzTrigger:liste){
      quartzTrigger.setEtat(state);
      getTriggerDao().modifier(quartzTrigger);
      result ++;
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  public Integer deleteTriggerListeners(Long schedulerId, String triggerName, String groupName){

    Integer result =new Integer(0);
    FiltreQuartzTriggerListener filtre = new FiltreQuartzTriggerListener(schedulerId);
    filtre.getTrigger().setNom(triggerName);
    filtre.getTrigger().setGroupe(groupName);
    List<QuartzTriggerListener> liste  = getTriggerListenerDao().getListe(filtre);
    for(QuartzTriggerListener quartzTriggerListener:liste){
      getTriggerListenerDao().supprimer(quartzTriggerListener.getId());
      result ++;
    }
    return result;
  }

  public Integer insertTriggerListener(Long schedulerId, Trigger trigger,String listener) {
    QuartzTrigger quartzTrigger = this.getQuartzTrigger(schedulerId, trigger.getName(), trigger.getGroup());
    QuartzTriggerListener entite = new QuartzTriggerListener(quartzTrigger.getId(), listener);
    entite.setEcouteurTrigger(listener);
    this.getTriggerListenerDao().ajouter(entite);
    return 1;
  }

  @SuppressWarnings("unchecked")
  public String[] selectTriggerListeners(Long schedulerId, String triggerName,String groupName){

    String[] result = null;
    FiltreQuartzTriggerListener filtre = new FiltreQuartzTriggerListener(schedulerId);
    filtre.getTrigger().setNom(triggerName);
    filtre.getTrigger().setGroupe(groupName);

    List<QuartzTriggerListener> liste  = getTriggerListenerDao().getListe(filtre);
    result = new String[liste.size()];
    for(Integer i=0; i < liste.size(); i++){
      QuartzTriggerListener quartzTriggerListener = liste.get(i);
      result[i] = quartzTriggerListener.getEcouteurTrigger();
    }
    return result;
  }

  public Integer deleteSimpleTrigger(Long schedulerId, String triggerName,
      String groupName) {
    return this.supprimerTypeDeclencheur(schedulerId, triggerName, groupName, Constants.TTYPE_SIMPLE);
  }

  public Integer deleteCronTrigger(Long schedulerId, String triggerName,
      String groupName) {
    return this.supprimerTypeDeclencheur(schedulerId, triggerName, groupName, Constants.TTYPE_CRON);
  }

  public Integer deleteBlobTrigger(Long schedulerId,String triggerName,
      String groupName) {
    return this.supprimerTypeDeclencheur(schedulerId, triggerName, groupName, Constants.TTYPE_BLOB);
  }

  public Integer deleteTrigger(Long schedulerId, String triggerName,String groupName) {
    QuartzTrigger trigger = this.getQuartzTrigger(schedulerId, triggerName, groupName);
    getTriggerDao().supprimer(trigger.getId());
    return 1;
  }

  @SuppressWarnings("unchecked")
  public Integer selectNumTriggersForJob(Long schedulerId, String jobName, String groupName) {

    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.getJob().setNom(jobName);
    filtre.getJob().setGroupe(groupName);

    List<QuartzTrigger> liste = getTriggerDao().getListe(filtre);

    return liste.size();
  }

  @SuppressWarnings("unchecked")
  public JobDetail selectJobForTrigger(Long schedulerId, String triggerName,
      String groupName,Boolean useProperties){
    JobDetail result = null;

    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setNom(triggerName);
    filtre.setGroupe(groupName);

    List<QuartzTrigger> liste = getTriggerDao().getListe(filtre);
    if(liste.size() > 0){
      QuartzTrigger quartzTrigger = liste.get(0);
      result = (JobDetail) quartzTrigger.getJob().getQuartzObject(useProperties);
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  public Trigger[] selectTriggersForJob(Long schedulerId, String jobName,String groupName,Boolean useProperties){
    ArrayList<Trigger> result = new ArrayList<Trigger>();
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.getJob().setNom(jobName);
    filtre.getJob().setGroupe(groupName);

    List<QuartzTrigger> liste = getTriggerDao().getListe(filtre);
    for(Integer i=0; i < liste.size(); i++){
      QuartzTrigger quartzTrigger = liste.get(i);
      Trigger trigger = selectTrigger(schedulerId,quartzTrigger.getNom(),quartzTrigger.getGroupe(),useProperties);
      if(trigger != null){
        result.add(trigger);
      }
    }
    return result.toArray(new Trigger[result.size()]);
  }

  @SuppressWarnings("unchecked")
  public Trigger[] selectTriggersForCalendar(Long schedulerId, String calName,Boolean useProperties){

    ArrayList<Trigger> result = new ArrayList<Trigger>();
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setNomCalendrier(calName);

    List<QuartzTrigger> liste = getTriggerDao().getListe(filtre);
    for(Integer i=0; i < liste.size(); i++){
      QuartzTrigger quartzTrigger = liste.get(i);
      Trigger trigger = selectTrigger(schedulerId, quartzTrigger.getNom(), quartzTrigger.getGroupe(), useProperties);
      if(trigger != null) {
        result.add(trigger);
      }
    }
    return result.toArray(new Trigger[result.size()]);
  }

  @SuppressWarnings({ "rawtypes" })
  public List selectStatefulJobsOfTriggerGroup(Long schedulerId,String groupName) {
    ArrayList<CleSerialisable> result = new ArrayList<CleSerialisable>();
    List<QuartzJob> liste = this.getJobDao().getJobsStatefullFromTriggerGroup(schedulerId, groupName);

    for(QuartzJob quartzJob : liste){
      result.add(new CleSerialisable(quartzJob.getNom(),quartzJob.getGroupe()));
    }
    return result;
  }

  public Trigger selectTrigger(Long schedulerId, String triggerName,String groupName,Boolean useProperties) {
    Trigger trigger = null;

    QuartzTrigger entiteTrigger = this.getQuartzTrigger(schedulerId, triggerName, groupName);

    if(entiteTrigger != null) {
      QuartzTypeTrigger entiteTypeTrigger = this.getQuartzTypeTrigger(schedulerId, triggerName, groupName, entiteTrigger.getType());
      if(entiteTypeTrigger != null) {
        trigger = (Trigger) entiteTypeTrigger.getQuartzObject(useProperties);
      }
    }

    return trigger;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public JobDataMap selectTriggerJobDataMap(Long schedulerId,String trigName,String trigGroup, Boolean isProperties){
    JobDataMap result = null;
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setNom(trigName);
    filtre.setGroupe(trigGroup);

    List<QuartzTrigger> liste  = getTriggerDao().getListe(filtre);
    if(liste.size() > 0){
      Map map = liste.get(0).getDataMap(isProperties);
      if (map != null) {
        result = new JobDataMap(map);
      }
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  public String selectTriggerState(Long schedulerId, String triggerName,String groupName) {
    String result = "DELETED";
    QuartzTrigger entite  = this.getQuartzTrigger(schedulerId, triggerName, groupName);
    if(entite != null){
      result = entite.getEtat();
    }
    return result;
  }

  public TriggerStatutSerialisable selectTriggerStatus(Long schedulerId, String triggerName, String groupName) {

    TriggerStatutSerialisable status = null;

    QuartzTrigger entiteTrigger  = this.getQuartzTrigger(schedulerId, triggerName, groupName);

    if(entiteTrigger != null) {
      Long prochaineExecution = entiteTrigger.getProchaineExecution();
      Date nextFireTime = null;
      if(prochaineExecution != null && prochaineExecution > 0) {
        nextFireTime = new Date(prochaineExecution);
      }

      status = new TriggerStatutSerialisable(entiteTrigger.getEtat()
          , nextFireTime
          ,new CleSerialisable(triggerName, groupName)
      ,new CleSerialisable(entiteTrigger.getJob().getNom(), entiteTrigger.getJob().getGroupe()));
    }

    return status;
  }

  @SuppressWarnings({ "rawtypes" })
  public Integer selectNumTriggers(Long schedulerId) {
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    List liste = getTriggerDao().getListe(filtre);
    return liste.size();
  }

  @SuppressWarnings("unchecked")
  public String[] selectTriggerGroups(Long schedulerId){

    String[] result = null;
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    List<QuartzTrigger> liste = getTriggerDao().getListe(filtre);

    ArrayList<String> listTemp = new ArrayList<String>();

    for(QuartzTrigger quartzTrigger:liste){
      listTemp.add(quartzTrigger.getGroupe());
    }

    if(listTemp.size() > 0){
      Object[] arrayTemp = listTemp.toArray();
      result = new String[arrayTemp.length];
      System.arraycopy(arrayTemp, 0, result, 0, arrayTemp.length);
    }

    return result;
  }

  @SuppressWarnings({ "unchecked" })
  public String[] selectTriggersInGroup(Long schedulerId, String groupName){
    String[] result = null;
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setGroupe(groupName);
    List<QuartzTrigger> liste = getTriggerDao().getListe(filtre);

    ArrayList<String> listTemp = new ArrayList<String>();

    for(QuartzTrigger quartzTrigger:liste){
      listTemp.add(quartzTrigger.getNom());
    }

    if(listTemp.size() > 0){
      Object[] arrayTemp = listTemp.toArray();
      result = new String[arrayTemp.length];
      System.arraycopy(arrayTemp, 0, result, 0, arrayTemp.length);
    }

    return result;
  }

  public Integer insertPausedTriggerGroup(Long schedulerId, String groupName) {
    QuartzPauseTriggerGroupe entite = new QuartzPauseTriggerGroupe(schedulerId, groupName);
    getPausedTriggerGroupeDao().ajouter(entite);
    return 1;
  }

  public Integer deletePausedTriggerGroup(Long schedulerId, String groupName) {
    QuartzPauseTriggerGroupe entite = this.getQuartzPauseTriggerGroupe(schedulerId, groupName);
    getPausedTriggerGroupeDao().supprimer(entite.getId());
    return 1;
  }

  public Integer deleteAllPausedTriggerGroups(Long schedulerId) {
    return getPausedTriggerGroupeDao().deleteAllPausedTriggerGroups(schedulerId);
  }

  public Boolean isTriggerGroupPaused(Long schedulerId, String groupName) {
    QuartzPauseTriggerGroupe entite = this.getQuartzPauseTriggerGroupe(schedulerId, groupName);
    return entite != null;
  }

  @SuppressWarnings("unchecked")
  public Boolean isExistingTriggerGroup(Long schedulerId, String groupName) {
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setGroupe(groupName);
    List<QuartzTrigger> liste = getTriggerDao().getListe(filtre);
    return liste.size() > 0;
  }

  //---------------------------------------------------------------------------
  // calendars
  //---------------------------------------------------------------------------

  public Integer insertCalendar(Long schedulerId, String calendarName, Calendar calendar) {
    QuartzCalendrier entite = new QuartzCalendrier(schedulerId, calendarName, calendar);
    getCalendrierDao().ajouter(entite);
    return 1;
  }

  public Integer updateCalendar(Long schedulerId, String calendarName, Calendar calendar) {
    QuartzCalendrier entite = this.getQuartzCalendrier(schedulerId, calendarName);
    // On se sert du constructeur pour sérialiser le calendrier
    QuartzCalendrier temp = new QuartzCalendrier(schedulerId, calendarName, calendar);
    entite.setCalendrierData(temp.getCalendrierData());
    getCalendrierDao().modifier(entite);
    return 1;
  }

  public Boolean calendarExists(Long schedulerId, String calendarName) {
    QuartzCalendrier entite = this.getQuartzCalendrier(schedulerId, calendarName);
    return entite != null;
  }

  public Calendar selectCalendar(Long schedulerId, String calendarName) {
    Calendar calendar = null;
    QuartzCalendrier entite = this.getQuartzCalendrier(schedulerId, calendarName);
    if(entite != null) {
      calendar = (Calendar) entite.getQuartzObject(null);
    }
    return calendar;
  }

  @SuppressWarnings("unchecked")
  public Boolean calendarIsReferenced(Long schedulerId, String calendarName) {
    QuartzCalendrier entite = this.getQuartzCalendrier(schedulerId, calendarName);
    return entite != null;
  }

  public Integer deleteCalendar(Long schedulerId, String calendarName) {
    QuartzCalendrier entite = this.getQuartzCalendrier(schedulerId, calendarName);
    getCalendrierDao().supprimer(entite.getId());
    return 1;
  }

  @SuppressWarnings("unchecked")
  public Integer selectNumCalendars(Long schedulerId) {
    FiltreQuartzCalendrier filtre = new FiltreQuartzCalendrier();
    filtre.setSchedulerId(schedulerId);
    List<QuartzCalendrier> liste = getCalendrierDao().getListe(filtre);
    return liste.size();
  }

  @SuppressWarnings("unchecked")
  public String[] selectCalendars(Long schedulerId) {
    List<String>  resultat = new ArrayList<String>();
    FiltreQuartzCalendrier filtre = new FiltreQuartzCalendrier();
    filtre.setSchedulerId(schedulerId);
    List<QuartzCalendrier> liste = getCalendrierDao().getListe(filtre);
    for(QuartzCalendrier calendrier : liste) {
      resultat.add(calendrier.getNom());
    }

    return resultat.toArray(new String[resultat.size()]);
  }

  //---------------------------------------------------------------------------
  // trigger firing
  //---------------------------------------------------------------------------

  public Long selectNextFireTime(Long schedulerId){
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setEtat(Constants.STATE_WAITING);
    return getTriggerDao().getNextFireTime(filtre);
  }

  @SuppressWarnings("unchecked")
  public CleSerialisable selectTriggerForFireTime(Long schedulerId, Long fireTime){
    CleSerialisable result = null;
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setEtat(Constants.STATE_WAITING);
    filtre.setProchaineExecution(fireTime);
    List<QuartzTrigger> triggers = getTriggerDao().getListe(filtre);
    if(triggers != null && triggers.size() > 0) {
      result = new CleSerialisable(triggers.get(0).getNom(), triggers.get(0).getGroupe());
    }

    return result;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public List selectTriggerToAcquire(Long schedulerId, Long noLaterThan, Long noEarlierThan){
    List result = new ArrayList();
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setEtat(Constants.STATE_WAITING);
    List<QuartzTrigger> triggers = getTriggerDao().getListeTriggerToAquire(filtre, noEarlierThan,noLaterThan);
    for(QuartzTrigger quartzTrigger:triggers){
      result.add(new CleSerialisable(quartzTrigger.getNom(),quartzTrigger.getGroupe()));
      if(result.size() >= 5) {
        break;
      }
    }

    return result;
  }

  public Integer insertFiredTrigger(Long schedulerId,String instanceId, Trigger trigger, String state, JobDetail job) {
    QuartzTrigger entiteDeclencheur = this.getQuartzTrigger(schedulerId, trigger.getName(), trigger.getGroup());
    QuartzFiredTrigger quartzFiredTrigger = new QuartzFiredTrigger(entiteDeclencheur.getId(),instanceId,trigger,state,job);
    this.getFiredTriggerDao().ajouter(quartzFiredTrigger);
    return 1;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public List selectFiredTriggerRecords(Long schedulerId, String triggerName,String groupName){
    List result = new ArrayList();
    FiltreQuartzFiredTrigger filtre = new FiltreQuartzFiredTrigger(schedulerId);
    filtre.getTrigger().setNom(triggerName);
    filtre.getTrigger().setGroupe(groupName);
    List<QuartzFiredTrigger> triggers = getFiredTriggerDao().getListe(filtre);
    for(QuartzFiredTrigger quartzFiredTrigger : triggers){
      result.add(quartzFiredTrigger.getQuartzObject(null));
    }
    return result;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public List selectFiredTriggerRecordsByJob(Long schedulerId, String jobName,String groupName)  {
    List result = new ArrayList();
    FiltreQuartzFiredTrigger filtre = new FiltreQuartzFiredTrigger(schedulerId);
    filtre.getTrigger().getJob().setNom(jobName);
    filtre.getTrigger().getJob().setGroupe(groupName);
    List<QuartzFiredTrigger> triggers = getFiredTriggerDao().getListe(filtre);

    for(QuartzFiredTrigger quartzFiredTrigger : triggers){
      result.add(quartzFiredTrigger.getQuartzObject(null));
    }

    return result;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public List selectInstancesFiredTriggerRecords(Long schedulerId,String instanceName) {
    List result = new ArrayList();
    FiltreQuartzFiredTrigger filtre = new FiltreQuartzFiredTrigger(schedulerId);
    filtre.setNomScheduler(instanceName);
    List<QuartzFiredTrigger> triggers = getFiredTriggerDao().getListe(filtre);

    for(QuartzFiredTrigger quartzFiredTrigger : triggers){
      result.add(quartzFiredTrigger.getQuartzObject(null));
    }

    return result;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public Set selectFiredTriggerInstanceNames(Long schedulerId) {
    Set result = new HashSet();
    FiltreQuartzFiredTrigger filtre = new FiltreQuartzFiredTrigger(schedulerId);
    List<QuartzFiredTrigger> triggers = getFiredTriggerDao().getListe(filtre);

    for(QuartzFiredTrigger quartzFiredTrigger : triggers) {
      if(!result.contains(quartzFiredTrigger.getNomScheduler())) {
        result.add(quartzFiredTrigger.getNomScheduler());
      }
    }

    return result;
  }

  public Integer deleteFiredTrigger(Long schedulerId, String entryId){
    Integer resultat = new Integer(0);
    FiltreQuartzFiredTrigger filtre = new FiltreQuartzFiredTrigger(schedulerId);
    filtre.setEntreeId(entryId);
    List<QuartzFiredTrigger> liste = getFiredTriggerDao().getListe(filtre);
    if(liste != null && liste.size() > 0) {
      getFiredTriggerDao().supprimer(liste.get(0).getId());
      resultat = 1;
    }
    return resultat;
  }

  @SuppressWarnings("unchecked")
  public Integer selectJobExecutionCount(Long schedulerId, String jobName,String jobGroup){
    FiltreQuartzFiredTrigger filtre = new FiltreQuartzFiredTrigger(schedulerId);
    filtre.getTrigger().getJob().setNom(jobName);
    filtre.getTrigger().getJob().setGroupe(jobGroup);
    List<QuartzFiredTrigger> triggers = getFiredTriggerDao().getListe(filtre);
    return triggers.size();
  }

  @SuppressWarnings("unchecked")
  public Integer deleteVolatileFiredTriggers(Long schedulerId){
    Integer result = new Integer(0);
    FiltreQuartzFiredTrigger filtre = new FiltreQuartzFiredTrigger(schedulerId);
    filtre.setTriggerVolatile(Boolean.TRUE);
    List<QuartzFiredTrigger> triggers = getFiredTriggerDao().getListe(filtre);
    for(QuartzFiredTrigger quartzFiredTrigger : triggers){
      getFiredTriggerDao().supprimer(quartzFiredTrigger.getId());
      result ++;
    }
    return result;
  }

  public Integer insertSchedulerState(Long schedulerId, String instanceId, Long checkInTime, Long interval){
    QuartzEtatScheduler quartzEtatScheduler = new QuartzEtatScheduler(schedulerId);
    quartzEtatScheduler.setNom(instanceId);
    quartzEtatScheduler.setDerniereValidation(checkInTime);
    quartzEtatScheduler.setIntervalValidation(interval);
    getEtatSchedulerDao().ajouter(quartzEtatScheduler);
    return 1;
  }

  @SuppressWarnings("unchecked")
  public Integer deleteSchedulerState(Long schedulerId, String instanceId){
    Integer result = new Integer(0);
    FiltreQuartzEtatScheduler filtre = new FiltreQuartzEtatScheduler();
    filtre.setSchedulerId(schedulerId);
    filtre.setNom(instanceId);
    List<QuartzEtatScheduler> schedulers = getEtatSchedulerDao().getListe(filtre);
    for(QuartzEtatScheduler quartzEtatScheduler:schedulers){
      getEtatSchedulerDao().supprimer(quartzEtatScheduler.getId());
      result ++;
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  public Integer updateSchedulerState(Long schedulerId, String instanceId, Long checkInTime){
    Integer result = new Integer(0);
    FiltreQuartzEtatScheduler filtre = new FiltreQuartzEtatScheduler();
    filtre.setSchedulerId(schedulerId);
    filtre.setNom(instanceId);

    List<QuartzEtatScheduler> schedulers = getEtatSchedulerDao().getListe(filtre);
    for(QuartzEtatScheduler quartzEtatScheduler:schedulers){
      quartzEtatScheduler.setDerniereValidation(checkInTime);
      getEtatSchedulerDao().modifier(quartzEtatScheduler);
      result ++;
    }
    return result;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public List selectSchedulerStateRecords(Long schedulerId, String instanceId){
    List result = new ArrayList();
    FiltreQuartzEtatScheduler filtre = new FiltreQuartzEtatScheduler();
    filtre.setSchedulerId(schedulerId);
    filtre.setNom(instanceId);
    List<QuartzEtatScheduler> schedulers = getEtatSchedulerDao().getListe(filtre);
    for(QuartzEtatScheduler quartzEtatScheduler:schedulers){
      result.add(quartzEtatScheduler.getQuartzObject(null));
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  public CleSerialisable[] selectVolatileTriggers(Long schedulerId) {
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    filtre.setTriggerVolatile(Boolean.TRUE);
    List<QuartzEntite> triggers = getTriggerDao().getListe(filtre);
    return getKeyArray(triggers,NOM,GROUPE);
  }

  @SuppressWarnings("unchecked")
  public CleSerialisable[] selectVolatileJobs(Long schedulerId){
    FiltreQuartzJob filtre = new FiltreQuartzJob();
    filtre.setSchedulerId(schedulerId);
    filtre.setJobVolatile(Boolean.TRUE);
    List<QuartzEntite> jobs = getJobDao().getListe(filtre);
    return getKeyArray(jobs,NOM,GROUPE);
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  public Set selectPausedTriggerGroups(Long schedulerId){
    HashSet result = new HashSet();
    FiltreQuartzPauseTrigger filtre = new FiltreQuartzPauseTrigger();
    filtre.setSchedulerId(schedulerId);
    List<QuartzPauseTriggerGroupe> triggerGroupes = getPausedTriggerGroupeDao().getListe(filtre);

    for(QuartzPauseTriggerGroupe quartzPauseTriggerGroupe: triggerGroupes){
      result.add(quartzPauseTriggerGroupe.getGroupe());
    }

    return result;
  }

  private CleSerialisable[] getKeyArray(List<QuartzEntite> liste,String attribut1,String attribut2){
    CleSerialisable[] result = null;

    if(liste != null && liste.size() > 0) {
      ArrayList<CleSerialisable> keys = new ArrayList<CleSerialisable>();

      for(QuartzEntite objet:liste ) {
        String permier = (String) UtilitaireObjet.getValeurAttribut(objet, attribut1);
        String second = (String) UtilitaireObjet.getValeurAttribut(objet, attribut2);
        keys.add(new CleSerialisable(permier, second));
      }

      result = keys.toArray(new CleSerialisable[keys.size()]);
    } else {
      result = new CleSerialisable[0];
    }

    return result;
  }

  /**
   * Permet d'obtenir un job â partir des infromations passées en paramètre
   * @param schedulerId un identifiant d'ordonnanceur
   * @param jobName un nom de job
   * @param groupName un nom de groupe de job
   * @return un objet de type {@link QuartzJob}
   */
  @SuppressWarnings("unchecked")
  private QuartzJob getQuartzJob(Long schedulerId, String jobName, String groupName) {
    QuartzJob job = null;
    FiltreQuartzJob filtre = new FiltreQuartzJob(schedulerId, jobName, groupName);
    List<QuartzJob> liste= getJobDao().getListe(filtre);
    if(liste != null && liste.size() > 0) {
      job = liste.get(0);
    }
    return job;
  }

  /**
   * Permet de récupérer une entité {@link QuartzTrigger} à partir de l'identifiant du schéduler et
   * des caractéristique du trigger
   * @param schedulerId
   * @param nomDeclencheur
   * @param groupeDeclencheur
   * @return un objet de type {@link QuartzTrigger}
   */
  private QuartzTrigger getQuartzTrigger(Long schedulerId, String nomDeclencheur, String groupeDeclencheur) {
    QuartzTrigger quartzTrigger = null;
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId, nomDeclencheur, groupeDeclencheur);
    List<QuartzTrigger> liste = this.getTriggerDao().getListe(filtre);
    if(liste != null && liste.size() > 0) {
      quartzTrigger = liste.get(0);
    }
    return quartzTrigger;
  }

  /**
   * Permet d'obtenir un objet de type {@link QuartzTypeTrigger} suivant les paramètres
   * @param schedulerId un objet de type Long
   * @param nomDeclencheur
   * @param groupeDeclencheur
   * @param codeTypeTrigger
   * @return un objet de type {@link QuartzTypeTrigger}
   */
  private QuartzTypeTrigger getQuartzTypeTrigger(Long schedulerId, String nomDeclencheur, String groupeDeclencheur, String codeTypeTrigger) {
    QuartzTypeTrigger typeTrigger = null;
    QuartzTrigger entiteDeclencheur = getQuartzTrigger(schedulerId, nomDeclencheur, groupeDeclencheur);
    FiltreTypeTrigger filtre = new FiltreTypeTrigger();
    filtre.setDeclencheurId(entiteDeclencheur.getId());

    BaseDao typeTriggerDao = null;
    if(Constants.TTYPE_SIMPLE.equals(codeTypeTrigger)) {
      typeTriggerDao = this.getSimpleTriggerDao();
    } else if(Constants.TTYPE_CRON.equals(codeTypeTrigger)) {
      typeTriggerDao = this.getCronTriggerDao();
    } else if(Constants.TTYPE_BLOB.equals(codeTypeTrigger)) {
      typeTriggerDao = this.getBlobTriggerDao();
    } else {
      throw new ModeleException("Aucun dao ne correspond au type de trigger recu : " + codeTypeTrigger);
    }
    List<QuartzTypeTrigger> liste = typeTriggerDao.getListe(filtre);
    if(liste != null && liste.size() > 0) {
      typeTrigger = liste.get(0);
    }
    return typeTrigger;
  }

  /**
   * Permet du récupérer un objet de type {@link QuartzPauseTriggerGroupe} à partir d'un ordonnanceur
   * et d'un nom de groupe de trigger.
   * @param schedulerId
   * @param groupName
   * @return un objet de type {@link QuartzPauseTriggerGroupe}
   */
  private QuartzPauseTriggerGroupe getQuartzPauseTriggerGroupe(Long schedulerId, String groupName) {
    QuartzPauseTriggerGroupe entite = null;
    FiltreQuartzPauseTrigger filtre = new FiltreQuartzPauseTrigger();
    filtre.setSchedulerId(schedulerId);
    filtre.setGroupe(groupName);
    List<QuartzPauseTriggerGroupe> liste = this.getPausedTriggerGroupeDao().getListe(filtre);
    if(liste != null && liste.size() > 0) {
      entite = liste.get(0);
    }
    return entite;
  }

  /**
   * Permet du récupérer un objet de type {@link QuartzCalendrier} à partir d'un ordonnanceur
   * et d'un nom de groupe de trigger.
   * @param schedulerId
   * @param nomCalendrier
   * @return un objet de type {@link QuartzCalendrier}
   */
  private QuartzCalendrier getQuartzCalendrier(Long schedulerId, String nomCalendrier) {
    QuartzCalendrier entite = null;
    FiltreQuartzCalendrier filtre = new FiltreQuartzCalendrier();
    filtre.setSchedulerId(schedulerId);
    filtre.setNom(nomCalendrier);
    List<QuartzCalendrier> liste = this.getCalendrierDao().getListe(filtre);
    if(liste != null && liste.size() > 0) {
      entite = liste.get(0);
    }
    return entite;
  }

  /**
   * Permet de supprimer une entite trigger typé (simple, cron ou blob) à partir de son id récupéré
   * avec les valeurs passé en paramètre
   * @param schedulerId
   * @param triggerName
   * @param groupName
   * @param codeTypeTrigger
   * @return
   */
  private Integer supprimerTypeDeclencheur(Long schedulerId, String triggerName,
      String groupName, String codeTypeTrigger) {
    Integer result = new Integer(0);
    QuartzTypeTrigger trigger = this.getQuartzTypeTrigger(schedulerId, triggerName, groupName, codeTypeTrigger);
    if(trigger != null) {
      getSimpleTriggerDao().supprimer(trigger.getId());
      result = new Integer(1);
    }
    return result;
  }

  public QuartzTriggerDao getTriggerDao() {
    return triggerDao;
  }

  public void setTriggerDao(QuartzTriggerDao triggerDao) {
    this.triggerDao = triggerDao;
  }

  public QuartzFiredTriggerDao getFiredTriggerDao() {
    return firedTriggerDao;
  }

  public void setFiredTriggerDao(QuartzFiredTriggerDao firedTriggerDao) {
    this.firedTriggerDao = firedTriggerDao;
  }

  public QuartzJobDao getJobDao() {
    return jobDao;
  }

  public void setJobDao(QuartzJobDao jobDao) {
    this.jobDao = jobDao;
  }

  public QuartzJobListenerDao getJobListenerDao() {
    return jobListenerDao;
  }

  public void setJobListenerDao(QuartzJobListenerDao jobListenerDao) {
    this.jobListenerDao = jobListenerDao;
  }

  public QuartzTriggerListenerDao getTriggerListenerDao() {
    return triggerListenerDao;
  }

  public void setTriggerListenerDao(QuartzTriggerListenerDao triggerListenerDao) {
    this.triggerListenerDao = triggerListenerDao;
  }

  public QuartzSimpleTriggerDao getSimpleTriggerDao() {
    return simpleTriggerDao;
  }

  public void setSimpleTriggerDao(QuartzSimpleTriggerDao simpleTriggerDao) {
    this.simpleTriggerDao = simpleTriggerDao;
  }

  public QuartzCronTriggerDao getCronTriggerDao() {
    return cronTriggerDao;
  }

  public void setCronTriggerDao(QuartzCronTriggerDao cronTriggerDao) {
    this.cronTriggerDao = cronTriggerDao;
  }

  public QuartzBlobTriggerDao getBlobTriggerDao() {
    return blobTriggerDao;
  }

  public void setBlobTriggerDao(QuartzBlobTriggerDao blobTriggerDao) {
    this.blobTriggerDao = blobTriggerDao;
  }

  public QuartzPausedTriggerGroupeDao getPausedTriggerGroupeDao() {
    return pausedTriggerGroupeDao;
  }

  public void setPausedTriggerGroupeDao(QuartzPausedTriggerGroupeDao pausedTriggerGroupeDao) {
    this.pausedTriggerGroupeDao = pausedTriggerGroupeDao;
  }

  public QuartzCalendrierDao getCalendrierDao() {
    return calendrierDao;
  }

  public void setCalendrierDao(QuartzCalendrierDao calendrierDao) {
    this.calendrierDao = calendrierDao;
  }

  public QuartzEtatSchedulerDao getEtatSchedulerDao() {
    return etatSchedulerDao;
  }

  public void setEtatSchedulerDao(QuartzEtatSchedulerDao etatSchedulerDao) {
    this.etatSchedulerDao = etatSchedulerDao;
  }

  public QuartzVerrouDao getVerrouDao() {
    return verrouDao;
  }

  public void setVerrouDao(QuartzVerrouDao verrouDao) {
    this.verrouDao = verrouDao;
  }

  public QuartzSchedulerDao getSchedulerDao() {
    return schedulerDao;
  }

  public void setSchedulerDao(QuartzSchedulerDao schedulerDao) {
    this.schedulerDao = schedulerDao;
  }

}