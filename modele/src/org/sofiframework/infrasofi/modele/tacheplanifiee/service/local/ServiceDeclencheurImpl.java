/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele.tacheplanifiee.service.local;

import java.io.Serializable;
import java.util.List;

import org.quartz.impl.jdbcjobstore.Constants;
import org.sofiframework.infrasofi.modele.BaseServiceInfrastructureImpl;
import org.sofiframework.infrasofi.modele.entite.QuartzTrigger;
import org.sofiframework.infrasofi.modele.entite.QuartzTypeTrigger;
import org.sofiframework.infrasofi.modele.service.ServiceDeclencheur;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzBlobTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzCronTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.dao.QuartzSimpleTriggerDao;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreQuartzTrigger;
import org.sofiframework.infrasofi.modele.tacheplanifiee.filtre.FiltreTypeTrigger;
import org.sofiframework.modele.spring.dao.BaseDao;

/**
 * TODO
 * 
 * @author Rémi Mercier
 * @author Mathieu Blanchet
 */
public class ServiceDeclencheurImpl extends BaseServiceInfrastructureImpl implements ServiceDeclencheur{

  private QuartzSimpleTriggerDao simpleTriggerDao;
  private QuartzCronTriggerDao cronTriggerDao;
  private QuartzBlobTriggerDao blobTriggerDao;

  public QuartzTrigger getDeclencheur(Long declencheurId){
    QuartzTrigger declencheur = (QuartzTrigger) this.get(declencheurId);

    BaseDao typeTriggerDao = null;
    if(declencheur.isSimple()){
      typeTriggerDao = this.simpleTriggerDao;
    }else if(declencheur.isCron()){
      typeTriggerDao = this.cronTriggerDao;
    }
    else if(declencheur.isBlob()){
      typeTriggerDao = this.blobTriggerDao;
    }

    if(typeTriggerDao != null) {
      FiltreTypeTrigger filtre = new FiltreTypeTrigger();
      filtre.setDeclencheurId(declencheur.getId());
      List<QuartzTypeTrigger> liste = typeTriggerDao.getListe(filtre);
      if(liste != null && liste.size() > 0) {
        declencheur.setEnfant(liste.get(0));
      }
    }
    return declencheur;
  }

  public Serializable ajouter(QuartzTrigger declencheur) {
    Long declencheurId = (Long) super.ajouter(declencheur);
    declencheur.getEnfant().setDeclencheurId(declencheurId);

    if(declencheur.isSimple()) {
      this.simpleTriggerDao.ajouter(declencheur.getEnfant());
    }
    else if(declencheur.isCron()) {
      this.cronTriggerDao.ajouter(declencheur.getEnfant());
    }
    else if(declencheur.isBlob()) {
      this.blobTriggerDao.ajouter(declencheur.getEnfant());
    }

    return declencheur;
  }

  public void modifier(QuartzTrigger declencheur) {
    super.modifier(declencheur);


    if(declencheur.isSimple()) {
      this.simpleTriggerDao.modifier(declencheur.getEnfant());
    }
    else if(declencheur.isCron()) {
      this.cronTriggerDao.modifier(declencheur.getEnfant());
    }
    else if(declencheur.isBlob()) {
      this.blobTriggerDao.modifier(declencheur.getEnfant());
    }
  }

  public void supprimerDeclencheur(QuartzTrigger declencheur){

    if(declencheur.isSimple()) {
      this.simpleTriggerDao.supprimer(declencheur.getEnfant().getId());
    }
    else if(declencheur.isCron()) {
      this.cronTriggerDao.supprimer(declencheur.getEnfant().getId());
    }
    else if(declencheur.isBlob()) {
      this.blobTriggerDao.supprimer(declencheur.getEnfant().getId());
    }

    this.supprimer(declencheur.getId());
  }

  /*
   * (non-Javadoc)
   * @see org.sofiframework.infrasofi.modele.service.ServiceDeclencheur#getListeDeclencheur(java.lang.Long)
   */
  @SuppressWarnings("unchecked")
  public List<QuartzTrigger> getListeDeclencheur(Long schedulerId) {
    FiltreQuartzTrigger filtre = new FiltreQuartzTrigger(schedulerId);
    return this.getDao().getListe(filtre);
  }

  /*
   * (non-Javadoc)
   * @see com.nurun.infrasofi.modele.service.ServiceDeclencheur#activerDeclencheur(java.io.Serializable)
   */
  public void activerDeclencheur(Serializable id) {
    QuartzTrigger declencheur = (QuartzTrigger) this.get(id);
    declencheur.setEtat(Constants.STATE_WAITING);
    this.getDao().modifier(declencheur);
  }

  /*
   * (non-Javadoc)
   * @see com.nurun.infrasofi.modele.service.ServiceDeclencheur#desactiverDeclencheur(java.io.Serializable)
   */
  public void desactiverDeclencheur(Serializable id) {
    QuartzTrigger declencheur = (QuartzTrigger) this.get(id);
    declencheur.setEtat(Constants.STATE_PAUSED);
    this.getDao().modifier(declencheur);
  }

  public QuartzSimpleTriggerDao getSimpleTriggerDao() {
    return simpleTriggerDao;
  }

  public void setSimpleTriggerDao(QuartzSimpleTriggerDao simpleTriggerDao) {
    this.simpleTriggerDao = simpleTriggerDao;
  }

  public QuartzCronTriggerDao getCronTriggerDao() {
    return cronTriggerDao;
  }

  public void setCronTriggerDao(QuartzCronTriggerDao cronTriggerDao) {
    this.cronTriggerDao = cronTriggerDao;
  }

  public QuartzBlobTriggerDao getBlobTriggerDao() {
    return blobTriggerDao;
  }

  public void setBlobTriggerDao(QuartzBlobTriggerDao blobTriggerDao) {
    this.blobTriggerDao = blobTriggerDao;
  }



}
