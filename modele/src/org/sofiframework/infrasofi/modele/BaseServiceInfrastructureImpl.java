/*
 * Copyright 2008-2010 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sofiframework.infrasofi.modele;

import java.io.Serializable;
import java.util.Date;

import org.sofiframework.application.securite.GestionSecurite;
import org.sofiframework.application.securite.objetstransfert.ObjetSecurisable;
import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.infrasofi.modele.entite.Journalisation;
import org.sofiframework.infrasofi.modele.entite.Tracable;
import org.sofiframework.infrasofi.modele.service.ServiceJournalisation;
import org.sofiframework.modele.entite.EntiteTrace;
import org.sofiframework.modele.spring.service.local.DaoServiceImpl;
import org.sofiframework.objetstransfert.ObjetTransfert;
import org.sofiframework.utilitaire.UtilitaireObjet;

/**
 * Service parent des services de l'infrastructure.
 * Encapsule des fonctionnalités réutilisables.
 * 
 * @author Jean-Maxime Pelletier
 */
public abstract class BaseServiceInfrastructureImpl extends DaoServiceImpl {

  private ServiceJournalisation serviceJournalisation = null;

  /*
   * (non-Javadoc)
   * @see org.sofiframework.modele.spring.service.local.DaoServiceImpl#ajouter(java.lang.Object)
   */
  @Override
  public Serializable ajouter(Object entite) {
    return super.ajouter(entite);
  }

  /*
   * (non-Javadoc)
   * @see org.sofiframework.modele.spring.service.local.DaoServiceImpl#modifier(java.lang.Object)
   */
  @Override
  public void modifier(Object entite) {
    super.modifier(entite);
  }

  public void enregistrer(EntiteTrace entite) {
    if (entite.getDateCreation() != null) {
      this.modifier(entite);
    } else {
      this.ajouter(entite);
    }
  }

  @Override
  public void supprimer(Serializable id) {
    super.supprimer(id);
  }

  /**
   * Mise à jour des propriétés de
   * trace de modification de l'entité.
   * 
   * @param tracable Entité traçable
   */
  protected void traceModification(Tracable tracable) {
    Utilisateur u = this.getUtilisateur();
    if (u != null) {
      tracable.setModifiePar(u.getCodeUtilisateur());
    }
    tracable.setDateModification(new Date());
  }

  /**
   * Mise à jour des propriétés de trace de
   * création de l'entité.
   * 
   * @param tracable Entité traçable
   */
  protected void traceCreation(Tracable tracable) {
    Utilisateur u = this.getUtilisateur();
    if (u != null) {
      tracable.setCreePar(u.getCodeUtilisateur());
    }
    tracable.setDateCreation(new Date());
  }

  protected void journaliserMaj(String type, Object entite) {
    if (this.serviceJournalisation != null) {
      Journalisation j = new Journalisation();
      j.setCodeApplication(GestionSecurite.getInstance().getCodeApplication());
      j.setDateHeure(new Date());

      String  detail = entite.getClass().getCanonicalName();
      if (entite instanceof ObjetTransfert) {
        ObjetTransfert objetTransfert = (ObjetTransfert)entite;
        String  cleStr = "";
        for (String cle : objetTransfert.getCle()) {
          Object valeurCle = UtilitaireObjet.getValeurAttribut(objetTransfert, cle);
          if (valeurCle != null) {
            cleStr += cle + "=" + valeurCle.toString() + ",";
          }
        }
        detail += cleStr.lastIndexOf(",")  >= 0 ? "[" + cleStr.substring(0, cleStr.lastIndexOf(",")) + "]" : cleStr;
      }

      j.setDetail(detail);
      j.setType(type);

      ObjetSecurisable o = GestionSecurite.getInstance().getComposantMenuAccueil();
      if (o != null) {
        j.setSeqObjetSecurisable(o.getSeqObjetSecurisable());
      }

      Utilisateur u = this.getUtilisateur();
      if (u != null) {
        j.setCodeUtilisateur(u.getCodeUtilisateur());
      }

      this.serviceJournalisation.ajouter(j);
    }
  }

  public ServiceJournalisation getServiceJournalisation() {
    return serviceJournalisation;
  }

  public void setServiceJournalisation(ServiceJournalisation serviceJournalisation) {
    this.serviceJournalisation = serviceJournalisation;
  }
}
