package org.sofiframework.infrasofi.modele.parametresysteme.service.ejb;

import javax.ejb.CreateException;

import org.sofiframework.application.parametresysteme.service.ServiceParametreSysteme;
import org.sofiframework.modele.ejb.exception.ServiceException;
import org.springframework.ejb.support.AbstractStatelessSessionBean;

/**
 * Composant serveur du service EJB des paramètres système.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceParametreSystemeBean extends AbstractStatelessSessionBean {

  private static final long serialVersionUID = 2771643733212860995L;

  private ServiceParametreSysteme delegate = null;

  protected void onEjbCreate() throws CreateException {
    delegate = (ServiceParametreSysteme) getBeanFactory().getBean(
        "serviceParametreSystemeCommun");
  }

  public String getValeur(String codeApplication, String cle)
      throws ServiceException {
    String valeur = null;

    try {
      valeur = delegate.getValeur(codeApplication, cle);
    } catch (Exception e) {
      throw new ServiceException(e);
    }

    return valeur;
  }

  public void enregistrerValeur(String codeApplication, String code,
      String valeur) throws ServiceException {
    try {
      delegate.enregistrerValeur(codeApplication, code, valeur);
    } catch (Exception e) {
      throw new ServiceException(e);
    }
  }
}