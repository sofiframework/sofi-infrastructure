package org.sofiframework.infrasofi.modele.domainevaleur.service.ejb;

import java.util.List;

import javax.ejb.CreateException;

import org.sofiframework.application.domainevaleur.objetstransfert.DomaineValeur;
import org.sofiframework.application.domainevaleur.service.ServiceDomaineValeur;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.infrasofi.modele.AbstractStatelessSessionBean;
import org.sofiframework.modele.ejb.exception.ServiceException;


public class ServiceDomaineValeurBean extends AbstractStatelessSessionBean {

  private static final long serialVersionUID = -1080166606888935366L;

  private ServiceDomaineValeur delegate = null;

  protected void onEjbCreate() throws CreateException {
    this.delegate = (ServiceDomaineValeur) this.getBeanFactory().getBean(
        "serviceDomaineValeurCommun");
  }

  public ListeNavigation getListeValeur(ListeNavigation liste)
      throws ServiceException {
    ListeNavigation retour = null;
    try {
      retour = this.delegate.getListeValeur(liste);
    } catch (Exception e) {
      throw new ServiceException(e);
    }
    return retour;
  }

  @SuppressWarnings("rawtypes")
  public List getListeValeur(String codeApplicationParent, String nomParent,
      String valeurParent, String codeLangueParent) throws ServiceException {
    List retour = null;
    try {
      retour = this.delegate.getListeValeur(codeApplicationParent, nomParent,
          valeurParent, codeLangueParent);
    } catch (Exception e) {
      throw new ServiceException(e);
    }
    return retour;
  }

  public DomaineValeur getDomaineValeur(String codeApplication, String nom,
      String valeur, String codeLangue) throws ServiceException {
    DomaineValeur retour = null;
    try {
      retour = (DomaineValeur) this.delegate.getDomaineValeur(codeApplication,
          nom, valeur, codeLangue);
    } catch (Exception e) {
      throw new ServiceException(e);
    }
    return retour;
  }
}
