package org.sofiframework.infrasofi.modele.journalisation.service.ejb;

import javax.ejb.CreateException;

import org.sofiframework.application.journalisation.objetstransfert.Journal;
import org.sofiframework.application.journalisation.service.ServiceJournalisation;
import org.sofiframework.composantweb.liste.ListeNavigation;
import org.sofiframework.modele.ejb.exception.ServiceException;
import org.springframework.ejb.support.AbstractStatelessSessionBean;

/**
 * Composant serveur du service de journalisation.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceJournalisationBean extends AbstractStatelessSessionBean {

  private static final long serialVersionUID = -4470881164300855994L;

  private ServiceJournalisation delegate = null;

  protected void onEjbCreate() throws CreateException {
    delegate = (ServiceJournalisation) getBeanFactory().getBean(
        "serviceJournalisationCommun");
  }

  public void ecrire(Journal journal) throws ServiceException {
    try {
      delegate.ecrire(journal);
    } catch (Exception e) {
      throw new ServiceException(e);
    }
  }

  public Journal getJournal(Long idJournalisation) throws ServiceException {
    Journal journal = null;

    try {
      journal = delegate.getJournal(idJournalisation);
    } catch (Exception e) {
      throw new ServiceException(e);
    }

    return journal;
  }

  public ListeNavigation getListeJournalisation(ListeNavigation liste)
      throws ServiceException {
    try {
      liste = delegate.getListeJournalisation(liste);
    } catch (Exception e) {
      throw new ServiceException(e);
    }

    return liste;
  }
}