package org.sofiframework.infrasofi.modele.message.service.ejb;

import javax.ejb.CreateException;

import org.sofiframework.application.message.objetstransfert.Message;
import org.sofiframework.application.message.service.ServiceMessage;
import org.sofiframework.modele.ejb.exception.ServiceException;
import org.springframework.ejb.support.AbstractStatelessSessionBean;

/**
 * Composant serveur du EJB des messages.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceMessageBean extends AbstractStatelessSessionBean {

  private static final long serialVersionUID = 7714932808270236666L;

  private ServiceMessage delegate = null;

  protected void onEjbCreate() throws CreateException {
    delegate = (ServiceMessage) getBeanFactory().getBean("serviceMessageCommun");
  }

  public Message getMessage(String identifiant, String codeLangue, String codeSexe) throws ServiceException {
    Message message = null;

    try {
      message = delegate.get(identifiant, codeLangue, null);
    } catch (Exception e) {
      throw new ServiceException(e);
    }

    return message;
  }
}