package org.sofiframework.infrasofi.modele.message.service.ejb;

import javax.ejb.CreateException;

import org.sofiframework.application.message.objetstransfert.Libelle;
import org.sofiframework.application.message.service.ServiceLibelle;
import org.sofiframework.modele.ejb.exception.ServiceException;
import org.springframework.ejb.support.AbstractStatelessSessionBean;

/**
 * Composant serveur du service EJB des libellés.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceLibelleBean extends AbstractStatelessSessionBean {

  private static final long serialVersionUID = 1621827075954172477L;

  private ServiceLibelle delegate = null;

  protected void onEjbCreate() throws CreateException {
    delegate = (ServiceLibelle) getBeanFactory().getBean("serviceLibelleCommun");
  }

  public Libelle getLibelle(String identifiant, String codeLangue, String codeSexe) throws ServiceException {
    Libelle libelle = null;

    try {
      libelle = delegate.get(identifiant, codeLangue, null);
    } catch (Exception e) {
      throw new ServiceException(e);
    }

    return libelle;
  }
}