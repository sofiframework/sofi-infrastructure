package org.sofiframework.infrasofi.modele.securite.service.ejb;

import javax.ejb.CreateException;

import org.sofiframework.application.securite.objetstransfert.Application;
import org.sofiframework.application.securite.service.ServiceObjetSecurisable;
import org.sofiframework.modele.ejb.exception.ServiceException;
import org.springframework.ejb.support.AbstractStatelessSessionBean;

/**
 * Composant serveur du service EJB d'objets sécurisés.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceObjetSecurisableBean extends AbstractStatelessSessionBean {

  private static final long serialVersionUID = 605720553141351093L;

  private ServiceObjetSecurisable delegate = null;

  protected void onEjbCreate() throws CreateException {
    delegate = (ServiceObjetSecurisable) getBeanFactory().getBean(
        "serviceObjetSecurisableCommun");
  }

  public Application getApplicationAvecObjetSecurisable(String codeApplication,
      boolean applicationActiveSeulement) throws ServiceException {
    Application app = null;
    try {
      app = delegate.getApplicationAvecObjetSecurisable(codeApplication,
          applicationActiveSeulement);
    } catch (Exception e) {
      throw new ServiceException(e);
    }
    return app;
  }

  public Application getApplicationAvecObjetSecurisable(String codeApplication,
      String codeFacette, boolean applicationActiveSeulement)
      throws ServiceException {
    Application application = null;

    try {
      application = delegate.getApplicationAvecObjetSecurisable(
          codeApplication, codeFacette, applicationActiveSeulement);
    } catch (Exception e) {
      throw new ServiceException(e);
    }

    return application;
  }
}