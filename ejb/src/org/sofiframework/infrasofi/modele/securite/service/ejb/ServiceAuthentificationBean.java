package org.sofiframework.infrasofi.modele.securite.service.ejb;

import javax.ejb.CreateException;

import org.sofiframework.application.securite.service.ServiceAuthentification;
import org.sofiframework.modele.ejb.exception.ServiceException;
import org.springframework.ejb.support.AbstractStatelessSessionBean;

/**
 * Composant serveur du service EJB d'authentification.
 * 
 * @author Jean-Maxime Pelletier
 */
public class ServiceAuthentificationBean extends AbstractStatelessSessionBean {

  private static final long serialVersionUID = 3762999779603391929L;

  private ServiceAuthentification delegate = null;

  protected void onEjbCreate() throws CreateException {
    delegate = (ServiceAuthentification) getBeanFactory().getBean(
        "serviceAuthentificationCommun");
  }

  public String getCodeUtilisateur(String certificat) throws ServiceException {
    String code = null;
    try {
      code = delegate.getCodeUtilisateur(certificat);
    } catch (Exception e) {
      throw new ServiceException(e);
    }
    return code;
  }

  public void terminerCertificat(String certificat) throws ServiceException {
    try {
      delegate.terminerCertificat(certificat);
    } catch (Exception e) {
      throw new ServiceException(e);
    }
  }

  public String genererCertificat(String codeUtilisateur)
      throws ServiceException {
    String certificat = null;
    try {
      delegate.genererCertificat(codeUtilisateur);
    } catch (Exception e) {
      throw new ServiceException(e);
    }
    return certificat;
  }
}