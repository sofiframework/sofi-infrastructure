package org.sofiframework.infrasofi.modele.securite.service.ejb;

import javax.ejb.CreateException;

import org.sofiframework.application.securite.objetstransfert.Utilisateur;
import org.sofiframework.application.securite.service.ServiceSecurite;
import org.sofiframework.modele.ejb.exception.ServiceException;
import org.springframework.ejb.support.AbstractStatelessSessionBean;

public class ServiceSecuriteBean extends AbstractStatelessSessionBean {

  private static final long serialVersionUID = 4331159472685937837L;

  private ServiceSecurite delegate = null;

  protected void onEjbCreate() throws CreateException {
    delegate = (ServiceSecurite) getBeanFactory().getBean(
        "serviceSecuriteCommun");
  }

  public Utilisateur getUtilisateurAvecObjetsSecurises(String codeApplication,
      String identifiant) throws ServiceException {
    Utilisateur utilisateur = null;

    try {
      utilisateur = delegate.getUtilisateurAvecObjetsSecurises(codeApplication,
          identifiant);
    } catch (Exception e) {
      throw new ServiceException(e);
    }

    return utilisateur;
  }

  public Utilisateur getUtilisateurAvecObjetsSecurises(String codeApplication,
      String codeFacette, String identifiant) throws ServiceException {
    Utilisateur utilisateur = null;

    try {
      delegate.getUtilisateurAvecObjetsSecurises(codeApplication, codeFacette,
          identifiant);
    } catch (Exception e) {
      throw new ServiceException(e);
    }

    return utilisateur;
  }

  public Utilisateur getUtilisateurSecuriseSelonClient(String codeApplication,
      String identifiant, String codeClient) throws ServiceException {
    Utilisateur utilisateur = null;

    try {
      utilisateur = delegate.getUtilisateurSecuriseSelonClient(codeApplication,
          identifiant, codeClient);
    } catch (Exception e) {
      throw new ServiceException(e);
    }

    return utilisateur;
  }

  public Utilisateur getUtilisateurSecuriseSelonClient(String codeApplication,
      String codeFacette, String identifiant, String codeClient)
      throws ServiceException {
    Utilisateur utilisateur = null;

    try {
      utilisateur = delegate.getUtilisateurSecuriseSelonClient(codeApplication,
          codeFacette, identifiant, codeClient);
    } catch (Exception e) {
      throw new ServiceException(e);
    }

    return utilisateur;
  }
}