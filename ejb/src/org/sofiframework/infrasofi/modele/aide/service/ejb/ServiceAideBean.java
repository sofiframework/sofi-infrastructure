package org.sofiframework.infrasofi.modele.aide.service.ejb;

import javax.ejb.CreateException;

import org.sofiframework.application.aide.objetstransfert.Aide;
import org.sofiframework.application.aide.service.ServiceAide;
import org.sofiframework.modele.ejb.exception.ServiceException;
import org.springframework.ejb.support.AbstractStatelessSessionBean;

/**
 * Composant serveur du service EJB d'aide contextuelle.
 * 
 * @author Jean-Maxime Pelletier
 * @author Marie-Soleil L'Allier
 * @author Stéphane Martineau
 */
public class ServiceAideBean extends AbstractStatelessSessionBean {

  private static final long serialVersionUID = -1831156741960046116L;

  private ServiceAide delegate = null;

  protected void onEjbCreate() throws CreateException {
    delegate = (ServiceAide) getBeanFactory().getBean("serviceAideCommun");
  }

  public Aide getAide(Long seqObjetSecurisable, String codeLangue)
      throws ServiceException {
    Aide aide = null;
    try {
      aide = delegate.getAide(seqObjetSecurisable, codeLangue);
    } catch (Exception e) {
      throw new ServiceException(e);
    }
    return aide;
  }

  public Aide getAideSelonCle(String cleAide, String codeLangue)
      throws ServiceException {
    Aide aide = null;
    try {
      aide = delegate.getAideSelonCle(cleAide, codeLangue);
    } catch (Exception e) {
      throw new ServiceException(e);
    }
    return aide;
  }
}