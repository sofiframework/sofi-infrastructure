package org.sofiframework.infrasofi.modele;

import org.sofiframework.modele.ejb.exception.ServiceException;

@SuppressWarnings("serial")
public abstract class AbstractStatelessSessionBean extends org.springframework.ejb.support.AbstractStatelessSessionBean {
  
  protected Object execute(DelegateExecution execution)
      throws org.sofiframework.modele.ejb.exception.ServiceException {
    try {
      return execution.execute();
    } catch (Exception e) {
      throw new ServiceException(e);
    }
  }
  
  protected abstract class DelegateExecution {
    public abstract Object execute();
  }
}
